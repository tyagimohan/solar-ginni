package com.solarginni.RaiseQuote;

import android.view.MotionEvent;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.solarginni.Backend.DAOs.QuoteImpDao;
import com.solarginni.Base.BaseActivity;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.ManufacturerModel;
import com.solarginni.ProspectUser.GetManuFacturerDataApi;
import com.solarginni.ProspectUser.ProspectViewPager;
import com.solarginni.ProspectUser.QutationModel;
import com.solarginni.R;
import com.solarginni.Utility.AppConstants;
import com.solarginni.Utility.Utils;

import java.util.List;

import static com.solarginni.Utility.AppConstants.ANYBATTERY;
import static com.solarginni.Utility.AppConstants.TOKEN;

public class RaiseQuotationActivity extends BaseActivity implements View.OnClickListener, RaiseQuoteFragment1.HybridSol, RaiseQuoteFilterFragement.SendData {
    public static ProspectViewPager viewPager;
    private ViewPagerAdapter adapter;


    @Override
    public int getLayout() {
        return R.layout.quotation_activity;
    }

    @Override
    public void initViews() {
        viewPager = findViewById(R.id.viewPager);
        adapter = new ViewPagerAdapter(getSupportFragmentManager());

        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });


    }

    @Override
    public void setUp() {

        if (Utils.hasNetwork(this)) {
            GetManuFacturerDataApi api = new GetManuFacturerDataApi() {
                @Override
                public void onComplete(CommomResponse response) {
                    ManufacturerModel manufacturerModel = response.toResponseModel(ManufacturerModel.class);
                    securePrefManager.storeSharedValue(AppConstants.MANUFACTURER_MODEL, Utils.toString(manufacturerModel));

                    viewPager.setAdapter(adapter);
                    viewPager.addOnPageChangeListener(adapter);
                    viewPager.setOffscreenPageLimit(3);
                }

                @Override
                protected void onFailur(String msg) {

                }
            };

            api.hit(securePrefManager.getSharedValue(TOKEN),true,"");
        } else {
            showToast("Please check Internet");
        }




        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });


    }

    @Override
    public void onClick(View view) {


    }

    @Override
    public void selectHybrid(boolean isHybrid) {

        if (!isHybrid) {
            RaiseQuoteFragment2.batteryLayout.setVisibility(View.GONE);
        } else {
            RaiseQuoteFragment2.batteryLayout.setVisibility(View.VISIBLE);

            RaiseQuoteFragment2.batteryType = ANYBATTERY;
        }

    }

    @Override
    public void sendData(List<QutationModel.Filter> filterList) {
        QuoteImpDao.getInstance(this).insertData(filterList);
        RaiseQuoteFragment3.adapter.notifyItemRangeRemoved(0, RaiseQuoteFragment3.adapter.getItemCount());
        RaiseQuoteFragment3.adapter.addAll(filterList);
        RaiseQuoteFragment3.adapter.notifyDataSetChanged();

    }

    public class ViewPagerAdapter extends FragmentStatePagerAdapter implements ViewPager.OnPageChangeListener {

        private Fragment[] childFragments;

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
            childFragments = new Fragment[]{
                    new RaiseQuoteFragment1(), //0
                    new RaiseQuoteFragment2(),
                    new RaiseQuoteFilterFragement(),
                    new RaiseQuoteFragment3()
            };
        }


        @Override
        public Fragment getItem(int position) {
            childFragments[position].setArguments(getIntent().getExtras());

            return childFragments[position];
        }

        @Override
        public int getCount() {
            return childFragments.length;
        }

        @Override
        public void onPageScrolled(int i, float v, int i1) {

        }

        @Override
        public void onPageSelected(int i) {


        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    }

}
