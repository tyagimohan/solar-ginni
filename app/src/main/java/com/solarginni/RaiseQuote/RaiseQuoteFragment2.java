package com.solarginni.RaiseQuote;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.solarginni.Base.BaseFragment;
import com.solarginni.Base.SolarGinieApplication;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.DemandQuoteModel;
import com.solarginni.DBModel.ManufacturerModel;
import com.solarginni.DBModel.OportunityResponse;
import com.solarginni.DBModel.QuotationModel;
import com.solarginni.DBModel.QuoteListModel;
import com.solarginni.EventBus.MessageEvent;
import com.solarginni.ProspectUser.ProspectContract;
import com.solarginni.ProspectUser.ProspectPresenter;
import com.solarginni.R;
import com.solarginni.Utility.AppConstants;
import com.solarginni.Utility.AppDialogs;
import com.solarginni.Utility.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import static com.solarginni.Utility.AppConstants.ANYBATTERY;
import static com.solarginni.Utility.AppConstants.ANYINVERTER;
import static com.solarginni.Utility.AppConstants.ANYPANEL;
import static com.solarginni.Utility.AppConstants.BO_MANAGER;
import static com.solarginni.Utility.AppConstants.COMPANYID;
import static com.solarginni.Utility.AppConstants.IMPLEMENTER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.MANUFACTURER_MODEL;
import static com.solarginni.Utility.AppConstants.OPTYID;
import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.SG_ADMIN;
import static com.solarginni.Utility.AppConstants.SUPER_IMPLEMENTER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.TOKEN;
import static com.solarginni.Utility.AppConstants.USER_ID;

public class RaiseQuoteFragment2 extends BaseFragment implements View.OnClickListener, ProspectContract.View {
    public static String batteryType = ANYBATTERY;
    public static LinearLayout batteryLayout;
    private HorizontalScrollView horizontalScrollview;
    private TextView level1, level2, level3, payment1, payment2, opex, capex;
    private String panelMake = ANYPANEL, inverterType = ANYINVERTER, level = "1", payment = "1", projectType = "CAPEX";
    private ProspectPresenter presenter;
    private boolean isUpdate = true;
    private boolean isQuote = false;
    private ImageView nextBtn, prevBtn, infoIcon;
    private LinearLayout panelMakeLayout, inverterMakeLayout;
    private LinearLayout batteryMakeLayout;
    private EventBus mEventBus;
    private Button createQuoteBtn;
    private LinearLayout projectTypeLay;
    private FrameLayout anyPanelType, twinperk, monoperc, polycrystalline;
    private ImageView polycrystallineTick, monoPercTick, twinPerkTick, anyTick;
    private String panelType = "Polycrystalline";
    private AppDialogs mAppDialogs;
    private ImageView payment1tickIcon, payment2tickIcon, level1tickIcon, level2tickIcon, level3tickIcon, capextickIcon, opextickIcon;
    private boolean isForNewUser=false;
    ManufacturerModel manufacturerModel;


    @Override
    public int getLayout() {
        return R.layout.quotation_choice;
    }

    @Override
    public void initViews(View view) {
        batteryLayout = view.findViewById(R.id.batteryLayout);
        panelMakeLayout = view.findViewById(R.id.panelMakeLayout);
        inverterMakeLayout = view.findViewById(R.id.inverterMakeLayout);
        batteryMakeLayout = view.findViewById(R.id.batteryMakeLayout);
        horizontalScrollview = view.findViewById(R.id.horizontalScrollview);

        nextBtn = view.findViewById(R.id.nextBtn);
        infoIcon = view.findViewById(R.id.infoIcon);
        payment1tickIcon = view.findViewById(R.id.payment1tickIcon);
        level3tickIcon = view.findViewById(R.id.level3tickIcon);
        level2tickIcon = view.findViewById(R.id.level2tickIcon);
        level1tickIcon = view.findViewById(R.id.level1tickIcon);
        payment2tickIcon = view.findViewById(R.id.payment2tickIcon);
        polycrystalline = view.findViewById(R.id.polycrystalline);
        polycrystallineTick = view.findViewById(R.id.polycrystallineTick);
        monoperc = view.findViewById(R.id.monoperc);
        monoPercTick = view.findViewById(R.id.monoPercTick);
        twinperk = view.findViewById(R.id.twinperk);
        twinPerkTick = view.findViewById(R.id.twinPerkTick);
        anyPanelType = view.findViewById(R.id.anyPanelType);
        anyTick = view.findViewById(R.id.anyTick);
        projectTypeLay = view.findViewById(R.id.projectType);
        capex = view.findViewById(R.id.capex);
        opex = view.findViewById(R.id.opex);
        prevBtn = view.findViewById(R.id.prevBtn);
        createQuoteBtn = view.findViewById(R.id.createQuoteBtn);
        level1 = view.findViewById(R.id.level1);
        level2 = view.findViewById(R.id.level2);
        level3 = view.findViewById(R.id.level3);
        mAppDialogs = new AppDialogs(getActivity());

        opextickIcon = view.findViewById(R.id.opextickIcon);
        capextickIcon = view.findViewById(R.id.capextickIcon);


        payment1 = view.findViewById(R.id.payment1);
        payment2 = view.findViewById(R.id.payment2);


        mEventBus = SolarGinieApplication.getMyApplication().getEventBus();
        registerEventBus();

        presenter = new ProspectPresenter(this);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mEventBus.unregister(this);
    }

    @Override
    public void setUp() {

        if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(IMPLEMENTER_ROLE_ID) | securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SUPER_IMPLEMENTER_ROLE_ID)){
            nextBtn.setVisibility(View.GONE);
            createQuoteBtn.setVisibility(View.VISIBLE);
        }

         manufacturerModel = Utils.getObj(securePrefManager.getSharedValue(MANUFACTURER_MODEL), ManufacturerModel.class);


        for (int i = 0; i < manufacturerModel.panelMakeList.size(); i++) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.manufacturer_item, null);
            ImageView panelImage = view.findViewById(R.id.panel1);
            ImageView tickIcon = view.findViewById(R.id.tickIcon);

            Glide.with(getContext()).load(manufacturerModel.panelMakeList.get(i).url).into(panelImage);

            if (manufacturerModel.panelMakeList.get(i)._default) {
                tickIcon.setVisibility(View.VISIBLE);
                this.panelMake = manufacturerModel.panelMakeList.get(i).companyId;
            }

            int finalI = i;



                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        panelMake = manufacturerModel.panelMakeList.get(finalI).companyId;
                        isUpdate = true;
                        refreshManufacturerMake(manufacturerModel.panelMakeList.size(), finalI, "PanelMake");

                    }
                });

            panelMakeLayout.addView(view);


        }

        for (int i = 0; i < manufacturerModel.inverterMakeList.size(); i++) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.manufacturer_item, null);
            ImageView panelImage = view.findViewById(R.id.panel1);
            ImageView tickIcon = view.findViewById(R.id.tickIcon);

            Glide.with(getContext()).load(manufacturerModel.inverterMakeList.get(i).url).into(panelImage);

            if (manufacturerModel.inverterMakeList.get(i)._default) {
                tickIcon.setVisibility(View.VISIBLE);
                this.inverterType = manufacturerModel.inverterMakeList.get(i).companyId;
            }
            int finalI = i;


                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        inverterType = manufacturerModel.inverterMakeList.get(finalI).companyId;
                        isUpdate = true;
                        refreshManufacturerMake(manufacturerModel.inverterMakeList.size(), finalI, "InverterMake");


                        Log.e("InverterMake", "" + manufacturerModel.inverterMakeList.get(finalI).companyId);
                    }
                });

            inverterMakeLayout.addView(view);

        }
        for (int i = 0; i < manufacturerModel.batteryMakeList.size(); i++) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.manufacturer_item, null);
            ImageView panelImage = view.findViewById(R.id.panel1);
            ImageView tickIcon = view.findViewById(R.id.tickIcon);

            Glide.with(getContext()).load(manufacturerModel.batteryMakeList.get(i).url).into(panelImage);

            if (manufacturerModel.batteryMakeList.get(i)._default) {
                tickIcon.setVisibility(View.VISIBLE);
                this.batteryType = manufacturerModel.batteryMakeList.get(i).companyId;
            }
            int finalI = i;


                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        batteryType = manufacturerModel.batteryMakeList.get(finalI).companyId;
                        isUpdate = true;
                        refreshManufacturerMake(manufacturerModel.batteryMakeList.size(), finalI, "BatteryMake");


                    }
                });




            batteryMakeLayout.addView(view);

        }
        /*For Panel Make*/
        if (getArguments() != null) {
            if (getArguments().getBoolean("usedForNewUser")){
                isForNewUser=true;

            }else{
                QuoteListModel.QuoteList data = (QuoteListModel.QuoteList) getArguments().getSerializable("data");

                if (data.getLovModel().getInverterMake() != null) {
//                setInverterType(data.getLovModel().getInverterMake());
//                setPanel(data.getLovModel().getPanelMake());

                    int finalI = 0;
                    for (int i = 0; i < manufacturerModel.panelMakeList.size(); i++) {
                        if (data.getLovModel().getPanelMake().equalsIgnoreCase(manufacturerModel.panelMakeList.get(i).companyId)) {
                            finalI = i;
                            break;
                        }
                    }

                    refreshManufacturerMake(manufacturerModel.panelMakeList.size(), finalI, "PanelMake");

                    int finalJ = 0;
                    for (int i = 0; i < manufacturerModel.inverterMakeList.size(); i++) {
                        if (data.getLovModel().getInverterMake().equalsIgnoreCase(manufacturerModel.inverterMakeList.get(i).companyId)) {
                            finalJ = i;
                            break;
                        }
                    }

                    refreshManufacturerMake(manufacturerModel.inverterMakeList.size(), finalJ, "InverterMake");


                    setServiceType(String.valueOf(data.getServiceLevel()));
                    setPaymentType(String.valueOf(data.getPaymentTerm()));

                }
                isQuote = data.getQuote();

                if (data.getLovModel().getSolution().equalsIgnoreCase("Hybrid")) {
                    batteryLayout.setVisibility(View.VISIBLE);
                    if (data.getLovModel().getBatteryMake() != null) {
                        int finalI = 0;
                        for (int i = 0; i < manufacturerModel.batteryMakeList.size(); i++) {
                            if (data.getLovModel().getBatteryMake().equalsIgnoreCase(manufacturerModel.batteryMakeList.get(i).companyId)) {
                                finalI = i;
                                break;
                            }
                        }
                        refreshManufacturerMake(manufacturerModel.batteryMakeList.size(), finalI, "BatteryMake");
                    } else {
                        refreshManufacturerMake(manufacturerModel.batteryMakeList.size(), 0, "BatteryMake");
                    }

                } else {
                    batteryLayout.setVisibility(View.GONE);
                }

            }



        }





            level1.setOnClickListener(this);
            level2.setOnClickListener(this);
            level3.setOnClickListener(this);
            infoIcon.setOnClickListener(this);

            polycrystalline.setOnClickListener(this);
            anyPanelType.setOnClickListener(this);
            twinperk.setOnClickListener(this);
            monoperc.setOnClickListener(this);
            payment1.setOnClickListener(this);
            capex.setOnClickListener(this);
            opex.setOnClickListener(this);
            payment2.setOnClickListener(this);


        nextBtn.setOnClickListener(this);
        prevBtn.setOnClickListener(this);
        createQuoteBtn.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        if (!(view.getId() == R.id.nextBtn))
            isUpdate = true;

        switch (view.getId()) {
            case R.id.level1:
                setServiceType("1");
                break;
            case R.id.level2:
                setServiceType("2");
                break;
            case R.id.level3:
                setServiceType("3");
                break;
            case R.id.payment1:
                setPaymentType("1");
                break;
            case R.id.payment2:
                setPaymentType("2");

                break;
            case R.id.capex:
                setProjectType("CAPEX");

                break;
            case R.id.opex:
                setProjectType("RESCO");

                break;
            case R.id.createQuoteBtn:
                if (Utils.hasNetwork(getContext())){
                    hitQuotationApi();
                }else{
                    showToast("Please check Internet");
                }
                break;
            case R.id.infoIcon:
                mAppDialogs.showInfoDialog("Ok", "Polycrystalline panels generate approx 1400 units per Kw in an year, MonoPERC generates 1500 units and Half Cut provided 1525 units or higher", new AppDialogs.DialogInfo() {
                    @Override
                    public void onPositiveButton(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }

                    @Override
                    public void onNegativeButton(DialogInterface dialog, int which) {

                    }
                });

                break;
            case R.id.nextBtn:


                if (isUpdate) {
                    if (Utils.hasNetwork(getContext()))
                        hitQuotationApi();
                    else
                        showToast("Please check Internet");
                } else {
                    RaiseQuotationActivity.viewPager.setCurrentItem(RaiseQuotationActivity.viewPager.getCurrentItem()+1);
                }



                break;
            case R.id.prevBtn:
                RaiseQuotationActivity.viewPager.setCurrentItem(RaiseQuotationActivity.viewPager.getCurrentItem()-1);
                break;
            case R.id.anyPanelType:
                panelType = "Any";
                anyTick.setVisibility(View.VISIBLE);
                monoPercTick.setVisibility(View.GONE);
                twinPerkTick.setVisibility(View.GONE);
                polycrystallineTick.setVisibility(View.GONE);
                break;
            case R.id.polycrystalline:
                panelType = "Polycrystalline";
                anyTick.setVisibility(View.GONE);
                monoPercTick.setVisibility(View.GONE);
                twinPerkTick.setVisibility(View.GONE);
                polycrystallineTick.setVisibility(View.VISIBLE);
                break;
            case R.id.monoperc:
                panelType = "Mono PERC";
                anyTick.setVisibility(View.GONE);
                monoPercTick.setVisibility(View.VISIBLE);
                twinPerkTick.setVisibility(View.GONE);
                polycrystallineTick.setVisibility(View.GONE);
                break;
            case R.id.twinperk:
                panelType = "Half Cut";
                anyTick.setVisibility(View.GONE);
                monoPercTick.setVisibility(View.GONE);
                twinPerkTick.setVisibility(View.VISIBLE);
                polycrystallineTick.setVisibility(View.GONE);
                break;

        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);



    }


    private void hitQuotationApi() {
        QuotationModel model = new QuotationModel();
        model.panelMake = panelMake;
        model.batteryMake = batteryType;
        model.inverterMake = inverterType;
        model.serviceLevel = level;
        model.paymentTerm = payment;
        model.panelType = panelType;
        model.projectType = projectType;
        model.userId = securePrefManager.getSharedValue(USER_ID);
        showProgress("Please wait...");
        presenter.saveFirstQuotattion(securePrefManager.getSharedValue(TOKEN), model, String.valueOf(RaiseQuotationActivity.viewPager.getCurrentItem() + 1), securePrefManager.getSharedValue(AppConstants.OPTYID));

    }


    private void setServiceType(String serviceType) {

        this.level = serviceType;

        switch (serviceType) {
            case "1":

                level3tickIcon.setVisibility(View.GONE);
                level1tickIcon.setVisibility(View.VISIBLE);
                level2tickIcon.setVisibility(View.GONE);

                level1.setBackgroundResource(R.drawable.box_bg);
                level1.setTextColor(Color.WHITE);


                level2.setBackgroundResource(R.drawable.border_bg);
                level2.setTextColor(getResources().getColor(R.color.colortheme));

                level3.setBackgroundResource(R.drawable.border_bg);
                level3.setTextColor(getResources().getColor(R.color.colortheme));

                break;
            case "2":
                level3tickIcon.setVisibility(View.GONE);
                level1tickIcon.setVisibility(View.GONE);
                level2tickIcon.setVisibility(View.VISIBLE);
                level2.setBackgroundResource(R.drawable.box_bg);
                level2.setTextColor(Color.WHITE);


                level1.setBackgroundResource(R.drawable.border_bg);
                level1.setTextColor(getResources().getColor(R.color.colortheme));

                level3.setBackgroundResource(R.drawable.border_bg);
                level3.setTextColor(getResources().getColor(R.color.colortheme));
                break;
            case "3":


                level3tickIcon.setVisibility(View.VISIBLE);
                level1tickIcon.setVisibility(View.GONE);
                level2tickIcon.setVisibility(View.GONE);

                level3.setBackgroundResource(R.drawable.box_bg);
                level3.setTextColor(Color.WHITE);

                level1.setBackgroundResource(R.drawable.border_bg);
                level1.setTextColor(getResources().getColor(R.color.colortheme));

                level2.setBackgroundResource(R.drawable.border_bg);
                level2.setTextColor(getResources().getColor(R.color.colortheme));

                break;


        }


    }

    private void setPaymentType(String payment) {

        this.payment = payment;

        switch (payment) {
            case "1":
                payment1.setBackgroundResource(R.drawable.box_bg);
                payment1.setTextColor(Color.WHITE);
                payment2tickIcon.setVisibility(View.GONE);
                payment1tickIcon.setVisibility(View.VISIBLE);

                payment2.setBackgroundResource(R.drawable.border_bg);
                payment2.setTextColor(getResources().getColor(R.color.colortheme));


                break;
            case "2":

                payment2.setBackgroundResource(R.drawable.box_bg);
                payment2.setTextColor(Color.WHITE);
                payment1tickIcon.setVisibility(View.GONE);
                payment2tickIcon.setVisibility(View.VISIBLE);
                payment1.setBackgroundResource(R.drawable.border_bg);
                payment1.setTextColor(getResources().getColor(R.color.colortheme));
                break;


        }


    }

    private void setProjectType(String type) {

        this.projectType = type;

        switch (type) {
            case "CAPEX":
                capex.setBackgroundResource(R.drawable.box_bg);
                capex.setTextColor(Color.WHITE);

                capextickIcon.setVisibility(View.VISIBLE);
                opextickIcon.setVisibility(View.GONE);
                opex.setBackgroundResource(R.drawable.border_bg);
                opex.setTextColor(getResources().getColor(R.color.colortheme));


                break;
            case "RESCO":
                opex.setBackgroundResource(R.drawable.box_bg);
                opex.setTextColor(Color.WHITE);

                capextickIcon.setVisibility(View.GONE);
                opextickIcon.setVisibility(View.VISIBLE);

                capex.setBackgroundResource(R.drawable.border_bg);
                capex.setTextColor(getResources().getColor(R.color.colortheme));
                break;


        }


    }

    @Override
    public void onFetchAccountInfo(CommomResponse response) {

    }

    @Override
    public void onFetchLandingDetails(CommomResponse response) {

    }

    @Override
    public void onSaveFirstQuotation(OportunityResponse response) {

        if (createQuoteBtn.getVisibility()==View.VISIBLE) {
            DemandQuoteModel model = new DemandQuoteModel();
            model.setImpleId(securePrefManager.getSharedValue(COMPANYID));

            model.setImpleSite(0);
            model.setPriceRange(70000);
            model.setSiteDist(100);
            model.sortedValue = 0;
            model.empanel = false;
//                        model.userId=securePrefManager.getSharedValue(USER_ID);
            presenter.getQuote(getContext(), securePrefManager.getSharedValue(TOKEN), model, securePrefManager.getSharedValue(OPTYID));

        }
        else{
            hideProgress();
            isUpdate = false;
            RaiseQuotationActivity.viewPager.setCurrentItem(RaiseQuotationActivity.viewPager.getCurrentItem()+1);

        }
//
//        showToast("Saved Second Quote");


    }

    @Override
    public void fetChQuotation(CommomResponse response) {
//        hideProgress();
//        isUpdate = false;
//
//        QutationModel model = response.toResponseModel(QutationModel.class);
//        SD.sendData(model.getFilter());
//        RaiseQuotationActivity.viewPager.setCurrentItem(2);


    }

    @Override
    public void fetchPendingQuote(CommomResponse response) {

    }

    @Override
    public void onGetQuote() {
        hideProgress();
        showToast("A new Quote has been raised");
        getActivity().finish();

    }

    @Override
    public void guestUserApi(CommomResponse response) {

    }

    @Override
    public void onFailure(String msg) {
        hideProgress();
        showToast(msg);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(MessageEvent event) {

        if (Double.parseDouble(event.newText) >= 50.0) {

            projectTypeLay.setVisibility(View.VISIBLE);
        } else {
            projectTypeLay.setVisibility(View.GONE);

        }



         manufacturerModel = event.response.toResponseModel(ManufacturerModel.class);
        panelMakeLayout.removeAllViews();
        inverterMakeLayout.removeAllViews();
        batteryMakeLayout.removeAllViews();

        for (int i = 0; i < manufacturerModel.panelMakeList.size(); i++) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.manufacturer_item, null);
            ImageView panelImage = view.findViewById(R.id.panel1);
            ImageView tickIcon = view.findViewById(R.id.tickIcon);

            Glide.with(getContext()).load(manufacturerModel.panelMakeList.get(i).url).into(panelImage);

            if (manufacturerModel.panelMakeList.get(i)._default) {
                tickIcon.setVisibility(View.VISIBLE);
                this.panelMake = manufacturerModel.panelMakeList.get(i).companyId;
            }

            int finalI = i;



                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        panelMake = manufacturerModel.panelMakeList.get(finalI).companyId;
                        isUpdate = true;
                        refreshManufacturerMake(manufacturerModel.panelMakeList.size(), finalI, "PanelMake");

                    }
                });

            panelMakeLayout.addView(view);


        }

        for (int i = 0; i < manufacturerModel.inverterMakeList.size(); i++) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.manufacturer_item, null);
            ImageView panelImage = view.findViewById(R.id.panel1);
            ImageView tickIcon = view.findViewById(R.id.tickIcon);

            Glide.with(getContext()).load(manufacturerModel.inverterMakeList.get(i).url).into(panelImage);

            if (manufacturerModel.inverterMakeList.get(i)._default) {
                tickIcon.setVisibility(View.VISIBLE);
                this.inverterType = manufacturerModel.inverterMakeList.get(i).companyId;
            }
            int finalI = i;


                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        inverterType = manufacturerModel.inverterMakeList.get(finalI).companyId;
                        isUpdate = true;
                        refreshManufacturerMake(manufacturerModel.inverterMakeList.size(), finalI, "InverterMake");


                        Log.e("InverterMake", "" + manufacturerModel.inverterMakeList.get(finalI).companyId);
                    }
                });

            inverterMakeLayout.addView(view);

        }
        for (int i = 0; i < manufacturerModel.batteryMakeList.size(); i++) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.manufacturer_item, null);
            ImageView panelImage = view.findViewById(R.id.panel1);
            ImageView tickIcon = view.findViewById(R.id.tickIcon);

            Glide.with(getContext()).load(manufacturerModel.batteryMakeList.get(i).url).into(panelImage);

            if (manufacturerModel.batteryMakeList.get(i)._default) {
                tickIcon.setVisibility(View.VISIBLE);
                this.batteryType = manufacturerModel.batteryMakeList.get(i).companyId;
            }
            int finalI = i;


                view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        batteryType = manufacturerModel.batteryMakeList.get(finalI).companyId;
                        isUpdate = true;
                        refreshManufacturerMake(manufacturerModel.batteryMakeList.size(), finalI, "BatteryMake");


                    }
                });



            batteryMakeLayout.addView(view);

        }

    }




    private void registerEventBus() {
        if (!mEventBus.isRegistered(RaiseQuoteFragment2.this))
            mEventBus.register(this);
    }

    private void refreshManufacturerMake(int size, int finalI, String manuFacturerMake) {


        if (!isQuote) {
            if (manuFacturerMake.equalsIgnoreCase("InverterMake")) {

                for (int i = 0; i < size; i++) {

                    if (i == finalI) {
                        inverterMakeLayout.getChildAt(i).findViewById(R.id.tickIcon).setVisibility(View.VISIBLE);
                    } else {
                        inverterMakeLayout.getChildAt(i).findViewById(R.id.tickIcon).setVisibility(View.GONE);

                    }


                }
            } else if (manuFacturerMake.equalsIgnoreCase("PanelMake")) {
                for (int i = 0; i < size; i++) {

                    if (i == finalI) {
                        panelMakeLayout.getChildAt(i).findViewById(R.id.tickIcon).setVisibility(View.VISIBLE);
                    } else {
                        panelMakeLayout.getChildAt(i).findViewById(R.id.tickIcon).setVisibility(View.GONE);

                    }


                }
            } else if (manuFacturerMake.equalsIgnoreCase("BatteryMake")) {
                for (int i = 0; i < size; i++) {

                    if (i == finalI) {
                        batteryMakeLayout.getChildAt(i).findViewById(R.id.tickIcon).setVisibility(View.VISIBLE);
                    } else {
                        batteryMakeLayout.getChildAt(i).findViewById(R.id.tickIcon).setVisibility(View.GONE);

                    }


                }
            }

        }


    }

}
