package com.solarginni.RaiseQuote;

import android.content.Context;

import com.solarginni.Backend.DAOs.QuoteImpDao;
import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.DemandQuoteModel;
import com.solarginni.ProspectUser.QutationModel;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;

import retrofit2.Retrofit;

public class RaiseQuoteInteractor implements RaiseQuoteContract.Intreactor {

    private Retrofit retrofit;
    private ApiInterface apiInterface;

    public RaiseQuoteInteractor(Retrofit retrofit) {
        this.retrofit = retrofit;
        apiInterface= retrofit.create(ApiInterface.class);
    }

    @Override
    public void getQuote(Context context,String token, DemandQuoteModel model,String optId, RaiseQuoteContract.OnInteraction interaction) {
        apiInterface.demandQuote(token,model,optId).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                interaction.onGetQuote();
                QuoteImpDao.getInstance(context).updateStatus(model.getImpleId());

            }

            @Override
            public void onError(ApiError error) {
                interaction.failure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                interaction.failure(throwable.getMessage());

            }
        });

    }

    @Override
    public void sortListByValue(Context context, String value, RaiseQuoteContract.OnInteraction interaction) {
        interaction.onApplyFilter(QuoteImpDao.getInstance(context).sortListByValue(value));

    }

    @Override
    public void applyFilter(Context context, String token, String price, String distance, String siteCount, String optId, boolean isEmpaneled , int sorting, RaiseQuoteContract.OnInteraction interaction) {

        apiInterface.filterQuote(token,distance,price,siteCount,optId,isEmpaneled,sorting).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                QutationModel model = response.toResponseModel(QutationModel.class);
                QuoteImpDao.getInstance(context).insertData(model.getFilter());
                interaction.onApplyFilter(model.getFilter());

            }

            @Override
            public void onError(ApiError error) {

                interaction.failure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
             interaction.failure(throwable.getMessage());
            }
        });

    }


}
