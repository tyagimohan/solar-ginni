package com.solarginni.RaiseQuote;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.ProspectUser.QutationModel;
import com.solarginni.R;
import com.solarginni.Utility.SecurePrefManager;

import static com.solarginni.Utility.AppConstants.BO_MANAGER;
import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.SG_ADMIN;

public class RaiseQuoteImplementerListAdapter extends BaseRecyclerAdapter<QutationModel.Filter, RaiseQuoteImplementerListAdapter.Holder> {
    private int demandCount=0;
    public RaiseQuoteImplementerListAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public int getLayout(int viewType) {
        return R.layout.demand_quote_item;
    }

    @Override
    public Holder getViewHolder(View view, int viewType) {
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {


        holder.implementerName.setText(getItem(i).getCompanyName());

        holder.siteSize.setText(String.format("%.2f", getItem(i).getSitesize())+ " Kw");

        if (getItem(i).url!=null){
            holder.implementerIcon.setVisibility(View.VISIBLE);
            ImageLoader.getInstance().displayImage(getItem(i).url,holder.implementerIcon);
        }
        else {
            holder.implementerIcon.setVisibility(View.GONE);
        }

        holder.totalSite.setText(getItem(i).getSitecount().toString());
        holder.stateSite.setText(getItem(i).getStateSiteCount().toString());
            holder.demandQuote.setVisibility(View.VISIBLE);
            if (getItem(i).isQuoted()) {
                demandCount++;
                holder.demandQuote.setEnabled(false);
                holder.demandQuoteLay.setEnabled(false);
                holder.demandImage.setBackgroundResource(R.drawable.demand_quote);
                holder.demandQuote.setText("Demanded");
                holder.demandQuote.setTextColor(getContext().getResources().getColor(R.color.grey));
            } else {
                holder.demandQuote.setEnabled(true);
                holder.demandQuoteLay.setEnabled(true);
                holder.demandQuote.setText("Demand Quote");
                holder.demandImage.setBackgroundResource(R.drawable.trans_blub);
                holder.demandQuote.setTextColor(getContext().getResources().getColor(R.color.colortheme));
            }





    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView demandQuote;

        TextView implementerName, siteSize, totalSite,stateSite,showMore;
        ImageView demandImage,implementerIcon;
        LinearLayout  demandQuoteLay;

        public Holder(@NonNull View itemView) {
            super(itemView);

            implementerName = itemView.findViewById(R.id.implementerName);
            demandQuote = itemView.findViewById(R.id.demandQuote);
            siteSize = itemView.findViewById(R.id.siteSize);
            demandImage = itemView.findViewById(R.id.demandImage);
            implementerIcon = itemView.findViewById(R.id.implementerIcon);
            totalSite = itemView.findViewById(R.id.totalSite);
            stateSite = itemView.findViewById(R.id.totalStateSite);
            showMore = itemView.findViewById(R.id.showMore);
            demandQuoteLay = itemView.findViewById(R.id.demandQuoteLayout);
            demandQuoteLay.setOnClickListener(this);
            showMore.setOnClickListener(this);


        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.showMore:
                    getLongClickListener().onItemClick(getItem(getAdapterPosition()), getAdapterPosition(), view);
                    break;
                case R.id.demandQuoteLayout:
                    getListener().onClick(getItem(getAdapterPosition()), getAdapterPosition());

                    break;
                default:
                    break;
            }

        }


    }

}
