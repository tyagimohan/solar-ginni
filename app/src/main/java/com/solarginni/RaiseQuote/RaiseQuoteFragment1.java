package com.solarginni.RaiseQuote;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.solarginni.Base.BaseFragment;
import com.solarginni.Base.SolarGinieApplication;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.OportunityResponse;
import com.solarginni.DBModel.QuotationModel;
import com.solarginni.DBModel.QuoteListModel;
import com.solarginni.EventBus.MessageEvent;
import com.solarginni.ProspectUser.GetManuFacturerDataApi;
import com.solarginni.ProspectUser.ProspectContract;
import com.solarginni.ProspectUser.ProspectPresenter;
import com.solarginni.R;
import com.solarginni.Utility.AppDialogs;
import com.solarginni.Utility.DecimalDigitsInputFilter;
import com.solarginni.Utility.Utils;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.solarginni.Utility.AppConstants.BO_MANAGER;
import static com.solarginni.Utility.AppConstants.IMPLEMENTER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.OPTYID;
import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.SG_ADMIN;
import static com.solarginni.Utility.AppConstants.SUPER_IMPLEMENTER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.TOKEN;
import static com.solarginni.Utility.AppConstants.USER_ID;

/**
 * Created by Mohan on 13/3/20.
 */

public class RaiseQuoteFragment1 extends BaseFragment implements View.OnClickListener, ProspectContract.View {

    private static final int REQUEST_FOR_PLACE = 2;
    public static Double lat, lng;
    public String roofType = "Concrete-Flat", sitetype = "Residential", phaseType = "Three", siteSize;
    private EditText etSiteSize;
    private String solutionsType = "On-Grid";
    private LinearLayout religiousSite, commercialSite, industrialSite, institutionalSite, residentialSite, onGridBtn, HybridBtn, concreteBtn, othersBtn, tine_shedBtn, titledBtn, otherSite;
    private Button needEvalator;
    private ImageView nextBtn;
    private Button singlePhaseBtn, threePhaseBtn;
    private ProspectPresenter presenter;
    private boolean isUpdate = true, isFrom = true;
    private HybridSol sol;
    private boolean isQuote = false;
    private EditText city, state, country, pincode, etLine1, addLine2;
    private Switch addressToggle;
    private EventBus mEventBus;
    private HorizontalScrollView locationLayoutHorizontal;
    private AppDialogs mAppDialogs;
    private EditText etBill;
    private boolean isForNewUser=false;
    private String userState="";

    private final ToggleButton.OnCheckedChangeListener mToggleListener = (compoundButton, b
    ) -> {
        isUpdate = true;
        locationLayoutHorizontal.setVisibility(b ? View.GONE : View.VISIBLE);
        if (addressToggle.isChecked()) {
            hideKeyboard(getActivity());
        }
    };
    private Geocoder mGeocoder;
    private int residentialUnitPrice=840;
    private int nonResdenitalUnitPrice=1440;

    @Override
    public int getLayout() {
        return R.layout.quotation_form1;
    }

    @Override
    public void initViews(View view) {
        singlePhaseBtn = view.findViewById(R.id.phase_single);
        threePhaseBtn = view.findViewById(R.id.phase_three);
        commercialSite = view.findViewById(R.id.commercial);
        etBill = view.findViewById(R.id.etBill);
        religiousSite = view.findViewById(R.id.religious);
        otherSite = view.findViewById(R.id.otherSite);
        locationLayoutHorizontal = view.findViewById(R.id.locationLayout);
        residentialSite = view.findViewById(R.id.resdential_site);
        institutionalSite = view.findViewById(R.id.institutional_site);
        industrialSite = view.findViewById(R.id.industrial);
        onGridBtn = view.findViewById(R.id.on_grid);
        HybridBtn = view.findViewById(R.id.hybrid);
        concreteBtn = view.findViewById(R.id.concrete);
        othersBtn = view.findViewById(R.id.others);
        needEvalator = view.findViewById(R.id.needEvalator);
        tine_shedBtn = view.findViewById(R.id.tine_shed);
        titledBtn = view.findViewById(R.id.titled);
        nextBtn = view.findViewById(R.id.nextBtn);
        addressToggle = view.findViewById(R.id.reminderToggle);
        etSiteSize = view.findViewById(R.id.etSiteSize);

        etLine1 = view.findViewById(R.id.etLine1);
        addLine2 = view.findViewById(R.id.line2);
        state = view.findViewById(R.id.state);
        city = view.findViewById(R.id.cityName);
        country = view.findViewById(R.id.country);
        pincode = view.findViewById(R.id.pincode);

        mAppDialogs = new AppDialogs(getActivity());


        mEventBus = SolarGinieApplication.getMyApplication().getEventBus();


        if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SG_ADMIN) | securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(BO_MANAGER)) {
            needEvalator.setVisibility(View.GONE);

        } else {

//            SpannableString siteSize = new SpannableString("Evaluate your Need");
//            siteSize.setSpan(new UnderlineSpan(), 0, 18, 0);
//            needEvalator.setText(siteSize);
            needEvalator.setVisibility(View.GONE);
            needEvalator.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(openLink("https://solarginie.com/evaluator"));
                }
            });


        }

        disableSuggestion(city);
        disableSuggestion(etLine1);
        disableSuggestion(addLine2);
        disableSuggestion(country);
        disableSuggestion(state);
        disableSuggestion(pincode);

        view.findViewById(R.id.infoIcon).setOnClickListener(this);

        etSiteSize.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(7, 2)});
        etSiteSize.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                isUpdate = true;
                String value = etSiteSize.getText().toString();
                if (value.startsWith(".")) {
                    etSiteSize.setText("0.");
                    etSiteSize.setSelection(etSiteSize.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {


            }
        });
        presenter = new ProspectPresenter(this);


    }

    @Override
    public void setUp() {


//        etBill.setOnEditorActionListener((v, actionId, event) -> {
//            if (actionId == EditorInfo.IME_ACTION_DONE) {
//                if (Double.valueOf(etBill.getText().toString())>=840.0){
//                    etSiteSize.setText(calculatedSolarSite(Double.valueOf(etBill.getText().toString())));
//                }
//                else {
//                    showToast("Sorry");
//                }
//
//                return true;
//            }
//            return false;
//        });

         if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(IMPLEMENTER_ROLE_ID) | securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SUPER_IMPLEMENTER_ROLE_ID)){
            addressToggle.setChecked(false);
            getView().findViewById(R.id.siteAddresSwitch).setVisibility(View.GONE);
        }

        etBill.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    if (Double.valueOf(etBill.getText().toString())>=840.0){
                        etSiteSize.setText(calculatedSolarSite(Double.valueOf(etBill.getText().toString())));
                    }
                    else {
                        etSiteSize.setText("");
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        if (getArguments() != null) {
            if (getArguments().getBoolean("usedForNewUser")){
                isForNewUser=true;
                com.solarginni.SiteLovsModule.SiteDetailModel.Address address= (com.solarginni.SiteLovsModule.SiteDetailModel.Address) getArguments().getSerializable("address");
                       state.setText(address.getState());

            }else{
                  QuoteListModel.QuoteList data = (QuoteListModel.QuoteList) getArguments().getSerializable("data");
            setRoofStyle(data.getLovModel().getRoofStyle());
            setSiteType(data.getLovModel().getSiteType());
            isQuote = data.getQuote();


//            SpannableString siteSize = new SpannableString(data.getUserId());
//            siteSize.setSpan(new UnderlineSpan(), 0, data.getUserId().length(), 0);
//            cusotmerId.setText(siteSize);


//            cusotmerId.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Bundle bundle = new Bundle();
//                    bundle.putBoolean("isFormasking", false);
//                    bundle.putString("id", data.getUserId());
//                    goToNextScreen(CustomerDetailActivity.class, bundle);
//                }
//            });
            isFrom = !getArguments().getBoolean("isFrom");
            if (data.getLovModel().getPhase().equalsIgnoreCase("Single"))
                setPhaseButton(true);
            else
                setPhaseButton(false);

            if (data.getLovModel().getSolution().equalsIgnoreCase("Hybrid")) {
                setSolution(false);
            } else {
                setSolution(true);

            }
            etSiteSize.setText(data.getSolarSize());

            etLine1.setText(data.getAddress().getAddressLine1());
            etLine1.setVisibility(View.VISIBLE);
            addLine2.setText(data.getAddress().getAddressLine2());
            city.setText(data.getAddress().getCity());
            state.setText(data.getAddress().getState());
            country.setText(data.getAddress().getCountry());
            pincode.setText(data.getAddress().getPinCode());
            needEvalator.setVisibility(View.GONE);
            locationLayoutHorizontal.setVisibility(View.VISIBLE);

            }


        }

            singlePhaseBtn.setOnClickListener(this);
            threePhaseBtn.setOnClickListener(this);
            commercialSite.setOnClickListener(this);
            religiousSite.setOnClickListener(this);
            residentialSite.setOnClickListener(this);
            institutionalSite.setOnClickListener(this);
            industrialSite.setOnClickListener(this);
            onGridBtn.setOnClickListener(this);
            HybridBtn.setOnClickListener(this);
            concreteBtn.setOnClickListener(this);
            tine_shedBtn.setOnClickListener(this);
            otherSite.setOnClickListener(this);
            othersBtn.setOnClickListener(this);
            titledBtn.setOnClickListener(this);
            etSiteSize.setFocusableInTouchMode(true);
            etLine1.setFocusableInTouchMode(true);
            etLine1.setFocusable(true);
            etBill.setFocusableInTouchMode(true);
            pincode.setFocusable(true);
            pincode.setFocusableInTouchMode(true);
            addLine2.setOnClickListener(this);
            addressToggle.setOnCheckedChangeListener(mToggleListener);
            addressToggle.setEnabled(true);

            needEvalator.setVisibility(View.GONE);





        nextBtn.setOnClickListener(this);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof HybridSol) {

            sol = (HybridSol) context;
        } else {

            throw new RuntimeException(context.toString()
                    + " must implement TextClicked");


        }

    }

    @Override
    public void onDetach() {
        // => avoid leaking,
        super.onDetach();
        sol = null;
    }


    private void setSiteType(String siteType) {

        this.sitetype = siteType;

        switch (siteType) {
            case "Religious":
                religiousSite.setBackgroundResource(R.drawable.box_bg);
                ((ImageView) getView().findViewById(R.id.reiigious_icon)).setBackgroundResource(R.drawable.religious_sel);
                ((TextView) getView().findViewById(R.id.religious_text)).setTextColor(getResources().getColor(R.color.white));

                industrialSite.setBackgroundResource(R.drawable.border_bg);

                ((ImageView) getView().findViewById(R.id.industrial_icon)).setBackgroundResource(R.drawable.industrial);
                ((TextView) getView().findViewById(R.id.industrial_text)).setTextColor(getResources().getColor(R.color.colortheme));

                commercialSite.setBackgroundResource(R.drawable.border_bg);

                ((ImageView) getView().findViewById(R.id.commercial_icon)).setBackgroundResource(R.drawable.commercial);
                ((TextView) getView().findViewById(R.id.commercial_text)).setTextColor(getResources().getColor(R.color.colortheme));

                institutionalSite.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.institutional_icon)).setBackgroundResource(R.drawable.institutional);
                ((TextView) getView().findViewById(R.id.institutional_text)).setTextColor(getResources().getColor(R.color.colortheme));

                residentialSite.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.residential_icon)).setBackgroundResource(R.drawable.residential_sel);
                ((TextView) getView().findViewById(R.id.residential_text)).setTextColor(getResources().getColor(R.color.colortheme));

                otherSite.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.others_icon)).setBackgroundResource(R.drawable.other);
                ((TextView) getView().findViewById(R.id.others_text)).setTextColor(getResources().getColor(R.color.colortheme));

                break;
            case "Commercial":

                commercialSite.setBackgroundResource(R.drawable.box_bg);

                ((ImageView) getView().findViewById(R.id.commercial_icon)).setBackgroundResource(R.drawable.commercial_sel);
                ((TextView) getView().findViewById(R.id.commercial_text)).setTextColor(getResources().getColor(R.color.white));

                industrialSite.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.industrial_icon)).setBackgroundResource(R.drawable.industrial);
                ((TextView) getView().findViewById(R.id.industrial_text)).setTextColor(getResources().getColor(R.color.colortheme));

                religiousSite.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.reiigious_icon)).setBackgroundResource(R.drawable.religoiuse);
                ((TextView) getView().findViewById(R.id.religious_text)).setTextColor(getResources().getColor(R.color.colortheme));

                institutionalSite.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.institutional_icon)).setBackgroundResource(R.drawable.institutional);
                ((TextView) getView().findViewById(R.id.institutional_text)).setTextColor(getResources().getColor(R.color.colortheme));

                residentialSite.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.residential_icon)).setBackgroundResource(R.drawable.residential_sel);
                ((TextView) getView().findViewById(R.id.residential_text)).setTextColor(getResources().getColor(R.color.colortheme));

                otherSite.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.others_icon)).setBackgroundResource(R.drawable.other);
                ((TextView) getView().findViewById(R.id.others_text)).setTextColor(getResources().getColor(R.color.colortheme));
                break;
            case "Industrial":
                industrialSite.setBackgroundResource(R.drawable.box_bg);
                ((ImageView) getView().findViewById(R.id.industrial_icon)).setBackgroundResource(R.drawable.industrial_sel);
                ((TextView) getView().findViewById(R.id.industrial_text)).setTextColor(getResources().getColor(R.color.white));

                commercialSite.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.commercial_icon)).setBackgroundResource(R.drawable.commercial);
                ((TextView) getView().findViewById(R.id.commercial_text)).setTextColor(getResources().getColor(R.color.colortheme));

                religiousSite.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.reiigious_icon)).setBackgroundResource(R.drawable.religoiuse);
                ((TextView) getView().findViewById(R.id.religious_text)).setTextColor(getResources().getColor(R.color.colortheme));

                institutionalSite.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.institutional_icon)).setBackgroundResource(R.drawable.institutional);
                ((TextView) getView().findViewById(R.id.institutional_text)).setTextColor(getResources().getColor(R.color.colortheme));

                residentialSite.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.residential_icon)).setBackgroundResource(R.drawable.residential_sel);
                ((TextView) getView().findViewById(R.id.residential_text)).setTextColor(getResources().getColor(R.color.colortheme));

                otherSite.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.others_icon)).setBackgroundResource(R.drawable.other);
                ((TextView) getView().findViewById(R.id.others_text)).setTextColor(getResources().getColor(R.color.colortheme));
                break;
            case "Institutional":

                institutionalSite.setBackgroundResource(R.drawable.box_bg);
                ((ImageView) getView().findViewById(R.id.institutional_icon)).setBackgroundResource(R.drawable.institutional_sel);
                ((TextView) getView().findViewById(R.id.institutional_text)).setTextColor(getResources().getColor(R.color.white));


                residentialSite.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.residential_icon)).setBackgroundResource(R.drawable.residential_sel);
                ((TextView) getView().findViewById(R.id.residential_text)).setTextColor(getResources().getColor(R.color.colortheme));

                otherSite.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.others_icon)).setBackgroundResource(R.drawable.other);
                ((TextView) getView().findViewById(R.id.others_text)).setTextColor(getResources().getColor(R.color.colortheme));

                commercialSite.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.commercial_icon)).setBackgroundResource(R.drawable.commercial);
                ((TextView) getView().findViewById(R.id.commercial_text)).setTextColor(getResources().getColor(R.color.colortheme));

                religiousSite.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.reiigious_icon)).setBackgroundResource(R.drawable.religoiuse);
                ((TextView) getView().findViewById(R.id.religious_text)).setTextColor(getResources().getColor(R.color.colortheme));


                industrialSite.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.industrial_icon)).setBackgroundResource(R.drawable.industrial);
                ((TextView) getView().findViewById(R.id.industrial_text)).setTextColor(getResources().getColor(R.color.colortheme));


                break;
            case "Residential":

                residentialSite.setBackgroundResource(R.drawable.box_bg);
                ((ImageView) getView().findViewById(R.id.residential_icon)).setBackgroundResource(R.drawable.residential);
                ((TextView) getView().findViewById(R.id.residential_text)).setTextColor(getResources().getColor(R.color.white));

                otherSite.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.others_icon)).setBackgroundResource(R.drawable.other);
                ((TextView) getView().findViewById(R.id.others_text)).setTextColor(getResources().getColor(R.color.colortheme));

                commercialSite.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.commercial_icon)).setBackgroundResource(R.drawable.commercial);
                ((TextView) getView().findViewById(R.id.commercial_text)).setTextColor(getResources().getColor(R.color.colortheme));

                religiousSite.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.reiigious_icon)).setBackgroundResource(R.drawable.religoiuse);
                ((TextView) getView().findViewById(R.id.religious_text)).setTextColor(getResources().getColor(R.color.colortheme));

                institutionalSite.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.institutional_icon)).setBackgroundResource(R.drawable.institutional);
                ((TextView) getView().findViewById(R.id.institutional_text)).setTextColor(getResources().getColor(R.color.colortheme));


                industrialSite.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.industrial_icon)).setBackgroundResource(R.drawable.industrial);
                ((TextView) getView().findViewById(R.id.industrial_text)).setTextColor(getResources().getColor(R.color.colortheme));


                break;
            case "Others":


                otherSite.setBackgroundResource(R.drawable.box_bg);
                ((ImageView) getView().findViewById(R.id.others_icon)).setBackgroundResource(R.drawable.other_sel);
                ((TextView) getView().findViewById(R.id.others_text)).setTextColor(getResources().getColor(R.color.white));


                residentialSite.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.residential_icon)).setBackgroundResource(R.drawable.residential_sel);
                ((TextView) getView().findViewById(R.id.residential_text)).setTextColor(getResources().getColor(R.color.colortheme));

                commercialSite.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.commercial_icon)).setBackgroundResource(R.drawable.commercial);
                ((TextView) getView().findViewById(R.id.commercial_text)).setTextColor(getResources().getColor(R.color.colortheme));

                religiousSite.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.reiigious_icon)).setBackgroundResource(R.drawable.religoiuse);
                ((TextView) getView().findViewById(R.id.religious_text)).setTextColor(getResources().getColor(R.color.colortheme));

                institutionalSite.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.institutional_icon)).setBackgroundResource(R.drawable.institutional);
                ((TextView) getView().findViewById(R.id.institutional_text)).setTextColor(getResources().getColor(R.color.colortheme));


                industrialSite.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.industrial_icon)).setBackgroundResource(R.drawable.industrial);
                ((TextView) getView().findViewById(R.id.industrial_text)).setTextColor(getResources().getColor(R.color.colortheme));

                break;

        }


    }


    private void setRoofStyle(String roofStyle) {
        this.roofType = roofStyle;

        switch (roofStyle) {
            case "Concrete-Flat":
                concreteBtn.setBackgroundResource(R.drawable.box_bg);
                ((ImageView) getView().findViewById(R.id.concrete_icon)).setBackgroundResource(R.drawable.concrete);
                ((TextView) getView().findViewById(R.id.concrete_text)).setTextColor(getResources().getColor(R.color.white));


                titledBtn.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.tilted_icon)).setBackgroundResource(R.drawable.tilled);
                ((TextView) getView().findViewById(R.id.titlted_text)).setTextColor(getResources().getColor(R.color.colortheme));


                tine_shedBtn.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.tine_shed_icon)).setBackgroundResource(R.drawable.tin_shed);
                ((TextView) getView().findViewById(R.id.tine_shed_text)).setTextColor(getResources().getColor(R.color.colortheme));

                othersBtn.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.others_roof)).setBackgroundResource(R.drawable.other);
                ((TextView) getView().findViewById(R.id.others_roof_text)).setTextColor(getResources().getColor(R.color.colortheme));

                break;
            case "Tilted":
                titledBtn.setBackgroundResource(R.drawable.box_bg);
                ((ImageView) getView().findViewById(R.id.tilted_icon)).setBackgroundResource(R.drawable.titled_sel);
                ((TextView) getView().findViewById(R.id.titlted_text)).setTextColor(getResources().getColor(R.color.white));

                concreteBtn.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.concrete_icon)).setBackgroundResource(R.drawable.concret_sel);
                ((TextView) getView().findViewById(R.id.concrete_text)).setTextColor(getResources().getColor(R.color.colortheme));

                tine_shedBtn.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.tine_shed_icon)).setBackgroundResource(R.drawable.tin_shed);
                ((TextView) getView().findViewById(R.id.tine_shed_text)).setTextColor(getResources().getColor(R.color.colortheme));

                othersBtn.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.others_roof)).setBackgroundResource(R.drawable.other);
                ((TextView) getView().findViewById(R.id.others_roof_text)).setTextColor(getResources().getColor(R.color.colortheme));

                break;
            case "Tin shed":
                tine_shedBtn.setBackgroundResource(R.drawable.box_bg);
                ((ImageView) getView().findViewById(R.id.tine_shed_icon)).setBackgroundResource(R.drawable.tin_shed_sel);
                ((TextView) getView().findViewById(R.id.tine_shed_text)).setTextColor(getResources().getColor(R.color.white));


                titledBtn.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.tilted_icon)).setBackgroundResource(R.drawable.tilled);
                ((TextView) getView().findViewById(R.id.titlted_text)).setTextColor(getResources().getColor(R.color.colortheme));


                concreteBtn.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.concrete_icon)).setBackgroundResource(R.drawable.concret_sel);
                ((TextView) getView().findViewById(R.id.concrete_text)).setTextColor(getResources().getColor(R.color.colortheme));

                othersBtn.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.others_roof)).setBackgroundResource(R.drawable.other);
                ((TextView) getView().findViewById(R.id.others_roof_text)).setTextColor(getResources().getColor(R.color.colortheme));
                break;
            case "Others":
                othersBtn.setBackgroundResource(R.drawable.box_bg);
                ((ImageView) getView().findViewById(R.id.others_roof)).setBackgroundResource(R.drawable.other_sel);
                ((TextView) getView().findViewById(R.id.others_roof_text)).setTextColor(getResources().getColor(R.color.white));

                titledBtn.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.tilted_icon)).setBackgroundResource(R.drawable.tilled);
                ((TextView) getView().findViewById(R.id.titlted_text)).setTextColor(getResources().getColor(R.color.colortheme));


                concreteBtn.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.concrete_icon)).setBackgroundResource(R.drawable.concret_sel);
                ((TextView) getView().findViewById(R.id.concrete_text)).setTextColor(getResources().getColor(R.color.colortheme));

                tine_shedBtn.setBackgroundResource(R.drawable.border_bg);
                ((ImageView) getView().findViewById(R.id.tine_shed_icon)).setBackgroundResource(R.drawable.tin_shed);
                ((TextView) getView().findViewById(R.id.tine_shed_text)).setTextColor(getResources().getColor(R.color.colortheme));
                break;


        }
    }

    private void setPhaseButton(boolean isSingle) {

        if (isSingle) {
            threePhaseBtn.setBackgroundResource(R.drawable.border_bg);
            singlePhaseBtn.setTextColor(getResources().getColor(R.color.white));
            threePhaseBtn.setTextColor(getResources().getColor(R.color.colortheme));
            singlePhaseBtn.setBackgroundResource(R.drawable.box_bg);
            phaseType = "Single";
        } else {

            singlePhaseBtn.setBackgroundResource(R.drawable.border_bg);
            threePhaseBtn.setBackgroundResource(R.drawable.box_bg);

            singlePhaseBtn.setTextColor(getResources().getColor(R.color.colortheme));
            threePhaseBtn.setTextColor(getResources().getColor(R.color.white));

            phaseType = "Three";

        }
    }

    private void setSolution(boolean isOnGrid) {
        if (isFrom)
            sol.selectHybrid(!isOnGrid);

        if (isOnGrid) {
            HybridBtn.setBackgroundResource(R.drawable.border_bg);
            ((ImageView) getView().findViewById(R.id.on_grid_icon)).setBackgroundResource(R.drawable.ongrid);
            ((TextView) getView().findViewById(R.id.on_grid_text)).setTextColor(getResources().getColor(R.color.white));

            ((ImageView) getView().findViewById(R.id.hybrid_icon)).setBackgroundResource(R.drawable.hybrid_bg);
            ((TextView) getView().findViewById(R.id.hybrid_text)).setTextColor(getResources().getColor(R.color.colortheme));

            onGridBtn.setBackgroundResource(R.drawable.box_bg);
            solutionsType = "On-Grid";
        } else {

            onGridBtn.setBackgroundResource(R.drawable.border_bg);


            ((ImageView) getView().findViewById(R.id.on_grid_icon)).setBackgroundResource(R.drawable.on_grid_sel);
            ((TextView) getView().findViewById(R.id.on_grid_text)).setTextColor(getResources().getColor(R.color.colortheme));

            ((ImageView) getView().findViewById(R.id.hybrid_icon)).setBackgroundResource(R.drawable.hybrid_sel);
            ((TextView) getView().findViewById(R.id.hybrid_text)).setTextColor(getResources().getColor(R.color.white));

            HybridBtn.setBackgroundResource(R.drawable.box_bg);
            solutionsType = "Hybrid";

        }
    }


    @Override
    public void onClick(View view) {
        if (!(view.getId() == R.id.nextBtn))
            isUpdate = true;


        switch (view.getId()) {
            case R.id.phase_single:
                setPhaseButton(true);
                break;
            case R.id.phase_three:
                setPhaseButton(false);
                break;
            case R.id.commercial:
                setSiteType("Commercial");
                break;
            case R.id.religious:
                setSiteType("Religious");
                break;
            case R.id.resdential_site:
                setSiteType("Residential");
                break;
            case R.id.institutional_site:
                setSiteType("Institutional");
                break;
            case R.id.industrial:
                setSiteType("Industrial");
                break;
            case R.id.otherSite:
                setSiteType("Others");
                break;
            case R.id.on_grid:
                isFrom = true;
                setSolution(true);

                break;
            case R.id.hybrid:
                isFrom = true;
                setSolution(false);

                break;
            case R.id.concrete:
                setRoofStyle("Concrete-Flat");
                break;
            case R.id.others:
                setRoofStyle("Others");
                break;
            case R.id.tine_shed:
                setRoofStyle("Tin shed");
                break;
            case R.id.titled:
                setRoofStyle("Tilted");
                break;
            case R.id.infoIcon:
                mAppDialogs.showInfoDialog("Ok", "On-Grid Solar does not require battery and is appropriate for sites with good grid connection", new AppDialogs.DialogInfo() {
                    @Override
                    public void onPositiveButton(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }

                    @Override
                    public void onNegativeButton(DialogInterface dialog, int which) {

                    }
                });
                break;
            case R.id.line2:
                openPlacePicker();
                break;

            case R.id.nextBtn:
                hideKeyboard(getActivity());

                if (isUpdate) {
                    if (Utils.hasNetwork(getContext()))
                        hitQuotationApi();
                    else
                        showToast("Please check Internet");
                } else {
                    RaiseQuotationActivity.viewPager.setCurrentItem(RaiseQuotationActivity.viewPager.getCurrentItem()+1);
                }
                etSiteSize.clearFocus();

                break;

        }

    }

    private void openPlacePicker() {

        if (!Places.isInitialized()) {
            Places.initialize(getContext(), getString(R.string.google_api));
        }
        List<Place.Field> fields = Arrays.asList(Place.Field.ADDRESS_COMPONENTS, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.ADDRESS_COMPONENTS, Place.Field.LAT_LNG);

        // Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).build(Objects.requireNonNull(getActivity()));
        startActivityForResult(intent, REQUEST_FOR_PLACE);
    }


    @Override
    public void onFetchAccountInfo(CommomResponse response) {

    }

    @Override
    public void onFetchLandingDetails(CommomResponse response) {

    }

    @Override
    public void onSaveFirstQuotation(OportunityResponse response) {
        hideProgress();
        securePrefManager.storeSharedValue(OPTYID, response.optId);
        isUpdate = false;

        //mEventBus.post(new MessageEvent(etSiteSize.getText().toString(),new CommomResponse()));
        RaiseQuotationActivity.viewPager.setCurrentItem(RaiseQuotationActivity.viewPager.getCurrentItem()+1);

    }

    @Override
    public void fetChQuotation(CommomResponse response) {

    }

    @Override
    public void fetchPendingQuote(CommomResponse response) {

    }

    @Override
    public void onGetQuote() {

    }

    @Override
    public void guestUserApi(CommomResponse response) {

    }

    @Override
    public void onFailure(String msg) {
        hideProgress();
        showToast(msg);
    }

    private void hitQuotationApi() {
        if (etSiteSize.getText().length() == 0) {
            showToast("Please enter solar size in (Kw) or Calculate Solar Size by providing Monthly Bill Amount ");
            return;
        } else if (Double.valueOf(etSiteSize.getText().toString()) < 1.0) {
            showToast("Please enter site size greater than 1.0");
            return;
        } else if (!isForNewUser&&(!addressToggle.isChecked() && (etLine1.getText().length() == 0 || pincode.getText().length() == 0 || city.getText().length() == 0))) {
            showToast("Please Fill * marked Field");
            return;
        }

        userState=state.getText().toString();

        showProgress("Please wait...");
        GetManuFacturerDataApi api = new GetManuFacturerDataApi() {
            @Override
            public void onComplete(CommomResponse response) {
                mEventBus.post(new MessageEvent(etSiteSize.getText().toString(),response));
                QuotationModel model = new QuotationModel();
                model.phase = phaseType;
                model.roofStyle = roofType;
                model.siteType = sitetype;
                model.solarSize = Double.valueOf(etSiteSize.getText().toString());
                model.solution = solutionsType;
                model.userId=securePrefManager.getSharedValue(USER_ID);
                if (isForNewUser){
                    com.solarginni.SiteLovsModule.SiteDetailModel.Address address= (com.solarginni.SiteLovsModule.SiteDetailModel.Address) getArguments().getSerializable("address");
                    model.isAddresSame = false;
                    model.setAddress(address);
                }
                else{
                    if (addressToggle.isChecked()) {
                        model.isAddresSame = true;
                    } else {
                        com.solarginni.SiteLovsModule.SiteDetailModel.Address address = new com.solarginni.SiteLovsModule.SiteDetailModel.Address();
                        address.setAddressLine1(etLine1.getText().toString());
                        address.setAddressLine2(addLine2.getText().toString());
                        address.setCity(city.getText().toString());
                        address.setCountry(country.getText().toString());
                        address.setState(state.getText().toString());
                        address.setPinCode(pincode.getText().toString());
                        address.setLatitude(lat);
                        address.setLongitude(lng);
                        model.isAddresSame = false;
                        model.setAddress(address);
                    }
                }


                presenter.saveFirstQuotattion(securePrefManager.getSharedValue(TOKEN), model, String.valueOf(RaiseQuotationActivity.viewPager.getCurrentItem() + 1), securePrefManager.getSharedValue(OPTYID));



            }

            @Override
            protected void onFailur(String msg) {
                hideProgress();

                showToast(msg);
            }
        };

        api.hit(securePrefManager.getSharedValue(TOKEN),isForNewUser?false:addressToggle.isChecked(),userState);



    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_FOR_PLACE:
                if (resultCode == RESULT_OK) {
                    hideKeyboard(getActivity());
                    Place place = Autocomplete.getPlaceFromIntent(data);
                    addLine2.setText(place.getName());
                    LatLng latLng = place.getLatLng();
                    lat = latLng.latitude;
                    lng = latLng.longitude;
                    try {
                        getCityNameByCoordinates(latLng.latitude, latLng.longitude);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }


                    //  onParamSelected();
                } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                    Status status = Autocomplete.getStatusFromIntent(data);


                } else if (resultCode == RESULT_CANCELED) {

                }
                break;

        }
    }

    private void getCityNameByCoordinates(double lat, double lon) throws IOException {
        mGeocoder = new Geocoder(getContext(), Locale.getDefault());
        List<Address> addresses = mGeocoder.getFromLocation(lat, lon, 1);
        if (addresses != null && addresses.size() > 0) {
            state.setText(addresses.get(0).getAdminArea());
            addressToggle.setChecked(false);
//            if (addresses.get(0).getSubLocality() != null)
//                addLine2.setText(addLine2.getText() + " " + addresses.get(0).getSubLocality());
            city.setText(addresses.get(0).getSubAdminArea());
            country.setText(addresses.get(0).getCountryName());
            pincode.setText(addresses.get(0).getPostalCode());

        }

    }


    public interface HybridSol {
        void selectHybrid(boolean isHybrid);
    }



    private String calculatedSolarSite(double billAmount){

        double siteSize=0;

        switch (sitetype){
            case "Residential":
            case "Institutional":

                siteSize=Math.ceil(billAmount/residentialUnitPrice);

                break;
            default:
                siteSize=Math.ceil(billAmount/nonResdenitalUnitPrice);
                break;

        }


        return String.valueOf(siteSize);
    }


//    @Override
//    public void onConfigurationChanged(Configuration newConfig) {
//        super.onConfigurationChanged(newConfig);
//
//
//        // Checks whether a hardware keyboard is available
//        if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO) {
//            showToast("keyboard visible");
//        } else if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_YES) {
//            showToast("keyboard hidden");
//        }
//    }

//    private void registerEventBus() {
//        if (!mEventBus.isRegistered(RaiseQuoteFragment1.this))
//            mEventBus.register(this);
//    }
}
