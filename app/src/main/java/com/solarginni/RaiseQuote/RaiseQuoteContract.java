package com.solarginni.RaiseQuote;

import android.content.Context;

import com.solarginni.DBModel.DemandQuoteModel;
import com.solarginni.ProspectUser.QutationModel;

import java.util.List;

public interface RaiseQuoteContract {
    interface View {
        void onApplyFilter(List<QutationModel.Filter> list);

        void onGetQuote();

        void failure(String msg);
    }

    interface Presenter {
        void getQuote(Context context,String token, DemandQuoteModel model,String optId);

        void sortListByValue(Context context, String value);

        void applyFilter(Context context,String token,String price,String distance,String siteCount,String optId,boolean isEmpaneled,int sorting);


    }

    interface Intreactor {
        void getQuote(Context context,String token, DemandQuoteModel model,String optId, OnInteraction interaction);

        void sortListByValue(Context context, String value,OnInteraction interaction);


        void applyFilter(Context context,String token,String price,String distance,String siteCount,String optId,boolean isEmpaneled,int sorting, OnInteraction interaction);

    }

    interface OnInteraction {
        void onGetQuote();

        void failure(String msg);

        void onApplyFilter(List <QutationModel.Filter> list);
    }
}
