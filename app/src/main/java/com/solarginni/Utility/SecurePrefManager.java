package com.solarginni.Utility;

import android.content.Context;

import com.google.gson.Gson;

import java.util.List;

/**
 * Created by signity.
 */


public class SecurePrefManager {

    Context mContext;
    ObscuredSharedPreferences sharedPref;
    public static final String APP_PREFERENCES = "APP_PREFERENCES";

    public SecurePrefManager(Context context) {
        this.mContext = context;
        sharedPref = new ObscuredSharedPreferences(mContext, mContext.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE));

    }

    public void storeSharedValue(String key, String value) {
        ObscuredSharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.commit();
    }


    public <T> void setList(String key, List<T> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);

        set(key, json);
    }

    public void set(String key, String value) {

        ObscuredSharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.commit();
    }


    public <T> void getList(String key, List<T> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);

        set(key, json);
    }

    public void clearSharedPrefsValue(String key) {
        ObscuredSharedPreferences.Editor editor = sharedPref.edit();
        editor.remove(key);
        editor.commit();

    }

    public String getSharedValue(String key) {
        if (key.equalsIgnoreCase("notificationCount")){
            return sharedPref.getString(key, "0");
        }
        else
        return sharedPref.getString(key, "");
    }

    public String getScreenValue() {
        return sharedPref.getString("SCREEN", "Main");
    }

    public void setScreenValue(String value) {
        ObscuredSharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("SCREEN", value);
        editor.commit();
    }

    public boolean getBooleanKeyValue(String key) {
        return sharedPref.getBoolean(key, false);
    }

    public void storeBooleanValue(String key, boolean value) {
        ObscuredSharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public void clearAppsAllPrefs() {
        String fcmtoken = getSharedValue("FCM_TOKEN");
        ObscuredSharedPreferences.Editor editor = sharedPref.edit();
        editor.clear();
        editor.putString("FCM_TOKEN", fcmtoken);
        editor.putBoolean("isVisited", true);
        editor.commit();
    }


}
