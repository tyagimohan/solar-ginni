package com.solarginni.Utility;

public class AppConstants {
    public static final String TOKEN= "token";
    public static final String ROLE ="role" ;
    public static final String MOBILE = "mobile";
    public static final String DETAILS ="details" ;
    public static final String IMPLEMENTER_DETAILS ="implementer_details" ;
    public static final String ISAPPVISITED = "isAppVisited";
    public static final String SITELOVS = "siteDetails";


    public static final String dynamicUrl = "https://solarginie.page.link/u9DC";
    public static final String CONTACTNUMBER = "+917743009001";
    public static final String CONTACTEMAIL = "service@solarginie.com";


    /*ROLEs*/
    public static final String CUSTOMER_ROLE_ID = "7";
    public static final String GUEST_ROLE = "15";
    public static final String IMPLEMENTER_ROLE_ID = "1";
    public static final String SUPER_IMPLEMENTER_ROLE_ID = "9";
    public static final String IMPLEMENTER_ENGINEER_ROLE_ID = "1";
    public static final String FREELANCER_ROLE_ID = "1";
    public static final String REGULATOR_ROLE_ID = "1";
    public static final String REGULATOR_ENGINNER_ROLE_ID = "1";
    public static final String SG_ADMIN = "10";
    public static final String BO_MANAGER = "11";
    public static final String PROSPECT_ROLE_ID = "3";
    public static final String USER_ID = "userId";
    public static final String NAME = "name";
    public static final String PROFILE_IMAGE = "profle_image";
    public static final String ACCOUNT_MODEL = "account_model";
    public static final String SITEID = "siteId";
    public static final String SITEDETAILSMODEL = "siteDetailsModel";
    public static final String FCM_TOKEN ="FCM_TOKEN" ;
    public static final String REQUEST_DATA = "request_data";
    public static final String IMPLEMENTER_ACCOUNT ="implemenete_account" ;
    public static final String COMPANYNAME = "companyName";
    public static final String COMPANYID ="companyID" ;
    public static final String PROSPECT_LANDIND ="prospectLanding" ;
    public static final String NOTIFICATION_COUNT = "notificationCount";


    public static final String VIKRAM="SGPA1001";
    public static final String ADANI="SGPA1002";
    public static final String SATVIk ="SGPA1015";
    public static final String ANYPANEL="SG000";

    public static final String ABB="SGIN1006";
    public static final String GROWATT="SGIN1003";
    public static final String KACO="SGIN1004";
    public static final String ANYINVERTER="SG000";


    public static final String EXIDE="SGBA1001";
    public static final String ANYBATTERY="SG000";
    public static final String OKAYA="SGBA1002";
    public static final String LUMINIOUS="SGBA1005";


    public static final String OPTYID = "optyID";
    public static final String PENDING_QUOTE = "pendingQuote";
    public static final String IMPLEMENTER_QUOTE_LIST ="implementerQuoteList" ;
    public static final String REGISTERED = "registered";
    public static final String ALL = "12";
    public static final String ISVISITED = "isVisited";
    public static final String MANUFACTURER_MODEL = "ManuFacturerModel";
    public static final  String COMPONENT_MODEL="componentModel";
    public static final String SORTING_VALUE = "sorting_value";
}

