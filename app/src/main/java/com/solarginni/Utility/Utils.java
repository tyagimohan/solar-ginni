package com.solarginni.Utility;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

    private static Gson gson = new Gson();

    public static boolean hasNetwork(Context context) {

        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        try {
            ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo info = connectivity.getActiveNetworkInfo();

                if (info.getTypeName().equalsIgnoreCase("WIFI")) if (info.isConnected()) {
                    haveConnectedWifi = true;
                }
                if (info.getTypeName().equalsIgnoreCase("MOBILE")) if (info.isAvailable()) {
                    final android.net.NetworkInfo mobile = connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
                    if (mobile.isConnected()) {
                        haveConnectedMobile = true;
                    }
                }
            }
            return haveConnectedWifi || haveConnectedMobile;
        } catch (Exception e) {
        }
        return false;
    }

    public static boolean isValidEmail(String emailtxt) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(emailtxt);

        return matcher.matches();
    }

    public static boolean isMarshMallow() {
        return Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1;
    }

    public static <T> String toString(T data) {
        //        Gson gson = new Gson();
        return gson.toJson(data);
    }

    public static <T> T getObj(String data, Type dataType) {
        return gson.fromJson(data, dataType);
    }

    public static int dp2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }


    public static Bitmap compressImage(String filePath) {
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();
        //by setting this field as true, the actual bitmap pixels are not loaded in the memory.
        // Just the bounds are loaded.
        // If you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);
        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;
        //max Height and width values of the compressed image is taken as 816x612
//        float maxHeight = maxHt;
//        float maxWidth = maxWt;
        float maxWidth = 480.0f;
        float maxHeight = 800.0f;
        if (actualWidth > 0 && actualWidth < 1200) {
            maxWidth = 480.0f;
            maxHeight = 800.0f;
        }else if (actualWidth>=1200&&actualWidth<2500){
            maxWidth = 720.0f;
            maxHeight = 1280.0f;
        }else if (actualWidth>=2500){
            maxWidth = 1080.0f;
            maxHeight = 1920.0f;
        }
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;
        //width and height values are set maintaining the aspect ratio of the image
        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;
            }
        }

        /*https://stackoverflow.com/questions/28424942/decrease-image-size-without-losing-its-quality-in-android*/


        //setting inSampleSize value allows to load a scaled down version of the original image
        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        //inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;
        //this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            //load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;
        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);
        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        //check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

        } catch (IOException e) {
            e.printStackTrace();
        }

//        FileOutputStream out = null;
//        String filename = getFilename();
//        try {
//            out = new FileOutputStream(filename);
//            //write the compressed bitmap at the destination specified by filename.
//            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
        return scaledBitmap;
//        return "";
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }
        return inSampleSize;
    }

    public static String convertMsgDate(String inputDate) {
        try {
            inputDate = inputDate.replace("T"," " ).replace("Z","" );
            DateFormat inFormater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            DateFormat outFormatter = new SimpleDateFormat("dd/MM/yyyy HH:mm ");
            Date date = null;
            try {
                date = inFormater.parse(inputDate);
            } catch (Exception parseException) {
                // Date is invalid. Do what you want.
            }
            return outFormatter.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return inputDate;
        }
    }

    public static String convertDate(String inputDate) {
        try {
            DateFormat inFormater = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat outFormatter = new SimpleDateFormat("dd/MM/yyyy");
            Date date = null;
            try {
                date = inFormater.parse(inputDate);
            } catch (Exception parseException) {
                // Date is invalid. Do what you want.
            }
            return outFormatter.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return inputDate;
        }
    }

    public static String convertUTCtoMyTime(String inputDate) {
        inputDate = inputDate.replace("T"," " ).replace("Z","" );
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        SimpleDateFormat dfNew = new SimpleDateFormat("dd/MM/yyyy HH:mm ");

        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        String formattedDate = "";
        try {
            Date date = df.parse(inputDate);
            df.setTimeZone(TimeZone.getDefault());
            formattedDate = dfNew.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return formattedDate;

    }

    public static String convertDateNTime(String inputDate) {
        try {
            DateFormat inFormater = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat outFormatter = new SimpleDateFormat("dd/MM/yyyy");
            Date date = null;
            try {
                date = inFormater.parse(inputDate);
            } catch (Exception parseException) {
                // Date is invalid. Do what you want.
            }
            return outFormatter.format(date);
        } catch (Exception e) {
            e.printStackTrace();
            return inputDate;
        }
    }

    public static int dpToPx(final Context context, final int dp) {
        int px = Math.round(dp * getPixelScaleFactor(context));
        return px;
    }

    private static float getPixelScaleFactor(final Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT;
    }
}
