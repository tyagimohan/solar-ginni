package com.solarginni.Utility;

import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;

import java.text.DecimalFormat;

public class MyValueFornatter extends ValueFormatter implements IValueFormatter {

    DecimalFormat mFormat;

    public MyValueFornatter() {
        mFormat = new DecimalFormat("####0");

    }


    @Override
    public String getFormattedValue(float value) {
        return mFormat.format(value) ;
    }


}
