package com.solarginni.Utility;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import androidx.core.app.ActivityCompat;

import java.util.ArrayList;
import java.util.List;

public class Permissions {

    public List<String> permissions;
    public static final int READ_WRITE = 101;
    public static final int CALENDAR_PERMISSION = 102;
    public static final int CALL_PERMISSION = 103;
    public static final int CAMERA = 104;
    public static final int READ_PHONE_STATE = 105;

    private Permissions() {
        permissions = new ArrayList<>();
    }

    public Permissions addPermission(List<String> permissions) {
        this.permissions.addAll(permissions);
        return this;
    }

    public static Permissions getInstance() {
        return new Permissions();
    }

    public Permissions hasStorageReadPermission(Activity activity) {
        if (ActivityCompat.checkSelfPermission(activity,Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        return this;
    }

    public Permissions hasStorageWritePermission(Activity activity) {
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        return this;
    }

    public Permissions checkCallPermission(Activity mActivity) {
        if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.CALL_PHONE);
        }
        return this;
    }

    public boolean hasCallPermission(Activity mActivity) {
        return ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED;
    }

    public void askForPermissions(Activity activity, int requestcode) {
        if (permissions.size() > 0 && Utils.isMarshMallow()) {
            ActivityCompat.requestPermissions(activity, permissions.toArray(new String[permissions.size()]), requestcode);
        }
    }

    public Permissions checkReadWritePermissions(Activity activity) {
        hasStorageReadPermission(activity);
        hasStorageWritePermission(activity);
        return this;
    }


    public Permissions hasCameraPermission(Context context) {

        if (!hasPermission(context, Manifest.permission.CAMERA)) {
            permissions.add(Manifest.permission.CAMERA);
        }

        return this;
    }

    public boolean hasPermission(Context context, String permission) {
        return ActivityCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
    }

//    public Permissions hasTelephonyPermission(Context context) {
//        if (!hasPermission(context, Manifest.permission.READ_PHONE_STATE)) {
//            permissions.add(Constants.Permissions.READ_PHONE_STATE);
//        }
//        return this;
//    } public Permissions hasCallPermisiion(Context context) {
//        if (!hasPermission(context, Manifest.permission.CALL_PHONE)) {
//            permissions.add(Constants.Permissions.CALL);
//        }
//        return this;
//    }

//    public Permissions hasSendMsgPermisiion(Context context) {
//        if (!hasPermission(context, Manifest.permission.SEND_SMS)) {
//            permissions.add(Constants.Permissions.SEND_MESSAGE);
//        }
//        return this;
//    }

}
