package com.solarginni.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.solarginni.CommonModule.SplashScreen;
import com.solarginni.R;
import com.solarginni.Utility.AppConstants;
import com.solarginni.Utility.SecurePrefManager;

import java.util.Random;

/**
 * Developer:
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getName();
    private static final int TAG2 = 1000;
    private static final String DEFAULT_CHANNEL_ID = "default";
    private static final String ADVERTISEMENT_CHANNEL_ID = "advertisement";

    private static final long[] NOTIFICATION_VIBRATION_PATTERN = new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400};
    private static NotificationManager mNotificationManager;

    /**
     * Clear notifications
     */
    public static void clearNotification() {
        if (mNotificationManager != null) {
            mNotificationManager.cancelAll();
        }
    }

    @Override
    public void onMessageReceived(final RemoteMessage remoteMessage) {


        /*
         * Foreground.get(getApplication()).isForeground() checks if the app is in foreground i.e visible not minimized or killed
         * if it is killed or minimized show notification
         */
//        if (Foreground.get(getApplication()).isForeground()) {
//            Intent mIntent = new Intent(AppConstant.NOTIFICATION_RECEIVED);
//            Bundle mBundle = new Bundle();
//            for (String key : remoteMessage.getData().keySet()) {
//                mBundle.putString(key, remoteMessage.getData().get(key));
//            }
//            mIntent.putExtras(mBundle);
//            LocalBroadcastManager.getInstance(this).sendBroadcast(mIntent);
//        } else {
//            CommonData.setPushData(remoteMessage.getData());
        showNotification(remoteMessage.getNotification());
        //}


    }


    @Override
    public void onNewToken(@NonNull String s) {
        String token = FirebaseInstanceId.getInstance().getToken();
        SecurePrefManager securePrefManager = new SecurePrefManager(this);

        securePrefManager.storeSharedValue(AppConstants.FCM_TOKEN, token);
        Log.d(TAG, token);
        super.onNewToken(s);
    }

    /**
     * Show notification
     *
     * @param data notification data map
     */
    public void showNotification(final RemoteMessage.Notification data) {
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        final Intent notificationIntent = new Intent(getApplicationContext(), SplashScreen.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

//        // handle advertisement notification, which will be shown on first activity to be opened except the splash activity
//        if (data.get(AppConstant.TYPE).equals(AppConstant.KEY_ADVERTISEMENT)) {
//
//            notificationIntent.putExtra(KEY_IS_FROM_ADVERTISEMENT, true);
//            notificationIntent.putExtra(KEY_ADVERTISEMENT_URL, data.get(AppConstant.KEY_IMAGE_URL));
//        }
//        if (data.get(AppConstant.TYPE).equals(ApiConstant.CAMPAIGN)){
//            notificationIntent.putExtra(KEY_IS_FOR_CAMPAIGN, true);
//            notificationIntent.putExtra(KEY_INFO, data.get("info"));
//            notificationIntent.putExtra(MESSAGE, data.get("messsage"));
//        }

        PendingIntent pi = PendingIntent.getActivity(this, getRandomNumber(1, 100), notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            int importance = NotificationManager.IMPORTANCE_HIGH;


            // Default channel
            NotificationChannel mChannel = new NotificationChannel(DEFAULT_CHANNEL_ID, "Test",
                    importance);
            // Configure the notification channel.
            mChannel.setDescription("Test");
            mChannel.enableLights(true);
            // Sets the notification light color for notifications posted to this
            // channel, if the device supports this feature.
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(NOTIFICATION_VIBRATION_PATTERN);
            mNotificationManager.createNotificationChannel(mChannel);

            // Advertisement channel
            NotificationChannel mChannelAdvertisement = new NotificationChannel(ADVERTISEMENT_CHANNEL_ID,
                    "Test", importance);
            // Configure the notification channel.
            mChannelAdvertisement.setDescription("Test");
            mChannelAdvertisement.enableLights(true);
            // Sets the notification light color for notifications posted to this
            // channel, if the device supports this feature.
            mChannelAdvertisement.setLightColor(Color.RED);
            mChannelAdvertisement.enableVibration(true);
            mChannelAdvertisement.setVibrationPattern(NOTIFICATION_VIBRATION_PATTERN);
            mNotificationManager.createNotificationChannel(mChannelAdvertisement);
        }

        // data to be displayed in notifications
        String title = data.getTitle();
        String body = data.getBody();


        NotificationCompat.Builder mNotificationBuilder = new NotificationCompat.Builder(this, DEFAULT_CHANNEL_ID)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(body))
                .setSmallIcon(R.drawable.icon)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                .setContentTitle(title)
                .setContentText(body)
                .setContentIntent(pi)
                .setDefaults(Notification.DEFAULT_ALL)
                .setPriority(Notification.PRIORITY_MAX)
                .setAutoCancel(true);
//
//        if (data.get(AppConstant.TYPE).equals(AppConstant.KEY_ADVERTISEMENT)) {
//            mNotificationBuilder.setChannelId(ADVERTISEMENT_CHANNEL_ID);
//        } else {
         mNotificationBuilder.setChannelId(DEFAULT_CHANNEL_ID);
        //    }

            mNotificationManager.notify((int) System.currentTimeMillis(), mNotificationBuilder.build());
    }

    public int getRandomNumber(int min, int max) {
        // min (inclusive) and max (exclusive)
        Random r = new Random();
        return r.nextInt(max - min) + min;
    }
}