package com.solarginni.CustomizeView;

import java.io.Serializable;

public class ProposalStateModel  implements Serializable {

    public String state;
    public String  key;
    public boolean isSelected;

    public ProposalStateModel(String state, String key, boolean isSelected) {
        this.state = state;
        this.key = key;
        this.isSelected = isSelected;
    }
}
