package com.solarginni.CustomizeView;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class CustomPager extends ViewPager  {
    public  Boolean disable= true;
    private Boolean mAnimStarted = false;
    private View child;

    public CustomPager(@NonNull Context context) {
        super(context);

    }

    public CustomPager(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

    }



    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return !disable && super.onInterceptTouchEvent(event);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return !disable && super.onTouchEvent(event);
    }

    public void disableScroll(Boolean disable){
        //When disable = true not work the scroll and when disble = false work the scroll
        this.disable = disable;
    }

    public Boolean getPagerScroll(){
        return disable;
    }
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
//    @Override
//    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//
//        if(!mAnimStarted && null != getAdapter()) {
//            int height = 0;
////            View child = ((FragmentStatePagerAdapter) getAdapter()).getItem(getCurrentItem()).getView();
//            if (child != null) {
//                child.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
//                height = child.getMeasuredHeight();
//                if ( height < getMinimumHeight()) {
//                    height = getMinimumHeight();
//                }
//            }
//
//            // Not the best place to put this animation, but it works pretty good.
//            int newHeight = MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY);
//            if (getLayoutParams().height != 0 && heightMeasureSpec != newHeight) {
//                final int targetHeight = height;
//                final int currentHeight = getLayoutParams().height;
//                final int heightChange = targetHeight - currentHeight;
//
//                Animation a = new Animation() {
//                    @Override
//                    protected void applyTransformation(float interpolatedTime, Transformation t) {
//                        if (interpolatedTime >= 1) {
//                            getLayoutParams().height = targetHeight;
//                        } else {
//                            int stepHeight = (int) (heightChange * interpolatedTime);
//                            getLayoutParams().height = currentHeight + stepHeight;
//                        }
//                        requestLayout();
//                    }
//
//                    @Override
//                    public boolean willChangeBounds() {
//                        return true;
//                    }
//                };
//
//                a.setAnimationListener(new Animation.AnimationListener() {
//                    @Override
//                    public void onAnimationStart(Animation animation) {
//                        mAnimStarted = true;
//                    }
//
//                    @Override
//                    public void onAnimationEnd(Animation animation) {
//                        mAnimStarted = false;
//                    }
//
//                    @Override
//                    public void onAnimationRepeat(Animation animation) {
//                    }
//                });
//
//                a.setDuration(1000);
//                startAnimation(a);
//                mAnimStarted = true;
//            } else {
//                heightMeasureSpec = newHeight;
//            }
//        }
//
//        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
//    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        View child = getChildAt(getCurrentItem());
        if (child != null) {
            child.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
            int h = child.getMeasuredHeight();
            heightMeasureSpec = MeasureSpec.makeMeasureSpec(h, MeasureSpec.EXACTLY);
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public void measureCurrentView(View currentView) {
        addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                child = currentView;

                requestLayout();
            }
        });

    }

}
