package com.solarginni.CustomizeView;

/**
 * Created by pramod on 20/01/18.
 */

/**
 * The interface Custom dialog interface.
 */
public interface CustomDialogInterface {
    /**
     * The interface On click listener.
     */
    public interface OnClickListener {
        /**
         * On click.
         */
        void onClick();
    }

    /**
     * The interface On dismiss listener.
     */
    public interface OnDismissListener {
        /**
         * On dismiss.
         */
        void onDismiss();
    }

    /**
     * The interface On cancel listener.
     */
    public interface OnCancelListener {
        /**
         * On cancel.
         */
        void onCancel();
    }
}