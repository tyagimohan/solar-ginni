package com.solarginni.CustomizeView;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;

import com.solarginni.DBModel.CompanyMakeModel;
import com.solarginni.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public final class CompanyListDialog {


    /**
     * Instantiates a new Service Dialog.
     */
    private CompanyListDialog() {

    }

    /**
     * Show dialog.
     *
     * @param mContext      the m context
     * @param mServiceArray array
     * @param listener      the listener
     * @return the dialog
     */
    public static Dialog show(final Context mContext, final List<CompanyMakeModel> mServiceArray, final SelectServiceListener listener) {
        //set service  dialog

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.service_dialog, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);
        AlertDialog dialog = dialogBuilder.create();
//        final Dialog dialog = new Dialog(mContext, R.style.AppTheme);
//        dialog.setContentView(R.layout.service_dialog);
//        Window dialogWindow = dialog.getWindow();
//        if (dialogWindow != null) {
//            WindowManager.LayoutParams lp = dialogWindow.getAttributes();
//            dialogWindow.setAttributes(lp);
//        }

        //Initialization of views
        final AppCompatImageView ivClear = dialogView.findViewById(R.id.ivClear);
        dialogView.findViewById(R.id.allLay).setVisibility(View.GONE);
        // final AppCompatImageView ivBack = dialogView.findViewById(R.id.ivBack);
        final AppCompatEditText searchEditText = (AppCompatEditText) dialogView.findViewById(R.id.country_code_picker_search);
        final RecyclerView countryRecyclerView = (RecyclerView) dialogView.findViewById(R.id.country_code_picker_recyclerview);
        countryRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        countryRecyclerView.setItemAnimator(new DefaultItemAnimator());
        countryRecyclerView.setHasFixedSize(true);

//        //set view click listeners
//        ivBack.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(final View view) {
//                dialog.dismiss();
//            }
//        });
        ivClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                searchEditText.setText("");
            }
        });

        final CompanyListAdapter adapter = new CompanyListAdapter(mContext);
        adapter.addAll(mServiceArray);
        countryRecyclerView.setAdapter(adapter);
        adapter.setOnClickListener((data, position) -> {
            listener.onSelectService((CompanyMakeModel) data);
            dialog.dismiss();
        });


        searchEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
            }

            @Override
            public void beforeTextChanged(final CharSequence s, final int start, final int count, final int after) {
            }

            @Override
            public void afterTextChanged(final Editable s) {
                //show clear button if edittext is not empty
                ivClear.setVisibility(s.toString().isEmpty() ? View.GONE : View.VISIBLE);
                ArrayList<CompanyMakeModel> mServiceList = new ArrayList<>();
                for (CompanyMakeModel service : mServiceArray) {

                    if (service.getCompanyName().toLowerCase(Locale.ENGLISH).contains(s.toString().trim().toLowerCase())) {
                        mServiceList.add(service);
                    }


                }
                adapter.notifyItemRangeRemoved(0, adapter.getItemCount());
                adapter.addAll(mServiceList);
                adapter.notifyDataSetChanged();
            }
        });

        dialog.show();
        return dialog;

    }

    /**
     * The interface On item select listener.
     */
    interface OnItemSelectListener {
        /**
         * On item select.
         *
         * @param mServices the m services
         */
        void onItemSelect(String[] mServices);
    }

    public static interface SelectServiceListener {

        void onSelectService(CompanyMakeModel item);
    }
}
