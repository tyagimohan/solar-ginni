package com.solarginni.CustomizeView;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.DBModel.CompanyMakeModel;
import com.solarginni.R;

public class CompanyListAdapter extends BaseRecyclerAdapter<CompanyMakeModel, CompanyListAdapter.Holder> {


    public CompanyListAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public int getLayout(int viewType) {
        return R.layout.service_make_iten;
    }

    @Override
    public Holder getViewHolder(View view, int viewType) {
        return new Holder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {

        holder.companyName.setText(getItem(i).getCompanyName());


    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView companyName;

        public Holder(@NonNull View itemView) {
            super(itemView);
            companyName = itemView.findViewById(R.id.companyName);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            getListener().onClick(getItem(getAdapterPosition()), getAdapterPosition());

        }
    }

}
