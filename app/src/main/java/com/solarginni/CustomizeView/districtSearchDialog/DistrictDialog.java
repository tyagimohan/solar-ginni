package com.solarginni.CustomizeView.districtSearchDialog;

import android.app.Dialog;
import android.content.Context;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.solarginni.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public final class DistrictDialog {


    /**
     * Instantiates a new Service Dialog.
     */
    private DistrictDialog() {

    }

    /**
     * Show dialog.
     *
     * @param mContext      the m context
     * @param mServiceArray array
     * @param listener      the listener
     * @return the dialog
     */
    public static Dialog show(final Context mContext, final List<DistrictModel> mServiceArray,boolean showAll, final SelectDistrictListener listener) {
        //set service  dialog
        final Dialog dialog = new Dialog(mContext, R.style.AppTheme);
        dialog.setContentView(R.layout.service_dialog);
        Window dialogWindow = dialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams lp = dialogWindow.getAttributes();
            dialogWindow.setGravity(Gravity.TOP);
            dialogWindow.setAttributes(lp);
        }

        //Initialization of views
        final AppCompatImageView ivClear = dialog.findViewById(R.id.ivClear);
        final LinearLayoutCompat allLay = dialog.findViewById(R.id.allLay);
        if (showAll){
            allLay.setVisibility(View.VISIBLE);
        }else {
            allLay.setVisibility(View.GONE);

        }
        final AppCompatButton btnPositive = dialog.findViewById(R.id.btnPositive);
        final CheckBox checkBox = dialog.findViewById(R.id.checkBox);
        final AppCompatButton btnNegative = dialog.findViewById(R.id.btnNegative);
        final LinearLayout buttonlayout = dialog.findViewById(R.id.buttonlayout);
        buttonlayout.setVisibility(View.VISIBLE);
        final AppCompatEditText searchEditText = (AppCompatEditText) dialog.findViewById(R.id.country_code_picker_search);
        final RecyclerView countryRecyclerView = (RecyclerView) dialog.findViewById(R.id.country_code_picker_recyclerview);
        countryRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        countryRecyclerView.setItemAnimator(new DefaultItemAnimator());
        countryRecyclerView.setHasFixedSize(true);


        ivClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                searchEditText.setText("");
            }
        });




        btnNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                ArrayList<DistrictModel> selected = new ArrayList<>();
                if (mServiceArray != null) {
                    for (DistrictModel district : mServiceArray) {
                        district.isSelected = false;
                        selected.add(district);
                    }
                }
                listener.onUnSelectDistrict(selected);
                dialog.dismiss();
            }
        });
        final DistrictListAdapter adapter = new DistrictListAdapter(mContext, mServiceArray,
                new OnItemSelectListener() {
                    @Override
                    public void onItemSelect(final String[] mServices) {
                        dialog.dismiss();
                    }
                });

        countryRecyclerView.setAdapter(adapter);
        checkBox.setClickable(false);
        allLay.setOnClickListener(v -> {

            if (!checkBox.isChecked()){
                for (DistrictModel model:mServiceArray) {
                    model.isSelected=true;
                    adapter.notifyDataSetChanged();
                }
                checkBox.setChecked(true);
            }
            else {
                for (DistrictModel model:mServiceArray) {
                    model.isSelected=false;
                    adapter.notifyDataSetChanged();

                }
                checkBox.setChecked(false);
            }


        });
        btnPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                ArrayList<DistrictModel> selected = new ArrayList<>();
                if (mServiceArray != null) {
                    for (DistrictModel service : mServiceArray) {
                        if (service.isSelected) {
                            selected.add(service);
                        }
                    }
                }
                if (selected.size()!=0){
                    listener.onSelectDistrict(selected);
                    dialog.dismiss();
                }
                else {
                    Toast.makeText(mContext, "Please select district", Toast.LENGTH_SHORT).show();
                }

            }
        });


        searchEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
            }

            @Override
            public void beforeTextChanged(final CharSequence s, final int start, final int count, final int after) {
            }

            @Override
            public void afterTextChanged(final Editable s) {
                //show clear button if edittext is not empty
                ivClear.setVisibility(s.toString().isEmpty() ? View.GONE : View.VISIBLE);
                ArrayList<DistrictModel> mServiceList = new ArrayList<>();
                for (DistrictModel service : mServiceArray) {
                    if (service.district.toLowerCase(Locale.ENGLISH).contains(s.toString().trim().toLowerCase())) {
                        mServiceList.add(service);
                    }

                }

                adapter.setData(mServiceList);
            }
        });

        dialog.show();
        return dialog;

    }

    /**
     * The interface On item select listener.
     */
    interface OnItemSelectListener {
        /**
         * On item select.
         *
         * @param mServices the m services
         */
        void onItemSelect(String[] mServices);
    }
}
