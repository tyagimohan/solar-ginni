package com.solarginni.CustomizeView.districtSearchDialog;

import java.io.Serializable;

public class DistrictModel implements Serializable {

    public DistrictModel(String district, boolean isSelected) {
        this.district = district;
        this.isSelected = isSelected;
    }

    public String district;

    public boolean isSelected = false;


}
