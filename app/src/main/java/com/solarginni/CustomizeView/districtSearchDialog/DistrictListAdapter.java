package com.solarginni.CustomizeView.districtSearchDialog;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.solarginni.CustomizeView.ProposalDiaLog.ProposalStateDialog;
import com.solarginni.CustomizeView.ProposalStateModel;
import com.solarginni.R;

import java.util.ArrayList;
import java.util.List;



public class DistrictListAdapter extends RecyclerView.Adapter {

    private static final int VIEW_TYPE_EMPTY_LIST_PLACEHOLDER = 0;
    private static final int VIEW_TYPE_OBJECT_VIEW = 1;
    private Context mContext;
    private List<DistrictModel> services;
    private DistrictDialog.OnItemSelectListener listener;
    private ArrayList<DistrictModel> selServices = new ArrayList<>();


    /**
     * Instantiates a new Country list adapter.
     *
     * @param context   the context
     * @param mServices the countries
     * @param listener  the listener
     */
    public DistrictListAdapter(final Context context, final List<DistrictModel> mServices, final DistrictDialog.OnItemSelectListener listener) {
        this.mContext = context;
        this.services = mServices;
        this.listener = listener;
    }
    @Override
    public int getItemViewType(final int position) {
        if (services == null || services.isEmpty()) {
            return VIEW_TYPE_EMPTY_LIST_PLACEHOLDER;
        } else {
            return VIEW_TYPE_OBJECT_VIEW;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        final RecyclerView.ViewHolder vh;
        if (viewType == VIEW_TYPE_EMPTY_LIST_PLACEHOLDER) {
            final View v = LayoutInflater.from(mContext).inflate(R.layout.item_view_placeholder, parent, false);
            vh = new EmptyViewHolder(v);
        } else {
            final View v = LayoutInflater.from(mContext).inflate(R.layout.service_row, parent, false);
            vh = new CountryViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof CountryViewHolder) {
            final DistrictModel mServices = services.get(holder.getAdapterPosition());
            final CountryViewHolder mCountryViewHolder = (CountryViewHolder) holder;
            mCountryViewHolder.textView.setText(mServices.district);
            if (mServices.isSelected) {
                mCountryViewHolder.ivSelected.setBackgroundResource(R.drawable.ic_checkbox_check);
            } else {
                mCountryViewHolder.ivSelected.setBackgroundResource(R.drawable.ic_checkbox_unchecked);
            }

            mCountryViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    if (mServices.district.equalsIgnoreCase("All"))
                    {
                        if (!mServices.isSelected){
                            for (DistrictModel model:services) {
                                model.isSelected=true;
                                notifyDataSetChanged();
                                selServices.add(model);
                            }
                        }
                        else {
                            for (DistrictModel model:services) {
                                model.isSelected=false;
                                notifyDataSetChanged();
                                selServices.clear();
                            }
                        }

                    }
                    else {
                        mServices.isSelected = !mServices.isSelected;
                        notifyItemChanged(holder.getAdapterPosition());
                        selServices.add(mServices);

                    }

                }
            });


        }
    }


    @Override
    public long getItemId(final int arg0) {
        return 0;
    }

    @Override
    public int getItemCount() {
        if (services == null || services.isEmpty()) {
            return 1;
        } else {
            return services.size();
        }
    }

    /**
     * Sets data.
     *
     * @param mServices the m countries
     */
    public void setData(final ArrayList<DistrictModel> mServices) {
        this.services = mServices;
        notifyDataSetChanged();
    }

    /**
     * Sets data.
     *
     * @return array
     */
    public ArrayList<DistrictModel> getSelectedServiceIds() {
        return selServices;
    }

    /**
     * View holder class for country row
     */
    private final class CountryViewHolder extends RecyclerView.ViewHolder {
        private TextView textView;
        private ImageView ivSelected;

        private CountryViewHolder(final View view) {
            super(view);
            textView = (TextView) view.findViewById(R.id.row_title);
            ivSelected = view.findViewById(R.id.checkBox);
        }
    }

    /**
     * View holder class for empty view layout
     */
    private final class EmptyViewHolder extends RecyclerView.ViewHolder {
        private EmptyViewHolder(final View view) {
            super(view);
        }
    }
}