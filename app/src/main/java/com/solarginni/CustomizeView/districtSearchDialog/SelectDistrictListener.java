package com.solarginni.CustomizeView.districtSearchDialog;


import java.util.ArrayList;


public interface SelectDistrictListener {


    /**
     * On select service.
     *
     * @param districtList the name
     */
    void onSelectDistrict(ArrayList<DistrictModel> districtList);

    void onUnSelectDistrict(ArrayList<DistrictModel> districtList);
}

