package com.solarginni.CustomizeView;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;

import com.solarginni.R;

/**
 * Dated: 19-02-2017.
 */
public class CustomAlertPopup {
    /**
     * Instantiates a new Custom alert dialog.
     *
     * @param builder the builder
     */
    public CustomAlertPopup(final Builder builder) {
    }

    /**
     * The type Builder.
     */
    public static class Builder {
        private final Context mContext;
        private final LayoutInflater mInflater;


        private int mHeight;
        private CharSequence mMessage;
        private CharSequence mPositiveButtonText;
        private int mImageViewResource = R.drawable.mobile_screen;
        private CustomDialogInterface.OnClickListener mPositiveButtonListener;
        private CharSequence mNegativeButtonText;
        private CustomDialogInterface.OnClickListener mNegativeButtonListener;
        private CharSequence mTextViewActionText;
        private CustomDialogInterface.OnClickListener mTextViewActionListener;
        private boolean mCancelable, isShowEdit = false;
        private CustomDialogInterface.OnCancelListener mOnCancelListener;
        private CustomDialogInterface.OnDismissListener mOnDismissListener;
        private AlertDialog.Builder mBuilder;
        private AlertDialog mAlertDialog;
        private EditText editMobile;

        /**
         * Instantiates a new Builder.
         *
         * @param context the context
         */
        public Builder(@NonNull final Context context) {
            mContext = context;
            mCancelable = true;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public Builder(@NonNull final Context context, boolean isShowEditBox) {
            mContext = context;
            mCancelable = true;
            isShowEdit = isShowEditBox;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }


        /**
         * Set the message to display using the given resource id.
         *
         * @param messageId the message id
         * @return This Builder object to allow for chaining of calls to set methods
         */
        public Builder setMessage(@StringRes final int messageId) {
            mMessage = mContext.getString(messageId);
            return this;
        }

        /**
         * Set the message to display using the string.
         *
         * @param messageId the message string
         * @return This Builder object to allow for chaining of calls to set methods
         */
        public Builder setMessage(final String messageId) {
            mMessage = messageId;
            return this;
        }

        /**
         * Set the text for action text view
         *
         * @param imageViewResource the drawable resource to be used
         * @return This Builder object to allow for chaining of calls to set methods
         */
        public Builder setImageViewResc(@DrawableRes final int imageViewResource) {
            mImageViewResource = imageViewResource;
            return this;
        }

        /**
         * Set the text for action text view
         *
         * @param message the message
         * @return This Builder object to allow for chaining of calls to set methods
         */
        public Builder setActionTextViewText(@Nullable final CharSequence message) {
            mTextViewActionText = message;
            return this;
        }


        /**
         * Set the text for action text view using the given resource id.
         *
         * @param messageId the message id
         * @param listener  The {@link DialogInterface.OnClickListener} to use.
         * @return This Builder object to allow for chaining of calls to set methods
         */
        public Builder setActionTextViewText(@StringRes final int messageId, final CustomDialogInterface.OnClickListener listener) {
            mTextViewActionText = mContext.getString(messageId);
            mTextViewActionListener = listener;
            return this;
        }

        /**
         * Set the message to display.
         *
         * @param message  the message
         * @param listener The {@link DialogInterface.OnClickListener} to use.
         * @return This Builder object to allow for chaining of calls to set methods
         */
        public Builder setMessage(@Nullable final CharSequence message, final CustomDialogInterface.OnClickListener listener) {
            mMessage = message;
            mTextViewActionListener = listener;
            return this;
        }


        /**
         * Set a listener to be invoked when the positive button of the dialog is pressed.
         *
         * @param textId   The resource id of the text to display in the positive button
         * @param listener The {@link DialogInterface.OnClickListener} to use.
         * @return This Builder object to allow for chaining of calls to set methods
         */
        public Builder setPositiveButton(@StringRes final int textId, final CustomDialogInterface.OnClickListener listener) {
            mPositiveButtonText = mContext.getString(textId);
            mPositiveButtonListener = listener;
            return this;
        }

        /**
         * Set a listener to be invoked when the positive button of the dialog is pressed.
         *
         * @param text     The text to display in the positive button
         * @param listener The {@link DialogInterface.OnClickListener} to use.
         * @return This Builder object to allow for chaining of calls to set methods
         */
        public Builder setPositiveButton(final CharSequence text, final CustomDialogInterface.OnClickListener listener) {
            mPositiveButtonText = text;
            mPositiveButtonListener = listener;
            return this;
        }

        /**
         * Set a listener to be invoked when the negative button of the dialog is pressed.
         *
         * @param textId   The resource id of the text to display in the negative button
         * @param listener The {@link DialogInterface.OnClickListener} to use.
         * @return This Builder object to allow for chaining of calls to set methods
         */
        public Builder setNegativeButton(@StringRes final int textId, final CustomDialogInterface.OnClickListener listener) {
            mNegativeButtonText = mContext.getString(textId);
            mNegativeButtonListener = listener;
            return this;
        }

        /**
         * Set a listener to be invoked when the negative button of the dialog is pressed.
         *
         * @param text     The text to display in the negative button
         * @param listener The {@link DialogInterface.OnClickListener} to use.
         * @return This Builder object to allow for chaining of calls to set methods
         */
        public Builder setNegativeButton(final CharSequence text, final CustomDialogInterface.OnClickListener listener) {
            mNegativeButtonText = text;
            mNegativeButtonListener = listener;
            return this;
        }

        /**
         * Sets whether the dialog is cancelable or not.  Default is true.
         *
         * @param cancelable the cancelable
         * @return This Builder object to allow for chaining of calls to set methods
         */
        public Builder setCancelable(final boolean cancelable) {
            mCancelable = cancelable;
            return this;
        }

        /**
         * set height of the dialog
         *
         * @param height : height of dialog
         * @return : This Builder object to allow for chaining of calls to set methods
         */
        public Builder setHeight(final int height) {
            mHeight = height;
            return this;
        }

        /**
         * Sets the callback that will be called if the dialog is canceled.
         * <p>
         * <p>Even in a cancelable dialog, the dialog may be dismissed for reasons other than
         * being canceled or one of the supplied choices being selected.
         * If you are interested in listening for all cases where the dialog is dismissed
         * and not just when it is canceled, see
         * {@link #//setOnCancelListener(android.content.DialogInterface.OnCancelListener)
         * setOnDismissListener}*.</p>
         *
         * @param onCancelListener the on cancel listener
         * @return This Builder object to allow for chaining of calls to set methods
         * @see #setCancelable(boolean) #setCancelable(boolean)
         * @see #//setOnCancelListener(android.content.DialogInterface.OnCancelListener)
         * #setOnDismissListener(android.content.DialogInterface.OnDismissListener)
         */
        public Builder setOnCancelListener(final CustomDialogInterface.OnCancelListener onCancelListener) {
            mOnCancelListener = onCancelListener;
            return this;
        }

        /**
         * Sets the callback that will be called when the dialog is dismissed for any reason.
         *
         * @param onDismissListener the on dismiss listener
         * @return This Builder object to allow for chaining of calls to set methods
         */
        public Builder setOnDismissListener(final CustomDialogInterface.OnDismissListener onDismissListener) {
            mOnDismissListener = onDismissListener;
            return this;
        }

        public String getEditText() {
            return editMobile.getText().toString();
        }

        /**
         * Creates an {@link AlertDialog} with the arguments supplied to this
         * builder.
         * <p>
         * Calling this method does not display the dialog. If no additional
         * processing is needed, {@link #show(int gravity)} may be called instead to both
         * create and display the dialog.
         *
         * @return the alert dialog
         */
        public AlertDialog create(int gravity) {
            mBuilder = new AlertDialog.Builder(mContext, R.style.AlertDialogDefault);

            final View dialogView = mInflater.inflate(R.layout.dialog_custom_popup, null);
            mBuilder.setView(dialogView);
            mAlertDialog = mBuilder.create();
            AppCompatTextView tvMessage = dialogView.findViewById(R.id.tvMessage);
            AppCompatTextView tvAction = dialogView.findViewById(R.id.tvAction);
            AppCompatImageView imageView = dialogView.findViewById(R.id.imageView);
            AppCompatButton btnNegative = dialogView.findViewById(R.id.btnNegative);
            AppCompatButton btnPositive = dialogView.findViewById(R.id.btnPositive);
            TextInputLayout textInputLayout = dialogView.findViewById(R.id.tvMobile);
            editMobile = dialogView.findViewById(R.id.editMobile);


            if (isShowEdit) {
                textInputLayout.setVisibility(View.VISIBLE);
            }

            if (mMessage != null) {
                tvMessage.setText(mMessage);
            }

            // set image view resource
            if (mImageViewResource != -1) {
                imageView.setImageResource(mImageViewResource);
            } else {
                imageView.setVisibility(View.GONE);
            }

            //set positive button
            if (mPositiveButtonText != null) {
                btnPositive.setText(mPositiveButtonText);
                btnPositive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        if (!isShowEdit)
                        mAlertDialog.dismiss();
                        if (mPositiveButtonListener != null) {
                            mPositiveButtonListener.onClick();
                        }
                    }
                });
            } else {
                btnPositive.setVisibility(View.GONE);
            }
            //set negative button
            if (mNegativeButtonText != null) {
                btnNegative.setText(mNegativeButtonText);
                btnNegative.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        mAlertDialog.dismiss();
                        if (mNegativeButtonListener != null) {
                            mNegativeButtonListener.onClick();
                        }
                    }
                });
            } else {
                btnNegative.setVisibility(View.GONE);
            }
            //set action text view
            if (mTextViewActionText != null) {
                tvAction.setText(mTextViewActionText);
                tvAction.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        mAlertDialog.dismiss();
                        if (mTextViewActionListener != null) {
                            mTextViewActionListener.onClick();
                        }
                    }
                });
            } else {
                tvAction.setVisibility(View.GONE);
            }
            //set Cancelable
            mAlertDialog.setCancelable(mCancelable);
            mAlertDialog.setCanceledOnTouchOutside(mCancelable);
            //set cancel listener
            if (mOnCancelListener != null) {
                mAlertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(final DialogInterface dialog) {
                        if (mOnCancelListener != null) {
                            mOnCancelListener.onCancel();
                        }
                    }
                });
            }
            //set dismiss listener
            if (mOnDismissListener != null) {
                mAlertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(final DialogInterface dialog) {
                        if (mOnDismissListener != null) {
                            mOnDismissListener.onDismiss();
                        }
                    }
                });
            }

            // set Height
            Window window = mAlertDialog.getWindow();
            if (window != null) {
                window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                window.setGravity(gravity);
            }

            return mAlertDialog;
        }

        /**
         * Creates an {@link AlertDialog} with the arguments supplied to this
         * builder and immediately displays the dialog.
         * <p>
         * Calling this method is functionally identical to:
         * <pre>
         *     AlertDialog dialog = builder.create();
         *     dialog.show();
         * </pre>
         *
         * @return the alert dialog
         */
        public AlertDialog show(int gravity) {
            final AlertDialog dialog = create(gravity);

//            //set background color
//            Window window = dialog.getWindow();
//            if (window != null) {
//                final WindowManager.LayoutParams attributes = window.getAttributes();
//                attributes.dimAmount = 0.0f;
//                window.setAttributes(attributes);
//                window.setBackgroundDrawable(new ColorDrawable(ColorDrawable.));
//                window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
//            }
            if (!((Activity) mContext).isFinishing()) {
                dialog.show();
            }


            return dialog;
        }

        public Builder dismis() {
           mAlertDialog.dismiss();
           return this;
        }

        /**
         * Build custom alert dialog.
         *
         * @return the custom alert dialog
         */
        public CustomAlertPopup build() {
            return new CustomAlertPopup(this);
        }
    }
}