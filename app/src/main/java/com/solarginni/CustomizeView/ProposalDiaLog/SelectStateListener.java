package com.solarginni.CustomizeView.ProposalDiaLog;


import com.solarginni.CustomizeView.ProposalStateModel;
import com.solarginni.CustomizeView.districtSearchDialog.DistrictModel;

import java.util.ArrayList;


public interface SelectStateListener {


    /**
     * On select service.
     *
     * @param districtList the name
     */
    void onSelectState(ArrayList<ProposalStateModel> districtList);

    void onUnSelectState(ArrayList<ProposalStateModel> districtList);
}

