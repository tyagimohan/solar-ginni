package com.solarginni.CustomizeView.ProposalDiaLog;

import android.app.Dialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.solarginni.CustomizeView.ProposalStateModel;
import com.solarginni.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public
class ProposalStateDialog {

    /**
     * Instantiates a new Service Dialog.
     */
    private ProposalStateDialog() {

    }

    /**
     * Show dialog.
     *
     * @param mContext      the m context
     * @param mServiceArray array
     * @param listener      the listener
     * @return the dialog
     */
    public static Dialog show(final Context mContext,String key, final List<ProposalStateModel> mServiceArray, boolean showAll, final SelectStateListener listener) {
        //set service  dialog
        final Dialog dialog = new Dialog(mContext, R.style.AppTheme);
        dialog.setContentView(R.layout.service_dialog);
        Window dialogWindow = dialog.getWindow();
        if (dialogWindow != null) {
            WindowManager.LayoutParams lp = dialogWindow.getAttributes();
            dialogWindow.setGravity(Gravity.TOP);
            dialogWindow.setAttributes(lp);
        }

        //Initialization of views
        final AppCompatImageView ivClear = dialog.findViewById(R.id.ivClear);
        final LinearLayoutCompat allLay = dialog.findViewById(R.id.allLay);
        if (showAll) {
            allLay.setVisibility(View.VISIBLE);
        } else {
            allLay.setVisibility(View.GONE);

        }
        final AppCompatButton btnPositive = dialog.findViewById(R.id.btnPositive);
        final CheckBox checkBox = dialog.findViewById(R.id.checkBox);
        final AppCompatButton btnNegative = dialog.findViewById(R.id.btnNegative);
        final LinearLayout buttonlayout = dialog.findViewById(R.id.buttonlayout);
        buttonlayout.setVisibility(View.VISIBLE);
        final AppCompatEditText searchEditText = (AppCompatEditText) dialog.findViewById(R.id.country_code_picker_search);
        final RecyclerView countryRecyclerView = (RecyclerView) dialog.findViewById(R.id.country_code_picker_recyclerview);
        countryRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        countryRecyclerView.setItemAnimator(new DefaultItemAnimator());
        countryRecyclerView.setHasFixedSize(true);


        ivClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                searchEditText.setText("");
            }
        });


        btnNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                ArrayList<ProposalStateModel> selected = new ArrayList<>();
                if (mServiceArray != null) {
                    for (ProposalStateModel district : mServiceArray) {
                        district.isSelected = false;
                        district.key="";
                        selected.add(district);
                    }
                }
                listener.onUnSelectState(selected);
                dialog.dismiss();
            }
        });
        final ProposalListAdapter adapter = new ProposalListAdapter(mContext, mServiceArray,
                new OnItemSelectListener() {
                    @Override
                    public void onItemSelect(String[] mServices) {
                        dialog.dismiss();

                    }
                });

        countryRecyclerView.setAdapter(adapter);
        checkBox.setClickable(false);
        allLay.setOnClickListener(v -> {

            if (!checkBox.isChecked()) {
                for (ProposalStateModel model : mServiceArray) {
                    model.isSelected = true;
                    adapter.notifyDataSetChanged();
                }
                checkBox.setChecked(true);
            } else {
                for (ProposalStateModel model : mServiceArray) {
                    model.isSelected = false;
                    adapter.notifyDataSetChanged();

                }
                checkBox.setChecked(false);
            }


        });
        btnPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                ArrayList<ProposalStateModel> selected = new ArrayList<>();
                if (mServiceArray != null) {
                    for (ProposalStateModel service : mServiceArray) {
                        if (service.isSelected) {
                            service.key=key;
                            selected.add(service);
                        }else {
                            service.key="";
                            selected.add(service);
                        }
                    }
                }
                if (selected.size() != 0) {
                    listener.onSelectState(selected);
                    dialog.dismiss();
                } else {
                    Toast.makeText(mContext, "Please select State", Toast.LENGTH_SHORT).show();
                }

            }
        });


        searchEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
            }

            @Override
            public void beforeTextChanged(final CharSequence s, final int start, final int count, final int after) {
            }

            @Override
            public void afterTextChanged(final Editable s) {
                //show clear button if edittext is not empty
                ivClear.setVisibility(s.toString().isEmpty() ? View.GONE : View.VISIBLE);
                ArrayList<ProposalStateModel> mServiceList = new ArrayList<>();
                for (ProposalStateModel service : mServiceArray) {
                    if (service.state.toLowerCase(Locale.ENGLISH).contains(s.toString().trim().toLowerCase())) {
                        mServiceList.add(service);
                    }

                }

                adapter.setData(mServiceList);
            }
        });

        dialog.show();
        return dialog;

    }


    interface OnItemSelectListener {
        /**
         * On item select.
         *
         * @param mServices the m services
         */
        void onItemSelect(String[] mServices);
    }


}
