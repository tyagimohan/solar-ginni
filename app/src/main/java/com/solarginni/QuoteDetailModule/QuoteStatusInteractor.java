package com.solarginni.QuoteDetailModule;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;
import com.solarginni.network.MultipartParams;

import java.io.File;

import retrofit2.Retrofit;

public class QuoteStatusInteractor implements QuoteStatusContract.Interactor {

    private Retrofit retrofit;
    private ApiInterface apiInterface;

    public QuoteStatusInteractor(Retrofit retrofit) {
        this.retrofit = retrofit;
        apiInterface = retrofit.create(ApiInterface.class);
    }

    @Override
    public void changeImpStatus(String token, StatusModel model, File file, QuoteStatusContract.OnInteraction interaction) {
        MultipartParams multipartParams = null;
        if (file != null) {
            multipartParams = new MultipartParams.Builder()
                    .addPDFFile("file", file).build();

        } else {
            multipartParams = new MultipartParams.Builder()
                    .addBlankFile("blank").build();

        }


        apiInterface.chnageStausForImp(token, multipartParams.getMap(), model.getQuoteId(), model.getStatus(), model.getPrice(), model.getComment(), model.panelType, model.panelMake, model.inverterSize, model.inverterMake, model.structure_type, model.bidirectional, model.distributionBox, Integer.parseInt(model.warrantyTerm), model.getRecommendedSIze(), model.batteryMake,model.url).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                interaction.onChangeStatus(response);

            }

            @Override
            public void onError(ApiError error) {
                interaction.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                interaction.onFailure(throwable.getMessage());

            }
        });

    }

    @Override
    public void createProposal(String token, StatusModel model, QuoteStatusContract.OnInteraction interaction) {
        apiInterface.createProposal(token, model).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                interaction.ondocumentCreated(response);

            }

            @Override
            public void onError(ApiError error) {
                interaction.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                interaction.onFailure(throwable.getMessage());

            }
        });

    }

    @Override
    public void changeProsStatus(String token, String status, String quote_id, QuoteStatusContract.OnInteraction interaction) {
        apiInterface.chnageStausForPros(token, status, quote_id).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                interaction.onChangeStatus(response);

            }

            @Override
            public void onError(ApiError error) {
                interaction.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                interaction.onFailure(throwable.getMessage());

            }
        });

    }

    @Override
    public void revokeQuote(String token, String quoteId, QuoteStatusContract.OnInteraction interaction) {

        apiInterface.revokeQuote(token, quoteId).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                interaction.onQuoteRevoked();

            }

            @Override
            public void onError(ApiError error) {
                interaction.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {

                interaction.onFailure(throwable.getMessage());
            }
        });

    }
}
