package com.solarginni.QuoteDetailModule;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.solarginni.Base.BaseActivity;
import com.solarginni.Base.BaseFragment;
import com.solarginni.ComponentModel;
import com.solarginni.CustomizeView.CompanyListDialog;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.ImplementerQuoteListModel;
import com.solarginni.DBModel.QuoteListModel;
import com.solarginni.R;
import com.solarginni.Utility.AppConstants;
import com.solarginni.Utility.AppDialogs;
import com.solarginni.Utility.DecimalDigitsInputFilter;
import com.solarginni.Utility.FileUtils;
import com.solarginni.Utility.Permissions;
import com.solarginni.Utility.Utils;

import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;
import static com.solarginni.Utility.AppConstants.COMPONENT_MODEL;
import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.SG_ADMIN;
import static com.solarginni.Utility.AppConstants.TOKEN;

public class QuoteDetailFragment3 extends BaseFragment implements QuoteStatusContract.View, View.OnClickListener {
    private static final int PICKFILE_RESULT_CODE = 7;
    private static final int REQUEST_CODE_FOR_PERMISSION = 5;
    private EditText implementerName, implementerComment, projectPrice, recommendSize, status, requestedOn, responded, attachDocument, inverterSize, inverterMake, panelType, PanelMake, structure_type, distributionBox, bidirectional, labourWarranty, batteryMake;
    private QuoteStatusPresenter presenter;
    private String[] statusArray;
    private File file = null;
    private String statusType;
    private String quoteID;
    private Button revokeQuote;
    private ComponentModel model;
    private String inverterMakeId = "";
    private String panelMakeId = "";
    protected AppDialogs mAppDialogs;
    private Button createProposal, viewProspoal;
    private String proposalUrl = "";
    private String batteryMakeId = "";
    private RadioGroup radioGroup;
    private RadioButton radioButton1, radioButton2;


    @Override
    public int getLayout() {
        return R.layout.quote_details3;
    }

    @Override
    public void initViews(View view) {
        implementerComment = view.findViewById(R.id.implementerComment);
        createProposal = view.findViewById(R.id.createProposal);
        viewProspoal = view.findViewById(R.id.viewProspoal);
        implementerName = view.findViewById(R.id.implementerName);
        projectPrice = view.findViewById(R.id.projectPrice);
        recommendSize = view.findViewById(R.id.recommendSize);
        radioGroup = view.findViewById(R.id.radioGroup);
        responded = view.findViewById(R.id.responded);
        revokeQuote = view.findViewById(R.id.revokeQuote);
        radioButton1 = view.findViewById(R.id.radio_btn1);
        radioButton2 = view.findViewById(R.id.radio_btn2);
        attachDocument = view.findViewById(R.id.attachDocument);
        PanelMake = view.findViewById(R.id.PanelMake);
        batteryMake = view.findViewById(R.id.batteryMake);
        panelType = view.findViewById(R.id.panelType);
        inverterMake = view.findViewById(R.id.inverterMake);
        inverterSize = view.findViewById(R.id.inverterSize);
        requestedOn = view.findViewById(R.id.requestedOn);
        bidirectional = view.findViewById(R.id.bidirectional);
        distributionBox = view.findViewById(R.id.distributionBox);
        labourWarranty = view.findViewById(R.id.labourWarranty);
        structure_type = view.findViewById(R.id.structure_type);
        recommendSize.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(8, 2)});
        status = view.findViewById(R.id.status);
        projectPrice.addTextChangedListener(onTextChangedListener());
        mAppDialogs = new AppDialogs(getActivity());

        disableSuggestion(implementerName);
        disableSuggestion(projectPrice);
        disableSuggestion(recommendSize);
        disableSuggestion(requestedOn);
        disableSuggestion(responded);
        disableSuggestion(status);
        disableSuggestion(attachDocument);
        disableSuggestion(labourWarranty);
        disableSuggestion(inverterSize);
        disableSuggestion(bidirectional);
        disableSuggestion(distributionBox);
        disableSuggestion(structure_type);

        createProposal.setOnClickListener(this);
        viewProspoal.setOnClickListener(this);
        getView().findViewById(R.id.regenrate).setOnClickListener(this);


        recommendSize.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String value = recommendSize.getText().toString();
                if (value.startsWith(".")) {
                    recommendSize.setText("0.");
                    recommendSize.setSelection(recommendSize.getText().length());
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {


            }
        });

        presenter = new QuoteStatusPresenter(this);
    }


    @Override
    public void setUp() {

        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            RadioButton rb = (RadioButton) group.findViewById(checkedId);
            switch (checkedId) {
                case R.id.radio_btn1:
                    getView().findViewById(R.id.attachDocumentLay).setVisibility(View.GONE);
                    break;
                case R.id.radio_btn2:
                    getView().findViewById(R.id.attachDocumentLay).setVisibility(View.VISIBLE);
                    break;

            }
        });

        if (getArguments().getBoolean("isFromImplementer", false)) {
            statusArray = getResources().getStringArray(R.array.implementerStatus);
            ImplementerQuoteListModel.QuoteInfo data = (ImplementerQuoteListModel.QuoteInfo) getArguments().getSerializable("data");
            quoteID = data.getQuoteId();
            panelMakeId = data.panelMake;
            inverterMakeId = data.inverterMake;
            batteryMakeId = data.batteryMake;
            proposalUrl = data.attachment;
            try {
                if (!proposalUrl.equalsIgnoreCase("")&& (data.getStatus().equalsIgnoreCase("Responded")|data.getStatus().equalsIgnoreCase("Accepted")|data.getStatus().equalsIgnoreCase("Booked"))) {
                    getView().findViewById(R.id.comparisionView).setVisibility(View.VISIBLE);
                    getView().findViewById(R.id.regenrate).setVisibility(View.GONE);
                    radioGroup.setVisibility(View.GONE);


                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            implementerName.setText(securePrefManager.getSharedValue(AppConstants.COMPANYNAME));
            implementerComment.setText(data.getComment());
            panelType.setText(data.panelType);
            PanelMake.setText(data.panelMakeName);
            if (data.getQuoteImpData().getLovModel().getSolution().equalsIgnoreCase("Hybrid")) {
                getView().findViewById(R.id.batteryLayout).setVisibility(View.VISIBLE);
            } else {
                getView().findViewById(R.id.batteryLayout).setVisibility(View.GONE);
            }
            batteryMake.setText(data.batteryMake);
            inverterMake.setText(data.inverterMakeName);
            distributionBox.setText(data.distributionBox);
            bidirectional.setText(data.bidirectionalMeter);
            structure_type.setText(data.structureType);
            labourWarranty.setText("" + data.warrantyTerm);
            inverterSize.setText(data.inverterSize);

            if (data.getStatus().equalsIgnoreCase("Requested") | data.getStatus().equalsIgnoreCase("Working") |
                    data.getStatus().equalsIgnoreCase("Revise")) {
                createProposal.setVisibility(View.VISIBLE);

            }


            Long longval = data.getFinalPrice().longValue();
            if (data.getStatus().trim().contains("Respond") | data.getStatus().trim().contains("Booked") | data.getStatus().trim().contains("Accepted")) {
                getView().findViewById(R.id.respondedOnLay).setVisibility(View.VISIBLE);
            }

            DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
            formatter.applyPattern("#,##,##,###");
            String formattedString = formatter.format(longval);

            projectPrice.setText(data.getFinalPrice() == 0 ? "" : formattedString);
            recommendSize.setText(data.getRecmndSiteSize() == 0 ? "" : String.valueOf(data.getRecmndSiteSize()));

            status.setText(data.getStatus());

            if (data.getStatus().equalsIgnoreCase("Requested") | data.getStatus().equalsIgnoreCase("Working")) {
                panelType.setOnClickListener(this);
                PanelMake.setOnClickListener(this);
                inverterMake.setOnClickListener(this);
                batteryMake.setOnClickListener(this);
                bidirectional.setOnClickListener(this);
                distributionBox.setOnClickListener(this);
                structure_type.setOnClickListener(this);

                attachDocument.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (checkPermissionAPI23()) {
                            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                            intent.setType("application/pdf");
                            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
                                    | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICKFILE_RESULT_CODE);
                        }

                    }
                });


            }

            requestedOn.setText(Utils.convertDate(data.getCreatedAt()));
            responded.setText(Utils.convertDate(data.getRespondedDate()));

            if (data.getStatus().equalsIgnoreCase("Requested")) {

                statusArray = new String[]{"Decline", "Working", "Respond"};
                status.setOnClickListener(this);
                implementerComment.setFocusableInTouchMode(true);
                projectPrice.setFocusableInTouchMode(true);
                recommendSize.setFocusableInTouchMode(true);

            }
            if (data.getStatus().equalsIgnoreCase("Revise")) {

                statusArray = new String[]{"Respond"};
                status.setOnClickListener(this);
                implementerComment.setFocusableInTouchMode(true);
                projectPrice.setFocusableInTouchMode(true);
                recommendSize.setFocusableInTouchMode(true);
                panelType.setOnClickListener(this);
                PanelMake.setOnClickListener(this);
                inverterMake.setOnClickListener(this);
                bidirectional.setOnClickListener(this);
                distributionBox.setOnClickListener(this);
                structure_type.setOnClickListener(this);
                batteryMake.setOnClickListener(this);

                attachDocument.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (checkPermissionAPI23()) {
                            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                            intent.setType("application/pdf");
                            intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
                                    | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICKFILE_RESULT_CODE);
                        }

                    }
                });

            } else if (data.getStatus().equalsIgnoreCase("Working")) {
                implementerComment.setFocusableInTouchMode(true);
                projectPrice.setFocusableInTouchMode(true);
                recommendSize.setFocusableInTouchMode(true);
                status.setEnabled(true);
                status.setOnClickListener(this);
                statusArray = new String[]{"Decline", "Respond"};

            } else if (data.getStatus().equalsIgnoreCase("Declined") || data.getStatus().equalsIgnoreCase("Responded") || data.getStatus().equalsIgnoreCase("Accepted")) {
                implementerComment.setFocusableInTouchMode(false);
                projectPrice.setFocusableInTouchMode(false);
                recommendSize.setFocusableInTouchMode(false);
                status.setEnabled(false);
            } else if (data.getStatus().equalsIgnoreCase("Accepted")) {
                implementerComment.setFocusableInTouchMode(false);
                projectPrice.setFocusableInTouchMode(false);
                recommendSize.setFocusableInTouchMode(false);
                status.setEnabled(false);

            }


        } else {
            statusArray = getResources().getStringArray(R.array.prospectStatus);


            int index = getArguments().getInt("index");

            QuoteListModel.QuoteList data = (QuoteListModel.QuoteList) getArguments().getSerializable("data");
            quoteID = data.getDemandQuotes().get(index).getQuoteId();
//            if (data.getDemandQuotes().get(index).attachment != null)
//                attachDocument.setText(data.getDemandQuotes().get(index).attachment.substring(data.getDemandQuotes().get(index).attachment.lastIndexOf('/') + 1, data.getDemandQuotes().get(index).attachment.length()));


            if (data.getDemandQuotes().get(index).getStatus().trim().contains("Respond") | data.getDemandQuotes().get(index).getStatus().trim().contains("Booked") | data.getDemandQuotes().get(index).getStatus().trim().contains("Accepted")) {
                getView().findViewById(R.id.respondedOnLay).setVisibility(View.VISIBLE);
            }

            implementerName.setText(data.getDemandQuotes().get(index).getCompanyName());
            requestedOn.setText(Utils.convertDate(data.getDemandQuotes().get(index).getCreatedAt()));
            responded.setText(Utils.convertDate(data.getDemandQuotes().get(index).getRespondedDate()));
            implementerComment.setText(data.getDemandQuotes().get(index).getComment());
            panelType.setText(data.getDemandQuotes().get(index).panelType);
            PanelMake.setText(data.getDemandQuotes().get(index).panelMake);
            inverterMake.setText(data.getDemandQuotes().get(index).inverterMake);
            inverterSize.setText(data.getDemandQuotes().get(index).inverterSize);
            bidirectional.setText(data.getDemandQuotes().get(index).bidirectionalMeter);
            distributionBox.setText(data.getDemandQuotes().get(index).distributionBox);
            structure_type.setText(data.getDemandQuotes().get(index).structureType);
            labourWarranty.setText("" + data.getDemandQuotes().get(index).warrantyTerm);
            labourWarranty.setFocusableInTouchMode(false);
            inverterSize.setFocusable(false);
            inverterSize.setFocusableInTouchMode(false);
            projectPrice.setText(data.getDemandQuotes().get(index).getFinalPrice() == 0 ? "" : String.valueOf(data.getDemandQuotes().get(index).getFinalPrice()));
            recommendSize.setText(data.getDemandQuotes().get(index).getRecmndSiteSize() == 0 ? "" : String.valueOf(data.getDemandQuotes().get(index).getRecmndSiteSize()));

            status.setText(data.getDemandQuotes().get(index).getStatus());

            proposalUrl = data.getDemandQuotes().get(index).attachment;
            try {
                if (!proposalUrl.equalsIgnoreCase("")) {
                    getView().findViewById(R.id.comparisionView).setVisibility(View.VISIBLE);
                    getView().findViewById(R.id.regenrate).setVisibility(View.GONE);
                    radioGroup.setVisibility(View.GONE);


                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
//            if (attachDocument.getText().length() != 0) {
//                attachDocument.setOnClickListener(v -> {
//                    startActivity(openLink(data.getDemandQuotes().get(index).attachment));
//
//                });
//            }

            if (data.getDemandQuotes().get(index).getStatus().equalsIgnoreCase("Responded")) {
                status.setOnClickListener(this);
            } else {

            }
            if (data.getDemandQuotes().get(index).getStatus().equalsIgnoreCase("Requested")) {
                revokeQuote.setVisibility(View.VISIBLE);
                revokeQuote.setOnClickListener(this);

            } else {

            }

            implementerComment.setFocusableInTouchMode(false);
            projectPrice.setFocusableInTouchMode(false);
            recommendSize.setFocusableInTouchMode(false);


        }

        if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SG_ADMIN)) {
            QuoteListModel.QuoteList data = (QuoteListModel.QuoteList) getArguments().getSerializable("data");
            int index = getArguments().getInt("index");

            if (data.getDemandQuotes().get(index).getStatus().equalsIgnoreCase("Responded") | data.getDemandQuotes().get(index).getStatus().equalsIgnoreCase("Accepted")) {
                if (data.getDemandQuotes().get(index).getStatus().equalsIgnoreCase("Responded"))
                    statusArray = new String[]{"Revise"};
                else
                    statusArray = new String[]{"Booked", "Cancelled"};
                status.setOnClickListener(this);
            } else {
                status.setEnabled(false);

            }

//            if (data.getDemandQuotes().get(index).getStatus().equalsIgnoreCase("Accepted")) {
////                if (data.getDemandQuotes().get(index).getStatus().equalsIgnoreCase("Responded")){
////                    statusArray = new String[]{"Re-Work"};
////
////                }else {
//                    statusArray = new String[]{"Booked", "Cancelled"};
//
//                //}
//                status.setOnClickListener(this);
//            } else {
//                status.setEnabled(false);
//
//            }
        }

    }

    @Override
    public void onChangeStatus(CommomResponse response) {
        hideProgress();
        showToast("Status has been Changed");
        status.setText(statusType);
        if (statusType.equalsIgnoreCase("Accepted") | statusType.equalsIgnoreCase("Declined") | statusType.equalsIgnoreCase("Rejected")) {
            status.setFocusableInTouchMode(false);
        }
        if (statusType.equalsIgnoreCase("Working")) {
            projectPrice.setFocusableInTouchMode(true);
            recommendSize.setFocusableInTouchMode(true);
            implementerComment.setFocusableInTouchMode(true);
        } else {
            projectPrice.setFocusableInTouchMode(false);
            recommendSize.setFocusableInTouchMode(false);
            implementerComment.setFocusableInTouchMode(false);
        }
        getActivity().finish();


    }

    @Override
    public void ondocumentCreated(CommomResponse response) {
        hideProgress();
        String url = response.toResponseModel(String.class);
        getView().findViewById(R.id.comparisionView).setVisibility(View.VISIBLE);
        createProposal.setVisibility(View.GONE);
        proposalUrl = url;
        radioButton1.setChecked(true);
        showToast("Created");


    }

    @Override
    public void onFailure(String msg) {
        hideProgress();
        showToast(msg);

    }

    @Override
    public void onQuoteRevoked() {
        hideProgress();
        getActivity().finish();

    }

    public void showAlertWithList(final String[] dataArray, final @StringRes int title, final TextView textView) {
        hideKeyboard(getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(title));

        if (getArguments().getBoolean("isFromImplementer", false)) {
            builder.setItems(dataArray, (dialog, item) -> {
                StatusModel model = new StatusModel();
                if (dataArray[item].equalsIgnoreCase("Respond")) {
                    if ((projectPrice.getText().length() == 0) || implementerComment.getText().length() == 0 | recommendSize.getText().length() == 0 | panelType.getText().length() == 0 | inverterMake.getText().length() == 0  | structure_type.getText().length() == 0 | distributionBox.getText().length() == 0 | bidirectional.getText().length() == 0 | labourWarranty.getText().length() == 0 | panelMakeId.length() == 0|(proposalUrl.equalsIgnoreCase("")&& file==null)) {
                        showToast("Please fill the fields and attach the proposal document");
                    } else if (getView().findViewById(R.id.batteryLayout).getVisibility() == View.VISIBLE && batteryMake.getText().toString().equalsIgnoreCase("")) {
                        showToast("Please fill BatteryMake");

                    } else {
                        model.setComment(implementerComment.getText().toString());
                        model.setPrice(Integer.parseInt(projectPrice.getText().toString().replace(",", "")));
                        model.setQuoteId(quoteID);
                        model.batteryMake = batteryMakeId;
                        if (inverterMake.getText().toString().trim().equalsIgnoreCase(""))
                            model.inverterMake = null;
                        else
                            model.inverterMake = inverterMakeId;
                        if (PanelMake.getText().toString().trim().equalsIgnoreCase(""))
                            model.panelMake = null;
                        else
                            model.panelMake = panelMakeId;
                        model.panelType = panelType.getText().toString();
                        model.structure_type = structure_type.getText().toString();
                        model.distributionBox = distributionBox.getText().toString();
                        model.bidirectional = bidirectional.getText().toString();
                        model.warrantyTerm = labourWarranty.getText().toString();
                        model.url=proposalUrl;
                        if (!recommendSize.getText().toString().trim().isEmpty()) {
                            if (Double.valueOf(recommendSize.getText().toString()) < 1.0) {
                                showToast("Please enter site size greater than 1.0");
                                return;
                            } else
                                model.setRecommendedSIze(Double.parseDouble(recommendSize.getText().toString()));
                        } else
                            model.setRecommendedSIze(0.0);

                        if (inverterSize.getText().toString().trim().isEmpty()||Double.valueOf(inverterSize.getText().toString()) == 0.0) {
                            model.inverterSize = "0";

                        } else{
                            if (Double.valueOf(inverterSize.getText().toString()) < 1.0) {
                                showToast("Please enter inverter size greater than 1.0");
                                return;
                            } else
                                model.inverterSize = inverterSize.getText().toString();
                        }

                        if (dataArray[item].equalsIgnoreCase("Working")) {
                            model.setStatus(dataArray[item]);
                            statusType = dataArray[item];
                        } else if (dataArray[item].equalsIgnoreCase("Decline")) {
                            model.setStatus(dataArray[item] + "d");
                            statusType = dataArray[item] + "d";
                        } else if (dataArray[item].equalsIgnoreCase("Respond")) {
                            model.setStatus(dataArray[item] + "ed");
                            statusType = dataArray[item] + "ed";
                        }


                        implementerComment.clearFocus();
                        showProgress("Please wait...");
                        presenter.changeImpStatus(securePrefManager.getSharedValue(TOKEN), model, file);

                    }

                } else {
                    model.setComment(implementerComment.getText().toString());
                    if (!projectPrice.getText().toString().trim().isEmpty())
                        model.setPrice(Integer.parseInt(projectPrice.getText().toString().replace(",", "")));
                    else
                        model.setPrice(0);

                    model.setQuoteId(quoteID);
                    model.inverterMake = inverterMakeId;
                    model.panelMake = panelMakeId;
                    model.panelType = panelType.getText().toString();
                    model.structure_type = structure_type.getText().toString();
                    model.distributionBox = distributionBox.getText().toString();
                    model.bidirectional = bidirectional.getText().toString();
                    model.warrantyTerm = labourWarranty.getText().toString();
                    model.url=proposalUrl;
                    if (!recommendSize.getText().toString().trim().isEmpty()) {
                        if (Double.valueOf(recommendSize.getText().toString()) < 1.0) {
                            showToast("Please enter site size greater than 1.0");
                            return;
                        } else
                            model.setRecommendedSIze(Double.parseDouble(recommendSize.getText().toString()));
                    } else
                        model.setRecommendedSIze(0.0);


                    if (inverterSize.getText().toString().trim().isEmpty()||Double.valueOf(inverterSize.getText().toString()) == 0.0) {
                        model.inverterSize = "0";
                    } else{
                        if (Double.valueOf(inverterSize.getText().toString()) < 1.0) {
                            showToast("Please enter inverter size greater than 1.0");
                            return;
                        } else
                            model.inverterSize = inverterSize.getText().toString();
                    }
                    if (dataArray[item].equalsIgnoreCase("Working")) {
                        model.setStatus(dataArray[item]);
                        statusType = dataArray[item];
                    } else if (dataArray[item].equalsIgnoreCase("Decline")) {
                        model.setStatus(dataArray[item] + "d");
                        statusType = dataArray[item] + "d";
                    } else if (dataArray[item].equalsIgnoreCase("Respond")) {
                        model.setStatus(dataArray[item] + "ed");
                        statusType = dataArray[item] + "ed";
                    }
                    showProgress("Please wait...");
                    presenter.changeImpStatus(securePrefManager.getSharedValue(TOKEN), model, file);
                }


            });
        } else {

            builder.setItems(dataArray, (dialog, item) -> {

                statusType = dataArray[item];
                showProgress("Please wait...");
                presenter.changeProsStatus(securePrefManager.getSharedValue(TOKEN), dataArray[item], quoteID);


            });

        }


        AlertDialog alerts = builder.create();
        alerts.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.status:
                showAlertWithList(statusArray, R.string.select_chnage, status);
                break;
            case R.id.viewProspoal:
                startActivity(openLink(proposalUrl));
                break;
            case R.id.createProposal:
            case R.id.regenrate:
                StatusModel model1 = new StatusModel();

                if ((projectPrice.getText().length() == 0) || implementerComment.getText().length() == 0 | recommendSize.getText().length() == 0 | panelType.getText().length() == 0 | inverterMake.getText().length() == 0 | structure_type.getText().length() == 0 | distributionBox.getText().length() == 0 | bidirectional.getText().length() == 0 | labourWarranty.getText().length() == 0 | panelMakeId.length() == 0) {
                    showToast("Please fill the fields");
                } else {
                    model1.setComment(implementerComment.getText().toString());
                    model1.setPrice(Integer.parseInt(projectPrice.getText().toString().replace(",", "")));
                    model1.setQuoteId(quoteID);
                    if (inverterMake.getText().toString().trim().equalsIgnoreCase(""))
                        model1.inverterMake = null;
                    else
                        model1.inverterMake = inverterMakeId;
                    if (PanelMake.getText().toString().trim().equalsIgnoreCase(""))
                        model1.panelMake = null;
                    else
                        model1.panelMake = panelMakeId;
                    model1.panelType = panelType.getText().toString();
                    model1.structure_type = structure_type.getText().toString();
                    model1.distributionBox = distributionBox.getText().toString();
                    model1.bidirectional = bidirectional.getText().toString();
                    model1.warrantyTerm = labourWarranty.getText().toString();
                    model1.batteryMake=batteryMakeId;
                    if (!recommendSize.getText().toString().trim().isEmpty()) {
                        if (Double.valueOf(recommendSize.getText().toString()) < 1.0) {
                            showToast("Please enter site size greater than 1.0");
                            return;
                        } else
                            model1.setRecommendedSIze(Double.parseDouble(recommendSize.getText().toString()));
                    } else
                        model1.setRecommendedSIze(0.0);

                    if (inverterSize.getText().toString().trim().isEmpty()|| Double.valueOf(inverterSize.getText().toString()) == 0.0) {
                        model1.inverterSize = "0";

                    } else{
                        if (Double.valueOf(inverterSize.getText().toString()) < 1.0) {
                            showToast("Please enter inverter size greater than 1.0");
                            return;
                        } else
                            model1.inverterSize = inverterSize.getText().toString();
                    }

                    model1.setStatus("");


                    implementerComment.clearFocus();
                    showProgress("Proposal Generation in Progress...");
                    presenter.createProposal(securePrefManager.getSharedValue(TOKEN), model1);

                }
                break;
            case R.id.revokeQuote:


                mAppDialogs.showInfoDialogBothButtons("Cancel", "Do you want to Canel this Quote?", new AppDialogs.DialogInfo() {
                    @Override
                    public void onPositiveButton(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }

                    @Override
                    public void onNegativeButton(DialogInterface dialog, int which) {
                        if (Utils.hasNetwork(getContext())) {
                            showProgress("Please wait...");
                            presenter.revokeQuote(securePrefManager.getSharedValue(TOKEN), quoteID);

                        } else {
                            showToast("Please check Internet");
                        }
                    }
                }, "Ok");


                break;
            case R.id.inverterMake:
                model = Utils.getObj(securePrefManager.getSharedValue(COMPONENT_MODEL), ComponentModel.class);
                CompanyListDialog.show(getContext(), model.data.inverter, item -> {
                    inverterMakeId = item.getCompanyId();
                    inverterMake.setText(item.getCompanyName());
                });
                break;
            case R.id.batteryMake:
                model = Utils.getObj(securePrefManager.getSharedValue(COMPONENT_MODEL), ComponentModel.class);
                CompanyListDialog.show(getContext(), model.data.battery, item -> {
                    batteryMakeId = item.getCompanyId();
                    batteryMake.setText(item.getCompanyName());
                });
                break;
            case R.id.PanelMake:
                model = Utils.getObj(securePrefManager.getSharedValue(COMPONENT_MODEL), ComponentModel.class);
                CompanyListDialog.show(getContext(), model.data.panel, item -> {
                    panelMakeId = item.getCompanyId();
                    PanelMake.setText(item.getCompanyName());
                });
                break;
            case R.id.panelType:
                model = Utils.getObj(securePrefManager.getSharedValue(COMPONENT_MODEL), ComponentModel.class);
                ((BaseActivity) getActivity()).showAlertWithList(model.data.panelType.toArray(new String[0]), "Please select Panel Type", panelType);
                break;
            case R.id.structure_type:
                model = Utils.getObj(securePrefManager.getSharedValue(COMPONENT_MODEL), ComponentModel.class);
                ((BaseActivity) getActivity()).showAlertWithList(model.data.structureType.toArray(new String[0]), "Please select Structure Type", structure_type);
                break;
            case R.id.distributionBox:
                model = Utils.getObj(securePrefManager.getSharedValue(COMPONENT_MODEL), ComponentModel.class);
                ((BaseActivity) getActivity()).showAlertWithList(model.data.distribution_box.toArray(new String[0]), "Please select Distribution Box", distributionBox);
                break;
            case R.id.bidirectional:
                model = Utils.getObj(securePrefManager.getSharedValue(COMPONENT_MODEL), ComponentModel.class);
                ((BaseActivity) getActivity()).showAlertWithList(model.data.bidirectional_meter.toArray(new String[0]), "Please select Bidirectional Meter", bidirectional);
                break;
        }

    }

    private TextWatcher onTextChangedListener() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                projectPrice.removeTextChangedListener(this);

                try {
                    String originalString = s.toString();

                    Long longval;
                    if (originalString.contains(",")) {
                        originalString = originalString.replaceAll(",", "");
                    }
                    longval = Long.parseLong(originalString);

                    DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                    formatter.applyPattern("#,##,##,###");
                    String formattedString = formatter.format(longval);

                    //setting text after format to EditText
                    projectPrice.setText(formattedString);
                    projectPrice.setSelection(projectPrice.getText().length());
                } catch (NumberFormatException nfe) {
                    nfe.printStackTrace();
                }

                projectPrice.addTextChangedListener(this);
            }
        };
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PICKFILE_RESULT_CODE:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    Uri uri = data.getData();


                    try {
                        file = FileUtils.getFile(getContext(), uri);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (file != null)
                        attachDocument.setText(file.getName());
                    else {
                        showToast("Please Select file from your Storage");
                    }
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);


        super.onActivityResult(requestCode, resultCode, data);


    }


    private boolean checkPermissionAPI23() {
        boolean isPermissionRequired = false;
        List<String> permissionArray = new ArrayList<>();


        if (Permissions.getInstance().checkReadWritePermissions(getActivity()).permissions.size() > 0) {
            isPermissionRequired = true;
            permissionArray.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            permissionArray.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (isPermissionRequired) {
            Permissions.getInstance().addPermission(permissionArray).askForPermissions(getActivity(), REQUEST_CODE_FOR_PERMISSION);
            return false;
        }
        return true;
    }
}
