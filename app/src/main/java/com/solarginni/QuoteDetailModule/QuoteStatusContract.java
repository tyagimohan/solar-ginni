package com.solarginni.QuoteDetailModule;

import com.solarginni.DBModel.CommomResponse;

import java.io.File;

public interface QuoteStatusContract  {

    interface  View{
        void onChangeStatus(CommomResponse response);
        void ondocumentCreated(CommomResponse response);
        void onFailure(String msg);
        void onQuoteRevoked();
    }

    interface  Presenter{
        void changeImpStatus(String token, StatusModel model, File file);
        void createProposal(String token,StatusModel model);
        void changeProsStatus(String token, String status,String quote_id);
        void revokeQuote(String token, String quoteId);
    }
    interface  Interactor{
        void changeImpStatus(String token,StatusModel model,File file,OnInteraction interaction);

        void createProposal(String token,StatusModel model,OnInteraction interaction);
        void changeProsStatus(String token,String status,String quote_id,OnInteraction interaction);
        void revokeQuote(String token, String quoteId,OnInteraction interaction);

    }

    interface OnInteraction{
        void onChangeStatus(CommomResponse response);
        void onQuoteRevoked();
        void ondocumentCreated(CommomResponse response);

        void onFailure(String msg);
    }

}
