package com.solarginni.QuoteDetailModule;

import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.RestClient;

import java.io.File;

public class QuoteStatusPresenter implements QuoteStatusContract.Presenter, QuoteStatusContract.OnInteraction {

    private QuoteStatusContract.View view;
    private QuoteStatusInteractor interactor;

    public QuoteStatusPresenter(QuoteStatusContract.View view) {
        this.view = view;
        interactor = new QuoteStatusInteractor(RestClient.getRetrofitBuilder());
    }

    @Override
    public void changeImpStatus(String token, StatusModel model, File file) {
        interactor.changeImpStatus(token, model, file, this);

    }

    @Override
    public void createProposal(String token, StatusModel model) {
        interactor.createProposal(token, model, this);

    }

    @Override
    public void changeProsStatus(String token, String status, String quote_id) {
        interactor.changeProsStatus(token, status, quote_id, this);

    }

    @Override
    public void revokeQuote(String token, String quoteId) {
        interactor.revokeQuote(token, quoteId, this);
    }

    @Override
    public void onChangeStatus(CommomResponse response) {
        view.onChangeStatus(response);

    }

    @Override
    public void onQuoteRevoked() {
        view.onQuoteRevoked();

    }

    @Override
    public void ondocumentCreated(CommomResponse response) {
        view.ondocumentCreated(response);

    }

    @Override
    public void onFailure(String msg) {
        view.onFailure(msg);

    }
}
