package com.solarginni.QuoteDetailModule;

import com.google.android.material.textfield.TextInputLayout;
import android.view.View;
import android.widget.EditText;

import com.solarginni.Base.BaseFragment;
import com.solarginni.DBModel.ImplementerQuoteListModel;
import com.solarginni.DBModel.QuoteListModel;
import com.solarginni.R;

import static com.solarginni.Utility.AppConstants.BO_MANAGER;
import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.SG_ADMIN;

public class QuoteDetailsFragment1 extends BaseFragment {

    private EditText phase, siteType, roofStyle, solutions, solarSize, city, state, customerId, pincode,line1,line2;
    private EditText inverter, panel, battery, paymentTerm, serviceLevel,priceRange,siteDistance,siteCount,sortedValue,panelType;
    private TextInputLayout batteryLayout;
    private String[] sortedArr;



    @Override
    public int getLayout() {
        return R.layout.quote_request;
    }

    @Override
    public void initViews(View view) {

        phase = view.findViewById(R.id.phase);
        siteType = view.findViewById(R.id.siteType);
        pincode = view.findViewById(R.id.pincode);
        roofStyle = view.findViewById(R.id.roofStyle);
        solutions = view.findViewById(R.id.solution);
        solarSize = view.findViewById(R.id.solarsize);
        city = view.findViewById(R.id.city);
        state = view.findViewById(R.id.state);
        customerId = view.findViewById(R.id.customerId);
        line2 = view.findViewById(R.id.line2);
        line1 = view.findViewById(R.id.line1);


        inverter = view.findViewById(R.id.inverter);
        panel = view.findViewById(R.id.panel);
        panelType = view.findViewById(R.id.panelType);
        battery = view.findViewById(R.id.battery);
        paymentTerm = view.findViewById(R.id.payment);
        batteryLayout = view.findViewById(R.id.batteryLayout);
        serviceLevel = view.findViewById(R.id.serviceLevel);
        priceRange = view.findViewById(R.id.priceRange);
        sortedValue = view.findViewById(R.id.sortedValue);
        siteDistance = view.findViewById(R.id.siteDistance);
        siteCount = view.findViewById(R.id.siteCount);

        disableSuggestion(inverter);
        disableSuggestion(panel);
        disableSuggestion(paymentTerm);
        disableSuggestion(battery);
        disableSuggestion(serviceLevel);
        disableSuggestion(phase);
        disableSuggestion(siteType);
        disableSuggestion(pincode);
        disableSuggestion(city);
        disableSuggestion(state);
        disableSuggestion(customerId);
        disableSuggestion(solarSize);
        disableSuggestion(solutions);
        disableSuggestion(line1);
        disableSuggestion(line2);
        disableSuggestion(roofStyle);

        if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SG_ADMIN)|securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(BO_MANAGER)){
            view.findViewById(R.id.filterLayout).setVisibility(View.VISIBLE);
            view.findViewById(R.id.filterLay).setVisibility(View.VISIBLE);
            view.findViewById(R.id.filterInfo).setVisibility(View.VISIBLE);
            siteCount.setVisibility(View.VISIBLE);
        }else
        {
            view.findViewById(R.id.filterLayout).setVisibility(View.GONE);
            view.findViewById(R.id.filterLay).setVisibility(View.GONE);
            view.findViewById(R.id.filterInfo).setVisibility(View.GONE);
            siteCount.setVisibility(View.GONE);
        }


    }

    @Override
    public void setUp() {
        if (getArguments().getBoolean("isFromImplementer", false)) {
            ImplementerQuoteListModel.QuoteInfo data = (ImplementerQuoteListModel.QuoteInfo) getArguments().getSerializable("data");

            phase.setText(data.getQuoteImpData().getLovModel().getPhase());
            solarSize.setText(data.getQuoteImpData().getSolarSize());
            solutions.setText(data.getQuoteImpData().getLovModel().getSolution());
            roofStyle.setText(data.getQuoteImpData().getLovModel().getRoofStyle());
            siteType.setText(data.getQuoteImpData().getLovModel().getSiteType());
            city.setText(data.getQuoteImpData().getAddress().getCity());
            pincode.setText(data.getQuoteImpData().getAddress().getPinCode());
            state.setText(data.getQuoteImpData().getAddress().getState());
            customerId.setText(data.getQuoteImpData().getUserId());

            if (data.getStatus().equalsIgnoreCase("Accepted")){
                line1.setText(data.getQuoteImpData().getAddress().getAddressLine1());
                line2.setText(data.getQuoteImpData().getAddress().getAddressLine2());
            }
            else {
                line1.setText("********");
                line2.setText("********");
            }
            inverter.setText(data.getQuoteImpData().getLovModel().getInverterName());
            panel.setText(data.getQuoteImpData().getLovModel().getPanelName());
            panelType.setText(data.getQuoteImpData().getLovModel().panelType);
            if (data.getQuoteImpData().getPaymentTerm() == 1) {
                paymentTerm.setText(getResources().getText(R.string.payment_term1));
            } else {
                paymentTerm.setText(getResources().getText(R.string.payment_term2));
            }


            if (data.getQuoteImpData().getServiceLevel() == 1) {
                serviceLevel.setText(getResources().getText(R.string.leve1));
            } else if (data.getQuoteImpData().getServiceLevel() == 2) {
                serviceLevel.setText(getResources().getText(R.string.level2));
            } else {
                serviceLevel.setText(getResources().getText(R.string.level3));
            }
            if (data.getQuoteImpData().getLovModel().getSolution().equalsIgnoreCase("Hybrid")) {
                batteryLayout.setVisibility(View.VISIBLE);
                battery.setText(data.getQuoteImpData().getLovModel().getBatteryName());
            } else {
                batteryLayout.setVisibility(View.GONE);

            }


        } else {
            QuoteListModel.QuoteList data = (QuoteListModel.QuoteList) getArguments().getSerializable("data");
            sortedArr = getResources().getStringArray(R.array.sorted_value);
            inverter.setText(data.getLovModel().getInverterName());
            panelType.setText(data.getLovModel().panelType);

            panel.setText(data.getLovModel().getPanelName());

            if (data.getPaymentTerm() == 1) {
                paymentTerm.setText(getResources().getText(R.string.payment_term1));
            } else {
                paymentTerm.setText(getResources().getText(R.string.payment_term2));

            }
            int index = getArguments().getInt("index");

            if (data.getServiceLevel() == 1) {
                serviceLevel.setText(getResources().getText(R.string.leve1));
            } else if (data.getServiceLevel() == 2) {
                serviceLevel.setText(getResources().getText(R.string.level2));

            } else {
                serviceLevel.setText(getResources().getText(R.string.level3));
            }

            siteCount.setText(""+data.getDemandQuotes().get(index).getImpleSite());
            siteDistance.setText(""+data.getDemandQuotes().get(index).getSiteDist());
            priceRange.setText(""+data.getDemandQuotes().get(index).getPriceRange());
            sortedValue.setText(sortedArr[data.getDemandQuotes().get(index).sorted]);

            if (data.getLovModel().getSolution().equalsIgnoreCase("Hybrid")) {
                batteryLayout.setVisibility(View.VISIBLE);
                battery.setText(data.getLovModel().getBatteryName());
            } else {
                batteryLayout.setVisibility(View.GONE);

            }


            phase.setText(data.getLovModel().getPhase());
            solarSize.setText(data.getSolarSize());
            solutions.setText(data.getLovModel().getSolution());
            roofStyle.setText(data.getLovModel().getRoofStyle());
            siteType.setText(data.getLovModel().getSiteType());
            city.setText(data.getAddress().getCity());
            pincode.setText(data.getAddress().getPinCode());
            state.setText(data.getAddress().getState());
            customerId.setText(data.getUserId());
            line1.setText(data.getAddress().getAddressLine1());
            line2.setText(data.getAddress().getAddressLine2());
        }


    }
}
