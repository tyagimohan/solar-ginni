package com.solarginni.QuoteDetailModule;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class StatusModel implements Serializable {

    @SerializedName("comment")
    private String comment;
    @SerializedName("price")
    private Integer price;
    @SerializedName("quoteId")
    private String quoteId;
    @SerializedName("recommendedSIze")
    private Double recommendedSIze;
    @SerializedName("status")
    public String status;
    @SerializedName("panelMake")
    public String panelMake;
    @SerializedName("inverterSize")
    public String inverterSize;
    @SerializedName("inverterMake")
    public String inverterMake;
    @SerializedName("panelType")
    public String panelType;
    @SerializedName("bidirectionalMeter")
    public String bidirectional;
    @SerializedName("distributionBox")
    public String distributionBox;
    @SerializedName("structureType")
    public String structure_type;
    @SerializedName("warrantyTerm")
    public String warrantyTerm;
    @SerializedName("batteryMake")
    public String batteryMake;
    @SerializedName("url")
    public String url;



    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getQuoteId() {
        return quoteId;
    }

    public void setQuoteId(String quoteId) {
        this.quoteId = quoteId;
    }

    public Double getRecommendedSIze() {
        return recommendedSIze;
    }

    public void setRecommendedSIze(Double recommendedSIze) {
        this.recommendedSIze = recommendedSIze;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
