package com.solarginni.QuoteDetailModule;

import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.ViewGroup;

import com.solarginni.Base.BaseActivity;
import com.solarginni.CustomizeView.CustomPager;
import com.solarginni.R;

public class QuoteDetailsActivity extends BaseActivity {

    private CustomPager viewPager;
    private TabLayout tabIndicator;
    private ViewPagerAdapter adapter;

    @Override
    public int getLayout() {
        return R.layout.implementer_quote_details;
    }

    @Override
    public void initViews() {
        viewPager = findViewById(R.id.viewPager);
        tabIndicator = findViewById(R.id.tabIndicator);
        viewPager.disableScroll(false);

        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("");
        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });
        adapter = new ViewPagerAdapter(getSupportFragmentManager());

    }

    @Override
    public void setUp() {

        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(adapter);
        viewPager.setOffscreenPageLimit(2);
        tabIndicator.setupWithViewPager(viewPager);
        tabIndicator.setSelected(false);


    }

    public class ViewPagerAdapter extends FragmentStatePagerAdapter implements ViewPager.OnPageChangeListener {

        private Fragment[] childFragments;

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
            childFragments = new Fragment[]{
                    new QuoteDetailsFragment1(), //0
                    new QuoteDetailsFragment2(),
                    new QuoteDetailFragment3()
            };
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            super.setPrimaryItem(container, position, object);
            int mCurrentPosition = -1;
            if (position != mCurrentPosition) {
                Fragment fragment = (Fragment) object;
                CustomPager pager = (CustomPager) container;
                if (fragment != null && fragment.getView() != null) {
                    mCurrentPosition = position;
                    pager.measureCurrentView(fragment.getView());
                }
            }
        }
        @Override
        public Fragment getItem(int position) {
            childFragments[position].setArguments(getIntent().getExtras());

            return childFragments[position];
        }

        @Override
        public int getCount() {
            return childFragments.length;
        }

        @Override
        public void onPageScrolled(int i, float v, int i1) {
            hideKeyboard(QuoteDetailsActivity.this);


        }

        @Override
        public void onPageSelected(int i) {


        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    }


}
