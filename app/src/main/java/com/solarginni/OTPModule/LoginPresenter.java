package com.solarginni.OTPModule;

import android.content.Context;
import com.solarginni.network.RestClient;

public class LoginPresenter implements LoginContract.Presenter, LoginContract.OnInteraction {
    private LoginInteractor interactor;
    private LoginContract.View view;

    public LoginPresenter(LoginContract.View view) {
        this.view = view;
        interactor = new LoginInteractor(RestClient.getRetrofitBuilder());
    }

    @Override
    public void sentOtp(Context context, String mobileNumber) {
        interactor.sentOtp(context, mobileNumber, this);

    }

    @Override
    public void onOtpSent(String msg,String token,String otp) {
        view.onOtpSent(msg,token,otp);

    }

    @Override
    public void onFailure(String msg) {
        view.onFailure(msg);
    }
}
