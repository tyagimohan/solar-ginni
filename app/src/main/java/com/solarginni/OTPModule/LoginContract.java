package com.solarginni.OTPModule;

import android.content.Context;

public interface LoginContract {

    interface View {

         void onOtpSent(String msg,String token,String otp);
        void onFailure(String msg);

    }

    interface Presenter {

        void sentOtp(Context context, String mobileNumber);

    }

    interface Interactor {
        void sentOtp(Context context, String mobileNumber,LoginContract.OnInteraction listner);
    }

    interface OnInteraction {
             void onOtpSent(String msg,String  token,String otp);
             void onFailure(String msg);
    }

}
