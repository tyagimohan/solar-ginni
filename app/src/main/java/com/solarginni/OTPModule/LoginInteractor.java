package com.solarginni.OTPModule;

import android.content.Context;
import android.provider.Settings;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.OtpModel;
import com.solarginni.DBModel.APIBody.SendOtp;
import com.solarginni.DBModel.TokenModel;
import com.solarginni.R;
import com.solarginni.Utility.SecurePrefManager;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Retrofit;

import static com.solarginni.Utility.AppConstants.FCM_TOKEN;

public class LoginInteractor implements LoginContract.Interactor {
    private Retrofit mRetrofit;
    private ApiInterface mApiInterface;

    public LoginInteractor(Retrofit retrofit) {
        this.mRetrofit = retrofit;
        mApiInterface = mRetrofit.create(ApiInterface.class);

    }

    @Override
    public void sentOtp(Context context, String mobileNumber, LoginContract.OnInteraction listener) {
        //     mApiInterface.sendOtp(, )
        SecurePrefManager securePrefManager= new SecurePrefManager(context);
        String deviceId=securePrefManager.getSharedValue(FCM_TOKEN);
        Map<String, String> map = new HashMap<>();
        map.put("phone",mobileNumber);
        map.put("deviceId",deviceId);

        SendOtp otp= new SendOtp(mobileNumber,deviceId);


        mApiInterface.sendOtp(otp, context.getString(R.string.static_token)).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
            @Override
            public void onSuccess(CommomResponse commomResponse) {
                TokenModel otpModel = commomResponse.toResponseModel(TokenModel.class);

                //TODO Save Authentication Token
                listener.onOtpSent(commomResponse.getMessage(), otpModel.getToken(),otpModel.getOtp());


            }

            @Override
            public void onError(ApiError error) {
                listener.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                listener.onFailure(throwable.getMessage());


            }
        });

    }
}
