package com.solarginni.Implementer.SetEmpanelment;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.DBModel.SiteDetailsModel.EmapnelmentInfo;
import com.solarginni.R;
import com.solarginni.Utility.Utils;

public class EmpanelmentAdapter extends BaseRecyclerAdapter<EmapnelmentInfo.EmpInfo, EmpanelmentAdapter.Holder> {


    public EmpanelmentAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public int getLayout(int viewType) {
        return R.layout.empanelment_list_item;
    }

    @Override
    public Holder getViewHolder(View view, int viewType) {
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {

        holder.nodalBody.setText(getItem(i).getNodalBody());
        holder.endDate.setText(Utils.convertDate(getItem(i).getEnd()));
        holder.fromDate.setText(Utils.convertDate(getItem(i).getFrom()));




    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView nodalBody,endDate,fromDate;


        public Holder(@NonNull View itemView) {
            super(itemView);
            nodalBody= itemView.findViewById(R.id.nodalBody);
            fromDate= itemView.findViewById(R.id.fromDate);
            endDate= itemView.findViewById(R.id.endDate);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            getListener().onClick(getItem(getAdapterPosition()),getAdapterPosition());

        }
    }
}
