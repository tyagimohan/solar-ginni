package com.solarginni.Implementer.SetEmpanelment;

import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.EmpanelMentBody;
import com.solarginni.network.RestClient;

public class SetEmpanelmentPresenter implements SetEmapnelmentContract.Preseneter,SetEmapnelmentContract.OnInteraction {

    private SetEmapanementInteractor interactor;


    private SetEmapnelmentContract.View view;


    public SetEmpanelmentPresenter(SetEmapnelmentContract.View view) {
        this.view = view;
        interactor= new SetEmapanementInteractor(RestClient.getRetrofitBuilder());
    }

    @Override
    public void onSetEmapanelMent() {
        view.onSetEmapanelMent();

    }

    @Override
    public void onFailure(String msg) {
        view.onFailure(msg);


    }

    @Override
    public void onFetcheEmpanelMent(CommomResponse response) {
        view.onFetcheEmpanelMent(response);

    }

    @Override
    public void onFetchNodalBoday(CommomResponse response) {
        view.onFetchNodalBoday(response);

    }

    @Override
    public void setEmpanelMentInfo(String token, EmpanelMentBody model,Integer id) {


        interactor.setEmpanelMentInfo(token,model,id,this);
    }

    @Override
    public void fetchEmpanelment(String token,String companyId) {
        interactor.fetchEmpanelment(token,companyId,this);

    }

    @Override
    public void fetchNodalBoday(String token) {
        interactor.fetchNodalBoday(token,this);

    }
}
