package com.solarginni.Implementer.SetEmpanelment;

import android.app.DatePickerDialog;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.solarginni.Base.BaseActivity;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.EmpanelMentBody;
import com.solarginni.DBModel.NodalBodyLov;
import com.solarginni.DBModel.SiteDetailsModel.EmapnelmentInfo;
import com.solarginni.R;
import com.solarginni.Utility.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static com.solarginni.Utility.AppConstants.TOKEN;

public class SetEmpanelmentActivity extends BaseActivity implements View.OnClickListener, SetEmapnelmentContract.View {

    private final Calendar myCalendar = Calendar.getInstance();
    private EditText nodalBoday, fromDate, endDate;
    private Button submitBtn;
    private SetEmpanelmentPresenter presenter;
    private String[] nodalBodyArray;
    private int id;
    private NodalBodyLov nodalBodyLov;
    private String state = "";

    @Override
    public int getLayout() {
        return R.layout.set_empanelment;
    }

    @Override
    public void initViews() {

        nodalBoday = findViewById(R.id.nodalBody);
        fromDate = findViewById(R.id.from);
        endDate = findViewById(R.id.end);
        submitBtn = findViewById(R.id.confirmBtn);

        disableSuggestion(nodalBoday);
        disableSuggestion(fromDate);
        disableSuggestion(endDate);

        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("");
        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });
        presenter = new SetEmpanelmentPresenter(this);

    }

    @Override
    public void setUp() {
        fromDate.setOnClickListener(this);
        endDate.setOnClickListener(this);
        submitBtn.setOnClickListener(this);
        nodalBoday.setOnClickListener(this);

        nodalBodyLov = (NodalBodyLov) getIntent().getExtras().getSerializable("data");
        nodalBodyArray = new String[nodalBodyLov.lov.size()];

        for (int i = 0; i < nodalBodyLov.lov.size(); i++) {
            nodalBodyArray[i] = nodalBodyLov.lov.get(i).getName();
        }
        if (getIntent().getExtras().getBoolean("isForEdit", false)) {
            EmapnelmentInfo.EmpInfo empInfo = (EmapnelmentInfo.EmpInfo) getIntent().getExtras().getSerializable("nodalBodyData");
            nodalBoday.setText(empInfo.getNodalBody());
            endDate.setText(Utils.convertDate(empInfo.getEnd()));
            fromDate.setText(Utils.convertDate(empInfo.getFrom()));
            id = empInfo.getId();
        } else {

        }

    }

    private void openDatePicker(TextView textView, boolean isSetminDate) {

        int year = myCalendar.get(Calendar.YEAR);
        int month = myCalendar.get(Calendar.MONTH);
        int day = myCalendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.DialogTheme, (datePicker, year1, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year1);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            String myFormat = "dd/MM/yyyy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
            textView.setText(sdf.format(myCalendar.getTime()));

        }, year, month,
                day);


        if (!isSetminDate) {
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            endDate.setText("");
        } else {
            datePickerDialog.getDatePicker().setMinDate(convertTime(fromDate.getText().toString()));
        }

        datePickerDialog.show();

    }

    public long convertTime(String time) {
        long time1 = (long) 0.0;
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");


        try {
            Date date = df.parse(time);
            time1 = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return time1;

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.from:
                openDatePicker(fromDate, false);
                break;
            case R.id.end:
                openDatePicker(endDate, true);

                break;
            case R.id.nodalBody:
                showAlertWithList(nodalBodyArray, R.string.select_nodal, nodalBoday);

                break;
            case R.id.confirmBtn:
                if (nodalBoday.getText().length() == 0 || fromDate.getText().length() == 0 | endDate.getText().length() == 0) {
                    showToast("Please fill details");
                    return;
                } else {
                    showProgress("Please wait...");
                    EmpanelMentBody body = new EmpanelMentBody();
                    body.setNodalBody(nodalBoday.getText().toString());
                    body.setEnd(endDate.getText().toString());
                    body.setCity("All");
                    body.setState(state);
                    body.setFrom(fromDate.getText().toString());
                    if (getIntent().getExtras().getBoolean("isForEdit", false))
                        presenter.setEmpanelMentInfo(securePrefManager.getSharedValue(TOKEN), body, id);
                    else
                        presenter.setEmpanelMentInfo(securePrefManager.getSharedValue(TOKEN), body, -1);
                }
                break;
        }

    }

    public void showAlertWithList(final String[] dataArray, final @StringRes int title, final TextView textView) {
        hideKeyboard(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(title));
        builder.setItems(dataArray, (dialog, item) -> {
            textView.setText(dataArray[item]);
            state = nodalBodyLov.lov.get(item).getState();
        });
        AlertDialog alerts = builder.create();
        alerts.show();
    }


    @Override
    public void onSetEmapanelMent() {
        hideProgress();
        showToast("Empanelment details successfully recorded");
        setResult(RESULT_OK);
        finish();


    }

    @Override
    public void onFailure(String msg) {

        hideProgress();

        showToast(msg);

    }

    @Override
    public void onFetcheEmpanelMent(CommomResponse response) {

    }

    @Override
    public void onFetchNodalBoday(CommomResponse response) {

    }
}
