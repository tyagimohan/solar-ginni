package com.solarginni.Implementer.SetEmpanelment;

import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.EmpanelMentBody;

public interface SetEmapnelmentContract {

    interface  View {
        void onSetEmapanelMent();
        void onFailure(String msg);
        void onFetcheEmpanelMent(CommomResponse response);
        void onFetchNodalBoday(CommomResponse response);
    }

    interface Preseneter{
        void setEmpanelMentInfo(String token, EmpanelMentBody model,Integer id);
        void fetchEmpanelment(String token,String companyId);
        void fetchNodalBoday(String token);
    }

    interface Interactor{
        void setEmpanelMentInfo(String token, EmpanelMentBody model,Integer id,OnInteraction interaction);
        void fetchEmpanelment(String token,String companyId,OnInteraction interaction);
        void fetchNodalBoday(String token,OnInteraction interaction);


    }

    interface  OnInteraction{
        void onSetEmapanelMent();
        void onFailure(String msg);
        void onFetcheEmpanelMent(CommomResponse response);
        void onFetchNodalBoday(CommomResponse response);

    }
}
