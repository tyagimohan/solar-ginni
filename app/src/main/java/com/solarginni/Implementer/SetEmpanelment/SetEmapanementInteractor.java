package com.solarginni.Implementer.SetEmpanelment;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.EmpanelMentBody;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;

import retrofit2.Retrofit;

public class SetEmapanementInteractor implements SetEmapnelmentContract.Interactor {


    private Retrofit retrofit;
    private ApiInterface apiInterface;

    public SetEmapanementInteractor(Retrofit retrofit) {
        this.retrofit = retrofit;
        apiInterface = retrofit.create(ApiInterface.class);
    }

    @Override
    public void setEmpanelMentInfo(String token, EmpanelMentBody model, Integer id, SetEmapnelmentContract.OnInteraction interaction) {

        apiInterface.addEmapanelmentInfo(token, model, String.valueOf(id)).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                interaction.onSetEmapanelMent();

            }

            @Override
            public void onError(ApiError error) {
                interaction.onFailure(error.getMessage());
            }

            @Override
            public void onFailure(Throwable throwable) {


                interaction.onFailure(throwable.getMessage());

            }
        });
    }

    @Override
    public void fetchEmpanelment(String token,String companyId, SetEmapnelmentContract.OnInteraction interaction) {
        apiInterface.fetchEmapanelmentInfo(token,companyId).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                interaction.onFetcheEmpanelMent(response);

            }

            @Override
            public void onError(ApiError error) {
                interaction.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                interaction.onFailure(throwable.getMessage());

            }
        });

    }

    @Override
    public void fetchNodalBoday(String token, SetEmapnelmentContract.OnInteraction interaction) {
        apiInterface.fetchNodalBodyInfo(token).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                interaction.onFetchNodalBoday(response);

            }

            @Override
            public void onError(ApiError error) {
                interaction.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                interaction.onFailure(throwable.getMessage());

            }
        });


    }
}
