package com.solarginni.Implementer.SetEmpanelment;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.solarginni.Base.BaseActivity;
import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.NodalBodyLov;
import com.solarginni.DBModel.SiteDetailsModel.EmapnelmentInfo;
import com.solarginni.R;
import com.solarginni.Utility.Utils;

import static com.solarginni.Utility.AppConstants.IMPLEMENTER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.SUPER_IMPLEMENTER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.TOKEN;

public class EmpanelmentList extends BaseActivity implements SetEmapnelmentContract.View {
    private static final int REQUEST_FOR_SET = 12;
    private RecyclerView recyclerViewe;
    private FloatingActionButton setEmpanelment;
    private SetEmpanelmentPresenter presenter;
    private boolean isForUpdate = false;
    private EmapnelmentInfo.EmpInfo empInfo;
    private String companyId;
    private EmpanelmentAdapter.OnClick<EmapnelmentInfo.EmpInfo> listener = (data, position) -> {

        if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(IMPLEMENTER_ROLE_ID)|securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SUPER_IMPLEMENTER_ROLE_ID)){
            showProgress("Please wait..");
            presenter.fetchNodalBoday(securePrefManager.getSharedValue(TOKEN));
            isForUpdate = true;
            empInfo = data;
        }



    };

    @Override
    public int getLayout() {
        return R.layout.empanelment_list;
    }

    @Override
    public void initViews() {

        recyclerViewe = findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        recyclerViewe.setLayoutManager(manager);
        recyclerViewe.setNestedScrollingEnabled(false);
        recyclerViewe.setHasFixedSize(true);
        recyclerViewe.addItemDecoration(new BaseRecyclerAdapter.DividerItemDecoration(this, RecyclerView.VERTICAL));
        setEmpanelment = findViewById(R.id.setPrice);
        presenter = new SetEmpanelmentPresenter(this);

        if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SUPER_IMPLEMENTER_ROLE_ID)|securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(IMPLEMENTER_ROLE_ID)){
            setEmpanelment.show();
        }

        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("");
        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });
        setEmpanelment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utils.hasNetwork(EmpanelmentList.this)) {
                    showProgress("Please wait..");
                    presenter.fetchNodalBoday(securePrefManager.getSharedValue(TOKEN));
                    isForUpdate = false;
                } else {
                    showToast("Please Check Internet");
                }


            }
        });

    }

    @Override
    public void setUp() {

        companyId = getIntent().getExtras().getString("companyId","");

        if (Utils.hasNetwork(this)) {
            showProgress("Please wait...");
            presenter.fetchEmpanelment(securePrefManager.getSharedValue(TOKEN),companyId);
        } else {
            showToast("Please check Internet");
        }

    }

    @Override
    public void onSetEmapanelMent() {
        hideProgress();

    }

    @Override
    public void onFailure(String msg) {
        hideProgress();

    }

    @Override
    public void onFetcheEmpanelMent(CommomResponse response) {
        hideProgress();

        EmapnelmentInfo emapnelmentInfo = response.toResponseModel(EmapnelmentInfo.class);
        EmpanelmentAdapter adapter = new EmpanelmentAdapter(this);

        adapter.addAll(emapnelmentInfo.getEmpInfo());

        recyclerViewe.setAdapter(adapter);
        adapter.setOnClickListener(listener);

    }

    @Override
    public void onFetchNodalBoday(CommomResponse response) {
        hideProgress();
        NodalBodyLov nodalBodyLov = response.toResponseModel(NodalBodyLov.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("data", nodalBodyLov);
        bundle.putBoolean("isForEdit", isForUpdate);
        if (isForUpdate)
            bundle.putSerializable("nodalBodyData", empInfo);

        Intent intent = new Intent(EmpanelmentList.this, SetEmpanelmentActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, REQUEST_FOR_SET);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_FOR_SET) {
            if (resultCode == RESULT_OK)
                presenter.fetchEmpanelment(securePrefManager.getSharedValue(TOKEN),companyId);

        }
    }

}
