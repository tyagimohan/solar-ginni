package com.solarginni.Implementer;

import android.Manifest;
import android.content.Intent;

import androidx.appcompat.app.AlertDialog;

import com.kbeanie.multipicker.api.CacheLocation;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.solarginni.Base.BaseActivity;
import com.solarginni.R;
import com.solarginni.Utility.Permissions;

import java.util.ArrayList;
import java.util.List;

public class SetCompanyProfile extends BaseActivity implements ImagePickerCallback {


    private static final int OPEN_MEDIA_PICKER = 1;
    private ImagePicker mImagePicker;
    private CameraImagePicker mCameraImagePicker;

    @Override
    public int getLayout() {
        return R.layout.set_company_page;
    }

    @Override
    public void initViews() {

        mImagePicker = new ImagePicker(this);
        mImagePicker.setCacheLocation(CacheLocation.INTERNAL_APP_DIR);
        mImagePicker.setImagePickerCallback(this);
        mImagePicker.shouldGenerateThumbnails(false);
        mImagePicker.shouldGenerateMetadata(false);
        mCameraImagePicker = new CameraImagePicker(this);
        mCameraImagePicker.setCacheLocation(CacheLocation.INTERNAL_APP_DIR);
        mCameraImagePicker.setImagePickerCallback(this);
        mCameraImagePicker.shouldGenerateThumbnails(false);
        mCameraImagePicker.shouldGenerateMetadata(false);

        findViewById(R.id.confirmBtn).setOnClickListener(v -> {
            if (checkPermissionAPI23()) {

                getImagePicker();
            }

        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        switch (requestCode) {
            case Picker.PICK_IMAGE_DEVICE:
                if (resultCode == RESULT_OK) {
                    mImagePicker.submit(data);
                } else {

                }
                break;

            case Picker.PICK_IMAGE_CAMERA:
                if (resultCode == RESULT_OK) {
                    mCameraImagePicker.submit(data);
                } else {

                }
                break;
        }

    }


    @Override
    public void setUp() {

    }

    @Override
    public void onImagesChosen(List<ChosenImage> list) {
//        siteImage.setVisibility(View.VISIBLE);
//        mPhotoFile = new File(list.get(0).getOriginalPath());
//        Bitmap bitmap = Utils.compressImage(mPhotoFile.getPath());
//        siteImage.setImageBitmap(bitmap);


//        OutputStream os = null;
//        try {
//            os = new BufferedOutputStream(new FileOutputStream(mPhotoFile));
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
//        try {
//            os.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public void onError(String s) {

    }

    private void getImagePicker() {
        CharSequence[] options = getResources().getStringArray(R.array.image_picker_options);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(options, (dialog, which) -> {
            switch (which) {
                case 0:
                    mCameraImagePicker.pickImage();
                    break;
                case 1:
                    mImagePicker.pickImage();
                    break;
                default:
                    break;
            }
        });
        builder.show();
    }

    private boolean checkPermissionAPI23() {
        boolean isPermissionRequired = false;
        List<String> permissionArray = new ArrayList<>();

        if (Permissions.getInstance().hasCameraPermission(this).permissions.size() > 0) {
            permissionArray.add(Manifest.permission.CAMERA);
            isPermissionRequired = true;
        }

        if (Permissions.getInstance().checkReadWritePermissions(this).permissions.size() > 0) {
            isPermissionRequired = true;
            permissionArray.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            permissionArray.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (isPermissionRequired) {
            Permissions.getInstance().addPermission(permissionArray).askForPermissions(this, 2);
            return false;
        }
        return true;
    }
}
