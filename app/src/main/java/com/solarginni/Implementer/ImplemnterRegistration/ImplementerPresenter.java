package com.solarginni.Implementer.ImplemnterRegistration;

import android.content.Context;

import com.solarginni.DBModel.APIBody.ImplementerModel;
import com.solarginni.network.RestClient;

import java.io.File;

public class ImplementerPresenter implements ImplementerContract.Presenter, ImplementerContract.OnInteraction {

    private ImplementerContract.View view;
    private ImplementerInteractor interactor;


    public ImplementerPresenter(ImplementerContract.View view) {
        this.view = view;
        interactor = new ImplementerInteractor(RestClient.getRetrofitBuilder());
    }


    @Override
    public void onRegister(String userID,String role) {
        view.onRegister(userID,role);

    }

    @Override
    public void onFailure(String msg) {
        view.onFailure(msg);

    }

    @Override
    public void registerimplenter(Context context, ImplementerModel model, String token, File file) {
        interactor.registerimplenter(context, model, token, file,this);

    }
}
