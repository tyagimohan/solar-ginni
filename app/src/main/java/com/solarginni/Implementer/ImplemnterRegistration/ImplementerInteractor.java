package com.solarginni.Implementer.ImplemnterRegistration;

import android.content.Context;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.APIBody.ImageType;
import com.solarginni.DBModel.APIBody.ImplementerModel;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.UserIdModel;
import com.solarginni.Utility.AppConstants;
import com.solarginni.Utility.SecurePrefManager;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;
import com.solarginni.network.MultipartParams;

import java.io.File;

import retrofit2.Retrofit;

public class ImplementerInteractor implements ImplementerContract.Interactor {

    private Retrofit mRetrofit;
    private ApiInterface mApiInterface;

    public ImplementerInteractor(Retrofit mRetrofit) {
        this.mRetrofit = mRetrofit;
        mApiInterface = mRetrofit.create(ApiInterface.class);
    }


    @Override
    public void registerimplenter(Context context, ImplementerModel model, String token, File file, ImplementerContract.OnInteraction listenr) {

        if (file == null) {
            mApiInterface.registerImplementer(model, token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                @Override
                public void onSuccess(CommomResponse commomResponse) {

                    UserIdModel model = commomResponse.toResponseModel(UserIdModel.class);
                    listenr.onRegister(model.getUserId(),model.role);

                }

                @Override
                public void onError(ApiError error) {
                    listenr.onFailure(error.getMessage());

                }

                @Override
                public void onFailure(Throwable throwable) {
                    listenr.onFailure(throwable.getMessage());

                }
            });

        } else {
            MultipartParams multipartParams = new MultipartParams.Builder()
                    .addFile("upload", file).build();

            mApiInterface.uploadImage(multipartParams.getMap(), ImageType.USER_TYPE, token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                @Override
                public void onSuccess(CommomResponse commomResponse) {
                    String profileImage = commomResponse.toResponseModel(String.class);
                    SecurePrefManager securePrefManager = new SecurePrefManager(context);
                    securePrefManager.storeSharedValue(AppConstants.PROFILE_IMAGE, profileImage);
                    model.profileImage = profileImage;

                    mApiInterface.registerImplementer(model, token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                        @Override
                        public void onSuccess(CommomResponse commomResponse) {

                            UserIdModel model = commomResponse.toResponseModel(UserIdModel.class);
                            listenr.onRegister(model.getUserId(),model.role);

                        }

                        @Override
                        public void onError(ApiError error) {
                            listenr.onFailure(error.getMessage());

                        }

                        @Override
                        public void onFailure(Throwable throwable) {
                            listenr.onFailure(throwable.getMessage());

                        }
                    });


                }

                @Override
                public void onError(ApiError error) {
                    listenr.onFailure(error.getMessage());

                }

                @Override
                public void onFailure(Throwable throwable) {
                    listenr.onFailure(throwable.getMessage());
                }
            });

        }
    }


}
