package com.solarginni.Implementer.ImplemnterRegistration;

import android.content.Context;

import com.solarginni.DBModel.APIBody.ImplementerModel;

import java.io.File;

public interface ImplementerContract {

    interface View {

        void onRegister(String userID,String role);

        void onFailure(String msg);
    }

    interface Presenter {
        void registerimplenter(Context context, ImplementerModel model, String token, File file);
    }


    interface OnInteraction {
        void onRegister(String userID,String role);

        void onFailure(String msg);
    }

    interface Interactor {
        void registerimplenter(Context context, ImplementerModel model, String token,File file, ImplementerContract.OnInteraction listenr);

    }
}

