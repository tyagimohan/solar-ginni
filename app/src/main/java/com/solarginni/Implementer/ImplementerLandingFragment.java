package com.solarginni.Implementer;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.android.material.tabs.TabLayout;
import com.makeramen.roundedimageview.RoundedImageView;
import com.solarginni.Base.BaseFragment;
import com.solarginni.CommonModule.Testimonial.StatePagerAdapter;
import com.solarginni.GetDetails.ImplementerDetails;
import com.solarginni.Implementer.SetEmpanelment.EmpanelmentList;
import com.solarginni.Implementer.SetPricingModule.PriceListActivity;
import com.solarginni.Implementer.SetProposal.SetProposalTerms;
import com.solarginni.R;
import com.solarginni.SGUser.RegisterNewUser;
import com.solarginni.Utility.MyValueFornatter;
import com.solarginni.Utility.Utils;
import com.solarginni.Utility.VideoPlayerActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static android.graphics.Typeface.BOLD;
import static com.solarginni.Utility.AppConstants.CONTACTEMAIL;
import static com.solarginni.Utility.AppConstants.CONTACTNUMBER;
import static com.solarginni.Utility.AppConstants.IMPLEMENTER_DETAILS;

public class ImplementerLandingFragment extends BaseFragment implements View.OnClickListener {
    final long DELAY_MS = 500;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 3000;
    Timer timer, tmTimer;
    private TextView myInstalledSite, siteCapacity, respondedQuottes, convertedOrder, pendingQuotes, pendingServices;
    private CardView lay1, lay3, lay4, lay7, lay8,lay9,lay10;
    private ViewPager adViewPager;
    private TabLayout adTabIndicator;
    private int adCurrentPage = 0, tmCurentPage = 0;
    private Handler adHandler, tmHandler;
    private AdviewPageAdapter adviewPageAdapter;
    private ViewPager tmViewpager;
    private TabLayout tmIndicator;
    private List<String> array= new ArrayList<>();
    private RelativeLayout videoLayout;
    private RelativeLayout tmLayout;
    private PieChart nationPieChart, statePieChart;



    @Override
    public int getLayout() {
        return R.layout.implementer_landing;
    }

    @Override
    public void initViews(View view) {
        myInstalledSite = view.findViewById(R.id.implementerName);
        siteCapacity = view.findViewById(R.id.tvReqCount);
        lay1 = view.findViewById(R.id.card1);
        lay3 = view.findViewById(R.id.card5);
        lay4 = view.findViewById(R.id.pendingReqCard);
        lay7 = view.findViewById(R.id.card7);
        lay8 = view.findViewById(R.id.card8);
        lay9 = view.findViewById(R.id.card9);
        lay10 = view.findViewById(R.id.card10);
        tmLayout = view.findViewById(R.id.tmLayout);
        videoLayout = view.findViewById(R.id.videoLayout);

        statePieChart = view.findViewById(R.id.StatePieChart);
        nationPieChart = view.findViewById(R.id.NationalPieChart);

        respondedQuottes = view.findViewById(R.id.tvImpSiteCount);
        convertedOrder = view.findViewById(R.id.tvTotalSiteCount);
        pendingQuotes = view.findViewById(R.id.tvNewMonthSiteCount);
        pendingServices = view.findViewById(R.id.pendingReq);

        adViewPager = view.findViewById(R.id.adViewPager);
        tmViewpager = view.findViewById(R.id.tmViewPager);
        adTabIndicator = view.findViewById(R.id.advTabIndicator);
        tmIndicator = view.findViewById(R.id.tmTabIndicator);
        view.findViewById(R.id.EmailUs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                    emailIntent.setType("text/html");
                    emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{CONTACTEMAIL});
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
                    emailIntent.putExtra(Intent.EXTRA_TEXT, "");

                    ResolveInfo best = null;
                    try {
                        final PackageManager pm = getContext().getPackageManager();
                        final List<ResolveInfo> matches = pm.queryIntentActivities(emailIntent, 0);
                        for (final ResolveInfo info : matches) {
                            if (info.activityInfo.packageName.endsWith(".gm") || info.activityInfo.name.toLowerCase().contains("gmail")) {
                                best = info;
                                break;
                            }
                        }
                    } catch (Exception e) {
                    }

                    if (best != null) {
                        emailIntent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
                        getContext().startActivity(emailIntent);
                    } else {
                        emailIntent.setType("message/rfc822");
                        getContext().startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                    }
                } catch (Exception e) {
                }
            }
        });
        view.findViewById(R.id.callUs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + CONTACTNUMBER));
                startActivity(intent);
            }
        });

    }

    @Override
    public void setUp() {
        ImplementerDetails details = Utils.getObj(securePrefManager.getSharedValue(IMPLEMENTER_DETAILS), ImplementerDetails.class);

        myInstalledSite.setText(String.valueOf(details.getInstallSite()));
        siteCapacity.setText(String.valueOf(details.getInstallCapacity()));
        convertedOrder.setText(String.valueOf(details.getConvertedOrder()));
        pendingServices.setText(String.valueOf(details.getServiceRequest()));
        pendingQuotes.setText(String.valueOf(details.getPendingQuotes()));
        respondedQuottes.setText(String.valueOf(details.getRespondedQuotes()));
        array.addAll(details.advertisementList);

        setNationalPieChart(details);
        setStatePieChart(details);

        myInstalledSite.setOnClickListener(this);
        pendingQuotes.setOnClickListener(this);
        pendingServices.setOnClickListener(this);
        lay1.setOnClickListener(this);
        lay3.setOnClickListener(this);
        lay4.setOnClickListener(this);
        lay7.setOnClickListener(this);
        lay8.setOnClickListener(this);
        lay9.setOnClickListener(this);
        lay10.setOnClickListener(this);
        videoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), VideoPlayerActivity.class);
                intent.putExtra("URL", getResources().getString(R.string.implemnter_video_url));
                startActivity(intent);
            }
        });


        HandlerViewPager(details);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.implementerName:
            case R.id.card1:
                ((ImplemeterHomeActivity) getActivity()).bottomNavigationView.setSelectedItemId(R.id.nav_site);

                break;
            case R.id.tvNewMonthSiteCount:
            case R.id.card5:
                ((ImplemeterHomeActivity) getActivity()).bottomNavigationView.setSelectedItemId(R.id.nav_quote);


                break;
            case R.id.pendingReq:
            case R.id.pendingReqCard:

                ((ImplemeterHomeActivity) getActivity()).bottomNavigationView.setSelectedItemId(R.id.nav_request);


                break;
            case R.id.card7:

                Bundle bundle = new Bundle();
                goToNextScreen(PriceListActivity.class, bundle);


                break;
            case R.id.card8:
                bundle = new Bundle();
                goToNextScreen(EmpanelmentList.class, bundle);

                break;
            case R.id.card9:
                bundle = new Bundle();
                goToNextScreen(SetProposalTerms.class, bundle);

                break;
                case R.id.card10:
                bundle = new Bundle();
                goToNextScreen(RegisterNewUser.class, bundle);

                break;
        }

    }

    private void HandlerViewPager(ImplementerDetails details) {

        adviewPageAdapter = new AdviewPageAdapter(array);
        adViewPager.setAdapter(adviewPageAdapter);
        adTabIndicator.setupWithViewPager(adViewPager, true);

        adHandler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (adCurrentPage == array.size()) {
                    adCurrentPage = 0;
                }
                adViewPager.setCurrentItem(adCurrentPage++, true);
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                adHandler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);

        adViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                adCurrentPage = i;
                adViewPager.setCurrentItem(adCurrentPage++, true);

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });


        StatePagerAdapter adapter = new StatePagerAdapter(getChildFragmentManager(), details.getTestimonialModelList());

        tmViewpager.setAdapter(adapter);

        tmIndicator.setupWithViewPager(tmViewpager, true);
          if (details.getTestimonialModelList().size()==0){
              tmLayout.setVisibility(View.GONE);
          }
          else{
              tmLayout.setVisibility(View.VISIBLE);

          }

        tmHandler = new Handler();
        final Runnable UpdateTm = new Runnable() {
            public void run() {
                if (tmCurentPage == details.getTestimonialModelList().size()) {
                    tmCurentPage = 0;
                }
                tmViewpager.setCurrentItem(tmCurentPage++, true);
            }
        };

        tmTimer = new Timer(); // This will create a new Thread
        tmTimer.schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                tmHandler.post(UpdateTm);
            }
        }, DELAY_MS, PERIOD_MS);

        tmViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                tmCurentPage = i;
                tmViewpager.setCurrentItem(tmCurentPage++, true);

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });


    }

    private class AdviewPageAdapter extends PagerAdapter {

        private List<String> adList = new ArrayList<>();


        public AdviewPageAdapter(List<String> adList) {
            this.adList = adList;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {

            View view = LayoutInflater.from(getContext()).inflate(R.layout.advertise_layout, container, false);
            RoundedImageView imageView = view.findViewById(R.id.advertisementImage);
            Glide.with(getContext()).load(adList.get(position)).into(imageView);

//            ImageLoader.getInstance().displayImage(adList.get(position), imageView);

            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return adList.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
            return view == o;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {

        }
    }


    private void setNationalPieChart(ImplementerDetails detail) {


        if (detail.getInstallSite() == 0) {
            nationPieChart.setVisibility(View.GONE);
        }

        nationPieChart.setUsePercentValues(true);
        nationPieChart.getDescription().setEnabled(false);
        nationPieChart.setExtraOffsets(5, 10, 5, 5);
        nationPieChart.setDragDecelerationFrictionCoef(0.9f);
        nationPieChart.setTransparentCircleRadius(61f);
        nationPieChart.setCenterText("Sites in India");
        nationPieChart.setCenterTextColor(getResources().getColor(R.color.white));
        nationPieChart.setHoleColor(Color.TRANSPARENT);
        nationPieChart.setRotationEnabled(false);
        nationPieChart.setDrawEntryLabels(false);
        nationPieChart.setUsePercentValues(false);

        nationPieChart.animateY(1000, Easing.EaseInOutCubic);
        ArrayList<PieEntry> yValues = new ArrayList<>();

        nationPieChart.getDescription().setText("");
        nationPieChart.getLegend().setTextColor(Color.WHITE);

        if (detail.nationalModel.residential != 0)
            yValues.add(new PieEntry(detail.nationalModel.residential * detail.getInstallSite(), "Residential"));
        if (detail.nationalModel.industrial != 0)
            yValues.add(new PieEntry(detail.nationalModel.industrial * detail.getInstallSite(), "Industrial"));
        if (detail.nationalModel.commercial != 0)
            yValues.add(new PieEntry(detail.nationalModel.commercial * detail.getInstallSite(), "Commercial"));
        if (detail.nationalModel.religious != 0)
            yValues.add(new PieEntry(detail.nationalModel.religious * detail.getInstallSite(), "Religious"));
        if (detail.nationalModel.institutional != 0)
            yValues.add(new PieEntry(detail.nationalModel.institutional * detail.getInstallSite(), "Institutional"));
        if (detail.nationalModel.others != 0)
            yValues.add(new PieEntry(detail.nationalModel.others * detail.getInstallSite(), "Others"));

        PieDataSet dataSet = new PieDataSet(yValues, "");
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);
        dataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        PieData pieData = new PieData((dataSet));
        pieData.setValueTextSize(15f);
        pieData.setValueTypeface(Typeface.defaultFromStyle(BOLD));
        pieData.setValueFormatter(new MyValueFornatter());
        pieData.setValueTextColor(getResources().getColor(R.color.white));

        nationPieChart.setData(pieData);


    }

    private void setStatePieChart(ImplementerDetails detail) {


        if (detail.stateCount == 0) {
            statePieChart.setVisibility(View.GONE);
        }

        statePieChart.setUsePercentValues(true);
        statePieChart.getDescription().setEnabled(true);
        statePieChart.setExtraOffsets(5, 10, 5, 5);
        statePieChart.setDragDecelerationFrictionCoef(0.9f);
        statePieChart.setTransparentCircleRadius(61f);
        statePieChart.setCenterText("Sites in Your Home State");
        statePieChart.setCenterTextColor(getResources().getColor(R.color.white));
        statePieChart.setHoleColor(Color.TRANSPARENT);
        statePieChart.setRotationEnabled(false);
        statePieChart.setUsePercentValues(false);

        statePieChart.setDrawEntryLabels(false);
        statePieChart.animateY(1000, Easing.EaseInOutCubic);
        ArrayList<PieEntry> yValues = new ArrayList<>();

        statePieChart.getDescription().setText("");
        statePieChart.getLegend().setTextColor(Color.WHITE);
        if (detail.stateModel.residential != 0)
            yValues.add(new PieEntry(detail.stateModel.residential * detail.stateCount, "Residential"));
        if (detail.stateModel.industrial != 0)
            yValues.add(new PieEntry(detail.stateModel.industrial * detail.stateCount, "Industrial"));
        if (detail.stateModel.commercial != 0)
            yValues.add(new PieEntry(detail.stateModel.commercial * detail.stateCount, "Commercial"));
        if (detail.stateModel.religious != 0)
            yValues.add(new PieEntry(detail.stateModel.religious * detail.stateCount, "Religious"));
        if (detail.stateModel.institutional != 0)
            yValues.add(new PieEntry(detail.stateModel.institutional * detail.stateCount, "Institutional"));
        if (detail.stateModel.others != 0)
            yValues.add(new PieEntry(detail.stateModel.others * detail.stateCount, "Others"));

        PieDataSet dataSet = new PieDataSet(yValues, "");
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);
        dataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        PieData pieData = new PieData((dataSet));
        pieData.setValueTextSize(15f);
        pieData.setValueFormatter(new MyValueFornatter());

        pieData.setValueTypeface(Typeface.defaultFromStyle(BOLD));
        pieData.setValueTextColor(getResources().getColor(R.color.white));

        statePieChart.setData(pieData);

    }

}
