package com.solarginni.Implementer;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import androidx.appcompat.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.kbeanie.multipicker.api.CacheLocation;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.solarginni.Base.BaseActivity;
import com.solarginni.CommonModule.ImplementerCompanyModel;
import com.solarginni.CustomizeView.CompanyListDialog;
import com.solarginni.DBModel.APIBody.ImplementerModel;
import com.solarginni.DBModel.CompanyMakeModel;
import com.solarginni.Implementer.ImplemnterRegistration.ImplementerContract;
import com.solarginni.Implementer.ImplemnterRegistration.ImplementerPresenter;
import com.solarginni.R;
import com.solarginni.Utility.AppConstants;
import com.solarginni.Utility.Permissions;
import com.solarginni.Utility.Utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import static com.solarginni.Utility.AppConstants.MOBILE;
import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.TOKEN;

public class ImplementerRegistration extends BaseActivity implements View.OnClickListener, ImagePickerCallback, ImplementerContract.View {

    private static final int REQUEST_FOR_PLACE = 2;
    private static final int REQUEST_CODE_FOR_PERMISSION = 4;
    private Button submitBtn;
    private EditText etPincode, etLine1, etName, etEmail, etPromotion, addLine2, etCompany;
    private TextInputLayout address1Layout, address2Layout, pincodeLayout;
    private TextView tvCity, tvState, tvCountry, tvMobileNumber, editImage;
    private Geocoder mGeocoder;
    private ImageView mapImg;
    private String url = "", mobileNumber;
    private String lat, lng;
    private int role;
    private ImagePicker mImagePicker;
    private CameraImagePicker mCameraImagePicker;
    private File mPhotoFile = null;
    private ImageView profileImage;
    private List<CompanyMakeModel> companyArray = new ArrayList<>();
    private String companyId;
    private ImplementerPresenter presenter;
    private ImplementerCompanyModel details;
    private LinearLayout lowerView;
    private EditText etCity;
    private String city;


    @Override
    public int getLayout() {
        return R.layout.layout_implementer_registration;
    }

    @Override
    public void initViews() {

        submitBtn = findViewById(R.id.submit_btn);
        addLine2 = findViewById(R.id.line2);
        tvCity = findViewById(R.id.tvCity);
        tvState = findViewById(R.id.tvState);
        tvMobileNumber = findViewById(R.id.mobileNumber);
        tvCountry = findViewById(R.id.tvCountry);
        etCity = findViewById(R.id.tvCity);
        etLine1 = findViewById(R.id.etLine1);
        etPromotion = findViewById(R.id.etPromotion);
        etCompany = findViewById(R.id.etCompany);
        etName = findViewById(R.id.etName);
        etEmail = findViewById(R.id.etEmail);
        mapImg = findViewById(R.id.mapImg);
        lowerView = findViewById(R.id.lowerView);
        address1Layout = findViewById(R.id.line1lay);
        address2Layout = findViewById(R.id.line2lay);
        pincodeLayout = findViewById(R.id.pincoeLayout);
        etPincode = findViewById(R.id.etPincode);
        editImage = findViewById(R.id.editImage);
        profileImage = findViewById(R.id.profileImage);
        mGeocoder = new Geocoder(this, Locale.getDefault());
        Places.initialize(getApplicationContext(), getString(R.string.google_api));

        presenter = new ImplementerPresenter(this);
        mImagePicker = new ImagePicker(this);
        mImagePicker.setCacheLocation(CacheLocation.INTERNAL_APP_DIR);
        mImagePicker.setImagePickerCallback(this);
        mImagePicker.shouldGenerateThumbnails(false);
        mImagePicker.shouldGenerateMetadata(false);

        mCameraImagePicker = new CameraImagePicker(this);
        mCameraImagePicker.setCacheLocation(CacheLocation.INTERNAL_APP_DIR);
        mCameraImagePicker.setImagePickerCallback(this);
        mCameraImagePicker.shouldGenerateThumbnails(false);
        mCameraImagePicker.shouldGenerateMetadata(false);

        disableSuggestion(addLine2);
        disableSuggestion(etCity);
        disableSuggestion(etEmail);
        disableSuggestion(etLine1);
        disableSuggestion(etName);
        disableSuggestion(etPincode);
        disableSuggestion(etPromotion);
        disableSuggestion(tvCountry);
        disableSuggestion(tvCity);
        disableSuggestion(tvState);
        disableSuggestion(tvMobileNumber);
        disableSuggestion(etCompany);
        setupUI(findViewById(R.id.parent));

    }

    @Override
    public void setUp() {

        if (getIntent().getExtras() != null) {
            role = getIntent().getExtras().getInt("role", 88);

        }
        mobileNumber = securePrefManager.getSharedValue(MOBILE);
        disableAutofill();

        tvMobileNumber.setText(mobileNumber);
        details = (ImplementerCompanyModel) getIntent().getExtras().getSerializable("details");

        companyArray.addAll(details.getCompanies());

        submitBtn.setOnClickListener(this);
        addLine2.setOnClickListener(this);
        etCompany.setOnClickListener(this);
        editImage.setOnClickListener(this);
        profileImage.setOnClickListener(this);
        etPromotion.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 10) {
                    hideKeyboard(ImplementerRegistration.this);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }

    @TargetApi(Build.VERSION_CODES.O)
    private void disableAutofill() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            getWindow().getDecorView().setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.submit_btn:
                /*hit User Registration api*/

                submitForm();

                break;
            case R.id.line2:
                openPlacePicker();
                break;
            case R.id.etCompany:
                CompanyListDialog.show(this, companyArray, item -> {
                    etCompany.setText(item.getCompanyName());
                    this.companyId = item.getCompanyId();

                });

                break;
            case R.id.editImage:
            case R.id.profileImage:
                if (checkPermissionAPI23())
                    getImagePicker();
                break;

        }

    }

    private boolean checkPermissionAPI23() {
        boolean isPermissionRequired = false;
        List<String> permissionArray = new ArrayList<>();

        if (Permissions.getInstance().hasCameraPermission(this).permissions.size() > 0) {
            permissionArray.add(Manifest.permission.CAMERA);
            isPermissionRequired = true;
        }

        if (Permissions.getInstance().checkReadWritePermissions(this).permissions.size() > 0) {
            isPermissionRequired = true;
            permissionArray.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            permissionArray.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (isPermissionRequired) {
            Permissions.getInstance().addPermission(permissionArray).askForPermissions(this, REQUEST_CODE_FOR_PERMISSION);
            return false;
        }
        return true;
    }

    private void getImagePicker() {
        CharSequence[] options = getResources().getStringArray(R.array.image_picker_options);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(options, (dialog, which) -> {
            switch (which) {
                case 0:
                    getImageFromCamera();
                    break;
                case 1:
                    getImageFromGallery();
                    break;
                default:
                    break;
            }
        });
        builder.show();
    }


    private void getImageFromGallery() {
        mImagePicker.pickImage();

    }


    private void getImageFromCamera() {

        mCameraImagePicker.pickImage();

    }

    @Override
    public void onImagesChosen(List<ChosenImage> list) {
        mPhotoFile = new File(list.get(0).getOriginalPath());
        Bitmap bitmap = Utils.compressImage(mPhotoFile.getPath());
        profileImage.setImageBitmap(bitmap);


        OutputStream os = null;
        try {
            os = new BufferedOutputStream(new FileOutputStream(mPhotoFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
        try {
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onError(String s) {
        showToast(s);

    }

    private void submitForm() {
        if (!validateName()) {
            return;
        }

        if (!validateAddress1()) {
            return;
        }
        if (!validateAddress2()) {
            return;
        }
        if (!validateCity()) {
            return;
        }
        if (!pincode()) {
            return;
        }
        if (!etEmail.getText().toString().trim().isEmpty()) {
            if (!Utils.isValidEmail(etEmail.getText().toString().trim())) {
                showToast(getString(R.string.email_error));
                return;
            }

        }

        /*Hit Registration API*/
        if (Utils.hasNetwork(this)) {

            ImplementerModel user = new ImplementerModel(etLine1.getText().toString(), addLine2.getText().toString(), city, tvCountry.getText().toString(), "+91", etEmail.getText().toString(), lat, lng, etName.getText().toString(), etPincode.getText().toString(), etPromotion.getText().toString(), role, tvState.getText().toString(), companyId);
            presenter.registerimplenter(this, user, securePrefManager.getSharedValue(TOKEN), mPhotoFile);
            showProgress("Please wait");
        } else {
            showToast(getResources().getString(R.string.internet_error));
        }


    }


    private boolean validateName() {
        if (etName.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.err_msg_name));
            requestFocus(etName);
            return false;
        }


        return true;
    }

    private boolean validateAddress1() {
        if (etLine1.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.err_msg_add));
            requestFocus(etLine1);
            return false;
        } else {
            address1Layout.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateAddress2() {
        if (addLine2.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.err_msg_add));
            requestFocus(addLine2);
            return false;
        } else {
            address2Layout.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateCity() {
        city = tvCity.getText().toString().equalsIgnoreCase("") ? etCity.getText().toString() : tvCity.getText().toString();

        if (city.trim().isEmpty() || city.length() < 3) {
            showToast("Please Add City");
            return false;
        }
        return true;
    }

    private boolean pincode() {
        if (etPincode.getText().toString().trim().isEmpty() || etPincode.getText().length() < 5) {
            showToast(getString(R.string.pincode_err));
            requestFocus(etPincode);
            return false;
        } else {
            pincodeLayout.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void openPlacePicker() {

        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), getString(R.string.google_api));
        }
        List<Place.Field> fields = Arrays.asList(Place.Field.ADDRESS_COMPONENTS, Place.Field.NAME, Place.Field.LAT_LNG);

        // Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).build(Objects.requireNonNull(this));
        startActivityForResult(intent, REQUEST_FOR_PLACE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_FOR_PLACE:
                if (resultCode == RESULT_OK) {
                    hideKeyboard(ImplementerRegistration.this);
                    lowerView.setVisibility(View.VISIBLE);
                    Place place = Autocomplete.getPlaceFromIntent(data);
                    addLine2.setText(place.getName());
                    LatLng latLng = place.getLatLng();
                    lat = String.valueOf(latLng.latitude);
                    lng = String.valueOf(latLng.longitude);
                    try {
                        getCityNameByCoordinates(latLng.latitude, latLng.longitude);
                        Glide.with(this).load(getStaticMapURL(String.valueOf(latLng.latitude), String.valueOf(latLng.longitude))).into(mapImg);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    //  onParamSelected();
                } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                    Status status = Autocomplete.getStatusFromIntent(data);


                } else if (resultCode == RESULT_CANCELED) {

                }
                break;

            case Picker.PICK_IMAGE_DEVICE:
                if (resultCode == RESULT_OK) {
                    mImagePicker.submit(data);
                } else {

                }
                break;

            case Picker.PICK_IMAGE_CAMERA:
                if (resultCode == RESULT_OK) {
                    mCameraImagePicker.submit(data);
                } else {

                }
                break;


        }

    }


// ...

    private void getCityNameByCoordinates(double lat, double lon) throws IOException {

        List<Address> addresses = mGeocoder.getFromLocation(lat, lon, 1);
        if (addresses != null && addresses.size() > 0) {
            tvState.setText(addresses.get(0).getAdminArea());
            //   addLine2.setText(addLine2.getText() + " " + addresses.get(0).getSubLocality());
//            if (addresses.get(0).getSubAdminArea().isEmpty() || addresses.get(0).getSubAdminArea().equalsIgnoreCase("")) {
//                etCity.setVisibility(View.VISIBLE);
//                tvCity.setVisibility(View.GONE);
//            } else {
//                etCity.setVisibility(View.GONE);
//                tvCity.setVisibility(View.VISIBLE);
//                tvCity.setText(addresses.get(0).getSubAdminArea());
//            }

            tvCity.setText(addresses.get(0).getSubAdminArea());

            tvCountry.setText(addresses.get(0).getCountryName());
            etPincode.setText(addresses.get(0).getPostalCode());

        }

    }

    private String getStaticMapURL(String lat, String lng) {

        url = "http://maps.googleapis.com/maps/api/staticmap?center=" + lat + "," + lng + "&zoom=15&size=200x200&markers=color:red|label:S|" + lat + "," + lng + "&sensor=true&key=" + getString(R.string.google_api);
        return url;
    }


    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideKeyboard(ImplementerRegistration.this);
                    etPincode.clearFocus();
                    etEmail.clearFocus();
                    etName.clearFocus();
                    etPromotion.clearFocus();
                    etLine1.clearFocus();
                    return false;
                }
            });
        }
    }

    @Override
    public void onRegister(String userId,String role) {
        securePrefManager.storeSharedValue(AppConstants.REGISTERED, "true");

        hideProgress();
        securePrefManager.storeSharedValue(AppConstants.NAME, etName.getText().toString());
        securePrefManager.storeSharedValue(AppConstants.COMPANYNAME, etCompany.getText().toString());
        securePrefManager.storeSharedValue(AppConstants.COMPANYID, companyId);
        securePrefManager.storeSharedValue(AppConstants.USER_ID, userId);
        securePrefManager.storeSharedValue(ROLE, role);
        Bundle bundle = new Bundle();
        bundle.putString("name", etName.getText().toString());
        bundle.putString("phone", tvMobileNumber.getText().toString());
        bundle.putString("email", etEmail.getText().toString());
        bundle.putString("userID", userId);
        bundle.putString("companyName", etCompany.getText().toString());
        bundle.putString("address", etLine1.getText().toString() + " , " + addLine2.getText().toString() + " , " + tvCity.getText().toString() + " , " + tvState.getText().toString() + " , " + tvCountry.getText().toString() + "," + etPincode.getText().toString());
        bundle.putString("mapUrl", url);

        bundle.putString("isFrom", "implementerFrag");
        bundle.putString("city", tvCity.getText().toString());
        int flag = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;
        goToNextScreen(ImplementerProfile.class, bundle, flag);


    }

    @Override
    public void onFailure(String msg) {
        showToast("Error in Registration");
        hideProgress();
    }

}
