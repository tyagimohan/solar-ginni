package com.solarginni.Implementer.SetPricingModule;

import com.solarginni.ComponentModel;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.SetComponentModel;
import com.solarginni.DBModel.SetPriceModel;

public interface SetPriceContract {

    interface  View{
        void onSetPrice();
        void onFailure(String msg);
        void showPriceList(CommomResponse commomResponse);
        void showComponentList(CommomResponse commomResponse);
        void showDistrict(CommomResponse commomResponse);
        void showComponentMake(ComponentModel commomResponse);
        void showComponentDetail(CommomResponse commomResponse);

    }

    interface  Presenter{
        void setPrice(String token, SetPriceModel model,Integer id);
        void setComponent(String token, SetComponentModel model, Integer id);
        void getPrice(String token,String state,String year,String companyId);
        void getComponentList(String token,String state,String year,String companyId);
        void getDistrict(String token);
        void getComponentDetails(String token,String id );
        void getComponentMake(String token);
    }
    interface Interector {
        void setPrice(String token,SetPriceModel model,Integer id,OnInteraction listenr);
        void getPrice(String token,String state,String year,String companyId,OnInteraction listenr);
        void setComponent(String token, SetComponentModel model, Integer id,OnInteraction listenr);
        void getComponentList(String token,String state,String year,String companyId,OnInteraction listenr);

        void getDistrict(String token,OnInteraction listenr);
        void getComponentDetails(String token,String id, OnInteraction listenr);
        void getComponentMake(String token,OnInteraction listenr);

    }

    interface OnInteraction{
        void onSetPrice();
        void onFailure(String msg);
        void showPriceList(CommomResponse commomResponse);
        void showDistrict(CommomResponse commomResponse);
        void showComponentList(CommomResponse commomResponse);
        void showComponentDetail(CommomResponse commomResponse);

        void showComponentMake(ComponentModel commomResponse);



    }
}
