package com.solarginni.Implementer.SetPricingModule;

import com.solarginni.ComponentModel;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.SetComponentModel;
import com.solarginni.DBModel.SetPriceModel;
import com.solarginni.network.RestClient;

public class SetPricingPresenter implements SetPriceContract.OnInteraction,SetPriceContract.Presenter {
    private SetPriceContract.View view;
    private SetPriceInterector interector;

    public SetPricingPresenter(SetPriceContract.View view) {
        this.view = view;
        interector= new SetPriceInterector(RestClient.getRetrofitBuilder());
    }

    @Override
    public void setPrice(String token, SetPriceModel model,Integer id) {
        interector.setPrice(token,model,id,this);

    }

    @Override
    public void setComponent(String token, SetComponentModel model, Integer id) {
        interector.setComponent(token,model,id,this);

    }

    @Override
    public void getPrice(String token,String state,String year,String companyId) {
        interector.getPrice(token,state,year,companyId,this);

    }

    @Override
    public void getComponentList(String token, String state, String siteType, String companyId) {
        interector.getComponentList(token,state,siteType,companyId,this);

    }

    @Override
    public void getDistrict(String token) {
        interector.getDistrict(token,this);

    }

    @Override
    public void getComponentDetails(String token, String id) {
        interector.getComponentDetails(token,id,this);

    }

    @Override
    public void getComponentMake(String token) {
         interector.getComponentMake(token,this);
    }

    @Override
    public void onSetPrice() {
        view.onSetPrice();

    }

    @Override
    public void onFailure(String msg) {
        view.onFailure(msg);

    }

    @Override
    public void showPriceList(CommomResponse commomResponse) {
        view.showPriceList(commomResponse);

    }

    @Override
    public void showDistrict(CommomResponse commomResponse) {
        view.showDistrict(commomResponse);

    }

    @Override
    public void showComponentList(CommomResponse commomResponse) {
     view.showComponentList(commomResponse);
    }

    @Override
    public void showComponentDetail(CommomResponse commomResponse) {
        view.showComponentDetail(commomResponse);

    }

    @Override
    public void showComponentMake(ComponentModel commomResponse) {
        view.showComponentMake(commomResponse);

    }
}
