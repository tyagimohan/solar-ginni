package com.solarginni.Implementer.SetPricingModule;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.ComponentModel;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.SetComponentModel;
import com.solarginni.DBModel.SetPriceModel;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;

import retrofit2.Retrofit;

public class SetPriceInterector implements SetPriceContract.Interector {
    private Retrofit retrofit;
    private ApiInterface apiInterface;

    public SetPriceInterector(Retrofit retrofit) {
        this.retrofit = retrofit;
        apiInterface = retrofit.create(ApiInterface.class);
    }

    @Override
    public void setPrice(String token, SetPriceModel model, Integer id, SetPriceContract.OnInteraction listenr) {
        apiInterface.setPriceInfo(token, model, String.valueOf(id)).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                listenr.onSetPrice();

            }

            @Override
            public void onError(ApiError error) {
                listenr.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                listenr.onFailure(throwable.getMessage());

            }
        });

    }

    @Override
    public void getPrice(String token, String state, String year, String companyId, SetPriceContract.OnInteraction listenr) {
        apiInterface.getPricing(token, state, year, companyId).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                listenr.showPriceList(response);

            }

            @Override
            public void onError(ApiError error) {
                listenr.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                listenr.onFailure(throwable.getMessage());

            }
        });

    }

    @Override
    public void setComponent(String token, SetComponentModel model, Integer id, SetPriceContract.OnInteraction listenr) {

        apiInterface.setComponent(token, model).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse commomResponse) {
                listenr.onSetPrice();

            }

            @Override
            public void onError(ApiError error) {
                listenr.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                listenr.onFailure(throwable.getMessage());

            }
        });

    }

    @Override
    public void getComponentList(String token, String state, String siteType, String companyId, SetPriceContract.OnInteraction listenr) {
        apiInterface.getComponentList(token,state,siteType,companyId).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse commomResponse) {
                listenr.showComponentList(commomResponse);

            }

            @Override
            public void onError(ApiError error) {
                listenr.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                listenr.onFailure(throwable.getMessage());

            }
        });

    }

    @Override
    public void getDistrict(String token, SetPriceContract.OnInteraction listenr) {
        apiInterface.getDistrict(token).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse commomResponse) {
                listenr.showDistrict(commomResponse);

            }

            @Override
            public void onError(ApiError error) {
                listenr.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                listenr.onFailure(throwable.getMessage());

            }
        });

    }

    @Override
    public void getComponentDetails(String token, String id, SetPriceContract.OnInteraction listenr) {
        apiInterface.getComponentDetails(token,id).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                listenr.showComponentDetail(response);

            }


            @Override
            public void onError(ApiError error) {
                listenr.onFailure(error.getMessage());
            }

            @Override
            public void onFailure(Throwable throwable) {
                listenr.onFailure(throwable.getMessage());

            }
        });
    }

    @Override
    public void getComponentMake(String token, SetPriceContract.OnInteraction listenr) {
        apiInterface.getComponentMake(token).enqueue(new ResponseResolver<ComponentModel>(retrofit) {
            @Override
            public void onSuccess(ComponentModel commomResponse) {
                listenr.showComponentMake(commomResponse);

            }

            @Override
            public void onError(ApiError error) {
                listenr.onFailure(error.getMessage());
            }

            @Override
            public void onFailure(Throwable throwable) {
                listenr.onFailure(throwable.getMessage());

            }
        });
    }
}
