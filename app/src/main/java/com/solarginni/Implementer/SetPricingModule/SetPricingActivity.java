package com.solarginni.Implementer.SetPricingModule;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;

import com.solarginni.Base.BaseActivity;
import com.solarginni.CommonModule.MainActivity;
import com.solarginni.ComponentModel;
import com.solarginni.CustomizeView.districtSearchDialog.DistrictDialog;
import com.solarginni.CustomizeView.districtSearchDialog.DistrictModel;
import com.solarginni.CustomizeView.districtSearchDialog.SelectDistrictListener;
import com.solarginni.DBModel.APIBody.DistrictStateModel;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.PricingModel;
import com.solarginni.DBModel.SetPriceModel;
import com.solarginni.R;
import com.solarginni.Utility.NumberTextWatcherForThousand;
import com.solarginni.Utility.Utils;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static com.solarginni.Utility.AppConstants.BO_MANAGER;
import static com.solarginni.Utility.AppConstants.IMPLEMENTER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.SUPER_IMPLEMENTER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.TOKEN;

public class SetPricingActivity extends BaseActivity implements SetPriceContract.View, View.OnClickListener {

    private EditText month, state, year, price, siteType, siteRange, tvDistrict;
    private Button submitBtn;
    private SetPricingPresenter pricingPresenter;
    private String[] monthArray, stateArray, yearArray, siteTypeArray, siteRangeArray;
    private List<DistrictModel> districtList=new ArrayList<>();
    private Integer id;
    private boolean isForEdit = false;
    private int range = 1;
    private int monthCount = 0;
    private Calendar calendar = Calendar.getInstance();
    private DistrictStateModel districtStateModel;
    private List<String> selectedDistrict= new ArrayList<>();
    private CheckBox cbCapex,cbResco,cbFinance;

    @Override
    public int getLayout() {
        return R.layout.pricing_setup;
    }

    @Override
    public void initViews() {
        month = findViewById(R.id.month);
        cbCapex = findViewById(R.id.capex);
        cbResco = findViewById(R.id.resco);
        cbFinance = findViewById(R.id.finance);
        state = findViewById(R.id.state);
        year = findViewById(R.id.Year);
        price = findViewById(R.id.pricing);
        tvDistrict = findViewById(R.id.district);
        siteRange = findViewById(R.id.siteRange);
        siteType = findViewById(R.id.siteType);
        submitBtn = findViewById(R.id.confirmBtn);

        disableSuggestion(month);
        disableSuggestion(siteType);
        disableSuggestion(siteRange);
        disableSuggestion(year);
        disableSuggestion(state);
        disableSuggestion(price);
        disableSuggestion(tvDistrict);

        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });
        pricingPresenter = new SetPricingPresenter(this);

    }

    @Override
    public void setUp() {
        monthArray = getResources().getStringArray(R.array.months_array);
        stateArray = getResources().getStringArray(R.array.state);
        yearArray = getResources().getStringArray(R.array.year);
        siteTypeArray = getResources().getStringArray(R.array.sitetype);
        siteRangeArray = getResources().getStringArray(R.array.siteRange);




        if (Utils.hasNetwork(this)) {
            showProgress("Please wait...");
            pricingPresenter.getDistrict(securePrefManager.getSharedValue(TOKEN));
        } else {
            showToast("Please check internet");

        }
        if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(IMPLEMENTER_ROLE_ID) | securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SUPER_IMPLEMENTER_ROLE_ID)){
            month.setOnClickListener(this);
            state.setOnClickListener(this);
            siteType.setOnClickListener(this);
            submitBtn.setOnClickListener(this);
            siteRange.setOnClickListener(this);
            year.setOnClickListener(this);
            tvDistrict.setOnClickListener(this);
            price.addTextChangedListener(new NumberTextWatcherForThousand(price));
        }
        else {
            month.setFocusableInTouchMode(false);
            state.setFocusableInTouchMode(false);
            siteType.setFocusableInTouchMode(false);
            siteRange.setFocusableInTouchMode(false);
            year.setFocusableInTouchMode(false);
            submitBtn.setVisibility(View.GONE);
            year.setFocusableInTouchMode(false);
            tvDistrict.setFocusableInTouchMode(false);
            price.setFocusableInTouchMode(false);
        }




    }

    @Override
    public void onSetPrice() {
        hideProgress();
        showToast("Price details successfully recorded");
        setResult(Activity.RESULT_OK);
        finish();

    }

    @Override
    public void onFailure(String msg) {
        hideProgress();

        if (msg.equalsIgnoreCase("invalid token")) {
            securePrefManager.clearAppsAllPrefs();
            startActivity(new Intent(SetPricingActivity.this, MainActivity.class));
            finish();
        } else {

        }

        showToast(msg);

    }

    @Override
    public void showPriceList(CommomResponse commomResponse) {


    }

    @Override
    public void showComponentList(CommomResponse commomResponse) {

    }

    @Override
    public void showDistrict(CommomResponse commomResponse) {
        hideProgress();
        districtStateModel = commomResponse.toResponseModel(DistrictStateModel.class);
        stateArray = new String[districtStateModel.stateLov.size()];
        for (int i = 0; i < districtStateModel.stateLov.size(); i++) {
            stateArray[i] = districtStateModel.stateLov.get(i).state;
        }
        if (getIntent().getExtras() != null) {
            Bundle bundle = getIntent().getExtras();
            PricingModel.PriceInfo model = (PricingModel.PriceInfo) bundle.getSerializable("data");
            isForEdit = getIntent().getExtras().getBoolean("isForEdit", false);
            month.setText(model.getMonth());
            siteType.setText(model.getSiteType());
            for (int i = 0; i < monthArray.length; i++) {
                if (monthArray[i].equalsIgnoreCase(model.getMonth())) {
                    monthCount = i;
                    break;
                }

            }
            cbCapex.setChecked(false);

            for (String value:model.pricingModel){

                switch (value){
                    case "CAPEX":
                        cbCapex.setChecked(true);

                        break;
                    case "RESCO":
                        cbResco.setChecked(true);

                        break;
                    case "FINANCE":
                        cbFinance.setChecked(true);

                        break;
                }
            }

            Long longval = model.getPrice().longValue();

            DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
            formatter.applyPattern("#,##,##,###");
            String formattedString = formatter.format(longval);
            if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(BO_MANAGER))
            price.setText("*********");
            else
            price.setText(formattedString);
            state.setText(model.getState());

            for (int i=0;i<districtStateModel.stateLov.size();i++){
                if (districtStateModel.stateLov.get(i).state.equalsIgnoreCase(model.getState())){
                    for (String name:districtStateModel.stateLov.get(i).districts){
                    if (model.districtList.contains(name)){
                        DistrictModel mDistrictModel= new DistrictModel(name,true);
                        districtList.add(mDistrictModel);
                    }
                    else {
                        DistrictModel mDistrictModel= new DistrictModel(name,false);
                        districtList.add(mDistrictModel);
                    }
                    }
                }
            }


            year.setText(String.valueOf(model.getYear()));
            if (model.getRange() == 1) {
                siteRange.setText("1.00-10.00");
                range = 1;
            } else if (model.getRange() == 2) {
                siteRange.setText("10.01-100.00");
                range = 2;
            } else {
                siteRange.setText(" >100.01");
                range = 3;
            }

            id = model.getId();

            String districtStr="";


            selectedDistrict.addAll(model.districtList);

            for (String dist: model.districtList){
                districtStr=districtStr+","+dist;
            }
            tvDistrict.setText(districtStr.substring(1));
        }


    }

    @Override
    public void showComponentMake(ComponentModel commomResponse) {

    }

    @Override
    public void showComponentDetail(CommomResponse commomResponse) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.month:
                showAlertWithListForMonth(monthArray, R.string.month_title, month);
                break;
            case R.id.Year:
                showAlertWithList(yearArray, R.string.year_title, year);
                break;
            case R.id.siteType:
                showAlertWithList(siteTypeArray, R.string.select_site, siteType);

                break;
            case R.id.state:
                showAlertWithListForState(stateArray, R.string.state_title, state);

                break;
            case R.id.siteRange:
                showAlertWithListForRange(siteRangeArray, R.string.range_title, siteRange);

                break;
            case R.id.district:

                if (state.getText().toString().length()==0){
                    showToast("Please select state first");
                }
                else {
                    DistrictDialog.show(this, districtList, true,new SelectDistrictListener() {
                        @Override
                        public void onSelectDistrict(ArrayList<DistrictModel> districtList) {
                            Log.e("dsdsdfg",""+districtList.toString());


                            String districtStr="";
                            selectedDistrict.clear();
                            for (DistrictModel district : districtList){
                                selectedDistrict.add(district.district);
                                districtStr=districtStr+","+district.district;
                            }
                            tvDistrict.setText(districtStr.substring(1));

                        }

                        @Override
                        public void onUnSelectDistrict(ArrayList<DistrictModel> districtList) {
                            Log.e("dsdsdfg",""+districtList.toString());
                            selectedDistrict.clear();
                            tvDistrict.setText("");
                        }
                    });

                }

                break;

            case R.id.confirmBtn:

                if (siteType.getText().length() == 0 || month.getText().length() == 0 || price.getText().length() == 0 || state.getText().length() == 0 || year.getText().length() == 0 || siteRange.getText().length() == 0|| tvDistrict.getText().length() == 0) {
                    showToast("Please Fill the details");
                } else if (monthCount < calendar.get(Calendar.MONTH) & year.getText().toString().equalsIgnoreCase("2021")) {
                    showToast("Your Selected month either from past or not a valid month");
                    return;
                } else {
                    if (Utils.hasNetwork(this)) {
                        showProgress("Please wait...");
                        hideKeyboard(this);
                        price.clearFocus();
                        SetPriceModel model = new SetPriceModel();
                        model.setMonth(month.getText().toString());
                        model.setSiteType(siteType.getText().toString());
                        model.setPrice(price.getText().toString().replace(",", ""));
                        model.setState(state.getText().toString());
                        model.setYear(year.getText().toString());
                        model.setSiteRange(range);
                        model.district.addAll(selectedDistrict);
                        if (cbFinance.isChecked()){
                            model.pricingModel.add(cbFinance.getText().toString());
                        }
                        if (cbResco.isChecked()) {
                            model.pricingModel.add(cbResco.getText().toString());
                        }
                         if (cbCapex.isChecked()) {
                            model.pricingModel.add(cbCapex.getText().toString());
                        }

                        if (isForEdit) {
                            pricingPresenter.setPrice(securePrefManager.getSharedValue(TOKEN), model, id);

                        } else {
                            pricingPresenter.setPrice(securePrefManager.getSharedValue(TOKEN), model, -1);

                        }


                    } else {
                        showToast("Please Check Internet");
                    }
                }


                break;
        }

    }

    public void showAlertWithListForRange(final String[] dataArray, final @StringRes int title, final TextView textView) {
        hideKeyboard(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(title));
        builder.setItems(dataArray, (dialog, item) -> {
            textView.setText(dataArray[item]);
            range = item + 1;
        });
        AlertDialog alerts = builder.create();
        alerts.show();
    }

    private void showAlertWithListForState(final String[] dataArray, final @StringRes int title, final TextView textView) {
        hideKeyboard(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(title));
        builder.setItems(dataArray, (dialog, item) -> {
            textView.setText(dataArray[item]);
            districtList.clear();
            tvDistrict.setText("");

            for (String district: districtStateModel.stateLov.get(item).districts){

                DistrictModel districtModel = new DistrictModel(district,false);

               districtList.add(districtModel);

            }


        });
        AlertDialog alerts = builder.create();
        alerts.show();
    }

    public void showAlertWithListForMonth(final String[] dataArray, final @StringRes int title, final TextView textView) {
        hideKeyboard(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(title));
        builder.setItems(dataArray, (dialog, item) -> {
            textView.setText(dataArray[item]);
            monthCount = item;
        });
        AlertDialog alerts = builder.create();
        alerts.show();
    }
}
