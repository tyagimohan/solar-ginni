package com.solarginni.Implementer.SetPricingModule;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;

import com.solarginni.Base.BaseActivity;
import com.solarginni.CommonModule.MainActivity;
import com.solarginni.ComponentModel;
import com.solarginni.CustomizeView.CompanyListDialog;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.CompanyMakeModel;
import com.solarginni.DBModel.PriceComponentModel;
import com.solarginni.DBModel.PricingModel;
import com.solarginni.DBModel.SetComponentModel;
import com.solarginni.R;
import com.solarginni.Utility.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.solarginni.Utility.AppConstants.IMPLEMENTER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.SUPER_IMPLEMENTER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.TOKEN;

public class SetComponentActivity extends BaseActivity implements SetPriceContract.View, View.OnClickListener {

    private SetPricingPresenter pricingPresenter;
    private EditText structureType, inverterMake, panelMake, panelType, siteType, state, warranty, acCableBrand,
            acCableLength, dcCableLength, dcCableBrand, earthingKit, lightingArrestor, bidirectionalMeter, distributionBox, remark;
    private Switch sensorToggle, washingToggle, zeroTouchToggle, monitoringToggle;
    private List<CompanyMakeModel> panelMakeArray = new ArrayList<>();
    private List<CompanyMakeModel> inverterMakeArray = new ArrayList<>();
    private List<CompanyMakeModel> batteryArray = new ArrayList<>();
    private String[] stateArray, structureTypeArray, panelTypeArray, siteTypeArray, distributionTypeArray, earthingKitArray, acCableBrandArray, dcCableBrandArray, lighthingKit, bidirectionMeterArray, lengthArray;
    private String panelMakeId;
    private String inverterMakeId;
    private Integer id;
    private boolean isForEdit = false;

    @Override
    public int getLayout() {
        return R.layout.set_component;
    }

    @Override
    public void initViews() {
        state = findViewById(R.id.state);
        monitoringToggle = findViewById(R.id.monitoringToggle);
        remark = findViewById(R.id.remark);
        zeroTouchToggle = findViewById(R.id.zeroTouchToggle);
        washingToggle = findViewById(R.id.washingToggle);
        sensorToggle = findViewById(R.id.sensorToggle);
        structureType = findViewById(R.id.structureType);
        warranty = findViewById(R.id.warranty);
        acCableBrand = findViewById(R.id.acCableBrand);
        acCableLength = findViewById(R.id.acCableLength);
        dcCableBrand = findViewById(R.id.dcCableBrand);
        dcCableLength = findViewById(R.id.dcCableLength);
        earthingKit = findViewById(R.id.earthingKit);
        lightingArrestor = findViewById(R.id.lightingArrestor);
        bidirectionalMeter = findViewById(R.id.bidirectionalMeter);
        distributionBox = findViewById(R.id.distributionBox);
        inverterMake = findViewById(R.id.inverterMake);
        panelMake = findViewById(R.id.panelMake);
        panelType = findViewById(R.id.panelType);
        siteType = findViewById(R.id.siteType);
        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });

    }

    @Override
    public void setUp() {

        pricingPresenter = new SetPricingPresenter(this);


        if (getIntent().getExtras() != null & getIntent().getExtras().getBoolean("isFromPrice", false)) {

            PricingModel.PriceInfo model = (PricingModel.PriceInfo) getIntent().getExtras().getSerializable("data");

            state.setText(model.getState());
            id = model.getId();
            siteType.setText(model.getSiteType());

            if (model.componentCreated) {
                if (Utils.hasNetwork(this)) {
                    showProgress("Please wait...");
                    pricingPresenter.getComponentDetails(securePrefManager.getSharedValue(TOKEN), String.valueOf(model.getId()));


                } else {
                    showToast("Please wait...");

                }
            }

        } else if (getIntent().getExtras() != null) {
            Bundle bundle = getIntent().getExtras();

            PriceComponentModel.PriceComponentDetail model = (PriceComponentModel.PriceComponentDetail) bundle.getSerializable("data");
            structureType.setText(model.structureType);
            state.setText(model.state);
            siteType.setText(model.siteType);
            panelType.setText(model.panelType);
            panelMake.setText(model.panelCompanyName);
            inverterMake.setText(model.inverterCompanyName);
            panelMakeId = model.panelMake;
            inverterMakeId = model.inverterMake;
            id = model.priceId;
            structureType.setText(model.structureType);
            acCableBrand.setText(model.acCableBrand);
            acCableLength.setText(model.acCableLength);
            dcCableBrand.setText(model.dcCableBrand);
            dcCableLength.setText(model.dcCableLength);
            warranty.setText("" + model.serviceWarranty);
            remark.setText(model.remark);
            earthingKit.setText(model.earthingKit);
            lightingArrestor.setText(model.lightingArrestor);
            bidirectionalMeter.setText(model.bidirectionalMeter);
            distributionBox.setText(model.distributionBox);
            sensorToggle.setChecked(model.irradianceSensor);
            zeroTouchToggle.setChecked(model.zeroTouch);
            monitoringToggle.setChecked(model.monitoring);
            washingToggle.setChecked(model.washingSystem);
            isForEdit = getIntent().getExtras().getBoolean("isForEdit", false);

        }


        if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(IMPLEMENTER_ROLE_ID) | securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SUPER_IMPLEMENTER_ROLE_ID)) {
            structureType.setOnClickListener(this);
            panelMake.setOnClickListener(this);
            panelType.setOnClickListener(this);
            inverterMake.setOnClickListener(this);
            acCableBrand.setOnClickListener(this);
            acCableLength.setOnClickListener(this);
            dcCableBrand.setOnClickListener(this);
            dcCableLength.setOnClickListener(this);
            earthingKit.setOnClickListener(this);
            lightingArrestor.setOnClickListener(this);
            bidirectionalMeter.setOnClickListener(this);
            remark.setFocusableInTouchMode(true);
            monitoringToggle.setClickable(true);
            washingToggle.setClickable(true);
            sensorToggle.setClickable(true);
            zeroTouchToggle.setClickable(true);
            distributionBox.setOnClickListener(this);
            findViewById(R.id.confirmBtn).setOnClickListener(this);
            findViewById(R.id.confirmBtn).setVisibility(View.VISIBLE);
        }

        if (Utils.hasNetwork(this)) {
            showProgress("Please wait...");
            pricingPresenter.getComponentMake(securePrefManager.getSharedValue(TOKEN));


        } else {
            showToast("Please wait...");

        }


    }

    @Override
    public void onSetPrice() {
        hideProgress();
        showToast("Component details successfully recorded");
        setResult(Activity.RESULT_OK);
        finish();
    }

    @Override
    public void onFailure(String msg) {
        hideProgress();

        if (msg.equalsIgnoreCase("invalid token")) {
            securePrefManager.clearAppsAllPrefs();
            startActivity(new Intent(SetComponentActivity.this, MainActivity.class));
            finish();
        } else {

        }

        showToast(msg);

    }

    @Override
    public void showPriceList(CommomResponse commomResponse) {

    }

    @Override
    public void showComponentList(CommomResponse commomResponse) {

    }

    @Override
    public void showDistrict(CommomResponse commomResponse) {

    }

    @Override
    public void showComponentMake(ComponentModel commomResponse) {
        hideProgress();

        stateArray = commomResponse.data.state.toArray(new String[0]);
        panelMakeArray = commomResponse.data.panel;
        inverterMakeArray = commomResponse.data.inverter;
        batteryArray = commomResponse.data.battery;
        structureTypeArray = commomResponse.data.structureType.toArray(new String[0]);
        siteTypeArray = getResources().getStringArray(R.array.sitetype);
        lengthArray = getResources().getStringArray(R.array.lengthArray);
        panelTypeArray = commomResponse.data.panelType.toArray(new String[0]);
        acCableBrandArray = commomResponse.data.ac_cable_brand.toArray(new String[0]);
        dcCableBrandArray = commomResponse.data.dc_cable_brand.toArray(new String[0]);
        earthingKitArray = commomResponse.data.earthing_kit.toArray(new String[0]);
        lighthingKit = commomResponse.data.lightning_arrestor.toArray(new String[0]);
        bidirectionMeterArray = commomResponse.data.bidirectional_meter.toArray(new String[0]);
        distributionTypeArray = commomResponse.data.distribution_box.toArray(new String[0]);


    }

    @Override
    public void showComponentDetail(CommomResponse commomResponse) {
        hideProgress();

        PriceComponentModel model = commomResponse.toResponseModel(PriceComponentModel.class);

        state.setText(model.priceComponentDetail.state);
        siteType.setText(model.priceComponentDetail.siteType);
        panelType.setText(model.priceComponentDetail.panelType);
        panelMake.setText(model.priceComponentDetail.panelCompanyName);
        inverterMake.setText(model.priceComponentDetail.inverterCompanyName);
        panelMakeId = model.priceComponentDetail.panelMake;
        inverterMakeId = model.priceComponentDetail.inverterMake;
        id = model.priceComponentDetail.priceId;
        structureType.setText(model.priceComponentDetail.structureType);
        acCableBrand.setText(model.priceComponentDetail.acCableBrand);
        acCableLength.setText(model.priceComponentDetail.acCableLength);
        dcCableBrand.setText(model.priceComponentDetail.dcCableBrand);
        dcCableLength.setText(model.priceComponentDetail.dcCableLength);
        earthingKit.setText(model.priceComponentDetail.earthingKit);
        lightingArrestor.setText(model.priceComponentDetail.lightingArrestor);
        bidirectionalMeter.setText(model.priceComponentDetail.bidirectionalMeter);
        warranty.setText("" + model.priceComponentDetail.serviceWarranty);
        remark.setText(model.priceComponentDetail.remark);
        distributionBox.setText(model.priceComponentDetail.distributionBox);
        sensorToggle.setChecked(model.priceComponentDetail.irradianceSensor);
        zeroTouchToggle.setChecked(model.priceComponentDetail.zeroTouch);
        monitoringToggle.setChecked(model.priceComponentDetail.monitoring);
        washingToggle.setChecked(model.priceComponentDetail.washingSystem);
        structureType.setText(model.priceComponentDetail.structureType);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.siteType:
                showAlertWithList(siteTypeArray, "Please Select SiteType", siteType);
                break;
            case R.id.panelType:
                showAlertWithList(panelTypeArray, "Please Select Panel Type", panelType);
                break;
            case R.id.acCableBrand:
                showAlertWithList(acCableBrandArray, "Please Select AC Cable Brand", acCableBrand);
                break;
            case R.id.dcCableBrand:
                showAlertWithList(dcCableBrandArray, "Please Select DC Cable Brand", dcCableBrand);
                break;
            case R.id.dcCableLength:
                showAlertWithList(lengthArray, "Please Select DC Cable Length", dcCableLength);
                break;
            case R.id.acCableLength:
                showAlertWithList(lengthArray, "Please Select AC Cable Length", acCableLength);
                break;
            case R.id.bidirectionalMeter:
                showAlertWithList(bidirectionMeterArray, "Please Select Bi-Directional Meter", bidirectionalMeter);
                break;
            case R.id.earthingKit:
                showAlertWithList(earthingKitArray, "Please Select Earthing Kit", earthingKit);
                break;
            case R.id.lightingArrestor:
                showAlertWithList(lighthingKit, "Please Select Lighting Arrestor", lightingArrestor);
                break;
            case R.id.distributionBox:
                showAlertWithList(distributionTypeArray, "Please Select Distribution Box", distributionBox);
                break;
            case R.id.panelMake:
                CompanyListDialog.show(this, panelMakeArray, item -> {
                    panelMake.setText(item.getCompanyName());
                    panelMakeId = item.getCompanyId();

                });
                break;
            case R.id.inverterMake:
                CompanyListDialog.show(this, inverterMakeArray, item -> {
                    inverterMake.setText(item.getCompanyName());
                    inverterMakeId = item.getCompanyId();

                });
                break;
            case R.id.structureType:
                showAlertWithList(structureTypeArray, "Please Select Structure Type", structureType);
                break;
            case R.id.state:

                showAlertWithList(stateArray, "Please Select State", state);
                break;
            case R.id.confirmBtn:

                if (state.getText().length() == 0 | panelMake.getText().length() == 0 | panelType.getText().length() == 0 | inverterMake.getText().length() == 0 | siteType.getText().length() == 0 | structureType.getText().length() == 0
                        | warranty.getText().length() == 0 | remark.getText().length() == 0 | acCableBrand.getText().length() == 0 | acCableLength.getText().length() == 0 | dcCableLength.getText().length() == 0 | dcCableBrand.getText().length() == 0
                        | earthingKit.getText().length() == 0 | lightingArrestor.getText().length() == 0 | bidirectionalMeter.getText().length() == 0 | distributionBox.getText().length() == 0) {

                    showToast("Please Fill the details");
                    return;
                } else {
                    SetComponentModel model = new SetComponentModel();
                    if (Utils.hasNetwork(this)) {
                        showProgress("Please wait...");
                        hideKeyboard(this);
                        model.inverterMake = inverterMakeId;
                        model.priceId = id;
                        model.structureType = structureType.getText().toString();
                        model.irradianceSensor = sensorToggle.isChecked();
                        model.zeroTouch = zeroTouchToggle.isChecked();
                        model.acCableBrand = acCableBrand.getText().toString();
                        model.acCableLength = acCableLength.getText().toString();
                        model.dcCableLength = dcCableLength.getText().toString();
                        model.dcCableBrand = dcCableBrand.getText().toString();
                        model.earthingKit = earthingKit.getText().toString();
                        model.bidirectionalMeter = bidirectionalMeter.getText().toString();
                        model.remark = remark.getText().toString();
                        model.distributionBox = distributionBox.getText().toString();
                        model.monitoring = monitoringToggle.isChecked();
                        model.washingSystem = washingToggle.isChecked();
                        model.serviceWarranty = Integer.valueOf(warranty.getText().toString());
                        model.panelMake = panelMakeId;
                        model.lightingArrestor = lightingArrestor.getText().toString();
                        model.panelType = panelType.getText().toString();
                        model.structureType = structureType.getText().toString();
                        pricingPresenter.setComponent(securePrefManager.getSharedValue(TOKEN), model, id);


                    } else {
                        showToast("Please Check Internet");
                    }
                }

                break;
        }

    }
}
