package com.solarginni.Implementer.SetPricingModule;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.solarginni.Base.BaseActivity;
import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.ComponentModel;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.PriceComponentModel;
import com.solarginni.DBModel.PricingModel;
import com.solarginni.R;
import com.solarginni.Utility.Utils;

import static com.solarginni.Utility.AppConstants.BO_MANAGER;
import static com.solarginni.Utility.AppConstants.IMPLEMENTER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.SG_ADMIN;
import static com.solarginni.Utility.AppConstants.SUPER_IMPLEMENTER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.TOKEN;

public class PriceListActivity extends BaseActivity implements SetPriceContract.View, View.OnClickListener {
    SetPricingPresenter pricingPresenter;
    private ImageView addPriceComponet;
    private TextView state, year, siteType, componentstate;
    private LinearLayout componentFilter, priceFilter;
    private Button setPrice, setComponent;

    private int REQUEST_FOR_SET = 3;
    private int REQUEST_FOR_COMPONENT = 4;

    private String[] yearArray;
    private RecyclerView recyclerView;
    private String companyId = "";


    @Override
    public int getLayout() {
        return R.layout.price_list_activity;
    }

    @Override
    public void initViews() {
        recyclerView = findViewById(R.id.recyclerView);
        state = findViewById(R.id.state);
        componentstate = findViewById(R.id.componentstate);
        siteType = findViewById(R.id.siteType);
        addPriceComponet = findViewById(R.id.addPriceComponet);
        setComponent = findViewById(R.id.setComponent);
        setPrice = findViewById(R.id.setPrice);
        year = findViewById(R.id.year);
        componentFilter = findViewById(R.id.componentFilter);
        priceFilter = findViewById(R.id.priceFilter);
        yearArray = getResources().getStringArray(R.array.year);
        state.setOnClickListener(this);
        year.setOnClickListener(this);
        setComponent.setOnClickListener(this);
        setPrice.setOnClickListener(this);
        siteType.setOnClickListener(this);
        componentstate.setOnClickListener(this);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new BaseRecyclerAdapter.DividerItemDecoration(this, RecyclerView.HORIZONTAL));

        if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SG_ADMIN) | securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(BO_MANAGER)) {
            addPriceComponet.setVisibility(View.GONE);
        }

        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });

        companyId = getIntent().getExtras().getString("companyId", "");


        addPriceComponet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (priceFilter.getVisibility() == View.VISIBLE) {
                    Intent intent = new Intent(PriceListActivity.this, SetPricingActivity.class);
                    startActivityForResult(intent, REQUEST_FOR_SET);
                }


            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_FOR_SET) {
            if (resultCode == RESULT_OK) {
                state.setText("All");
                year.setText("2021");
                pricingPresenter.getPrice(securePrefManager.getSharedValue(TOKEN), state.getText().toString(), year.getText().toString(), companyId);
            }


        } else if (requestCode == REQUEST_FOR_COMPONENT) {
            if (resultCode == RESULT_OK) {
                setComponent.performClick();
            }


        }
    }

    @Override
    public void setUp() {
        pricingPresenter = new SetPricingPresenter(this);

        if (Utils.hasNetwork(this)) {

            showProgress("Please wait...");
            pricingPresenter.getPrice(securePrefManager.getSharedValue(TOKEN), "All", "2021", companyId);
        } else {
            showToast("Please check Internet");

        }


    }

    @Override
    public void onSetPrice() {

    }

    @Override
    public void onFailure(String msg) {
        hideProgress();
        showToast(msg);

    }

    @Override
    public void showPriceList(CommomResponse commomResponse) {
        hideProgress();
        PricingModel model = commomResponse.toResponseModel(PricingModel.class);
        PricingListAdpter adpter = new PricingListAdpter(this);
        adpter.addAll(model.getPriceInfo());
        adpter.setOnClickListener(listener);
        adpter.setOnLongClickListener(itemClick);
        recyclerView.setAdapter(adpter);

    }

    @Override
    public void showComponentList(CommomResponse commomResponse) {
        hideProgress();
        PriceComponentModel model = commomResponse.toResponseModel(PriceComponentModel.class);
        ComponentListAdapter adpter = new ComponentListAdapter(this);
        adpter.addAll(model.priceComponentDetails);
        adpter.setOnClickListener(componentListener);
        recyclerView.setAdapter(adpter);
    }

    @Override
    public void showDistrict(CommomResponse commomResponse) {

    }

    @Override
    public void showComponentMake(ComponentModel commomResponse) {

    }

    @Override
    public void showComponentDetail(CommomResponse commomResponse) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.state:
                showAlertWithList(getResources().getStringArray(R.array.stateAll), R.string.state_title, state);
                break;
            case R.id.componentstate:
                showAlertWithList(getResources().getStringArray(R.array.stateAll), R.string.state_title, componentstate);
                break;
            case R.id.siteType:
                showAlertWithList(getResources().getStringArray(R.array.sitetypeAll), R.string.site_title, siteType);
                break;
            case R.id.year:
                showAlertWithList(yearArray, R.string.year_title, year);
                break;
            case R.id.setPrice:
                if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SG_ADMIN) | securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(BO_MANAGER)) {
                    addPriceComponet.setVisibility(View.GONE);
                }else {
                    addPriceComponet.setVisibility(View.VISIBLE);
                }
                priceFilter.setVisibility(View.VISIBLE);
                setPrice.setBackground(getDrawable(R.drawable.orange_button));
                setComponent.setTextColor(getResources().getColor(R.color.colortheme));
                setPrice.setTextColor(getResources().getColor(R.color.white));
                setComponent.setBackground(null);
                state.setText("All");
                year.setText("2021");
                pricingPresenter.getPrice(securePrefManager.getSharedValue(TOKEN), state.getText().toString(), year.getText().toString(), companyId);

                componentFilter.setVisibility(View.GONE);
                break;
            case R.id.setComponent:
                priceFilter.setVisibility(View.GONE);
                addPriceComponet.setVisibility(View.GONE);
                componentFilter.setVisibility(View.VISIBLE);
                setComponent.setBackground(getDrawable(R.drawable.orange_button));
                setPrice.setBackground(null);
                componentstate.setText("All");
                siteType.setText("Residential");
                setPrice.setTextColor(getResources().getColor(R.color.colortheme));
                setComponent.setTextColor(getResources().getColor(R.color.white));
                pricingPresenter.getComponentList(securePrefManager.getSharedValue(TOKEN), "All", siteType.getText().toString(), companyId);

                break;
        }

    }

    public void showAlertWithList(final String[] dataArray, final @StringRes int title, final TextView textView) {
        hideKeyboard(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(title));
        builder.setItems(dataArray, (dialog, item) -> {
            textView.setText(dataArray[item]);
            if (priceFilter.getVisibility() == View.VISIBLE) {
                pricingPresenter.getPrice(securePrefManager.getSharedValue(TOKEN), state.getText().toString(), year.getText().toString(), companyId);

            } else {
                pricingPresenter.getComponentList(securePrefManager.getSharedValue(TOKEN), componentstate.getText().toString(), siteType.getText().toString(), companyId);

            }
        });
        AlertDialog alerts = builder.create();
        alerts.show();
    }

    private PricingListAdpter.OnClick<PricingModel.PriceInfo> listener = (data, position) -> {


        Bundle bundle = new Bundle();
        bundle.putSerializable("data", data);
        bundle.putBoolean("isForEdit", true);
        Intent intent = new Intent(PriceListActivity.this, SetPricingActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, REQUEST_FOR_SET);
        // goToNextScreen(SetPricingActivity.class,bundle);


    };

    private ComponentListAdapter.OnClick<PriceComponentModel.PriceComponentDetail> componentListener = (data, position) -> {

            Bundle bundle = new Bundle();
            bundle.putSerializable("data", data);
            bundle.putBoolean("isForEdit", true);
            Intent intent = new Intent(PriceListActivity.this, SetComponentActivity.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, REQUEST_FOR_COMPONENT);


    };
    private BaseRecyclerAdapter.onItemClick<PricingModel.PriceInfo> itemClick = (data, position, view) -> {
            Bundle bundle = new Bundle();
            bundle.putSerializable("data", data);
            bundle.putBoolean("isFromPrice", true);
            Intent intent = new Intent(PriceListActivity.this, SetComponentActivity.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, REQUEST_FOR_COMPONENT);
    };
}
