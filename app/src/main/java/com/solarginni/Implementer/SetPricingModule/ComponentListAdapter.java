package com.solarginni.Implementer.SetPricingModule;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.DBModel.PriceComponentModel;
import com.solarginni.R;

public class ComponentListAdapter extends BaseRecyclerAdapter<PriceComponentModel.PriceComponentDetail, ComponentListAdapter.Holder> {


    public ComponentListAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public int getLayout(int viewType) {
        return R.layout.component_iten;
    }

    @Override
    public Holder getViewHolder(View view, int viewType) {
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {

        holder.panelType.setText(getItem(i).panelType);
        holder.siteType.setText(getItem(i).siteType);
        holder.state.setText(getItem(i).state);
        holder.structureType.setText(getItem(i).structureType);

//        if (getItem(i).batteryLogo!=null){
//            ImageLoader.getInstance().displayImage(getItem(i).batteryLogo,holder.batteryMake);
//
//        }
//        else {
//
//        }
        holder.zeroTouchToggle.setChecked(getItem(i).zeroTouch);
        if (getItem(i).inverterLogo!=null){
            ImageLoader.getInstance().displayImage(getItem(i).inverterLogo,holder.inverterMake);
        }
        else {

        }
        if (getItem(i).panelLogo != null) {
            ImageLoader.getInstance().displayImage(getItem(i).panelLogo,holder.panelMake);

        } else {

        }

    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView structureType, panelType, state, siteType;
        public ImageView panelMake,inverterMake;
        public Switch zeroTouchToggle;


        public Holder(@NonNull View itemView) {
            super(itemView);

            structureType = itemView.findViewById(R.id.structureType);
            state = itemView.findViewById(R.id.state);
            siteType = itemView.findViewById(R.id.siteType);
            panelType = itemView.findViewById(R.id.panelType);
            zeroTouchToggle = itemView.findViewById(R.id.zeroTouchToggle);
            inverterMake = itemView.findViewById(R.id.inverterMake);
//            batteryMake = itemView.findViewById(R.id.batteryMake);
            panelMake = itemView.findViewById(R.id.panelMake);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            getListener().onClick(getItem(getAdapterPosition()), getAdapterPosition());

        }
    }
}
