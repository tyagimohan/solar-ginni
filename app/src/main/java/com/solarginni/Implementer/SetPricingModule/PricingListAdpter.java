package com.solarginni.Implementer.SetPricingModule;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.DBModel.PricingModel;
import com.solarginni.R;
import com.solarginni.Utility.SecurePrefManager;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import static com.solarginni.Utility.AppConstants.BO_MANAGER;
import static com.solarginni.Utility.AppConstants.ROLE;

public class PricingListAdpter extends BaseRecyclerAdapter<PricingModel.PriceInfo, PricingListAdpter.Holder> {
    SecurePrefManager securePrefManager;

    public PricingListAdpter(Context mContext) {
        super(mContext);
        securePrefManager = new SecurePrefManager(mContext);

    }

    @Override
    public int getLayout(int viewType) {
        return R.layout.price_tem;
    }

    @Override
    public Holder getViewHolder(View view, int viewType) {
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        holder.month.setText(getItem(i).getMonth() + " - " + getItem(i).getYear());
        Long longval = getItem(i).getPrice().longValue();

        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        formatter.applyPattern("#,##,##,###");
        String formattedString = formatter.format(longval);

        //setting text after format to EditText

        if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(BO_MANAGER))
            holder.pricing.setText(" \u20B9 " + "*********");
        else
            holder.pricing.setText(" \u20B9 " + formattedString);


        holder.state.setText(getItem(i).getState());

        if (getItem(i).getRange() == 1) {
            holder.range.setText("1.00-10.00 Kw");
        } else if (getItem(i).getRange() == 2) {
            holder.range.setText("10.01-100.00 Kw");
        } else {
            holder.range.setText(" >100.01 Kw");
        }

        if (getItem(i).componentCreated){
            holder.setComponent.setText("View Component");
        }else {
            holder.setComponent.setText("Set Component");

        }

        holder.setComponent.setOnClickListener(v -> {
            getLongClickListener().onItemClick(getItem(i), i,holder.setComponent);

        });

//        String district = "";
//
//        for (String x : getItem(i).districtList) {
//            district = district+"," + x;
//
//        }
//
//        holder.districtText.setText(district.substring(1));

        holder.sitetype.setText(getItem(i).getSiteType());

    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView month, sitetype, pricing, range, state, districtText, setComponent;
        View aniview;
        RelativeLayout cardView1, cardView2;

        public Holder(@NonNull View itemView) {
            super(itemView);
            aniview = itemView;
            month = itemView.findViewById(R.id.month);
            districtText = itemView.findViewById(R.id.districtText);
            setComponent = itemView.findViewById(R.id.setComponent);
            sitetype = itemView.findViewById(R.id.siteType);
            cardView1 = itemView.findViewById(R.id.card1);
            cardView2 = itemView.findViewById(R.id.card2);
            itemView.findViewById(R.id.backTodistrict).setVisibility(View.GONE);
            itemView.findViewById(R.id.backTodistrict).setOnClickListener(this);
            pricing = itemView.findViewById(R.id.pricing);
            range = itemView.findViewById(R.id.range);
            state = itemView.findViewById(R.id.state);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.backTodistrict:
//                    if (cardView2.getVisibility() == View.VISIBLE) {
//                        cardView2.setVisibility(View.GONE);
//                        cardView1.setVisibility(View.VISIBLE);
//                    } else {
//                        cardView2.setVisibility(View.VISIBLE);
//                        cardView1.setVisibility(View.GONE);
//                    }
//                    Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.rotate_anim);
//                    aniview.startAnimation(animation);
                    break;
                default:
                    getListener().onClick(getItem(getAdapterPosition()), getAdapterPosition());
                    break;
            }
        }

    }
}
