package com.solarginni.Implementer.ImplementerDetails;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.solarginni.Base.BaseActivity;
import com.solarginni.CommonModule.MainActivity;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.DemandQuoteModel;
import com.solarginni.DBModel.ImplementerDetail;
import com.solarginni.Implementer.SetEmpanelment.EmpanelmentList;
import com.solarginni.Implementer.SetPricingModule.PriceListActivity;
import com.solarginni.Implementer.SetProposal.SetProposalTerms;
import com.solarginni.R;
import com.solarginni.SGUser.CompanyModule.ActivateDeactivateCompanyApi;
import com.solarginni.Utility.MyValueFornatter;
import com.solarginni.Utility.Utils;

import java.util.ArrayList;

import static android.graphics.Typeface.*;
import static com.solarginni.Utility.AppConstants.BO_MANAGER;
import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.SG_ADMIN;
import static com.solarginni.Utility.AppConstants.TOKEN;

public class ImplemeterDetailsActivity extends BaseActivity implements ImplementerDetailsContract.View, View.OnClickListener {
    private TextView implemeterName, implementerId, address, totalSite, totalCapacity, primary, contact;
    private ImplementerDetailsPresenter presenter;
    private boolean isFormasking = false;
    private ImageView profileImage;
    private DemandQuoteModel model;
    private Button activateCompany;
    private PieChart nationPieChart, statePieChart;


    @Override
    public int getLayout() {
        return R.layout.implementer_details;
    }

    @Override
    public void initViews() {
        implementerId = findViewById(R.id.implementerId);
        implemeterName = findViewById(R.id.implementerName);
        address = findViewById(R.id.address);
        totalSite = findViewById(R.id.siteCount);
        totalCapacity = findViewById(R.id.capacity);
        activateCompany = findViewById(R.id.activate);
        primary = findViewById(R.id.primary);
        contact = findViewById(R.id.contact);
        profileImage = findViewById(R.id.profileImage);
        statePieChart = findViewById(R.id.StatePieChart);
        nationPieChart = findViewById(R.id.NationalPieChart);

        disableSuggestion(implementerId);
        disableSuggestion(implemeterName);
        disableSuggestion(address);
        disableSuggestion(totalCapacity);
        disableSuggestion(totalSite);
        disableSuggestion(primary);
        disableSuggestion(contact);

        activateCompany.setOnClickListener(this);

        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });
        presenter = new ImplementerDetailsPresenter(this);


    }

    @Override
    public void setUp() {


        isFormasking = getIntent().getExtras().getBoolean("isFormasking", false);

        if (!getIntent().getExtras().getBoolean("isImlementer", false)) {
            findViewById(R.id.detailLay).setVisibility(View.VISIBLE);
            nationPieChart.setVisibility(View.VISIBLE);
            statePieChart.setVisibility(View.VISIBLE);


        } else {
            findViewById(R.id.detailLay).setVisibility(View.GONE);
            nationPieChart.setVisibility(View.GONE);
            statePieChart.setVisibility(View.GONE);

        }


        if (Utils.hasNetwork(this)) {
            showProgress("Please wait..");
            presenter.fetchImp(securePrefManager.getSharedValue(TOKEN), getIntent().getExtras().getString("id", ""));
        } else {
            showToast("Please check Internet");
        }


        if (getIntent().getExtras().getString("id", "").startsWith("SGIM")) {

            findViewById(R.id.empanelMentInfo).setVisibility(View.VISIBLE);
            findViewById(R.id.lay7).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putString("companyId", getIntent().getExtras().getString("id", ""));
                    goToNextScreen(PriceListActivity.class, bundle);
                }
            });

            findViewById(R.id.proposalLay).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putString("companyId", getIntent().getExtras().getString("id", ""));
                    goToNextScreen(SetProposalTerms.class, bundle);
                }
            });
            findViewById(R.id.lay8).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putString("companyId", getIntent().getExtras().getString("id", ""));
                    goToNextScreen(EmpanelmentList.class, bundle);
                }
            });

            if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SG_ADMIN)|securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(BO_MANAGER)) {
                findViewById(R.id.priceLay).setVisibility(View.VISIBLE);
                findViewById(R.id.proposalInfo).setVisibility(View.VISIBLE);
                activateCompany.setVisibility(View.VISIBLE);

            } else {
                findViewById(R.id.priceLay).setVisibility(View.INVISIBLE);

            }
        }

    }

    @Override
    public void onFailure(String msg) {
        hideProgress();
        if (msg.equalsIgnoreCase("invalid token")) {
            securePrefManager.clearAppsAllPrefs();
            startActivity(new Intent(ImplemeterDetailsActivity.this, MainActivity.class));
            finish();
        } else {
            showToast(msg);
            finish();

        }


    }

    @Override
    public void onFetchImplementer(CommomResponse response) {
        hideProgress();
        ImplementerDetail detail = response.toResponseModel(ImplementerDetail.class);
        implemeterName.setText(detail.getCompanyName());
        implementerId.setText(detail.getUserId());
        primary.setText(detail.getFirstUserName());

        if (detail.activate){
            activateCompany.setText("De-Activate");
        }
        else {
            activateCompany.setText("Activate");
        }

        activateCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                    showProgress("Please wait..");

                    ActivateDeactivateCompanyApi api = new ActivateDeactivateCompanyApi() {
                        @Override
                        public void onComplete(CommomResponse response) {
                            hideProgress();
                            if (detail.activate){
                                activateCompany.setText("Activate");

                            }
                            else {
                                activateCompany.setText("De-Activate");

                            }
                        }

                        @Override
                        protected void onFailur(String msg) {
                            hideProgress();
                            showToast(msg);

                        }
                    };
                    api.hit(securePrefManager.getSharedValue(TOKEN),detail.getUserId(),!detail.activate);




            }
        });
        setNationalPieChart(detail);
        setStatePieChart(detail);
        if (isFormasking) {
            contact.setText("+91**********");
        } else {
            contact.setText(detail.getFirstUserPhone());
            if (!detail.getFirstUserPhone().equalsIgnoreCase("")){
                contact.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {

                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + detail.getFirstUserPhone()));
                            startActivity(intent);
                        } catch (Exception e) {
                            e.printStackTrace();
                            // TODO: handle exception
                        }
                    }
                });
            }

        }

        totalSite.setText(String.valueOf(detail.getTotalSite()));
        totalCapacity.setText(String.valueOf(detail.getTotalCapacity()));
        try {
            if (!detail.getUrl().isEmpty())
                Glide.with(this).load(detail.getUrl()).into(profileImage);
        } catch (Exception e) {
            e.printStackTrace();

        }
        address.setText(detail.getAddress().getAddressLine1().trim() + "," + detail.getAddress().getAddressLine2().trim() + "," + detail.getAddress().getCity().trim() + "," + detail.getAddress().getState() + "," + detail.getAddress().getCountry() + "," + detail.getAddress().getPinCode());

    }

    @Override
    public void onDemanded(CommomResponse response) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.demandQuote:
                break;
        }

    }


    private void setNationalPieChart(ImplementerDetail detail) {


        if (detail.getTotalSite() == 0) {
            nationPieChart.setVisibility(View.GONE);
        }

        nationPieChart.setUsePercentValues(true);
        nationPieChart.getDescription().setEnabled(false);
        nationPieChart.setExtraOffsets(5, 10, 5, 5);
        nationPieChart.setDragDecelerationFrictionCoef(0.9f);
        nationPieChart.setTransparentCircleRadius(61f);
        nationPieChart.setCenterText("Sites in India");
        nationPieChart.setCenterTextColor(getResources().getColor(R.color.white));
        nationPieChart.setHoleColor(Color.TRANSPARENT);
        nationPieChart.setRotationEnabled(false);
        nationPieChart.setDrawEntryLabels(false);
        nationPieChart.setUsePercentValues(false);

        nationPieChart.animateY(1000, Easing.EaseInOutCubic);
        ArrayList<PieEntry> yValues = new ArrayList<>();

        nationPieChart.getDescription().setText("");

        if (detail.nationalModel.residential != 0)
            yValues.add(new PieEntry(detail.nationalModel.residential * detail.getTotalSite(), "Residential"));
        if (detail.nationalModel.industrial != 0)
            yValues.add(new PieEntry(detail.nationalModel.industrial * detail.getTotalSite(), "Industrial"));
        if (detail.nationalModel.commercial != 0)
            yValues.add(new PieEntry(detail.nationalModel.commercial * detail.getTotalSite(), "Commercial"));
        if (detail.nationalModel.religious != 0)
            yValues.add(new PieEntry(detail.nationalModel.religious * detail.getTotalSite(), "Religious"));
        if (detail.nationalModel.institutional != 0)
            yValues.add(new PieEntry(detail.nationalModel.institutional * detail.getTotalSite(), "Institutional"));
        if (detail.nationalModel.others != 0)
            yValues.add(new PieEntry(detail.nationalModel.others * detail.getTotalSite(), "Others"));

        PieDataSet dataSet = new PieDataSet(yValues, "");
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);
        dataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        PieData pieData = new PieData((dataSet));
        pieData.setValueTextSize(15f);
        pieData.setValueTypeface(Typeface.defaultFromStyle(BOLD));
        pieData.setValueFormatter(new MyValueFornatter());
        pieData.setValueTextColor(getResources().getColor(R.color.white));

        nationPieChart.setData(pieData);


    }

    private void setStatePieChart(ImplementerDetail detail) {


        if (detail.stateCount == 0) {
            statePieChart.setVisibility(View.GONE);
        }

        statePieChart.setUsePercentValues(true);
        statePieChart.getDescription().setEnabled(true);
        statePieChart.setExtraOffsets(5, 10, 5, 5);
        statePieChart.setDragDecelerationFrictionCoef(0.9f);
        statePieChart.setTransparentCircleRadius(61f);
        statePieChart.setCenterText("Sites in Your State");
        statePieChart.setCenterTextColor(getResources().getColor(R.color.white));
        statePieChart.setHoleColor(Color.TRANSPARENT);
        statePieChart.setRotationEnabled(false);
        statePieChart.setUsePercentValues(false);

        statePieChart.setDrawEntryLabels(false);
        statePieChart.animateY(1000, Easing.EaseInOutCubic);
        ArrayList<PieEntry> yValues = new ArrayList<>();

        statePieChart.getDescription().setText("");

        if (detail.stateModel.residential != 0)
            yValues.add(new PieEntry(detail.stateModel.residential * detail.stateCount, "Residential"));
        if (detail.stateModel.industrial != 0)
            yValues.add(new PieEntry(detail.stateModel.industrial * detail.stateCount, "Industrial"));
        if (detail.stateModel.commercial != 0)
            yValues.add(new PieEntry(detail.stateModel.commercial * detail.stateCount, "Commercial"));
        if (detail.stateModel.religious != 0)
            yValues.add(new PieEntry(detail.stateModel.religious * detail.stateCount, "Religious"));
        if (detail.stateModel.institutional != 0)
            yValues.add(new PieEntry(detail.stateModel.institutional * detail.stateCount, "Institutional"));
        if (detail.stateModel.others != 0)
            yValues.add(new PieEntry(detail.stateModel.others * detail.stateCount, "Others"));

        PieDataSet dataSet = new PieDataSet(yValues, "");
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);
        dataSet.setColors(ColorTemplate.COLORFUL_COLORS);
        PieData pieData = new PieData((dataSet));
        pieData.setValueTextSize(15f);
        pieData.setValueFormatter(new MyValueFornatter());

        pieData.setValueTypeface(Typeface.defaultFromStyle(BOLD));
        pieData.setValueTextColor(getResources().getColor(R.color.white));

        statePieChart.setData(pieData);

    }

}
