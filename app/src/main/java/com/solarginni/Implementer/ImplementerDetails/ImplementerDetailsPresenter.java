package com.solarginni.Implementer.ImplementerDetails;

import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.DemandQuoteModel;
import com.solarginni.network.RestClient;

public class ImplementerDetailsPresenter implements ImplementerDetailsContract.Preseneter, ImplementerDetailsContract.OnInteraction {

    private ImplementerDetailsInteractor interactor;


    private ImplementerDetailsContract.View view;


    public ImplementerDetailsPresenter(ImplementerDetailsContract.View view) {
        this.view = view;
        interactor = new ImplementerDetailsInteractor(RestClient.getRetrofitBuilder());
    }


    @Override
    public void onFailure(String msg) {
        view.onFailure(msg);



    }

    @Override
    public void onFetchImplementer(CommomResponse response) {
        view.onFetchImplementer(response);

    }

    @Override
    public void onDemanded(CommomResponse response) {
        view.onDemanded(response);

    }

    @Override
    public void fetchImp(String token,String id) {
        interactor.fetchImp(token,id, this);

    }

    @Override
    public void demandQuote(String token, DemandQuoteModel model, String id) {
        interactor.demandQuote(token,model,id,this);

    }
}
