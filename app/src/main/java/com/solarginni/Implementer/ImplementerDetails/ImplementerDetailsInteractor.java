package com.solarginni.Implementer.ImplementerDetails;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.DemandQuoteModel;
import com.solarginni.DBModel.EmpanelMentBody;
import com.solarginni.Implementer.SetEmpanelment.SetEmapnelmentContract;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;

import retrofit2.Retrofit;

public class ImplementerDetailsInteractor implements ImplementerDetailsContract.Interactor {


    private Retrofit retrofit;
    private ApiInterface apiInterface;

    public ImplementerDetailsInteractor(Retrofit retrofit) {
        this.retrofit = retrofit;
        apiInterface = retrofit.create(ApiInterface.class);
    }
    @Override
    public void fetchImp(String token,String id, ImplementerDetailsContract.OnInteraction interaction) {
        apiInterface.fetchImp(token,id).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                interaction.onFetchImplementer(response);

            }

            @Override
            public void onError(ApiError error) {
                interaction.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                interaction.onFailure(throwable.getMessage());

            }
        });


    }

    @Override
    public void demandQuote(String token, DemandQuoteModel model, String id, ImplementerDetailsContract.OnInteraction interaction) {
        apiInterface.demandQuote(token,model,"optId").enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                interaction.onDemanded(response);

            }

            @Override
            public void onError(ApiError error) {
                interaction.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                interaction.onFailure(throwable.getMessage());

            }
        });


    }
}
