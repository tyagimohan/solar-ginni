package com.solarginni.Implementer.ImplementerDetails;

import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.DemandQuoteModel;
import com.solarginni.DBModel.EmpanelMentBody;

interface ImplementerDetailsContract {

    interface  View {
        void onFailure(String msg);
        void onFetchImplementer(CommomResponse response);
        void onDemanded(CommomResponse response);

    }

    interface Preseneter{
        void fetchImp(String token,String id);
        void demandQuote(String token, DemandQuoteModel model, String id);
    }

    interface Interactor{

        void fetchImp(String token,String id, OnInteraction interaction);
        void demandQuote(String token,DemandQuoteModel model,String id, OnInteraction interaction);



    }

    interface  OnInteraction{

        void onFailure(String msg);
        void onFetchImplementer(CommomResponse response);
        void onDemanded(CommomResponse response);

    }
}
