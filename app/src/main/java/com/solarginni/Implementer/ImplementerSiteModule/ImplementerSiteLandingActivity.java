package com.solarginni.Implementer.ImplementerSiteModule;

import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.solarginni.Base.BaseActivity;
import com.solarginni.CustomizeView.CustomPager;
import com.solarginni.R;
import com.solarginni.user.UserSiteModule.SiteLandingChild2;
import com.solarginni.user.UserSiteModule.SiteLandingChild3;
import com.solarginni.user.UserSiteModule.SiteLandinngChild1;

public class ImplementerSiteLandingActivity extends BaseActivity {
    public CustomPager viewPager;
    private ViewPagerAdapter adapter;
    private TabLayout tabIndicator;
    private TextView raiseRequest;


    @Override
    public int getLayout() {
        return R.layout.site_landing;
    }

    @Override
    public void initViews() {
        tabIndicator = findViewById(R.id.tabIndicator);
        viewPager = findViewById(R.id.viewPager);
        raiseRequest = findViewById(R.id.raiseRequest);
        adapter = new ViewPagerAdapter(getSupportFragmentManager());

    }

    @Override
    public void setUp() {
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(adapter);
        viewPager.setOffscreenPageLimit(2);
        tabIndicator.setupWithViewPager(viewPager);
        tabIndicator.setSelected(false);
        raiseRequest.setVisibility(View.GONE);
        viewPager.disableScroll(false);

    }

    public class ViewPagerAdapter extends FragmentStatePagerAdapter implements ViewPager.OnPageChangeListener {

        private Fragment[] childFragments;

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
            childFragments = new Fragment[]{
                    new SiteLandinngChild1(), //0
                    new SiteLandingChild2(), //1
                    new SiteLandingChild3() //2
            };
        }


        @Override
        public Fragment getItem(int position) {

            childFragments[position].setArguments(getIntent().getExtras());

            return childFragments[position];
        }

        @Override
        public int getCount() {
            return childFragments.length; //3 items
        }

        @Override
        public void onPageScrolled(int i, float v, int i1) {
            hideKeyboard();

        }

        @Override
        public void onPageSelected(int i) {


        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    }

}
