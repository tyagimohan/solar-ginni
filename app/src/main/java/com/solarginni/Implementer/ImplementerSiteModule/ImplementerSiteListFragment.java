package com.solarginni.Implementer.ImplementerSiteModule;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.solarginni.Base.BaseFragment;
import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.CommonModule.MainActivity;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.SiteDetailsModel.SiteDetails;
import com.solarginni.DBModel.SiteDetailsModel.SiteDetailsModel;
import com.solarginni.Implementer.ImplementerLanding.ImplementerHomeContract;
import com.solarginni.Implementer.ImplementerLanding.ImplementerLandingPresenter;
import com.solarginni.R;
import com.solarginni.SGUser.SiteModule.UpdateSiteActivity;
import com.solarginni.Utility.Utils;
import com.solarginni.user.UserSiteModule.SiteLandingAcity;
import com.solarginni.user.customerDetail.CustomerDetailActivity;

import java.io.Serializable;

import static com.solarginni.Utility.AppConstants.TOKEN;

public class ImplementerSiteListFragment extends BaseFragment implements ImplementerHomeContract.View, View.OnClickListener {
    private static final int REQUEST_SITE = 2;
    private static final int REQUEST_FOR_EDIT = 3;
    private RecyclerView recyclerView;
    private TextView implementerName;
    private TextView siteTypeSpinner, stateSpinnier;
    private ImplementerLandingPresenter presenter;
    private ImplementerSiteAdapter.onItemClick<SiteDetails> lonListener = (data, position, view) -> {

        switch (view.getId()) {
            case R.id.mobileNumber:
                if (data.getCustomerUserId() != null) {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("isFormasking", false);
                    bundle.putString("id", data.getCustomerUserId());
                    goToNextScreen(CustomerDetailActivity.class, bundle);
                }
                break;
            case R.id.editSite:
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", data);
                Intent intent = new Intent(getActivity(),UpdateSiteActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent,REQUEST_FOR_EDIT);
                break;
            case R.id.siteImage:

                if (data.getImageUrl() != null) {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
                    LayoutInflater inflater = this.getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.advertise_layout, null);

                    dialogView.findViewById(R.id.advertisementImage).setVisibility(View.GONE);
                    ImageView siteImage = dialogView.findViewById(R.id.imageView);
                    siteImage.setVisibility(View.VISIBLE);
                    Glide.with(getContext()).load(data.getImageUrl().get(0)).into(siteImage);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(true);
                    AlertDialog alertDialog = dialogBuilder.create();
                    alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    alertDialog.show();
                }

                break;
        }


    };
    private ImplementerSiteAdapter.OnClick listener = (data, position) -> {
        Bundle bundle = new Bundle();
        bundle.putSerializable("data", (Serializable) data);
        bundle.putBoolean("isFromImp", true);
        Intent intent = new Intent(getActivity(), SiteLandingAcity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, REQUEST_SITE);


    };

    @Override
    public int getLayout() {
        return R.layout.site_list;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_SITE:
            case REQUEST_FOR_EDIT:
                if (Utils.hasNetwork(getContext())) {
                    showProgress("Please wait..");
                    presenter.getSiteDetails(securePrefManager.getSharedValue(TOKEN), siteTypeSpinner.getText().toString(), stateSpinnier.getText().toString());
                } else {
                    showToast("Please check Internet");

                }
                break;
        }
    }

    @Override
    public void initViews(View view) {
        recyclerView = view.findViewById(R.id.recyclerView);
        implementerName = view.findViewById(R.id.implementerName);
        siteTypeSpinner = view.findViewById(R.id.siteType);
        stateSpinnier = view.findViewById(R.id.state);
        siteTypeSpinner.setOnClickListener(this);
        stateSpinnier.setOnClickListener(this);
        view.findViewById(R.id.filter).setVisibility(View.VISIBLE);
        implementerName.setText("Customer Mobile");
        presenter = new ImplementerLandingPresenter(this);

    }

    @Override
    public void setUp() {
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new BaseRecyclerAdapter.DividerItemDecoration(getContext(), RecyclerView.HORIZONTAL));

        if (Utils.hasNetwork(getContext())) {
            showProgress("Please wait..");
            presenter.getSiteDetails(securePrefManager.getSharedValue(TOKEN), siteTypeSpinner.getText().toString(), stateSpinnier.getText().toString());
        } else {
            showToast("Please check Internet");

        }


    }

    @Override
    public void OnGetLandingDetails(CommomResponse response) {

    }

    @Override
    public void onGetSiteLov(CommomResponse response) {

    }

    @Override
    public void onGetImplementerAccount(CommomResponse response) {

    }

    @Override
    public void onGetSiteDetails(CommomResponse response) {
        hideProgress();
        SiteDetailsModel model = response.toResponseModel(SiteDetailsModel.class);
        ImplementerSiteAdapter adapter = new ImplementerSiteAdapter(getContext());
        adapter.addAll(model.getSiteDetails());
        adapter.setOnClickListener(listener);
        adapter.setOnLongClickListener(lonListener);
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void onfetchQuote(CommomResponse response) {

    }

    @Override
    public void onFailure(String msg) {

        hideProgress();
        if (msg.equalsIgnoreCase("invalid token")) {
            securePrefManager.clearAppsAllPrefs();
            startActivity(new Intent(getActivity(), MainActivity.class));
            getActivity().finish();
        } else {
            showToast(msg);
        }

    }

    public void showAlertWithList(final String[] dataArray, final @StringRes int title, final TextView textView) {
        hideKeyboard(getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(getString(title));
        builder.setItems(dataArray, (dialog, item) -> {
            textView.setText(dataArray[item]);
            if (Utils.hasNetwork(getContext())) {
                showProgress("Please wait..");
                presenter.getSiteDetails(securePrefManager.getSharedValue(TOKEN), siteTypeSpinner.getText().toString(), stateSpinnier.getText().toString());
            } else {
                showToast("Please check Internet");

            }

        });
        AlertDialog alerts = builder.create();
        alerts.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.siteType:
                showAlertWithList(new String[]{"All", "Residential", "Institutional", "Industrial", "Commercial", "Religious", "Others"},R.string.site_title,siteTypeSpinner);
                break;
            case R.id.state:
                showAlertWithList(getResources().getStringArray(R.array.stateAll),R.string.state_title,stateSpinnier);
                break;
        }

    }
}
