package com.solarginni.Implementer.ImplementerSiteModule;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.DBModel.SiteDetailsModel.SiteDetails;
import com.solarginni.R;
import com.solarginni.Utility.Utils;

public class ImplementerSiteAdapter extends BaseRecyclerAdapter<SiteDetails, ImplementerSiteAdapter.Holder> {
    public ImplementerSiteAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public int getLayout(int viewType) {
        return R.layout.implementer_site_item;
    }

    @Override
    public Holder getViewHolder(View view, int viewType) {
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {

        holder.cardView1.setVisibility(View.VISIBLE);
        holder.cardView2.setVisibility(View.GONE);

        String concatinatedString = getItem(i).getSiteId() + "-" + getItem(i).getSolutionType();

        SpannableString siteID = new SpannableString(concatinatedString);
        siteID.setSpan(new UnderlineSpan(), 0, concatinatedString.length(), 0);
        holder.siteID.setText(siteID);


        if (getItem(i).getImageUrl().get(0)!=null){
            holder.siteImage.setVisibility(View.VISIBLE);
        }else {
            holder.siteImage.setVisibility(View.GONE);

        }


        if (getItem(i).panelLogo != null) {
            holder.panelMake.setVisibility(View.VISIBLE);

            ImageLoader.getInstance().displayImage(getItem(i).panelLogo, holder.panelMake);
        } else {
            holder.panelMake.setVisibility(View.GONE);
        }
        if (getItem(i).getInverter().get(0).inverterLogo != null) {
            holder.inverterMake.setVisibility(View.VISIBLE);
            ImageLoader.getInstance().displayImage(getItem(i).getInverter().get(0).inverterLogo, holder.inverterMake);
        } else {
            holder.inverterMake.setVisibility(View.GONE);
        }
        if (getItem(i).getBattery() != null) {
            holder.batteryMake.setVisibility(View.VISIBLE);
            holder.batteryMakeLay.setVisibility(View.VISIBLE);


            ImageLoader.getInstance().displayImage(getItem(i).getBattery().batteryLogo, holder.batteryMake);
        } else {
            holder.batteryMakeLay.setVisibility(View.GONE);
            holder.batteryMake.setVisibility(View.GONE);
        }


        if (getItem(i).getCustomerUserId() != null) {
            SpannableString content = new SpannableString(getItem(i).getPhone());
            content.setSpan(new UnderlineSpan(), 0, getItem(i).getPhone().length(), 0);
            holder.mobileNumber.setText(content);
            holder.mobileNumber.setOnClickListener(view -> {
                getLongClickListener().onItemClick(getItem(i), i, holder.mobileNumber);
            });
        } else {
            holder.mobileNumber.setText(getItem(i).getPhone());
            holder.mobileNumber.setOnClickListener(view -> {

            });
        }

        holder.address.setText(getItem(i).getAddress().getAddressLine1() + ", " + getItem(i).getAddress().getAddressLine2() + ", " +  getItem(i).getAddress().getCity()
                + ", " + getItem(i).getAddress().getState() + ", " + getItem(i).getAddress().getCountry());
        holder.installedOn.setText(Utils.convertDate(getItem(i).getInstalledOn()));
        if (getItem(i).getAmcEnd()!=null){
            holder.amcTill.setText(Utils.convertDate(getItem(i).getAmcEnd()));
            holder.amcLay.setVisibility(View.VISIBLE);

        }
        else {
            holder.amcLay.setVisibility(View.GONE);
        }

        holder.siteSize.setText(String.valueOf(getItem(i).getSiteSize()) + " Kw");

        holder.siteType.setText(getItem(i).getSiteType());
        holder.state.setText(getItem(i).getAddress().getState());

    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView siteID, mobileNumber, siteSize, siteType, state, installedOn, amcTill, address;
        RelativeLayout cardView1, cardView2;
        ImageView batteryMake, panelMake, inverterMake;
        LinearLayout batteryMakeLay,amcLay,siteImage;
        View aniview;


        LinearLayout lay1;

        public Holder(@NonNull View itemView) {
            super(itemView);
            aniview=itemView;
            siteID = itemView.findViewById(R.id.siteId);
            siteSize = itemView.findViewById(R.id.siteSize);
            address = itemView.findViewById(R.id.address);
            siteType = itemView.findViewById(R.id.siteType);
            amcLay = itemView.findViewById(R.id.amcLay);
            batteryMakeLay = itemView.findViewById(R.id.batteryMakeLay);
            amcTill = itemView.findViewById(R.id.amc);
            inverterMake = itemView.findViewById(R.id.inverterMake);
            panelMake = itemView.findViewById(R.id.panelMake);
            batteryMake = itemView.findViewById(R.id.batteryMake);
            installedOn = itemView.findViewById(R.id.installedOn);
            siteImage = itemView.findViewById(R.id.siteImage);
            itemView.findViewById(R.id.showmore).setOnClickListener(this);
            itemView.findViewById(R.id.showless).setOnClickListener(this);
            itemView.findViewById(R.id.editSite).setOnClickListener(this);
            siteImage.setOnClickListener(this);
//            state = itemView.findViewById(R.id.state);
            cardView1 = itemView.findViewById(R.id.card1);
            cardView2 = itemView.findViewById(R.id.card2);
            state = itemView.findViewById(R.id.state);
            lay1 = itemView.findViewById(R.id.lay1);
            mobileNumber = itemView.findViewById(R.id.mobileNumber);
            itemView.setOnClickListener(this);
            mobileNumber.setOnClickListener(this);
            siteID.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.implementerName:
                case R.id.editSite:
                case R.id.siteImage:
                    getLongClickListener().onItemClick(getItem(getAdapterPosition()), getAdapterPosition(), view);
                    break;
                case R.id.siteId:
                    getListener().onClick(getItem(getAdapterPosition()), getAdapterPosition());
                    break;
                case R.id.showmore:
                case R.id.showless:
                    if (cardView2.getVisibility() == View.VISIBLE) {
                        cardView2.setVisibility(View.GONE);
                        cardView1.setVisibility(View.VISIBLE);
                    } else {
                        cardView2.setVisibility(View.VISIBLE);
                        cardView1.setVisibility(View.GONE);
                    }
                    Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.rotate_anim);
                    aniview.startAnimation(animation);

                    //     getListener().onClick(getItem(getAdapterPosition()), getAdapterPosition());
                    break;
            }

        }
    }
}
