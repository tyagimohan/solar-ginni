package com.solarginni.Implementer;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.solarginni.Base.BaseActivity;
import com.solarginni.CommonModule.FAQs.FAQsActivity;
import com.solarginni.CommonModule.LogoutApi;
import com.solarginni.CommonModule.MainActivity;
import com.solarginni.CommonModule.Referral.ReferralActivity;
import com.solarginni.CommonModule.Testimonial.AddTestimonialActivity;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.ImplementerQuoteListModel;
import com.solarginni.DBModel.SiteDetailsModel.SiteDetailsModel;
import com.solarginni.DBModel.UserRoleModel;
import com.solarginni.GetDetails.ImplementerDetails;
import com.solarginni.Implementer.ImplementerLanding.ImplementerHomeContract;
import com.solarginni.Implementer.ImplementerLanding.ImplementerLandingPresenter;
import com.solarginni.Implementer.ImplementerQuoteModule.ImplementerQuoteListFragment;
import com.solarginni.Implementer.ImplementerSiteModule.ImplementerSiteListFragment;
import com.solarginni.Implementer.ImplementerUserModule.ImplementerUseActivity;
import com.solarginni.NotificationModule.NotificationActivity;
import com.solarginni.R;
import com.solarginni.SiteLovsModule.SiteDetailModel.SiteLov;
import com.solarginni.Utility.AppConstants;
import com.solarginni.Utility.Converter;
import com.solarginni.Utility.Utils;
import com.solarginni.user.RequestDetailsModule.RequestListFragment;
import com.solarginni.user.SiteRegistrationActivity;

import static com.solarginni.Utility.AppConstants.IMPLEMENTER_DETAILS;
import static com.solarginni.Utility.AppConstants.MOBILE;
import static com.solarginni.Utility.AppConstants.NOTIFICATION_COUNT;
import static com.solarginni.Utility.AppConstants.TOKEN;
import static com.solarginni.Utility.AppConstants.dynamicUrl;

public class ImplemeterHomeActivity extends BaseActivity implements View.OnClickListener, ImplementerHomeContract.View, BottomNavigationView.OnNavigationItemSelectedListener {
    private static final int REQUEST_FOR_SITE = 5;
    private static final int ADD_TESTIMONILA = 2;
    public static LinearLayout homeLay, siteLay, reqLay, impLay, moreLay;
    public static BottomNavigationView bottomNavigationView;
    private FloatingActionButton registerSite;
    private TextView tvHome, tvSite, tvRequest, tvImplemeter, tvMore, tvContinue;
    private ImageView ivHome, ivSite, ivRequest, ivImplemeter, ivMore;
    private ImageView myAccount;
    private ImplementerLandingPresenter presenter;
    private int notificationCount = 0;
    private boolean testimonialCreated=false;
    private int exitCount=0;

    @Override
    public int getLayout() {
        return R.layout.implementer_home;
    }

    @Override
    public void initViews() {
        /*TextView*/
        tvHome = findViewById(R.id.tvHome);
        tvImplemeter = findViewById(R.id.tvImp);
        tvMore = findViewById(R.id.tvMore);
        tvSite = findViewById(R.id.tvSite);
        tvRequest = findViewById(R.id.tvRequest);
        tvContinue = findViewById(R.id.cont);
        registerSite = (FloatingActionButton) findViewById(R.id.regiterSite);
        registerSite.setOnClickListener(this);
        bottomNavigationView = findViewById(R.id.nav_bottom);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        /*LinearLayout*/
        homeLay = findViewById(R.id.home_lay);
        siteLay = findViewById(R.id.site_lay);
        reqLay = findViewById(R.id.request_lay);
        impLay = findViewById(R.id.imp_lay);
        moreLay = findViewById(R.id.more_lay);

        /*ImageView*/
        ivHome = findViewById(R.id.ivHome);
        ivImplemeter = findViewById(R.id.ivImp);
        ivMore = findViewById(R.id.ivMore);
        ivRequest = findViewById(R.id.ivReq);
        ivSite = findViewById(R.id.ivSite);
        myAccount = findViewById(R.id.myAccount);

        presenter = new ImplementerLandingPresenter(this);

        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("");
        toolbar.setTitle("");

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void setUp() {
        Bundle bundle = getIntent().getExtras();


        tvHome.setTextColor(getResources().getColor(R.color.colortheme));
        ivHome.setImageDrawable(getResources().getDrawable(R.drawable.ic_home_white));

        registerSite.hide();

        tvContinue.setVisibility(View.GONE);

        notificationCount = Integer.valueOf(securePrefManager.getSharedValue(NOTIFICATION_COUNT));
        testimonialCreated = bundle.getBoolean("testimonials",false);
        invalidateOptionsMenu();
        loadFragment(new ImplementerLandingFragment(), bundle, ImplementerLandingFragment.class.getSimpleName());


        homeLay.setOnClickListener(this);
        siteLay.setOnClickListener(this);
        reqLay.setOnClickListener(this);
        impLay.setOnClickListener(this);
        moreLay.setOnClickListener(this);
        myAccount.setOnClickListener(this);
        tvContinue.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.regiterSite:
                Bundle bundle = getIntent().getExtras();
                bundle.putString("isFrom", "implementerFrag");
                Intent intent = new Intent(ImplemeterHomeActivity.this, SiteRegistrationActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, REQUEST_FOR_SITE);
                break;
            case R.id.myAccount:
                if (Utils.hasNetwork(this)) {
                    showProgress("Please wait..");
                    presenter.getImplementerAccount(securePrefManager.getSharedValue(TOKEN));
                } else {
                    showToast("Please wait...");

                }
                break;
            case R.id.cont:
                if (Utils.hasNetwork(this)) {
                    showProgress("Please Wait...");
                    presenter.getLandingDetails(securePrefManager.getSharedValue(TOKEN));
                } else
                    showToast(getString(R.string.internet_error));
                break;
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.alert_menu, menu);
        MenuItem menuItem2 = menu.findItem(R.id.notification_action);
        menuItem2.setIcon(Converter.convertLayoutToImage(ImplemeterHomeActivity.this, notificationCount, R.drawable.ic_bell));
        return true;
    }

    @Override
    public void OnGetLandingDetails(CommomResponse response) {
        hideProgress();
        ImplementerDetails implementerDetails = response.toResponseModel(ImplementerDetails.class);
        securePrefManager.storeSharedValue(IMPLEMENTER_DETAILS, Utils.toString(implementerDetails));
        Bundle bundle = new Bundle();
        securePrefManager.storeSharedValue(NOTIFICATION_COUNT, String.valueOf(implementerDetails.getNotificationCount()));
        notificationCount = implementerDetails.getNotificationCount();
        testimonialCreated= implementerDetails.testimonialCreated;
        invalidateOptionsMenu();
        registerSite.hide();

        bundle.putString("for", "landing");
        loadFragment(new ImplementerLandingFragment(), bundle, ImplementerLandingFragment.class.getSimpleName());


    }

    @Override
    public void onGetSiteLov(CommomResponse response) {

        hideProgress();
        SiteLov details = response.toResponseModel(SiteLov.class);
        securePrefManager.storeSharedValue(AppConstants.SITELOVS, Utils.toString(details));
        Bundle bundle = getIntent().getExtras();
        bundle.putString("isFrom", "implementerFrag");


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_FOR_SITE|requestCode==ADD_TESTIMONILA) {
            if (resultCode == RESULT_OK) {
                Bundle bundle = new Bundle();
                bundle.putString("for", "landing");
                bottomNavigationView.setSelectedItemId(R.id.nav_home);


            }
        }
    }

    @Override
    public void onGetImplementerAccount(CommomResponse response) {
        hideProgress();

        UserRoleModel userRoleModel = response.toResponseModel(UserRoleModel.class);


        securePrefManager.storeSharedValue(AppConstants.IMPLEMENTER_ACCOUNT, Utils.toString(userRoleModel.getMyAccount()));

        Bundle bundle = new Bundle();
        bundle.putString("isFrom", "Landing");
        goToNextScreen(ImplementerProfile.class, bundle);

    }

    @Override
    public void onGetSiteDetails(CommomResponse response) {
        hideProgress();

        SiteDetailsModel model = response.toResponseModel(SiteDetailsModel.class);
        registerSite.show();


        securePrefManager.storeSharedValue(AppConstants.SITEDETAILSMODEL, Utils.toString(model));

        loadFragment(new ImplementerSiteListFragment(), null, ImplementerSiteListFragment.class.getSimpleName());
    }

    @Override
    public void onfetchQuote(CommomResponse response) {
        hideProgress();
        ImplementerQuoteListModel model = response.toResponseModel(ImplementerQuoteListModel.class);
        securePrefManager.storeSharedValue(AppConstants.IMPLEMENTER_QUOTE_LIST, Utils.toString(model));


    }

    @Override
    public void onFailure(String msg) {

        hideProgress();
        if (msg.equalsIgnoreCase("invalid token")) {
            securePrefManager.clearAppsAllPrefs();
            startActivity(new Intent(ImplemeterHomeActivity.this, MainActivity.class));
            finish();
        } else {
            showToast(msg);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.notification_action:
                startActivity(new Intent(ImplemeterHomeActivity.this, NotificationActivity.class));
                notificationCount = 0;
                invalidateOptionsMenu();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        exitCount++;
        if (exitCount > 1)
            super.onBackPressed();
        else {
            Handler handler = new Handler();
            handler.postDelayed(() -> exitCount = 0, 3000);
            Toast.makeText(this, "Press again to exit the application", Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.nav_home:
                if (Utils.hasNetwork(this)) {
                    showProgress("Please wait...");
                    registerSite.show();
                    presenter.getLandingDetails(securePrefManager.getSharedValue(TOKEN));
                } else {
                    showToast("Check Internet ");
                }
                break;
            case R.id.nav_site:
                    registerSite.show();
                loadFragment(new ImplementerSiteListFragment(), null, ImplementerSiteListFragment.class.getSimpleName());

                break;
            case R.id.nav_request:

                if (Utils.hasNetwork(this)) {
                    registerSite.hide();
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("isFromImp", true);
                    loadFragment(new RequestListFragment(), bundle, RequestListFragment.class.getSimpleName());

                } else {
                    showToast(getString(R.string.internet_error));

                }
                break;
            case R.id.nav_quote:
                registerSite.hide();
                loadFragment(new ImplementerQuoteListFragment(), null, ImplementerQuoteListFragment.class.getSimpleName());
                break;
            case R.id.nav_more:
                registerSite.hide();
                showFilterDialog(this);
               // showMore();
                break;
        }

        return true;
    }


    private void showFilterDialog(Context context) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.more_layout, null);
        Animation slideUpIn;
        slideUpIn = AnimationUtils.loadAnimation(this, R.anim.slide_in_bottom);
        dialogView.startAnimation(slideUpIn);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);
        AlertDialog alertDialog = dialogBuilder.create();
        RelativeLayout logout,faqs,showImpelementer,addTestimonial,share,refer;

        logout = dialogView.findViewById(R.id.logout);
        share = dialogView.findViewById(R.id.share);
        faqs = dialogView.findViewById(R.id.faqs);
        refer = dialogView.findViewById(R.id.refer);
        showImpelementer = dialogView.findViewById(R.id.showImpelementer);

        share.setVisibility(View.GONE);
        refer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ImplemeterHomeActivity.this, ReferralActivity.class));
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String content = "Hello Friend, I have used Solar Ginie for my Solar rooftop need and found it very interesting. Recommend you to download Solar Ginie from Android playstore " + dynamicUrl + "\n" +"Use my Promotion code " +securePrefManager.getSharedValue(MOBILE).substring(3)+" during Registration to earn Bonus points";


                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/html");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, content);
                if (sharingIntent.resolveActivity(getPackageManager()) != null)
                    startActivity(Intent.createChooser(sharingIntent, "Share Using"));
                else
                    showToast("Your device haven't any app to share this content");

                alertDialog.dismiss();

            }
        });

            showImpelementer.setVisibility(View.VISIBLE);

        logout.setOnClickListener(view -> {
            if (Utils.hasNetwork(this)) {
                showProgress("Please wait...");
                LogoutApi api= new LogoutApi() {
                    @Override
                    public void onComplete(CommomResponse response) {
                        hideProgress();
                        securePrefManager.clearAppsAllPrefs();
                        startActivity(new Intent(ImplemeterHomeActivity.this, MainActivity.class));
                        finish();
                    }
                    @Override
                    protected void onFailur(String msg) {
                        hideProgress();
                        securePrefManager.clearAppsAllPrefs();
                        startActivity(new Intent(ImplemeterHomeActivity.this, MainActivity.class));
                        finish();

                    }
                };
                api.hit(securePrefManager.getSharedValue(TOKEN));
            } else {
                showToast("Please check Internet");

            }

            alertDialog.dismiss();
        });

         addTestimonial = dialogView.findViewById(R.id.addTestimonial);

        if (testimonialCreated) {
            addTestimonial.setVisibility(View.GONE);
        } else {
            addTestimonial.setVisibility(View.VISIBLE);
        }

        addTestimonial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(ImplemeterHomeActivity.this, AddTestimonialActivity.class),ADD_TESTIMONILA);;
                alertDialog.dismiss();
            }
        });

        faqs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToNextScreen(FAQsActivity.class,null);
                alertDialog.dismiss();
            }
        });

        showImpelementer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               goToNextScreen(ImplementerUseActivity.class,null);
                alertDialog.dismiss();
            }
        });



        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.BOTTOM;

        window.setAttributes(wlp);

        alertDialog.show();

    }
}
