package com.solarginni.Implementer.ImplementerLanding;

import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.RestClient;

/**
 * Created by Mohan on 6/2/20.
 */

public class ImplementerLandingPresenter implements ImplementerHomeContract.Presenter, ImplementerHomeContract.OnInteraction {

    ImplementerHomeContract.View view;
    ImplementerLandingInteractor interactor;


    public ImplementerLandingPresenter(ImplementerHomeContract.View view) {
        this.view = view;
        interactor = new ImplementerLandingInteractor(RestClient.getRetrofitBuilder());
    }

    @Override
    public void getLandingDetails(String token) {
        interactor.getLandingDetails(token, this);


    }

    @Override
    public void getSiteLov(String token) {
        interactor.getSiteLov(token,this );

    }

    @Override
    public void getImplementerAccount(String token) {
        interactor.getImplementerAccount(
                token,this );
    }

    @Override
    public void fetchQuote(String token) {
        interactor.fetchQuote(token,this);

    }

    @Override
    public void getSiteDetails(String token,String siteType,String state) {
       interactor.getSiteDetails(token,siteType,state,this);
    }

    @Override
    public void OnGetLandingDetails(CommomResponse response) {
        view.OnGetLandingDetails(response);
    }

    @Override
    public void onGetSiteLov(CommomResponse response) {
        view.onGetSiteLov(response);

    }

    @Override
    public void onGetSiteDetails(CommomResponse response) {
        view.onGetSiteDetails(response);

    }

    @Override
    public void onfetchQuote(CommomResponse response) {
        view.onfetchQuote(response);

    }

    @Override
    public void onGetImplementerAccount(CommomResponse response) {
        view.onGetImplementerAccount(response);

    }

    @Override
    public void onFailure(String msg) {


        view.onFailure(msg);
    }
}
