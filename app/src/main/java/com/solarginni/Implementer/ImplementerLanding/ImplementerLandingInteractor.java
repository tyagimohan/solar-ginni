package com.solarginni.Implementer.ImplementerLanding;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;

import retrofit2.Retrofit;

/**
 * Created by Mohan on 6/2/20.
 */

public class ImplementerLandingInteractor implements ImplementerHomeContract.Interactor {

    private Retrofit retrofit;
    private ApiInterface apiInterface;

    public ImplementerLandingInteractor(Retrofit retrofit) {
        this.retrofit = retrofit;
        apiInterface = retrofit.create(ApiInterface.class);
    }

    @Override
    public void getLandingDetails(String token, ImplementerHomeContract.OnInteraction interaction) {
        apiInterface.getImplementer(token).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                interaction.OnGetLandingDetails(response);

            }

            @Override
            public void onError(ApiError error) {
                interaction.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {


                interaction.onFailure(throwable.getMessage());
            }
        });


    }

    @Override
    public void getSiteLov(String token, ImplementerHomeContract.OnInteraction interaction) {
        apiInterface.getSiteLOVs(token).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                interaction.onGetSiteLov(response);

            }

            @Override
            public void onError(ApiError error) {
                interaction.onFailure(error.getMessage());
            }

            @Override
            public void onFailure(Throwable throwable) {
                interaction.onFailure(throwable.getMessage());

            }
        });

    }

    @Override
    public void fetchQuote(String token, ImplementerHomeContract.OnInteraction interaction) {
        apiInterface.getImplementerQuote(token,"","",2).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                interaction.onfetchQuote(response);

            }

            @Override
            public void onError(ApiError error) {
                interaction.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                interaction.onFailure(throwable.getMessage());

            }
        });

    }

    @Override
    public void getSiteDetails(String token,String siteType,String state, ImplementerHomeContract.OnInteraction interaction) {
        apiInterface.getImpSitesDetails(token,siteType,state).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                interaction.onGetSiteDetails(response);
            }

            @Override
            public void onError(ApiError error) {
                interaction.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {


                interaction.onFailure(throwable.getMessage());
            }
        });


    }

    @Override
    public void getImplementerAccount(String token, ImplementerHomeContract.OnInteraction interaction) {
        apiInterface.getAccountDetails(token).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                interaction.onGetImplementerAccount(response);

            }

            @Override
            public void onError(ApiError error) {
                interaction.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                interaction.onFailure(throwable.getMessage());

            }
        });


    }
}
