package com.solarginni.Implementer.ImplementerLanding;

import com.solarginni.DBModel.CommomResponse;

/**
 * Created by Mohan on 6/2/20.
 */

public interface ImplementerHomeContract {

    interface View {
        void OnGetLandingDetails(CommomResponse response);

        void onGetSiteLov(CommomResponse response);

        void onGetImplementerAccount(CommomResponse response);

        void onGetSiteDetails(CommomResponse response);

        void onfetchQuote(CommomResponse response);


        void onFailure(String msg);
    }

    interface Presenter {
        void getLandingDetails(String token);

        void getSiteLov(String token);

        void getImplementerAccount(String token);

        void fetchQuote(String token);


        void getSiteDetails(String token,String siteType,String state);
    }

    interface Interactor {

        void getLandingDetails(String token, OnInteraction interaction);

        void getSiteLov(String token, OnInteraction interaction);

        void fetchQuote(String token, OnInteraction interaction);

        void getSiteDetails(String token,String siteType,String state, OnInteraction interaction);

        void getImplementerAccount(String token, OnInteraction interaction);
    }

    interface OnInteraction {

        void OnGetLandingDetails(CommomResponse response);

        void onGetSiteLov(CommomResponse response);

        void onGetSiteDetails(CommomResponse response);

        void onfetchQuote(CommomResponse response);


        void onGetImplementerAccount(CommomResponse response);


        void onFailure(String msg);

    }
}
