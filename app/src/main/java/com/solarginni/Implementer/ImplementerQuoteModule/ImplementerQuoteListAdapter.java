package com.solarginni.Implementer.ImplementerQuoteModule;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.DBModel.ImplementerQuoteListModel;
import com.solarginni.R;

public class ImplementerQuoteListAdapter extends BaseRecyclerAdapter<ImplementerQuoteListModel.QuoteInfo, ImplementerQuoteListAdapter.Holder> {


    public ImplementerQuoteListAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public int getLayout(int viewType) {
        return R.layout.implementer_quote_list_item;
    }

    @Override
    public Holder getViewHolder(View view, int viewType) {
       return new Holder(view);
    }



    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {

        SpannableString id = new SpannableString(getItem(i).getQuoteId());
        id.setSpan(new UnderlineSpan(), 0, getItem(i).getQuoteId().length(), 0);
        holder.quotation_id.setText(id);

        SpannableString content = new SpannableString(getItem(i).getQuoteImpData().getUserId());
        content.setSpan(new UnderlineSpan(), 0, getItem(i).getQuoteImpData().getUserId().length(), 0);
        holder.customerId.setText(content);

        holder.customerId.setOnClickListener(view -> {
            getLongClickListener().onItemClick(getItem(i),i,holder.customerId);
        });

        if (getItem(i).feasibilityCreated){
            holder.showfeasibilityLay.setVisibility(View.VISIBLE);
            holder.showfeasibilityLay.setOnClickListener(v -> {
                getLongClickListener().onItemClick(getItem(i),i,holder.showfeasibilityLay);
            });
        }
        else {
            holder.showfeasibilityLay.setVisibility(View.GONE);
        }

        holder.siteType.setText(getItem(i).getQuoteImpData().getLovModel().getSiteType());
        holder.status.setText(getItem(i).getStatus());
        if (getItem(i).getStatus().equalsIgnoreCase("Requested") || getItem(i).getStatus().equalsIgnoreCase("Working")|getItem(i).getStatus().equalsIgnoreCase("Revise"))
            holder.statusImage.setBackgroundResource(R.drawable.ic_light_bulb);
        else if (getItem(i).getStatus().equalsIgnoreCase("Responded"))
            holder.statusImage.setBackgroundResource(R.drawable.ic_green_blub);
        else if (getItem(i).getStatus().equalsIgnoreCase("Rejected") || getItem(i).getStatus().equalsIgnoreCase("Declined")|| getItem(i).getStatus().contains("Cancel"))
            holder.statusImage.setBackgroundResource(R.drawable.ic_red_blub);
        else if (getItem(i).getStatus().equalsIgnoreCase("Accepted"))
            holder.statusImage.setBackgroundResource(R.drawable.ic_blue_blub);
        else if (getItem(i).getStatus().contains("Book"))
            holder.statusImage.setBackgroundResource(R.drawable.ic_orange_blub);else if (getItem(i).getStatus().contains("Book"))
            holder.statusImage.setBackgroundResource(R.drawable.ic_orange_blub);

        holder.solarSize.setText(getItem(i).getQuoteImpData().getSolarSize()+" Kw");

    }

    public class Holder extends RecyclerView.ViewHolder  implements View.OnClickListener{
        private TextView quotation_id,solarSize,status,customerId,siteType;
        private ImageView statusImage;
        private LinearLayout showfeasibilityLay;

        public Holder(@NonNull View itemView) {
            super(itemView);
            quotation_id= itemView.findViewById(R.id.quotation_id);
            solarSize= itemView.findViewById(R.id.siteSize);
            status= itemView.findViewById(R.id.status);
            statusImage= itemView.findViewById(R.id.demandImage);
            siteType= itemView.findViewById(R.id.siteType);
            customerId= itemView.findViewById(R.id.customerId);
            showfeasibilityLay= itemView.findViewById(R.id.showfeasibilityLay);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            getListener().onClick(getItem(getAdapterPosition()),getAdapterPosition());

        }
    }
}
