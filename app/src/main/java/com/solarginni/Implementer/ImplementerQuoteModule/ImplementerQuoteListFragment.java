package com.solarginni.Implementer.ImplementerQuoteModule;

import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.solarginni.Base.BaseFragment;
import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.CommonModule.MainActivity;
import com.solarginni.CustomizeView.MovableFloatingActionButton;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.ImplementerQuoteListModel;
import com.solarginni.Implementer.ImplementerQuoteModule.ImplementerQuoteListAdapter;
import com.solarginni.ProspectUser.FetchQuoteContract;
import com.solarginni.ProspectUser.FetchQuotePresenter;
import com.solarginni.ProspectUser.QuoteDetailActivity;
import com.solarginni.R;
import com.solarginni.SGUser.RegisterNewUser;
import com.solarginni.Utility.Utils;
import com.solarginni.user.addFeasibility.ShowFeasibility;
import com.solarginni.user.customerDetail.CustomerDetailActivity;

import org.angmarch.views.NiceSpinner;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static com.solarginni.Utility.AppConstants.BO_MANAGER;
import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.SG_ADMIN;
import static com.solarginni.Utility.AppConstants.TOKEN;

public class ImplementerQuoteListFragment extends BaseFragment implements FetchQuoteContract.View,View.OnClickListener {
    ImplementerQuoteListAdapter adapter;

    private RecyclerView recyclerView;
    private FetchQuotePresenter presenter;
    private NiceSpinner siteSpinner, statusSpinnier;
    private Button ginieQuote,myLeads;
    private String siteType = "All", status = "All";
    private  int createdBy=2;
    private MovableFloatingActionButton raiseQuote;



    @Override
    public int getLayout() {
        return R.layout.implementer_quote_list;
    }

    @Override
    public void initViews(View view) {
        recyclerView = view.findViewById(R.id.recyclerView);
        myLeads = view.findViewById(R.id.myLeads);
        raiseQuote = view.findViewById(R.id.raiseQuote);
        ginieQuote = view.findViewById(R.id.ginieQuote);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new BaseRecyclerAdapter.DividerItemDecoration(getContext(), RecyclerView.VERTICAL));
        statusSpinnier = (NiceSpinner) view.findViewById(R.id.state_spinner);
        siteSpinner = (NiceSpinner) view.findViewById(R.id.site_spinner);
        List<String> dataset = new LinkedList<>(Arrays.asList("All", "Residential", "Institutional", "Industrial", "Commercial", "Religious", "Others"));
        siteSpinner.attachDataSource(dataset);
        List<String> statedataset = new LinkedList<>(Arrays.asList("All", "Requested", "Responded", "Working", "Accepted", "Declined", "Rejected"));
        statusSpinnier.attachDataSource(statedataset);
        presenter = new FetchQuotePresenter(this);
        ginieQuote.setOnClickListener(this);
        myLeads.setOnClickListener(this);
        raiseQuote.setOnClickListener(this);
    }

    @Override
    public void setUp() {


        siteSpinner.setOnSpinnerItemSelectedListener((parent, view, position, id) -> {

            siteType = "" + parent.getItemAtPosition(position);
            if (Utils.hasNetwork(getContext())) {
                showProgress("Please wait..");
                presenter.getPendingQuote(securePrefManager.getSharedValue(TOKEN), securePrefManager.getSharedValue(ROLE), siteType, status,createdBy);
            } else {
                showToast("Please check Internet");

            }
        });

        statusSpinnier.setOnSpinnerItemSelectedListener((parent, view, position, id) -> {

            status = "" + parent.getItemAtPosition(position);
            if (Utils.hasNetwork(getContext())) {
                showProgress("Please wait..");
                presenter.getPendingQuote(securePrefManager.getSharedValue(TOKEN), securePrefManager.getSharedValue(ROLE), siteType, status,createdBy);
            } else {
                showToast("Please check Internet");

            }
        });


    }

    @Override
    public void onFetchQuote(CommomResponse response) {
        hideProgress();
        ImplementerQuoteListModel model = response.toResponseModel(ImplementerQuoteListModel.class);
        adapter = new ImplementerQuoteListAdapter(getContext());
        adapter.addAll(model.getQuoteInfo());
        adapter.setOnClickListener(listner);
        adapter.setOnLongClickListener(lonListener);
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void onGetRecall(CommomResponse response) {

    }

    @Override
    public void onGetComparision(CommomResponse response) {

    }

    @Override
    public void onFailure(String msg) {
        hideProgress();
        if (msg.equalsIgnoreCase("invalid token")) {
            securePrefManager.clearAppsAllPrefs();
            startActivity(new Intent(getActivity(), MainActivity.class));
        } else
            showToast(msg);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Utils.hasNetwork(getContext())) {
            showProgress("Please wait...");
            status = "All";
            siteType = "All";
            List<String> dataset = new LinkedList<>(Arrays.asList("All", "Residential", "Institutional", "Industrial", "Commercial", "Religious", "Others"));
            siteSpinner.attachDataSource(dataset);
            List<String> statedataset = new LinkedList<>(Arrays.asList("All", "Requested", "Responded", "Working", "Accepted", "Declined", "Rejected"));
            statusSpinnier.attachDataSource(statedataset);
            statusSpinnier.setText(status);

            siteSpinner.setText(siteType);

            presenter.getPendingQuote(securePrefManager.getSharedValue(TOKEN), securePrefManager.getSharedValue(ROLE), "All", "All",createdBy);
        } else {
            showToast("Please check Internet");
        }
    }

    ImplementerQuoteListAdapter.OnClick<ImplementerQuoteListModel.QuoteInfo> listner = (data, position) -> {

        Bundle bundle = new Bundle();
        bundle.putSerializable("data", data);
        bundle.putBoolean("isFromImplementer", true);
        goToNextScreen(QuoteDetailActivity.class, bundle);


    };
    private ImplementerQuoteListAdapter.onItemClick<ImplementerQuoteListModel.QuoteInfo> lonListener = (data, position, view) -> {
        Bundle bundle = new Bundle();
        switch (view.getId()) {
            case R.id.showfeasibilityLay:
                bundle.putString("optId", data.getQuoteImpData().getOptyId());
                goToNextScreen(ShowFeasibility.class, bundle);
                break;
            case R.id.customerId:
                if (data.getStatus().equalsIgnoreCase("Accepted")) {
                    bundle.putBoolean("isFormasking", false);
                } else {
                    bundle.putBoolean("isFormasking", true);
                }

                bundle.putString("id", data.getQuoteImpData().getUserId());
                goToNextScreen(CustomerDetailActivity.class, bundle);
                break;
        }


    };

    @Override
    public void onClick(View v) {
        if (!Utils.hasNetwork(getContext())){
            showToast("Please check internet");
            return;
        }
        switch (v.getId()){
            case R.id.ginieQuote:
                createdBy=2;
                ginieQuote.setBackground(getActivity().getDrawable(R.drawable.orange_button));
                myLeads.setTextColor(getResources().getColor(R.color.colortheme));
                ginieQuote.setTextColor(getResources().getColor(R.color.white));
                myLeads.setBackground(null);
                raiseQuote.hide();
                showProgress("Please wait...");

                presenter.getPendingQuote(securePrefManager.getSharedValue(TOKEN), securePrefManager.getSharedValue(ROLE), "All", "All",createdBy);



                break;
            case R.id.myLeads:
                createdBy=1;
                raiseQuote.show();
                myLeads.setBackground(getActivity().getDrawable(R.drawable.orange_button));
                ginieQuote.setTextColor(getResources().getColor(R.color.colortheme));
                myLeads.setTextColor(getResources().getColor(R.color.white));
                ginieQuote.setBackground(null);
                showProgress("Please wait...");

                presenter.getPendingQuote(securePrefManager.getSharedValue(TOKEN), securePrefManager.getSharedValue(ROLE), "All", "All",createdBy);

                break;
                case R.id.raiseQuote:
                   Bundle bundle = new Bundle();
                    goToNextScreen(RegisterNewUser.class, bundle);


                break;
        }

    }
}
