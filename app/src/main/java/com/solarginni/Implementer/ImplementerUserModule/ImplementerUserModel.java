package com.solarginni.Implementer.ImplementerUserModule;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.solarginni.SiteLovsModule.SiteDetailModel.Address;

import java.io.Serializable;
import java.util.List;

public class ImplementerUserModel implements Serializable {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<UserModel> data = null;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<UserModel> getData() {
        return data;
    }

    public void setData(List<UserModel> data) {
        this.data = data;
    }

    public class UserModel implements Serializable{
        @SerializedName("activate")
        @Expose
        public Boolean activate;
        @SerializedName("phone")
        @Expose
        public  String phone;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("companyId")
        @Expose
        public String companyId;
        @SerializedName("companyName")
        @Expose
        public String companyName;
        @SerializedName("onBoardDate")
        @Expose
        public String onboardDate;
        @SerializedName("userId")
        @Expose
        public String userId;
        @SerializedName("email")
        @Expose
        public String email;
        @SerializedName("role")
        @Expose
        public Integer role;
        @SerializedName("address")
        @Expose
        public Address address;
        @SerializedName("imageUrl")
        @Expose
        public String imageUrl;
    }
}
