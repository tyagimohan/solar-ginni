package com.solarginni.Implementer.ImplementerUserModule;

import com.solarginni.DBModel.CommomResponse;

public interface ImplementerUserContract {

    interface  View{
        void onFetchUser(ImplementerUserModel response);
        void failure(String msg);

    }

    interface Presenter{
        void getImpUser(String token);

    }
    interface Interactor{
        void getImpUser(String token,OnInteraction listener);
    }
    interface OnInteraction{
        void onFetchUser(ImplementerUserModel response);
        void failure(String msg);
    }
}
