package com.solarginni.Implementer.ImplementerUserModule;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class ImplementerUserListAdapter extends BaseRecyclerAdapter<ImplementerUserModel.UserModel, ImplementerUserListAdapter.Holder> {


    public ImplementerUserListAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public int getLayout(int viewType) {
        return R.layout.implementer_user_list_item;
    }

    @Override
    public Holder getViewHolder(View view, int viewType) {
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {

        try {
            if (!getItem(i).imageUrl.isEmpty())
                Glide.with(getContext()).load(getItem(i).imageUrl).into(holder.profileImage);
        } catch (Exception e) {
            e.printStackTrace();

        }

        if (getItem(i).role==1){
            holder.role.setText("Sub-Controller");

        }
        else {
            holder.role.setText("National Controller");

        }

        if (getItem(i).activate){
            holder.status.setText("Activated");
        }
        else {
            holder.status.setText("Pending");
        }

        holder.implementerPhone.setText(getItem(i).phone);
        holder.implementerName.setText(getItem(i).name);
        holder.implementerId.setText(getItem(i).userId);

    }


    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener{

        CircleImageView profileImage;
        TextView implementerName,implementerId,implementerPhone,status,role;

        public Holder(@NonNull View itemView) {
            super(itemView);

            profileImage=itemView.findViewById(R.id.profile_image);
            role=itemView.findViewById(R.id.role);
            implementerName=itemView.findViewById(R.id.implementerName);
            implementerId=itemView.findViewById(R.id.implementerId);
            implementerPhone=itemView.findViewById(R.id.implementerPhone);
            status=itemView.findViewById(R.id.status);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            getListener().onClick(getItem(getAdapterPosition()),getAdapterPosition());
        }
    }

}
