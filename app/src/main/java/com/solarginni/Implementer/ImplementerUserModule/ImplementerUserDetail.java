package com.solarginni.Implementer.ImplementerUserModule;

import android.app.Activity;
import androidx.appcompat.app.AlertDialog;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.solarginni.Base.BaseActivity;
import com.solarginni.CustomizeView.CustomAlertPopup;
import com.solarginni.CustomizeView.CustomDialogInterface;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.Implementer.ImplementerDetails.ImplemeterDetailsActivity;
import com.solarginni.R;
import com.solarginni.SGUser.AllUserModule.AllUserModel;
import com.solarginni.SGUser.AllUserModule.SendNotificationActivity;
import com.solarginni.Utility.Utils;
import com.solarginni.network.UpdatePhoneNumberApi;

import static com.solarginni.Utility.AppConstants.MOBILE;
import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.SG_ADMIN;
import static com.solarginni.Utility.AppConstants.TOKEN;

public class ImplementerUserDetail extends BaseActivity {

    private TextView address, contact, customerId, customerName, email, roleUpGrade, companyName, onboardDate;
    private ImageView profileImage;
    private Button activate, editMobileNumber,notificationButton;
    private int role;
    private String[] roleArray = new String[]{"Sub-Controller", "National Controller"};

    @Override
    public int getLayout() {
        return R.layout.customer_detail;
    }

    @Override
    public void initViews() {
        address = findViewById(R.id.address);
        contact = findViewById(R.id.contact);
        customerId = findViewById(R.id.customerId);
        companyName = findViewById(R.id.companyName);
        onboardDate = findViewById(R.id.onboardDate);
        customerName = findViewById(R.id.customerName);
        email = findViewById(R.id.email);
        notificationButton = findViewById(R.id.notificationButton);
        profileImage = findViewById(R.id.profileImage);
        activate = findViewById(R.id.activate);
        editMobileNumber = findViewById(R.id.edit_number);
        roleUpGrade = findViewById(R.id.role_upgrade);
        findViewById(R.id.statusLAbel).setVisibility(View.VISIBLE);



        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });

        findViewById(R.id.companyLabel).setVisibility(View.VISIBLE);
        disableSuggestion(address);
        disableSuggestion(contact);
        disableSuggestion(customerId);
        disableSuggestion(customerName);
        disableSuggestion(roleUpGrade);
        disableSuggestion(companyName);
    }

    @Override
    public void setUp() {


        if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase("9") | securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SG_ADMIN)) {
            activate.setVisibility(View.VISIBLE);



        }

        /*For SG User*/

        if (getIntent().getSerializableExtra("data") instanceof AllUserModel.Detail) {


            AllUserModel.Detail model = (AllUserModel.Detail) getIntent().getSerializableExtra("data");

            notificationButton.setVisibility(View.VISIBLE);
            notificationButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putString("customerName",model.name);
                    bundle.putString("contact",model.phone);
                    bundle.putString("customerId",model.userId);

                    goToNextScreen(SendNotificationActivity.class,bundle);
                }
            });

            if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SG_ADMIN) && !model.phone.equalsIgnoreCase(securePrefManager.getSharedValue(MOBILE))) {
                editMobileNumber.setVisibility(View.VISIBLE);
            }
            companyName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("isFormasking", false);
                    bundle.putString("id", model.companyId);
                    goToNextScreen(ImplemeterDetailsActivity.class, bundle);
                }
            });
            contact.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {

                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + model.phone));
                        startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                        // TODO: handle exception
                    }
                }
            });
            role = model.role;
            customerName.setText(model.name);
            customerId.setText(model.userId);
            companyName.setText(model.companyName);
            onboardDate.setText(Utils.convertDate(model.onboardDate));
            if (!model.email.isEmpty()) {
                findViewById(R.id.emailLAbel).setVisibility(View.VISIBLE);
            } else {
                findViewById(R.id.emailLAbel).setVisibility(View.GONE);

            }
            try {
                if (!model.imageUrl.isEmpty())
                    Glide.with(this).load(model.imageUrl).into(profileImage);
            } catch (Exception e) {
                e.printStackTrace();

            }

            if (model.activate) {
                activate.setText("De-Activate");
            } else {
                activate.setText("Activate");

            }
            contact.setText(model.phone);
            email.setText(model.email);
            address.setText(model.address.getAddressLine1().trim() + "," + model.address.getAddressLine2().trim() + "," + model.address.getCity().trim() + "," + model.address.getState() + "," + model.address.getCountry() + "," + model.address.getPinCode());


            activate.setOnClickListener(v -> {
                if (Utils.hasNetwork(this)) {
                    showProgress("Please wait...");
                    ActivateDeactivateUserApi activateDeactivateUserApi = new ActivateDeactivateUserApi() {
                        @Override
                        public void onComplete(CommomResponse response) {
                            hideProgress();
                            showToast(response.getMessage());
                            model.activate = model.activate ? false : true;
                            activate.setText(model.activate ? "De-Activate" : "Activate");
                            if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase("10")) {
                                setResult(RESULT_OK);
                                finish();
                            }


                        }

                        @Override
                        protected void onFailur(String msg) {
                            hideProgress();
                            showToast(msg);

                        }
                    };
                    activateDeactivateUserApi.hit(securePrefManager.getSharedValue(TOKEN), model.userId, model.activate ? false : true);
                } else {

                }

            });
            if (model.role == 1) {
                roleUpGrade.setText("Sub-Controller");
                roleArray = new String[]{"National Controller"};


            } else {
                roleUpGrade.setText("National Controller");
                roleArray = new String[]{"Sub-Controller"};


            }
            if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase("9") | securePrefManager.getSharedValue(ROLE).equalsIgnoreCase("10")) {
                roleUpGrade.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showAlertWithList(roleArray, "Select Role", roleUpGrade, model.userId);
                    }
                });
            }


            editMobileNumber.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CustomAlertPopup.Builder builder = new CustomAlertPopup.Builder(ImplementerUserDetail.this, true);
                    builder.setMessage("Please enter 10 digit mobile number");
                    builder.setPositiveButton("Submit", new CustomDialogInterface.OnClickListener() {
                        @Override
                        public void onClick() {
                            if (builder.getEditText().trim().length() < 10 || builder.getEditText().startsWith("+91")) {
                                showToast("Please enter valid number");
                            } else {
                                showProgress("Please wait...");
                                UpdatePhoneNumberApi api = new UpdatePhoneNumberApi() {
                                    @Override
                                    public void onComplete(CommomResponse response) {
                                        hideProgress();
                                        showToast(response.getMessage());
                                        contact.setText("+91" + builder.getEditText().trim());
                                        setResult(Activity.RESULT_OK);
                                        builder.dismis();
                                    }

                                    @Override
                                    protected void onFailur(String msg) {
                                        hideProgress();
                                        showToast(msg);

                                    }
                                };
                                String moobile = "+91" + builder.getEditText().trim();
                                api.hit(securePrefManager.getSharedValue(TOKEN), model.userId, moobile);


                            }

                        }
                    });

                    builder.setNegativeButton(R.string.text_cancel, new CustomDialogInterface.OnClickListener() {
                        @Override
                        public void onClick() {
                            builder.dismis();
                        }
                    });

                    builder.setCancelable(false);
                    builder.show(Gravity.CENTER);
                }
            });

        }



        /*For Implementer Admin*/


        else {

            ImplementerUserModel.UserModel model = (ImplementerUserModel.UserModel) getIntent().getSerializableExtra("data");

            role = model.role;
            customerName.setText(model.name);
            customerId.setText(model.userId);
            companyName.setText(model.companyName);
            onboardDate.setText(Utils.convertDate(model.onboardDate));

            if (!model.email.isEmpty()) {
                findViewById(R.id.emailLAbel).setVisibility(View.VISIBLE);
            } else {
                findViewById(R.id.emailLAbel).setVisibility(View.GONE);

            }
            try {
                if (!model.imageUrl.isEmpty())
                    Glide.with(this).load(model.imageUrl).into(profileImage);
            } catch (Exception e) {
                e.printStackTrace();

            }

            if (model.activate) {
                activate.setText("De-Activate");
            } else {
                activate.setText("Activate");

            }
            contact.setText(model.phone);
            email.setText(model.email);
            address.setText(model.address.getAddressLine1().trim() + "," + model.address.getAddressLine2().trim() + "," + model.address.getCity().trim() + "," + model.address.getState() + "," + model.address.getCountry() + "," + model.address.getPinCode());
            activate.setOnClickListener(v -> {
                if (Utils.hasNetwork(this)) {
                    showProgress("Please wait...");
                    ActivateDeactivateUserApi activateDeactivateUserApi = new ActivateDeactivateUserApi() {
                        @Override
                        public void onComplete(CommomResponse response) {
                            hideProgress();
                            showToast(response.getMessage());
                            model.activate = model.activate ? false : true;
                            activate.setText(model.activate ? "De-Activate" : "Activate");
                            if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase("10")) {
                                setResult(RESULT_OK);
                                finish();
                            }


                        }

                        @Override
                        protected void onFailur(String msg) {
                            hideProgress();
                            showToast(msg);

                        }
                    };
                    activateDeactivateUserApi.hit(securePrefManager.getSharedValue(TOKEN), model.userId, model.activate ? false : true);
                } else {

                }

            });
            if (model.role == 1) {
                roleUpGrade.setText("Sub-Controller");

            } else {
                roleUpGrade.setText("National Controller");

            }
            if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase("9") | securePrefManager.getSharedValue(ROLE).equalsIgnoreCase("10")) {
                roleUpGrade.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showAlertWithList(roleArray, "Select Role", roleUpGrade, model.userId);
                    }
                });
            }

        }


    }


    public void showAlertWithList(final String[] dataArray, String title, final TextView textView, String usID) {
        hideKeyboard(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setItems(dataArray, (dialog, item) -> {
            RoleUpgradeApi api = new RoleUpgradeApi() {
                @Override
                public void onComplete(CommomResponse response) {
                    showToast(response.getMessage());
                    textView.setText(dataArray[item]);
                    role = role == 1 ? 9 : 1;
                    if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase("10")) {
                        setResult(RESULT_OK);
                        finish();
                    }
                }

                @Override
                protected void onFailur(String msg) {
                    showToast(msg);
                }
            };

            if (role == 1) {
                api.hit(securePrefManager.getSharedValue(TOKEN), usID, "9");
            } else {
                api.hit(securePrefManager.getSharedValue(TOKEN), usID, "1");

            }

        });
        AlertDialog alerts = builder.create();
        alerts.show();
    }
}
