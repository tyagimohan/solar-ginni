package com.solarginni.Implementer.ImplementerUserModule;

import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.RestClient;

public class ImplementerUserPresenter implements ImplementerUserContract.OnInteraction,ImplementerUserContract.Presenter {

    private ImplementerUserContract.View view;
    private ImplementerUserInterator interator;

    public ImplementerUserPresenter(ImplementerUserContract.View view) {
        this.view = view;
        interator= new ImplementerUserInterator(RestClient.getRetrofitBuilder());
    }

    @Override
    public void getImpUser(String token) {
        interator.getImpUser(token,this);

    }

    @Override
    public void onFetchUser(ImplementerUserModel response) {
        view.onFetchUser(response);

    }

    @Override
    public void failure(String msg) {
        view.failure(msg);

    }
}
