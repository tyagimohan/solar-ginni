package com.solarginni.Implementer.ImplementerUserModule;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;

import retrofit2.Retrofit;

public class ImplementerUserInterator implements ImplementerUserContract.Interactor {

    Retrofit retrofit;
    ApiInterface apiInterface;

    public ImplementerUserInterator(Retrofit retrofit) {
        this.retrofit = retrofit;
        apiInterface=retrofit.create(ApiInterface.class);
    }

    @Override
    public void getImpUser(String token, ImplementerUserContract.OnInteraction listener) {
        apiInterface.getImpUser(token,0).enqueue(new ResponseResolver<ImplementerUserModel>(retrofit) {
            @Override
            public void onSuccess(ImplementerUserModel implementerUserModel) {
                listener.onFetchUser(implementerUserModel);
            }

            @Override
            public void onError(ApiError error) {
                listener.failure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                listener.failure(throwable.getMessage());

            }
        });

    }
}
