package com.solarginni.Implementer.ImplementerUserModule;

import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.widget.ImageView;

import com.solarginni.Base.BaseActivity;
import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.CommonModule.MainActivity;
import com.solarginni.R;
import com.solarginni.Utility.Utils;

import static com.solarginni.Utility.AppConstants.TOKEN;

public class ImplementerUseActivity extends BaseActivity implements ImplementerUserContract.View {

    private ImageView back;
    private RecyclerView recyclerView;
    private ImplementerUserPresenter presenter;

    @Override
    public int getLayout() {
        return R.layout.implementer_user;
    }

    @Override
    public void initViews() {
        back= findViewById(R.id.back);
        recyclerView= findViewById(R.id.recyclerView);
        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("");
        toolbar.setTitle("");
        back.setOnClickListener(v -> {
            finish();
        });
        presenter= new ImplementerUserPresenter(this);
    }

    @Override
    public void setUp() {



        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new BaseRecyclerAdapter.DividerItemDecoration(this, RecyclerView.VERTICAL));


    }

    @Override
    public void onFetchUser(ImplementerUserModel response) {
        hideProgress();
        ImplementerUserListAdapter adapter= new ImplementerUserListAdapter(this);
        adapter.addAll(response.getData());
        if (response.getData().size()==0){
            showToast("No User found");
        }
        recyclerView.setAdapter(adapter);
        adapter.setOnClickListener(listener);


    }

    ImplementerUserListAdapter.OnClick<ImplementerUserModel.UserModel> listener= (data, position) -> {

        Bundle bundle = new Bundle();
        bundle.putSerializable("data",data);
        goToNextScreen(ImplementerUserDetail.class,bundle);
    };

    @Override
    public void failure(String msg) {
        hideProgress();
        if (msg.equalsIgnoreCase("invalid token")) {
            securePrefManager.clearAppsAllPrefs();
            startActivity(new Intent(ImplementerUseActivity.this, MainActivity.class));
            finish();
        } else
        showToast(msg);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utils.hasNetwork(this)){
            showProgress("Please wait...");
            presenter.getImpUser(securePrefManager.getSharedValue(TOKEN));
        }
        else {
            showToast("Please check Internet");

        }
    }
}
