package com.solarginni.Implementer.SetProposal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SetProposalModel implements Serializable {

    @SerializedName("cancellationCharge")
    @Expose
    public Double cancellationCharge;
    @SerializedName("paymentTermDtos")
    @Expose
    public List<PaymentTerm> paymentTermDtos = new ArrayList<>();
    @SerializedName("serviceLevel")
    @Expose
    public List<ServiceLevel> serviceLevel = new ArrayList<>();
    @SerializedName("validity")
    @Expose
    public Integer validity;
    @SerializedName("id")
    @Expose
    public Integer id;


    public static class PaymentTerm implements Serializable{

        @SerializedName("advance")
        @Expose
        public Double advance;
        @SerializedName("milestone1")
        @Expose
        public Double milestone1;
        @SerializedName("milestone2")
        @Expose
        public Double milestone2;
        @SerializedName("title")
        @Expose
        public String title;

    }

    public static class ServiceLevel implements Serializable{

        @SerializedName("states")
        @Expose
        public List<String> states = new ArrayList<>();
        @SerializedName("title")
        @Expose
        public String title;

    }


}
