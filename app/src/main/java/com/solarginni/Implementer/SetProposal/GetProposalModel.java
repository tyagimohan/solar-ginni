package com.solarginni.Implementer.SetProposal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetProposalModel implements Serializable {

    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("userId")
    @Expose
    public String userId;
    @SerializedName("paymentTermDtos")
    @Expose
    public List<PaymentTerm> paymentTermDtos = null;
    @SerializedName("serviceLevel")
    @Expose
    public List<ServiceLevel> serviceLevel = null;
    @SerializedName("validity")
    @Expose
    public Integer validity;
    @SerializedName("cancellationCharge")
    @Expose
    public Double cancellationCharge;


    public static class PaymentTerm implements  Serializable{

        @SerializedName("title")
        @Expose
        public String title;
        @SerializedName("milestone1")
        @Expose
        public Double milestone1;
        @SerializedName("milestone2")
        @Expose
        public Double milestone2;
        @SerializedName("advance")
        @Expose
        public Double advance;

    }

    public static class ServiceLevel implements  Serializable{

        @SerializedName("states")
        @Expose
        public List<String> states = null;
        @SerializedName("title")
        @Expose
        public String title;

    }

}
