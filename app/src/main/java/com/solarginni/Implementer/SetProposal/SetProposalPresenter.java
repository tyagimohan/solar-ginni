package com.solarginni.Implementer.SetProposal;

import com.solarginni.ComponentModel;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.RestClient;

public class SetProposalPresenter implements SetProposalContract.OnIteraction, SetProposalContract.Presenter {

    private SetProposalContract.View view;
    private SetProposalInteractor interector;

    public SetProposalPresenter(SetProposalContract.View view) {
        this.view = view;
        interector = new SetProposalInteractor(RestClient.getRetrofitBuilder());
    }

    @Override
    public void getComponentMake(String token) {
        interector.getComponentMake(token, this);

    }

    @Override
    public void getProposalDetail(String token,String companyId) {
            interector.getProposalDetail(token,companyId,this);
    }

    @Override
    public void SetProposal(String token, SetProposalModel model) {
        interector.SetProposal(token,model,this);

    }


    @Override
    public void showComponentMake(ComponentModel commomResponse) {
        view.showComponentMake(commomResponse);

    }

    @Override
    public void onSetProposal() {
        view.onSetProposal();

    }

    @Override
    public void showProposalDetails(CommomResponse commomResponse) {
        view.showProposalDetails(commomResponse);

    }

    @Override
    public void onFailure(String msg) {
        view.onFailure(msg);

    }
}
