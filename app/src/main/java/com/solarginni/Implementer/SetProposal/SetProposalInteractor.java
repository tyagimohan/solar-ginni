package com.solarginni.Implementer.SetProposal;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.ComponentModel;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;

import retrofit2.Retrofit;

public class SetProposalInteractor implements SetProposalContract.Interactor{

    private Retrofit retrofit;
    private ApiInterface apiInterface;

    public SetProposalInteractor(Retrofit retrofit) {
        this.retrofit = retrofit;
        apiInterface = retrofit.create(ApiInterface.class);
    }

    @Override
    public void getComponentMake(String token, SetProposalContract.OnIteraction listenr) {
        apiInterface.getComponentMake(token).enqueue(new ResponseResolver<ComponentModel>(retrofit) {
            @Override
            public void onSuccess(ComponentModel commomResponse) {
                listenr.showComponentMake(commomResponse);

            }

            @Override
            public void onError(ApiError error) {
                listenr.onFailure(error.getMessage());
            }

            @Override
            public void onFailure(Throwable throwable) {
                listenr.onFailure(throwable.getMessage());

            }
        });
    }

    @Override
    public void getProposalDetail(String token,String companyId, SetProposalContract.OnIteraction iteraction) {
        apiInterface.getProposalDetails(token,companyId).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                iteraction.showProposalDetails(response);

            }

            @Override
            public void onError(ApiError error) {
                iteraction.onFailure(error.getMessage());


            }

            @Override
            public void onFailure(Throwable throwable) {
                iteraction.onFailure(throwable.getMessage());

            }
        });
    }

    @Override
    public void SetProposal(String token, SetProposalModel model, SetProposalContract.OnIteraction iteraction) {
        apiInterface.setProposalterrm(token,model).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                iteraction.onSetProposal();

            }

            @Override
            public void onError(ApiError error) {
                iteraction.onFailure(error.getMessage());


            }

            @Override
            public void onFailure(Throwable throwable) {
                iteraction.onFailure(throwable.getMessage());

            }
        });



    }
}
