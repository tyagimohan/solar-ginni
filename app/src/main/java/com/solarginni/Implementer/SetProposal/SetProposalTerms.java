package com.solarginni.Implementer.SetProposal;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.solarginni.Base.BaseActivity;
import com.solarginni.CommonModule.MainActivity;
import com.solarginni.ComponentModel;
import com.solarginni.CustomizeView.ProposalDiaLog.ProposalStateDialog;
import com.solarginni.CustomizeView.ProposalDiaLog.SelectStateListener;
import com.solarginni.CustomizeView.ProposalStateModel;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.R;
import com.solarginni.Utility.Utils;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static com.solarginni.Utility.AppConstants.IMPLEMENTER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.SUPER_IMPLEMENTER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.TOKEN;

public class SetProposalTerms extends BaseActivity implements SetProposalContract.View, View.OnClickListener {
    private EditText advance1, advance2, completion2, completion1, shipping1, shipping2, cancellationCharge, proposalDay;
    private EditText goldState, silverState, bronzeState;
    private Button submit;
    private List<String> goldList = new ArrayList<>();
    private List<String> silverList = new ArrayList<>();
    private List<String> bronzeList = new ArrayList<>();
    private List<ProposalStateModel> stateList = new ArrayList<>();
    private SetProposalPresenter presenter;
    private String companyId;
    private int id=-1;

    @Override
    public int getLayout() {
        return R.layout.proposal_term;
    }

    @Override
    public void initViews() {
        goldState = findViewById(R.id.goldState);
        bronzeState = findViewById(R.id.bronzeState);
        silverState = findViewById(R.id.silverState);
        advance1 = findViewById(R.id.advance1);
        advance2 = findViewById(R.id.advance2);
        shipping1 = findViewById(R.id.shipping1);
        shipping2 = findViewById(R.id.shipping2);
        completion1 = findViewById(R.id.completion1);
        completion2 = findViewById(R.id.completion2);
        proposalDay = findViewById(R.id.proposalDay);
        cancellationCharge = findViewById(R.id.cancellationCharge);
        submit = findViewById(R.id.confirmBtn);

        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });

        presenter = new SetProposalPresenter(this);

    }

    @Override
    public void setUp() {


        if (getIntent().getExtras()!=null){
            companyId= getIntent().getExtras().getString("companyId","");
        }

        if (Utils.hasNetwork(this)) {
            showProgress("Please wait...");
            presenter.getComponentMake(securePrefManager.getSharedValue(TOKEN));

        } else {
            showToast("Please Check Internet Connection");
        }

        if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(IMPLEMENTER_ROLE_ID)|securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SUPER_IMPLEMENTER_ROLE_ID)){
            submit.setOnClickListener(this);
            submit.setVisibility(View.VISIBLE);
            /*advance1, advance2, completion2, completion1, shipping1, shipping2, cancellationCharge, proposalDay*/
            advance2.setFocusable(true);
            advance1.setFocusable(true);
            completion2.setFocusable(true);
            completion1.setFocusable(true);
            shipping1.setFocusable(true);
            shipping2.setFocusable(true);
            cancellationCharge.setFocusable(true);
            proposalDay.setFocusable(true);
            goldState.setOnClickListener(this);
            bronzeState.setOnClickListener(this);
            silverState.setOnClickListener(this);

        }
        else{
            submit.setVisibility(View.GONE);
            advance2.setFocusable(false);
            advance1.setFocusable(false);
            completion2.setFocusable(false);
            completion1.setFocusable(false);
            shipping1.setFocusable(false);
            shipping2.setFocusable(false);
            cancellationCharge.setFocusable(false);
            proposalDay.setFocusable(false);
        }



    }

    @Override
    public void showComponentMake(ComponentModel commomResponse) {

        for (String state : commomResponse.data.state) {
            ProposalStateModel model = new ProposalStateModel(state, "", false);
            stateList.add(model);
        }
        presenter.getProposalDetail(securePrefManager.getSharedValue(TOKEN),companyId);


    }

    @Override
    public void showProposalDetails(CommomResponse commomResponse) {
          hideProgress();
        GetProposalModel model = commomResponse.toResponseModel(GetProposalModel.class);
        cancellationCharge.setText(String.valueOf(model.cancellationCharge));
        proposalDay.setText(String.valueOf(model.validity));
        advance1.setText(String.valueOf(model.paymentTermDtos.get(0).advance));
        shipping1.setText(String.valueOf(model.paymentTermDtos.get(0).milestone1));
        completion1.setText(String.valueOf(model.paymentTermDtos.get(0).milestone2));
        advance2.setText(String.valueOf(model.paymentTermDtos.get(1).advance));
        shipping2.setText(String.valueOf(model.paymentTermDtos.get(1).milestone1));
        completion2.setText(String.valueOf(model.paymentTermDtos.get(1).milestone2));
       id = model.id;
        List<ProposalStateModel> list = new ArrayList<>();

        for (GetProposalModel.ServiceLevel serviceLevel : model.serviceLevel){
            switch (serviceLevel.title){
                case "Gold":
                    String districtStr="";
                    for (String state : serviceLevel.states) {
                        ProposalStateModel stateModel = new ProposalStateModel(state, "Gold", true);
                        list.add(stateModel);
                        goldList.add(state);
                        districtStr=districtStr+","+state;

                    }
                    goldState.setText(districtStr.trim().length()==0?"":districtStr.substring(1));
                    break;
                case  "Silver":
                    districtStr="";
                    for (String state : serviceLevel.states) {
                        ProposalStateModel stateModel = new ProposalStateModel(state, "Silver", true);
                        list.add(stateModel);
                        silverList.add(state);
                        districtStr=districtStr+","+state;
                    }
                    silverState.setText(districtStr.trim().length()==0?"":districtStr.substring(1));

                    break;
                case "Bronze":
                    districtStr="";
                    for (String state : serviceLevel.states) {
                        ProposalStateModel stateModel = new ProposalStateModel(state, "Bronze", true);
                        list.add(stateModel);
                        bronzeList.add(state);
                        districtStr=districtStr+","+state;

                    }
                    bronzeState.setText(districtStr.trim().length()==0?"":districtStr.substring(1));
                    break;
            }

        }

        for (int i=0;i<stateList.size();i++){
            for (int j=0;j<list.size();j++){

                if (list.get(j).state.equalsIgnoreCase(stateList.get(i).state)){
                    stateList.get(i).key=list.get(j).key;
                    stateList.get(i).isSelected=list.get(j).isSelected;
                }
            }
        }
        Log.e("dfsdfg",""+stateList.size());




    }

    @Override
    public void onSetProposal() {
        hideProgress();
        finish();

    }

    @Override
    public void onFailure(String msg) {
        hideProgress();
        if (msg.equalsIgnoreCase("invalid token")) {
            securePrefManager.clearAppsAllPrefs();
            startActivity(new Intent(SetProposalTerms.this, MainActivity.class));
            finish();
        } else {
            showToast(msg);

        }


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.confirmBtn:
                try {
                    if (Double.parseDouble(advance1.getText().toString()) + Double.parseDouble(shipping1.getText().toString()) + Double.parseDouble(completion1.getText().toString()) != 100) {
                        showToast("Total Sum for the payment term for residential should be 100 %");
                    } else if (Double.parseDouble(advance2.getText().toString()) + Double.parseDouble(shipping2.getText().toString()) + Double.parseDouble(completion2.getText().toString()) != 100) {
                        showToast("Total Sum for the payment term for Non- Residential should be 100 %");

                    } else if (cancellationCharge.getText().toString().trim().length() == 0) {
                        showToast("Define Cancellation charge");


                    } else if (proposalDay.getText().toString().length() == 0) {
                        showToast("Define Proposal Validity");

                    }
                    else if (stateList.size()!=(goldList.size()+silverList.size()+bronzeList.size())){
                        showToast("Please fill all state in atleast once Service Level");
                    }
                    else {
                            SetProposalModel model = new SetProposalModel();
                            model.cancellationCharge= Double.parseDouble(cancellationCharge.getText().toString());
                            model.validity= Integer.parseInt(proposalDay.getText().toString());
                            model.id=id;
                        SetProposalModel.PaymentTerm paymentTerm = new SetProposalModel.PaymentTerm();
                        paymentTerm.advance=Double.parseDouble(advance1.getText().toString());
                        paymentTerm.milestone1=Double.parseDouble(shipping1.getText().toString());
                        paymentTerm.milestone2=Double.parseDouble(completion1.getText().toString());
                        paymentTerm.title="Residential";
                        model.paymentTermDtos.add(paymentTerm);
                        paymentTerm = new SetProposalModel.PaymentTerm();

                        paymentTerm.advance=Double.parseDouble(advance2.getText().toString());
                        paymentTerm.milestone1=Double.parseDouble(shipping2.getText().toString());
                        paymentTerm.milestone2=Double.parseDouble(completion2.getText().toString());
                        paymentTerm.title="Non-Residential";
                        model.paymentTermDtos.add(paymentTerm);

                        SetProposalModel.ServiceLevel serviceLevel = new SetProposalModel.ServiceLevel();
                        serviceLevel.states.addAll(goldList);
                        serviceLevel.title="Gold";
                        model.serviceLevel.add(serviceLevel);

                         serviceLevel = new SetProposalModel.ServiceLevel();

                        serviceLevel.states.clear();
                        serviceLevel.states.addAll(bronzeList);
                        serviceLevel.title = "Bronze";
                        model.serviceLevel.add(serviceLevel);

                        serviceLevel = new SetProposalModel.ServiceLevel();

                        serviceLevel.states.clear();
                        serviceLevel.states.addAll(silverList);
                        serviceLevel.title = "Silver";
                        model.serviceLevel.add(serviceLevel);

                        showProgress("Please wait...");
                        presenter.SetProposal(securePrefManager.getSharedValue(TOKEN),model);



                    }
                } catch (NumberFormatException e) {
                    showToast("Please fill the fields");
                    e.printStackTrace();
                }
                break;
            case R.id.bronzeState:

                List<ProposalStateModel> localList=new ArrayList<>();

                for(int i=0;i<stateList.size();i++){
                    if (stateList.get(i).key.equalsIgnoreCase("")||stateList.get(i).key.equalsIgnoreCase("Bronze")){
                        localList.add(stateList.get(i));
                    }
                }

                if (localList.size()!=0){
                    ProposalStateDialog.show(this, "Bronze",localList, false, new SelectStateListener() {
                        @Override
                        public void onSelectState(ArrayList<ProposalStateModel> districtList) {



                            String districtStr="";
                            localList.clear();
                            bronzeList.clear();
                            for (ProposalStateModel district : districtList){
                                if (district.key.equalsIgnoreCase("Bronze")){
                                    districtStr=districtStr+","+district.state;
                                    bronzeList.add(district.state);
                                }else {
//                                    ProposalStateModel model = new ProposalStateModel(district.state, "", false);
//                                    stateList.add(district);
                                }

                            }
                            bronzeState.setText(districtStr.trim().length()==0?"":districtStr.substring(1));
                        }

                        @Override
                        public void onUnSelectState(ArrayList<ProposalStateModel> districtList) {

                            bronzeState.setText("");
                            bronzeList.clear();


                        }
                    });

                }else {
                    showToast("You have selected all state");
                }

                break;
            case R.id.goldState:
               localList=new ArrayList<>();

                for(int i=0;i<stateList.size();i++){
                    if (stateList.get(i).key.equalsIgnoreCase("")||stateList.get(i).key.equalsIgnoreCase("Gold")){
                        localList.add(stateList.get(i));
                    }
                }

                if (localList.size()!=0){
                    ProposalStateDialog.show(this, "Gold",localList, false, new SelectStateListener() {
                        @Override
                        public void onSelectState(ArrayList<ProposalStateModel> districtList) {


                            String districtStr="";
                            localList.clear();
                            goldList.clear();
                            for (ProposalStateModel district : districtList){
                                if (district.key.equalsIgnoreCase("Gold")){
                                    districtStr=districtStr+","+district.state;
                                    goldList.add(district.state);
                                }else {
//                                    ProposalStateModel model = new ProposalStateModel(district.state, "", false);
//                                    stateList.add(model);
                                }


                            }

                            goldState.setText(districtStr.trim().length()==0?"":districtStr.substring(1));
                        }

                        @Override
                        public void onUnSelectState(ArrayList<ProposalStateModel> districtList) {

                            goldState.setText("");
                            goldList.clear();


                        }
                    });

                }else {
                          showToast("You have selected all state");
                }

                break;
            case R.id.silverState:
               localList=new ArrayList<>();

                for(int i=0;i<stateList.size();i++){
                    if (stateList.get(i).key.equalsIgnoreCase("")||stateList.get(i).key.equalsIgnoreCase("Silver")){
                        localList.add(stateList.get(i));
                    }
                }
                if (localList.size()!=0){
                    ProposalStateDialog.show(this, "Silver",localList, false, new SelectStateListener() {
                        @Override
                        public void onSelectState(ArrayList<ProposalStateModel> districtList) {


                            String districtStr="";
                            localList.clear();
                            silverList.clear();
                            for (ProposalStateModel district : districtList){
                                if (district.key.equalsIgnoreCase("Silver")){
                                    districtStr=districtStr+","+district.state;
                                    silverList.add(district.state);
                                }else {
//                                    ProposalStateModel model = new ProposalStateModel(district.state, "", false);
//                  .                  stateList.add(model);
                                }


                            }
                            silverState.setText(districtStr.trim().length()==0?"":districtStr.substring(1));
                        }

                        @Override
                        public void onUnSelectState(ArrayList<ProposalStateModel> districtList) {

                            silverState.setText("");
                            silverList.clear();


                        }
                    });

                }else {
                    showToast("You have selected all state");
                }
                break;
        }

    }
}
