package com.solarginni.Implementer.SetProposal;

import com.solarginni.ComponentModel;
import com.solarginni.DBModel.CommomResponse;

public
interface SetProposalContract {

    interface  View{
        void showComponentMake(ComponentModel commomResponse);
        void showProposalDetails(CommomResponse commomResponse);
        void onSetProposal();
        void onFailure(String msg);


    }
    interface  Presenter{
        void getComponentMake(String token);
        void getProposalDetail(String token,String companyId);
        void SetProposal(String token,SetProposalModel model);

    }

    interface Interactor{
        void getComponentMake(String token,OnIteraction iteraction);
        void getProposalDetail(String token,String companyId,OnIteraction iteraction);
        void SetProposal(String token,SetProposalModel model,OnIteraction iteraction);


    }
    interface OnIteraction{

        void showComponentMake(ComponentModel commomResponse);
        void onSetProposal();
        void showProposalDetails(CommomResponse commomResponse);

        void onFailure(String msg);


    }
}
