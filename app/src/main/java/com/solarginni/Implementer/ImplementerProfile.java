package com.solarginni.Implementer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.solarginni.Base.BaseActivity;
import com.solarginni.CommonModule.MainActivity;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.UserRoleModel;
import com.solarginni.GetDetails.ImplementerDetails;
import com.solarginni.Implementer.ImplementerLanding.ImplementerHomeContract;
import com.solarginni.Implementer.ImplementerLanding.ImplementerLandingPresenter;
import com.solarginni.R;
import com.solarginni.Utility.Utils;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.solarginni.Utility.AppConstants.IMPLEMENTER_ACCOUNT;
import static com.solarginni.Utility.AppConstants.IMPLEMENTER_DETAILS;
import static com.solarginni.Utility.AppConstants.PROFILE_IMAGE;
import static com.solarginni.Utility.AppConstants.TOKEN;

public class ImplementerProfile extends BaseActivity implements View.OnClickListener, ImplementerHomeContract.View {

    private TextView tvName,  tvEmail, tvMobile, tvAddress,  tvUserID, tvCompanyName, tvContinue;
    private LinearLayout tvEmailLabel;
    private ImplementerLandingPresenter presenter;
    private ImageView profileImage;
    private String url = "";
    private ImageView editProfile;


    @Override
    public int getLayout() {
        return R.layout.implementer_fragment;
    }

    @Override
    public void initViews() {
        editProfile = findViewById(R.id.editProfile);
        tvAddress = findViewById(R.id.tvAddres);
        tvName = findViewById(R.id.tvName);
        tvEmail = findViewById(R.id.tvEmail);
        tvUserID = findViewById(R.id.tvuserId);
        tvMobile = findViewById(R.id.mobileNumber);
        tvCompanyName = findViewById(R.id.tvCompanyName);
        tvEmailLabel = findViewById(R.id.emailLAbel);
        tvContinue = findViewById(R.id.cont);
        profileImage= findViewById(R.id.profileImage);
        presenter = new ImplementerLandingPresenter(this);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 3:
                if (resultCode == Activity.RESULT_OK) {
                    showProgress("Please wait...");
                    presenter.getImplementerAccount(securePrefManager.getSharedValue(TOKEN));
                } else {

                }
                break;
        }
    }

    @Override
    public void setUp() {

        if (getIntent().getExtras().getString("isFrom", "").equalsIgnoreCase("Landing")){

            editProfile.setVisibility(VISIBLE);
            editProfile.setOnClickListener(v -> {
                Intent intent = new Intent(ImplementerProfile.this, ImplementerEditProfile.class);

                startActivityForResult(intent, 3);
            });
            findViewById(R.id.back).setVisibility(VISIBLE);
            findViewById(R.id.back).setOnClickListener(v -> {
                finish();
            });


        }


        tvContinue.setOnClickListener(this);
        UserRoleModel.MyAccount accountInfo = Utils.getObj(securePrefManager.getSharedValue(IMPLEMENTER_ACCOUNT), UserRoleModel.MyAccount.class);
        Bundle bundle = getIntent().getExtras();

        if (bundle.getString("isFrom", "").equalsIgnoreCase("Landing")) {
            tvContinue.setVisibility(View.GONE);
            tvMobile.setText(accountInfo.getPhone());
            tvName.setText(accountInfo.getName());
            tvUserID.setText(accountInfo.getUserId());
            tvCompanyName.setText(accountInfo.getCompanyId().getCompanyName());
            if (!accountInfo.getEmail().trim().isEmpty())
                tvEmail.setText(accountInfo.getEmail());
            else {

                tvEmail.setVisibility(View.GONE);
                tvEmailLabel.setVisibility(View.GONE);

            }
            try {
                if (!accountInfo.getImages().isEmpty())
                    Glide.with(this).load(accountInfo.getImages()).into(profileImage);
            } catch (Exception e) {
                e.printStackTrace();

            }


            tvAddress.setText(accountInfo.getAddress().getAddressLine1().trim() + ", " + accountInfo.getAddress().getAddressLine2().trim()+", "+accountInfo.getAddress().getCity().trim()+", "+accountInfo.getAddress().getState()+", "+accountInfo.getAddress().getCountry().trim()+", "+accountInfo.getAddress().getPinCode().trim());
        } else {
            tvContinue.setVisibility(View.VISIBLE);

            tvMobile.setText(bundle.getString("phone",""));
            tvName.setText(bundle.getString("name",""));
            tvUserID.setText(bundle.getString("userID",""));
            tvCompanyName.setText(bundle.getString("companyName",""));
            if (!bundle.getString("email","").equalsIgnoreCase(""))
                tvEmail.setText(bundle.getString("email",""));
            else {

                tvEmail.setVisibility(View.GONE);
                tvEmailLabel.setVisibility(View.GONE);

            }

            tvAddress.setText(bundle.getString("address",""));
            if (!securePrefManager.getSharedValue(PROFILE_IMAGE).equalsIgnoreCase(""))
                Glide.with(this).load(securePrefManager.getSharedValue(PROFILE_IMAGE)).into(profileImage);

        }



    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.cont:
                if (Utils.hasNetwork(this)) {
                    showProgress("Please wait...");
                    presenter.getLandingDetails(securePrefManager.getSharedValue(TOKEN));
                } else {
                    showToast(getString(R.string.internet_error));
                }

                break;

        }

    }

    private String getStaticMapURL(String lat, String lng) {

        url = "http://maps.googleapis.com/maps/api/staticmap?center=" + lat + "," + lng + "&zoom=15&size=200x200&markers=color:red|label:S|" + lat + "," + lng + "&sensor=true&key=" + getString(R.string.google_api);
        return url;
    }

    @Override
    public void OnGetLandingDetails(CommomResponse response) {
        hideProgress();
        ImplementerDetails implementerDetails = response.toResponseModel(ImplementerDetails.class);
        securePrefManager.storeSharedValue(IMPLEMENTER_DETAILS, Utils.toString(implementerDetails));
        Bundle bundle = new Bundle();
        //   bundle.putSerializable("LandingData",details);
        bundle.putString("for", "landing");
        int flag = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;
        goToNextScreen(ImplemeterHomeActivity.class, bundle, flag);

    }

    @Override
    public void onGetSiteLov(CommomResponse response) {

    }

    @Override
    public void onGetImplementerAccount(CommomResponse response) {
        hideProgress();
        UserRoleModel userRoleModel = response.toResponseModel(UserRoleModel.class);
        securePrefManager.storeSharedValue(IMPLEMENTER_ACCOUNT, Utils.toString(userRoleModel.getMyAccount()));
        UserRoleModel.MyAccount myAccount = Utils.getObj(securePrefManager.getSharedValue(IMPLEMENTER_ACCOUNT), UserRoleModel.MyAccount.class);
        if (myAccount.getImages() != null)
                 Glide.with(this).load(myAccount.getImages()).into(profileImage);


            tvMobile.setText(myAccount.getPhone());
        tvName.setText(myAccount.getName());
        tvUserID.setText(myAccount.getUserId());
        if (!myAccount.getEmail().trim().isEmpty())
            tvEmail.setText(myAccount.getEmail());
        else {

            tvEmail.setVisibility(GONE);
            tvEmailLabel.setVisibility(GONE);

        }

        tvAddress.setText(myAccount.getAddress().getAddressLine1() + ", " + myAccount.getAddress().getAddressLine2() + ", " + myAccount.getAddress().getCity() + ", " + myAccount.getAddress().getState() + ", " + myAccount.getAddress().getCountry() + ", " + myAccount.getAddress().getPinCode());

    }

    @Override
    public void onGetSiteDetails(CommomResponse response) {

    }

    @Override
    public void onfetchQuote(CommomResponse response) {

    }

    @Override
    public void onFailure(String msg) {
        hideProgress();
        if (msg.equalsIgnoreCase("invalid token")){
            securePrefManager.clearAppsAllPrefs();
            startActivity(new Intent(ImplementerProfile.this, MainActivity.class));
            finish();
        }
        else {
            showToast(msg);
        }

    }
}
