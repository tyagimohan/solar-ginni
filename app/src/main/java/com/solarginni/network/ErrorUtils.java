package com.solarginni.network;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;
import retrofit2.Retrofit;

import java.lang.annotation.Annotation;

/**
 * Created by cl-macmini-01 on 11/23/17.
 * Error Utils
 */

public class ErrorUtils {

    static final int DEFAULT_STATUS_CODE = 900;

    /**
     * Prevent instantiation
     */
    private ErrorUtils() {
    }

    /**
     * Parses error from the api response
     *
     * @param response the api response
     * @param retrofit retrofit client
     * @return parsed instance of ApiError
     */
    public static ApiError parseError(final Response<?> response, final Retrofit retrofit) {
        Converter<ResponseBody, ApiError> converter = retrofit.responseBodyConverter(ApiError.class, new Annotation[0]);
        ApiError error;
        try {
            if (response.errorBody() != null) {
                error = converter.convert(response.errorBody());
                error.setCode(response.code());
            } else {
                error = new ApiError(response.code(), response.message());
            }

        } catch (Exception e) {
            int statusCode = DEFAULT_STATUS_CODE;
            // keeping empty string as we cannot reference direct strings here
            String message = "";
            if (response.code() != 0) {
                statusCode = response.code();
            }
            if (response.message() != null && !response.message().isEmpty()) {
                message = response.message();
            }
            return new ApiError(statusCode, message);
        }
        return error;
    }
}