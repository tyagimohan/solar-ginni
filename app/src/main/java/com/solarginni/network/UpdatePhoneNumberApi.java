package com.solarginni.network;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.CommomResponse;

import retrofit2.Retrofit;

public abstract class UpdatePhoneNumberApi {

    private Retrofit retrofit;
    private ApiInterface mApiInterface;

    public UpdatePhoneNumberApi() {
        retrofit= RestClient.getRetrofitBuilder();
        mApiInterface= retrofit.create(ApiInterface.class);
    }


    public void hit (String token,String userId,String phone){
        mApiInterface.updatePhone(token,phone,userId).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                onComplete(response);


            }

            @Override
            public void onError(ApiError error) {
                onFailur(error.getMessage());
            }

            @Override
            public void onFailure(Throwable throwable) {
                onFailur(throwable.getMessage());
            }
        });
    }

    public abstract void onComplete(CommomResponse response);
    protected abstract void onFailur(String msg);


}
