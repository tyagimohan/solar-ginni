package com.solarginni.network;


import com.solarginni.CommonModule.Testimonial.AddTestimonialActivity;
import com.solarginni.ComponentModel;
import com.solarginni.DBModel.APIBody.CompanyModel;
import com.solarginni.DBModel.APIBody.GuestModel;
import com.solarginni.DBModel.APIBody.ImageType;
import com.solarginni.DBModel.APIBody.ImplementerModel;
import com.solarginni.DBModel.APIBody.RaiseRequestModel;
import com.solarginni.DBModel.APIBody.RequestParam;
import com.solarginni.DBModel.APIBody.SendOtp;
import com.solarginni.DBModel.APIBody.SiteRatingModel;
import com.solarginni.DBModel.APIBody.SiteRegistrationModel;
import com.solarginni.DBModel.APIBody.UserRegisteration;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.DemandQuoteModel;
import com.solarginni.DBModel.EmpanelMentBody;
import com.solarginni.DBModel.QuotationModel;
import com.solarginni.DBModel.SendNotificationModel;
import com.solarginni.DBModel.SetComponentModel;
import com.solarginni.DBModel.SetPriceModel;
import com.solarginni.Implementer.ImplementerUserModule.ImplementerUserModel;
import com.solarginni.Implementer.SetProposal.SetProposalModel;
import com.solarginni.QuoteDetailModule.StatusModel;

import java.util.HashMap;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;

import static com.solarginni.network.ApiConstant.ACTIVATE_COMPANY;
import static com.solarginni.network.ApiConstant.ADD_FEASIBILITY;
import static com.solarginni.network.ApiConstant.ADD_TESTIMONILA;
import static com.solarginni.network.ApiConstant.CHANGE_PHONE;
import static com.solarginni.network.ApiConstant.CLOSE_REQUEST_BY_CUSTOMER;
import static com.solarginni.network.ApiConstant.COMPANY_LIST;
import static com.solarginni.network.ApiConstant.COMPANY_REGISTRATION;
import static com.solarginni.network.ApiConstant.COMPARISION;
import static com.solarginni.network.ApiConstant.CREATE_PROPOSAL;
import static com.solarginni.network.ApiConstant.FETCH_PRICED_COMPANY;
import static com.solarginni.network.ApiConstant.GET_COMPANY_DETAILS;
import static com.solarginni.network.ApiConstant.GET_COMPONENT;
import static com.solarginni.network.ApiConstant.GET_COMPONENT_DETAILS;
import static com.solarginni.network.ApiConstant.GET_COMPONENT_LIST;
import static com.solarginni.network.ApiConstant.GET_CUSTOMER_DETAILS;
import static com.solarginni.network.ApiConstant.GET_CUSTOMER_REQUEST;
import static com.solarginni.network.ApiConstant.GET_DISTRICT;
import static com.solarginni.network.ApiConstant.GET_FEASIBILITY_LOV;
import static com.solarginni.network.ApiConstant.GET_IMPLEMENTER_DETAILS;
import static com.solarginni.network.ApiConstant.GET_IMP_REQ_DETAILS_;
import static com.solarginni.network.ApiConstant.GET_IMP_SITE_DETAILS_;
import static com.solarginni.network.ApiConstant.GET_MY_ACCOUNT_DETAILS;
import static com.solarginni.network.ApiConstant.GET_PRICING;
import static com.solarginni.network.ApiConstant.GET_PROPOSAL_TERM;
import static com.solarginni.network.ApiConstant.GET_PROSPECT_LANDING_DETAILS_;
import static com.solarginni.network.ApiConstant.GET_QUOTE;
import static com.solarginni.network.ApiConstant.GET_QUOTE_REQUEST_LOV;
import static com.solarginni.network.ApiConstant.GET_REQUEST;
import static com.solarginni.network.ApiConstant.GET_REQUEST_LOV;
import static com.solarginni.network.ApiConstant.GET_SITES;
import static com.solarginni.network.ApiConstant.GET_SITE_DETAILS;
import static com.solarginni.network.ApiConstant.GET_SITE_LOVS;
import static com.solarginni.network.ApiConstant.GET_SITE_NEAR_ME;
import static com.solarginni.network.ApiConstant.GET_SITE_QUOTEIDS;
import static com.solarginni.network.ApiConstant.GET_USER;
import static com.solarginni.network.ApiConstant.GET_VERSION;
import static com.solarginni.network.ApiConstant.MANUFACTURER_LIST;
import static com.solarginni.network.ApiConstant.RAISE_REQUEST;
import static com.solarginni.network.ApiConstant.RECALL_QUOTE;
import static com.solarginni.network.ApiConstant.REFRRAL_DETAIL;
import static com.solarginni.network.ApiConstant.REGISTER_IMPLEMENTER;
import static com.solarginni.network.ApiConstant.REGISTER_USER;
import static com.solarginni.network.ApiConstant.REVOKE_QUOTE;
import static com.solarginni.network.ApiConstant.SEND_NOTIFICATION;
import static com.solarginni.network.ApiConstant.SEND_OTP;
import static com.solarginni.network.ApiConstant.SET_PROPOSAL_TERM;
import static com.solarginni.network.ApiConstant.SG_LANDING;
import static com.solarginni.network.ApiConstant.SG_PROMOTION;
import static com.solarginni.network.ApiConstant.SHOW_FEASIBILITY;
import static com.solarginni.network.ApiConstant.SITE_REGISTER;
import static com.solarginni.network.ApiConstant.SUBMIT_SITE_RATING;
import static com.solarginni.network.ApiConstant.UPDATE_COMPANY;
import static com.solarginni.network.ApiConstant.UPDATE_SITE;
import static com.solarginni.network.ApiConstant.UPDATE_USER;
import static com.solarginni.network.ApiConstant.UPGRADE_ROLE;
import static com.solarginni.network.ApiConstant.UPLOAD_IMAGE;
import static com.solarginni.network.ApiConstant.USER_REGISTRATION;
import static com.solarginni.network.ApiConstant.VERIFY_OTP;

public interface ApiInterface {


    @PUT(VERIFY_OTP)
    Call<CommomResponse> verifyOtp(@Path("otp") String otp, @Header("Authorization") String token);


    @POST(SEND_OTP)
    Call<CommomResponse> sendOtp(@Body SendOtp otp, @Header("Authorization") String token);

    @POST(REGISTER_USER)
    Call<CommomResponse> registerUser(@Body UserRegisteration user, @Header("Authorization") String token);

    @PUT(UPDATE_USER)
    Call<CommomResponse> updateUser(@Body UserRegisteration user, @Header("Authorization") String token);

    @POST(REGISTER_IMPLEMENTER)
    Call<CommomResponse> registerImplementer(@Body ImplementerModel user, @Header("Authorization") String token);

    @POST(SITE_REGISTER)
    Call<CommomResponse> registerSite(@Body SiteRegistrationModel model, @Header("Authorization") String token);


    @GET(GET_CUSTOMER_DETAILS)
    Call<CommomResponse> getCustomerDetails(@Header("Authorization") String token);

    @GET(GET_IMPLEMENTER_DETAILS)
    Call<CommomResponse> getImplementer(@Header("Authorization") String token);

    @GET(GET_SITE_LOVS)
    Call<CommomResponse> getSiteLOVs(@Header("Authorization") String token);

    @GET(GET_COMPANY_DETAILS)
    Call<CommomResponse> getCompanyDetails(@Header("Authorization") String token);

    @GET(GET_MY_ACCOUNT_DETAILS)
    Call<CommomResponse> getAccountDetails(@Header("Authorization") String token);

    @Multipart
    @POST(UPLOAD_IMAGE)
    Call<CommomResponse> uploadImage(@PartMap HashMap<String, RequestBody> partMap, @Path("type") ImageType userType, @Header("Authorization") String token);


    @POST(SUBMIT_SITE_RATING)
    Call<CommomResponse> submitRating(@Body SiteRatingModel model, @Path("siteId") String siteID, @Header("Authorization") String token);

    @GET(GET_SITE_DETAILS)
    Call<CommomResponse> getSiteDetails(@Header("Authorization") String token, @Query("siteType") String siteType, @Query("state") String state);

    @POST(RAISE_REQUEST)
    Call<CommomResponse> raiseRequest(@Header("Authorization") String token, @Path("siteId") String siteId, @Body RaiseRequestModel model);


    @GET(GET_CUSTOMER_REQUEST)
    Call<CommomResponse> getRequest(@Header("Authorization") String token, @Query("requestType") String requestType, @Query("status") String status);

    @GET(GET_REQUEST_LOV)
    Call<CommomResponse> getRequestLOV(@Header("Authorization") String token, @Path("siteId") String siteId);

    @PUT(CLOSE_REQUEST_BY_CUSTOMER)
    Call<CommomResponse> closeRequest(@Header("Authorization") String token, @Path("requestId") String requestId, @Query("note") String note, @Query("closureNotes") String closureNote, @Query("status") String status, @Query("assignTo") String assignedTo);

    @GET(GET_IMP_SITE_DETAILS_)
    Call<CommomResponse> getImpSitesDetails(@Header("Authorization") String token, @Query("siteType") String siteType, @Query("state") String state);


    @GET(GET_IMP_REQ_DETAILS_)
    Call<CommomResponse> getImpRequest(@Header("Authorization") String token);

    @GET(GET_PRICING)
    Call<CommomResponse> getPricing(@Header("Authorization") String token, @Query("state") String state, @Query("year") String year, @Query("companyId") String companyId);

    @GET(GET_PROSPECT_LANDING_DETAILS_)
    Call<CommomResponse> getProspectLandingDetails(@Header("Authorization") String token);

    @POST(ApiConstant.SAVE_FIRST_QUOTATION)
    Call<CommomResponse> saveQuotation(@Header("Authorization") String token, @Body QuotationModel model, @Path("page") String page, @Query("opt_id") String optyId);

    @POST(ApiConstant.SET_PRICE_INFO)
    Call<CommomResponse> setPriceInfo(@Header("Authorization") String token, @Body SetPriceModel model, @Path("id") String id);

    @POST(ApiConstant.ADD_EMPANELMENT_INFO)
    Call<CommomResponse> addEmapanelmentInfo(@Header("Authorization") String token, @Body EmpanelMentBody model, @Path("id") String id);

    @GET(ApiConstant.FETCH_NODAL_BODY)
    Call<CommomResponse> fetchNodalBodyInfo(@Header("Authorization") String token);

    @GET(ApiConstant.FETCH_EMPANELMENT_INFO)
    Call<CommomResponse> fetchEmapanelmentInfo(@Header("Authorization") String token, @Query("companyId") String companyId);

    @GET(ApiConstant.QUOTE_FILTER)
    Call<CommomResponse> filterQuote(@Header("Authorization") String token, @Query("distance") String distance, @Query("price") String price, @Query("siteNumber") String siteCount, @Query("optId") String
            optID, @Query("empanelment") boolean isEmpanelment,@Query("sorting")int sorting);

    @POST(ApiConstant.DEMAND_QUOTE)
    Call<CommomResponse> demandQuote(@Header("Authorization") String token, @Body DemandQuoteModel model, @Query("opt_id") String
            optID);

    @GET(ApiConstant.GET_PENDING_QUOTE)
    Call<CommomResponse> getPedningQuote(@Header("Authorization") String token);

    @GET(ApiConstant.GET_IMPLEMENTER_QUOTE)
    Call<CommomResponse> getImplementerQuote(@Header("Authorization") String token, @Query("siteType") String siteType, @Query("status") String status,@Query("createdBy") int createdBy);

    @Multipart
    @POST(ApiConstant.CHNAGE_STATUS_IMP)
    Call<CommomResponse> chnageStausForImp(@Header("Authorization") String token, @PartMap HashMap<String, RequestBody> partMap, @Query("quoteId") String quoteId
            , @Query("status") String status, @Query("price") int price,
                                           @Query("comment") String comment, @Query("panelType") String panelType, @Query("panelMake") String panelMake, @Query("inverterSize") String inverterSize, @Query("inverterMake") String inverterMake, @Query("structureType") String structureType, @Query("bidirectionalMeter") String bidirectionalMeter, @Query("distributionBox") String distributionBox, @Query("warrantyTerm") int warrantyTerm, @Query("recommendedSIze") double recommendedSIze,@Query("batteryMake") String batteryMake,@Query("url") String url);

    @Multipart
    @POST(ApiConstant.CHNAGE_STATUS_IMP)
    Call<CommomResponse> chnageStausForImp(@Header("Authorization") String token, @Query("quoteId") String quoteId
            , @Query("status") String status, @Query("price") int price,
                                           @Query("comment") String comment, @Query("recommendedSIze") double recommendedSIze);

    @PUT(ApiConstant.CHNAGE_STATUS_PROS)
    Call<CommomResponse> chnageStausForPros(@Header("Authorization") String token, @Query("status") String status, @Query("quote_id") String quote_id);

    @GET(ApiConstant.GET_NOTIFICATION)
    Call<CommomResponse> fetchNotification(@Header("Authorization") String token);

    @POST(ApiConstant.RATE_REQUEST)
    Call<CommomResponse> rateRequest(@Header("Authorization") String token, @Body RequestParam model, @Path("requestId") String
            requestID);

    @GET(ApiConstant.GET_IMPLEMENTER_DETAIL)
    Call<CommomResponse> fetchImp(@Header("Authorization") String token, @Path("userId") String id);

    @GET(ApiConstant.GET_USER_DETAILS)
    Call<CommomResponse> fetchUser(@Header("Authorization") String token, @Path("userId") String id);

    @GET(ApiConstant.FAQ)
    Call<CommomResponse> fetcFAQs(@Header("Authorization") String token);

    @GET(ApiConstant.LOGOUT)
    Call<CommomResponse> logout(@Header("Authorization") String token);

    @PUT(ApiConstant.ACTIVATE_USER)
    Call<CommomResponse> activate(@Header("Authorization") String token, @Query("status") Boolean status, @Query("userId") String userId);

    @GET(ApiConstant.GET_IMP)
    Call<ImplementerUserModel> getImpUser(@Header("Authorization") String token, @Query("filter") Integer filter);

    @POST(ADD_TESTIMONILA)
    Call<CommomResponse> addTestimonial(@Header("Authorization") String token, @Body AddTestimonialActivity.TestimonialModel filter);

    @GET(GET_FEASIBILITY_LOV)
    Call<CommomResponse> getFLov(@Header("Authorization") String token);

    @GET(SHOW_FEASIBILITY)
    Call<CommomResponse> showFeasibility(@Header("Authorization") String token, @Path("optId") String optId);

    @Multipart
    @POST(ADD_FEASIBILITY)
    Call<CommomResponse> addFeasibility(@Header("Authorization") String token, @Path("optId") String optId, @PartMap HashMap<String, RequestBody> partMap,
                                        @Query("phase") String phase,
                                        @Query("connectionId") String connectionId,
                                        @Query("budget") Integer budget,
                                        @Query("annualUnit") Integer annualUnit,
                                        @Query("sancLoad") Double sancLoad,
                                        @Query("roofSpace") Integer roofSpace,
                                        @Query("gensetD") String gensetD,
                                        @Query("southD") String southD,
                                        @Query("eastD") String eastD,
                                        @Query("westD") String westD,
                                        @Query("waterT") String waterT,
                                        @Query("timeline") String timeline);


    @GET(GET_SITE_QUOTEIDS)
    Call<CommomResponse> getIds(@Header("Authorization") String token);

    @GET(GET_QUOTE_REQUEST_LOV)
    Call<CommomResponse> getQuoteRequestLov(@Header("Authorization") String token);

    @POST(ApiConstant.RAISE_QUOTE_REQUEST)
    Call<CommomResponse> raiseQuoteRequest(@Header("Authorization") String token, @Path("quoteId") String quoteId, @Query("requestType") String requestType, @Query("comment") String comment);

    @PUT(UPGRADE_ROLE)
    Call<CommomResponse> upGradeRole(@Header("Authorization") String token, @Query("userId") String userId, @Query("role") String type);

    @GET(SG_LANDING)
    Call<CommomResponse> getSGLading(@Header("Authorization") String token);

    @POST(COMPANY_REGISTRATION)
    Call<CommomResponse> registerCompany(@Header("Authorization") String token, @Body CompanyModel model);

    @PUT(UPDATE_COMPANY)
    Call<CommomResponse> updateCompany(@Header("Authorization") String token, @Body CompanyModel model, @Path("userId") String userId);

    @GET(COMPANY_LIST)
    Call<CommomResponse> getCompanyList(@Header("Authorization") String token, @Query("keys") String key, @Query("page") int page, @Query("type") String type, @Query("state") String state);

    @GET(GET_USER)
    Call<CommomResponse> getUserList(@Header("Authorization") String token, @Query("keys") String key, @Query("page") int page, @Query("type") String type, @Query("state") String state, @Query("roles") String role);

    @GET(GET_SITES)
    Call<CommomResponse> getSiteLilst(@Header("Authorization") String token, @Query("keys") String key, @Query("page") int page, @Query("type") String type, @Query("state") String state);

    @PUT(UPDATE_SITE)
    Call<CommomResponse> updateSite(@Header("Authorization") String token, @Path("siteId") String siteId, @Body SiteRegistrationModel model);

    @GET(GET_REQUEST)
    Call<CommomResponse> getRequest(@Header("Authorization") String token, @Query("keys") String key, @Query("page") int page, @Query("type") String type, @Query("status") String status);

    @GET(GET_QUOTE)
    Call<CommomResponse> getQuote(@Header("Authorization") String token, @Query("keys") String key, @Query("page") int page, @Query("type") String type, @Query("state") String status);

    @GET(GET_VERSION)
    Call<CommomResponse> getVersion(@Header("Authorization") String token);

    @GET(GET_SITE_NEAR_ME)
    Call<CommomResponse> getSiteNearMe(@Header("Authorization") String token);

    @PUT(CHANGE_PHONE)
    Call<CommomResponse> updatePhone(@Header("Authorization") String token, @Query("phone") String phone, @Query("userId") String userId);

    @PUT(ACTIVATE_COMPANY)
    Call<CommomResponse> activateCompany(@Header("Authorization") String token, @Query("status") boolean status, @Query("userId") String userId);

    @PUT(FETCH_PRICED_COMPANY)
    Call<CommomResponse> fetchPricedCompany(@Header("Authorization") String token, @Query("key")
            String key, @Query("state") String state, @Query("month") String month, @Query("siteType") String siteType,
                                            @Query("undefined") String undefined, @Query("orderBy") String orderBy, @Query("page") int page, @Query("siteRange") int siteRange, @Query("year") int year, @Query("sort") String sort);


    @GET(MANUFACTURER_LIST)
    Call<CommomResponse> getManuFacturer(@Header("Authorization") String token,@Query("isSame") boolean isSame,@Query("state") String state);

    /*For SG*/

    @PUT(RECALL_QUOTE)
    Call<CommomResponse> recallQuote(@Header("Authorization") String token, @Query("optId") String quoteId);


    /*For Prospect*/

    @PUT(REVOKE_QUOTE)
    Call<CommomResponse> revokeQuote(@Header("Authorization") String token, @Path("quoteId") String quoteId);


    @GET(COMPARISION)
    Call<CommomResponse> getComparision(@Header("Authorization") String token, @Path("optId") String optID);

    @GET(GET_DISTRICT)
    Call<CommomResponse>   getDistrict(@Header("Authorization") String token);

    @GET(GET_COMPONENT)
    Call<ComponentModel> getComponentMake(@Header("Authorization") String token);


    @POST(ApiConstant.ADD_COMPONENT)
    Call<CommomResponse> setComponent(@Header("Authorization") String token, @Body SetComponentModel model);

    @POST(ApiConstant.GUEST_USER)
    Call<CommomResponse> guestUserApi(@Header("Authorization") String token, @Body GuestModel model);

    @GET(GET_COMPONENT_LIST)
    Call<CommomResponse> getComponentList(@Header("Authorization") String token, @Query("state") String state, @Query("siteType") String siteType, @Query("companyId") String coompanyId);

    @GET(REFRRAL_DETAIL)
    Call<CommomResponse> getReferralDeails(@Header("Authorization") String token);

    @GET(SG_PROMOTION)
    Call<CommomResponse> getSGPromotions(@Header("Authorization") String token,@Query("keys")String key,@Query("page")int page,@Query("size") int size,@Query("type")int type,@Query("state")String state);

    @POST(CREATE_PROPOSAL)
    Call<CommomResponse> createProposal(@Header("Authorization") String token,@Body StatusModel model);

    @POST(SEND_NOTIFICATION)
    Call<CommomResponse> sendNotification(@Header("Authorization") String token, @Body SendNotificationModel model);

    @POST(SET_PROPOSAL_TERM)
    Call<CommomResponse> setProposalterrm(@Header("Authorization") String token, @Body SetProposalModel  model);

    @GET(GET_COMPONENT_DETAILS)
    Call<CommomResponse> getComponentDetails(@Header("Authorization") String token, @Path("id")String id);

    @GET(GET_PROPOSAL_TERM)
    Call<CommomResponse> getProposalDetails(@Header("Authorization") String token,@Query("companyId")String companyId);

    @POST(USER_REGISTRATION)
    Call<CommomResponse> registerUserBySGIM(@Body UserRegisteration user, @Header("Authorization") String token);

}
