package com.solarginni.SiteRegistrationModule;

import android.content.Context;

import com.solarginni.DBModel.APIBody.SiteRatingModel;
import com.solarginni.DBModel.APIBody.SiteRegistrationModel;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.SiteLovsModule.SiteDetailModel.SiteLov;
import com.solarginni.network.RestClient;

import java.io.File;

public class SitePresenter implements SiteContract.Presenter, SiteContract.OnInteraction {

    private SiteContract.View view;
    private SiteInteractor interactor;


    public SitePresenter(SiteContract.View view) {
        this.view = view;
        interactor = new SiteInteractor(RestClient.getRetrofitBuilder());
    }

    @Override
    public void registerSite(Context context, SiteRegistrationModel model, String token, File file) {
        interactor.registerSite(context, model, token,file, this);

    }

    @Override
    public void update(Context context, SiteRegistrationModel model, String siteId, String token, File file) {
        interactor.update(context,model,siteId,token,file,this);
    }

    @Override
    public void rateSite(String token,String siteID, SiteRatingModel model) {
        interactor.rateSite(token,siteID,model,this);
    }

    @Override
    public void getSiteLov(String token) {
        interactor.getSiteLOVs(token,this);

    }

    @Override
    public void OnRegister(CommomResponse response) {
      view.OnRegisterSite(response);
    }

    @Override
    public void onSiteRated() {
        view.onSiteRated();

    }

    @Override
    public void onSiteUpdated(CommomResponse response) {
        view.onSiteUpdated(response);

    }

    @Override
    public void onGetSiteLovs(CommomResponse siteLov) {
        view.onGetSiteLovs(siteLov);

    }

    @Override
    public void OnFailure(String msg) {
        view.OnFailure(msg);
    }
}
