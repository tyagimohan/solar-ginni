package com.solarginni.SiteRegistrationModule;

import android.content.Context;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.APIBody.ImageType;
import com.solarginni.DBModel.APIBody.SiteRatingModel;
import com.solarginni.DBModel.APIBody.SiteRegistrationModel;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.Utility.AppConstants;
import com.solarginni.Utility.SecurePrefManager;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;
import com.solarginni.network.MultipartParams;

import java.io.File;

import retrofit2.Retrofit;

import static com.solarginni.Utility.AppConstants.BO_MANAGER;
import static com.solarginni.Utility.AppConstants.CUSTOMER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.IMPLEMENTER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.SG_ADMIN;
import static com.solarginni.Utility.AppConstants.SUPER_IMPLEMENTER_ROLE_ID;

public class SiteInteractor implements SiteContract.Interector {

    private Retrofit mRetrofit;
    private ApiInterface mApiInterface;

    public SiteInteractor(Retrofit mRetrofit) {
        this.mRetrofit = mRetrofit;
        mApiInterface = mRetrofit.create(ApiInterface.class);
    }

    @Override
    public void registerSite(Context context, SiteRegistrationModel model, String token, File file, SiteContract.OnInteraction listener) {

        SecurePrefManager securePrefManager = new SecurePrefManager(context);
        String role = securePrefManager.getSharedValue(ROLE);

        if (file == null) {
            switch (role) {
                case IMPLEMENTER_ROLE_ID:
                case SUPER_IMPLEMENTER_ROLE_ID:
                    mApiInterface.registerSite(model, token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                        @Override
                        public void onSuccess(CommomResponse commomResponse) {
                            mApiInterface.getImplementer(token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                                @Override
                                public void onSuccess(CommomResponse commomResponse) {
                                    listener.OnRegister(commomResponse);
                                }

                                @Override
                                public void onError(ApiError error) {
                                    listener.OnFailure(error.getMessage());
                                }

                                @Override
                                public void onFailure(Throwable throwable) {
                                    listener.OnFailure(throwable.getMessage());
                                }
                            });


                        }

                        @Override
                        public void onError(ApiError error) {
                            listener.OnFailure(error.getMessage());

                        }

                        @Override
                        public void onFailure(Throwable throwable) {
                            listener.OnFailure(throwable.getMessage());
                        }
                    });

                    break;

                case CUSTOMER_ROLE_ID:
                    mApiInterface.registerSite(model, token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                        @Override
                        public void onSuccess(CommomResponse commomResponse) {
                            String siteId = commomResponse.toResponseModel(String.class);
                            securePrefManager.storeSharedValue(AppConstants.SITEID, siteId);
                            mApiInterface.getCustomerDetails(token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                                @Override
                                public void onSuccess(CommomResponse commomResponse) {
                                    listener.OnRegister(commomResponse);
                                }

                                @Override
                                public void onError(ApiError error) {
                                    listener.OnFailure(error.getMessage());
                                }

                                @Override
                                public void onFailure(Throwable throwable) {
                                    listener.OnFailure(throwable.getMessage());
                                }
                            });


                        }

                        @Override
                        public void onError(ApiError error) {
                            listener.OnFailure(error.getMessage());

                        }

                        @Override
                        public void onFailure(Throwable throwable) {
                            listener.OnFailure(throwable.getMessage());
                        }
                    });

                    break;
                case SG_ADMIN:
                case BO_MANAGER:
                    mApiInterface.registerSite(model, token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                        @Override
                        public void onSuccess(CommomResponse commomResponse) {
                            listener.OnRegister(commomResponse);


                        }

                        @Override
                        public void onError(ApiError error) {
                            listener.OnFailure(error.getMessage());

                        }

                        @Override
                        public void onFailure(Throwable throwable) {
                            listener.OnFailure(throwable.getMessage());
                        }
                    });
                    break;
            }
        }
        else {
            MultipartParams multipartParams = new MultipartParams.Builder()
                    .addFile("upload", file).build();

            mApiInterface.uploadImage(multipartParams.getMap(), ImageType.SITE_TYPE, token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                @Override
                public void onSuccess(CommomResponse response) {
                    model.setImageUrl(response.toResponseModel(String.class));
                    switch (role) {
                        case IMPLEMENTER_ROLE_ID:
                        case SUPER_IMPLEMENTER_ROLE_ID:
                            mApiInterface.registerSite(model, token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                                @Override
                                public void onSuccess(CommomResponse commomResponse) {
                                    mApiInterface.getImplementer(token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                                        @Override
                                        public void onSuccess(CommomResponse commomResponse) {
                                            listener.OnRegister(commomResponse);
                                        }

                                        @Override
                                        public void onError(ApiError error) {
                                            listener.OnFailure(error.getMessage());
                                        }

                                        @Override
                                        public void onFailure(Throwable throwable) {
                                            listener.OnFailure(throwable.getMessage());
                                        }
                                    });


                                }

                                @Override
                                public void onError(ApiError error) {
                                    listener.OnFailure(error.getMessage());

                                }

                                @Override
                                public void onFailure(Throwable throwable) {
                                    listener.OnFailure(throwable.getMessage());
                                }
                            });

                            break;

                        case CUSTOMER_ROLE_ID:
                            mApiInterface.registerSite(model, token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                                @Override
                                public void onSuccess(CommomResponse commomResponse) {
                                    String siteId = commomResponse.toResponseModel(String.class);
                                    securePrefManager.storeSharedValue(AppConstants.SITEID, siteId);
                                    mApiInterface.getCustomerDetails(token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                                        @Override
                                        public void onSuccess(CommomResponse commomResponse) {
                                            listener.OnRegister(commomResponse);
                                        }

                                        @Override
                                        public void onError(ApiError error) {
                                            listener.OnFailure(error.getMessage());
                                        }

                                        @Override
                                        public void onFailure(Throwable throwable) {
                                            listener.OnFailure(throwable.getMessage());
                                        }
                                    });


                                }

                                @Override
                                public void onError(ApiError error) {
                                    listener.OnFailure(error.getMessage());

                                }

                                @Override
                                public void onFailure(Throwable throwable) {
                                    listener.OnFailure(throwable.getMessage());
                                }
                            });

                            break;
                        case SG_ADMIN:
                        case BO_MANAGER:
                            mApiInterface.registerSite(model, token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                                @Override
                                public void onSuccess(CommomResponse commomResponse) {
                                    listener.OnRegister(commomResponse);


                                }

                                @Override
                                public void onError(ApiError error) {
                                    listener.OnFailure(error.getMessage());

                                }

                                @Override
                                public void onFailure(Throwable throwable) {
                                    listener.OnFailure(throwable.getMessage());
                                }
                            });
                            break;
                    }

                }

                @Override
                public void onError(ApiError error) {
                    listener.OnFailure(error.getMessage());

                }

                @Override
                public void onFailure(Throwable throwable) {
                    listener.OnFailure(throwable.getMessage());


                }
            });
        }


    }

    @Override
    public void update(Context context, SiteRegistrationModel model,String token, String siteId,  File file, SiteContract.OnInteraction listener) {
        if (file == null) {
                    mApiInterface.updateSite( token,siteId,model).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                        @Override
                        public void onSuccess(CommomResponse commomResponse) {
                            listener.onSiteUpdated(commomResponse);


                        }

                        @Override
                        public void onError(ApiError error) {
                            listener.OnFailure(error.getMessage());

                        }

                        @Override
                        public void onFailure(Throwable throwable) {
                            listener.OnFailure(throwable.getMessage());
                        }
                    });
            }

        else {
            MultipartParams multipartParams = new MultipartParams.Builder()
                    .addFile("upload", file).build();

            mApiInterface.uploadImage(multipartParams.getMap(), ImageType.SITE_TYPE, token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                @Override
                public void onSuccess(CommomResponse response) {
                    model.setImageUrl(response.toResponseModel(String.class));
                            mApiInterface.updateSite( token,siteId,model).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                                @Override
                                public void onSuccess(CommomResponse commomResponse) {
                                  listener.onSiteUpdated(response);
                                }

                                @Override
                                public void onError(ApiError error) {
                                    listener.OnFailure(error.getMessage());

                                }

                                @Override
                                public void onFailure(Throwable throwable) {
                                    listener.OnFailure(throwable.getMessage());
                                }
                            });

                }

                @Override
                public void onError(ApiError error) {
                    listener.OnFailure(error.getMessage());

                }

                @Override
                public void onFailure(Throwable throwable) {
                    listener.OnFailure(throwable.getMessage());


                }
            });
        }
    }

    @Override
    public void rateSite(String token, String siteID, SiteRatingModel model, SiteContract.OnInteraction listener) {
        mApiInterface.submitRating(model, siteID, token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                listener.onSiteRated();
            }

            @Override
            public void onError(ApiError error) {
                listener.OnFailure(error.getMessage());
            }

            @Override
            public void onFailure(Throwable throwable) {
                listener.OnFailure(throwable.getMessage());
            }
        });

    }

    @Override
    public void getSiteLOVs(String token, SiteContract.OnInteraction listener) {
        mApiInterface.getSiteLOVs(token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
            @Override
            public void onSuccess(CommomResponse commomResponse) {

                listener.onGetSiteLovs(commomResponse);

            }

            @Override
            public void onError(ApiError error) {
                listener.OnFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                listener.OnFailure(throwable.getMessage());

            }
        });

    }
}
