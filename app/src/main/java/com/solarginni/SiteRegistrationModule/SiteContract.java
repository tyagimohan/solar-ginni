package com.solarginni.SiteRegistrationModule;

import android.content.Context;

import com.solarginni.DBModel.APIBody.SiteRatingModel;
import com.solarginni.DBModel.APIBody.SiteRegistrationModel;
import com.solarginni.DBModel.CommomResponse;

import java.io.File;

public interface SiteContract {

    interface View {
        void OnRegisterSite(CommomResponse response);
        void onSiteUpdated(CommomResponse response);
        void onSiteRated();
        void OnFailure(String msg);
        void onGetSiteLovs(CommomResponse siteLov);

    }

    interface Presenter {
        void registerSite(Context context, SiteRegistrationModel model, String token, File file);

        void update(Context context, SiteRegistrationModel model, String siteId,String token, File file);
        void rateSite(String token,String siteID, SiteRatingModel model);
        void getSiteLov(String token);
    }

    interface Interector {
        void registerSite(Context context, SiteRegistrationModel model, String token, File file, OnInteraction listener);
        void update(Context context, SiteRegistrationModel model, String siteId,String token, File file, OnInteraction listener);

        void rateSite(String token,String siteID, SiteRatingModel model, OnInteraction listener);
        void getSiteLOVs(String token, OnInteraction listener);

    }

    interface OnInteraction {
        void OnRegister(CommomResponse response);
        void onSiteRated();
        void onSiteUpdated(CommomResponse response);

        void onGetSiteLovs(CommomResponse siteLov);
        void OnFailure(String msg);

    }
}
