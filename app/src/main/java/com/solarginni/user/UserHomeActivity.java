package com.solarginni.user;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.solarginni.Base.BaseActivity;
import com.solarginni.CommonModule.FAQs.FAQsActivity;
import com.solarginni.CommonModule.LogoutApi;
import com.solarginni.CommonModule.MainActivity;
import com.solarginni.CommonModule.Referral.ReferralActivity;
import com.solarginni.CommonModule.Testimonial.AddTestimonialActivity;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.SiteDetailsModel.SiteDetailsModel;
import com.solarginni.DBModel.UserRoleModel;
import com.solarginni.GetDetails.Details;
import com.solarginni.NotificationModule.NotificationActivity;
import com.solarginni.ProspectUser.CustomerQuotationListFragment;
import com.solarginni.ProspectUser.GetQuotationActivity;
import com.solarginni.R;
import com.solarginni.SiteLovsModule.UserContract;
import com.solarginni.SiteLovsModule.SiteDetailModel.SiteLov;
import com.solarginni.SiteLovsModule.UserDetailsPresenter;
import com.solarginni.Utility.AppConstants;
import com.solarginni.Utility.Converter;
import com.solarginni.Utility.Utils;
import com.solarginni.user.RequestDetailsModule.RequestListFragment;
import com.solarginni.user.UserSiteModule.SiteListingFragment;

import static com.solarginni.Utility.AppConstants.NOTIFICATION_COUNT;
import static com.solarginni.Utility.AppConstants.OPTYID;
import static com.solarginni.Utility.AppConstants.TOKEN;

/**
 * Created by Mohan on 3/1/20.
 */

public class UserHomeActivity extends BaseActivity implements View.OnClickListener, UserContract.View, BottomNavigationView.OnNavigationItemSelectedListener {

    private static final int REQUEST_FOR_SITE = 5;
    private static final int ADD_TESTIMONILA = 7;
    private static final int REQUEST_FOR_PROFILE = 10;
    public static LinearLayout siteLay, reqLay;
    public static BottomNavigationView bottomNavigationView;
    public FloatingActionButton registerSite, getQutation, logout;

    private LinearLayout homeLay, impLay, moreLay;
    private ImageView myAccount;
    private Boolean testimonialCreated = false;
    private ActionBar actionBar;
    private UserDetailsPresenter presenter;
    private int notificationCount = 0;
    private int exitCount=0;


    @Override
    public int getLayout() {
        return R.layout.activity_home;
    }

    @Override
    public void initViews() {
        /*TextView*/

        registerSite = findViewById(R.id.regiterSite);
        getQutation = findViewById(R.id.getQutation);
        logout = findViewById(R.id.logout);
        registerSite.setOnClickListener(this);
        getQutation.setOnClickListener(this);
        logout.setOnClickListener(this);
        bottomNavigationView = findViewById(R.id.nav_bottom);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        /*LinearLayout*/
        homeLay = findViewById(R.id.home_lay);
        siteLay = findViewById(R.id.site_lay);
        reqLay = findViewById(R.id.request_lay);
        impLay = findViewById(R.id.imp_lay);
        moreLay = findViewById(R.id.more_lay);

        /*ImageView*/


        myAccount = findViewById(R.id.myAccount);

        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        presenter = new UserDetailsPresenter(this);

        getSupportActionBar().setTitle("");

        toolbar.setTitle("");

    }

    @Override
    public void setUp() {
        Bundle bundle = getIntent().getExtras();


        notificationCount = Integer.valueOf(securePrefManager.getSharedValue(NOTIFICATION_COUNT));
        invalidateOptionsMenu();
        loadFragment(new CustomerLandingFragment(), bundle, CustomerLandingFragment.class.getSimpleName());


        testimonialCreated = bundle.getBoolean("testimonials",false);

        homeLay.setOnClickListener(this);
        siteLay.setOnClickListener(this);
        reqLay.setOnClickListener(this);
        impLay.setOnClickListener(this);
        moreLay.setOnClickListener(this);
        myAccount.setOnClickListener(this);

    }

    @Override
    public void onBackPressed() {
        exitCount++;
        if (exitCount > 1)
            super.onBackPressed();
        else {
            Handler handler = new Handler();
            handler.postDelayed(() -> exitCount = 0, 3000);
            Toast.makeText(this, "Press again to exit the application", Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.regiterSite:
                Bundle bundle = getIntent().getExtras();
                bundle.putBoolean("isFromLanding", true);
                Intent intent = new Intent(UserHomeActivity.this, SiteRegistrationActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, REQUEST_FOR_SITE);


                securePrefManager.setScreenValue("Site Registration");
                break;
            case R.id.myAccount:
                presenter.getAccount(securePrefManager.getSharedValue(TOKEN));

                break;
            case R.id.getQutation:
                securePrefManager.storeSharedValue(OPTYID, "-1");
                goToNextScreen(GetQuotationActivity.class, null);

                break;
            case R.id.logout:
                securePrefManager.clearAppsAllPrefs();
                startActivity(new Intent(UserHomeActivity.this, MainActivity.class));
                finish();

                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_FOR_SITE|requestCode==ADD_TESTIMONILA) {
            if (resultCode == RESULT_OK) {
                bottomNavigationView.setSelectedItemId(R.id.nav_home);

            }
        }
        else if (requestCode==REQUEST_FOR_PROFILE){
            bottomNavigationView.setSelectedItemId(R.id.nav_home);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.alert_menu, menu);
        MenuItem menuItem2 = menu.findItem(R.id.notification_action);
        menuItem2.setIcon(Converter.convertLayoutToImage(UserHomeActivity.this, notificationCount, R.drawable.ic_bell));
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.notification_action:
                startActivity(new Intent(UserHomeActivity.this, NotificationActivity.class));
                notificationCount = 0;
                invalidateOptionsMenu();
                break;
        }
        return true;
    }

    @Override
    public void onGetSiteLov(SiteLov details) {
        hideProgress();
        securePrefManager.storeSharedValue(AppConstants.SITELOVS, Utils.toString(details));
        Bundle bundle = getIntent().getExtras();

        bundle.putBoolean("isFromLanding", true);
        goToNextScreen(SiteRegistrationActivity.class, bundle);
        securePrefManager.setScreenValue("Site Registration");

    }

    @Override
    public void onGetAccountDetails(CommomResponse response) {
        UserRoleModel userRoleModel = response.toResponseModel(UserRoleModel.class);
        securePrefManager.storeSharedValue(AppConstants.ACCOUNT_MODEL, Utils.toString(userRoleModel.getMyAccount()));
        Bundle bundle = new Bundle();
        bundle.putString("isFrom", "Landing");
        Intent intent = new Intent(UserHomeActivity.this,MyProfileActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent,REQUEST_FOR_PROFILE);



    }

    @Override
    public void onGetLandingDetails(CommomResponse response) {
        hideProgress();
        registerSite.hide();
        Details details = response.toResponseModel(Details.class);
        notificationCount = details.getNotificationCount();
        Bundle bundle = new Bundle();
        testimonialCreated = details.testimonialCreated;
        bundle.putSerializable("LandingData", details);
        bundle.putString("for", "landing");
        loadFragment(new CustomerLandingFragment(), bundle, CustomerLandingFragment.class.getSimpleName());
        invalidateOptionsMenu();


    }

    @Override
    public void onGetSiteDetails(CommomResponse response) {
        hideProgress();
        registerSite.show();
        SiteDetailsModel model = response.toResponseModel(SiteDetailsModel.class);

        securePrefManager.storeSharedValue(AppConstants.SITEDETAILSMODEL, Utils.toString(model));

        loadFragment(new SiteListingFragment(), null, SiteListingFragment.class.getSimpleName());


    }

    @Override
    public void OnGetSGDetails(CommomResponse response) {
        hideProgress();
        securePrefManager.storeSharedValue(AppConstants.REQUEST_DATA, Utils.toString(response));

        loadFragment(new RequestListFragment(), null, RequestListFragment.class.getSimpleName());


    }


    @Override
    public void onFailure(String msg) {
        hideProgress();
        if (msg.equalsIgnoreCase("invalid token")) {
            securePrefManager.clearAppsAllPrefs();
            startActivity(new Intent(UserHomeActivity.this, MainActivity.class));
            finish();
        } else
            showToast(msg);
    }

    @Override
    public void onGetProspectDetails(CommomResponse commomResponse) {

    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.nav_home:
                getQutation.hide();
                if (Utils.hasNetwork(this)) {
                    showProgress("Please wait..");
                    presenter.getLandingDetails(securePrefManager.getSharedValue(TOKEN));
                } else {
                    showToast(getString(R.string.internet_error));
                }
                break;
            case R.id.nav_site:

                getQutation.hide();
                if (Utils.hasNetwork(this)) {
                    showProgress("Please wait..");

                    presenter.getSiteDetails(securePrefManager.getSharedValue(TOKEN));
                } else {
                    showToast(getString(R.string.internet_error));

                }
                break;
            case R.id.nav_request:
                if (Utils.hasNetwork(this)) {
                    registerSite.hide();
                    getQutation.hide();
                    loadFragment(new RequestListFragment(), null, RequestListFragment.class.getSimpleName());

                } else {
                    showToast(getString(R.string.internet_error));

                }
                break;
            case R.id.nav_quote:
                registerSite.hide();
                getQutation.show();
                loadFragment(new CustomerQuotationListFragment(), null, CustomerQuotationListFragment.class.getSimpleName());
                break;
            case R.id.nav_more:
                registerSite.hide();
                showFilterDialog(this);
                break;
        }
        return true;
    }

    private void showFilterDialog(Context context) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.more_layout, null);
        Animation slideUpIn;
        slideUpIn = AnimationUtils.loadAnimation(this, R.anim.slide_in_bottom);
        dialogView.startAnimation(slideUpIn);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);
        AlertDialog alertDialog = dialogBuilder.create();
        RelativeLayout logout, faqs, addTestimonial,refer;

        logout = dialogView.findViewById(R.id.logout);
        refer = dialogView.findViewById(R.id.refer);
        faqs = dialogView.findViewById(R.id.faqs);
        addTestimonial = dialogView.findViewById(R.id.addTestimonial);
        dialogView.findViewById(R.id.share).setVisibility(View.GONE);
        if (testimonialCreated) {
            addTestimonial.setVisibility(View.GONE);
        } else {
            addTestimonial.setVisibility(View.VISIBLE);
        }


        logout.setOnClickListener(view -> {
            if (Utils.hasNetwork(this)) {
                showProgress("Please wait...");
                LogoutApi api = new LogoutApi() {
                    @Override
                    public void onComplete(CommomResponse response) {
                        hideProgress();
                        securePrefManager.clearAppsAllPrefs();
                        startActivity(new Intent(UserHomeActivity.this, MainActivity.class));
                        finish();
                    }

                    @Override
                    protected void onFailur(String msg) {
                        hideProgress();
                        securePrefManager.clearAppsAllPrefs();
                        startActivity(new Intent(UserHomeActivity.this, MainActivity.class));
                        finish();

                    }
                };
                api.hit(securePrefManager.getSharedValue(TOKEN));
            } else {
                showToast("Please check Internet");

            }

            alertDialog.dismiss();
        });

        faqs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToNextScreen(FAQsActivity.class, null);
                alertDialog.dismiss();
            }
        });

        refer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(UserHomeActivity.this, ReferralActivity.class));
            }
        });

        addTestimonial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(UserHomeActivity.this,AddTestimonialActivity.class),ADD_TESTIMONILA);;
                alertDialog.dismiss();
            }
        });


        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.BOTTOM;

        window.setAttributes(wlp);

        alertDialog.show();

    }


}
