package com.solarginni.user;

import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.solarginni.Base.BaseFragment;
import com.solarginni.CommonModule.Testimonial.StatePagerAdapter;
import com.solarginni.GetDetails.Details;
import com.solarginni.Implementer.ImplementerDetails.ImplemeterDetailsActivity;
import com.solarginni.ProspectUser.GetQuotationActivity;
import com.solarginni.R;
import com.solarginni.Utility.Utils;
import com.solarginni.user.SiteNearMe.SiteNearMeActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.solarginni.Utility.AppConstants.DETAILS;
import static com.solarginni.Utility.AppConstants.OPTYID;

public class CustomerLandingFragment extends BaseFragment implements View.OnClickListener {
    private TextView tvSiteCount, tvImpCount, tvReqCount, tvTotalSiteCount, tvMontnCount, tvYearCount, topImp, topImpState;
    private LinearLayout lay1, lay2, lay8;
    private ViewPager adViewPager;
    private TabLayout adTabIndicator;
    private int adCurrentPage = 0, tmCurentPage = 0;
    private Handler adHandler, tmHandler;
    private AdviewPageAdapter adviewPageAdapter;
    private ViewPager tmViewpager;
    private TabLayout tmIndicator;
    Timer timer, tmTimer;
    final long DELAY_MS = 500;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 3000;
    private String[] array ;


    @Override
    public int getLayout() {
        return R.layout.customer_landing_page;
    }

    @Override
    public void initViews(View view) {
        tvImpCount = view.findViewById(R.id.tvImpSiteCount);
        tvSiteCount = view.findViewById(R.id.implementerName);
        tvReqCount = view.findViewById(R.id.tvReqCount);
        tvTotalSiteCount = view.findViewById(R.id.tvTotalSiteCount);
        lay1 = view.findViewById(R.id.lay1);
        lay2 = view.findViewById(R.id.lay2);
        lay8 = view.findViewById(R.id.lay8);
        tvMontnCount = view.findViewById(R.id.tvNewMonthSiteCount);
        tvYearCount = view.findViewById(R.id.tvNewYearSiteCount);
        topImp = view.findViewById(R.id.topImp);
        topImpState = view.findViewById(R.id.topImpState);

        adViewPager = view.findViewById(R.id.adViewPager);
        tmViewpager = view.findViewById(R.id.tmViewPager);
        adTabIndicator = view.findViewById(R.id.advTabIndicator);
        tmIndicator = view.findViewById(R.id.tmTabIndicator);

        array = new String[getResources().getStringArray(R.array.advertisment_image).length];
        array=getResources().getStringArray(R.array.advertisment_image);

        view.findViewById(R.id.lay7).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(openLink("https://solarginie.com/evaluator"));
            }
        });
        view.findViewById(R.id.needEvalator).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(openLink("https://solarginie.com/evaluator"));
            }
        });


    }

    @Override
    public void setUp() {
        Details details;
        String isFor = getArguments().getString("for", "");


        if (isFor.trim().equalsIgnoreCase("landing")) {
            details = (Details) getArguments().getSerializable("LandingData");
        } else {
            details = Utils.getObj(securePrefManager.getSharedValue(DETAILS), Details.class);
        }


        tvImpCount.setText(String.valueOf(details.getTotalImplementerSite()));
        tvMontnCount.setText(String.valueOf(details.getLastMonthSite()));
        tvReqCount.setText(String.valueOf(details.getTotalRequest()));
        tvSiteCount.setText(String.valueOf(details.getTotalSite()));
        tvTotalSiteCount.setText(String.valueOf(details.getTotalSolarGinieSite()));
        tvYearCount.setText(String.valueOf(details.getCurrentYearSite()));
        topImp.setText(details.getTopImplCity());
        topImpState.setText(details.getTopImplementerState());


        HandlerViewPager(details);

        if (!topImp.getText().toString().equalsIgnoreCase("No Implementer Found")){
            topImp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("isFormasking", true);
                    bundle.putString("id", details.getTopImplCityUserId());
                    goToNextScreen(ImplemeterDetailsActivity.class, bundle);
                }
            });
        }

        if (!topImpState.getText().toString().equalsIgnoreCase("No Implementer Found")){
            topImpState.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("isFormasking", true);
                    bundle.putString("id", details.getTopImplementerStateUserId());
                    goToNextScreen(ImplemeterDetailsActivity.class, bundle);
                }
            });
        }


        getView().findViewById(R.id.nearSite).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToNextScreen(SiteNearMeActivity.class, null);
            }
        });getView().findViewById(R.id.siteLabel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToNextScreen(SiteNearMeActivity.class, null);
            }
        });




        tvSiteCount.setOnClickListener(this);
        lay1.setOnClickListener(this);
        lay2.setOnClickListener(this);
        lay8.setOnClickListener(this);
        tvReqCount.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.implementerName:
            case R.id.lay1:
                ((UserHomeActivity) getActivity()).bottomNavigationView.setSelectedItemId(R.id.nav_site);
                break;
            case R.id.tvReqCount:
            case R.id.lay2:
                ((UserHomeActivity) getActivity()).bottomNavigationView.setSelectedItemId(R.id.nav_request);


                break;
            case R.id.lay8:
                securePrefManager.storeSharedValue(OPTYID, "-1");
                goToNextScreen(GetQuotationActivity.class, null);


                break;
        }

    }

    private class AdviewPageAdapter extends PagerAdapter {

        private List<String> adList = new ArrayList<>();


        public AdviewPageAdapter(List<String> adList) {
            this.adList = adList;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {

            View view = LayoutInflater.from(getContext()).inflate(R.layout.advertise_layout, container, false);
            RoundedImageView imageView = view.findViewById(R.id.advertisementImage);

            Glide.with(getContext()).load(adList.get(position)).into(imageView);

            //ImageLoader.getInstance().displayImage(adList.get(position), imageView);

            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return adList.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
            return view == o;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {

        }
    }


    private void HandlerViewPager(Details details) {

        adviewPageAdapter = new AdviewPageAdapter(Arrays.asList(array));
        adViewPager.setAdapter(adviewPageAdapter);
        adTabIndicator.setupWithViewPager(adViewPager, true);

        adHandler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (adCurrentPage == array.length) {
                    adCurrentPage = 0;
                }
                adViewPager.setCurrentItem(adCurrentPage++, true);
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                adHandler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);

        adViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                adCurrentPage = i;
                adViewPager.setCurrentItem(adCurrentPage++, true);


            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });



        StatePagerAdapter adapter = new StatePagerAdapter(getChildFragmentManager(), details.getTestimonialModelList());

        tmViewpager.setAdapter(adapter);

        tmIndicator.setupWithViewPager(tmViewpager);


        tmHandler = new Handler();
        final Runnable UpdateTm = new Runnable() {
            public void run() {
                if (tmCurentPage == details.getTestimonialModelList().size()) {
                    tmCurentPage = 0;
                }
                tmViewpager.setCurrentItem(tmCurentPage++, true);
            }
        };

        tmTimer = new Timer(); // This will create a new Thread
        tmTimer.schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                tmHandler.post(UpdateTm);
            }
        }, DELAY_MS, PERIOD_MS);

        tmViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                tmCurentPage = i;
                tmViewpager.setCurrentItem(tmCurentPage++, true);

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

    }

}
