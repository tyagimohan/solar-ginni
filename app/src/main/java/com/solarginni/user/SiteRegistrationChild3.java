package com.solarginni.user;

import android.content.Context;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.solarginni.Base.BaseFragment;
import com.solarginni.CustomizeView.CompanyListDialog;
import com.solarginni.DBModel.APIBody.InverterModelList;
import com.solarginni.DBModel.CompanyMakeModel;
import com.solarginni.R;
import com.solarginni.SiteLovsModule.SiteDetailModel.SiteLov;
import com.solarginni.Utility.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.solarginni.Utility.AppConstants.SITELOVS;

/**
 * Created by Mohan on 16/1/20.
 */

public class SiteRegistrationChild3 extends BaseFragment implements View.OnClickListener {

    public static EditText etPanelSize, batterySize, batterySerial, inverterCount;
    private String[] panelTypeArray, invertelModelArray;
    public static EditText tvPanelMake, tvPanelType;
    public static EditText tvBatteryMake;
    public static LinearLayout linearLayout;
    private View dynamicEntryView;
    public static String panelMakeId, inverterMakeId, batteryId;
    private Boolean isExtraLayout = true;
    public static LinearLayout batteryLayout;
    private SiteLov details;
    private List<CompanyMakeModel> panelMakeArray = new ArrayList<>();
    private List<CompanyMakeModel> inverterMakeArray = new ArrayList<>();
    private List<CompanyMakeModel> batteryArray = new ArrayList<>();
    private String[] inverterArray = {"1", "2", "3"};
    public static boolean isListPrepared = false;
    public static List<InverterModelList> lists;


    @Override
    public int getLayout() {
        return R.layout.site_registration_part3;
    }

    @Override
    public void initViews(View view) {
        etPanelSize = view.findViewById(R.id.panelSize);
        tvPanelType = view.findViewById(R.id.panelType);
        tvPanelMake = view.findViewById(R.id.panelMake);
        inverterCount = view.findViewById(R.id.inverterCount);
        batteryLayout = view.findViewById(R.id.batteryLayout);
        batterySize = view.findViewById(R.id.batterySize);
        batterySerial = view.findViewById(R.id.batterySerial);
        tvBatteryMake = view.findViewById(R.id.batteryMake);
        linearLayout = view.findViewById(R.id.linearLayout);

        disableSuggestion(etPanelSize);
        disableSuggestion(tvBatteryMake);
        disableSuggestion(tvPanelMake);
        disableSuggestion(batterySerial);
        disableSuggestion(batterySize);
        disableSuggestion(tvPanelType);
        disableSuggestion(inverterCount);
        // tvInverterMake = view.findViewById(R.id.inverterMake);
        setupUI(view.findViewById(R.id.parent));
        view.clearFocus();


    }

    @Override
    public void setUp() {

        details = Utils.getObj(securePrefManager.getSharedValue(SITELOVS), SiteLov.class);


        panelMakeArray.addAll(details.getSiteDetails().getPanelMake());
        inverterMakeArray.addAll(details.getSiteDetails().getInverterMake());
        batteryArray.addAll(details.getSiteDetails().getBatteryMake());

        panelTypeArray = new String[details.getSiteDetails().getPanelType().getLovValues().size()];
        for (int i = 0; i < details.getSiteDetails().getPanelType().getLovValues().size(); i++) {
            panelTypeArray = details.getSiteDetails().getPanelType().getLovValues().toArray(panelTypeArray);
        }
        inverterCount.setText("1");
        createDynamicLayout(1);


        tvPanelMake.setOnClickListener(this);
        tvPanelType.setOnClickListener(this);
        //tvInverterMake.setOnClickListener(this);
        tvBatteryMake.setOnClickListener(this);
        inverterCount.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.panelMake:
                CompanyListDialog.show(getActivity(), panelMakeArray, item -> {
                    tvPanelMake.setText(item.getCompanyName());
                    panelMakeId = item.getCompanyId();

                });
                break;
            case R.id.panelType:
                showAlertWithList(panelTypeArray, R.string.panel_type, tvPanelType);
                break;
            case R.id.inverterMake:
                CompanyListDialog.show(getActivity(), inverterMakeArray, item -> {
                    inverterMakeId = item.getCompanyId();
                });

                break;
            case R.id.batteryMake:
                CompanyListDialog.show(getActivity(), batteryArray, item -> {
                    tvBatteryMake.setText(item.getCompanyName());
                    batteryId = item.getCompanyId();

                });
                break;
            case R.id.inverterCount:
                showAlertWithListForCount(inverterArray, R.string.select_number, inverterCount);
                break;

        }
    }


    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    ///  view.requestFocus();
                    hideKeyboard();
                    v.clearFocus();
                    return false;
                }
            });
        }
    }


    public void showAlertWithListForCount(final String[] dataArray, final @StringRes int title, final TextView textView) {
        hideKeyboard(getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(title));
        builder.setItems(dataArray, (dialog, item) -> {
            textView.setText(dataArray[item]);
            createDynamicLayout(Integer.parseInt(dataArray[item]));
        });
        AlertDialog alerts = builder.create();
        alerts.show();
    }


    private void createDynamicLayout(int size) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        linearLayout.removeAllViews();

        for (int i = 0; i < size; i++) {
            dynamicEntryView = inflater.inflate(R.layout.child_layout, null);

            linearLayout.addView(dynamicEntryView);

            View view = linearLayout.getChildAt(i);

            TextView inverterMake = view.findViewById(R.id.inverterMake);
            TextView inertMakeId = view.findViewById(R.id.inertMakeId);
            disableSuggestion(inverterMake);
            disableSuggestion(((EditText) view.findViewById(R.id.inverterModel)));
            disableSuggestion(((EditText) view.findViewById(R.id.inverterSize)));



            inverterMake.setOnClickListener(clickHandler(inverterMake, inertMakeId));
        }


    }

    private View.OnClickListener clickHandler(TextView textView, TextView inverterId) {

        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CompanyListDialog.show(getActivity(), inverterMakeArray, item -> {
                    textView.setText(item.getCompanyName());
                    inverterId.setText(item.getCompanyId());
                });


            }
        };
    }

    public static void prepareInverterLis() {
        isListPrepared = false;
        lists = new ArrayList<>();


        for (int i = 0; i < Integer.parseInt(inverterCount.getText().toString()); i++) {
            View view = linearLayout.getChildAt(i);
            InverterModelList model = new InverterModelList();
            EditText textView = ((EditText) view.findViewById(R.id.inertMakeId));
            if (textView.getText().toString().equalsIgnoreCase("")) {

                isListPrepared = false;
                break;
            } else {
                isListPrepared = true;
                model.setInverterMake(textView.getText().toString());
                model.setInverterModel(((EditText) view.findViewById(R.id.inverterModel)).getText().toString());
                model.setInverterSize(((EditText) view.findViewById(R.id.inverterSize)).getText().toString());
                lists.add(model);

            }


        }


    }
}