package com.solarginni.user.SiteNearMe;

public class SiteNearMeTable {

    public static final String SITE_NEAR_ME_TABLE= "site_near_me";
    public static final String SITE_TYPE= "site_type";
    public static final String SITE_SIZE= "site_size";
    public static final String DISTANCE= "distance";
    public static final String INSTALLED_BY= "installed_by";
    public static final String INSTALLED_ON= "installed_on";
    public static final String PANEL_MAKE= "panel_make";
    public static final String INVERTER_MAKE= "inverter_make";
    public static final String IMPLEMENTER_ID= "implementer_id";



    public static final String CREATE_TABLE ="CREATE TABLE IF NOT EXISTS " + SITE_NEAR_ME_TABLE + "("

            +SITE_TYPE + " TEXT,"
            +INSTALLED_BY + " TEXT,"
            +INSTALLED_ON + " TEXT,"
            +SITE_SIZE + " DOUBLE,"
            +PANEL_MAKE + " TEXT,"
            +DISTANCE + " DOUBLE,"
            +INVERTER_MAKE + " TEXT, "
            +IMPLEMENTER_ID + " TEXT );";



    public static final String INSERT_DATA=" INSERT INTO "+ SITE_NEAR_ME_TABLE + "("
            + SITE_TYPE + ","
            + INSTALLED_BY + ","
            + INSTALLED_ON + ","
            + SITE_SIZE + ","
            + PANEL_MAKE + ","
            + DISTANCE + ","
            + INVERTER_MAKE + ","
            + IMPLEMENTER_ID
            + " ) values(?,?,?,?,?,?,?,?);";

}
