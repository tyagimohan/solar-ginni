package com.solarginni.user.SiteNearMe;

import android.content.Context;

import java.util.List;

public interface SiteNearMeContract {

    interface View {
        void OnFetchSites(List<SiteNearMeModel.SiteNearMeDetails> list);

        void onFailure(String msg);
    }

    interface Presenter {
        void fetchSites(Context context, String token);

        void applyFilterAndSort(Context context, String sortingKey, String siteType, int distance);
    }

    interface Interactor {
        void fetchSites(Context context, String token, OnInteraction lisnter);

        void applyFilterAndSort(Context context, String sortingKey, String siteType, int distance, OnInteraction lisnter);
    }

    interface OnInteraction {
        void OnFetchSites(List<SiteNearMeModel.SiteNearMeDetails> list);

        void onFailure(String msg);
    }


}
