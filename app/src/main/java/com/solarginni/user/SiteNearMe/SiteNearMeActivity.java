package com.solarginni.user.SiteNearMe;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import com.solarginni.Base.BaseActivity;
import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.CommonModule.MainActivity;
import com.solarginni.Implementer.ImplementerDetails.ImplemeterDetailsActivity;
import com.solarginni.R;
import com.solarginni.Utility.Utils;
import com.xw.repo.BubbleSeekBar;

import java.util.ArrayList;
import java.util.List;

import static com.solarginni.Utility.AppConstants.TOKEN;

public class SiteNearMeActivity extends BaseActivity implements View.OnClickListener, SiteNearMeContract.View {

    public String sitetype = "All";
    SortingAdapter sortingAdapter;
    List<SortingModel> sortedList = new ArrayList<>();
    private Button religiousSite, commercialSite, industrialSite, institutionalSite, residentialSite, otherSite;
    private int distanceFilterVal = 500;
    private TextView lay1, lay2;
    private SiteNearMePresenter presenter;
    private RecyclerView recyclerView;
    private String sortKey = "Site Distance -- Low to High";


    @Override
    public int getLayout() {
        return R.layout.site_near_me;
    }

    @Override
    public void initViews() {


        lay1 = findViewById(R.id.lay1);
        lay2 = findViewById(R.id.lay2);
        recyclerView = findViewById(R.id.recyclerView);

        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.addItemDecoration(new BaseRecyclerAdapter.DividerItemDecoration(this, RecyclerView.VERTICAL));

        presenter = new SiteNearMePresenter(this);
        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });

        sortingAdapter = new SortingAdapter(this);
        prepaerList();
        sortingAdapter.addAll(sortedList);

        if (Utils.hasNetwork(this)) {
            showProgress("Please wait...");
            presenter.fetchSites(this, securePrefManager.getSharedValue(TOKEN));
        } else {
            showToast("Please check Internet Connection");

        }

    }

    @Override
    public void setUp() {


        lay1.setOnClickListener(this);
        lay2.setOnClickListener(this);

    }


    private void showSortingDialog(Context context) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.sorting_layout, null);
        Animation slideUpIn;
        slideUpIn = AnimationUtils.loadAnimation(this, R.anim.slide_in_bottom);
        dialogView.startAnimation(slideUpIn);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);
        AlertDialog alertDialog = dialogBuilder.create();

        RecyclerView recyclerView = dialogView.findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.addItemDecoration(new DividerItemDecoration(context, 0));


        recyclerView.setAdapter(sortingAdapter);
        sortingAdapter.setOnClickListener((data, position) -> {

            sortKey = sortingAdapter.getItem(position).getSortingText();
            presenter.applyFilterAndSort(this, sortingAdapter.getItem(position).getSortingText(), sitetype, distanceFilterVal);
            handleSortClick(position);
            sortingAdapter.notifyDataSetChanged();
            alertDialog.dismiss();
        });


        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.BOTTOM;
        window.setAttributes(wlp);

        alertDialog.show();

    }


    private void showFilterDialog(Context context) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.site_filter_item, null);
        Animation slideUpIn;
        slideUpIn = AnimationUtils.loadAnimation(this, R.anim.slide_in_bottom);
        dialogView.startAnimation(slideUpIn);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);
        AlertDialog alertDialog = dialogBuilder.create();
        BubbleSeekBar distanceSeekbar;
        TextView applyFilter;


        commercialSite = dialogView.findViewById(R.id.commercial);
        religiousSite = dialogView.findViewById(R.id.religious);
        otherSite = dialogView.findViewById(R.id.otherSite);
        residentialSite = dialogView.findViewById(R.id.resdential_site);
        institutionalSite = dialogView.findViewById(R.id.institutional_site);
        industrialSite = dialogView.findViewById(R.id.industrial);
        distanceSeekbar = dialogView.findViewById(R.id.distanceSeekbar);
        applyFilter = dialogView.findViewById(R.id.applyFilter);

        setSiteType(sitetype);

        commercialSite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSiteType("Commercial");
            }
        });
        religiousSite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSiteType("Religious");

            }
        });
        residentialSite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSiteType("Residential");

            }
        });
        institutionalSite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSiteType("Institutional");

            }
        });
        industrialSite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSiteType("Industrial");

            }
        });
        otherSite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setSiteType("All");

            }
        });

        distanceSeekbar.setProgress(distanceFilterVal);

        applyFilter.setOnClickListener(view -> {

            distanceFilterVal = distanceSeekbar.getProgress();
//                isResetValue=true;

            if (sitetype.equalsIgnoreCase("")){
                showToast("Please Select Site Type");
            }
            else {
                presenter.applyFilterAndSort(this,sortKey,sitetype,distanceFilterVal);
                alertDialog.dismiss();

            }



        });


        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.BOTTOM;
        window.setAttributes(wlp);

        alertDialog.show();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.lay1:
                showSortingDialog(this);
                break;

            case R.id.lay2:
                showFilterDialog(this);
                break;

        }
    }

    public void prepaerList() {

        SortingModel model1 = new SortingModel();

        model1.setSortingText("Site Distance -- Low to High");
        model1.setSelected(true);
        sortedList.add(model1);

        String[] array = new String[]{"Site Distance -- High to Low", "Site Size -- Low to High", "Site Size -- High to Low"};
        for (int i = 0; i < array.length; i++) {
            SortingModel model = new SortingModel();
            model.setSelected(false);
            model.setSortingText(array[i]);
            sortedList.add(model);
        }


    }

    private void handleSortClick(int pos) {

        for (int i = 0; i < sortedList.size(); i++) {
            if (i == pos) {
                sortedList.get(i).setSelected(true);
            } else {
                sortedList.get(i).setSelected(false);
            }
        }

    }

    private void setSiteType(String siteType) {

        this.sitetype = siteType;

        switch (siteType) {
            case "Religious":
                religiousSite.setBackgroundResource(R.drawable.button_bg);
                religiousSite.setTextColor(Color.WHITE);

                industrialSite.setBackgroundResource(R.drawable.button_background);
                industrialSite.setTextColor(getResources().getColor(R.color.colortheme));

                commercialSite.setBackgroundResource(R.drawable.button_background);
                commercialSite.setTextColor(getResources().getColor(R.color.colortheme));

                institutionalSite.setBackgroundResource(R.drawable.button_background);
                institutionalSite.setTextColor(getResources().getColor(R.color.colortheme));

                residentialSite.setBackgroundResource(R.drawable.button_background);
                residentialSite.setTextColor(getResources().getColor(R.color.colortheme));

                otherSite.setBackgroundResource(R.drawable.button_background);
                otherSite.setTextColor(getResources().getColor(R.color.colortheme));


                break;
            case "Commercial":

                commercialSite.setBackgroundResource(R.drawable.button_bg);
                commercialSite.setTextColor(Color.WHITE);

                industrialSite.setBackgroundResource(R.drawable.button_background);
                industrialSite.setTextColor(getResources().getColor(R.color.colortheme));

                religiousSite.setBackgroundResource(R.drawable.button_background);
                religiousSite.setTextColor(getResources().getColor(R.color.colortheme));

                institutionalSite.setBackgroundResource(R.drawable.button_background);
                institutionalSite.setTextColor(getResources().getColor(R.color.colortheme));

                residentialSite.setBackgroundResource(R.drawable.button_background);
                residentialSite.setTextColor(getResources().getColor(R.color.colortheme));

                otherSite.setBackgroundResource(R.drawable.button_background);
                otherSite.setTextColor(getResources().getColor(R.color.colortheme));
                break;
            case "Industrial":
                industrialSite.setBackgroundResource(R.drawable.button_bg);
                industrialSite.setTextColor(Color.WHITE);

                commercialSite.setBackgroundResource(R.drawable.button_background);
                commercialSite.setTextColor(getResources().getColor(R.color.colortheme));

                religiousSite.setBackgroundResource(R.drawable.button_background);
                religiousSite.setTextColor(getResources().getColor(R.color.colortheme));

                institutionalSite.setBackgroundResource(R.drawable.button_background);
                institutionalSite.setTextColor(getResources().getColor(R.color.colortheme));

                residentialSite.setBackgroundResource(R.drawable.button_background);
                residentialSite.setTextColor(getResources().getColor(R.color.colortheme));

                otherSite.setBackgroundResource(R.drawable.button_background);
                otherSite.setTextColor(getResources().getColor(R.color.colortheme));
                break;
            case "Institutional":

                institutionalSite.setBackgroundResource(R.drawable.button_bg);
                institutionalSite.setTextColor(Color.WHITE);

                commercialSite.setBackgroundResource(R.drawable.button_background);
                commercialSite.setTextColor(getResources().getColor(R.color.colortheme));

                religiousSite.setBackgroundResource(R.drawable.button_background);
                religiousSite.setTextColor(getResources().getColor(R.color.colortheme));

                industrialSite.setBackgroundResource(R.drawable.button_background);
                industrialSite.setTextColor(getResources().getColor(R.color.colortheme));

                residentialSite.setBackgroundResource(R.drawable.button_background);
                residentialSite.setTextColor(getResources().getColor(R.color.colortheme));

                otherSite.setBackgroundResource(R.drawable.button_background);
                otherSite.setTextColor(getResources().getColor(R.color.colortheme));
                break;
            case "Residential":

                residentialSite.setBackgroundResource(R.drawable.button_bg);
                residentialSite.setTextColor(Color.WHITE);

                commercialSite.setBackgroundResource(R.drawable.button_background);
                commercialSite.setTextColor(getResources().getColor(R.color.colortheme));

                religiousSite.setBackgroundResource(R.drawable.button_background);
                religiousSite.setTextColor(getResources().getColor(R.color.colortheme));

                industrialSite.setBackgroundResource(R.drawable.button_background);
                industrialSite.setTextColor(getResources().getColor(R.color.colortheme));

                institutionalSite.setBackgroundResource(R.drawable.button_background);
                institutionalSite.setTextColor(getResources().getColor(R.color.colortheme));


                otherSite.setBackgroundResource(R.drawable.button_background);
                otherSite.setTextColor(getResources().getColor(R.color.colortheme));


                break;
            case "All":

                residentialSite.setBackgroundResource(R.drawable.button_background);
                residentialSite.setTextColor(getResources().getColor(R.color.colortheme));

                commercialSite.setBackgroundResource(R.drawable.button_background);
                commercialSite.setTextColor(getResources().getColor(R.color.colortheme));

                religiousSite.setBackgroundResource(R.drawable.button_background);
                religiousSite.setTextColor(getResources().getColor(R.color.colortheme));

                otherSite.setBackgroundResource(R.drawable.button_bg);
                otherSite.setTextColor(Color.WHITE);

                industrialSite.setBackgroundResource(R.drawable.button_background);
                industrialSite.setTextColor(getResources().getColor(R.color.colortheme));

                institutionalSite.setBackgroundResource(R.drawable.button_background);
                institutionalSite.setTextColor(getResources().getColor(R.color.colortheme));
                break;

        }


    }

    @Override
    public void OnFetchSites(List<SiteNearMeModel.SiteNearMeDetails> list) {
        hideProgress();
        SiteNearAdapter adapter = new SiteNearAdapter(this);
        adapter.addAll(list);
        recyclerView.setAdapter(adapter);
        adapter.setOnClickListener(listner);


    }

    private SiteNearAdapter.OnClick<SiteNearMeModel.SiteNearMeDetails> listner = (data, position) -> {
        Bundle bundle = new Bundle();
        bundle.putString("id", data.installedBy);
        bundle.putBoolean("isFormasking", true);
        goToNextScreen(ImplemeterDetailsActivity.class, bundle);



    };

    @Override
    public void onFailure(String msg) {
        hideProgress();
        if (msg.equalsIgnoreCase("invalid token")) {
            securePrefManager.clearAppsAllPrefs();
            startActivity(new Intent(SiteNearMeActivity.this, MainActivity.class));
            finish();
        } else
            showToast(msg);
    }

    public class SortingAdapter extends BaseRecyclerAdapter<SortingModel, SortingAdapter.Holder> {


        public SortingAdapter(Context mContext) {
            super(mContext);
        }

        @Override
        public int getLayout(int viewType) {
            return R.layout.sort_layout_item;
        }

        @Override
        public SortingAdapter.Holder getViewHolder(View view, int viewType) {
            return new SortingAdapter.Holder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull SortingAdapter.Holder holder, int i) {
            holder.sortText.setText(getItem(i).sortingText);
            if (getItem(i).getSelected()) {
                holder.radioButton.setChecked(true);
            } else
                holder.radioButton.setChecked(false);

        }


        public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView sortText;
            RadioButton radioButton;

            public Holder(@NonNull View itemView) {
                super(itemView);
                sortText = itemView.findViewById(R.id.sortText);
                radioButton = itemView.findViewById(R.id.radio_btn);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                getListener().onClick(getItem(getAdapterPosition()), getAdapterPosition());
            }
        }
    }

    public class SortingModel {
        private String sortingText;
        private Boolean isSelected;


        public SortingModel() {

        }

        public String getSortingText() {
            return sortingText;
        }

        public void setSortingText(String sortingText) {
            this.sortingText = sortingText;
        }

        public Boolean getSelected() {
            return isSelected;
        }

        public void setSelected(Boolean selected) {
            isSelected = selected;
        }

    }


}
