package com.solarginni.user.SiteNearMe;

import android.content.Context;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Retrofit;

public class SiteNearMeInteractor implements SiteNearMeContract.Interactor {

    private Retrofit retrofit;
    private ApiInterface apiInterface;

    public SiteNearMeInteractor(Retrofit retrofit) {
        this.retrofit = retrofit;
        apiInterface = retrofit.create(ApiInterface.class);

    }

    @Override
    public void fetchSites(Context context, String token, SiteNearMeContract.OnInteraction lisnter) {

        apiInterface.getSiteNearMe(token).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                SiteNearMeModel model = response.toResponseModel(SiteNearMeModel.class);

                lisnter.OnFetchSites(model.details);
                SiteNearMeDao.getInstance(context).insertData(model.details);

            }

            @Override
            public void onError(ApiError error) {
                lisnter.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                lisnter.onFailure(throwable.getMessage());

            }
        });


    }

    @Override
    public void applyFilterAndSort(Context context,String sortingKey, String siteType, int distance, SiteNearMeContract.OnInteraction lisnter) {

        List<SiteNearMeModel.SiteNearMeDetails> list = new ArrayList<>();

        list.addAll(SiteNearMeDao.getInstance(context).sortListByValue(sortingKey,siteType,distance));

        lisnter.OnFetchSites(list);


    }
}
