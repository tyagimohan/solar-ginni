package com.solarginni.user.SiteNearMe;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.TextView;

import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.R;
import com.solarginni.Utility.Utils;

public class SiteNearAdapter extends BaseRecyclerAdapter<SiteNearMeModel.SiteNearMeDetails, SiteNearAdapter.Holder> {


    public SiteNearAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public int getLayout(int viewType) {
        return R.layout.site_near_item;
    }

    @Override
    public Holder getViewHolder(View view, int viewType) {
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {


        SpannableString spannableString = new SpannableString(getItem(i).panelMake);


        holder.panelMake.setText(getItem(i).panelMake.trim().split(" ")[0]);
        holder.siteSize.setText(""+getItem(i).siteSize +" Kw");

        holder.distance.setText(String.format("%.2f", getItem(i).distance)+ " Km");

//        holder.distance.setText(""+getItem(i).distance + " Km");

        SpannableString content = new SpannableString(getItem(i).companyName);
        content.setSpan(new UnderlineSpan(), 0, getItem(i).companyName.length(), 0);
        holder.installedBy.setText(content);

        holder.installedBy.setOnClickListener(v -> {
            getListener().onClick(getItem(i),i);
        });


        holder.siteType.setText(""+getItem(i).siteType);
        holder.installedOn.setText(Utils.convertDate(getItem(i).installedOn));

        String stringInverterMake="";

        for (int j=0;j<getItem(i).inverter.size();j++){
            stringInverterMake=stringInverterMake+", " +getItem(i).inverter.get(j).getInverterMake();
        }



        holder.inverterMake.setText(getItem(i).inverter.get(0).getInverterMake());

    }

    public class Holder extends RecyclerView.ViewHolder {

        public TextView siteType, distance, siteSize, installedBy, panelMake, inverterMake, installedOn;

        public Holder(@NonNull View itemView) {
            super(itemView);
            siteType = itemView.findViewById(R.id.siteType);
            distance = itemView.findViewById(R.id.distance);
            siteSize = itemView.findViewById(R.id.siteSize);
            installedBy = itemView.findViewById(R.id.installedBy);
            installedOn = itemView.findViewById(R.id.installedOn);
            inverterMake = itemView.findViewById(R.id.inverterMake);
            panelMake = itemView.findViewById(R.id.panelMake);
        }
    }
}
