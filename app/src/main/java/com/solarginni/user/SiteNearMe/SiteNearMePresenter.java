package com.solarginni.user.SiteNearMe;

import android.content.Context;

import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.RestClient;

import java.util.List;

public class SiteNearMePresenter  implements SiteNearMeContract.Presenter,SiteNearMeContract.OnInteraction{

    private SiteNearMeContract.View view;
    private SiteNearMeInteractor interactor;

    public SiteNearMePresenter(SiteNearMeContract.View view) {
        this.view = view;
        interactor= new SiteNearMeInteractor(RestClient.getRetrofitBuilder());

    }

    @Override
    public void fetchSites(Context context, String token) {
        interactor.fetchSites(context,token,this);

    }

    @Override
    public void applyFilterAndSort(Context context,String sortingKey, String siteType, int distance) {
              interactor.applyFilterAndSort(context,sortingKey,siteType,distance,this);
    }

    @Override
    public void OnFetchSites(List<SiteNearMeModel.SiteNearMeDetails> list) {
        view.OnFetchSites(list);

    }

    @Override
    public void onFailure(String msg) {
            view.onFailure(msg);
    }
}
