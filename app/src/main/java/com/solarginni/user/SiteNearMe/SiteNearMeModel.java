package com.solarginni.user.SiteNearMe;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.solarginni.DBModel.SiteDetailsModel.Inverter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SiteNearMeModel implements Serializable {
    @SerializedName("details")
    @Expose
    public List<SiteNearMeDetails> details = new ArrayList<>();


    public static class SiteNearMeDetails {

        @SerializedName("siteType")
        @Expose
        public String siteType;
        @SerializedName("siteSize")
        @Expose
        public Double siteSize;
        @SerializedName("installedOn")
        @Expose
        public String installedOn;
        @SerializedName("inverter")
        @Expose
        public List<Inverter> inverter = null;
        @SerializedName("distance")
        @Expose
        public double distance;
        @SerializedName("panelMake")
        @Expose
        public String panelMake;
        @SerializedName("companyName")
        @Expose
        public String companyName;
        @SerializedName("installedBy")
        @Expose
        public String installedBy;

    }

}
