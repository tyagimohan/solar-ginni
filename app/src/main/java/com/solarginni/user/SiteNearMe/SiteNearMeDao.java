package com.solarginni.user.SiteNearMe;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import com.google.gson.reflect.TypeToken;
import com.solarginni.Backend.DAOs.BaseDao;
import com.solarginni.Backend.DAOs.QuoteImpDao;
import com.solarginni.DBModel.SiteDetailsModel.Inverter;
import com.solarginni.Utility.Utils;

import java.util.ArrayList;
import java.util.List;

public class SiteNearMeDao extends BaseDao {
    protected SiteNearMeDao(Context context) {
        super(context);
    }



    public static SiteNearMeDao getInstance(Context mContext) {
        return new SiteNearMeDao(mContext);
    }

    public void insertData(List<SiteNearMeModel.SiteNearMeDetails> list) {

        openDB(1);

        deleteData(SiteNearMeTable.SITE_NEAR_ME_TABLE);

        try {
            getDb().beginTransaction();
            SQLiteStatement stmt = getDb().compileStatement(SiteNearMeTable.INSERT_DATA);

            for (int i = 0; i < list.size(); i++) {
                stmt.bindString(1, list.get(i).siteType);
                stmt.bindString(2, list.get(i).companyName);
                stmt.bindString(3, list.get(i).installedOn);
                stmt.bindDouble(4, list.get(i).siteSize);
                stmt.bindString(5, list.get(i).panelMake);
                stmt.bindDouble(6, list.get(i).distance);
                stmt.bindString(7, Utils.toString(list.get(i).inverter));
                stmt.bindString(8, list.get(i).installedBy);
                stmt.executeInsert();
                stmt.clearBindings();
            }
            getDb().setTransactionSuccessful();

        } catch (Exception e) {
            e.printStackTrace();
            getDb().endTransaction();
        } finally {

            getDb().endTransaction();
            closeDb();

        }

    }


    public List<SiteNearMeModel.SiteNearMeDetails> sortListByValue(String value, String siteType, int distance) {

        List<SiteNearMeModel.SiteNearMeDetails> list = new ArrayList<>();

        String query = "Select * from " + SiteNearMeTable.SITE_NEAR_ME_TABLE + "  WHERE ";

        if (!siteType.equalsIgnoreCase("All")){
            query+= " site_near_me.site_type = '" + siteType +"' and ";
        }

        query+="site_near_me.distance <= " + distance;



        /*
 model1.setSortingText("Site Distance -- Low to High");
        model1.setSelected(true);
        sortedList.add(model1);

        String[] array = new String[]{ "Site Distance -- High to Low", "Site Size -- Low to High", "Site Size -- High to Low"};

SELECT * from site_near_me WHERE site_near_me.site_type = "Residential" and site_near_me.distance <= 150 ORDER BY site_near_me.distance DESC
*/
        switch (value) {
            case "Site Distance -- Low to High":
                query += " ORDER BY site_near_me.distance ASC ";
                break;
            case "Site Distance -- High to Low":
                query += " ORDER BY site_near_me.distance DESC ";
                break;
            case "Site Size -- Low to High":
                query += " ORDER BY site_near_me.site_size ASC ";
                break;
            case "Site Size -- High to Low":
                query += " ORDER BY site_near_me.site_size DESC ";
                break;
        }


        openDB(0);

        Cursor mCursor = null;

        try {
            mCursor = getDb().rawQuery(query, null);

            if (mCursor.moveToFirst()) {

                SiteNearMeModel.SiteNearMeDetails aData;
                do {
                    aData = new SiteNearMeModel.SiteNearMeDetails();
                    parseData(aData, mCursor);
                    list.add(aData);

                } while (mCursor.moveToNext());

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeCursor(mCursor);
            closeDb();
        }
        return list;

    }

    public void parseData(SiteNearMeModel.SiteNearMeDetails data, Cursor cursor) {

        data.siteType = cursor.getString(cursor.getColumnIndex(SiteNearMeTable.SITE_TYPE));
        data.siteSize = Double.valueOf(cursor.getString(cursor.getColumnIndex(SiteNearMeTable.SITE_SIZE)));
        data.companyName = cursor.getString(cursor.getColumnIndex(SiteNearMeTable.INSTALLED_BY));
        data.installedOn = cursor.getString(cursor.getColumnIndex(SiteNearMeTable.INSTALLED_ON));
        data.panelMake = cursor.getString(cursor.getColumnIndex(SiteNearMeTable.PANEL_MAKE));
        data.distance = Double.parseDouble(cursor.getString(cursor.getColumnIndex(SiteNearMeTable.DISTANCE)));
        data.installedBy = cursor.getString(cursor.getColumnIndex(SiteNearMeTable.IMPLEMENTER_ID));
        data.inverter = Utils.getObj(cursor.getString(cursor.getColumnIndex(SiteNearMeTable.INVERTER_MAKE)), new TypeToken<List<Inverter>>() {
        }.getType());

    }

}
