package com.solarginni.user.customerDetail;

import com.solarginni.DBModel.CommomResponse;

public interface CustomerDetailContract  {

    interface View{
         void showDetails(CommomResponse response);
        void onFailure(String msg);
    }

    interface Interactor{
        void getDetails(String token,String id,OnInteraction interaction);
    }
    interface  Presenter{
        void getDetails(String token,String id);

    }

    interface  OnInteraction{
        void showDetails(CommomResponse response);
        void onFailure(String msg);
    }
}
