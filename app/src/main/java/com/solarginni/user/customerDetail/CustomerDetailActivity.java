package com.solarginni.user.customerDetail;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.bumptech.glide.Glide;
import com.solarginni.Base.BaseActivity;
import com.solarginni.CustomizeView.CustomAlertPopup;
import com.solarginni.CustomizeView.CustomDialogInterface;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.Implementer.ImplementerUserModule.ActivateDeactivateUserApi;
import com.solarginni.Implementer.ImplementerUserModule.RoleUpgradeApi;
import com.solarginni.R;
import com.solarginni.SGUser.AllUserModule.SendNotificationActivity;
import com.solarginni.Utility.Utils;
import com.solarginni.network.UpdatePhoneNumberApi;

import static com.solarginni.Utility.AppConstants.MOBILE;
import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.SG_ADMIN;
import static com.solarginni.Utility.AppConstants.TOKEN;

public class CustomerDetailActivity extends BaseActivity implements CustomerDetailContract.View ,View.OnClickListener{

    private CustomerDetailPresenter presenter;
    private TextView address, contact, customerId, customerName,email,roleUpGrade,onBoardDate;
    private ImageView profileImage;
    private Button activate,editMobileNumber,notificationButton;
    private int role;
    private  DetailModel model;

    private String [] roleArray= new String[]{"Customer","Prospect"};


    private boolean isFormasking=false;

    @Override
    public int getLayout() {
        return R.layout.customer_detail;
    }

    @Override
    public void initViews() {
        presenter = new CustomerDetailPresenter(this);
        address = findViewById(R.id.address);
        contact = findViewById(R.id.contact);
        customerId = findViewById(R.id.customerId);
        onBoardDate = findViewById(R.id.onboardDate);
        customerName = findViewById(R.id.customerName);
        email = findViewById(R.id.email);
        activate = findViewById(R.id.activate);
        notificationButton = findViewById(R.id.notificationButton);
        notificationButton.setOnClickListener(this);
        editMobileNumber = findViewById(R.id.edit_number);
        roleUpGrade = findViewById(R.id.role_upgrade);


        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });

        disableSuggestion(address);
        disableSuggestion(customerId);
        disableSuggestion(customerName);
        disableSuggestion(email);
        profileImage = findViewById(R.id.profileImage);

    }

    @Override
    public void setUp() {



        isFormasking=getIntent().getExtras().getBoolean("isFormasking",false);
        if (Utils.hasNetwork(this)) {
            showProgress("Please wait..");
            presenter.getDetails(securePrefManager.getSharedValue(TOKEN), getIntent().getExtras().getString("id", ""));
        } else
            showToast("Please check Internet");

    }

    @Override
    public void showDetails(CommomResponse response) {
        hideProgress();
     model= response.toResponseModel(DetailModel.class);
         customerName.setText(model.getName());
         customerId.setText(model.getUserId());
        onBoardDate.setText(Utils.convertDate(model.onBoardDate));

        if (getIntent().getBooleanExtra("isForActivate",false) && securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SG_ADMIN) && !model.getPhone().equalsIgnoreCase(securePrefManager.getSharedValue(MOBILE))){
            activate.setVisibility(View.VISIBLE);
            roleUpGrade.setVisibility(View.VISIBLE);
            editMobileNumber.setVisibility(View.VISIBLE);
            notificationButton.setVisibility(View.VISIBLE);
            findViewById(R.id.statusLAbel).setVisibility(View.VISIBLE);
            role=getIntent().getIntExtra("role",0);

            if (role==11){
                findViewById(R.id.statusLAbel).setVisibility(View.GONE);

            }
            if (getIntent().getIntExtra("role",0)==3){
                roleUpGrade.setText("Prospect");
                roleArray= new String[]{"Customer"};
            }
            else {
                roleUpGrade.setText("Customer");
                roleArray= new String[]{"Prospect"};


            }

            roleUpGrade.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAlertWithList(roleArray,"Select Role",roleUpGrade,model.getUserId());
                }
            });

        }
         if (!model.getEmail().isEmpty()){
             findViewById(R.id.emailLAbel).setVisibility(View.VISIBLE);
         }
         else {
             findViewById(R.id.emailLAbel).setVisibility(View.GONE);

         }
        try {
            if (!model.getImageUrl().isEmpty())
                Glide.with(this).load(model.getImageUrl()).into(profileImage);
        } catch (Exception e) {
            e.printStackTrace();

        }

        if (model.activate){
            activate.setText("De-Activate");
        }
        else {
            activate.setText("Activate");

        }



        activate.setOnClickListener(v -> {
            if (Utils.hasNetwork(this)){
                showProgress("Please wait...");
                ActivateDeactivateUserApi activateDeactivateUserApi= new ActivateDeactivateUserApi() {
                    @Override
                    public void onComplete(CommomResponse response) {
                        hideProgress();
                        showToast(response.getMessage());
                        model.activate=model.activate?false:true;
                        activate.setText(model.activate?"De-Activate":"Activate");
                        setResult(Activity.RESULT_OK);
                        finish();


                    }

                    @Override
                    protected void onFailur(String msg) {
                        hideProgress();
                        showToast(msg);

                    }
                };
                activateDeactivateUserApi.hit(securePrefManager.getSharedValue(TOKEN),model.getUserId(),model.activate?false:true);
            }
            else {

            }

        });

        editMobileNumber.setOnClickListener(this);

        if (isFormasking){
            contact.setText("+91**********");
            email.setText("****@gmail.com");
            address.setText(model.getAddress().getCity().trim() + "," + model.getAddress().getState() + "," + model.getAddress().getCountry() + "," + model.getAddress().getPinCode());

        }
        else {
            contact.setText(model.getPhone());
            contact.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {

                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + model.getPhone()));
                        startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();
                        // TODO: handle exception
                    }
                }
            });
            email.setText(model.getEmail());
            address.setText(model.getAddress().getAddressLine1().trim() + "," + model.getAddress().getAddressLine2().trim() + "," + model.getAddress().getCity().trim() + "," + model.getAddress().getState() + "," + model.getAddress().getCountry() + "," + model.getAddress().getPinCode());

        }


    }

    @Override
    public void onFailure(String msg) {
        hideProgress();

    }

    public void showAlertWithList(final String[] dataArray, String title, final TextView textView, String usID) {
        hideKeyboard(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setItems(dataArray, (dialog, item) -> {
            RoleUpgradeApi api = new RoleUpgradeApi() {
                @Override
                public void onComplete(CommomResponse response) {
                    showToast(response.getMessage());
                    textView.setText(dataArray[item]);
                    role=role==3?7:3;
                    if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SG_ADMIN)){
                        setResult(RESULT_OK);
                        finish();
                    }
                }

                @Override
                protected void onFailur(String msg) {
                    showToast(msg);
                }
            };

            if (role==3){
                api.hit(securePrefManager.getSharedValue(TOKEN),usID,"7");
            }
            else {
                api.hit(securePrefManager.getSharedValue(TOKEN),usID,"3");

            }

        });
        AlertDialog alerts = builder.create();
        alerts.show();
    }

    @Override
    public void onClick(View v) {
         switch (v.getId()){
             case R.id.notificationButton:
                 Bundle bundle = new Bundle();
                 bundle.putString("customerName",model.getName());
                 bundle.putString("contact",model.getPhone());
                 bundle.putString("customerId",model.getUserId());

                 goToNextScreen(SendNotificationActivity.class,bundle);

                 break;
             case R.id.edit_number:
                 CustomAlertPopup.Builder builder = new CustomAlertPopup.Builder(CustomerDetailActivity.this, true);
                 builder.setMessage("Please enter 10 digit mobile number");
                 builder.setPositiveButton("Submit", new CustomDialogInterface.OnClickListener() {
                     @Override
                     public void onClick() {
                         if (builder.getEditText().trim().length() < 10|| builder.getEditText().startsWith("+91")) {
                             showToast("Please enter valid number");
                         } else {
                             showProgress("Please wait...");
                             UpdatePhoneNumberApi api = new UpdatePhoneNumberApi() {
                                 @Override
                                 public void onComplete(CommomResponse response) {
                                     hideProgress();
                                     showToast(response.getMessage());
                                     contact.setText("+91" + builder.getEditText().trim());
                                     setResult(Activity.RESULT_OK);
                                     builder.dismis();
                                 }

                                 @Override
                                 protected void onFailur(String msg) {
                                     hideProgress();
                                     showToast(msg);

                                 }
                             };
                             String moobile = "+91" + builder.getEditText().trim();
                             api.hit(securePrefManager.getSharedValue(TOKEN),model.getUserId(),moobile);


                         }

                     }
                 });

                 builder.setNegativeButton(R.string.text_cancel, new CustomDialogInterface.OnClickListener() {
                     @Override
                     public void onClick() {
                         builder.dismis();
                     }
                 });

                 builder.setCancelable(false);
                 builder.show(Gravity.CENTER);
                 break;
         }
    }
}
