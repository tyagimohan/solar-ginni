package com.solarginni.user.customerDetail;

import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.RestClient;

public class CustomerDetailPresenter implements CustomerDetailContract.OnInteraction, CustomerDetailContract.Presenter {

    private CustomerDetailContract.View view;
    private CustomerDetaisInteractor interactor;

    public CustomerDetailPresenter(CustomerDetailContract.View view) {
        this.view = view;
        interactor = new CustomerDetaisInteractor(RestClient.getRetrofitBuilder());
    }

    @Override
    public void getDetails(String token, String id) {
        interactor.getDetails(token, id, this);

    }

    @Override
    public void showDetails(CommomResponse response) {

        view.showDetails(response);
    }

    @Override
    public void onFailure(String msg) {

        view.onFailure(msg);
    }
}
