package com.solarginni.user.customerDetail;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;

import retrofit2.Retrofit;

public class CustomerDetaisInteractor implements CustomerDetailContract.Interactor {

    private Retrofit retrofit;
    private ApiInterface apiInterface;

    public CustomerDetaisInteractor(Retrofit retrofit) {
        this.retrofit = retrofit;
        apiInterface = retrofit.create(ApiInterface.class);
    }

    @Override
    public void getDetails(String token, String id, CustomerDetailContract.OnInteraction interaction) {
        apiInterface.fetchUser(token, id).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                interaction.showDetails(response);

            }

            @Override
            public void onError(ApiError error) {
                interaction.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                interaction.onFailure(throwable.getMessage());

            }
        });
    }
}
