package com.solarginni.user.RequestDetailsModule;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.hsalf.smilerating.SmileRating;
import com.solarginni.Base.BaseFragment;
import com.solarginni.CommonModule.MainActivity;
import com.solarginni.DBModel.APIBody.RequestParam;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.RequestData;
import com.solarginni.R;
import com.solarginni.Utility.Utils;

import static com.solarginni.Utility.AppConstants.CUSTOMER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.IMPLEMENTER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.SUPER_IMPLEMENTER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.TOKEN;

public class RequestDetailFrament2 extends BaseFragment implements RequestDetailCOntract.View, View.OnClickListener {
    private EditText customerRemark, implementerRemark, requestStatus;
    private Button closeRequest;
    private RequestDetailPresenter presenter;
    private RequestData.RequestDetail requestDetail;
    private String requestID;
    public EditText assignedTo,implementerName;

    private String[] statusArray;


    @Override
    public int getLayout() {
        return R.layout.request_details_child2;
    }

    @Override
    public void initViews(View view) {
        implementerRemark = view.findViewById(R.id.implementerRemark);
        customerRemark = view.findViewById(R.id.customerRemark);
        assignedTo = view.findViewById(R.id.assignedTo);
        closeRequest = view.findViewById(R.id.confirmBtn);
        requestStatus = view.findViewById(R.id.requestStatus);
        implementerName = view.findViewById(R.id.implementerName);

        disableSuggestion(implementerName);
        disableSuggestion(implementerRemark);
        disableSuggestion(customerRemark);
        disableSuggestion(assignedTo);
        disableSuggestion(requestStatus);

        presenter = new RequestDetailPresenter(this);

    }

    @Override
    public void setUp() {


        Bundle bundle = getArguments();
        requestDetail = (RequestData.RequestDetail) bundle.getSerializable("data");

        requestStatus.setText(requestDetail.getRequestStatus());


        implementerName.setText(requestDetail.getCompanyName());

        if (requestDetail.getResolutionNotes() != null)
            implementerRemark.setText(requestDetail.getResolutionNotes());
        else
            implementerRemark.setText("");


        customerRemark.setText(requestDetail.getClosureNotes());

        /*set for implementer*/


        assignedTo.setText(requestDetail.getAssignTo());
        customerRemark.setText(requestDetail.getClosureNotes());

        if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(IMPLEMENTER_ROLE_ID) | securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SUPER_IMPLEMENTER_ROLE_ID)) {
            assignedTo.setFocusableInTouchMode(true);
            assignedTo.setFocusable(true);
        } else {
            assignedTo.setFocusableInTouchMode(false);
            assignedTo.setFocusable(false);
        }


        if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(IMPLEMENTER_ROLE_ID) | securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SUPER_IMPLEMENTER_ROLE_ID
        )) {
            implementerRemark.setFocusableInTouchMode(true);
            implementerRemark.setFocusable(true);
            customerRemark.setFocusable(false);
            customerRemark.setFocusableInTouchMode(false);

            if (requestDetail.getRequestStatus().equalsIgnoreCase("Assigned")) {
                statusArray = new String[]{"Resolved"};
                requestStatus.setOnClickListener(this);
            } else if (requestDetail.getRequestStatus().equalsIgnoreCase("Closed") | requestDetail.getRequestStatus().equalsIgnoreCase("Resolved")) {
                closeRequest.setVisibility(View.GONE);


            } else if (requestDetail.getRequestStatus().equalsIgnoreCase("Re-Opened") | requestDetail.getRequestStatus().equalsIgnoreCase("Open")) {
                statusArray = new String[]{"Assigned"};
                requestStatus.setOnClickListener(this);
            }

        }

        /*set for customer*/

        else if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(CUSTOMER_ROLE_ID)) {
            implementerRemark.setFocusableInTouchMode(false);
            implementerRemark.setFocusable(false);


            if (requestDetail.getRequestStatus().equalsIgnoreCase("Open")) {
                statusArray = new String[]{"Closed"};
                requestStatus.setOnClickListener(this);
            }
            else if (requestDetail.getRequestStatus().equalsIgnoreCase("Assigned")) {
                closeRequest.setVisibility(View.GONE);
            }
            else if (requestDetail.getRequestStatus().equalsIgnoreCase("Resolved")) {
                statusArray = new String[]{"Re-Opened", "Closed"};
                requestStatus.setOnClickListener(this);
            }
            customerRemark.setFocusable(true);
            customerRemark.setFocusableInTouchMode(true);
        }
        if (requestDetail.getRequestStatus().equalsIgnoreCase("Closed")) {

            customerRemark.setFocusable(false);
            customerRemark.setFocusableInTouchMode(false);
            closeRequest.setVisibility(View.GONE);
            customerRemark.setText(requestDetail.getClosureNotes());

        }

        closeRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(getActivity());

                if (!requestDetail.getRequestStatus().equalsIgnoreCase(requestStatus.getText().toString())) {
                    if (Utils.hasNetwork(getContext())) {
                        showProgress("Please wait..");
                        presenter.closeRequest(securePrefManager.getSharedValue(TOKEN), implementerRemark.getText().toString(), customerRemark.getText().toString(), requestStatus.getText().toString(), requestDetail.getRequestId(), assignedTo.getText().toString());
                    } else
                        showToast("Please check Internet");
                } else {
                    showToast("Please change request status ");
                }


            }
        });

    }

    @Override
    public void onFetchRequest(CommomResponse response) {


    }

    @Override
    public void onGetIds(CommomResponse response) {

    }

    @Override
    public void onRequestClose() {
        hideProgress();
        showToast("Status Updated");

        if (requestStatus.getText().toString().equalsIgnoreCase("Closed") || requestStatus.getText().toString().equalsIgnoreCase("Resolved"))
            closeRequest.setVisibility(View.GONE);

        if (requestStatus.getText().toString().equalsIgnoreCase("Closed")) {
            showToast("Close Request");
            showRatingDailog(getContext());
        } else {
            getActivity().setResult(Activity.RESULT_OK);
            getActivity().finish();

        }


    }

    private void showRatingDailog(Context context) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.request_rating_bar, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);
        AlertDialog alertDialog = dialogBuilder.create();
        SmileRating responseRating = dialogView.findViewById(R.id.responseRating);
        SmileRating closureRating = dialogView.findViewById(R.id.closureRating);
        SmileRating teamRating = dialogView.findViewById(R.id.teamRating);
        Button submitBtn = dialogView.findViewById(R.id.confirmBtn);

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((int) responseRating.getRating() > 0 || (int) closureRating.getRating() > 0 || (int) teamRating.getRating() > 0) {
                    showProgress("Please wait...");
                    RequestParam param = new RequestParam();
                    param.setEngineerRating(teamRating.getRating());
                    param.setResolveRating(closureRating.getRating());
                    param.setResponseRating(responseRating.getRating());
                    presenter.rateRequest(securePrefManager.getSharedValue(TOKEN), RequestDetailsFragment1.requestId.getText().toString(), param);

                    alertDialog.dismiss();
                } else {
                    showToast("Please Rate Service");
                }


            }
        });


        alertDialog.show();

    }


    @Override
    public void onCloseRequestImp() {

    }

    @Override
    public void onSubmitFeedback() {
        hideProgress();
        getActivity().setResult(Activity.RESULT_OK);
        getActivity().finish();

    }

    @Override
    public void onFailure(String msg) {

        hideProgress();
        if (msg.equalsIgnoreCase("invalid token")) {
            securePrefManager.clearAppsAllPrefs();
            startActivity(new Intent(getActivity(), MainActivity.class));
        } else
            showToast(msg);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.requestStatus:
                showAlertWithList(statusArray, R.string.select_status, requestStatus);
                break;
        }

    }
}
