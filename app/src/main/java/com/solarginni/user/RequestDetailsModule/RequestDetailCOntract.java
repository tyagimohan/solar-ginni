package com.solarginni.user.RequestDetailsModule;

import com.solarginni.DBModel.APIBody.RequestParam;
import com.solarginni.DBModel.CommomResponse;

public interface RequestDetailCOntract {

    interface View {
        void onFetchRequest(CommomResponse response);

        void onGetIds(CommomResponse response);

        void onRequestClose();

        void onCloseRequestImp();

        void onSubmitFeedback();

        void onFailure(String msg);
    }

    interface Presenter {

        void fetchAllRequest(String token, String requestType, String status);

        void closeRequest(String token, String impleRemark, String closureRemarl, String status, String requestId, String assignedTo);

        void rateRequest(String token, String requestId, RequestParam param);

        void fetchIds(String token);


    }

    interface Interector {
        void fetchAllRequest(String token, String requestType, String status, OnIneraction ineraction);

        void closeRequest(String token, String impleRemark, String closureRemarl, String status, String requestId, String assignedTo, OnIneraction ineraction);

        void rateRequest(String token, String requestId, RequestParam param, OnIneraction ineraction);


        void fetchIds(String token, OnIneraction ineraction);
    }

    interface OnIneraction {
        void onFetchRequest(CommomResponse response);

        void onRequestClose();

        void onSubmitFeedback();

        void onGetIds(CommomResponse response);


        void onCloseRequestImp();

        void onFailure(String msg);
    }
}
