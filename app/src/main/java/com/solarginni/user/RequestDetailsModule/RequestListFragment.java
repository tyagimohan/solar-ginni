package com.solarginni.user.RequestDetailsModule;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hsalf.smilerating.SmileRating;
import com.solarginni.Base.BaseFragment;
import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.CommonModule.MainActivity;
import com.solarginni.CustomizeView.MovableFloatingActionButton;
import com.solarginni.DBModel.APIBody.RequestParam;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.RequestData;
import com.solarginni.DBModel.RequestLovs.SiteQuoteIdModel;
import com.solarginni.Implementer.ImplementerDetails.ImplemeterDetailsActivity;
import com.solarginni.R;
import com.solarginni.Utility.AppConstants;
import com.solarginni.Utility.Utils;
import com.solarginni.user.QuoteRequestModule.QuoteRequestDetailsActivity;
import com.solarginni.user.QuoteRequestModule.RaiseQuoteRequestActivity;
import com.solarginni.user.RaiseRequestModule.RaiseRequestActivity;
import com.solarginni.user.customerDetail.CustomerDetailActivity;

import org.angmarch.views.NiceSpinner;

import java.io.Serializable;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static com.solarginni.Utility.AppConstants.IMPLEMENTER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.SUPER_IMPLEMENTER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.TOKEN;

public class RequestListFragment extends BaseFragment implements RequestDetailCOntract.View, View.OnClickListener {
    private TextView requestType, requestStatus;
    private RecyclerView recyclerView;
    private LinearLayout upperLayout;
    private RequestDetailPresenter presenter;
    private TextView ratingLable, emptyView;
    private NiceSpinner siteSpinner, stateSpinnier;
    private MovableFloatingActionButton raiseRequest;

    private RequestListAdapter.OnClick<RequestData.RequestDetail> listenr = (data, position) -> {

        Bundle bundle = new Bundle();
        bundle.putSerializable("data", (Serializable) data);

        if (data.getSiteId() != null) {
            Intent intent = new Intent(getActivity(), RequestDetailsActivity.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, 3);
        } else {
            Intent intent = new Intent(getActivity(), QuoteRequestDetailsActivity.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, 3);
        }


    };
    private RequestListAdapter.onItemClick<RequestData.RequestDetail> lonListener = (data, position, view) -> {

        switch (view.getId()) {
            case R.id.lay3:
                if (!securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(IMPLEMENTER_ROLE_ID) | !securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SUPER_IMPLEMENTER_ROLE_ID))
                    showRatingDailog(getContext(), data.getRequestId());
                break;
            case R.id.mobileNumber:
                if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(IMPLEMENTER_ROLE_ID) | securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SUPER_IMPLEMENTER_ROLE_ID)) {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("isFormasking", false);
                    bundle.putString("id", data.getUserId());
                    goToNextScreen(CustomerDetailActivity.class, bundle);
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("isFormasking", false);
                    bundle.putString("id", data.getCompanyId());
                    goToNextScreen(ImplemeterDetailsActivity.class, bundle);

                }
                break;
        }


    };
    private String request = "All", status = "All";

    @Override
    public int getLayout() {
        return R.layout.request_list;
    }

    @Override
    public void initViews(View view) {
        recyclerView = view.findViewById(R.id.recyclerView);
        requestStatus = view.findViewById(R.id.request_status);
        requestType = view.findViewById(R.id.request_type);
        siteSpinner = (NiceSpinner) view.findViewById(R.id.site_spinner);
        stateSpinnier = (NiceSpinner) view.findViewById(R.id.state_spinner);
        emptyView = view.findViewById(R.id.emptyView);
        upperLayout = view.findViewById(R.id.upperLayout);
        raiseRequest = view.findViewById(R.id.raiseRequest);
        ratingLable = view.findViewById(R.id.rating);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new BaseRecyclerAdapter.DividerItemDecoration(getContext(), RecyclerView.VERTICAL));
        List<String> dataset = new LinkedList<>(Arrays.asList(getResources().getStringArray(R.array.customer_request_type)));
        siteSpinner.attachDataSource(dataset);
        List<String> statedataset = new LinkedList<>(Arrays.asList(getResources().getStringArray(R.array.customer_request_status)));
        stateSpinnier.attachDataSource(statedataset);

        raiseRequest.setOnClickListener(this);
    }

    @Override
    public void setUp() {

        if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(IMPLEMENTER_ROLE_ID) | securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SUPER_IMPLEMENTER_ROLE_ID)) {
            ratingLable.setText("Customer Id");
            raiseRequest.hide();

        } else {
            ratingLable.setText("Rating");
        }

        requestType.setOnClickListener(this);
        requestStatus.setOnClickListener(this);
        presenter = new RequestDetailPresenter(this);




        siteSpinner.setOnSpinnerItemSelectedListener((parent, view, position, id) -> {

            request = "" + parent.getItemAtPosition(position);
            if (Utils.hasNetwork(getContext())) {
                showProgress("Please wait..");
                presenter.fetchAllRequest(securePrefManager.getSharedValue(TOKEN), "" + parent.getItemAtPosition(position), status);

            } else {
                showToast("Please check Internet");

            }
        });

        stateSpinnier.setOnSpinnerItemSelectedListener((parent, view, position, id) -> {

            status = "" + parent.getItemAtPosition(position);
            if (Utils.hasNetwork(getContext())) {
                showProgress("Please wait..");
                presenter.fetchAllRequest(securePrefManager.getSharedValue(TOKEN), request, "" + parent.getItemAtPosition(position));

            } else {
                showToast("Please check Internet");

            }
        });

    }

    @Override
    public void onFetchRequest(CommomResponse response) {
        hideProgress();
        securePrefManager.storeSharedValue(AppConstants.REQUEST_DATA, Utils.toString(response));
        RequestData data = response.toResponseModel(RequestData.class);
        RequestListAdapter adapter;
        if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(IMPLEMENTER_ROLE_ID) | securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SUPER_IMPLEMENTER_ROLE_ID)) {
            adapter = new RequestListAdapter(getContext(), true);

        } else {
            adapter = new RequestListAdapter(getContext(), false);
        }

        upperLayout.setVisibility(View.VISIBLE);
        emptyView.setVisibility(View.GONE);
        adapter.addAll(data.getRequestDetails());
        recyclerView.setAdapter(adapter);
        adapter.setOnClickListener(listenr);
        adapter.setOnLongClickListener(lonListener);


    }

    @Override
    public void onGetIds(CommomResponse response) {
        hideProgress();
        SiteQuoteIdModel model = response.toResponseModel(SiteQuoteIdModel.class);
        if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase("7")){
            if (model.quoteIdList.size() == 0) {
                Bundle bundle = new Bundle();
                bundle.putString("siteId", model.siteIdList.get(0));
                bundle.putBoolean("forSiteId", true);
                bundle.putStringArrayList("siteList", model.siteIdList);
                goToNextScreen(RaiseRequestActivity.class, bundle);
            } else {
                askRequestType(model);

            }
        }
        else if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase("3")){
            if (model.quoteIdList.size()!=0){
               Bundle bundle = new Bundle();
                bundle.putString("quoteId", model.quoteIdList.get(0));
                bundle.putBoolean("forQuoteId", true);
                bundle.putStringArrayList("quoteList", model.quoteIdList);
                goToNextScreen(RaiseQuoteRequestActivity.class, bundle);
            }
            else {
                showToast("Looks like you don't have any Quote");
            }
        }



    }

    @Override
    public void onRequestClose() {

    }

    @Override
    public void onCloseRequestImp() {

    }

    @Override
    public void onSubmitFeedback() {
        presenter.fetchAllRequest(securePrefManager.getSharedValue(TOKEN), request, status);
        siteSpinner.setText(request);
        stateSpinnier.setText(status);

    }

    @Override
    public void onFailure(String msg) {
        hideProgress();
        if (msg.equalsIgnoreCase("invalid token")) {
            securePrefManager.clearAppsAllPrefs();
            startActivity(new Intent(getActivity(), MainActivity.class));

        } else
            showToast(msg);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 3:
                if (resultCode == Activity.RESULT_OK) {
                    presenter.fetchAllRequest(securePrefManager.getSharedValue(TOKEN), "All", "All");
                    request = "All";
                    status = "All";
                    siteSpinner.setText(request);
                    stateSpinnier.setText(status);
                } else {

                }
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.request_status:
                break;
            case R.id.raiseRequest:
                if (Utils.hasNetwork(getContext())) {
                    showProgress("Please wait..");
                    presenter.fetchIds(securePrefManager.getSharedValue(TOKEN));
                } else {
                    showToast("Please check Internet");
                }
                break;
        }

    }


    public void showAlertWithList(final String[] dataArray, final String title, final TextView textView) {
        hideKeyboard(getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(title);
        builder.setItems(dataArray, (dialog, item) -> {
            textView.setText(dataArray[item]);
            presenter.fetchAllRequest(securePrefManager.getSharedValue(TOKEN), requestType.getText().toString(), requestStatus.getText().toString());
        });
        AlertDialog alerts = builder.create();
        alerts.show();
    }

    private void showRatingDailog(Context context, String requestID) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.request_rating_bar, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);
        AlertDialog alertDialog = dialogBuilder.create();
        SmileRating responseRating = dialogView.findViewById(R.id.responseRating);
        SmileRating closureRating = dialogView.findViewById(R.id.closureRating);
        SmileRating teamRating = dialogView.findViewById(R.id.teamRating);
        Button submitBtn = dialogView.findViewById(R.id.confirmBtn);

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((int) responseRating.getRating() > 0 || (int) closureRating.getRating() > 0 || (int) teamRating.getRating() > 0) {
                    showProgress("Please wait...");
                    RequestParam param = new RequestParam();
                    param.setEngineerRating(teamRating.getRating());
                    param.setResolveRating(closureRating.getRating());
                    param.setResponseRating(responseRating.getRating());
                    presenter.rateRequest(securePrefManager.getSharedValue(TOKEN), requestID, param);

                    alertDialog.dismiss();
                } else {
                    showToast("Please Rate Service");
                }


            }
        });


        alertDialog.show();

    }


    private void askRequestType(SiteQuoteIdModel model) {
        CharSequence[] options = getResources().getStringArray(R.array.request_type_options);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Select Request Type");
        builder.setItems(options, (dialog, which) -> {
            switch (which) {
                case 0:
                    Bundle bundle = new Bundle();
                    bundle.putString("siteId", model.siteIdList.get(0));
                    bundle.putBoolean("forSiteId", true);
                    bundle.putStringArrayList("siteList", model.siteIdList);
                    goToNextScreen(RaiseRequestActivity.class, bundle);
                    break;
                case 1:
                    bundle = new Bundle();
                    bundle.putString("quoteId", model.quoteIdList.get(0));
                    bundle.putBoolean("forQuoteId", true);
                    bundle.putStringArrayList("quoteList", model.quoteIdList);
                    goToNextScreen(RaiseQuoteRequestActivity.class, bundle);
                    break;
                default:
                    break;
            }
        });
        builder.show();
    }


    @Override
    public void onResume() {
        super.onResume();
        if (Utils.hasNetwork(getContext())) {
            showProgress("Please wait..");

            presenter.fetchAllRequest(securePrefManager.getSharedValue(TOKEN), "All", "All");


        } else {
            showToast("Please check Internet");
        }
    }
}

