package com.solarginni.user.RequestDetailsModule;

import com.solarginni.DBModel.APIBody.RequestParam;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.RestClient;

public class RequestDetailPresenter implements RequestDetailCOntract.Presenter, RequestDetailCOntract.OnIneraction {
    private RequestDetailCOntract.View view;
    private RequestDetailInteractor interactor;

    public RequestDetailPresenter(RequestDetailCOntract.View view) {
        this.view = view;
        interactor = new RequestDetailInteractor(RestClient.getRetrofitBuilder());
    }

    @Override
    public void fetchAllRequest(String token,String requestType,String status) {
        interactor.fetchAllRequest(token,requestType,status, this);
    }

    @Override
    public void closeRequest(String token, String impleRemark,String closureRemarl, String status, String requestId,String assignedTo) {
          interactor.closeRequest(token,impleRemark,closureRemarl,status,requestId,assignedTo,this);
    }

    @Override
    public void rateRequest(String token, String requestId, RequestParam param) {
        interactor.rateRequest(token,requestId,param,this);

    }

    @Override
    public void fetchIds(String token) {
       interactor.fetchIds(token,this);
    }


    @Override
    public void onFetchRequest(CommomResponse response) {
        view.onFetchRequest(response);
    }

    @Override
    public void onRequestClose() {
     view.onRequestClose();
    }

    @Override
    public void onSubmitFeedback() {
        view.onSubmitFeedback();

    }

    @Override
    public void onGetIds(CommomResponse response) {
        view.onGetIds(response);

    }

    @Override
    public void onCloseRequestImp() {
        view.onCloseRequestImp();

    }

    @Override
    public void onFailure(String msg) {
        view.onFailure(msg);
    }
}
