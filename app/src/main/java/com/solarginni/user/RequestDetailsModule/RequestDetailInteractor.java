package com.solarginni.user.RequestDetailsModule;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.APIBody.RequestParam;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;

import retrofit2.Retrofit;

public class RequestDetailInteractor implements RequestDetailCOntract.Interector {
    private Retrofit retrofit;
    private ApiInterface apiInterface;


    public RequestDetailInteractor(Retrofit retrofit) {
        this.retrofit = retrofit;
        apiInterface = retrofit.create(ApiInterface.class);
    }

    @Override
    public void fetchAllRequest(String token,String requestType,String status, RequestDetailCOntract.OnIneraction ineraction) {

        apiInterface.getRequest(token,requestType,status).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                ineraction.onFetchRequest(response);
            }

            @Override
            public void onError(ApiError error) {
                ineraction.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {

                ineraction.onFailure(throwable.getMessage());
            }
        });

    }

    @Override
    public void closeRequest(String token, String impleRemark,String closureRemarl, String status, String requestId,String assignedTo, RequestDetailCOntract.OnIneraction ineraction) {
        apiInterface.closeRequest(token, requestId, impleRemark,closureRemarl,status,assignedTo).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                ineraction.onRequestClose();

            }

            @Override
            public void onError(ApiError error) {
                ineraction.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                ineraction.onFailure(throwable.getMessage());

            }
        });

    }

    @Override
    public void rateRequest(String token, String requestId, RequestParam param, RequestDetailCOntract.OnIneraction ineraction) {
        apiInterface.rateRequest(token,param,requestId).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                ineraction.onSubmitFeedback();

            }

            @Override
            public void onError(ApiError error) {
                ineraction.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                ineraction.onFailure(throwable.getMessage());

            }
        });

    }

    @Override
    public void fetchIds(String token, RequestDetailCOntract.OnIneraction ineraction) {
              apiInterface.getIds(token).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
                  @Override
                  public void onSuccess(CommomResponse response) {
                      ineraction.onGetIds(response);

                  }

                  @Override
                  public void onError(ApiError error) {

                      ineraction.onFailure(error.getMessage());
                  }

                  @Override
                  public void onFailure(Throwable throwable) {

                      ineraction.onFailure(throwable.getMessage());
                  }
              });
    }


}
