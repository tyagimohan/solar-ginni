package com.solarginni.user.RequestDetailsModule;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.solarginni.Base.BaseFragment;
import com.solarginni.DBModel.RequestData;
import com.solarginni.R;
import com.solarginni.Utility.Utils;

import static com.solarginni.Utility.AppConstants.CUSTOMER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.IMPLEMENTER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.SUPER_IMPLEMENTER_ROLE_ID;

public class RequestDetailsFragment1 extends BaseFragment{

    public static TextView requestId;
    private EditText  siteId, requestCategory, requestType, downSince, raisedOn, resolovedOn,customerMob,comments;

    @Override
    public int getLayout() {
        return R.layout.request_details_child1;
    }

    @Override
    public void initViews(View view) {
        requestId = view.findViewById(R.id.requestID);
        requestCategory = view.findViewById(R.id.requestCategory);
        requestType = view.findViewById(R.id.requestType);


        customerMob = view.findViewById(R.id.customerMob);

        siteId = view.findViewById(R.id.siteID);
        downSince = view.findViewById(R.id.downSince);
        raisedOn = view.findViewById(R.id.raisedOn);
        resolovedOn = view.findViewById(R.id.resolvedOn);
        comments = view.findViewById(R.id.comments);

        disableSuggestion(requestCategory);
        disableSuggestion(requestType);
        disableSuggestion(resolovedOn);
        disableSuggestion(comments);
        disableSuggestion(customerMob);
        disableSuggestion(siteId);
        disableSuggestion(downSince);
        disableSuggestion(downSince);
        disableSuggestion(raisedOn);



    }

    @Override
    public void setUp() {

        Bundle bundle = getArguments();
        RequestData.RequestDetail requestDetail = (RequestData.RequestDetail) bundle.getSerializable("data");


        requestId.setText(requestDetail.getRequestId());

        if (requestDetail.getComment() != null)
            comments.setText(requestDetail.getComment());

        requestType.setText(requestDetail.getRequestType());
        requestCategory.setText(requestDetail.getRequestCategory());
        siteId.setText(requestDetail.getSiteId());
        downSince.setText(Utils.convertDate(requestDetail.getDownSince()));
        customerMob.setText(requestDetail.getCustomerPhone());
        raisedOn.setText(Utils.convertUTCtoMyTime(requestDetail.getRaisedOn()));



        if (requestDetail.getRequestStatus().equalsIgnoreCase("Closed")) {
            resolovedOn.setText(Utils.convertUTCtoMyTime(requestDetail.getClosedOn()));
        } else if (requestDetail.getResolvedOn() != null) {
            resolovedOn.setText(Utils.convertUTCtoMyTime(requestDetail.getResolvedOn()));
        } else
            resolovedOn.setText("--");




    }


}
