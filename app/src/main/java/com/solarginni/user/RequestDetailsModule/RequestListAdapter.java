package com.solarginni.user.RequestDetailsModule;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.DBModel.RequestData;
import com.solarginni.R;
import com.solarginni.Utility.Utils;

/**
 * Created by Mohan on 24/2/20.
 */

public class RequestListAdapter extends BaseRecyclerAdapter<RequestData.RequestDetail, RequestListAdapter.Holder> {
 private boolean isForImp;

    public RequestListAdapter(Context mContext,boolean isForImp) {
        super(mContext);
        this.isForImp= isForImp;
    }

    @Override
    public int getLayout(int viewType) {
        return R.layout.request_item_layout;
    }

    @Override
    public Holder getViewHolder(View view, int viewType) {
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {


        if (isForImp){
            SpannableString content = new SpannableString(getItem(i).getCustomerPhone());
            content.setSpan(new UnderlineSpan(), 0, getItem(i).getCustomerPhone().length(), 0);
            holder.mobileNumber.setText(content);
            holder.raisedOn.setVisibility(View.GONE);
            holder.rating_status.setText(Utils.convertDate(getItem(i).getRaisedOn()));
            holder.mobileNumber.setOnClickListener(view -> {
                getLongClickListener().onItemClick(getItem(i),i,holder.mobileNumber);
            });

            holder.lay3.setVisibility(View.VISIBLE);
            holder.ratingImge.setVisibility(View.GONE);
        }
        else {
            holder.raisedOn.setText(Utils.convertDate(getItem(i).getRaisedOn()));
            if (getItem(i).getSiteId()!=null){
                holder.rating_status.setVisibility(View.VISIBLE);
                  holder.ratingImge.setVisibility(View.VISIBLE);
            }
            else {
                holder.rating_status.setVisibility(View.GONE);
                holder.ratingImge.setVisibility(View.GONE);
            }

            if (getItem(i).isRequestRating()){
                holder.ratingImge.setBackgroundResource(R.drawable.ic_happy);
                holder.rating_status.setText("Rated");
            }

            else{

                if (getItem(i).getRequestStatus().equalsIgnoreCase("Closed")){
                    holder.rating_status.setText("Rate");
                    holder.ratingImge.setBackgroundResource(R.drawable.ic_happy_grey);
                    holder.lay3.setOnClickListener(view -> {
                        getLongClickListener().onItemClick(getItem(i),i,holder.lay3);
                    });
                }
                else {
                    holder.rating_status.setText("Rate");
                }

            }
            SpannableString company = new SpannableString(getItem(i).getCompanyName());
            company.setSpan(new UnderlineSpan(), 0, getItem(i).getCompanyName().length(), 0);
            holder.mobileNumber.setText(company);
            holder.mobileNumber.setOnClickListener(view -> {
                getLongClickListener().onItemClick(getItem(i),i,holder.mobileNumber);
            });
        }

        SpannableString content = new SpannableString(getItem(i).getRequestId());
        content.setSpan(new UnderlineSpan(), 0,getItem(i).getRequestId().length(), 0);
        holder.request_id.setText(content);

        holder.requestType.setText(getItem(i).getRequestType());
        holder.requestStatus.setText(getItem(i).getRequestStatus());


    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView request_id, requestType,mobileNumber,raisedOn,requestStatus;
        private TextView rating_status;
        private ImageView ratingImge;
        private LinearLayout lay3;
        public Holder(@NonNull View itemView) {
            super(itemView);
            request_id = itemView.findViewById(R.id.request_id);
            requestType = itemView.findViewById(R.id.requestType);
            mobileNumber = itemView.findViewById(R.id.mobileNumber);
            raisedOn = itemView.findViewById(R.id.raisedOn);
            ratingImge = itemView.findViewById(R.id.ratingImg);
            rating_status = itemView.findViewById(R.id.rating);
            requestStatus = itemView.findViewById(R.id.requestStatus);
            lay3 = itemView.findViewById(R.id.lay3);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            getListener().onClick(
                    getItem(getAdapterPosition()), getAdapterPosition());

        }
    }
}
