package com.solarginni.user.RaiseRequestModule;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.solarginni.Base.BaseActivity;
import com.solarginni.CommonModule.MainActivity;
import com.solarginni.DBModel.APIBody.RaiseRequestModel;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.RequestLovs.RequestLoVs;
import com.solarginni.R;
import com.solarginni.Utility.Utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import static com.solarginni.Utility.AppConstants.TOKEN;

/**
 * Created by Mohan on 13/2/20.
 */

public class RaiseRequestActivity extends BaseActivity implements View.OnClickListener, RaiseRequestContract.RaiseRequestView {
    private final Calendar myCalendar = Calendar.getInstance();
    private EditText tvSiteId, tvRaiseedOn, tvRequestType, tvDown, tvRequestCategory, tvAmc, comment;
    private Button submit;
    private String[] categoryArray;
    private String[] requestTypeArray;
    private String[] siteArrays;
    private RaiseRequestPresenter presenter;

    @Override
    public int getLayout() {
        return R.layout.raise_request;
    }

    @Override
    public void initViews() {

        tvAmc = findViewById(R.id.tvAMC);
        tvSiteId = findViewById(R.id.tvSiteId);
        tvDown = findViewById(R.id.tvDown);
        tvRequestType = findViewById(R.id.tvRequest);
        tvRequestCategory = findViewById(R.id.tvRequestCategory);
        tvRaiseedOn = findViewById(R.id.tvRaisedOn);
        comment = findViewById(R.id.comment);


        disableSuggestion(tvAmc);
        disableSuggestion(tvSiteId);
        disableSuggestion(tvDown);
        disableSuggestion(tvRaiseedOn);
        disableSuggestion(tvRequestType);
        disableSuggestion(tvRequestCategory);

        submit = findViewById(R.id.confirmBtn);

        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("");
        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });

        presenter = new RaiseRequestPresenter(this);

    }

    @Override
    public void setUp() {

        Bundle bundle = getIntent().getExtras();

        tvSiteId.setText(bundle.getString("siteId", ""));

        if (bundle.getBoolean("forSiteId", false)) {
            ArrayList<String> list = new ArrayList<>();
            list = bundle.getStringArrayList("siteList");
            siteArrays = new String[list.size()];

            siteArrays = list.toArray(siteArrays);
            tvSiteId.setOnClickListener(this);

        }


        if (Utils.hasNetwork(this)) {

            showProgress("Please wait..");
            presenter.fetchRequestLovs(securePrefManager.getSharedValue(TOKEN), bundle.getString("siteId", ""));
        } else {
            showToast("Please check Internet connection");
        }


        tvDown.setOnClickListener(this);

        tvRequestCategory.setOnClickListener(this);
        tvRequestType.setOnClickListener(this);


        submit.setOnClickListener(this);


    }


    private String getDate(long milliSeconds) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvDown:
                openDatePicker();
                break;
            case R.id.confirmBtn:

                if (tvSiteId.getText().toString().trim().length() == 0) {
                    showToast("Please Select Site");
                    return;
                }
                if (tvRequestType.getText().toString().trim().length() == 0) {
                    showToast("Please Select Request Type");
                    return;
                } else if (tvRequestType.getText().toString().equalsIgnoreCase("Complaint") && (tvDown.getText().length() == 0 | tvRequestCategory.getText().length() == 0)) {
                    showToast("Please Fill Down Since and Request Category");
                } else {
                    if (Utils.hasNetwork(this)) {

                        showProgress("Please wait..");
                        RaiseRequestModel model = new RaiseRequestModel();

                        model.setAmcCovered(tvAmc.getText().toString());
                        model.setDownSince(tvDown.getText().toString());
                        model.setRaisedOn(tvRaiseedOn.getText().toString());
                        model.setRequestCategory(tvRequestCategory.getText().toString());
                        model.setRequestType(tvRequestType.getText().toString());
                        model.setComment(comment.getText().toString());

                        presenter.raiseRequest(securePrefManager.getSharedValue(TOKEN), tvSiteId.getText().toString(), model);
                    } else {
                        showToast(getString(R.string.internet_error));

                    }
                }



                /*Hit Submit Request Api*/
                break;
            case R.id.tvRequestCategory:
                showAlertWithList(categoryArray, R.string.rquest_category_title, tvRequestCategory);
                break;
            case R.id.tvRequest:
                showAlertWithListForreqType(requestTypeArray, R.string.rquest_type_title, tvRequestType);
                break;
            case R.id.tvSiteId:
                showAlertWithListForType(siteArrays, "Select Site", tvSiteId);
                break;
        }
    }

    private void openDatePicker() {

        int year = myCalendar.get(Calendar.YEAR);
        int month = myCalendar.get(Calendar.MONTH);
        int day = myCalendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.DialogTheme, (datePicker, year1, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year1);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            String myFormat = "dd/MM/yyyy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
            tvDown.setText(sdf.format(myCalendar.getTime()));

        }, year, month,
                day);


        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();

    }

    @Override
    public void onSubmitRequest(String msg) {
        showToast(msg);
        hideProgress();
        finish();

    }

    @Override
    public void onFetchParams(CommomResponse response) {
        hideProgress();
        RequestLoVs loVs = response.toResponseModel(RequestLoVs.class);
        categoryArray = new String[loVs.getRequestCategory().getLovValues().size()];

        for (int i = 0; i < loVs.getRequestCategory().getLovValues().size(); i++) {
            categoryArray[i] = loVs.getRequestCategory().getLovValues().get(i);
        }


        requestTypeArray = new String[loVs.getRequestType().getLovValues().size()];

        for (int i = 0; i < loVs.getRequestType().getLovValues().size(); i++) {
            requestTypeArray[i] = loVs.getRequestType().getLovValues().get(i);
        }

        if (loVs.getAmc()) {
            tvAmc.setText("YES");
        } else {
            tvAmc.setText("NO");
        }


        tvRaiseedOn.setText(getDate(System.currentTimeMillis()));

    }

    @Override
    public void onFailure(String msg) {
        hideProgress();
        if (msg.equalsIgnoreCase("invalid token")) {
            securePrefManager.clearAppsAllPrefs();
            startActivity(new Intent(RaiseRequestActivity.this, MainActivity.class));

        } else
            showToast(msg);
        finish();

    }

    public void showAlertWithListForreqType(final String[] dataArray, final @StringRes int title, final TextView textView) {
        hideKeyboard(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(title));
        builder.setItems(dataArray, (dialog, item) -> {
            if (dataArray[item].equalsIgnoreCase("Complaint")) {
                tvRequestCategory.setEnabled(true);
                tvDown.setEnabled(true);

            } else {
                tvRequestCategory.setEnabled(false);
                tvDown.setEnabled(false);
                tvDown.setText("");
                tvRequestCategory.setText("");

            }
            textView.setText(dataArray[item]);
        });
        AlertDialog alerts = builder.create();
        alerts.show();
    }

    public void showAlertWithListForType(final String[] dataArray, String title, final TextView textView) {
        hideKeyboard(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setItems(dataArray, (dialog, item) -> {
            if (Utils.hasNetwork(this)) {
                textView.setText(dataArray[item]);
                presenter.fetchRequestLovs(securePrefManager.getSharedValue(TOKEN), dataArray[item]);
            } else {
                showToast("Please Check Internet");
            }

        });
        AlertDialog alerts = builder.create();
        alerts.show();
    }
}
