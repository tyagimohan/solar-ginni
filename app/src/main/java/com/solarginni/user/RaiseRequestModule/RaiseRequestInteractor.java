package com.solarginni.user.RaiseRequestModule;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.APIBody.RaiseRequestModel;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;
import com.solarginni.network.ErrorUtils;

import retrofit2.Retrofit;

public class RaiseRequestInteractor implements RaiseRequestContract.RaiseRequestInteractor {
    private Retrofit retrofit;
    private ApiInterface apiInterfacel;

    public RaiseRequestInteractor(Retrofit retrofit) {
        this.retrofit = retrofit;
        apiInterfacel= retrofit.create(ApiInterface.class);
    }

    @Override
    public void raiseRequest(String token,String siteID, RaiseRequestModel model, RaiseRequestContract.onInteraction interaction) {
        apiInterfacel.raiseRequest(token,siteID,model).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                interaction.onSubmitRequest(response.getMessage());
            }

            @Override
            public void onError(ApiError error) {
                interaction.onFailure(error.getMessage());


            }

            @Override
            public void onFailure(Throwable throwable) {
             interaction.onFailure(throwable.getMessage());
            }
        });
    }

    @Override
    public void fetchRequestLovs(String token, String siteId, RaiseRequestContract.onInteraction interaction) {
        apiInterfacel.getRequestLOV(token,siteId).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                interaction.onFetchParams(response);

            }

            @Override
            public void onError(ApiError error) {
                interaction.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                interaction.onFailure(throwable.getMessage());

            }
        });

    }
}
