package com.solarginni.user.RaiseRequestModule;

import com.solarginni.DBModel.APIBody.RaiseRequestModel;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.RestClient;

public class RaiseRequestPresenter implements RaiseRequestContract.RaiseRequestPresenter, RaiseRequestContract.onInteraction {

    private RaiseRequestContract.RaiseRequestView view;
    private RaiseRequestInteractor interactor;

    public RaiseRequestPresenter(RaiseRequestContract.RaiseRequestView view) {
        this.view = view;
        interactor= new RaiseRequestInteractor(RestClient.getRetrofitBuilder());
    }

    @Override
    public void raiseRequest(String token,String siteID, RaiseRequestModel model) {
        interactor.raiseRequest(token,siteID,model,this);

    }

    @Override
    public void fetchRequestLovs(String token, String siteId) {
        interactor.fetchRequestLovs(token,siteId,this);

    }

    @Override
    public void onSubmitRequest(String msg) {
        view.onSubmitRequest(msg);

    }

    @Override
    public void onFetchParams(CommomResponse response) {
        view.onFetchParams(response);

    }

    @Override
    public void onFailure(String msg) {
        view.onFailure(msg);
    }
}
