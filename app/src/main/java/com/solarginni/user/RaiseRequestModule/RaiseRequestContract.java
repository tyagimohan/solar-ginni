package com.solarginni.user.RaiseRequestModule;

import com.solarginni.DBModel.APIBody.RaiseRequestModel;
import com.solarginni.DBModel.CommomResponse;

public class RaiseRequestContract {

    interface RaiseRequestView {

        void onSubmitRequest(String msg);
        void onFetchParams(CommomResponse response);
        void onFailure(String msg);

    }

    interface RaiseRequestPresenter {
        void raiseRequest(String token,String siteID, RaiseRequestModel model);
        void fetchRequestLovs(String token,String siteId);

    }

    interface RaiseRequestInteractor {
        void raiseRequest(String token,String siteID, RaiseRequestModel model, onInteraction interaction);
        void fetchRequestLovs(String token,String siteId,onInteraction interaction);
    }

    interface onInteraction {
        void onSubmitRequest(String msg);
        void onFetchParams(CommomResponse response);
        void onFailure(String msg);

    }

}
