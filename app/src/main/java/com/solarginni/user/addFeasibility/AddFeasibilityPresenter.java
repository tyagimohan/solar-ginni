package com.solarginni.user.addFeasibility;

import android.content.Context;

import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.RestClient;

import java.io.File;
import java.util.List;

public class AddFeasibilityPresenter implements AddFeasibilityContract.OnInteraction,AddFeasibilityContract.Presenter {

    private AddFeasibilityInteractor interactor;
    private AddFeasibilityContract.View view;

    public AddFeasibilityPresenter(AddFeasibilityContract.View view) {
        this.view = view;
        interactor= new AddFeasibilityInteractor(RestClient.getRetrofitBuilder());
    }

    @Override
    public void getFeasibilityLoV(String token) {
        interactor.getFeasibilityLoV(token,this);

    }

    @Override
    public void addFeasibitylity(Context context, String token, List<File> list, AddFeasibilityModel model, String optId) {
        interactor.addFeasibitylity(context,token,list,model,optId,this);

    }

    @Override
    public void onFeasibilityAdded(CommomResponse response) {
        view.onFeasibilityAdded(response);

    }

    @Override
    public void onGetLoV(CommomResponse response) {
        view.onGetLoV(response);

    }

    @Override
    public void onFailure(String msg) {
        view.onFailure(msg);

    }
}
