package com.solarginni.user.addFeasibility;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GetLovModel implements Serializable {

    @SerializedName("news")
    @Expose
    public LovModel newsLov;
    @SerializedName("phase")
    @Expose
    public LovModel phaseLove;
    @SerializedName("time")
    @Expose
    public LovModel timeLineList;
    @SerializedName("direction")
    @Expose
    public LovModel directionList;

    public static  class LovModel implements Serializable{

        @SerializedName("lovValues")
        public List<String> lovList = new ArrayList<>();



    }

}
