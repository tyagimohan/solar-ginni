package com.solarginni.user.addFeasibility;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import androidx.appcompat.app.AlertDialog;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.kbeanie.multipicker.api.CacheLocation;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.solarginni.Base.BaseActivity;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.R;
import com.solarginni.Utility.DecimalDigitsInputFilter;
import com.solarginni.Utility.NumberTextWatcherForThousand;
import com.solarginni.Utility.Permissions;
import com.solarginni.Utility.Utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import static com.solarginni.Utility.AppConstants.TOKEN;

public class AddFeasibililty extends BaseActivity implements View.OnClickListener, AddFeasibilityContract.View, ImagePickerCallback {

    private static final int REQUEST_CODE_FOR_PERMISSION = 2;
    private EditText phase, connectionId, sacntioned, unit, roofSpace, southD, eastD, westD, watertank, roofGen, budget, timeLine;
    private ImageView southImage, eastImage, westImage, nothImage;
    private AddFeasibilityPresenter presenter;
    private Button confirmBtn;
    private File southFile, eastFile, westFile, northFile;
    private String[] phaseArray, directionArray, newsArray, timeArray;
    private ImagePicker mImagePicker;
    private CameraImagePicker mCameraImagePicker;
    private int type = 0;


    @Override
    public int getLayout() {
        return R.layout.create_feasibility_layout;
    }

    @Override
    public void initViews() {

        phase = findViewById(R.id.phase);
        connectionId = findViewById(R.id.connectionId);
        sacntioned = findViewById(R.id.sanctioned_load);
        unit = findViewById(R.id.annualUnit);
        roofSpace = findViewById(R.id.roofSpace);
        southD = findViewById(R.id.southD);
        westD = findViewById(R.id.westD);
        eastD = findViewById(R.id.eastD);
        watertank = findViewById(R.id.waterTank);
        roofGen = findViewById(R.id.roofGen);
        budget = findViewById(R.id.budget);
        timeLine = findViewById(R.id.timeLine);
        southImage = findViewById(R.id.southImage);
        eastImage = findViewById(R.id.eastImage);
        nothImage = findViewById(R.id.northImage);
        westImage = findViewById(R.id.westImage);
        confirmBtn = findViewById(R.id.confirmBtn);

        disableSuggestion(phase);
        disableSuggestion(connectionId);
        disableSuggestion(sacntioned);
        disableSuggestion(unit);
        disableSuggestion(roofGen);
        disableSuggestion(roofSpace);
        disableSuggestion(southD);
        disableSuggestion(westD);
        disableSuggestion(eastD);
        disableSuggestion(watertank);
        disableSuggestion(budget);
        disableSuggestion(timeLine);

        phase.setOnClickListener(this);
        southImage.setOnClickListener(this);
        eastImage.setOnClickListener(this);
        westImage.setOnClickListener(this);
        nothImage.setOnClickListener(this);
        southD.setOnClickListener(this);
        westD.setOnClickListener(this);
        eastD.setOnClickListener(this);
        watertank.setOnClickListener(this);
        timeLine.setOnClickListener(this);
        roofGen.setOnClickListener(this);
        sacntioned.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(7,2)});



        budget.addTextChangedListener(new NumberTextWatcherForThousand(budget));
        unit.addTextChangedListener(new NumberTextWatcherForThousand(unit));
        roofSpace.addTextChangedListener(new NumberTextWatcherForThousand(roofSpace));


        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("");
        toolbar.setTitle("");
        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });



        mImagePicker = new ImagePicker(this);
        mImagePicker.setCacheLocation(CacheLocation.INTERNAL_APP_DIR);
        mImagePicker.setImagePickerCallback(this);
        mImagePicker.shouldGenerateThumbnails(false);
        mImagePicker.shouldGenerateMetadata(false);
        mCameraImagePicker = new CameraImagePicker(this);
        mCameraImagePicker.setCacheLocation(CacheLocation.INTERNAL_APP_DIR);
        mCameraImagePicker.setImagePickerCallback(this);
        mCameraImagePicker.shouldGenerateThumbnails(false);
        mCameraImagePicker.shouldGenerateMetadata(false);

        confirmBtn.setOnClickListener(this);
        presenter = new AddFeasibilityPresenter(this);


    }

    @Override
    public void setUp() {

        if (Utils.hasNetwork(this)) {
            showProgress("Please wait...");
            presenter.getFeasibilityLoV(getResources().getString(R.string.static_token));
        } else {
            showToast("Please check Internet");

        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.phase:
                showAlertWithList(phaseArray, "Please Select Phase", phase);
                break;
            case R.id.southD:
                showAlertWithList(directionArray, "Select Value", southD);
                break;
            case R.id.eastD:
                showAlertWithList(directionArray, "Select Value", eastD);
                break;
            case R.id.westD:
                showAlertWithList(directionArray, "Select Value", westD);
                break;
            case R.id.southImage:
                type = 0;
                if (checkPermissionAPI23()) {
                    getImagePicker();
                }
                break;
            case R.id.westImage:
                type = 1;
                if (checkPermissionAPI23()) {
                    getImagePicker();
                }
                break;
            case R.id.eastImage:
                type = 2;
                if (checkPermissionAPI23()) {
                    getImagePicker();
                }
                break;
            case R.id.northImage:
                if (checkPermissionAPI23()) {
                    getImagePicker();
                }
                type = 3;
                break;
            case R.id.waterTank:
                showAlertWithList(newsArray, "Please Select Direction", watertank);
                break;
            case R.id.timeLine:
                showAlertWithList(timeArray, "Please Select timeline", timeLine);
                break;
            case R.id.roofGen:
                showAlertWithList(newsArray, "Please Select Direction", roofGen);
                break;
            case R.id.confirmBtn:

                if (phase.getText().toString().trim().length() == 0 | sacntioned.getText().toString().trim().length() == 0 | unit.getText().toString().trim().length() == 0 | roofSpace.getText().toString().trim().length() == 0) {
                    showToast("Please fill * values");
                    return;
                }
                else if (Double.parseDouble(sacntioned.getText().toString().replace(",",""))<1.0){
                    showToast("Sanctioned value should be greater than 1.0");
                    return;
                }
                else if (Integer.parseInt(unit.getText().toString().replace(",",""))<100){
                    showToast("Annual Unit Should be greater than 100 ");
                    return;
                }
                else if (Integer.parseInt(roofSpace.getText().toString().replace(",",""))<100){
                    showToast("Roof Space Should be greater than 100 ");
                }

                else {
                    showProgress("Please wait...");
                    AddFeasibilityModel model = new AddFeasibilityModel();
                    model.phase= phase.getText().toString();
                    String unitUsed = unit.getText().toString().replace(",","");
                    model.annualUnit = unitUsed.trim().length()==0?0: Integer.valueOf(unitUsed);
                    String price = budget.getText().toString().replace(",","");
                    model.budget = price.trim().length() == 0 ? 0 :Integer.valueOf(price);
                    model.connectionId = connectionId.getText().toString();
                    model.eastD = eastD.getText().toString();
                    model.southD = southD.getText().toString();
                    model.westD = westD.getText().toString();
                    model.waterT = watertank.getText().toString();
                    String roofspace = roofSpace.getText().toString().replace(",","");
                    model.roofSpace = roofspace.trim().length()==0?0:Integer.valueOf(roofspace);
                    model.gensetD = roofGen.getText().toString();
                    model.timeline = timeLine.getText().toString();
                    model.sancLoad = Double.valueOf(sacntioned.getText().toString());

                    List<File> fileList = new ArrayList<>();
                    fileList.add(southFile);
                    fileList.add(westFile);
                    fileList.add(eastFile);
                    fileList.add(northFile);

                    presenter.addFeasibitylity(this, securePrefManager.getSharedValue(TOKEN), fileList,model,getIntent().getExtras().getString("optId","")  );

                }

                break;

        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Picker.PICK_IMAGE_DEVICE:
                if (resultCode == RESULT_OK) {
                    mImagePicker.submit(data);
                } else {

                }
                break;

            case Picker.PICK_IMAGE_CAMERA:
                if (resultCode == RESULT_OK) {
                    mCameraImagePicker.submit(data);
                } else {

                }
                break;
        }

    }


    private boolean checkPermissionAPI23() {
        boolean isPermissionRequired = false;
        List<String> permissionArray = new ArrayList<>();

        if (Permissions.getInstance().hasCameraPermission(this).permissions.size() > 0) {
            permissionArray.add(Manifest.permission.CAMERA);
            isPermissionRequired = true;
        }

        if (Permissions.getInstance().checkReadWritePermissions(this).permissions.size() > 0) {
            isPermissionRequired = true;
            permissionArray.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            permissionArray.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (isPermissionRequired) {
            Permissions.getInstance().addPermission(permissionArray).askForPermissions(this, REQUEST_CODE_FOR_PERMISSION);
            return false;
        }
        return true;
    }

    private void getImagePicker() {
        CharSequence[] options = getResources().getStringArray(R.array.image_picker_options);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(options, (dialog, which) -> {
            switch (which) {
                case 0:
                    getImageFromCamera();
                    break;
                case 1:
                    getImageFromGallery();
                    break;
                default:
                    break;
            }
        });
        builder.show();
    }

    private void getImageFromGallery() {
        mImagePicker.pickImage();

    }

    private void getImageFromCamera() {

        mCameraImagePicker.pickImage();

    }

    @Override
    public void onFeasibilityAdded(CommomResponse response) {
        hideProgress();
        setResult(Activity.RESULT_OK);
        finish();

    }

    @Override
    public void onGetLoV(CommomResponse response) {
        hideProgress();
        GetLovModel model = response.toResponseModel(GetLovModel.class);

        phaseArray = new String[model.phaseLove.lovList.size()];
        timeArray = new String[model.timeLineList.lovList.size()];
        newsArray = new String[model.newsLov.lovList.size()];
        directionArray = new String[model.directionList.lovList.size()];

        phaseArray = model.phaseLove.lovList.toArray(phaseArray);
        timeArray = model.timeLineList.lovList.toArray(timeArray);
        directionArray = model.directionList.lovList.toArray(directionArray);
        newsArray = model.newsLov.lovList.toArray(newsArray);


    }

    @Override
    public void onFailure(String msg) {
        hideProgress();
        showToast(msg);

    }

    @Override
    public void onImagesChosen(List<ChosenImage> list) {
        Bitmap bitmap=null;
        OutputStream os = null;
        switch (type) {
            case 0:
                southFile= new File(list.get(0).getOriginalPath());
                 bitmap =Utils.compressImage(southFile.getPath());
                southImage.setImageBitmap(bitmap);

                try {
                    os = new BufferedOutputStream(new FileOutputStream(southFile));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case 1:
                westFile= new File(list.get(0).getOriginalPath());
                 bitmap =Utils.compressImage(westFile.getPath());
                westImage.setImageBitmap(bitmap);

                try {
                    os = new BufferedOutputStream(new FileOutputStream(westFile));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case 2:
                eastFile= new File(list.get(0).getOriginalPath());
                 bitmap =Utils.compressImage(eastFile.getPath());
                eastImage.setImageBitmap(bitmap);
                try {
                    os = new BufferedOutputStream(new FileOutputStream(eastFile));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case 3:
                northFile= new File(list.get(0).getOriginalPath());
                 bitmap =Utils.compressImage(northFile.getPath());
                nothImage.setImageBitmap(bitmap);

                try {
                    os = new BufferedOutputStream(new FileOutputStream(northFile));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }



    }

    @Override
    public void onError(String s) {

    }
}
