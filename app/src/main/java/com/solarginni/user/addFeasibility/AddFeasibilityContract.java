package com.solarginni.user.addFeasibility;

import android.content.Context;

import com.solarginni.DBModel.CommomResponse;

import java.io.File;
import java.util.List;

public interface AddFeasibilityContract {

    interface View {
        void onFeasibilityAdded(CommomResponse response);

        void onGetLoV(CommomResponse response);

        void onFailure(String msg);
    }

    interface Presenter {

        void getFeasibilityLoV(String token);

        void addFeasibitylity(Context context, String token, List<File> list, AddFeasibilityModel model, String optId);
    }


    interface Interactor {
        void getFeasibilityLoV(String token, OnInteraction listenr);

        void addFeasibitylity(Context context, String token, List<File> list, AddFeasibilityModel model, String optId, OnInteraction listener);
    }

    interface OnInteraction {
        void onFeasibilityAdded(CommomResponse response);

        void onGetLoV(CommomResponse response);

        void onFailure(String msg);
    }
}



