package com.solarginni.user.addFeasibility;

import android.content.Context;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;
import com.solarginni.network.MultipartParams;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Retrofit;

public class AddFeasibilityInteractor implements AddFeasibilityContract.Interactor {

    private Retrofit retrofit;
    private ApiInterface apiInterface;

    public AddFeasibilityInteractor(Retrofit retrofit) {
        this.retrofit = retrofit;
        apiInterface = retrofit.create(ApiInterface.class);
    }

    @Override
    public void getFeasibilityLoV(String token, AddFeasibilityContract.OnInteraction listenr) {

        apiInterface.getFLov(token).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                listenr.onGetLoV(response);
            }

            @Override
            public void onError(ApiError error) {
                listenr.onFailure(error.getMessage());
            }

            @Override
            public void onFailure(Throwable throwable) {
                listenr.onFailure(throwable.getMessage());
            }
        });
    }

    @Override
    public void addFeasibitylity(Context context, String token, List<File> list, AddFeasibilityModel model, String optId, AddFeasibilityContract.OnInteraction listener) {
        MultipartParams multipartParams = null;
        MultipartParams.Builder builder = new MultipartParams.Builder();
        for (int i = 0; i < list.size(); i++) {
            switch (i) {
                case 0:
                    builder.addFile("south", list.get(i));
                    break;
                case 1:
                    builder.addFile("west", list.get(i));
                    break;
                case 2:
                    builder.addFile("east", list.get(i));
                    break;
                case 3:
                    builder.addFile("north", list.get(i));
                    break;
            }


        }
        multipartParams = builder.build();

        if (  multipartParams.getMap().size()==0){

            builder.addBlankFile("Blank");
            multipartParams = builder.build();
        }
        apiInterface.addFeasibility(token, optId, multipartParams.getMap(), model.phase,
                model.connectionId,
                model.budget,
                model.annualUnit,
                model.sancLoad,
                model.roofSpace,
                model.gensetD,
                model.southD,
                model.eastD,
                model.westD,
                model.waterT,
                model.timeline).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                listener.onFeasibilityAdded(response);

            }

            @Override
            public void onError(ApiError error) {
                listener.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                listener.onFailure(throwable.getMessage());

            }
        });
    }


}
