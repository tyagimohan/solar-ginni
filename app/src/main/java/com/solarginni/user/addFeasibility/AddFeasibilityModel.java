package com.solarginni.user.addFeasibility;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AddFeasibilityModel implements Serializable {


    @SerializedName("annualUnit")
    @Expose
    public int annualUnit;
    @SerializedName("budget")
    @Expose
    public int budget;
    @SerializedName("connectionId")
    @Expose
    public String connectionId;
    @SerializedName("eastD")
    @Expose
    public String eastD;
    @SerializedName("gensetD")
    @Expose
    public String gensetD;
    @SerializedName("phase")
    @Expose
    public String phase;
    @SerializedName("roofSpace")
    @Expose
    public int roofSpace;
    @SerializedName("sancLoad")
    @Expose
    public double sancLoad;
    @SerializedName("southD")
    @Expose
    public String southD;
    @SerializedName("timeline")
    @Expose
    public String timeline;
    @SerializedName("waterT")
    @Expose
    public String waterT;
    @SerializedName("westD")
    @Expose
    public String westD;

    @SerializedName("imageE")
    @Expose
    public String imageE;
    @SerializedName("imageW")
    @Expose
    public String imageW;
    @SerializedName("imageN")
    @Expose
    public String imageN;
    @SerializedName("imageS")
    @Expose
    public String imageS;

    @SerializedName("optId")
    @Expose
    public String optId;

}
