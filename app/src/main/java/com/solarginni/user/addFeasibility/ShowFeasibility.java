package com.solarginni.user.addFeasibility;

import android.widget.EditText;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.solarginni.Base.BaseActivity;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.R;
import com.solarginni.Utility.Utils;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import static com.solarginni.Utility.AppConstants.TOKEN;

public class ShowFeasibility extends BaseActivity {
    private EditText phase, connectionId, sacntioned, unit, roofSpace, southD, eastD, westD, watertank, roofGen, budget, timeLine;
    private ImageView southImage, eastImage, westImage, nothImage;


    @Override
    public int getLayout() {
        return R.layout.show_feasibility;
    }

    @Override
    public void initViews() {

        phase = findViewById(R.id.phase);
        connectionId = findViewById(R.id.connectionId);
        sacntioned = findViewById(R.id.sanctioned_load);
        unit = findViewById(R.id.annualUnit);
        roofSpace = findViewById(R.id.roofSpace);
        southD = findViewById(R.id.southD);
        westD = findViewById(R.id.westD);
        eastD = findViewById(R.id.eastD);
        watertank = findViewById(R.id.waterTank);
        roofGen = findViewById(R.id.roofGen);
        budget = findViewById(R.id.budget);
        timeLine = findViewById(R.id.timeLine);
        southImage = findViewById(R.id.southImage);
        eastImage = findViewById(R.id.eastImage);
        nothImage = findViewById(R.id.northImage);
        westImage = findViewById(R.id.westImage);


        disableSuggestion(phase);
        disableSuggestion(connectionId);
        disableSuggestion(sacntioned);
        disableSuggestion(unit);
        disableSuggestion(roofGen);
        disableSuggestion(roofSpace);
        disableSuggestion(southD);
        disableSuggestion(westD);
        disableSuggestion(eastD);
        disableSuggestion(watertank);
        disableSuggestion(budget);
        disableSuggestion(timeLine);


        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("");
        toolbar.setTitle("");
        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });

    }

    @Override
    public void setUp() {

        if (Utils.hasNetwork(this)){
            showProgress("Please wait...");
            ShowFeasibilityApi api= new ShowFeasibilityApi() {
                @Override
                public void onComplete(CommomResponse response) {
                    hideProgress();
                    AddFeasibilityModel model = response.toResponseModel(AddFeasibilityModel.class);
                    showData(model);

                }

                @Override
                protected void onFailur(String msg) {
                       hideProgress();

                       showToast(msg);
                }
            };
            api.hit(securePrefManager.getSharedValue(TOKEN),getIntent().getExtras().getString("optId",""));

        }
        else {
            showToast("Please check Internet");
        }



    }

    private void showData(AddFeasibilityModel model){


        phase.setText(model.phase);
        connectionId.setText(model.connectionId);
        sacntioned.setText(String.valueOf(model.sancLoad));
        eastD.setText(model.eastD);

        southD.setText(model.southD);
        westD.setText(model.westD);
        watertank.setText(model.waterT);
        roofGen.setText(model.gensetD);

        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        formatter.applyPattern("#,##,##,###");
        String formattedString = formatter.format(model.budget);
        budget.setText(formattedString);

        String formattedUnit= formatter.format(model.annualUnit);
        unit.setText(formattedUnit);

        String formattedRF= formatter.format(model.roofSpace);
        roofSpace.setText(formattedRF);




        timeLine.setText(model.timeline);

        try {
            if (!model.imageE.isEmpty())
                Glide.with(this).load(model.imageE).into(eastImage);
        } catch (Exception e) {
            e.printStackTrace();

        }
        try {
            if (!model.imageS.isEmpty())
                Glide.with(this).load(model.imageS).into(southImage);
        } catch (Exception e) {
            e.printStackTrace();

        }
        try {
            if (!model.imageN.isEmpty())
                Glide.with(this).load(model.imageN).into(nothImage);
        } catch (Exception e) {
            e.printStackTrace();

        }
        try {
            if (!model.imageW.isEmpty())
                Glide.with(this).load(model.imageW).into(westImage);
        } catch (Exception e) {
            e.printStackTrace();

        }

    }
}
