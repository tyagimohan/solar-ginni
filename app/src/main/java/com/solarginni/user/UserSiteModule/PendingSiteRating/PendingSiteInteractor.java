package com.solarginni.user.UserSiteModule.PendingSiteRating;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.APIBody.SiteRatingModel;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;

import retrofit2.Retrofit;

public class PendingSiteInteractor implements  PendingSiteContract.Interector{

    private Retrofit retrofit;
    private ApiInterface apiInterface;


    public PendingSiteInteractor(Retrofit retrofit) {
        this.retrofit = retrofit;
        apiInterface= retrofit.create(ApiInterface.class);
    }

    @Override
    public void rateSite(String token, String siteID, SiteRatingModel model, PendingSiteContract.OnInteraction listener) {
        apiInterface.submitRating(model,siteID,token).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                listener.onSiteRated();
            }

            @Override
            public void onError(ApiError error) {
                listener.OnFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                listener.OnFailure(throwable.getMessage());

            }
        });

    }
}
