package com.solarginni.user.UserSiteModule;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.solarginni.Base.BaseFragment;
import com.solarginni.DBModel.SiteDetailsModel.SiteDetails;
import com.solarginni.R;
import com.solarginni.Utility.Utils;

import static com.solarginni.Utility.AppConstants.SITEDETAILSMODEL;

public class SiteLandinngChild1 extends BaseFragment {
    public EditText tvSolution, tvSiteType, tvConnectionID,tvPhase,tvSiteSize;
    public  EditText tvSiteId;

    @Override
    public int getLayout() {
        return R.layout.site_landing_child1;
    }

    @Override
    public void initViews(View view) {

        tvSiteId = view.findViewById(R.id.tvSiteId);
        tvSolution = view.findViewById(R.id.solution);
        tvSiteType = view.findViewById(R.id.siteType);
        tvPhase = view.findViewById(R.id.phase);
        tvConnectionID = view.findViewById(R.id.connectionId);
        tvSiteSize = view.findViewById(R.id.siteSize);
        disableSuggestion(tvSiteType);
        disableSuggestion(tvSolution);
        disableSuggestion(tvPhase);
        disableSuggestion(tvConnectionID);
        disableSuggestion(tvSiteSize);
        disableSuggestion(tvSiteId);

    }

    @Override
    public void setUp() {


        SiteDetails details= (SiteDetails) getArguments().getSerializable("data");

        tvSiteSize.setText(details.getSiteSize());
        tvSiteId.setText(details.getSiteId());
        tvConnectionID.setText(details.getConnectionId());
        tvPhase.setText(details.getPhase());
        tvSolution.setText(details.getSolutionType());
        tvSiteType.setText(details.getSiteType());



    }
}
