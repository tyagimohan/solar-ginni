package com.solarginni.user.UserSiteModule.PendingSiteRating;

import com.solarginni.DBModel.APIBody.SiteRatingModel;

public interface PendingSiteContract {

    interface View {
        void onSiteRated();

        void OnFailure(String msg);
    }

    interface Presenter {
        void rateSite(String token, String siteID, SiteRatingModel model);
    }

    interface Interector {
        void rateSite(String token, String siteID, SiteRatingModel model, OnInteraction listener);

    }


    interface OnInteraction {
        void onSiteRated();

        void OnFailure(String msg);

    }
}
