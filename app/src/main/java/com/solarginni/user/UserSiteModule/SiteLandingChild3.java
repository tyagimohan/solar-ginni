package com.solarginni.user.UserSiteModule;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.solarginni.Base.BaseFragment;
import com.solarginni.DBModel.SiteDetailsModel.Inverter;
import com.solarginni.DBModel.SiteDetailsModel.SiteDetails;
import com.solarginni.R;
import com.solarginni.Utility.Utils;

import java.util.List;

import static com.solarginni.Utility.AppConstants.SITEDETAILSMODEL;

public class SiteLandingChild3 extends BaseFragment {
    public EditText tvPanelMake, tvPanelType, tvPanelSize, batterySize, batterySerial, tvBatteryMake;
    public  LinearLayout batteryLayout;
    public  LinearLayout linearLayout;
    private View dynamicEntryView;

    @Override
    public int getLayout() {
        return R.layout.site_landing_child3;
    }

    @Override
    public void initViews(View view) {

        tvPanelSize = view.findViewById(R.id.panelSize);
        tvPanelType = view.findViewById(R.id.panelType);
        tvPanelMake = view.findViewById(R.id.panelMake);
        linearLayout = view.findViewById(R.id.linearLayout);
        batteryLayout = view.findViewById(R.id.batteryLayout);
        batterySize = view.findViewById(R.id.batterySize);
        batterySerial = view.findViewById(R.id.batterySerial);
        tvBatteryMake = view.findViewById(R.id.batteryMake);

        disableSuggestion(tvPanelSize);
        disableSuggestion(tvBatteryMake);
        disableSuggestion(tvPanelMake);
        disableSuggestion(batterySerial);
        disableSuggestion(batterySize);
        disableSuggestion(tvPanelType);


    }

    @Override
    public void setUp() {

        SiteDetails details = (SiteDetails) getArguments().getSerializable("data");


//        tvInverterMake.setText(details.getInverter().get(0).getInverterMake());
//        tvInverterModel.setText(details.getInverter().get(0).getInverterSerial());
//        tvInverterSize.setText(details.getInverter().get(0).getInverterSize());
        createDynamicLayout(details.getInverter());
        tvPanelMake.setText(details.getPanelMake());
        tvPanelSize.setText(details.getPanelSize());
        tvPanelType.setText(details.getPanelType());

        if (details.getSolutionType().equalsIgnoreCase("Hybrid")) {
            batteryLayout.setVisibility(View.VISIBLE);
            batterySerial.setText(details.getBattery().getBatterySerial());
            tvBatteryMake.setText(details.getBattery().getBatteryMake());
            batterySize.setText(details.getBattery().getBatterySize());
        } else {
            batteryLayout.setVisibility(View.GONE);

        }

    }

    private void createDynamicLayout(List<Inverter> list) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        linearLayout.removeAllViews();

        for (int i = 0; i < list.size(); i++) {
            dynamicEntryView = inflater.inflate(R.layout.child_layout, null);


            TextView inverterMake = dynamicEntryView.findViewById(R.id.inverterMake);
            TextView inertMakeModel = dynamicEntryView.findViewById(R.id.inverterModel);
            TextView inertMakeSize = dynamicEntryView.findViewById(R.id.inverterSize);

            disableSuggestion(inverterMake);
            disableSuggestion(inertMakeModel);
            disableSuggestion(inertMakeSize);

            inertMakeModel.setFocusableInTouchMode(false);
            inertMakeModel.setFocusable(false);

            inertMakeSize.setFocusableInTouchMode(false);
            inertMakeSize.setFocusable(false);

            inverterMake.setFocusableInTouchMode(false);
            inverterMake.setFocusable(false);

            inertMakeModel.setText(list.get(i).getInverterSerial());
            inverterMake.setText(list.get(i).getInverterMake());
            inertMakeSize.setText(list.get(i).getInverterSize());

            linearLayout.addView(dynamicEntryView);
        }


    }
}
