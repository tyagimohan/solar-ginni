package com.solarginni.user.UserSiteModule;

import android.content.Context;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.solarginni.Base.BaseFragment;
import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.CustomizeView.CustomRatingBar;
import com.solarginni.DBModel.APIBody.SiteRatingModel;
import com.solarginni.DBModel.SiteDetailsModel.SiteDetails;
import com.solarginni.DBModel.SiteDetailsModel.SiteDetailsModel;
import com.solarginni.Implementer.ImplementerDetails.ImplemeterDetailsActivity;
import com.solarginni.R;
import com.solarginni.Utility.Utils;
import com.solarginni.user.UserSiteModule.PendingSiteRating.PendingSiteContract;
import com.solarginni.user.UserSiteModule.PendingSiteRating.PendingSitePresenter;

import java.io.Serializable;

import static com.solarginni.Utility.AppConstants.SITEDETAILSMODEL;
import static com.solarginni.Utility.AppConstants.TOKEN;

public class SiteListingFragment extends BaseFragment implements PendingSiteContract.View {

    private RecyclerView recyclerView;
    private PendingSitePresenter presenter;
    private SiteListAdapter adapter;
    private LinearLayout layout;
    private TextView emptyView;
    private int pos;
    private SiteListAdapter.OnClick<SiteDetails> listener = (data, position) -> {

        Bundle bundle = new Bundle();
        bundle.putSerializable("data", (Serializable) data);
        goToNextScreen(SiteLandingAcity.class, bundle);


    };
    private SiteListAdapter.onItemClick<SiteDetails> longListener = ((data, position, view) -> {

        switch (view.getId()) {
            case R.id.implementerName:
                Bundle bundle = new Bundle();
                bundle.putBoolean("isFormasking",false);
                bundle.putString("id", data.getCompanyId());
                goToNextScreen(ImplemeterDetailsActivity.class, bundle);
                break;
            case R.id.lay3:
                if (!data.getSiteRated()) {
                    pos = position;
                    showRatingDailog(getContext(), data.getSiteId());
                }
                break;
        }



    });

    @Override
    public int getLayout() {
        return R.layout.site_list;
    }

    @Override
    public void initViews(View view) {
        recyclerView = view.findViewById(R.id.recyclerView);
        layout = view.findViewById(R.id.upperLayout);
        emptyView = view.findViewById(R.id.emptyView);
        presenter = new PendingSitePresenter(this);
        ((TextView) view.findViewById(R.id.implementerName)).setText("Implementer");
        ((TextView) view.findViewById(R.id.ratingLabel)).setText("Rating");


    }

    @Override
    public void setUp() {

        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new BaseRecyclerAdapter.DividerItemDecoration(getContext(), RecyclerView.VERTICAL));

        SiteDetailsModel model = Utils.getObj(securePrefManager.getSharedValue(SITEDETAILSMODEL), SiteDetailsModel.class);

        adapter = new SiteListAdapter(getContext());
        adapter.addAll(model.getSiteDetails());
        adapter.setOnClickListener(listener);
        adapter.setOnLongClickListener(longListener);
        recyclerView.setAdapter(adapter);


    }

    @Override
    public void onSiteRated() {
        hideProgress();
        adapter.getItem(pos).setSiteRated(true);
        adapter.notifyDataSetChanged();

    }

    @Override
    public void OnFailure(String msg) {
        hideProgress();
        showToast(msg);

    }

    private void showRatingDailog(Context context, String siteId) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.service_rating, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);
        AlertDialog alertDialog = dialogBuilder.create();
        CustomRatingBar timeLine = dialogView.findViewById(R.id.rating);
        CustomRatingBar qualityRating = dialogView.findViewById(R.id.qualityRating);
        CustomRatingBar pricingRating = dialogView.findViewById(R.id.pricingRating);
        CustomRatingBar teamRating = dialogView.findViewById(R.id.teamRating);
        Button submitBtn = dialogView.findViewById(R.id.confirmBtn);

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((int) timeLine.getScore() > 0 || (int) pricingRating.getScore() > 0 || (int) qualityRating.getScore() > 0 || (int) teamRating.getScore() > 0) {
                    showProgress("Please wait...");
                    SiteRatingModel model = new SiteRatingModel();
                    model.setTime((int) timeLine.getScore());
                    model.setPrice((int) pricingRating.getScore());
                    model.setQuality((int) qualityRating.getScore());
                    model.setTeam((int) teamRating.getScore());
                    presenter.rateSite(securePrefManager.getSharedValue(TOKEN), siteId, model);

                    alertDialog.dismiss();
                } else {
                    showToast("Please Rate Service");
                }


            }
        });


        alertDialog.show();

    }


}
