package com.solarginni.user.UserSiteModule;

import com.google.android.material.textfield.TextInputLayout;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.solarginni.Base.BaseFragment;
import com.solarginni.DBModel.SiteDetailsModel.SiteDetails;
import com.solarginni.R;
import com.solarginni.Utility.Utils;

public class
SiteLandingChild2 extends BaseFragment {
    public EditText tvInstalledBy, tvInstalledOn, tvAMC,city, state, country, pincode,customerMob;
    public  EditText etLine1, addLine2;
    private TextInputLayout implementerlay,customerlay;
    private TextView infoTitle;

    @Override
    public int getLayout() {
        return R.layout.site_landing_child2;
    }

    @Override
    public void initViews(View view) {
        tvAMC = view.findViewById(R.id.amc);
        tvInstalledBy = view.findViewById(R.id.installedBy);
        customerMob = view.findViewById(R.id.customerMob);
        tvInstalledOn = view.findViewById(R.id.installedOn);
        state = view.findViewById(R.id.state);
        city = view.findViewById(R.id.cityName);
        country = view.findViewById(R.id.country);
        pincode = view.findViewById(R.id.pincode);
        etLine1 = view.findViewById(R.id.etLine1);
        addLine2 = view.findViewById(R.id.line2);
        customerlay = view.findViewById(R.id.customerlay);
        implementerlay = view.findViewById(R.id.implementerlay);
        infoTitle= view.findViewById(R.id.infoTitle);
        disableSuggestion(tvAMC);
        disableSuggestion(tvInstalledBy);
        disableSuggestion(tvInstalledOn);
        disableSuggestion(city);
        disableSuggestion(etLine1);
        disableSuggestion(addLine2);
        disableSuggestion(country);
        disableSuggestion(state);
        disableSuggestion(pincode);




    }

    @Override
    public void setUp() {

        SiteDetails details = (SiteDetails) getArguments().getSerializable("data");

        if (getArguments().getBoolean("isFromImp", false)) {
            customerMob.setText(details.getPhone());
            customerlay.setVisibility(View.VISIBLE);
            implementerlay.setVisibility(View.GONE);
            infoTitle.setText("Customer Info");

        } else {
            tvInstalledBy.setText(details.getInstalledBy());
            customerlay.setVisibility(View.GONE);
            implementerlay.setVisibility(View.VISIBLE);
            infoTitle.setText("Installer Info");


        }
        tvAMC.setText(Utils.convertDate(details.getAmcEnd()));
        tvInstalledOn.setText(Utils.convertDate(details.getInstalledOn()));
        etLine1.setText(details.getAddress().getAddressLine1());
        addLine2.setText(details.getAddress().getAddressLine2());
        city.setText(details.getAddress().getCity());
        state.setText(details.getAddress().getState());
        country.setText(details.getAddress().getCountry());
        pincode.setText(details.getAddress().getPinCode());

    }
}
