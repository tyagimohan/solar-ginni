package com.solarginni.user.UserSiteModule;

import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.solarginni.Base.BaseActivity;
import com.solarginni.CustomizeView.CustomPager;
import com.solarginni.DBModel.SiteDetailsModel.SiteDetails;
import com.solarginni.R;
import com.solarginni.SGUser.SiteModule.SiteDetailsFragment1;
import com.solarginni.SGUser.SiteModule.SiteDetailsFragment2;
import com.solarginni.SGUser.SiteModule.SiteDetailsFragments3;
import com.solarginni.SGUser.SiteModule.UpdateSiteActivity;

/**
 * Created by Mohan on 4/2/20.
 */

public class SiteLandingAcity extends BaseActivity implements View.OnClickListener {
    public CustomPager viewPager;
    private ViewPagerAdapter adapter;
    private ImageView site_image;
    private TabLayout tabIndicator;
    private FloatingActionButton updateSite;


    @Override
    public int getLayout() {
        return R.layout.site_landing;
    }


    @Override
    public void initViews() {
        tabIndicator = findViewById(R.id.tabIndicator);
        viewPager = findViewById(R.id.viewPager);
        site_image = findViewById(R.id.site_image);
        updateSite = findViewById(R.id.updateSite);

        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("");
        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });
        adapter = new ViewPagerAdapter(getSupportFragmentManager());


    }

    @Override
    public void setUp() {
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(adapter);
        viewPager.setOffscreenPageLimit(2);
        tabIndicator.setupWithViewPager(viewPager);
        tabIndicator.setSelected(false);
        updateSite.setOnClickListener(this);
        viewPager.disableScroll(false);
        SiteDetails details = (SiteDetails) getIntent().getExtras().getSerializable("data");


        if (getIntent().getBooleanExtra("isForSG", false)|getIntent().getBooleanExtra("isFromImp", false)){
            updateSite.show();
        }
        else {
            updateSite.hide();
        }
        if (details.getImageUrl().get(0) == null) {
            site_image.setVisibility(View.GONE);

        } else {
            site_image.setVisibility(View.VISIBLE);
            Glide.with(this).load(details.getImageUrl().get(0)).into(site_image);
        }






    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.updateSite:
                Bundle bundle = new Bundle();
                bundle.putSerializable("data",getIntent().getExtras().getSerializable("data"));
                goToNextScreen(UpdateSiteActivity.class,bundle);
                finish();
                break;
        }

    }

    public class ViewPagerAdapter extends FragmentStatePagerAdapter implements ViewPager.OnPageChangeListener {

        private Fragment[] childFragments;

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
            if (getIntent().getBooleanExtra("isForSG", false)) {
                childFragments = new Fragment[]{
                        new SiteDetailsFragment1(), //0
                        new SiteDetailsFragment2(), //1
                        new SiteDetailsFragments3() //2
                };
            } else {
                childFragments = new Fragment[]{
                        new SiteLandinngChild1(), //0
                        new SiteLandingChild2(), //1
                        new SiteLandingChild3() //2
                };
            }

        }


        @Override
        public Fragment getItem(int position) {

            childFragments[position].setArguments(getIntent().getExtras());

            return childFragments[position];
        }

        @Override
        public int getCount() {
            return childFragments.length; //3 items
        }

        @Override
        public void onPageScrolled(int i, float v, int i1) {
            hideKeyboard();

        }

        @Override
        public void onPageSelected(int i) {


        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            super.setPrimaryItem(container, position, object);
            int mCurrentPosition = -1;
            if (position != mCurrentPosition) {
                Fragment fragment = (Fragment) object;
                CustomPager pager = (CustomPager) container;
                if (fragment != null && fragment.getView() != null) {
                    mCurrentPosition = position;
                    pager.measureCurrentView(fragment.getView());
                }
            }
        }
    }

}
