package com.solarginni.user.UserSiteModule.PendingSiteRating;

import com.solarginni.DBModel.APIBody.SiteRatingModel;
import com.solarginni.network.RestClient;

public class PendingSitePresenter implements PendingSiteContract.Presenter,PendingSiteContract.OnInteraction {
     private PendingSiteContract.View view;
     private PendingSiteInteractor interactor;

    public PendingSitePresenter(PendingSiteContract.View view) {
        this.view = view;
        interactor= new PendingSiteInteractor(RestClient.getRetrofitBuilder());
    }

    @Override
    public void rateSite(String token, String siteID, SiteRatingModel model) {
        interactor.rateSite(token,siteID,model,this);
    }

    @Override
    public void onSiteRated() {
        view.onSiteRated();

    }

    @Override
    public void OnFailure(String msg) {
        view.OnFailure(msg);

    }
}
