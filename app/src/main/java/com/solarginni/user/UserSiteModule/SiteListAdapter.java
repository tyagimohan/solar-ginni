package com.solarginni.user.UserSiteModule;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.DBModel.SiteDetailsModel.SiteDetails;
import com.solarginni.R;

public class SiteListAdapter extends BaseRecyclerAdapter<SiteDetails, SiteListAdapter.Holder> {
    public SiteListAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public int getLayout(int viewType) {
        return R.layout.customer_site_item;
    }

    @Override
    public Holder getViewHolder(View view, int viewType) {
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        SpannableString siteID = new SpannableString(getItem(i).getSiteId());
        siteID.setSpan(new UnderlineSpan(), 0, getItem(i).getSiteId().length(), 0);
        holder.siteID.setText(getItem(i).getSiteId());

        holder.siteSize.setText(getItem(i).getSiteSize());

        GradientDrawable bgShape = (GradientDrawable) holder.lay1.getBackground();
        bgShape.setColor(getContext().getResources().getColor(R.color.colortheme));

        SpannableString content = new SpannableString(getItem(i).getInstalledBy());
        content.setSpan(new UnderlineSpan(), 0, getItem(i).getInstalledBy().length(), 0);
        holder.implementerName.setText(content);

        if (getItem(i).getSiteRated()) {
            holder.ratingImg.setBackground(getContext().getResources().getDrawable(R.drawable.ic_interface));
            holder.rating.setText("Rated");
        } else{
            holder.ratingImg.setBackground(getContext().getResources().getDrawable(R.drawable.ic_rate));
            holder.rating.setText("Rate");
            holder.lay3.setOnClickListener(view -> {
           getLongClickListener().onItemClick(getItem(i),i,holder.lay3);
            });
        }


    }


    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView siteID, implementerName,siteSize,rating;
        ImageView ratingImg;
        LinearLayout lay1,lay3;

        public Holder(@NonNull View itemView) {
            super(itemView);
            siteID= itemView.findViewById(R.id.siteId);
            implementerName= itemView.findViewById(R.id.implementerName);
            ratingImg= itemView.findViewById(R.id.ratingImg);
            rating= itemView.findViewById(R.id.rating);
            itemView.setOnClickListener(this);
            lay1= itemView.findViewById(R.id.lay1);
            lay3= itemView.findViewById(R.id.lay3);
            siteSize= itemView.findViewById(R.id.siteSize);
            implementerName.setOnClickListener(this);
                 }

        @Override
        public void onClick(View view) {
            if (view.getId()==R.id.rating){
                getLongClickListener().onItemClick(getItem(getAdapterPosition()),getAdapterPosition(),view);

            }
            else if (view.getId()==R.id.implementerName){
                getLongClickListener().onItemClick(getItem(getAdapterPosition()),getAdapterPosition(),view);
            }
            else {
                getListener().onClick(getItem(getAdapterPosition()),getAdapterPosition());
            }

        }


    }
}
