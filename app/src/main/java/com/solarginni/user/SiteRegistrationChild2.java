package com.solarginni.user;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.solarginni.Base.BaseFragment;
import com.solarginni.CustomizeView.CompanyListDialog;
import com.solarginni.DBModel.CompanyMakeModel;
import com.solarginni.R;
import com.solarginni.SiteLovsModule.SiteDetailModel.SiteLov;
import com.solarginni.Utility.Utils;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.solarginni.Utility.AppConstants.BO_MANAGER;
import static com.solarginni.Utility.AppConstants.COMPANYID;
import static com.solarginni.Utility.AppConstants.COMPANYNAME;
import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.SG_ADMIN;
import static com.solarginni.Utility.AppConstants.SITELOVS;

/**
 * Created by Mohan on 16/1/20.
 */

public class SiteRegistrationChild2 extends BaseFragment implements View.OnClickListener {

    private static final int REQUEST_FOR_PLACE = 2;
    public static EditText tvInstalledBy, tvInstalledOn, tvAMC, city, state, country, pincode;
    public static String implementerID;
    public static Double lat, lng;
    public static Switch addressToggle;
    public static EditText etLine1, addLine2;
    private final Calendar myCalendar = Calendar.getInstance();
    private List<CompanyMakeModel> installedByArray = new ArrayList<>();
    private SiteLov details;
    private boolean isDateSelecte = false;
    private LinearLayout addressLayout, lowerLayout;
    private final ToggleButton.OnCheckedChangeListener mToggleListener = (compoundButton, b
    ) -> {
        addressLayout.setVisibility(b ? View.GONE : View.VISIBLE);
        if (addressToggle.isChecked()) {
            hideKeyboard(getActivity());
        }
    };
    private Geocoder mGeocoder;

    @Override
    public int getLayout() {
        return R.layout.site_registration_part2;
    }

    @Override
    public void initViews(View view) {

        tvAMC = view.findViewById(R.id.amc);
        tvInstalledBy = view.findViewById(R.id.installedBy);
        tvInstalledOn = view.findViewById(R.id.installedOn);
        etLine1 = view.findViewById(R.id.etLine1);
        addLine2 = view.findViewById(R.id.line2);
        addressLayout = view.findViewById(R.id.extraLayout);
        lowerLayout = view.findViewById(R.id.lowerLayout);
        state = view.findViewById(R.id.state);
        city = view.findViewById(R.id.cityName);
        country = view.findViewById(R.id.country);
        pincode = view.findViewById(R.id.pincode);

        disableSuggestion(tvAMC);
        disableSuggestion(tvInstalledBy);
        disableSuggestion(tvInstalledOn);
        disableSuggestion(city);
        disableSuggestion(etLine1);
        disableSuggestion(addLine2);
        disableSuggestion(country);
        disableSuggestion(state);
        disableSuggestion(pincode);

        if (getArguments().getString("isFrom", "").equalsIgnoreCase("implementerFrag") | securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SG_ADMIN) | securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(BO_MANAGER)) {
            view.findViewById(R.id.on_offReminder).setVisibility(View.GONE);
            view.findViewById(R.id.siteAddresSwitch).setVisibility(View.GONE);
        } else {
            view.findViewById(R.id.on_offReminder).setVisibility(View.VISIBLE);
            view.findViewById(R.id.siteAddresSwitch).setVisibility(View.VISIBLE);


        }


        addressToggle = view.findViewById(R.id.reminderToggle);

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                hideKeyboard(getActivity());
                view.clearFocus();
                return false;
            }
        });


    }

    @Override
    public void setUp() {

        details = Utils.getObj(securePrefManager.getSharedValue(SITELOVS), SiteLov.class);
        addLine2.setOnClickListener(this);
        installedByArray.addAll(details.getSiteDetails().getImplementer());

        if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SG_ADMIN) | securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(BO_MANAGER)) {
            tvInstalledBy.setOnClickListener(this);
            addressToggle.setChecked(false);
            addressToggle.setVisibility(View.GONE);
            addressLayout.setVisibility(View.VISIBLE);
        } else {
            if (getArguments().getString("isFrom", "").equalsIgnoreCase("implementerFrag")) {
                tvInstalledBy.setText(securePrefManager.getSharedValue(COMPANYNAME));
                implementerID = securePrefManager.getSharedValue(COMPANYID);
                addressToggle.setChecked(false);
                addressToggle.setVisibility(View.GONE);
                addressLayout.setVisibility(View.VISIBLE);
            } else {
                addressToggle.setChecked(true);
                addressToggle.setVisibility(View.VISIBLE);
                addressToggle.setOnCheckedChangeListener(mToggleListener);
                addressLayout.setVisibility(View.GONE);
                tvInstalledBy.setOnClickListener(this);
            }
        }


        tvInstalledOn.setOnClickListener(this);
        tvAMC.setOnClickListener(this);
//        tvInstalledBy.addTextChangedListener(watcher);
//        tvInstalledOn.addTextChangedListener(watcher);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.installedOn:
                openDatePicker(tvInstalledOn, false);
                break;
            case R.id.amc:
                if (isDateSelecte)
                    openDatePicker(tvAMC, true);
                else
                    showToast("Please select Installed On Date first");

                break;
            case R.id.installedBy:

                CompanyListDialog.show(getActivity(), installedByArray, item -> {
                    tvInstalledBy.setText(item.getCompanyName());
                    implementerID = item.getCompanyId();

                });
                break;
            case R.id.line2:
                openPlacePicker();
                break;

        }

    }

    private void openPlacePicker() {

        if (!Places.isInitialized()) {
            Places.initialize(getContext(), getString(R.string.google_api));
        }
        List<Place.Field> fields = Arrays.asList(Place.Field.ADDRESS_COMPONENTS, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.ADDRESS_COMPONENTS, Place.Field.LAT_LNG);

        // Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).build(Objects.requireNonNull(getActivity()));
        startActivityForResult(intent, REQUEST_FOR_PLACE);
    }

    private void openDatePicker(TextView textView, boolean isSetminDate) {

        int year = myCalendar.get(Calendar.YEAR);
        int month = myCalendar.get(Calendar.MONTH);
        int day = myCalendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), (datePicker, year1, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year1);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            String myFormat = "dd/MM/yyyy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
            textView.setText(sdf.format(myCalendar.getTime()));
            if (!isSetminDate) {
                isDateSelecte = true;
            }

        }, year, month,
                day);


        if (!isSetminDate)
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        else {
            datePickerDialog.getDatePicker().setMinDate(convertTime(tvInstalledOn.getText().toString()));
        }

        datePickerDialog.show();

    }


    public long convertTime(String time) {
        long time1 = (long) 0.0;
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");


        try {
            Date date = df.parse(time);
            time1 = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return time1;

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_FOR_PLACE:
                if (resultCode == RESULT_OK) {
                    hideKeyboard(getActivity());
                    Place place = Autocomplete.getPlaceFromIntent(data);
                    addLine2.setText(place.getName());
                    LatLng latLng = place.getLatLng();
                    lat = latLng.latitude;
                    lng = latLng.longitude;
                    try {
                        getCityNameByCoordinates(latLng.latitude, latLng.longitude);
                    } catch (IOException e) {
                        e.printStackTrace();
                        visileLayoutForEditText();
                    }


                    //  onParamSelected();
                } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                    Status status = Autocomplete.getStatusFromIntent(data);


                } else if (resultCode == RESULT_CANCELED) {

                }
                break;

        }
    }

    private void getCityNameByCoordinates(double lat, double lon) throws IOException {
        mGeocoder = new Geocoder(getContext(), Locale.getDefault());
        List<Address> addresses = mGeocoder.getFromLocation(lat, lon, 1);
        if (addresses != null && addresses.size() > 0) {
            lowerLayout.setVisibility(View.VISIBLE);
            state.setText(addresses.get(0).getAdminArea());
            city.setText(addresses.get(0).getSubAdminArea());
            country.setText(addresses.get(0).getCountryName());
            pincode.setText(addresses.get(0).getPostalCode());

        }

    }

    public void visileLayoutForEditText() {
        lowerLayout.setVisibility(View.VISIBLE);
        state.setFocusableInTouchMode(true);
        country.setText("India");
    }


}
