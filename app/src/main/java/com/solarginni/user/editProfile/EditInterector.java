package com.solarginni.user.editProfile;

import android.content.Context;
import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.APIBody.ImageType;
import com.solarginni.DBModel.APIBody.UserRegisteration;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;
import com.solarginni.network.MultipartParams;
import retrofit2.Retrofit;

import java.io.File;

public class EditInterector implements EditContract.Intrector {

    private Retrofit retrofit;
    private ApiInterface apiInterface;

    public EditInterector(Retrofit retrofit) {
        this.retrofit = retrofit;
        apiInterface = retrofit.create(ApiInterface.class);
    }

    @Override
    public void updateProfile(Context context, UserRegisteration model, String token, File file, EditContract.Onintrection listener) {

        if (file == null) {
            apiInterface.updateUser(model, token).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
                @Override
                public void onSuccess(CommomResponse response) {
                    listener.onUpdateProfile("Profile details successfully updated");


                }

                @Override
                public void onError(ApiError error) {
                    listener.onFailure(error.getMessage());

                }

                @Override
                public void onFailure(Throwable throwable) {
                    listener.onFailure(throwable.getMessage());

                }
            });

        } else {
            MultipartParams multipartParams = new MultipartParams.Builder()
                    .addFile("upload", file).build();

            apiInterface.uploadImage(multipartParams.getMap(), ImageType.USER_TYPE, token).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
                @Override
                public void onSuccess(CommomResponse commomResponse) {
                    model.profileImage = commomResponse.toResponseModel(String.class);
                    apiInterface.updateUser(model, token).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
                        @Override
                        public void onSuccess(CommomResponse commomResponse) {
                            listener.onUpdateProfile("Profile details successfully updated");

                        }

                        @Override
                        public void onError(ApiError error) {
                            listener.onFailure(error.getMessage());

                        }

                        @Override
                        public void onFailure(Throwable throwable) {

                            listener.onFailure(throwable.getMessage());
                        }
                    });
                }

                @Override
                public void onError(ApiError error) {
                    listener.onFailure(error.getMessage());
                }

                @Override
                public void onFailure(Throwable throwable) {
                    listener.onFailure(throwable.getMessage());
                }
            });


        }


    }
}
