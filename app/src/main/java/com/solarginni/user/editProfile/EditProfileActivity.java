package com.solarginni.user.editProfile;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;

import androidx.appcompat.app.AlertDialog;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.kbeanie.multipicker.api.CacheLocation;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.solarginni.Base.BaseActivity;
import com.solarginni.CommonModule.MainActivity;
import com.solarginni.DBModel.APIBody.UserRegisteration;
import com.solarginni.DBModel.UserRoleModel;
import com.solarginni.R;
import com.solarginni.Utility.Permissions;
import com.solarginni.Utility.Utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import static com.solarginni.Utility.AppConstants.ACCOUNT_MODEL;
import static com.solarginni.Utility.AppConstants.TOKEN;

public class EditProfileActivity extends BaseActivity implements ImagePickerCallback, View.OnClickListener, EditContract.View {

    private static final int REQUEST_CODE_FOR_PERMISSION = 2;
    private static final int REQUEST_FOR_PLACE = 6;
    private TextView tvCity, tvState, tvCountry, tvMobileNumber;
    private Button submitBtn;
    private ImageView profileImage;
    private ImagePicker mImagePicker;
    private CameraImagePicker mCameraImagePicker;
    private File mPhotoFile;
    private String lat, lng;
    private Geocoder mGeocoder;
    private TextView etLine1, addLine2, etPincode;
    private EditPresenter presenter;
    private String url;
    private EditText tvEmail;
    private TextView tvName, tvuserId;

    @Override
    public int getLayout() {
        return R.layout.edit_profile;
    }

    @Override
    public void initViews() {
        profileImage = findViewById(R.id.profileImage);
        submitBtn = findViewById(R.id.confirmBtn);
        tvMobileNumber = findViewById(R.id.mobileNumber);
        addLine2 = findViewById(R.id.line2);
        tvCity = findViewById(R.id.tvCity);
        tvName = findViewById(R.id.tvName);
        tvEmail = findViewById(R.id.etEmail);
        tvuserId = findViewById(R.id.tvuserId);
        tvState = findViewById(R.id.tvState);
        tvCountry = findViewById(R.id.tvCountry);
        etLine1 = findViewById(R.id.etLine1);
        etPincode = findViewById(R.id.etPincode);
        mGeocoder = new Geocoder(this, Locale.getDefault());

        disableSuggestion(addLine2);
        disableSuggestion(tvCity);
        disableSuggestion(etLine1);
        disableSuggestion(tvName);
        disableSuggestion(etPincode);
        disableSuggestion(tvCountry);
        disableSuggestion(tvState);
        disableSuggestion(tvMobileNumber);

        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });
        Places.initialize(getApplicationContext(), getString(R.string.google_api));



        mImagePicker = new ImagePicker(this);
        mImagePicker.setCacheLocation(CacheLocation.INTERNAL_APP_DIR);
        mImagePicker.setImagePickerCallback(this);
        mImagePicker.shouldGenerateThumbnails(false);
        mImagePicker.shouldGenerateMetadata(false);

        mCameraImagePicker = new CameraImagePicker(this);
        mCameraImagePicker.setCacheLocation(CacheLocation.INTERNAL_APP_DIR);
        mCameraImagePicker.setImagePickerCallback(this);
        mCameraImagePicker.shouldGenerateThumbnails(false);
        mCameraImagePicker.shouldGenerateMetadata(false);
        setupUI(findViewById(R.id.parent));

        presenter = new EditPresenter(this);

    }

    @Override
    public void setUp() {
        UserRoleModel.MyAccount account = Utils.getObj(securePrefManager.getSharedValue(ACCOUNT_MODEL), UserRoleModel.MyAccount.class);

        tvName.setText(account.getName());
        if (!account.getEmail().trim().isEmpty()) {
            tvEmail.setText(account.getEmail());

        }
        tvuserId.setText(account.getUserId());
        tvMobileNumber.setText(account.getPhone());
        tvCity.setText(account.getAddress().getCity());


        tvCountry.setText(account.getAddress().getCountry());
        tvState.setText(account.getAddress().getState());
        etPincode.setText(account.getAddress().getPinCode());
        etLine1.setText(account.getAddress().getAddressLine1());
        addLine2.setText(account.getAddress().getAddressLine2());
        lat = String.valueOf(account.getAddress().getLatitude());
        lng = String.valueOf(account.getAddress().getLongitude());
        profileImage.setOnClickListener(this);



        try {
            if (!account.getImages().isEmpty())
                Glide.with(this).load(account.getImages()).into(profileImage);
        } catch (Exception e) {
            e.printStackTrace();

        }



        submitBtn.setOnClickListener(this);
        addLine2.setOnClickListener(this);


    }

    @Override
    public void onImagesChosen(List<ChosenImage> list) {
        mPhotoFile = new File(list.get(0).getOriginalPath());
        Bitmap bitmap = Utils.compressImage(mPhotoFile.getPath());
        profileImage.setImageBitmap(bitmap);


        OutputStream os = null;
        try {
            os = new BufferedOutputStream(new FileOutputStream(mPhotoFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
        try {
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static String getFileSizeKiloBytes(File file) {
        return (double) file.length() / 1024 + " kb";
    }

    @Override
    public void onError(String s) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_FOR_PLACE:
                if (resultCode == RESULT_OK) {
                    hideKeyboard(this);
                    Place place = Autocomplete.getPlaceFromIntent(data);
                    addLine2.setText(place.getName());
                    LatLng latLng = place.getLatLng();
                    lat = String.valueOf(latLng.latitude);
                    lng = String.valueOf(latLng.longitude);
                    try {
                        getCityNameByCoordinates(latLng.latitude, latLng.longitude);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    //  onParamSelected();
                } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                    Status status = Autocomplete.getStatusFromIntent(data);


                } else if (resultCode == RESULT_CANCELED) {

                }
                break;

            case Picker.PICK_IMAGE_DEVICE:
                if (resultCode == RESULT_OK) {
                    mImagePicker.submit(data);
                } else {

                }
                break;

            case Picker.PICK_IMAGE_CAMERA:
                if (resultCode == RESULT_OK) {
                    try {
                        mCameraImagePicker.submit(data);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {

                }
                break;


        }

    }

    private void submitForm() {


        if (!validateAddress1()) {
            return;
        }
        if (!validateAddress2()) {
            return;
        }
        if (!validateCity()) {
            return;
        }
        if (!pincode()) {
            return;
        }
        /*Hit Registration API*/
        if (Utils.hasNetwork(this)) {
            showProgress("Please wait");
            UserRegisteration user = new UserRegisteration(tvName.getText().toString().trim(),etLine1.getText().toString(), addLine2.getText().toString(), tvCity.getText().toString(), lat, lng, etPincode.getText().toString(), tvState.getText().toString(),tvEmail.getText().toString());
            presenter.updateProfile(this, user, securePrefManager.getSharedValue(TOKEN), mPhotoFile);

        } else {
            showToast(getResources().getString(R.string.internet_error));
        }


    }


    private boolean validateAddress1() {
        if (etLine1.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.err_msg_add));
            requestFocus(etLine1);
            return false;
        }

        return true;
    }

    private boolean validateAddress2() {
        if (addLine2.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.err_msg_add));
            requestFocus(addLine2);
            return false;
        }
        return true;
    }

    private boolean validateCity() {

        if (tvCity.getText().toString().trim().isEmpty()||tvCity.getText().toString().length()<3) {
            showToast("Please Add City");
            return false;
        }
        return true;
    }

    private boolean pincode() {
        if (etPincode.getText().toString().trim().isEmpty() ||etPincode.getText().length()<5) {
            showToast(getString(R.string.pincode_err));
            requestFocus(etPincode);
            return false;
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void disableAutofill() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            getWindow().getDecorView().setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS);
    }

    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideKeyboard(EditProfileActivity.this);
                   v.clearFocus();
                    return false;
                }
            });
        }
    }


    private void getCityNameByCoordinates(double lat, double lon) throws IOException {

        List<Address> addresses = mGeocoder.getFromLocation(lat, lon, 1);
        if (addresses != null && addresses.size() > 0) {
            tvState.setText(addresses.get(0).getAdminArea());
            tvCity.setText(addresses.get(0).getSubAdminArea());
            tvCountry.setText(addresses.get(0).getCountryName());
            etPincode.setText(addresses.get(0).getPostalCode());

        }

    }




    private void getImagePicker() {
        CharSequence[] options = getResources().getStringArray(R.array.image_picker_options);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(options, (dialog, which) -> {
            switch (which) {
                case 0:
                    getImageFromCamera();
                    break;
                case 1:
                    getImageFromGallery();
                    break;
                default:
                    break;
            }
        });
        builder.show();
    }


    private void getImageFromGallery() {
        mImagePicker.pickImage();

    }


    private void getImageFromCamera() {

        mCameraImagePicker.pickImage();

    }

    private void setProfileImage(final File file) {
        if (file == null) {
            return;
        }

        Glide.with(this).load(file).into(profileImage);
    }

    private boolean checkPermissionAPI23() {
        boolean isPermissionRequired = false;
        List<String> permissionArray = new ArrayList<>();

        if (Permissions.getInstance().hasCameraPermission(this).permissions.size() > 0) {
            permissionArray.add(Manifest.permission.CAMERA);
            isPermissionRequired = true;
        }

        if (Permissions.getInstance().checkReadWritePermissions(this).permissions.size() > 0) {
            isPermissionRequired = true;
            permissionArray.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            permissionArray.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (isPermissionRequired) {
            Permissions.getInstance().addPermission(permissionArray).askForPermissions(this, REQUEST_CODE_FOR_PERMISSION);
            return false;
        }
        return true;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.editImg:
            case R.id.profileImage:
                if (checkPermissionAPI23())
                    getImagePicker();
                break;

            case R.id.confirmBtn:

                submitForm();


                break;
            case R.id.line2:
              //  goToNextScreen(PlaceAutoCompleteActivity.class,null);

                openPlacePicker();
                break;
        }

    }

    private void openPlacePicker() {

        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), getString(R.string.google_api));
        }
        List<Place.Field> fields = Arrays.asList(Place.Field.ADDRESS_COMPONENTS, Place.Field.NAME, Place.Field.LAT_LNG);

        // Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).build(Objects.requireNonNull(this));
        startActivityForResult(intent, REQUEST_FOR_PLACE);
    }

    @Override
    public void onUpdateProfile(String msg) {
        hideProgress();
        showToast(msg);
        setResult(RESULT_OK);
        finish();

    }

    @Override
    public void onFailure(String msg) {
        hideProgress();
        if (msg.equalsIgnoreCase("invalid token")) {
            securePrefManager.clearAppsAllPrefs();
            startActivity(new Intent(EditProfileActivity.this, MainActivity.class));
            finish();
        } else
            showToast(msg);
    }
}
