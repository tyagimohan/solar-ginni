package com.solarginni.user.editProfile;

import android.content.Context;

import com.solarginni.DBModel.APIBody.UserRegisteration;

import java.io.File;

public interface EditContract {


    interface View {
        void onUpdateProfile(String msg);
        void onFailure(String msg);
    }

    interface Presenter{
        void updateProfile(Context context, UserRegisteration model, String token, File file);
    }

    interface Intrector{
        void updateProfile(Context context,UserRegisteration model,String token,File file,EditContract.Onintrection listener);


    }

    interface Onintrection{

        void onUpdateProfile(String msg);
        void onFailure(String msg);
    }
}
