package com.solarginni.user.editProfile;

import android.content.Context;

import com.solarginni.DBModel.APIBody.UserRegisteration;
import com.solarginni.network.RestClient;

import java.io.File;

import retrofit2.Retrofit;

public class EditPresenter implements EditContract.Presenter,EditContract.Onintrection {

    private EditContract.View view;
    private EditInterector interector;


    public EditPresenter(EditContract.View view) {
        this.view = view;
        interector= new EditInterector(RestClient.getRetrofitBuilder());
    }

    @Override
    public void updateProfile(Context context, UserRegisteration model, String token, File file) {
         interector.updateProfile(context,model,token,file,this);
    }

    @Override
    public void onUpdateProfile(String msg) {
        view.onUpdateProfile(msg);

    }

    @Override
    public void onFailure(String msg) {
        view.onFailure(msg);

    }
}
