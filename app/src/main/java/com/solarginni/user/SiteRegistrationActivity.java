package com.solarginni.user;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import android.view.*;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kbeanie.multipicker.api.CacheLocation;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.solarginni.Base.BaseActivity;
import com.solarginni.CommonModule.MainActivity;
import com.solarginni.CustomizeView.CustomPager;
import com.solarginni.CustomizeView.CustomRatingBar;
import com.solarginni.DBModel.APIBody.SiteRatingModel;
import com.solarginni.DBModel.APIBody.SiteRegistrationModel;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.GetDetails.Details;
import com.solarginni.GetDetails.ImplementerDetails;
import com.solarginni.R;
import com.solarginni.SiteLovsModule.SiteDetailModel.Address;
import com.solarginni.SiteLovsModule.SiteDetailModel.SiteLov;
import com.solarginni.SiteRegistrationModule.SiteContract;
import com.solarginni.SiteRegistrationModule.SitePresenter;
import com.solarginni.Utility.AppConstants;
import com.solarginni.Utility.Permissions;
import com.solarginni.Utility.Utils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static com.solarginni.Utility.AppConstants.*;

/**
 * Created by Mohan on 16/1/20.
 */

public class SiteRegistrationActivity extends BaseActivity implements View.OnClickListener, ImagePickerCallback, SiteContract.View, SiteRegistrationChild1.SolutionListener {

    private static final int REQUEST_CODE_FOR_PERMISSION = 2;
    public  CustomPager viewPager;
    private SitePresenter presenter;
    private TextView addImage;
    private TabLayout tabIndicator;
    private ImagePicker mImagePicker;
    private CameraImagePicker mCameraImagePicker;
    private ViewPagerAdapter adapter;
    private ImageView siteImage;
    private File mPhotoFile;
    private Button submitBtn;
    private ImageView preBtn, nextBtn;

    @Override
    public int getLayout() {
        return R.layout.site_registration;
    }

    @Override
    public void initViews() {
        viewPager = findViewById(R.id.viewPager);
        tabIndicator = findViewById(R.id.tabIndicator);
        addImage = findViewById(R.id.addImage);
        submitBtn = findViewById(R.id.submitBtn);
        preBtn = findViewById(R.id.prevBtn);
        nextBtn = findViewById(R.id.nextBtn);
        adapter = new ViewPagerAdapter(getSupportFragmentManager());

        mImagePicker = new ImagePicker(this);
        mImagePicker.setCacheLocation(CacheLocation.INTERNAL_APP_DIR);
        mImagePicker.setImagePickerCallback(this);
        mImagePicker.shouldGenerateThumbnails(false);
        mImagePicker.shouldGenerateMetadata(false);
        siteImage = findViewById(R.id.site_image);
        mCameraImagePicker = new CameraImagePicker(this);
        mCameraImagePicker.setCacheLocation(CacheLocation.INTERNAL_APP_DIR);
        mCameraImagePicker.setImagePickerCallback(this);
        mCameraImagePicker.shouldGenerateThumbnails(false);
        mCameraImagePicker.shouldGenerateMetadata(false);

        presenter = new SitePresenter(this);
        preBtn.setOnClickListener(this);
        nextBtn.setOnClickListener(this);
        tabIndicator.clearOnTabSelectedListeners();
        tabIndicator.setEnabled(false);
        viewPager.disableScroll(false);

        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("");
        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });


        setupUI(viewPager);


        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

    }


    @Override
    public void setUp() {
        if (Utils.hasNetwork(this)) {
            showProgress("Please Wait...");
            presenter.getSiteLov(securePrefManager.getSharedValue(TOKEN));
        } else {
            showToast("Please Check Internet");
            finish();
        }

        addImage.setOnClickListener(this);
        submitBtn.setOnClickListener(this);
        submitBtn.setVisibility(View.GONE);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.addImage:
                if (checkPermissionAPI23()) {
                    getImagePicker();
                }
                break;
            case R.id.submitBtn:
                hideKeyboard(this);
                SiteRegistrationChild3.prepareInverterLis();
                String mobileNumber = "";
                SiteRegistrationModel model = new SiteRegistrationModel();
                mobileNumber = SiteRegistrationChild1.tvMobileNumber.getText().toString();
                if (!SiteRegistrationChild1.tvMobileNumber.getText().toString().startsWith("+91"))
                    mobileNumber = "+91" + SiteRegistrationChild1.tvMobileNumber.getText().toString();

                 if (SiteRegistrationChild3.isListPrepared) {
                    model.setInverterModelList(SiteRegistrationChild3.lists);
                } else {
                    showToast("Please Fill * marked Field");
                    return;
                }

                if ( SiteRegistrationChild1.tvSiteType.getText().length() == 0 || SiteRegistrationChild1.tvSolution.getText().length() == 0 || SiteRegistrationChild1.etSiteSize.getText().length() == 0 || SiteRegistrationChild1.tvMobileNumber.getText().length() == 0) {
                    viewPager.setCurrentItem(0, true);
                    showToast("Please Fill * marked Field");
                    return;
                } else if (SiteRegistrationChild2.tvInstalledBy.getText().length() == 0 || SiteRegistrationChild2.tvInstalledOn.getText().length() == 0) {
                    viewPager.setCurrentItem(1, true);
                    showToast("Please Fill * marked Field");
                    return;

                } else if (!SiteRegistrationChild2.addressToggle.isChecked() && (SiteRegistrationChild2.etLine1.getText().length() == 0 || SiteRegistrationChild2.pincode.getText().length() == 0 || SiteRegistrationChild2.city.getText().length() == 0)) {
                    showToast("Please Fill * marked Field");
                    viewPager.setCurrentItem(1, true);
                    return;
                } else if (mobileNumber.length() < 13) {
                    viewPager.setCurrentItem(0, true);
                    showToast("Please enter valid mobile number");
                    return;

                } else if (SiteRegistrationChild3.tvPanelMake.getText().length() == 0) {
                    showToast("Please Fill * marked Field");
                    viewPager.setCurrentItem(2, true);
                    return;
                } else if (SiteRegistrationChild1.tvSolution.getText().toString().equalsIgnoreCase("Hybrid") && (SiteRegistrationChild3.tvBatteryMake.getText().length() == 0)) {
                    viewPager.setCurrentItem(2, true);
                    showToast("Please Fill * marked Field");
                    return;
                } else {
                    if (Utils.hasNetwork(this)) {

                        showProgress("Please wait");


                        model.setAmctill(SiteRegistrationChild2.tvAMC.getText().toString());
                        model.setConnectionId(SiteRegistrationChild1.etConnectionId.getText().toString().trim().length()==0?null:SiteRegistrationChild1.etConnectionId.getText().toString().trim());
                        model.setInstalledBy(SiteRegistrationChild2.implementerID);
                        model.setInstalledOn(SiteRegistrationChild2.tvInstalledOn.getText().toString());

                        model.setPanelSize(SiteRegistrationChild3.etPanelSize.getText().toString());
                        model.setPanelType(SiteRegistrationChild3.tvPanelType.getText().toString());
                        model.setPhase(SiteRegistrationChild1.tvPahsel.getText().toString());
                        model.setPhone(mobileNumber);

                        model.setSiteSize(SiteRegistrationChild1.etSiteSize.getText().toString());
                        model.setBatteryCompanyId(SiteRegistrationChild3.batteryId);
                        model.setBatterySerial(SiteRegistrationChild3.batterySerial.getText().toString());
                        model.setBatterySize(SiteRegistrationChild3.batterySize.getText().toString());
                        model.setSolution(SiteRegistrationChild1.tvSolution.getText().toString());
                        model.setPanelMake(SiteRegistrationChild3.panelMakeId);
                        model.setSiteType(SiteRegistrationChild1.tvSiteType.getText().toString());


                        if (SiteRegistrationChild2.addressToggle.isChecked()) {
                            model.setAddresSame(true);
                        } else {
                            Address address = new Address();
                            address.setAddressLine1(SiteRegistrationChild2.etLine1.getText().toString());
                            address.setAddressLine2(SiteRegistrationChild2.addLine2.getText().toString());
                            address.setCity(SiteRegistrationChild2.city.getText().toString());
                            address.setCountry(SiteRegistrationChild2.country.getText().toString());
                            address.setState(SiteRegistrationChild2.state.getText().toString());
                            address.setPinCode(SiteRegistrationChild2.pincode.getText().toString());
                            address.setLatitude(SiteRegistrationChild2.lat);
                            address.setLongitude(SiteRegistrationChild2.lng);
                            model.setAddresSame(false);
                            model.setAddress(address);
                        }
                        presenter.registerSite(this, model, securePrefManager.getSharedValue(TOKEN), mPhotoFile);

                    } else {
                        showToast(getString(R.string.internet_error));
                    }
                }


                break;

            case R.id.prevBtn:

                viewPager.setCurrentItem(getItem(-1), true);

                break;
            case R.id.nextBtn:


                viewPager.setCurrentItem(getItem(+1), true);


                break;

        }

    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    @Override
    public void onImagesChosen(List<ChosenImage> list) {
        siteImage.setVisibility(View.VISIBLE);
        mPhotoFile = new File(list.get(0).getOriginalPath());
        Bitmap bitmap = Utils.compressImage(mPhotoFile.getPath());
        siteImage.setImageBitmap(bitmap);


        OutputStream os = null;
        try {
            os = new BufferedOutputStream(new FileOutputStream(mPhotoFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
        try {
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void setProfileImage(final File file) {
        if (file == null) {
            return;
        }

        Glide.with(this).load(file).into(siteImage);
    }

    @Override
    public void onError(String s) {
        showToast(s);

    }

    @Override
    public void OnRegisterSite(CommomResponse response) {
        hideProgress();
        showToast("Site Successfully Registered");
        String role = securePrefManager.getSharedValue(ROLE);
        switch (role) {
            case CUSTOMER_ROLE_ID:
                Details details = response.toResponseModel(Details.class);

                securePrefManager.storeSharedValue(AppConstants.DETAILS, Utils.toString(details));
                securePrefManager.setScreenValue("LandingPage");
                showRatingDailog(this, response);


                break;

            case IMPLEMENTER_ROLE_ID:
            case SUPER_IMPLEMENTER_ROLE_ID:
                ImplementerDetails implementerDetails = response.toResponseModel(ImplementerDetails.class);
                securePrefManager.storeSharedValue(IMPLEMENTER_DETAILS, Utils.toString(implementerDetails));
                setResult(RESULT_OK);
                finish();

                break;

            case SG_ADMIN:
            case BO_MANAGER:
                setResult(RESULT_OK);
                finish();
                break;
        }
    }

    @Override
    public void onSiteUpdated(CommomResponse response) {

    }

    @Override
    public void onSiteRated() {
        showToast("Site Rated Successfully");


    }

    @Override
    public void OnFailure(String msg) {
        hideProgress();
        try {
            if (msg.equalsIgnoreCase("invalid token")) {
                securePrefManager.clearAppsAllPrefs();
                startActivity(new Intent(SiteRegistrationActivity.this, MainActivity.class));
            } else {

                showToast(msg);

            }
        } catch (Exception e) {
            e.printStackTrace();
            showToast(msg);
        } finally {

        }


    }

    @Override
    public void onGetSiteLovs(CommomResponse siteLov) {
        hideProgress();
        SiteLov details = siteLov.toResponseModel(SiteLov.class);
        securePrefManager.storeSharedValue(AppConstants.SITELOVS, Utils.toString(details));
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(adapter);
        viewPager.setOffscreenPageLimit(2);
        tabIndicator.setupWithViewPager(viewPager);
        tabIndicator.setSelected(false);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Picker.PICK_IMAGE_DEVICE:
                if (resultCode == RESULT_OK) {
                    mImagePicker.submit(data);
                } else {

                }
                break;

            case Picker.PICK_IMAGE_CAMERA:
                if (resultCode == RESULT_OK) {
                    mCameraImagePicker.submit(data);
                } else {

                }
                break;
        }

    }

    private boolean checkPermissionAPI23() {
        boolean isPermissionRequired = false;
        List<String> permissionArray = new ArrayList<>();

        if (Permissions.getInstance().hasCameraPermission(this).permissions.size() > 0) {
            permissionArray.add(Manifest.permission.CAMERA);
            isPermissionRequired = true;
        }

        if (Permissions.getInstance().checkReadWritePermissions(this).permissions.size() > 0) {
            isPermissionRequired = true;
            permissionArray.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            permissionArray.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (isPermissionRequired) {
            Permissions.getInstance().addPermission(permissionArray).askForPermissions(this, REQUEST_CODE_FOR_PERMISSION);
            return false;
        }
        return true;
    }

    private void getImagePicker() {
        CharSequence[] options = getResources().getStringArray(R.array.image_picker_options);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(options, (dialog, which) -> {
            switch (which) {
                case 0:
                    getImageFromCamera();
                    break;
                case 1:
                    getImageFromGallery();
                    break;
                default:
                    break;
            }
        });
        builder.show();
    }

    private void getImageFromGallery() {
        mImagePicker.pickImage();

    }

    private void getImageFromCamera() {

        mCameraImagePicker.pickImage();

    }

    private void showRatingDailog(Context context, CommomResponse response) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.service_rating, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);
        AlertDialog alertDialog = dialogBuilder.create();
        CustomRatingBar timeLine = dialogView.findViewById(R.id.rating);
        CustomRatingBar qualityRating = dialogView.findViewById(R.id.qualityRating);
        CustomRatingBar pricingRating = dialogView.findViewById(R.id.pricingRating);
        CustomRatingBar teamRating = dialogView.findViewById(R.id.teamRating);
        Button submitBtn = dialogView.findViewById(R.id.confirmBtn);

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((int) timeLine.getScore() > 0 || (int) pricingRating.getScore() > 0 || (int) qualityRating.getScore() > 0 || (int) teamRating.getScore() > 0) {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("isForRating", true);
                    SiteRatingModel model = new SiteRatingModel();
                    model.setTime((int) timeLine.getScore());
                    model.setPrice((int) pricingRating.getScore());
                    model.setQuality((int) qualityRating.getScore());
                    model.setTeam((int) teamRating.getScore());
                    presenter.rateSite(securePrefManager.getSharedValue(TOKEN), securePrefManager.getSharedValue(SITEID), model);
                    if (getIntent().getExtras().getBoolean("isFromLanding", false)) {
                        setResult(RESULT_OK);
                    } else {
                        bundle = getIntent().getExtras();
                        bundle.putString("for", "rating");
                        goToNextScreen(UserHomeActivity.class, bundle);
                    }
                    finish();
                    alertDialog.dismiss();
                } else {
                    showToast("Please Rate Service");
                }


            }
        });


        alertDialog.show();

    }

    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    ///  view.requestFocus();
                    hideKeyboard();
                    view.clearFocus();
                    return false;
                }
            });
        }
    }

    @Override
    public void selectHybrid(boolean isHybrid) {
        if (isHybrid) {
            SiteRegistrationChild3.batteryLayout.setVisibility(View.VISIBLE);
        } else {
            SiteRegistrationChild3.batteryLayout.setVisibility(View.GONE);

        }

    }

    public class ViewPagerAdapter extends FragmentStatePagerAdapter implements ViewPager.OnPageChangeListener {

        private Fragment[] childFragments;

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
            childFragments = new Fragment[]{
                    new SiteRegistrationChild1(), //0
                    new SiteRegistrationChild2(), //1
                    new SiteRegistrationChild3() //2
            };
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            super.setPrimaryItem(container, position, object);
            int mCurrentPosition = -1;
            if (position != mCurrentPosition) {
                Fragment fragment = (Fragment) object;
                CustomPager pager = (CustomPager) container;
                if (fragment != null && fragment.getView() != null) {
                    mCurrentPosition = position;
                    pager.measureCurrentView(fragment.getView());
                }
            }
        }


        @Override
        public Fragment getItem(int position) {

            childFragments[position].setArguments(getIntent().getExtras());

            return childFragments[position];
        }

        @Override
        public int getCount() {
            return childFragments.length; //3 items
        }

        @Override
        public void onPageScrolled(int i, float v, int i1) {
            hideKeyboard(SiteRegistrationActivity.this);

        }

        @Override
        public void onPageSelected(int i) {

            if (i == 2) {
                submitBtn.setVisibility(View.VISIBLE);
                nextBtn.setVisibility(View.GONE);
            } else {
                nextBtn.setVisibility(View.VISIBLE);
                submitBtn.setVisibility(View.GONE);
            }
            if (i == 0) {
                preBtn.setVisibility(View.GONE);
            } else {
                preBtn.setVisibility(View.VISIBLE);
            }

        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    }
}
