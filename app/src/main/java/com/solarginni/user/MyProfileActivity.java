package com.solarginni.user;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.solarginni.Base.BaseActivity;
import com.solarginni.CommonModule.MainActivity;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.ProspectLandingModel;
import com.solarginni.DBModel.UserRoleModel;
import com.solarginni.GetDetails.Details;
import com.solarginni.ProspectUser.ProspectHomeActivity;
import com.solarginni.R;
import com.solarginni.SGUser.SGModule.SGHomeActivity;
import com.solarginni.SGUser.SGModule.SGLandingModel;
import com.solarginni.SiteLovsModule.SiteDetailModel.SiteLov;
import com.solarginni.SiteLovsModule.UserContract;
import com.solarginni.SiteLovsModule.UserDetailsPresenter;
import com.solarginni.Utility.AppConstants;
import com.solarginni.Utility.Utils;
import com.solarginni.user.editProfile.EditProfileActivity;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.solarginni.Utility.AppConstants.ACCOUNT_MODEL;
import static com.solarginni.Utility.AppConstants.BO_MANAGER;
import static com.solarginni.Utility.AppConstants.NOTIFICATION_COUNT;
import static com.solarginni.Utility.AppConstants.PROFILE_IMAGE;
import static com.solarginni.Utility.AppConstants.PROSPECT_ROLE_ID;
import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.SG_ADMIN;
import static com.solarginni.Utility.AppConstants.TOKEN;

public class MyProfileActivity extends BaseActivity implements View.OnClickListener, UserContract.View {
    private TextView tvName, tvEmail, tvMobile, tvAddress, tvUserID, regiterSite, tvCont;
    private ImageView profileImage;
    private LinearLayout tvEmailLabel;
    private UserDetailsPresenter presenter;
    private TextView goForQuatation;
    private ImageView editProfile;

    @Override
    public int getLayout() {
        return R.layout.my_profile;
    }

    @Override
    public void initViews() {
        tvAddress = findViewById(R.id.tvAddres);
        tvName = findViewById(R.id.tvName);
        profileImage = findViewById(R.id.profileImage);
        tvEmail = findViewById(R.id.tvEmail);
        tvUserID = findViewById(R.id.tvuserId);
        tvMobile = findViewById(R.id.mobileNumber);
        editProfile = findViewById(R.id.editProfile);
        tvCont = findViewById(R.id.cont);
        tvEmailLabel = findViewById(R.id.emailLAbel);
        regiterSite = findViewById(R.id.regiterSite);
        goForQuatation = findViewById(R.id.quatationCont);

        disableSuggestion(tvAddress);
        disableSuggestion(tvEmail);
        disableSuggestion(tvMobile);
        disableSuggestion(tvUserID);
        disableSuggestion(tvName);


        presenter = new UserDetailsPresenter(this);

    }

    @Override
    public void setUp() {

//
        if (getIntent().getExtras().getString("isFrom", "").equalsIgnoreCase("Landing")) {
            editProfile.setVisibility(VISIBLE);
            editProfile.setOnClickListener(v -> {
                Intent intent = new Intent(MyProfileActivity.this, EditProfileActivity.class);

                startActivityForResult(intent, 3);
            });
            findViewById(R.id.back).setVisibility(VISIBLE);
            findViewById(R.id.back).setOnClickListener(v -> {
                finish();
            });
        }



        regiterSite.setOnClickListener(this);
        tvCont.setOnClickListener(this);
        goForQuatation.setOnClickListener(this);

        Bundle bundle = getIntent().getExtras();


        if (getIntent().getExtras().getString("isFrom").equalsIgnoreCase("Landing")) {
            regiterSite.setVisibility(GONE);


            UserRoleModel.MyAccount myAccount = Utils.getObj(securePrefManager.getSharedValue(ACCOUNT_MODEL), UserRoleModel.MyAccount.class);

            try {
                if (!myAccount.getImages().isEmpty())
                    Glide.with(this).load(myAccount.getImages()).into(profileImage);
            } catch (Exception e) {
                e.printStackTrace();

            }


            tvMobile.setText(myAccount.getPhone());
            tvName.setText(myAccount.getName());
            tvUserID.setText(myAccount.getUserId());
            if (!myAccount.getEmail().trim().isEmpty())
                tvEmail.setText(myAccount.getEmail());
            else {

                tvEmail.setVisibility(GONE);
                tvEmailLabel.setVisibility(GONE);

            }

            tvAddress.setText(myAccount.getAddress().getAddressLine1().trim() + ", " + myAccount.getAddress().getAddressLine2().trim() + ", " + myAccount.getAddress().getCity().trim() + ", " + myAccount.getAddress().getState() + "," + myAccount.getAddress().getCountry().trim() + ", " + myAccount.getAddress().getPinCode());


        } else {
            // Glide.with(this).load(securePrefManager.getSharedValue(PROFILE_IMAGE)).into(profileImage);


            tvMobile.setText(bundle.getString("phone"));
            tvName.setText(bundle.getString("name"));
            tvUserID.setText(bundle.getString("userID"));
            if (!bundle.getString("email").trim().isEmpty())
                tvEmail.setText(bundle.getString("email"));
            else {

                tvEmail.setVisibility(GONE);
                tvEmailLabel.setVisibility(GONE);

            }

            tvAddress.setText(bundle.getString("address"));
            if (!securePrefManager.getSharedValue(PROFILE_IMAGE).equalsIgnoreCase(""))
                Glide.with(this).load(securePrefManager.getSharedValue(PROFILE_IMAGE)).into(profileImage);


            if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(PROSPECT_ROLE_ID) | securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(BO_MANAGER) | securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SG_ADMIN)) {
                goForQuatation.setVisibility(VISIBLE);
                regiterSite.setVisibility(GONE);

            } else {
                goForQuatation.setVisibility(GONE);
                regiterSite.setVisibility(GONE);
                tvCont.setVisibility(VISIBLE);
//                if (bundle.getBoolean("isSiteExist", false)) {
//                    regiterSite.setVisibility(GONE);
//                    tvCont.setVisibility(VISIBLE);
//                } else {
//                    tvCont.setVisibility(GONE);
//                    regiterSite.setVisibility(VISIBLE);
//
//                }
            }

        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 3:
                if (resultCode == Activity.RESULT_OK) {
                    showProgress("Please wait...");
                    presenter.getAccount(securePrefManager.getSharedValue(TOKEN));
                } else {

                }
                break;
        }

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.edit:
                /*move to register screen*/


                Intent intent = new Intent(this, EditProfileActivity.class);

                startActivityForResult(intent, 3);


                break;
            case R.id.regiterSite:
                Bundle bundle = getIntent().getExtras();
                bundle.putString("isFrom", "site");
                goToNextScreen(SiteRegistrationActivity.class, bundle);
                finish();
                securePrefManager.setScreenValue("Site Registration");
                break;
            case R.id.cont:
                if (Utils.hasNetwork(this)) {
                    showProgress("Please wait...");
                    presenter.getLandingDetails(securePrefManager.getSharedValue(TOKEN));
                } else {
                    showToast("Please check Internet");
                }
                break;
            case R.id.quatationCont:

                if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(BO_MANAGER) | securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SG_ADMIN)) {
                    if (Utils.hasNetwork(this)) {
                        showProgress("Please wait...");
                        presenter.getSGDetails(securePrefManager.getSharedValue(TOKEN));
                    } else {
                        showToast("Please check Internet");
                    }
                } else {
                    if (Utils.hasNetwork(this)) {
                        showProgress("Please wait...");
                        presenter.getProspectLAndingDetails(securePrefManager.getSharedValue(TOKEN));
                    } else {
                        showToast("Please check Internet");
                    }
                }


                break;
        }
    }

    @Override
    public void onGetSiteLov(SiteLov details) {
        hideProgress();
        securePrefManager.storeSharedValue(AppConstants.SITELOVS, Utils.toString(details));

        finish();

    }

    @Override
    public void onGetAccountDetails(CommomResponse response) {
        hideProgress();
        UserRoleModel userRoleModel = response.toResponseModel(UserRoleModel.class);
        securePrefManager.storeSharedValue(AppConstants.ACCOUNT_MODEL, Utils.toString(userRoleModel.getMyAccount()));
        UserRoleModel.MyAccount myAccount = Utils.getObj(securePrefManager.getSharedValue(ACCOUNT_MODEL), UserRoleModel.MyAccount.class);
        if (myAccount.getImages() != null)
            Glide.with(this).load(myAccount.getImages()).into(profileImage);


        tvMobile.setText(myAccount.getPhone());
        tvName.setText(myAccount.getName());
        tvUserID.setText(myAccount.getUserId());
        if (!myAccount.getEmail().trim().isEmpty()) {
            tvEmail.setText(myAccount.getEmail());
            tvEmail.setVisibility(VISIBLE);
            tvEmailLabel.setVisibility(VISIBLE);
        } else {
            tvEmail.setVisibility(GONE);
            tvEmailLabel.setVisibility(GONE);

        }

        tvAddress.setText(myAccount.getAddress().getAddressLine1() + ", " + myAccount.getAddress().getAddressLine2() + ", " + myAccount.getAddress().getCity() + ", " + myAccount.getAddress().getState() + ", " + myAccount.getAddress().getCountry() + ", " + myAccount.getAddress().getPinCode());


    }

    @Override
    public void onGetLandingDetails(CommomResponse response) {
        hideProgress();
        Details details = response.toResponseModel(Details.class);
        Bundle bundle = new Bundle();
        securePrefManager.storeSharedValue(NOTIFICATION_COUNT, String.valueOf(details.getNotificationCount()));
        bundle.putSerializable("LandingData", details);
        bundle.putBoolean("testimonials", details.testimonialCreated);
        bundle.putString("for", "landing");
        int flag = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;
        goToNextScreen(UserHomeActivity.class, bundle, flag);


    }

    @Override
    public void onGetSiteDetails(CommomResponse response) {

    }

    @Override
    public void OnGetSGDetails(CommomResponse response) {

        hideProgress();
        SGLandingModel details = response.toResponseModel(SGLandingModel.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("LandingData", details);
        bundle.putString("for", "landing");
        int flag = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;
        goToNextScreen(SGHomeActivity.class, bundle, flag);

    }


    @Override
    public void onFailure(String msg) {
        hideProgress();
        if (msg.equalsIgnoreCase("invalid token")) {
            securePrefManager.clearAppsAllPrefs();
            startActivity(new Intent(MyProfileActivity.this, MainActivity.class));
            finish();
        } else
            showToast(msg);

    }

    @Override
    public void onGetProspectDetails(CommomResponse commomResponse) {
        hideProgress();
        ProspectLandingModel prospectLandingModel = commomResponse.toResponseModel(ProspectLandingModel.class);

        securePrefManager.storeSharedValue(AppConstants.PROSPECT_LANDIND, Utils.toString(prospectLandingModel));

        Intent intent = new Intent(this, ProspectHomeActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("for", "landing");
        int flag = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;
        intent.setFlags(flag);
        intent.putExtras(bundle);
        startActivity(intent);


    }
}
