package com.solarginni.user.UserRegistration;

import android.content.Context;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.APIBody.ImageType;
import com.solarginni.DBModel.APIBody.UserRegisteration;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.UserIdModel;
import com.solarginni.Utility.AppConstants;
import com.solarginni.Utility.SecurePrefManager;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;
import com.solarginni.network.MultipartParams;

import java.io.File;

import retrofit2.Retrofit;

public class RegsiterInteractor implements RegisterContract.Interector {

    private Retrofit mRetrofit;
    private ApiInterface mApiInterface;

    public RegsiterInteractor(Retrofit retrofit) {
        this.mRetrofit = retrofit;
        mApiInterface = mRetrofit.create(ApiInterface.class);
    }


    @Override
    public void registerUser(Context context, UserRegisteration user, String token, File file, RegisterContract.OnInterction listener) {

        if (file == null) {
            mApiInterface.registerUser(user, token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                @Override
                public void onSuccess(CommomResponse response) {
                    UserIdModel model = response.toResponseModel(UserIdModel.class);
                    listener.onregisterUser(model);


                }

                @Override
                public void onError(ApiError error) {
                    listener.onFailure(error.getMessage());

                }

                @Override
                public void onFailure(Throwable throwable) {
                    listener.onFailure(throwable.getMessage());

                }
            });

        }
        else {
            MultipartParams multipartParams = new MultipartParams.Builder()
                    .addFile("upload", file).build();

            mApiInterface.uploadImage(multipartParams.getMap(), ImageType.USER_TYPE, token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                @Override
                public void onSuccess(CommomResponse commomResponse) {
                    String profileImage = commomResponse.toResponseModel(String.class);
                    SecurePrefManager securePrefManager = new SecurePrefManager(context);
                    securePrefManager.storeSharedValue(AppConstants.PROFILE_IMAGE, profileImage);
                    user.profileImage = profileImage;

                    mApiInterface.registerUser(user, token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                        @Override
                        public void onSuccess(CommomResponse commomResponse) {
                            UserIdModel model = commomResponse.toResponseModel(UserIdModel.class);
                            listener.onregisterUser(model);
                        }

                        @Override
                        public void onError(ApiError error) {
                            listener.onFailure(error.getMessage());
                        }

                        @Override
                        public void onFailure(Throwable throwable) {
                            listener.onFailure(throwable.getMessage());
                        }
                    });


                }

                @Override
                public void onError(ApiError error) {
                    listener.onFailure(error.getMessage());

                }

                @Override
                public void onFailure(Throwable throwable) {
                    listener.onFailure(throwable.getMessage());
                }
            });

        }


    }
}
