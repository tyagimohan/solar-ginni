package com.solarginni.user.UserRegistration;

import android.content.Context;

import com.solarginni.DBModel.APIBody.UserRegisteration;
import com.solarginni.DBModel.UserIdModel;
import com.solarginni.network.RestClient;

import java.io.File;

public class RegisterPresenter implements RegisterContract.Presenter, RegisterContract.OnInterction {

    private RegisterContract.View view;
    private RegsiterInteractor interactor;


    public RegisterPresenter(RegisterContract.View view) {
        this.view = view;
        interactor= new RegsiterInteractor(RestClient.getRetrofitBuilder());
    }

    @Override
    public void registerUser(Context context, UserRegisteration user, String token, File file) {
        interactor.registerUser(context,user,token,file,this);

    }

    @Override
    public void onregisterUser(UserIdModel model) {
        view.onregisterUser(model);

    }

    @Override
    public void onFailure(String msg) {
        view.onFailure(msg);
    }
}
