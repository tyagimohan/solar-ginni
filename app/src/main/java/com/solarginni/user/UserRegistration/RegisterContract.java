package com.solarginni.user.UserRegistration;

import android.content.Context;

import com.solarginni.DBModel.APIBody.UserRegisteration;
import com.solarginni.DBModel.UserIdModel;

import java.io.File;

public interface RegisterContract {
    interface View {
        void onregisterUser(UserIdModel model);
        void onFailure(String msg);
    }

    interface Presenter {
        void registerUser(Context context, UserRegisteration user, String token,  File file);
    }

    interface Interector {
        void registerUser(Context context, UserRegisteration user, String token, File file, RegisterContract.OnInterction listener);

    }

    interface OnInterction {
        void onregisterUser(UserIdModel model);
        void onFailure(String msg);
    }
}
