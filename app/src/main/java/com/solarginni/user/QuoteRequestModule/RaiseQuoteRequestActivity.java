package com.solarginni.user.QuoteRequestModule;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.solarginni.Base.BaseActivity;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.RequestLovs.RequestType;
import com.solarginni.R;
import com.solarginni.Utility.Utils;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import static com.solarginni.Utility.AppConstants.TOKEN;

public class RaiseQuoteRequestActivity extends BaseActivity implements View.OnClickListener, QuoteRequestDetailCOntract.View {

    private EditText tvQuoteId, tvRequestType, comment, tvRaiseedOn;
    private Button submit;
    private String[] quoteArray, requestTypeArray;
    private QuoteRequestDetailPresenter presenter;

    @Override
    public int getLayout() {
        return R.layout.raise_quote_request;
    }

    @Override
    public void initViews() {

        tvQuoteId = findViewById(R.id.tvQuoteId);

        tvRequestType = findViewById(R.id.tvRequest);
        comment = findViewById(R.id.comment);
        tvRaiseedOn = findViewById(R.id.tvRaisedOn);
        presenter= new QuoteRequestDetailPresenter(this);
        submit = findViewById(R.id.confirmBtn);

        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("");
        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });


    }

    @Override
    public void setUp() {

        Bundle bundle = getIntent().getExtras();

        tvQuoteId.setText(bundle.getString("quoteId", ""));


        if (Utils.hasNetwork(this)){
            showProgress("Please wait..");
            presenter.fetchlov(securePrefManager.getSharedValue(TOKEN));

        }
        else {
            showToast("Please check Internet");
            finish();

        }

        if (bundle.getBoolean("forQuoteId", false)) {
            ArrayList<String> list = new ArrayList<>();
            list = bundle.getStringArrayList("quoteList");
            quoteArray = new String[list.size()];

            quoteArray = list.toArray(quoteArray);
            tvQuoteId.setOnClickListener(this);

        }
        tvRaiseedOn.setText(getDate(System.currentTimeMillis()));




        submit.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.confirmBtn:
                if (tvQuoteId.getText().toString().trim().length() == 0) {
                    showToast("Please Select Quote");
                    return;
                } else if (tvRequestType.getText().toString().trim().length() == 0) {
                    showToast("Please Select Request Type");
                    return;
                } else {
                    if (Utils.hasNetwork(this)) {

                        showProgress("Please wait..");
                        presenter.raiseRequest(securePrefManager.getSharedValue(TOKEN),tvRequestType.getText().toString(),tvQuoteId.getText().toString(),comment.getText().toString());


                    } else {
                        showToast(getString(R.string.internet_error));

                    }
                }
                break;
            case R.id.tvQuoteId:
                showAlertWithList(quoteArray,"Select Quote Id",tvQuoteId);

                break;
            case R.id.tvRequest:
                showAlertWithList(requestTypeArray, R.string.rquest_type_title, tvRequestType);
                break;
        }

    }


    @Override
    public void onRequestClose() {

    }

    @Override
    public void onCloseRequestImp() {

    }

    @Override
    public void onFetchLov(CommomResponse response) {
        hideProgress();
        RequesLovModel model = response.toResponseModel(RequesLovModel.class);
        requestTypeArray= new String[model.requestType.getLovValues().size()];

        requestTypeArray = model.requestType.getLovValues().toArray(requestTypeArray);

        tvRequestType.setOnClickListener(this);


    }

    @Override
    public void onSubmitFeedback() {

    }

    @Override
    public void onRaiseReq() {
        hideProgress();
        finish();
    }

    @Override
    public void onFailure() {
        hideProgress();
        showToast("Error in Raise Request");

    }

    private String getDate(long milliSeconds) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public class RequesLovModel implements Serializable{
        @SerializedName("requestType")
        @Expose
        public RequestType requestType;
    }
}
