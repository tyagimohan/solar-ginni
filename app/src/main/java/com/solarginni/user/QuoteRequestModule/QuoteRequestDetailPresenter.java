package com.solarginni.user.QuoteRequestModule;

import com.solarginni.DBModel.APIBody.RequestParam;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.RestClient;

public class QuoteRequestDetailPresenter implements QuoteRequestDetailCOntract.Presenter, QuoteRequestDetailCOntract.OnIneraction {
    private QuoteRequestDetailCOntract.View view;
    private QuoteRequestDetailInteractor interactor;

    public QuoteRequestDetailPresenter(QuoteRequestDetailCOntract.View view) {
        this.view = view;
        interactor = new QuoteRequestDetailInteractor(RestClient.getRetrofitBuilder());
    }



    @Override
    public void fetchlov(String token) {
        interactor.fetchlov(token,this);

    }

    @Override
    public void raiseRequest(String token, String type, String quoteId,String comment) {
        interactor.raiseRequest(token,type,quoteId,comment,this);

    }

    @Override
    public void closeRequest(String token, String impleRemark,String closureRemarl, String status, String requestId,String assignedTo) {
          interactor.closeRequest(token,impleRemark,closureRemarl,status,requestId,assignedTo,this);
    }

    @Override
    public void rateRequest(String token, String requestId, RequestParam param) {
        interactor.rateRequest(token,requestId,param,this);

    }




    @Override
    public void onRequestClose() {
     view.onRequestClose();
    }

    @Override
    public void onSubmitFeedback() {
        view.onSubmitFeedback();

    }

    @Override
    public void onFetchLov(CommomResponse response) {
        view.onFetchLov(response);
    }

    @Override
    public void onRaiseReq() {
        view.onRaiseReq();

    }


    @Override
    public void onCloseRequestImp() {
        view.onCloseRequestImp();

    }

    @Override
    public void onFailure() {
        view.onFailure();
    }
}
