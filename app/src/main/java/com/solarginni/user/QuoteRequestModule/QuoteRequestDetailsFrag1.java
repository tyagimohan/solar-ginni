package com.solarginni.user.QuoteRequestModule;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.solarginni.Base.BaseFragment;
import com.solarginni.DBModel.RequestData;
import com.solarginni.R;
import com.solarginni.Utility.Utils;

public class QuoteRequestDetailsFrag1 extends BaseFragment {
    public EditText requestId;
    private EditText quoteId, requestType, raisedOn, resolovedOn, customerMob, comment;

    @Override
    public int getLayout() {
        return R.layout.quote_request_details1;
    }

    @Override
    public void initViews(View view) {

        requestId = view.findViewById(R.id.requestID);

        requestType = view.findViewById(R.id.requestType);
        customerMob = view.findViewById(R.id.customerMob);

        quoteId = view.findViewById(R.id.quoteId);
        raisedOn = view.findViewById(R.id.raisedOn);
        resolovedOn = view.findViewById(R.id.resolvedOn);
        comment = view.findViewById(R.id.comments);

        disableSuggestion(comment);
        disableSuggestion(quoteId);
        disableSuggestion(requestType);
        disableSuggestion(raisedOn);
        disableSuggestion(resolovedOn);
        disableSuggestion(customerMob);
        disableSuggestion(requestId);

    }

    @Override
    public void setUp() {


        Bundle bundle = getArguments();
        RequestData.RequestDetail requestDetail = (RequestData.RequestDetail) bundle.getSerializable("data");

        requestId.setText(requestDetail.getRequestId());

        requestType.setText(requestDetail.getRequestType());

        quoteId.setText(requestDetail.getQuoteId());
        customerMob.setText(requestDetail.getCustomerPhone());
        raisedOn.setText(Utils.convertUTCtoMyTime(requestDetail.getRaisedOn()));

        if (requestDetail.getComment() != null)
            comment.setText(requestDetail.getComment());


        if (requestDetail.getRequestStatus().equalsIgnoreCase("Closed")) {
            resolovedOn.setText(Utils.convertUTCtoMyTime(requestDetail.getClosedOn()));
        } else if (requestDetail.getResolvedOn() != null) {
            resolovedOn.setText(Utils.convertUTCtoMyTime(requestDetail.getResolvedOn()));
        } else
            resolovedOn.setText("--");

    }
}
