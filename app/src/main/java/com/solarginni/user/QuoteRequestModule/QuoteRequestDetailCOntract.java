package com.solarginni.user.QuoteRequestModule;

import com.solarginni.DBModel.APIBody.RequestParam;
import com.solarginni.DBModel.CommomResponse;

public interface QuoteRequestDetailCOntract {

    interface View {

        void onRequestClose();

        void onCloseRequestImp();

        void onFetchLov(CommomResponse response);

        void onSubmitFeedback();

         void onRaiseReq();

        void onFailure();
    }

    interface Presenter {


        void fetchlov(String token);

        void raiseRequest(String token,String type,String quoteId,String comment);

        void closeRequest(String token, String impleRemark, String closureRemarl, String status, String requestId, String assignedTo);

        void rateRequest(String token, String requestId, RequestParam param);


    }

    interface Interector {

        void fetchlov(String token, OnIneraction ineraction);
        void raiseRequest(String token,String type,String quoteId,String comment, OnIneraction ineraction);

        void closeRequest(String token, String impleRemark, String closureRemarl, String status, String requestId, String assignedTo, OnIneraction ineraction);

        void rateRequest(String token, String requestId, RequestParam param, OnIneraction ineraction);


    }

    interface OnIneraction {

        void onRequestClose();

        void onSubmitFeedback();

        void onFetchLov(CommomResponse response);

        void onRaiseReq();

        void onCloseRequestImp();

        void onFailure();
    }
}
