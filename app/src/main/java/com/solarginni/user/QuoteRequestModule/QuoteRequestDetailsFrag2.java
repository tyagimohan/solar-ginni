package com.solarginni.user.QuoteRequestModule;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.solarginni.Base.BaseFragment;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.RequestData;
import com.solarginni.R;
import com.solarginni.Utility.Utils;

import static com.solarginni.Utility.AppConstants.CUSTOMER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.IMPLEMENTER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.SUPER_IMPLEMENTER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.TOKEN;

public class QuoteRequestDetailsFrag2 extends BaseFragment implements View.OnClickListener, QuoteRequestDetailCOntract.View {

    private EditText implementernane, assignedTo;
    private EditText customerRemark, implementerRemark, requestStatus;
    private Button closeRequest;
    private RequestData.RequestDetail requestDetail;
    private QuoteRequestDetailPresenter presenter;
    private String[] statusArray;

    @Override
    public int getLayout() {
        return R.layout.quote_request_details2;
    }

    @Override
    public void initViews(View view) {

        implementernane = view.findViewById(R.id.implementerName);
        assignedTo = view.findViewById(R.id.assignedTo);
        implementerRemark = view.findViewById(R.id.implementerRemark);
        customerRemark = view.findViewById(R.id.customerRemark);
        closeRequest = view.findViewById(R.id.confirmBtn);
        requestStatus = view.findViewById(R.id.requestStatus);
        presenter = new QuoteRequestDetailPresenter(this);
        disableSuggestion(implementerRemark);
        disableSuggestion(assignedTo);
        disableSuggestion(implementerRemark);
        disableSuggestion(customerRemark);
        disableSuggestion(requestStatus);


    }

    @Override
    public void setUp() {

        Bundle bundle = getArguments();
        requestDetail = (RequestData.RequestDetail) bundle.getSerializable("data");

        requestStatus.setText(requestDetail.getRequestStatus());


        assignedTo.setText(requestDetail.getAssignTo());


        /*set for implementer*/
        implementernane.setText(requestDetail.getCompanyName());

        customerRemark.setText(requestDetail.getClosureNotes());

        if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(IMPLEMENTER_ROLE_ID) | securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SUPER_IMPLEMENTER_ROLE_ID
        )) {
            implementerRemark.setFocusableInTouchMode(true);
            implementerRemark.setFocusable(true);
            customerRemark.setFocusable(false);
            customerRemark.setFocusableInTouchMode(false);

            if (requestDetail.getRequestStatus().equalsIgnoreCase("Assigned")) {
                statusArray = new String[]{"Resolved"};
                requestStatus.setOnClickListener(this);
            } else if (requestDetail.getRequestStatus().equalsIgnoreCase("Closed") | requestDetail.getRequestStatus().equalsIgnoreCase("Resolved")) {
                closeRequest.setVisibility(View.GONE);


            } else if (requestDetail.getRequestStatus().equalsIgnoreCase("Re-Opened") | requestDetail.getRequestStatus().equalsIgnoreCase("Open")) {
                statusArray = new String[]{"Assigned"};
                requestStatus.setOnClickListener(this);
            }

        }

        /*set for customer*/

        else if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(CUSTOMER_ROLE_ID)) {
            implementerRemark.setFocusableInTouchMode(false);
            implementerRemark.setFocusable(false);


            if (requestDetail.getRequestStatus().equalsIgnoreCase("Open")) {
                statusArray = new String[]{"Closed"};
                requestStatus.setOnClickListener(this);
            } else if (requestDetail.getRequestStatus().equalsIgnoreCase("Resolved")) {
                statusArray = new String[]{"Re-Opened", "Closed"};
                requestStatus.setOnClickListener(this);
            }
            customerRemark.setFocusable(true);
            customerRemark.setFocusableInTouchMode(true);
        }


        if (requestDetail.getResolutionNotes() != null)
            implementerRemark.setText(requestDetail.getResolutionNotes());
        else
            implementerRemark.setText("");


        if (requestDetail.getRequestStatus().equalsIgnoreCase("Closed")) {

            customerRemark.setFocusable(false);
            customerRemark.setFocusableInTouchMode(false);
            closeRequest.setVisibility(View.GONE);
            customerRemark.setText(requestDetail.getClosureNotes());

        }


        closeRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(getActivity());

                if (!requestDetail.getRequestStatus().equalsIgnoreCase(requestStatus.getText().toString())) {
                    if (Utils.hasNetwork(getContext())) {
                        showProgress("Please wait..");
                        presenter.closeRequest(securePrefManager.getSharedValue(TOKEN), implementerRemark.getText().toString(), customerRemark.getText().toString(), requestStatus.getText().toString(), requestDetail.getRequestId(), assignedTo.getText().toString());
                    } else
                        showToast("Please check Internet");
                } else {
                    showToast("Please change request status ");
                }


            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.requestStatus:
                showAlertWithList(statusArray, R.string.select_status, requestStatus);
                break;
        }

    }


    @Override
    public void onRequestClose() {
        hideProgress();
        showToast("Status Updated");

        if (requestStatus.getText().toString().equalsIgnoreCase("Closed") || requestStatus.getText().toString().equalsIgnoreCase("Resolved"))
            closeRequest.setVisibility(View.GONE);

        if (requestStatus.getText().toString().equalsIgnoreCase("Closed")) {
            showToast("Close Request");
            getActivity().setResult(Activity.RESULT_OK);
            getActivity().finish();
        } else {
            getActivity().setResult(Activity.RESULT_OK);
            getActivity().finish();

        }

    }

    @Override
    public void onCloseRequestImp() {

    }

    @Override
    public void onFetchLov(CommomResponse response) {


    }

    @Override
    public void onSubmitFeedback() {

    }

    @Override
    public void onRaiseReq() {

    }

    @Override
    public void onFailure() {
        showToast("Error in Close Request");
        hideProgress();

    }
}
