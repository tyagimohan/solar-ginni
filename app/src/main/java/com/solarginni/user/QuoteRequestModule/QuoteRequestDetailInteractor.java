package com.solarginni.user.QuoteRequestModule;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.APIBody.RequestParam;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;

import retrofit2.Retrofit;

public class QuoteRequestDetailInteractor implements QuoteRequestDetailCOntract.Interector {
    private Retrofit retrofit;
    private ApiInterface apiInterface;


    public QuoteRequestDetailInteractor(Retrofit retrofit) {
        this.retrofit = retrofit;
        apiInterface = retrofit.create(ApiInterface.class);
    }


    @Override
    public void fetchlov(String token, QuoteRequestDetailCOntract.OnIneraction ineraction) {
        apiInterface.getQuoteRequestLov(token).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                ineraction.onFetchLov(response);

            }

            @Override
            public void onError(ApiError error) {
                ineraction.onFailure();


            }

            @Override
            public void onFailure(Throwable throwable) {
                ineraction.onFailure();


            }
        });

    }

    @Override
    public void raiseRequest(String token, String type, String quoteId,String comment, QuoteRequestDetailCOntract.OnIneraction ineraction) {
        apiInterface.raiseQuoteRequest(token,quoteId,type,comment).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                ineraction.onRaiseReq();

            }

            @Override
            public void onError(ApiError error) {
                ineraction.onFailure();

            }

            @Override
            public void onFailure(Throwable throwable) {
                ineraction.onFailure();

            }
        });

    }

    @Override
    public void closeRequest(String token, String impleRemark,String closureRemarl, String status, String requestId,String assignedTo, QuoteRequestDetailCOntract.OnIneraction ineraction) {
        apiInterface.closeRequest(token, requestId, impleRemark,closureRemarl,status,assignedTo).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                ineraction.onRequestClose();

            }

            @Override
            public void onError(ApiError error) {
                ineraction.onFailure();

            }

            @Override
            public void onFailure(Throwable throwable) {
                ineraction.onFailure();

            }
        });

    }

    @Override
    public void rateRequest(String token, String requestId, RequestParam param, QuoteRequestDetailCOntract.OnIneraction ineraction) {
        apiInterface.rateRequest(token,param,requestId).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                ineraction.onSubmitFeedback();

            }

            @Override
            public void onError(ApiError error) {
                ineraction.onFailure();

            }

            @Override
            public void onFailure(Throwable throwable) {
                ineraction.onFailure();

            }
        });

    }




}
