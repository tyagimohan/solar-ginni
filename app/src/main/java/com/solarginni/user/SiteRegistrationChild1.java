package com.solarginni.user;

import android.content.Context;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.solarginni.Base.BaseFragment;
import com.solarginni.R;
import com.solarginni.SiteLovsModule.SiteDetailModel.SiteLov;
import com.solarginni.Utility.DecimalDigitsInputFilter;
import com.solarginni.Utility.Utils;

import static com.solarginni.Utility.AppConstants.BO_MANAGER;
import static com.solarginni.Utility.AppConstants.MOBILE;
import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.SG_ADMIN;
import static com.solarginni.Utility.AppConstants.SITELOVS;

/**
 * Created by Mohan on 16/1/20.
 */

public class SiteRegistrationChild1 extends BaseFragment implements View.OnClickListener {

    public static EditText tvPahsel;
    public static EditText tvSolution, tvSiteType, tvMobileNumber, inverterCount;
    public static EditText etSiteSize;
    public static EditText etConnectionId;
    private SolutionListener listener;
    private String[] phaseArray, solutionArray, siteTypeAray;

    public SiteRegistrationChild1() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SolutionListener) {
            listener = (SolutionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement TextClicked");
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    @Override
    public void onDetach() {
        // => avoid leaking, thanks @Deepscorn
        super.onDetach();
        listener = null;
    }

    @Override
    public int getLayout() {
        return R.layout.site_registration_part1;
    }

    @Override
    public void initViews(View view) {

        tvSolution = view.findViewById(R.id.solution);
        tvSiteType = view.findViewById(R.id.siteType);
        tvMobileNumber = view.findViewById(R.id.tvmobileNumber);
        tvPahsel = view.findViewById(R.id.phase);
        etConnectionId = view.findViewById(R.id.connectionId);
        etSiteSize = view.findViewById(R.id.siteSize);
        etSiteSize.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(7, 2)});

        disableSuggestion(tvMobileNumber);
        disableSuggestion(tvSiteType);
        disableSuggestion(tvSolution);
        disableSuggestion(tvPahsel);
        disableSuggestion(etConnectionId);
        disableSuggestion(etSiteSize);
        setupUI(view.findViewById(R.id.parentLay));
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        view.clearFocus();
    }

    @Override
    public void setUp() {


        if (getArguments().getString("isFrom", "").equalsIgnoreCase("implementerFrag") | securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SG_ADMIN) | securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(BO_MANAGER)) {

            tvMobileNumber.setFocusableInTouchMode(true);
            tvMobileNumber.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});


        } else {
            tvMobileNumber.setText(securePrefManager.getSharedValue(MOBILE));
            tvMobileNumber.setFocusableInTouchMode(false);
            tvMobileNumber.setFilters(new InputFilter[]{new InputFilter.LengthFilter(13)});
        }


        SiteLov details = Utils.getObj(securePrefManager.getSharedValue(SITELOVS), SiteLov.class);


        phaseArray = getContext().getResources().getStringArray(R.array.phase_type);
        this.siteTypeAray = new String[details.getSiteDetails().getSiteType().getLovValues().size()];
        for (int i = 0; i < details.getSiteDetails().getSiteType().getLovValues().size(); i++) {
            try {
                this.siteTypeAray = details.getSiteDetails().getSiteType().getLovValues().toArray(siteTypeAray);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        solutionArray = new String[details.getSiteDetails().getSolution().getLovValues().size()];
        for (int i = 0; i < details.getSiteDetails().getSolution().getLovValues().size(); i++) {
            try {
                this.solutionArray = details.getSiteDetails().getSolution().getLovValues().toArray(solutionArray);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        tvSolution.setOnClickListener(this);
        tvPahsel.setOnClickListener(this);
        tvSiteType.setOnClickListener(this);

        tvMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 10) {
                    hideKeyboard(getActivity());
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.solution:
                showAlertWithListForSOl(solutionArray, R.string.select_solution, tvSolution);
                break;

            case R.id.phase:
                showAlertWithList(phaseArray, R.string.select_phase, tvPahsel);
                break;

            case R.id.siteType:
                showAlertWithList(siteTypeAray, R.string.select_site, tvSiteType);
                break;
        }

    }

    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    ///  view.requestFocus();
                    hideKeyboard();
                    etConnectionId.clearFocus();
                    etSiteSize.clearFocus();
                    return false;
                }
            });
        }
    }

    public void showAlertWithListForSOl(final String[] dataArray, final @StringRes int title, final TextView textView) {
        hideKeyboard(getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(title));
        builder.setItems(dataArray, (dialog, item) -> {
            textView.setText(dataArray[item]);
            if (dataArray[item].equalsIgnoreCase("Hybrid")) {
                listener.selectHybrid(true);
            } else {
                listener.selectHybrid(false);

            }
        });
        AlertDialog alerts = builder.create();
        alerts.show();
    }


    public interface SolutionListener {
        void selectHybrid(boolean isHybrid);
    }


}
