package com.solarginni.CommonModule;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.solarginni.Base.BaseActivity;
import com.solarginni.CommonModule.ImplementerCompanyDetail.ImplementerCompanyContract;
import com.solarginni.CommonModule.ImplementerCompanyDetail.ImplementerCompanyPresenter;
import com.solarginni.Implementer.ImplementerRegistration;
import com.solarginni.R;
import com.solarginni.Utility.AppConstants;
import com.solarginni.Utility.Utils;
import com.solarginni.user.UserRegistration.CustomerRegisterActivity;

import static com.solarginni.Utility.AppConstants.MOBILE;

public class SelectRoleActivity extends BaseActivity implements View.OnClickListener, ImplementerCompanyContract.View {
    private Button customerBtn, implementerBtn;
    private ImplementerCompanyPresenter presenter;
    private ImageView implementerIcon, prospectIcon, customerIcon, boIcon, sgIcon;
    private LinearLayout lay1, lay2, lay3, lay4, lay5;
    private Button confirmBtn;
    private int role = 83;

    @Override
    public int getLayout() {
        return R.layout.role_based;
    }

    @Override
    public void initViews() {
        customerBtn = findViewById(R.id.customer);
        implementerBtn = findViewById(R.id.implenter);
        customerIcon = findViewById(R.id.customerIcon);
        prospectIcon = findViewById(R.id.prospectIcon);
        boIcon = findViewById(R.id.bo_manger);
        sgIcon = findViewById(R.id.sg_user);
        confirmBtn = findViewById(R.id.confirmBtn);
        implementerIcon = findViewById(R.id.implementerIcon);
        lay1 = findViewById(R.id.lay1);
        lay3 = findViewById(R.id.lay3);
        lay2 = findViewById(R.id.lay2);
        lay4 = findViewById(R.id.lay4);
        lay5 = findViewById(R.id.lay5);

        presenter = new ImplementerCompanyPresenter(this);
    }

    @Override
    public void setUp() {

        customerBtn.setOnClickListener(this);
        implementerBtn.setOnClickListener(this);
        lay2.setOnClickListener(this);
        lay3.setOnClickListener(this);
        lay1.setOnClickListener(this);
        lay4.setOnClickListener(this);
        lay5.setOnClickListener(this);
        confirmBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        final Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce);
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.5, 5);
        myAnim.setInterpolator(interpolator);
        switch (view.getId()) {
            case R.id.customer:

                Bundle bundle = getIntent().getExtras();

                int flag = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;
                goToNextScreen(SelectCustomerRole.class, bundle, flag);
                break;
            case R.id.implenter:

                break;
            case R.id.lay1:
                setRole("Customer");
                break;
            case R.id.lay2:
                setRole("Prospect");
                break;
            case R.id.lay4:
                setRole("BO");
                break;
            case R.id.lay5:
                setRole("SG");
                break;
            case R.id.lay3:
                setRole("Implementer");
                break;
            case R.id.confirmBtn:
                if (role == 83) {
                    showToast("Please select role");
                    return;
                } else if (role == 1) {
                    if (Utils.hasNetwork(this)) {
                        showProgress("Please wait...");
                        presenter.getImplementerCompany(securePrefManager.getSharedValue(AppConstants.TOKEN));
                    } else {
                        showToast(getString(R.string.internet_error));
                    }
                } else {
                    bundle = new Bundle();
                    bundle.putInt("role", role);
                    flag = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;
                    goToNextScreen(CustomerRegisterActivity.class, bundle, flag);
                }
                break;


        }

    }

    public void setRole(String roleType) {
        final Animation myAnim = AnimationUtils.loadAnimation(this, R.anim.bounce);
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.5, 5);
        myAnim.setInterpolator(interpolator);
        switch (roleType) {

            case "Customer":
                lay1.setAnimation(myAnim);
                role = 7;
                customerIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_credibility_blue));
                implementerIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_testing));
                prospectIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_review));
                sgIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_sg_user_un));
                boIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_bo_manger_un));


                break;
            case "Prospect":
                lay2.setAnimation(myAnim);
                role = 3;
                customerIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_credibility));
                implementerIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_testing));
                prospectIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_rating));
                sgIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_sg_user_un));
                boIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_bo_manger_un));

                break;
            case "Implementer":
                lay3.setAnimation(myAnim);
                role = 1;
                customerIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_credibility));
                implementerIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_project_blue));
                prospectIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_review));
                sgIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_sg_user_un));
                boIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_bo_manger_un));


                break;
            case "BO":
                lay4.setAnimation(myAnim);
                role = 11;
                customerIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_credibility));
                implementerIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_testing));
                prospectIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_review));
                boIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_bo_manger));
                sgIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_sg_user_un));

                break;
            case "SG":
                lay5.setAnimation(myAnim);
                role = 10;
                customerIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_credibility));
                implementerIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_testing));
                prospectIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_review));
                boIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_bo_manger_un));
                sgIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_sg_user));

                break;

        }

    }


    @Override
    public void onGetImplementerCompany(ImplementerCompanyModel details) {
        hideProgress();
        Bundle bundle1 = new Bundle();
        int flag = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;
        bundle1.putString("phone", securePrefManager.getSharedValue(MOBILE));
        bundle1.putSerializable("details", details);
        bundle1.putInt("role", 1);
        goToNextScreen(ImplementerRegistration.class, bundle1, flag);

    }

    @Override
    public void onFailure(String msg) {
        hideProgress();
        if (msg.equalsIgnoreCase("invalid token")) {
            securePrefManager.clearAppsAllPrefs();
            startActivity(new Intent(SelectRoleActivity.this, MainActivity.class));
            finish();
        } else {
            showToast(msg);
        }
    }

    public class MyBounceInterpolator implements android.view.animation.Interpolator {
        private double mAmplitude = 1;
        private double mFrequency = 10;

        public MyBounceInterpolator(double amplitude, double frequency) {
            mAmplitude = amplitude;
            mFrequency = frequency;
        }

        public float getInterpolation(float time) {
            return (float) (-1 * Math.pow(Math.E, -time / mAmplitude) *
                    Math.cos(mFrequency * time) + 1);
        }
    }

}
