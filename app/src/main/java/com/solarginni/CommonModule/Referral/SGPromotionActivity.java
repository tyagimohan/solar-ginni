package com.solarginni.CommonModule.Referral;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.solarginni.Base.BaseActivity;
import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.CommonModule.MainActivity;
import com.solarginni.CommonModule.SGPromotionApi;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.SGPromotionalModel;
import com.solarginni.R;
import com.solarginni.Utility.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.solarginni.Utility.AppConstants.TOKEN;

public class SGPromotionActivity extends BaseActivity implements View.OnClickListener {
    private TextView sortValue, filterValue;
    private EditText etMobileNumber;
    private Button searchButton;
    SortingAdapter sortingAdapter;

    private boolean isRefresh = false;
    private boolean isFilter = false;
    private LinearLayoutManager layoutManager;
    private RecyclerView recyclerView;
    private boolean isLoading = false;
    private LinearLayout progressPagination;
    private SGPromotionsAdapter adapter;
    private SGPromotionApi api;
    private TextView referred, rank, dispursed, earned;
    List<SortingModel> sortedList = new ArrayList<>();
    private String[] sortedValueArray;
    private int pos=0;

    @Override
    public int getLayout() {
        return R.layout.sg_promotion_layout;
    }

    @Override
    public void initViews() {
        recyclerView = findViewById(R.id.recyclerView);
        searchButton = findViewById(R.id.serach_btn);

        sortValue = findViewById(R.id.sortValue);
        filterValue = findViewById(R.id.filterValue);

        sortValue.setOnClickListener(this);
        filterValue.setOnClickListener(this);

        findViewById(R.id.lay1).setOnClickListener(this);
        findViewById(R.id.lay2).setOnClickListener(this);

        etMobileNumber = findViewById(R.id.mobileNumber);
        progressPagination = (LinearLayout) findViewById(R.id.progressPagination);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new BaseRecyclerAdapter.DividerItemDecoration(this, RecyclerView.VERTICAL));
        adapter = new SGPromotionsAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(recyclerScroller);

        sortingAdapter = new SortingAdapter(this);
        sortedValueArray = getResources().getStringArray(R.array.refer_sort);
        prepaerList();
        sortingAdapter.addAll(sortedList);
        referred = findViewById(R.id.referred);
        rank = findViewById(R.id.rank);
        dispursed = findViewById(R.id.dispursed);
        earned = findViewById(R.id.earned);
        findViewById(R.id.refresh).setOnClickListener(this);
    }

    @Override
    public void setUp() {
        setupUI(findViewById(R.id.parent));
        setupUI(findViewById(R.id.recyclerView));

        searchButton.setOnClickListener(this);

        if (Utils.hasNetwork(this)) {
            api = new SGPromotionApi() {
                @Override
                public void onComplete(CommomResponse response) {
                    hideProgress();
                    progressPagination.setVisibility(View.GONE);
                    SGPromotionalModel model = response.toResponseModel(SGPromotionalModel.class);
//                    SGPromotionsAdapter      adapter = new SGPromotionsAdapter(SGPromotionActivity.this);
//                    recyclerView.setAdapter(adapter);
                    etMobileNumber.setText("");
                    rank.setText( model.topRanker.equalsIgnoreCase("0")?" N.A ":model.topRanker);
                    referred.setText("" + model.referredCount);
                    earned.setText(" INR " + model.earned);
                    dispursed.setText(" INR " + model.dispursed);
                    if (model.sgPromotionModelList.size()==0){
                        findViewById(R.id.detailsLabel).setVisibility(View.GONE);
                    }
                    else {
                        findViewById(R.id.detailsLabel).setVisibility(View.VISIBLE);
                    }
                    if (isFilter) {
                        isFilter = false;
                        adapter.notifyItemRangeRemoved(0, adapter.getItemCount());
                        adapter.clear();
                        adapter.notifyDataSetChanged();
                    }

                    if (model.sgPromotionModelList.size() == 0 && isFilter) {
                        showToast("No User Found");
                        adapter.notifyItemRangeRemoved(0, adapter.getItemCount());
                        adapter.clear();
                        adapter.notifyDataSetChanged();
                        if (isRefresh) {
                            isRefresh = true;
                            filterValue.setText("All");
                            handleSortClick(0);

                        }

                    } else {
                        isLoading = false;
                        if (model.sgPromotionModelList.size() > 0 && model.sgPromotionModelList.size() <= 9) {
                            isLoading = true;
                        }
                        if (isRefresh) {
                            isRefresh = false;
                            filterValue.setText("All");
                            handleSortClick(0);
                            adapter.notifyItemRangeRemoved(0, adapter.getItemCount());
                            adapter.clear();
                            adapter.notifyDataSetChanged();


                        }
                        int initialCount = adapter.getItemCount();
                        adapter.addAllForPagination(model.sgPromotionModelList);
            //     adapter.notifyItemRangeChanged(initialCount, model.sgPromotionModelList.size());
                        adapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onSearch(CommomResponse response) {
                    hideProgress();
                    SGPromotionalModel model = response.toResponseModel(SGPromotionalModel.class);
                    rank.setText( model.topRanker.equalsIgnoreCase("0")?" N.A ":model.topRanker);
                    referred.setText("" + model.referredCount);
                    earned.setText(" INR " + model.earned);
                    dispursed.setText(" INR " + model.dispursed);
                    isLoading = true;
                    adapter.notifyItemRangeRemoved(0, adapter.getItemCount());
                    if (model.sgPromotionModelList.size()==0){
                        findViewById(R.id.detailsLabel).setVisibility(View.GONE);
                    }
                    else {
                        findViewById(R.id.detailsLabel).setVisibility(View.VISIBLE);
                    }
                    if (model.sgPromotionModelList.size() == 0)
                        showToast("No User Found");

                    filterValue.setText("All");
                    handleSortClick(0);
                    adapter.addAll(model.sgPromotionModelList);
                    adapter.notifyDataSetChanged();
                }

                @Override
                protected void onFailur(String msg) {
                    hideProgress();
                    progressPagination.setVisibility(View.GONE);

                    if (msg.equalsIgnoreCase("invalid token")) {
                        securePrefManager.clearAppsAllPrefs();
                        startActivity(new Intent(SGPromotionActivity.this, MainActivity.class));
                        finish();
                    } else
                        showToast(msg);
                }
            };

            api.hit(securePrefManager.getSharedValue(TOKEN), "#", 0, 0, "All");
        } else {
            showToast("Please check internet");
        }


        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });

    }

    private final RecyclerView.OnScrollListener recyclerScroller = new RecyclerView.OnScrollListener() {
        @SuppressWarnings("EmptyMethod")
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

            if (dy > 0)
                if (!isLoading) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition > 0) {
                        progressPagination.setVisibility(View.VISIBLE);
                        loadMoreItems(visibleItemCount + layoutManager.findFirstVisibleItemPosition());
                    }
                } else {

                }
        }

    };

    private void loadMoreItems(int pos) {
        isLoading = true;
        int page = (pos / 10);
        api.hit(securePrefManager.getSharedValue(TOKEN), "#", page, 0, filterValue.getText().toString().equalsIgnoreCase("Filter")?"All":filterValue.getText().toString());

    }

    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    view.requestFocus();
                    findViewById(R.id.labelLay).clearFocus();
                    hideKeyboard(SGPromotionActivity.this);
                    return false;
                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.refresh:
                if (Utils.hasNetwork(this)) {
                    showProgress("Please wait...");
                    isRefresh = true;
                    api.hit(securePrefManager.getSharedValue(TOKEN), "#", 0, 0, "All");

                } else {
                    showToast("Please check internet ");
                }
                break;
            case R.id.serach_btn:
                hideKeyboard(SGPromotionActivity.this);
                if (Utils.hasNetwork(this)) {
                    if (etMobileNumber.getText().toString().trim().length() == 0) {
                        showToast("Please enter mobile");
                        return;
                    } else {
                        showProgress("Please wait...");
                        etMobileNumber.clearFocus();
                        hideKeyboard(SGPromotionActivity.this);
                        api.hit(securePrefManager.getSharedValue(TOKEN), etMobileNumber.getText().toString(), 0, 0, "All");

                    }

                } else {
                    showToast("Please check Internet");
                }
                break;
            case R.id.sortValue:
            case R.id.lay1:
                showSortingDialog(this);
                break;
            case R.id.filterValue:
            case R.id.lay2:

                showAlertList(getResources().getStringArray(R.array.stateAll), R.string.state_title, filterValue);

//                showFilterDialog(getContext());
                break;

        }

    }


    private void showSortingDialog(Context context) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.sorting_layout, null);
        Animation slideUpIn;
        slideUpIn = AnimationUtils.loadAnimation(this, R.anim.slide_in_bottom);
        dialogView.startAnimation(slideUpIn);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);
        AlertDialog alertDialog = dialogBuilder.create();

        RecyclerView recyclerView = dialogView.findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.addItemDecoration(new DividerItemDecoration(context, 0));


        recyclerView.setAdapter(sortingAdapter);
        sortingAdapter.setOnClickListener((data, position) -> {

            pos=position;
            isFilter=true;
            api.hit(securePrefManager.getSharedValue(TOKEN),"#", 0, position, filterValue.getText().toString().equalsIgnoreCase("Filter")?"All":filterValue.getText().toString());
            handleSortClick(position);

            sortingAdapter.notifyDataSetChanged();
            alertDialog.dismiss();
        });


        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.BOTTOM;
        window.setAttributes(wlp);

        alertDialog.show();

    }

     public void showAlertList(final String[] dataArray, int title, final TextView textView) {
        hideKeyboard(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(title));
        builder.setItems(dataArray, (dialog, item) -> {
            textView.setText(dataArray[item]);
            isFilter=true;
            api.hit(securePrefManager.getSharedValue(TOKEN),"#", 0, pos, dataArray[item]);

            textView.clearFocus();
        });
        AlertDialog alerts = builder.create();
        alerts.show();
    }
    public void prepaerList() {

        SortingModel model1 = new SortingModel();

        model1.setSortingText(sortedValueArray[0]);
        model1.setSelected(true);
        sortedList.add(model1);


        for (int i = 1; i < sortedValueArray.length; i++) {
            SortingModel model = new SortingModel();
            model.setSelected(false);
            model.setSortingText(sortedValueArray[i]);
            sortedList.add(model);
        }


    }

    private void handleSortClick(int pos) {

        for (int i = 0; i < sortedList.size(); i++) {
            if (i == pos) {
                sortedList.get(i).setSelected(true);
            } else {
                sortedList.get(i).setSelected(false);
            }
        }

    }
    public class SortingAdapter extends BaseRecyclerAdapter<SortingModel, SortingAdapter.Holder> {


        public SortingAdapter(Context mContext) {
            super(mContext);
        }

        @Override
        public int getLayout(int viewType) {
            return R.layout.sort_layout_item;
        }

        @Override
        public Holder getViewHolder(View view, int viewType) {
            return new SortingAdapter.Holder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull SortingAdapter.Holder holder, int i) {
            holder.sortText.setText(getItem(i).sortingText);
            if (getItem(i).getSelected()) {
                holder.radioButton.setChecked(true);
            } else
                holder.radioButton.setChecked(false);

        }


        public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView sortText;
            RadioButton radioButton;

            public Holder(@NonNull View itemView) {
                super(itemView);
                sortText = itemView.findViewById(R.id.sortText);
                radioButton = itemView.findViewById(R.id.radio_btn);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                getListener().onClick(getItem(getAdapterPosition()), getAdapterPosition());
            }
        }
    }

    public class SortingModel {
        private String sortingText;
        private Boolean isSelected;


        public SortingModel() {

        }

        public String getSortingText() {
            return sortingText;
        }

        public void setSortingText(String sortingText) {
            this.sortingText = sortingText;
        }

        public Boolean getSelected() {
            return isSelected;
        }

        public void setSelected(Boolean selected) {
            isSelected = selected;
        }

    }
}
