package com.solarginni.CommonModule.Referral;

import android.content.Intent;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.solarginni.Base.BaseActivity;
import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.CommonModule.ReferralApi;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.ReferralModel;
import com.solarginni.R;
import com.solarginni.Utility.AppConstants;
import com.solarginni.Utility.Utils;

import static com.solarginni.Utility.AppConstants.MOBILE;
import static com.solarginni.Utility.AppConstants.dynamicUrl;

public class ReferralActivity  extends BaseActivity {
    private TextView referred,rank,dispursed,earned;
    private RecyclerView recyclerView;



    @Override
    public int getLayout() {
        return R.layout.prospect_refer;
    }

    @Override
    public void initViews() {
        recyclerView = findViewById(R.id.recyclerView);
        referred = findViewById(R.id.referred);
        rank = findViewById(R.id.rank);
        dispursed = findViewById(R.id.dispursed);
        earned = findViewById(R.id.earned);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new BaseRecyclerAdapter.DividerItemDecoration(this, RecyclerView.HORIZONTAL));

        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });

        findViewById(R.id.share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mobileNumber = securePrefManager.getSharedValue(MOBILE);
                if (mobileNumber.equalsIgnoreCase("")){
                    mobileNumber="+919501232200";
                }

                String content = "Hello Friend, I have used Solar Ginie for my Solar rooftop need and found it very interesting. Recommend you to download Solar Ginie from Android playstore " + dynamicUrl + "\n" + "Use my Promotion code " + mobileNumber.substring(3) + " during Registration to earn Bonus points";
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/html");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, content);
                if (sharingIntent.resolveActivity(getPackageManager()) != null)
                    startActivity(Intent.createChooser(sharingIntent, "Share Using"));
                else
                    showToast("Your device haven't any app to share this content");

            }
        });

    }

    @Override
    public void setUp() {

        if (Utils.hasNetwork(this)){
            showProgress("Please wait...");
            ReferralApi api = new ReferralApi() {
                @Override
                public void onComplete(CommomResponse response) {
                    hideProgress();
                    ReferralModel model = response.toResponseModel(ReferralModel.class);
                    rank.setText(""+model.rank);
                    rank.setText( String.valueOf(model.rank).equalsIgnoreCase("0")?" N.A ":String.valueOf(model.rank));
                    referred.setText(""+model.referContact);
                    earned.setText(" INR "+model.earned);
                    dispursed.setText(" INR "+model.dispursed);
                    if (model.referralModelList.size()==0){
                        findViewById(R.id.detailsLabel).setVisibility(View.GONE);
                    }
                    else {
                        findViewById(R.id.detailsLabel).setVisibility(View.VISIBLE);
                    }
                    ReferralAdapter adapter = new ReferralAdapter(ReferralActivity.this);
                    adapter.addAll(model.referralModelList);
                    recyclerView.setAdapter(adapter);

                }

                @Override
                protected void onFailur(String msg) {
                    hideProgress();
                    showToast(msg);

                }
            };
            api.hit(securePrefManager.getSharedValue(AppConstants.TOKEN));

        }
        else {
            showToast("Please check Internet");
        }





    }
}
