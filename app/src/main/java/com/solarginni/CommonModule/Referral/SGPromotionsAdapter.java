package com.solarginni.CommonModule.Referral;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.DBModel.SGPromotionalModel;
import com.solarginni.R;
import com.solarginni.Utility.Utils;

public class SGPromotionsAdapter extends BaseRecyclerAdapter<SGPromotionalModel.SgPromotionModelList, SGPromotionsAdapter.Holder> {


    public SGPromotionsAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public int getLayout(int viewType) {
        return R.layout.referred_item;
    }

    @Override
    public Holder getViewHolder(View view, int viewType) {
       return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {

        holder.amount.setText(""+getItem(i).amount);
        holder.phone.setText(": "+getItem(i).phone);
        holder.userName.setText(": "+getItem(i).name);
        holder.state.setText(": "+getItem(i).state);
        holder.status.setText(""+getItem(i).status);
        holder.referBy.setText(""+getItem(i).referredBy);
        holder.registeredOn.setText(""+ Utils.convertDate(getItem(i).registerDate));

    }

    public class Holder extends RecyclerView.ViewHolder{
        TextView phone,userName,state,status,amount,registeredOn,referBy;

        public Holder(@NonNull View itemView) {
            super(itemView);
            phone= itemView.findViewById(R.id.phone);
            userName= itemView.findViewById(R.id.userName);
            state= itemView.findViewById(R.id.state);
            status= itemView.findViewById(R.id.status);
            amount= itemView.findViewById(R.id.amount);
            referBy= itemView.findViewById(R.id.referredBy);
            registeredOn= itemView.findViewById(R.id.registeredOn);
            itemView.findViewById(R.id.dividerView).setVisibility(View.VISIBLE);
            itemView.findViewById(R.id.referView).setVisibility(View.VISIBLE);

        }
    }

}
