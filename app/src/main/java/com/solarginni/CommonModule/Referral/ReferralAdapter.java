package com.solarginni.CommonModule.Referral;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.DBModel.ReferralModel;
import com.solarginni.R;
import com.solarginni.Utility.Utils;

public class ReferralAdapter extends BaseRecyclerAdapter<ReferralModel.ReferralModelList, ReferralAdapter.Holder> {


    public ReferralAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public int getLayout(int viewType) {
        return R.layout.prospect_refer_item;
    }

    @Override
    public Holder getViewHolder(View view, int viewType) {
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {

        holder.amount.setText(""+getItem(i).amount);
        holder.phone.setText(": "+getItem(i).phone);
        holder.userName.setText(": "+getItem(i).name);
        holder.state.setText(": "+getItem(i).state);
        holder.status.setText(""+getItem(i).status);
        holder.registeredOn.setText(""+ Utils.convertDate(getItem(i).registerDate));


    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView phone,userName,state,status,amount,registeredOn;

        public Holder(@NonNull View itemView) {
            super(itemView);
            phone= itemView.findViewById(R.id.phone);
            userName= itemView.findViewById(R.id.userName);
            state= itemView.findViewById(R.id.state);
            status= itemView.findViewById(R.id.status);
            amount= itemView.findViewById(R.id.amount);
            registeredOn= itemView.findViewById(R.id.registeredOn);
        }
    }

}
