package com.solarginni.CommonModule;

import android.content.Intent;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.solarginni.Base.BaseActivity;
import com.solarginni.BuildConfig;
import com.solarginni.CommonModule.UserRoleModel.GetUserDetailPresenter;
import com.solarginni.CommonModule.UserRoleModel.GetUserDetailsContract;
import com.solarginni.CustomizeView.CustomAlertPopup;
import com.solarginni.CustomizeView.CustomDialogInterface;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.ProspectLandingModel;
import com.solarginni.GetDetails.Details;
import com.solarginni.GetDetails.ImplementerDetails;
import com.solarginni.Implementer.ImplemeterHomeActivity;
import com.solarginni.ProspectUser.ProspectHomeActivity;
import com.solarginni.R;
import com.solarginni.SGUser.SGModule.SGHomeActivity;
import com.solarginni.SGUser.SGModule.SGLandingModel;
import com.solarginni.Utility.AppConstants;
import com.solarginni.Utility.SecurePrefManager;
import com.solarginni.Utility.Utils;
import com.solarginni.user.SiteRegistrationActivity;
import com.solarginni.user.UserHomeActivity;
import com.solarginni.user.UserRegistration.CustomerRegisterActivity;

import static com.solarginni.Utility.AppConstants.BO_MANAGER;
import static com.solarginni.Utility.AppConstants.CUSTOMER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.GUEST_ROLE;
import static com.solarginni.Utility.AppConstants.IMPLEMENTER_DETAILS;
import static com.solarginni.Utility.AppConstants.IMPLEMENTER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.ISAPPVISITED;
import static com.solarginni.Utility.AppConstants.PROSPECT_ROLE_ID;
import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.SG_ADMIN;
import static com.solarginni.Utility.AppConstants.SUPER_IMPLEMENTER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.TOKEN;

/**
 * Created by Mohan on 24/1/20.
 */

public class SplashScreen extends BaseActivity implements GetUserDetailsContract.View {
    private static final int REQ_CODE_PLAY_STORE = 5;
    private SecurePrefManager securePrefManager;
    private String screenName;
    private TextView versionName;
    private Intent intent;
    private RelativeLayout transLay, reliableLay, bankLay;
    private GetUserDetailPresenter presenter;
    private VideoView videoView;
    ;


    @Override
    public int getLayout() {
        return R.layout.splash_layout;
    }

    @Override
    public void initViews() {

        versionName = findViewById(R.id.versionName);
        bankLay = findViewById(R.id.bankLay);
        reliableLay = findViewById(R.id.reliableLay);
        transLay = findViewById(R.id.transLay);
        videoView = findViewById(R.id.videoview);


    }

    @Override
    public void setUp() {

        versionName.setText("Version " + BuildConfig.VERSION_NAME);


    }

    @Override
    protected void onResume() {
        super.onResume();
        securePrefManager = new SecurePrefManager(this);

        Uri uri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.video);
        videoView.setVideoURI(uri);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            videoView.setAudioFocusRequest(AudioManager.AUDIOFOCUS_NONE);
        }


        videoView.start();

//        Animation slideUpIn, bankAnim, relAnim;
//        slideUpIn = AnimationUtils.loadAnimation(this, R.anim.slide_in_bottom);
//        bankAnim = AnimationUtils.loadAnimation(this, R.anim.slide_in_bottom);
//        relAnim = AnimationUtils.loadAnimation(this, R.anim.slide_in_bottom);
//
//        reliableLay.setVisibility(View.VISIBLE);
//        bankLay.setVisibility(View.VISIBLE);
//
//        transLay.startAnimation(slideUpIn);
//
//        slideUpIn.setAnimationListener(new Animation.AnimationListener() {
//            @Override
//            public void onAnimationStart(Animation animation) {
//                reliableLay.setVisibility(View.GONE);
//                bankLay.setVisibility(View.GONE);
//
//            }
//
//            @Override
//            public void onAnimationEnd(Animation animation) {
//                reliableLay.setVisibility(View.VISIBLE);
//                reliableLay.startAnimation(relAnim);
//
//
//                // HomeActivity.class is the activity to go after showing the splash screen.
//            }
//
//            @Override
//            public void onAnimationRepeat(Animation animation) {
//
//            }
//        });
//
//
//        relAnim.setAnimationListener(new Animation.AnimationListener() {
//            @Override
//            public void onAnimationStart(Animation animation) {
//                bankLay.setVisibility(View.GONE);
//            }
//
//            @Override
//            public void onAnimationEnd(Animation animation) {
//                bankLay.setVisibility(View.VISIBLE);
//                bankLay.startAnimation(bankAnim);
//
//
//                // HomeActivity.class is the activity to go after showing the splash screen.
//            }
//
//            @Override
//            public void onAnimationRepeat(Animation animation) {
//
//            }
//        });


        screenName = securePrefManager.getScreenValue();
        presenter = new GetUserDetailPresenter(this);
        presenter.GetVersion(getString(R.string.static_token), securePrefManager.getSharedValue(ROLE));


    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onPause() {
        super.onPause();
        videoView.pause();
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQ_CODE_PLAY_STORE) {
            presenter.GetVersion(getString(R.string.static_token), "");
        }
    }


    @Override
    public void onGetDetails(CommomResponse response, String role) {
        switch (role) {
            case CUSTOMER_ROLE_ID:
                /* 200 -> profile not complete , 201 -> site not complete, 202 -> data available */
                Details details = response.toResponseModel(Details.class);
                if (details.getStatus() == 201) {
                    Bundle bundle = new Bundle();
                    bundle.putString("isFrom", "site");
                    Intent intent = new Intent(this, SiteRegistrationActivity.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    finish();


                } else {
                    Intent intent = new Intent(this, UserHomeActivity.class);
                    Bundle bundle = new Bundle();
                    securePrefManager.storeSharedValue(AppConstants.NOTIFICATION_COUNT, String.valueOf(details.getNotificationCount()));
                    bundle.putSerializable("LandingData", details);
                    bundle.putBoolean("testimonials", details.testimonialCreated);
                    bundle.putString("for", "landing");
                    int flag = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;
                    intent.setFlags(flag);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    finish();
                }

                break;

            case IMPLEMENTER_ROLE_ID:
            case SUPER_IMPLEMENTER_ROLE_ID:
                ImplementerDetails implementerDetails = response.toResponseModel(ImplementerDetails.class);
                securePrefManager.storeSharedValue(AppConstants.NOTIFICATION_COUNT, String.valueOf(implementerDetails.getNotificationCount()));
                securePrefManager.storeSharedValue(IMPLEMENTER_DETAILS, Utils.toString(implementerDetails));
                Intent intent = new Intent(this, ImplemeterHomeActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("LandingData", implementerDetails);
                bundle.putBoolean("testimonials", implementerDetails.testimonialCreated);
                bundle.putString("for", "landing");
                int flag = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;
                intent.setFlags(flag);
                intent.putExtras(bundle);
                startActivity(intent);

                //    }
                break;

            case PROSPECT_ROLE_ID:

                ProspectLandingModel prospectLandingModel = response.toResponseModel(ProspectLandingModel.class);

                securePrefManager.storeSharedValue(AppConstants.PROSPECT_LANDIND, Utils.toString(prospectLandingModel));

                intent = new Intent(this, ProspectHomeActivity.class);
                bundle = new Bundle();
                bundle.putString("for", "landing");
                flag = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;
                intent.setFlags(flag);
                intent.putExtras(bundle);
                startActivity(intent);


                break;
            case GUEST_ROLE:
                intent = new Intent(this, ProspectHomeActivity.class);
                flag = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;
                intent.setFlags(flag);
                startActivity(intent);
                break;
            case SG_ADMIN:
            case BO_MANAGER:

                hideProgress();
                SGLandingModel model = response.toResponseModel(SGLandingModel.class);
                securePrefManager.storeSharedValue(AppConstants.NOTIFICATION_COUNT, String.valueOf(model.data.notificationCount));

                bundle = new Bundle();
                bundle.putSerializable("LandingData", model);
                bundle.putString("for", "landing");
                flag = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;
                goToNextScreen(SGHomeActivity.class, bundle, flag);


                break;
        }

    }

    @Override
    public void onFailure(String msg, int code) {
        if (msg.equalsIgnoreCase("invalid token")) {
            securePrefManager.clearAppsAllPrefs();
            startActivity(new Intent(SplashScreen.this, MainActivity.class));
            finish();
        } else {
            if (Utils.hasNetwork(this)) {
                if (code == 0) {
                    errorDialog("Internal Server Error,Please try again after sometime");
                } else {
                    errorDialog(msg);

                }
            } else {
                errorDialog("Please Check Internet Connection");
            }

        }


    }

    @Override
    public void onGetVersion(CommomResponse response) {

        VrsionModel model = response.toResponseModel(VrsionModel.class);

        if (Integer.parseInt(model.version) > BuildConfig.VERSION_CODE) {

            showUpdateAppDialog(model.critical);

        } else {
            enterInApplication();
        }


    }

    public void showUpdateAppDialog(final boolean isCriticalUpdate) {
        CustomAlertPopup.Builder builder = new CustomAlertPopup.Builder(this);
        builder.setMessage("You have an Update , Please update app");
        builder.setPositiveButton("Update", new CustomDialogInterface.OnClickListener() {
            @Override
            public void onClick() {
                openPlayStoreForAppUpdate();
            }
        });
        if (!isCriticalUpdate) {
            builder.setNegativeButton(R.string.text_cancel, new CustomDialogInterface.OnClickListener() {
                @Override
                public void onClick() {
                    enterInApplication();
                }
            });
        }
        builder.setCancelable(false);
        builder.show(Gravity.TOP);
    }

    private void enterInApplication() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (securePrefManager.getBooleanKeyValue(AppConstants.ISVISITED)) {

                    if (securePrefManager.getBooleanKeyValue(ISAPPVISITED) && !securePrefManager.getSharedValue(ROLE).trim().isEmpty()) {


                        if (securePrefManager.getSharedValue(AppConstants.REGISTERED).equalsIgnoreCase("true")) {
                            switch (securePrefManager.getSharedValue(ROLE)) {
                                case CUSTOMER_ROLE_ID:
                                    presenter.GetDetails(securePrefManager.getSharedValue(TOKEN), CUSTOMER_ROLE_ID);

                                    break;
                                case IMPLEMENTER_ROLE_ID:
                                case SUPER_IMPLEMENTER_ROLE_ID:
                                    presenter.GetDetails(securePrefManager.getSharedValue(TOKEN), IMPLEMENTER_ROLE_ID);
                                    break;
                                case PROSPECT_ROLE_ID:
                                    presenter.GetDetails(securePrefManager.getSharedValue(TOKEN), PROSPECT_ROLE_ID);
                                    break;

                                case SG_ADMIN:
                                case BO_MANAGER:
                                    presenter.GetDetails(securePrefManager.getSharedValue(TOKEN), SG_ADMIN);
                                    break;
                            }
                        }

                        else {
                            Bundle bundle1 = new Bundle();
                            bundle1.putInt("role", 7);
                            int flag = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;
                            goToNextScreen(CustomerRegisterActivity.class, bundle1, flag);

                        }

                    }

                    else if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(GUEST_ROLE)){
                        intent = new Intent(SplashScreen.this, ProspectHomeActivity.class);
                        int flag = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;
                        intent.setFlags(flag);
                        startActivity(intent);
                    }
                    else {
                        switch (screenName) {
                            case "Main":
                                intent = new Intent(SplashScreen.this, MainActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                break;

                            case "Site Registration":
                                intent = new Intent(SplashScreen.this, UserHomeActivity.class);
                                intent.putExtra("isFrom", "site");
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                break;

                            case "SelectRole":
                                intent = new Intent(SplashScreen.this, SelectRoleActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                break;
                            case "LandingPage":
                                intent = new Intent(SplashScreen.this, UserHomeActivity.class);
                                intent.putExtra("isFrom", "LandingPage");
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                break;
                        }

                    }

                } else {
                    startActivity(new Intent(SplashScreen.this, TutorialActivity.class));
                    finish();

                }


            }
        }, 7000);
    }

    /**
     * Open play store.
     */
    private void openPlayStoreForAppUpdate() {
        final Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("market://details?id=" + BuildConfig.APPLICATION_ID));
        try {
            startActivityForResult(intent, REQ_CODE_PLAY_STORE);
        } catch (final Exception e) {
            intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID));
            startActivityForResult(intent, REQ_CODE_PLAY_STORE);
        }
    }

    private void errorDialog(String msg) {
        new CustomAlertPopup.Builder(this).setMessage(msg).setPositiveButton(R.string.text_ok, new CustomDialogInterface.OnClickListener() {
            @Override
            public void onClick() {
                finish();
            }
        }).setCancelable(false).show(Gravity.TOP);
    }

    public class VrsionModel {


        @SerializedName("version")
        @Expose
        public String version;
        @SerializedName("critical")
        @Expose
        public Boolean critical;


    }
}
