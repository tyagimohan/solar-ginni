package com.solarginni.CommonModule;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.solarginni.Base.BaseActivity;
import com.solarginni.R;
import com.solarginni.user.UserRegistration.CustomerRegisterActivity;

public class SelectCustomerRole extends BaseActivity implements View.OnClickListener {

    private TextView tvExistingCusotmer, tvProspect;
    private int roleType;

    @Override
    public int getLayout() {
        return R.layout.customer_role;
    }

    @Override
    public void initViews() {

        tvExistingCusotmer = findViewById(R.id.existing);
        tvProspect = findViewById(R.id.prospect);

    }

    @Override
    public void setUp() {
        tvExistingCusotmer.setOnClickListener(this);
        tvProspect.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.existing:
                roleType = 7;
                Bundle bundle = new Bundle();
                bundle.putInt("role", roleType);
                int flag = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;
                goToNextScreen(CustomerRegisterActivity.class, bundle, flag);


                break;
            case R.id.prospect:
                roleType = 3;
                Bundle bundle1 = new Bundle();
                bundle1.putInt("role", roleType);
                flag = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;

                goToNextScreen(CustomerRegisterActivity.class, bundle1,flag);
                break;
        }

    }
}
