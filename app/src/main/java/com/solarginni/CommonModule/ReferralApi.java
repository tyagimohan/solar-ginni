package com.solarginni.CommonModule;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;
import com.solarginni.network.RestClient;

import retrofit2.Retrofit;

public abstract class ReferralApi {

    private Retrofit retrofit;
    private ApiInterface mApiInterface;

    public ReferralApi( ) {
        retrofit= RestClient.getRetrofitBuilder();
        mApiInterface= retrofit.create(ApiInterface.class);
    }

    public void hit(String token){
        mApiInterface.getReferralDeails(token).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                onComplete(response);

            }

            @Override
            public void onError(ApiError error) {
                onFailur(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                onFailur(throwable.getMessage());
            }
        });
    }

    public abstract void onComplete(CommomResponse response);
    protected abstract void onFailur(String msg);
}
