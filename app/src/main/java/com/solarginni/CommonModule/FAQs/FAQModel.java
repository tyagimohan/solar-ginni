package com.solarginni.CommonModule.FAQs;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FAQModel implements Serializable {

    @SerializedName("companies")
    public  List<FAQS> faqsList= new ArrayList();


    public class FAQS{
        @SerializedName("question")
        public String header;
        @SerializedName("answer")
        public String desc;


    }

}
