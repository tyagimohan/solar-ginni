package com.solarginni.CommonModule.FAQs;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.TextView;

import com.github.aakira.expandablelayout.ExpandableLayoutListenerAdapter;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.github.aakira.expandablelayout.Utils;
import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.R;
import com.solarginni.Utility.MyTagHandler;

public class FAQsAdapter extends BaseRecyclerAdapter<FAQModel.FAQS, FAQsAdapter.Holder> {
    private SparseBooleanArray expandState = new SparseBooleanArray();

    public FAQsAdapter(Context mContext, int size) {
        super(mContext);
        for (int i = 0; i < size; i++) {
            expandState.append(i, false);
        }
    }

    @Override
    public int getLayout(int viewType) {
        return R.layout.faq_screen;
    }

    @Override
    public Holder getViewHolder(View view, int viewType) {
        return new Holder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        holder.textView.setText(getItem(i).header);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            holder.textView2.setText(Html.fromHtml(getItem(i).desc, Html.FROM_HTML_MODE_LEGACY, null, new MyTagHandler()));
        } else {
            holder.textView2.setText(Html.fromHtml(getItem(i).desc, null, new MyTagHandler()));
        }
        holder.setIsRecyclable(false);
        holder.expandableLayout.setInRecyclerView(true);
        holder.expandableLayout.setInterpolator(Utils.createInterpolator(Utils.ACCELERATE_INTERPOLATOR));
        holder.expandableLayout.setExpanded(expandState.get(i));

        holder.expandableLayout.setListener(new ExpandableLayoutListenerAdapter() {
            @Override
            public void onPreOpen() {
                createRotateAnimator(holder.arrow, 0f, 180f).start();
                expandState.put(i, true);
            }

            @Override
            public void onPreClose() {
                createRotateAnimator(holder.arrow, 180f, 0f).start();
                expandState.put(i, false);
            }
        });
        holder.arrow.setRotation(expandState.get(i) ? 180f : 0f);



    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView textView, textView2;

        public View arrow;
        public ExpandableLinearLayout expandableLayout;


        public Holder(@NonNull View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.textView);
            textView2 = (TextView) itemView.findViewById(R.id.text2);
            arrow = (View) itemView.findViewById(R.id.arrow);
            expandableLayout = (ExpandableLinearLayout) itemView.findViewById(R.id.expandableLayout);
            textView.setOnClickListener(this);
            itemView.findViewById(R.id.button).setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.textView:
                case R.id.button:
                    expandableLayout.toggle();
                    break;

            }

        }
    }

    public ObjectAnimator createRotateAnimator(final View target, final float from, final float to) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(target, "rotation", from, to);
        animator.setDuration(300);
        animator.setInterpolator(Utils.createInterpolator(Utils.LINEAR_INTERPOLATOR));
        return animator;
    }
}
