package com.solarginni.CommonModule.FAQs;

import android.content.Context;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;
import com.solarginni.network.RestClient;

import retrofit2.Retrofit;

public abstract  class FAQsApi {
    private Retrofit retrofit;
    private ApiInterface mApiInterface;

    public FAQsApi( ) {
        retrofit= RestClient.getRetrofitBuilder();
        mApiInterface= retrofit.create(ApiInterface.class);
    }

    public void hit(String token){
        mApiInterface.fetcFAQs(token).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                onComplete(response);

            }

            @Override
            public void onError(ApiError error) {
                onFailur(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                onFailur(throwable.getMessage());
            }
        });
    }



    abstract void onComplete(CommomResponse response);
     abstract void onFailur(String msg);

}
