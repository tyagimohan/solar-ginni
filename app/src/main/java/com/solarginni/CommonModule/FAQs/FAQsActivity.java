package com.solarginni.CommonModule.FAQs;

import com.solarginni.Base.BaseActivity;
import com.solarginni.R;

public class FAQsActivity extends BaseActivity {
    @Override
    public int getLayout() {
        return R.layout.faqs_activity;
    }

    @Override
    public void initViews() {

        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("");
        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });


    }

    @Override
    public void setUp() {

        loadFragment(new FAQsFragment(), null, FAQsFragment.class.getSimpleName());

    }
}
