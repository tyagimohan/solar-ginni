package com.solarginni.CommonModule.FAQs;

import android.content.Intent;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.solarginni.Base.BaseFragment;
import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.CommonModule.MainActivity;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.R;
import com.solarginni.Utility.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.solarginni.Utility.AppConstants.TOKEN;

public class FAQsFragment extends BaseFragment {
    private RecyclerView recyclerView;
    List<FAQModel.FAQS> data;

    @Override
    public int getLayout() {
        return R.layout.faqs_layout;
    }

    @Override
    public void initViews(View view) {
        recyclerView = view.findViewById(R.id.recyclerView);

    }

    @Override
    public void setUp() {

        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new BaseRecyclerAdapter.DividerItemDecoration(getContext(), RecyclerView.VERTICAL));


        if (Utils.hasNetwork(getContext())) {
            showProgress("Please wait");
            FAQsApi api = new FAQsApi() {
                @Override
                void onComplete(CommomResponse response) {
                    hideProgress();
                    data = new ArrayList<>();
                    FAQModel model = response.toResponseModel(FAQModel.class);
                    data.addAll(model.faqsList);
                    FAQsAdapter adapter = new FAQsAdapter(getContext(), data.size());
                    adapter.addAll(data);
                    recyclerView.setAdapter(adapter);

                }

                @Override
                void onFailur(String msg) {

                    hideProgress();
                    if (msg.equalsIgnoreCase("invalid token")){
                        securePrefManager.clearAppsAllPrefs();
                        startActivity(new Intent(getActivity(),MainActivity.class));
                       getActivity().finish();
                    }
                    else
                        showToast(msg);

                }
            };
            api.hit(securePrefManager.getSharedValue(TOKEN));
        } else {
            showToast("Please check Internet");

        }


    }
}
