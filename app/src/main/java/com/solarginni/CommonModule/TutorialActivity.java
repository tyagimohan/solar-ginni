package com.solarginni.CommonModule;

import android.content.Intent;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.solarginni.Base.BaseActivity;
import com.solarginni.R;
import com.solarginni.Utility.AppConstants;

public class TutorialActivity extends BaseActivity {

    private ViewPager mViewPager;
    private TabLayout tabLayout;
    private Button getStarted;
    private ViewPagerAdapter adapter;
    private TextView skipBtn;
    private int pos=0;


    @Override
    public int getLayout() {
        return R.layout.activity_tutorial;
    }

    @Override
    public void initViews() {
        mViewPager = findViewById(R.id.vpPlaceholder);
        tabLayout = findViewById(R.id.tabLayout);
        getStarted = findViewById(R.id.getStarted);
        skipBtn = findViewById(R.id.skipBtn);

        adapter = new ViewPagerAdapter(getSupportFragmentManager());

        getStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pos==4){
                    securePrefManager.storeBooleanValue(AppConstants.ISVISITED,true);
                    startActivity(new Intent(TutorialActivity.this, MainActivity.class));
                    finish();
                }
                else {
                    mViewPager.setCurrentItem(pos+1);
                }


            }
        });
        skipBtn.setOnClickListener(v -> {
            securePrefManager.storeBooleanValue(AppConstants.ISVISITED,true);
            startActivity(new Intent(TutorialActivity.this, MainActivity.class));
            finish();
        });
    }

    @Override
    public void setUp() {
        mViewPager.setAdapter(adapter);
        mViewPager.addOnPageChangeListener(adapter);
        mViewPager.setOffscreenPageLimit(2);
        tabLayout.setupWithViewPager(mViewPager);



    }

    public class ViewPagerAdapter extends FragmentStatePagerAdapter implements ViewPager.OnPageChangeListener {



        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);

        }



        @Override
        public Fragment getItem(int position) {



            return  TutorialFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            return getResources().obtainTypedArray(R.array.array_tutorial_images).length(); //3 items
        }

        @Override
        public void onPageScrolled(int i, float v, int i1) {


        }

        @Override
        public void onPageSelected(int i) {
            pos= i;
              if (i==4){
                  getStarted.setVisibility(View.VISIBLE);
                  getStarted.setText("GET STARTED");
                  tabLayout.setVisibility(View.VISIBLE);
                  skipBtn.setVisibility(View.GONE);
              }
              else {
                  getStarted.setVisibility(View.VISIBLE);
                  getStarted.setText("NEXT");
                  skipBtn.setVisibility(View.VISIBLE);

                  tabLayout.setVisibility(View.VISIBLE);

              }
        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    }

}
