package com.solarginni.CommonModule.Testimonial;

import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.gson.annotations.SerializedName;
import com.solarginni.Base.BaseActivity;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.R;

import java.io.Serializable;

import static com.solarginni.Utility.AppConstants.TOKEN;

public class AddTestimonialActivity extends BaseActivity {
    private ImageView back;
    private Button submit;
    private EditText comment;

    @Override
    public int getLayout() {
        return R.layout.create_testimonial_layout;
    }

    @Override
    public void initViews() {
        back= findViewById(R.id.back);
        submit= findViewById(R.id.submit);
        comment= findViewById(R.id.comment);


        comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                for(int i = s.length()-1; i >= 0; i--){
                    if(s.charAt(i) == '\n'){
                        s.delete(i, i + 1);
                        return;
                    }
                }
            }
        });


        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("");
        toolbar.setTitle("");
        back.setOnClickListener(v -> {
            finish();
        });

    }

    @Override
    public void setUp() {

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(AddTestimonialActivity.this);
                if (comment.getText().toString().trim().length()==0){
                    showToast("Add your comment");
                }
                else {
                    showProgress("Please wait...");
                    TestimonialModel model = new TestimonialModel(comment.getText().toString(),1);

                    AddTestimonialAPI addTestimonialAPI= new AddTestimonialAPI() {
                        @Override
                        public void onComplete(CommomResponse response) {
                            hideProgress();
                            showToast(response.getMessage());
                            setResult(Activity.RESULT_OK);
                            finish();

                        }

                        @Override
                        protected void onFailur(String msg) {
                            hideProgress();
                            showToast(msg);

                        }
                    };
                    addTestimonialAPI.hit(securePrefManager.getSharedValue(TOKEN),model);
                }
            }
        });


    }

    public class TestimonialModel implements Serializable {

        public TestimonialModel(String comment, int rating) {
            this.comment = comment;
            this.rating = rating;
        }

        @SerializedName("comment")
        public String comment;
        @SerializedName("rating")
        public int rating;
    }

}
