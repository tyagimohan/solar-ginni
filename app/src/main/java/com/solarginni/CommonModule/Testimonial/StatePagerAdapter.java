package com.solarginni.CommonModule.Testimonial;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.solarginni.DBModel.Testimonial;

import java.util.List;

public class StatePagerAdapter extends FragmentStatePagerAdapter {

    private List<Testimonial> testimonials;

    public StatePagerAdapter(FragmentManager fm, List<Testimonial> testimonials) {
        super(fm);
        this.testimonials = testimonials;

    }

    @Override
    public Fragment getItem(int position) {
        return new TestimonialFragment().newInstance(testimonials.get(position));
    }

    @Override
    public int getCount() {
        return testimonials.size();
    }




}