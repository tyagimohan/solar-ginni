package com.solarginni.CommonModule.Testimonial;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.solarginni.DBModel.Testimonial;
import com.solarginni.R;

public class TestimonialFragment extends Fragment {

    private Testimonial testimonial;
    private View mLeak;




    public  TestimonialFragment newInstance(Testimonial testimonial) {

        Bundle args = new Bundle();
        args.putSerializable("arg_player", testimonial);
        TestimonialFragment fragment = new TestimonialFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public TestimonialFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            this.testimonial = (Testimonial) args.getSerializable("arg_player");
        }
        if (testimonial == null) {
            throw new IllegalArgumentException("Player list can not be null");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        mLeak = inflater.inflate(R.layout.testimonial_yout, container, false);
        return mLeak;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mLeak = null;

    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ImageView imageView = view.findViewById(R.id.profile_image);
        ((TextView) view.findViewById(R.id.textComment)).setText(testimonial.comment);
        ((TextView) view.findViewById(R.id.name)).setText(testimonial.name);


        try {
            if (!testimonial.image.isEmpty())
                Glide.with(this).load(testimonial.image).into(imageView);
        } catch (Exception e) {
            e.printStackTrace();

        }

    }

}



