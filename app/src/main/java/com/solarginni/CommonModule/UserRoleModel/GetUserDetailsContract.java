package com.solarginni.CommonModule.UserRoleModel;

import com.solarginni.DBModel.CommomResponse;

public interface GetUserDetailsContract {

    interface View {
        void onGetDetails(CommomResponse response,String role);
        void onFailure(String msg,int code);
        void onGetVersion(CommomResponse response);

    }

    interface Presenter {
        void GetDetails(String token,String role);
        void GetVersion(String token,String role);

    }
    interface Interactor {
        void GetDetails(String token,String role,OnInteraction listener);
        void GetVersion(String token,String role,OnInteraction listener);

    }
    interface OnInteraction {
        void onGetDetails(CommomResponse response,String role);
        void onFailure(String msg,int code);

        void onGetVersion(CommomResponse response);
    }
}
