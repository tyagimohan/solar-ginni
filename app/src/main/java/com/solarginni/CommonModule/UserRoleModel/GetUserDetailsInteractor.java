package com.solarginni.CommonModule.UserRoleModel;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;

import retrofit2.Retrofit;

import static com.solarginni.Utility.AppConstants.BO_MANAGER;
import static com.solarginni.Utility.AppConstants.CUSTOMER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.IMPLEMENTER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.PROSPECT_ROLE_ID;
import static com.solarginni.Utility.AppConstants.SG_ADMIN;
import static com.solarginni.Utility.AppConstants.SUPER_IMPLEMENTER_ROLE_ID;

public class GetUserDetailsInteractor implements GetUserDetailsContract.Interactor {

    private Retrofit mRetrofit;
    private ApiInterface mApiInterface;

    public GetUserDetailsInteractor(Retrofit retrofit) {
        this.mRetrofit = retrofit;
        mApiInterface = mRetrofit.create(ApiInterface.class);
    }

    @Override
    public void GetDetails(String token, String role, GetUserDetailsContract.OnInteraction listner) {
        switch (role) {
            case CUSTOMER_ROLE_ID:
                mApiInterface.getCustomerDetails(token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                    @Override
                    public void onSuccess(CommomResponse response) {
                        listner.onGetDetails(response, role);
                    }

                    @Override
                    public void onError(ApiError error) {
                        listner.onFailure(error.getMessage(),error.getStatusCode());

                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        listner.onFailure(throwable.getMessage(),0);

                    }
                });
                break;


            case IMPLEMENTER_ROLE_ID:
            case SUPER_IMPLEMENTER_ROLE_ID:
                mApiInterface.getImplementer(token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                    @Override
                    public void onSuccess(CommomResponse response) {
                        listner.onGetDetails(response,role);

                    }

                    @Override
                    public void onError(ApiError error) {
                        listner.onFailure(error.getMessage(),error.getStatusCode());
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        listner.onFailure(throwable.getMessage(),0);

                    }
                });
                break;

            case PROSPECT_ROLE_ID:
                mApiInterface.getProspectLandingDetails(token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                    @Override
                    public void onSuccess(CommomResponse response) {

                        listner.onGetDetails(response,role);
                    }

                    @Override
                    public void onError(ApiError error) {
                        listner.onFailure(error.getMessage(),error.getStatusCode());

                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        listner.onFailure(throwable.getMessage(),0);

                    }
                });
                break;
            case SG_ADMIN:
            case BO_MANAGER:
                mApiInterface.getSGLading(token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                    @Override
                    public void onSuccess(CommomResponse response) {

                        listner.onGetDetails(response, role);
                    }

                    @Override
                    public void onError(ApiError error) {
                        listner.onFailure(error.getMessage(),error.getStatusCode());

                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        listner.onFailure(throwable.getMessage(),0);

                    }
                });
                break;
        }

    }

    @Override
    public void GetVersion(String token, String role, GetUserDetailsContract.OnInteraction listener) {
        mApiInterface.getVersion(token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                listener.onGetVersion(response);

            }

            @Override
            public void onError(ApiError error) {
                listener.onFailure(error.getMessage(),error.getStatusCode());

            }

            @Override
            public void onFailure(Throwable throwable) {
                  listener.onFailure(throwable.getMessage(),0);
            }
        });

    }
}
