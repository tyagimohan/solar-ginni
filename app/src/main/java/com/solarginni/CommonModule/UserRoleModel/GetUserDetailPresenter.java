package com.solarginni.CommonModule.UserRoleModel;

import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.RestClient;

public class GetUserDetailPresenter implements GetUserDetailsContract.OnInteraction,GetUserDetailsContract.Presenter {

    private GetUserDetailsContract.View view;
    private GetUserDetailsInteractor interactor;

    public GetUserDetailPresenter(GetUserDetailsContract.View view) {
        this.view = view;
        interactor = new GetUserDetailsInteractor(RestClient.getRetrofitBuilder());
    }

    @Override
    public void GetDetails(String token,String role) {
        interactor.GetDetails(token,role,this);

    }

    @Override
    public void GetVersion(String token, String role) {
        interactor.GetVersion(token,role,this);

    }

    @Override
    public void onGetDetails(CommomResponse response, String role) {
        view.onGetDetails(response,role);

    }

    @Override
    public void onFailure(String msg,int code) {
        view.onFailure(msg,code);

    }

    @Override
    public void onGetVersion(CommomResponse response) {
        view.onGetVersion(response);

    }
}
