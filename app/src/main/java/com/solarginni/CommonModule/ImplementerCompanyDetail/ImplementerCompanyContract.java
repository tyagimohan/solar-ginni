package com.solarginni.CommonModule.ImplementerCompanyDetail;

import com.solarginni.CommonModule.ImplementerCompanyModel;

public interface ImplementerCompanyContract {

    interface View {
        void onGetImplementerCompany(ImplementerCompanyModel details);
        void onFailure(String msg);
    }

    interface Presenter{
        void getImplementerCompany(String token);
    }
    interface Interactor{
        void getImplementerCompany(String token, OnInteraction listener);
    }
    interface OnInteraction{
        void onGetImplementerCompany(ImplementerCompanyModel details);
        void onFailure(String msg);

    }
}
