package com.solarginni.CommonModule.ImplementerCompanyDetail;

import com.solarginni.CommonModule.ImplementerCompanyModel;
import com.solarginni.network.RestClient;

public class ImplementerCompanyPresenter implements ImplementerCompanyContract.Presenter,ImplementerCompanyContract.OnInteraction {
     private ImplementerCompanyContract.View view;
     private ImplementerCompanyInteractor interactor;

    public ImplementerCompanyPresenter(ImplementerCompanyContract.View view) {
        this.view = view;
        interactor= new ImplementerCompanyInteractor(RestClient.getRetrofitBuilder());
    }

    @Override
    public void getImplementerCompany(String token) {

        interactor.getImplementerCompany(token,this);
    }

    @Override
    public void onGetImplementerCompany(ImplementerCompanyModel details) {
          view.onGetImplementerCompany(details);
    }

    @Override
    public void onFailure(String msg) {
        view.onFailure(msg);

    }
}
