package com.solarginni.CommonModule.ImplementerCompanyDetail;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.CommonModule.ImplementerCompanyModel;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;

import retrofit2.Retrofit;

public class ImplementerCompanyInteractor implements ImplementerCompanyContract.Interactor {

    private Retrofit mRetrofit;
    private ApiInterface apiInterface;

    public ImplementerCompanyInteractor(Retrofit mRetrofit) {
        this.mRetrofit = mRetrofit;
        apiInterface = mRetrofit.create(ApiInterface.class);
    }

    @Override
    public void getImplementerCompany(String token, ImplementerCompanyContract.OnInteraction listener) {
        apiInterface.getCompanyDetails(token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
            @Override
            public void onSuccess(CommomResponse commomResponse) {
                ImplementerCompanyModel details = commomResponse.toResponseModel(ImplementerCompanyModel.class);

                listener.onGetImplementerCompany(details);

            }

            @Override
            public void onError(ApiError error) {
                listener.onFailure(error.getMessage());
            }

            @Override
            public void onFailure(Throwable throwable) {
                listener.onFailure(throwable.getMessage());
            }
        });

    }
}
