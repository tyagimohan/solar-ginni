package com.solarginni.CommonModule;

import android.util.Log;

import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.solarginni.Base.BaseActivity;
import com.solarginni.R;

import java.util.Arrays;

public class PlaceAutoCompleteActivity extends BaseActivity {
    @Override
    public int getLayout() {
        return R.layout.place_autocomplete;
    }

    @Override
    public void initViews() {

    }

    @Override
    public void setUp() {

        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);

        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ADDRESS_COMPONENTS, Place.Field.NAME, Place.Field.LAT_LNG));

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {

                Log.e("PlaceAutoComplete", "" + place.getName());
                // TODO: Get info about the selected place.
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.e("PlaceAutoComplete", "" + status.getStatusMessage());

            }
        });
    }

}


