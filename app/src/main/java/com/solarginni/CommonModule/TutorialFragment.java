package com.solarginni.CommonModule;

import android.content.res.TypedArray;
import android.os.Bundle;
import androidx.appcompat.widget.AppCompatImageView;
import android.view.View;

import com.bumptech.glide.Glide;
import com.solarginni.Base.BaseFragment;
import com.solarginni.R;

public class TutorialFragment extends BaseFragment {
    public static final String KEY_POSITION = "key_position";
    private int position = 0;
    private AppCompatImageView ivTutorial;
    @Override
    public int getLayout() {
        return R.layout.tutorial_fragment;
    }

    public static TutorialFragment newInstance(final int position) {

        Bundle args = new Bundle();
        args.putInt(KEY_POSITION, position);
        TutorialFragment fragment = new TutorialFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void initViews(View view) {
        ivTutorial = view.findViewById(R.id.imageView);

    }

    @Override
    public void setUp() {

        position = getArguments().getInt(KEY_POSITION, 0);
        final TypedArray arrayImages = getResources().obtainTypedArray(R.array.array_tutorial_images);

        Glide.with(this)
                .load(arrayImages.getDrawable(position))
                .into(ivTutorial);



        arrayImages.recycle();
    }
}
