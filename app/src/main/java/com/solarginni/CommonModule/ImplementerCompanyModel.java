package com.solarginni.CommonModule;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.solarginni.DBModel.CompanyMakeModel;

import java.io.Serializable;
import java.util.List;

public class ImplementerCompanyModel implements Serializable {

    @SerializedName("companies")
    @Expose
    private List<CompanyMakeModel> companies = null;

    public List<CompanyMakeModel> getCompanies() {
        return companies;
    }

    public void setCompanies(List<CompanyMakeModel> companies) {
        this.companies = companies;
    }


   public static class Company implements Serializable{
       @SerializedName("id")
       @Expose
       private Integer id;
       @SerializedName("userId")
       @Expose
       private String userId;

       public Integer getId() {
           return id;
       }

       public void setId(Integer id) {
           this.id = id;
       }

       public String getCompanyName() {
           return companyName;
       }

       public void setCompanyName(String companyName) {
           this.companyName = companyName;
       }

       @SerializedName("companyName")
       @Expose
       private String companyName;

       public String getUserId() {
           return userId;
       }

       public void setUserId(String userId) {
           this.userId = userId;
       }
   }

}
