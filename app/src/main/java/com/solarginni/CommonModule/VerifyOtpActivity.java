package com.solarginni.CommonModule;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.text.util.Linkify;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.solarginni.Base.BaseActivity;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.OtpModel;
import com.solarginni.DBModel.ProspectLandingModel;
import com.solarginni.GetDetails.Details;
import com.solarginni.GetDetails.ImplementerDetails;
import com.solarginni.Implementer.ImplemeterHomeActivity;
import com.solarginni.ProspectUser.ProspectHomeActivity;
import com.solarginni.R;
import com.solarginni.SGUser.SGModule.SGHomeActivity;
import com.solarginni.SGUser.SGModule.SGLandingModel;
import com.solarginni.SiteLovsModule.SiteDetailModel.SiteLov;
import com.solarginni.Utility.AppConstants;
import com.solarginni.Utility.Utils;
import com.solarginni.VerifyModule.VerifyContract;
import com.solarginni.VerifyModule.VerifyPresenter;
import com.solarginni.user.SiteRegistrationActivity;
import com.solarginni.user.UserHomeActivity;
import com.solarginni.user.UserRegistration.CustomerRegisterActivity;

import java.util.regex.Pattern;

import static com.solarginni.Utility.AppConstants.BO_MANAGER;
import static com.solarginni.Utility.AppConstants.CUSTOMER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.GUEST_ROLE;
import static com.solarginni.Utility.AppConstants.IMPLEMENTER_DETAILS;
import static com.solarginni.Utility.AppConstants.IMPLEMENTER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.MOBILE;
import static com.solarginni.Utility.AppConstants.PROSPECT_ROLE_ID;
import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.SG_ADMIN;
import static com.solarginni.Utility.AppConstants.SUPER_IMPLEMENTER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.TOKEN;


public class VerifyOtpActivity extends BaseActivity implements View.OnClickListener, VerifyContract.View {
    private Button confirmBtn;
    private TextView tvOtpLablel, resendOtp;
    private EditText etPassword;
    private String mobileNumber;
    private VerifyPresenter presenter;
    private TextView tvTNC;


    @Override
    public int getLayout() {
        return R.layout.activity_verify_otp;
    }

    @Override
    public void initViews() {

        confirmBtn = findViewById(R.id.confirmBtn);
        tvOtpLablel = findViewById(R.id.tvOtpLable);
        resendOtp = findViewById(R.id.resendOtp);
        etPassword = findViewById(R.id.etPassword);
        tvTNC = findViewById(R.id.tvEditMob);
        presenter = new VerifyPresenter(this);


    }

    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    etPassword.clearFocus();
                    hideSoftKeyboard(VerifyOtpActivity.this);
                    return false;
                }
            });
        }
    }


    public void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(etPassword.getWindowToken(), 0);
    }

    @Override
    public void setUp() {

        mobileNumber = getIntent().getStringExtra("mobile");
        securePrefManager.storeSharedValue(AppConstants.MOBILE, mobileNumber);
        tvOtpLablel.setText("One Time Password sent to " + mobileNumber + " number");
        confirmBtn.setOnClickListener(this);

        //etPassword.setText(getIntent().getStringExtra("otp"));

        SpannableString spannableString = new SpannableString("By Submitting OTP, I agree to Solar Ginie's Terms of Services and Privacy Policy.");
        spannableString.setSpan(new StyleSpan(Typeface.BOLD), 44, 61, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colortheme)), 44, 61, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new StyleSpan(Typeface.BOLD), 66, 81, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colortheme)), 66, 81, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new UnderlineSpan(), 44, 61, 0);
        spannableString.setSpan(new UnderlineSpan(), 66, 81, 0);
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                startActivity(openLink(getResources().getString(R.string.tosUrl)));
            }
        }, 44, 61, 0);
        spannableString.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                startActivity(openLink(getResources().getString(R.string.privacyPolicyUrl)));
            }
        }, 66, 81, 0);


        tvTNC.setMovementMethod(LinkMovementMethod.getInstance());
        tvTNC.setText(spannableString);


//        1212

        //etPassword.setText(getIntent().getStringExtra("otp"));

//        catcher = new SmsVerifyCatcher(this, new OnSmsCatchListener<String>() {
//            @Override
//            public void onSmsCatch(String message) {
//                etPassword.setText(message);
//            }
//        });


        resendOtp.setOnClickListener(this);
        etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 4) {
                    hideKeyboard(VerifyOtpActivity.this);
                }
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });
        setupUI(findViewById(R.id.parent));

    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.confirmBtn:
                if (etPassword.getText().length() < 4) {
                    showToast("Enter valid OTP");
                    etPassword.requestFocus();
                    return;
                }
                if (Utils.hasNetwork(this)) {
                    showProgress("Please wait");

                    presenter.verifyOtp(this, etPassword.getText().toString(), securePrefManager.getSharedValue(TOKEN));

                } else
                    showToast(getResources().getString(R.string.internet_error));


                break;

            case R.id.resendOtp:
                if (Utils.hasNetwork(this)) {
                    presenter.resedOtp(this, mobileNumber);
                    showProgress("Please wait");
                } else
                    showToast(getString(R.string.internet_error));

                break;
            case R.id.tvEditMob:
                //  startActivity(openLink(data.getDemandQuotes().get(index).attachment));

                break;
        }

    }

    @Override
    public void onVerifyOtp(String msg, OtpModel model) {

        securePrefManager.storeSharedValue(TOKEN, model.getUser().getAccessToken());
        securePrefManager.storeSharedValue(MOBILE, mobileNumber);
        securePrefManager.storeSharedValue(AppConstants.NAME, model.getUser().getName());
        securePrefManager.storeSharedValue(AppConstants.USER_ID, model.getUser().getUserID());
        if (model.getUser().getCompany() != null) {
            securePrefManager.storeSharedValue(AppConstants.COMPANYID, model.getUser().getCompany().getUserId());
            securePrefManager.storeSharedValue(AppConstants.COMPANYNAME, model.getUser().getCompany().getCompanyName());
        }

        securePrefManager.storeBooleanValue(AppConstants.ISAPPVISITED, true);
        if (model.getUser().getRoles() == 0) {
            Bundle bundle = new Bundle();
            hideProgress();

            if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(GUEST_ROLE)){
                bundle = new Bundle();
                bundle.putBoolean("isForGuest",true);
                bundle.putInt("role", 3);
               int flag = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;
                goToNextScreen(CustomerRegisterActivity.class, bundle, flag);
            }
            else {
                bundle = new Bundle();
                bundle.putString("phone", mobileNumber);
                int flag = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;
                securePrefManager.setScreenValue("SelectRole");
                goToNextScreen(SelectRoleActivity.class, bundle, flag);
            }


            etPassword.setText("");
        } else {
            securePrefManager.storeSharedValue(ROLE, String.valueOf(model.getUser().getRoles()));
            if (model.getUser().getRegistered()) {
                securePrefManager.storeSharedValue(AppConstants.REGISTERED, "true");
                presenter.getDetails(securePrefManager.getSharedValue(TOKEN), String.valueOf(model.getUser().getRoles()));
            } else {
                int flag = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;
                Bundle bundle1 = new Bundle();
                bundle1.putInt("role", model.getUser().getRoles());
                goToNextScreen(CustomerRegisterActivity.class, bundle1, flag);
            }


        }

    }

    @Override
    public void onOtpSent(String msg, String token) {
        hideProgress();
        etPassword.setText("");
        securePrefManager.storeSharedValue(TOKEN, token);
        showToast(msg);

    }

    @Override
    public void onFailure(String msg) {
        hideProgress();
        if (msg.equalsIgnoreCase("invalid token")) {
            securePrefManager.clearAppsAllPrefs();
            startActivity(new Intent(VerifyOtpActivity.this, MainActivity.class));
            finish();
        } else
            showToast(msg);

    }

    @Override
    public void OngetSiteDetails(CommomResponse response) {
        SiteLov details = response.toResponseModel(SiteLov.class);
        securePrefManager.storeSharedValue(AppConstants.SITELOVS, Utils.toString(details));


    }

    @Override
    public void onFetchDetails(CommomResponse response, String role) {
        hideProgress();
        switch (role) {
            case CUSTOMER_ROLE_ID:
                /* 200 -> profile not complete , 201 -> site not complete, 202 -> data available */
                Details details = response.toResponseModel(Details.class);
                if (details.getStatus() == 201) {
                    Bundle bundle = new Bundle();
                    bundle.putString("isFrom", "site");
                    int flag = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;

                    goToNextScreen(SiteRegistrationActivity.class, bundle, flag);


                } else {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("LandingData", details);
                    securePrefManager.storeSharedValue(AppConstants.NOTIFICATION_COUNT, String.valueOf(details.getNotificationCount()));
                    bundle.putString("for", "landing");
                    bundle.putBoolean("testimonials", details.testimonialCreated);

                    int flag = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;
                    goToNextScreen(UserHomeActivity.class, bundle, flag);


                }

                break;

            case IMPLEMENTER_ROLE_ID:
            case SUPER_IMPLEMENTER_ROLE_ID:
                ImplementerDetails implementerDetails = response.toResponseModel(ImplementerDetails.class);
                securePrefManager.storeSharedValue(AppConstants.NOTIFICATION_COUNT, String.valueOf(implementerDetails.getNotificationCount()));
                securePrefManager.storeSharedValue(IMPLEMENTER_DETAILS, Utils.toString(implementerDetails));
                Bundle bundle = new Bundle();
                bundle.putString("for", "landing");
                bundle.putBoolean("testimonials", implementerDetails.testimonialCreated);
                int flag = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;
                goToNextScreen(ImplemeterHomeActivity.class, bundle, flag);
                finish();

                break;
            case PROSPECT_ROLE_ID:
                ProspectLandingModel prospectLandingModel = response.toResponseModel(ProspectLandingModel.class);
                securePrefManager.storeSharedValue(AppConstants.PROSPECT_LANDIND, Utils.toString(prospectLandingModel));
                Intent intent = new Intent(this, ProspectHomeActivity.class);
                bundle = new Bundle();
                bundle.putString("for", "landing");
                flag = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;
                intent.setFlags(flag);
                intent.putExtras(bundle);
                startActivity(intent);

                break;
            case SG_ADMIN:
            case BO_MANAGER:

                hideProgress();
                SGLandingModel model = response.toResponseModel(SGLandingModel.class);
                securePrefManager.storeSharedValue(AppConstants.NOTIFICATION_COUNT, String.valueOf(model.data.notificationCount));
                bundle = new Bundle();
                bundle.putSerializable("LandingData", model);
                bundle.putString("for", "landing");
                flag = Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK;
                goToNextScreen(SGHomeActivity.class, bundle, flag);


                break;

        }

    }
}
