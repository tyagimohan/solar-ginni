package com.solarginni.CommonModule;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.solarginni.Base.BaseActivity;
import com.solarginni.OTPModule.LoginContract;
import com.solarginni.OTPModule.LoginPresenter;
import com.solarginni.ProspectUser.ProspectHomeActivity;
import com.solarginni.R;
import com.solarginni.Utility.AppConstants;
import com.solarginni.Utility.Utils;

import static com.solarginni.Utility.AppConstants.GUEST_ROLE;
import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.TOKEN;

/**
 * Created by Mohan on 3/1/20.
 */

public class MainActivity extends BaseActivity implements View.OnClickListener, LoginContract.View {

    private static final int PERMISSION_REQUEST_CODE_LOCATION = 1;
    private static final String TAG =MainActivity.class.getSimpleName() ;
    private Button submitBtn;
    private EditText etMobile;
    private LoginPresenter presenter;
    private static final int MAX_AGE = 15;       // Minutes
    public static Location mLatestLocation;
    public static String mLatestAddress;
    private static boolean mLocationProviderEnabled;
    private TextView guestUser;

    @Override
    public int getLayout() {
        return R.layout.activity_main;
    }

    @Override
    public void initViews() {
        submitBtn = findViewById(R.id.confirmBtn);
        etMobile = findViewById(R.id.etMobileNmbr);
        guestUser = findViewById(R.id.guestUser);
        presenter = new LoginPresenter(this);
        disableSuggestion(etMobile);

    }

    @Override
    public void setUp() {
        submitBtn.setOnClickListener(this);
        guestUser.setOnClickListener(this);
        etMobile.setText("+91");

        if (getIntent().getBooleanExtra("isForGuest",false)){
            guestUser.setVisibility(View.GONE);
        }

        try {
            Selection.setSelection(etMobile.getText(), etMobile.getText().length());
        } catch (Exception e) {
            e.printStackTrace();
        }
        etMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 13) {

                    hideKeyboard(MainActivity.this);
                }
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().startsWith("+91")){
                    etMobile.setText("+91");
                    try {
                        Selection.setSelection(etMobile.getText(), etMobile.getText().length());
                    } catch (IndexOutOfBoundsException e) {
                        e.printStackTrace();
                    }

                }

                // TODO Auto-generated method stub
            }
        });

        setupUI(findViewById(R.id.parent));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.confirmBtn:
                if (etMobile.getText().toString().equals("") || etMobile.getText().length() < 13) {
                    showToast("Enter Valid Mobile Number");
                    etMobile.requestFocus();
                    return;
                }
                if (Utils.hasNetwork(this)) {
                    presenter.sentOtp(this, etMobile.getText().toString());
                    showProgress("Please wait");
                } else
                    showToast(getResources().getString(R.string.internet_error));

                break;
            case R.id.guestUser:

                securePrefManager.storeSharedValue(ROLE,GUEST_ROLE);
                securePrefManager.storeBooleanValue(AppConstants.ISVISITED,true);
                startActivity(new Intent(MainActivity.this, ProspectHomeActivity.class));
                finish();

                break;
        }
    }

    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    ///  view.requestFocus();
                    hideKeyboard(MainActivity.this);
                    etMobile.clearFocus();
                    return false;
                }
            });
        }
    }


    @Override
    public void onOtpSent(String msg, String token,String otp) {
        hideProgress();
        showToast(msg);
        securePrefManager.storeSharedValue(TOKEN, token);
        Intent intent = new Intent(new Intent(MainActivity.this, VerifyOtpActivity.class));
        intent.putExtra("mobile", etMobile.getText().toString());
        intent.putExtra("otp", otp);
        startActivity(intent);
        etMobile.setText("");

    }

    @Override
    public void onFailure(String msg) {
        hideProgress();
        if (msg.equalsIgnoreCase("invalid token")){
            securePrefManager.clearAppsAllPrefs();
            startActivity(new Intent(MainActivity.this,MainActivity.class));
            finish();
        }
        else
        showToast(msg);

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PERMISSION_REQUEST_CODE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    securePrefManager.storeSharedValue(ROLE,GUEST_ROLE);
                    startActivity(new Intent(MainActivity.this, ProspectHomeActivity.class));
                    finish();
                }
                else {

                }
            }
            break;

        }
    }







}
