package com.solarginni.CommonModule;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;
import com.solarginni.network.RestClient;

import retrofit2.Retrofit;

public abstract class SGPromotionApi {

    private Retrofit retrofit;
    private ApiInterface mApiInterface;

    public SGPromotionApi( ) {
        retrofit= RestClient.getRetrofitBuilder();
        mApiInterface= retrofit.create(ApiInterface.class);
    }

    public void hit(String token,String key,int page,int type, String state){
        mApiInterface.getSGPromotions(token,key,page,10,type,state).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                if (key.equalsIgnoreCase("#")){

                    onComplete(response);
                }else {
                    onSearch(response);
                }

            }

            @Override
            public void onError(ApiError error) {
                onFailur(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                onFailur(throwable.getMessage());
            }
        });
    }

    public abstract void onComplete(CommomResponse response);
    public abstract void onSearch(CommomResponse response);
    protected abstract void onFailur(String msg);



}
