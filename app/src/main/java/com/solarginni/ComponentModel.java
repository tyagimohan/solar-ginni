package com.solarginni;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.solarginni.DBModel.CompanyMakeModel;

import java.io.Serializable;
import java.util.List;

public class ComponentModel implements Serializable {

    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public Data data;


    public class Data {

        @SerializedName("structure_type")
        @Expose
        public List<String> structureType = null;
        @SerializedName("panel_type")
        @Expose
        public List<String> panelType = null;
        @SerializedName("distribution_box")
        @Expose
        public List<String> distribution_box = null;
        @SerializedName("bidirectional_meter")
        @Expose
        public List<String> bidirectional_meter = null;
        @SerializedName("dc_cable_brand")
        @Expose
        public List<String> dc_cable_brand = null;
        @SerializedName("ac_cable_brand")
        @Expose
        public List<String> ac_cable_brand = null;
        @SerializedName("lightning_arrestor")
        @Expose
        public List<String> lightning_arrestor = null;
        @SerializedName("earthing_kit")
        @Expose
        public List<String> earthing_kit = null;
        @SerializedName("inverter")
        @Expose
        public List<CompanyMakeModel> inverter = null;
        @SerializedName("panel")
        @Expose
        public List<CompanyMakeModel> panel = null;
        @SerializedName("battery")
        @Expose
        public List<CompanyMakeModel> battery = null;
        @SerializedName("state")
        @Expose
        public List<String> state = null;

    }


}















