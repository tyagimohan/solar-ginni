package com.solarginni.Backend;

public class QuoteImplementerTable {

    public static final String QUOTE_IMPLEMENTER_TABLE = "quote_implementer_table";

    public static final String USER_ID = "user_id";

    public static final String COMPANY_NAME = "company_name";

    public static final String COMPANY_LOGO = "companyLogo";

    public static final String STATUS = "status";

    public static final String SITE_SIZE = "site_size";

    public static final String SITE_COUNT = "site_count";

    public static final String MAXDIS = "max_dis";

    public static final String MINDIS = "min_dis";

    public static final String PRICE = "price";

    public static final String STATE_SITE = "state_site";

    public static final String STATE_SIZE = "state_size";

    public static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + QUOTE_IMPLEMENTER_TABLE + "("

            + USER_ID + " TEXT,"
            + COMPANY_NAME + " TEXT,"
            + STATUS + " TEXT,"
            + SITE_SIZE + " DOUBLE,"
            + SITE_COUNT + " TEXT,"
            + MAXDIS + " DOUBLE,"
            + MINDIS + " DOUBLE,"
            + PRICE + " TEXT,"
            + STATE_SIZE + " DOUBLE,"
            + STATE_SITE + " DOUBLE,"
            + COMPANY_LOGO + " TEXT );";


    public static final String INSERT_DATA = " INSERT INTO " + QUOTE_IMPLEMENTER_TABLE + "("
            + USER_ID + ","
            + COMPANY_NAME + ","
            + STATUS + ","
            + SITE_SIZE + ","
            + SITE_COUNT + ","
            + MAXDIS + ","
            + MINDIS + ","
            + PRICE + ","
            + STATE_SIZE + ","
            + STATE_SITE + ","
            + COMPANY_LOGO
            + " ) values(?,?,?,?,?,?,?,?,?,?,?);";
}