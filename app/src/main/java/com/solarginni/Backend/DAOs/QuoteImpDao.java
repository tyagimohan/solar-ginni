package com.solarginni.Backend.DAOs;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import com.solarginni.Backend.QuoteImplementerTable;
import com.solarginni.ProspectUser.QutationModel;

import java.util.ArrayList;
import java.util.List;

public class QuoteImpDao extends BaseDao {
    protected QuoteImpDao(Context context) {
        super(context);
    }

    public static QuoteImpDao getInstance(Context mContext) {
        return new QuoteImpDao(mContext);
    }

    public void insertData(List<QutationModel.Filter> list) {

        openDB(1);

        deleteData(QuoteImplementerTable.QUOTE_IMPLEMENTER_TABLE);

        try {
            getDb().beginTransaction();
            SQLiteStatement stmt = getDb().compileStatement(QuoteImplementerTable.INSERT_DATA);

            for (int i = 0; i < list.size(); i++) {
                stmt.bindString(1, list.get(i).getUserId());
                stmt.bindString(2, list.get(i).getCompanyName());
                stmt.bindString(3, String.valueOf(list.get(i).isQuoted()));
                stmt.bindDouble(4, list.get(i).getSitesize());
                stmt.bindString(5, String.valueOf(list.get(i).getSitecount()));
                stmt.bindDouble(6, list.get(i).getMax());
                stmt.bindDouble(7, list.get(i).getMin());
                stmt.bindString(8, String.valueOf(list.get(i).getPrice()));
                stmt.bindDouble(9, list.get(i).getStateSitesize());
                stmt.bindString(10, String.valueOf(list.get(i).getStateSiteCount()));
                if (list.get(i).url!=null)
                stmt.bindString(11, list.get(i).url);
                else
                    stmt.bindString(11,"");

                stmt.executeInsert();
                stmt.clearBindings();
            }
            getDb().setTransactionSuccessful();

        } catch (Exception e) {
            e.printStackTrace();
            getDb().endTransaction();
        } finally {

            getDb().endTransaction();
            closeDb();

        }

    }

    public List<QutationModel.Filter> sortListByValue(String value) {

        List<QutationModel.Filter> list = new ArrayList<>();

        String query = "Select * from " + QuoteImplementerTable.QUOTE_IMPLEMENTER_TABLE;
/*
1. Implementer's Installed Sites -- High to Low
2. Implementer's Installed Sites --Low to High
3. Implementer's Price -- High to Low
4. Implementer's Price -- Low to High
5. Implementer's Installed Capacity -- High to Low
6. Implementer's Installed Capacity -- Low to High


*/
        switch (value) {
            case "Implementer's Installed Sites -- High to Low":
                query += " ORDER by quote_implementer_table.site_count DESC,quote_implementer_table.site_size DESC ";
                break;
            case "Implementer's Installed Sites -- Low to High":
                query += " ORDER by quote_implementer_table.site_count ASC,quote_implementer_table.site_size DESC ";
                break;
            case "Implementer's Price -- High to Low":
                query += " ORDER by quote_implementer_table.price DESC ";
                break;
            case "Implementer's Price -- Low to High":
                query += " ORDER by quote_implementer_table.price ASC ";
                break;
            case "Implementer's Installed Capacity -- High to Low":
                query += " ORDER by quote_implementer_table.site_size DESC ";
                break;
            case "Implementer's Installed Capacity -- Low to High":
                query += " ORDER by quote_implementer_table.site_size ASC ";
                break;
            case "Implementer's Site in your State -- Low to High":
                query += " ORDER by quote_implementer_table.state_site ASC ";
                break;
            case "Implementer's Site in your State -- High to Low":
                query += " ORDER by quote_implementer_table.state_site DESC ";
                break;


        }


        openDB(0);

        Cursor mCursor = null;

        try {
            mCursor = getDb().rawQuery(query, null);

            if (mCursor.moveToFirst()) {

                QutationModel.Filter aData;
                do {
                    aData = new QutationModel.Filter();
                    parseData(aData, mCursor);
                    list.add(aData);

                } while (mCursor.moveToNext());

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeCursor(mCursor);
            closeDb();
        }
        return list;

    }

    public void parseData(QutationModel.Filter data, Cursor cursor) {

        data.setUserId(cursor.getString(cursor.getColumnIndex(QuoteImplementerTable.USER_ID)));
        data.setCompanyName(cursor.getString(cursor.getColumnIndex(QuoteImplementerTable.COMPANY_NAME)));
        data.setMax(cursor.getDouble(cursor.getColumnIndex(QuoteImplementerTable.MAXDIS)));
        data.setMin(cursor.getDouble(cursor.getColumnIndex(QuoteImplementerTable.MINDIS)));
        data.setPrice(Integer.valueOf(cursor.getString(cursor.getColumnIndex(QuoteImplementerTable.PRICE))));
        data.setQuoted(Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(QuoteImplementerTable.STATUS))));
        data.setSitecount(Integer.valueOf(cursor.getString(cursor.getColumnIndex(QuoteImplementerTable.SITE_COUNT))));
        data.setSitesize(cursor.getDouble(cursor.getColumnIndex(QuoteImplementerTable.SITE_SIZE)));
        data.setStateSitesize(cursor.getDouble(cursor.getColumnIndex(QuoteImplementerTable.STATE_SIZE)));
        data.setStateSiteCount(Integer.valueOf(cursor.getString(cursor.getColumnIndex(QuoteImplementerTable.STATE_SITE))));
        data.url=cursor.getString(cursor.getColumnIndex(QuoteImplementerTable.COMPANY_LOGO));
    }


    public void updateStatus(String id) {


        openDB(1);

        ContentValues cv = new ContentValues();

        cv.put("status", String.valueOf(true));

        try {
            getDb().update(QuoteImplementerTable.QUOTE_IMPLEMENTER_TABLE, cv, "user_id = '" + id + "'", null);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeDb();
        }

    }


}
