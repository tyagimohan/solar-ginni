package com.solarginni.Backend.DAOs;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

import com.solarginni.Backend.MyDBHandler;

public  class BaseDao {

    private Context mContext;

    protected BaseDao(Context context) {
        mContext = context;
    }

    /**
     * Open SQLiteDatabase(db) object according to need.
     *
     * @param type variable according to which SQLiteDatabase object is returned writable/readable object.
     * @param type == 0 it returns writable db object otherwise readable db object.
     */
    protected void openDB(int type) throws SQLiteException {
        MyDBHandler.getInstance(mContext).getDBObject(type);
    }

    protected void closeDb() throws SQLiteException {
        if (getDb() != null && getDb().isOpen()) {
            MyDBHandler.getInstance(mContext).closeDb();
        }
    }

    protected void closeCursor(Cursor cursor) {
        if (cursor != null && !cursor.isClosed()) {
            cursor.close();
        }
    }

    protected Context getContext() {
        return mContext;
    }

    public synchronized void deleteData(String tableName) {
        openDB(1);
        try {
            getDb().delete(tableName, null, null);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (getDb() != null) {
                closeDb();
            }
        }
    }

    protected SQLiteDatabase getDb() {
     return MyDBHandler.getInstance(mContext).getDb();
    }

}
