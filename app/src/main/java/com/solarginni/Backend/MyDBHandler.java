package com.solarginni.Backend;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import androidx.annotation.Nullable;

import com.solarginni.BuildConfig;
import com.solarginni.user.SiteNearMe.SiteNearMeTable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class MyDBHandler extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = BuildConfig.APP_NAME;
    private static final int DATABASE_VERSION = 9;
    private static MyDBHandler mMyDbHandler;
    private SQLiteDatabase db = null;
    private AtomicInteger mOpenCounter = new AtomicInteger();

    public MyDBHandler(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(QuoteImplementerTable.CREATE_TABLE);
        db.execSQL(SiteNearMeTable.CREATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
          if (i1>i){
              try {
                  sqLiteDatabase.execSQL("ALTER TABLE " + SiteNearMeTable.SITE_NEAR_ME_TABLE + " ADD COLUMN " + SiteNearMeTable.IMPLEMENTER_ID + " TEXT  default '' ");
                  sqLiteDatabase.execSQL("ALTER TABLE " + QuoteImplementerTable.QUOTE_IMPLEMENTER_TABLE + " ADD COLUMN " + QuoteImplementerTable.COMPANY_LOGO + " TEXT  default '' ");
              } catch (SQLException e) {
                  e.printStackTrace();
              }
          }


    }


    public static MyDBHandler getInstance(Context context) {
        if (mMyDbHandler == null) {
            synchronized (MyDBHandler.class) {
                mMyDbHandler = new MyDBHandler(context);

            }
        }
        return mMyDbHandler;
    }

    public synchronized SQLiteDatabase getDBObject(int isWrtitable) {
        if (mOpenCounter.incrementAndGet() == 1) {
            db = (isWrtitable == 1) ? getWritableDatabase() : getReadableDatabase();
        }
        return db;
    }

    public synchronized void closeDb() {
        if (mOpenCounter.decrementAndGet() == 0) {
            db.close();
        }
    }

    public SQLiteDatabase getDb() {
        return db;
    }

    public void deleteAll() {

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor c = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);
        List<String> tables = new ArrayList<>();

        while (c.moveToNext()) {
            tables.add(c.getString(0));
        }

        for (int i = 0; i < tables.size() - 2; i++) {
            db.execSQL("delete from " + tables.get(i));
        }
        db.close();

    }
}
