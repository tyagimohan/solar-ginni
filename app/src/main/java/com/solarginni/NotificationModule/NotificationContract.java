package com.solarginni.NotificationModule;

import com.solarginni.DBModel.CommomResponse;

public interface NotificationContract {

    interface View {
        void onNotificationFetch(CommomResponse response);

        void onFailure(String msg);
    }

    interface Presenter {
        void fetchNotification(String token);
    }

    interface Interactor {
        void fetchNotification(String token, OnInteraction interaction);
    }

    interface OnInteraction {
        void onNotificationFetch(CommomResponse response);

        void onFailure(String msg);

    }
}
