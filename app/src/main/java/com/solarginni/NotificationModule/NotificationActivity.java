package com.solarginni.NotificationModule;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.solarginni.Base.BaseActivity;
import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.R;
import com.solarginni.Utility.Utils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static com.solarginni.Utility.AppConstants.TOKEN;

public class NotificationActivity extends BaseActivity implements NotificationContract.View{
    private RecyclerView recyclerView;
    private NotificationPresenter presenter;
    @Override
    public int getLayout() {
        return R.layout.notification_activity;
    }

    @Override
    public void initViews() {

        recyclerView= findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new BaseRecyclerAdapter.DividerItemDecoration(this, RecyclerView.VERTICAL));

        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("");
        toolbar.setTitle("");
        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });
    }

    @Override
    public void setUp() {

        presenter= new NotificationPresenter(this);
        if (Utils.hasNetwork(this)){
            showProgress("Please wait...");
            presenter.fetchNotification(securePrefManager.getSharedValue(TOKEN));
        }
        else {
            showToast("Please check Internet");

        }

    }

    @Override
    public void onNotificationFetch(CommomResponse response) {
        hideProgress();
        List<NotificationModel.Datum> notificationList= new ArrayList<>();
         NotificationModel model= response.toResponseModel(NotificationModel.class);
         notificationList.addAll(model.getData());
        NotificationAdapter adapter= new NotificationAdapter(this);

        adapter.addAll(notificationList);
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void onFailure(String msg) {
        hideProgress();
        showToast(msg);

    }

    public class NotificationModel implements Serializable{
        @SerializedName("notifications")
        @Expose
        private List<Datum> data = null;


        public List<Datum> getData() {
            return data;
        }

        public void setData(List<Datum> data) {
            this.data = data;
        }

        public class Datum implements Serializable {

            @SerializedName("message")
            @Expose
            private String message;

            public String getMessage() {
                return message;
            }

            public void setMessage(String message) {
                this.message = message;
            }

        }
    }
}
