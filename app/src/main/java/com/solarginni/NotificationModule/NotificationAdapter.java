package com.solarginni.NotificationModule;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.R;

public class NotificationAdapter extends BaseRecyclerAdapter<NotificationActivity.NotificationModel.Datum, NotificationAdapter.Holder> {


    public NotificationAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public int getLayout(int viewType) {
        return R.layout.notification_item;
    }

    @Override
    public Holder getViewHolder(View view, int viewType) {
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {

        holder.desc.setText(getItem(i).getMessage());

    }

    public class Holder extends RecyclerView.ViewHolder{
        TextView desc;

        public Holder(@NonNull View itemView) {
            super(itemView);
            desc= itemView.findViewById(R.id.notification_des);
        }
    }

}
