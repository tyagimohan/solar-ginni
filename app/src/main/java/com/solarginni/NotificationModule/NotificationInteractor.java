package com.solarginni.NotificationModule;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;

import retrofit2.Retrofit;

public class NotificationInteractor  implements NotificationContract.Interactor {

    private Retrofit retrofit;
    private ApiInterface apiInterface;

    public NotificationInteractor(Retrofit retrofit) {
        this.retrofit = retrofit;
        apiInterface=retrofit.create(ApiInterface.class);
    }

    @Override
    public void fetchNotification(String token, NotificationContract.OnInteraction interaction) {
        apiInterface.fetchNotification(token).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                interaction.onNotificationFetch(response);

            }

            @Override
            public void onError(ApiError error) {
                interaction.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                interaction.onFailure(throwable.getMessage());

            }
        });

    }
}
