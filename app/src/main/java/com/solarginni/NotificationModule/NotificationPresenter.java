package com.solarginni.NotificationModule;

import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.RestClient;

public class NotificationPresenter implements NotificationContract.Presenter, NotificationContract.OnInteraction {
    private NotificationContract.View view;
    private NotificationInteractor interactor;


    public NotificationPresenter(NotificationContract.View view) {
        this.view = view;
        interactor= new NotificationInteractor(RestClient.getRetrofitBuilder());
    }

    @Override
    public void fetchNotification(String token) {
           interactor.fetchNotification(token,this);
    }


    @Override
    public void onNotificationFetch(CommomResponse response) {
        view.onNotificationFetch(response);

    }

    @Override
    public void onFailure(String msg) {
        view.onFailure(msg);

    }
}
