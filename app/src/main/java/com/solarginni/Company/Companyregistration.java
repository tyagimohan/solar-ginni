package com.solarginni.Company;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.kbeanie.multipicker.api.CacheLocation;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.solarginni.Base.BaseActivity;
import com.solarginni.CommonModule.MainActivity;
import com.solarginni.DBModel.APIBody.CompanyModel;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.UserRoleModel;
import com.solarginni.R;
import com.solarginni.SGUser.CompanyModule.CompanyContract;
import com.solarginni.SGUser.CompanyModule.CompanyListModel;
import com.solarginni.SGUser.CompanyModule.CompanyPresenter;
import com.solarginni.Utility.Permissions;
import com.solarginni.Utility.Utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import static com.solarginni.Utility.AppConstants.TOKEN;

public class Companyregistration extends BaseActivity implements View.OnClickListener, CompanyContract.View, ImagePickerCallback {
    private static final int REQUEST_FOR_PLACE = 2;
    private static final int REQUEST_CODE_FOR_PERMISSION = 4;
    private String[] companyTypeArray, subTypeArray, isIndianArray;
    private TextView companyType, subCompanyType;
    private CompanyPresenter presenter;
    private Button submitBtn;
    private String url = "";
    private Geocoder mGeocoder;
    private ImageView mapImg;
    private ImageView companyLogo;
    private ImagePicker mImagePicker;
    private CameraImagePicker mCameraImagePicker;
    private File mPhotoFile = null;
    private EditText etCompanyName, etGSTNumber, etLine1Add, etPincode;
    private TextView tvline2Add, tvCity, tvState, tvCountry;
    private Double lat, lng;
    private EditText companyId, telephone, videoUrl, website, etisIndian;

    @Override
    public int getLayout() {
        return R.layout.company_registration;
    }

    @Override
    public void initViews() {
        companyType = findViewById(R.id.companyType);
        submitBtn = findViewById(R.id.submit_btn);
        etCompanyName = findViewById(R.id.companyName);
        etGSTNumber = findViewById(R.id.gstNumber);
        etPincode = findViewById(R.id.etPincode);
        etLine1Add = findViewById(R.id.etLine1);
        tvline2Add = findViewById(R.id.line2);
        tvCity = findViewById(R.id.tvCity);
        tvState = findViewById(R.id.tvState);
        mapImg = findViewById(R.id.mapImg);
        companyLogo = findViewById(R.id.profileImage);
        etisIndian = findViewById(R.id.isIndian);
        companyId = findViewById(R.id.companyId);
        telephone = findViewById(R.id.telephone);
        website = findViewById(R.id.website);
        videoUrl = findViewById(R.id.videoUrl);
        subCompanyType = findViewById(R.id.companySubType);
        tvCountry = findViewById(R.id.tvCountry);
        presenter = new CompanyPresenter(this);
        mGeocoder = new Geocoder(this, Locale.getDefault());
        Places.initialize(getApplicationContext(), getString(R.string.google_api));


        mImagePicker = new ImagePicker(this);
        mImagePicker.setCacheLocation(CacheLocation.INTERNAL_APP_DIR);
        mImagePicker.setImagePickerCallback(this);
        mImagePicker.shouldGenerateThumbnails(false);
        mImagePicker.shouldGenerateMetadata(false);

        mCameraImagePicker = new CameraImagePicker(this);
        mCameraImagePicker.setCacheLocation(CacheLocation.INTERNAL_APP_DIR);
        mCameraImagePicker.setImagePickerCallback(this);
        mCameraImagePicker.shouldGenerateThumbnails(false);
        mCameraImagePicker.shouldGenerateMetadata(false);
        companyTypeArray = getResources().getStringArray(R.array.company_type);
        subTypeArray = getResources().getStringArray(R.array.sub_company_type);
        isIndianArray = getResources().getStringArray(R.array.booleanValue);

        disableSuggestion(tvline2Add);
        disableSuggestion(tvCity);
        disableSuggestion(etLine1Add);
        disableSuggestion(companyType);
        disableSuggestion(etPincode);
        disableSuggestion(tvCountry);
        disableSuggestion(tvCity);
        disableSuggestion(tvState);
        disableSuggestion(telephone);
        disableSuggestion(etCompanyName);

        setupUI(findViewById(R.id.parent));

        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("");
        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });

    }

    @Override
    public void setUp() {
        disableAutofill();

        Bundle bundle = getIntent().getExtras();

        if (bundle.getBoolean("isForEdit", false)) {
            CompanyListModel.CompanyDt data = (CompanyListModel.CompanyDt) bundle.getSerializable("data");
            companyId.setText(data.userId);
            companyId.setVisibility(View.VISIBLE);
            findViewById(R.id.companyIdLay).setVisibility(View.VISIBLE);
            etCompanyName.setText(data.companyName);
            companyType.setText(data.companyType);
            if (data.companyType.equalsIgnoreCase("Implementer"))
                subCompanyType.setText("Implementer");
            else{
                subCompanyType.setText(data.industry);
                subCompanyType.setOnClickListener(this);

            }

            try {
                if (!data.imageUrl.isEmpty())
                    Glide.with(this).load(data.imageUrl).into(companyLogo);
            } catch (Exception e) {
                e.printStackTrace();

            }

            telephone.setText(data.spocTelephone);
            etGSTNumber.setText(data.gstNumber);
            tvCity.setText(data.address.getCity());
            tvState.setText(data.address.getState());
            etPincode.setText(data.address.getPinCode());
            tvCountry.setText(data.address.getCountry());
            etLine1Add.setText(data.address.getAddressLine1());
            tvline2Add.setText(data.address.getAddressLine2());
            website.setText(data.websiteUrl);
            videoUrl.setText(data.videoUrl);
            if (data.indian) {
                etisIndian.setText("Yes");
            } else {
                etisIndian.setText("No");
            }
        }

        companyType.setOnClickListener(this);
        submitBtn.setOnClickListener(this);
        tvline2Add.setOnClickListener(this);
        companyLogo.setOnClickListener(this);
        findViewById(R.id.editImage).setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.companyType:
                showAlertWithList(companyTypeArray, "Select Company Type", companyType, true);
                break;
            case R.id.companySubType:
                showAlertWithList(subTypeArray, "Select Sub- Type", subCompanyType, false);
                break;
            case R.id.submit_btn:
                submitForm();
                break;
            case R.id.line2:
                openPlacePicker();
                break;
            case R.id.isIndian:
                showAlertWithList(isIndianArray, "Is Company Indian ?", etisIndian, false);
                break;
            case R.id.editImage:
            case R.id.profileImage:
                if (checkPermissionAPI23())
                    getImagePicker();
                break;
        }

    }


    private boolean checkPermissionAPI23() {
        boolean isPermissionRequired = false;
        List<String> permissionArray = new ArrayList<>();

        if (Permissions.getInstance().hasCameraPermission(this).permissions.size() > 0) {
            permissionArray.add(Manifest.permission.CAMERA);
            isPermissionRequired = true;
        }

        if (Permissions.getInstance().checkReadWritePermissions(this).permissions.size() > 0) {
            isPermissionRequired = true;
            permissionArray.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            permissionArray.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (isPermissionRequired) {
            Permissions.getInstance().addPermission(permissionArray).askForPermissions(this, REQUEST_CODE_FOR_PERMISSION);
            return false;
        }
        return true;
    }

    private void getImagePicker() {
        CharSequence[] options = getResources().getStringArray(R.array.image_picker_options);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(options, (dialog, which) -> {
            switch (which) {
                case 0:
                    mCameraImagePicker.pickImage();
                    break;
                case 1:
                    mImagePicker.pickImage();
                    break;
                default:
                    break;
            }
        });
        builder.show();
    }


    private void submitForm() {

        if (!validateName()) {
            return;
        }
        if (!validateType()) {
            return;
        }
        if (!validateSubType()) {
            return;
        }

        if (!validateAddress1()) {
            return;
        }
        if (!validatePhone()) {
            return;
        }
        if (!validateAddress2()) {
            return;
        }
        if (!validateState()) {
            return;
        }
        if (!validateCountry()) {
            return;
        }
        if (!pincode()) {
            return;
        }
        CompanyModel model = new CompanyModel();
        model.companyName = etCompanyName.getText().toString().trim();
        model.companyType = companyType.getText().toString().trim();
        model.industry = subCompanyType.getText().toString().trim();
        model.gstNumber = etGSTNumber.getText().toString().trim();
        model.spocTelephone = telephone.getText().toString();
        model.websiteUrl = website.getText().toString();
        model.videoUrl = videoUrl.getText().toString();
        UserRoleModel.Address address = new UserRoleModel.Address();
        address.setAddressLine1(etLine1Add.getText().toString());
        address.setAddressLine2(tvline2Add.getText().toString());
        address.setCity(tvCity.getText().toString());
        address.setCountry(tvCountry.getText().toString());
        address.setPinCode(etPincode.getText().toString());
        address.setLongitude(lng);
        address.setLatitude(lat);
        address.setState(tvState.getText().toString());
        model.address = address;
        if (etisIndian.getText().toString().equalsIgnoreCase("") | etisIndian.getText().toString().equalsIgnoreCase("Yes"))
            model.indian = true;
        else {
            model.indian = false;
        }


        if (Utils.hasNetwork(this)) {
            showProgress("Please wait...");
            if (getIntent().getExtras().getBoolean("isForEdit", false)) {
                presenter.updateCompany(this, model, securePrefManager.getSharedValue(TOKEN), mPhotoFile, companyId.getText().toString());
            } else {
                presenter.registerImplementer(this, model, securePrefManager.getSharedValue(TOKEN), mPhotoFile);

            }

        } else
            showToast(getResources().getString(R.string.internet_error));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_FOR_PLACE:
                if (resultCode == RESULT_OK) {
                    Place place = Autocomplete.getPlaceFromIntent(data);
                    tvline2Add.setText(place.getName());
                    LatLng latLng = place.getLatLng();
                    lat = latLng.latitude;
                    lng = latLng.longitude;
                    try {
                        getCityNameByCoordinates(latLng.latitude, latLng.longitude);
                        Glide.with(this).load(getStaticMapURL(String.valueOf(latLng.latitude), String.valueOf(latLng.longitude))).into(mapImg);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    //  onParamSelected();
                } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                    Status status = Autocomplete.getStatusFromIntent(data);


                } else if (resultCode == RESULT_CANCELED) {

                }
                break;
            case Picker.PICK_IMAGE_DEVICE:
                if (resultCode == RESULT_OK) {
                    mImagePicker.submit(data);
                } else {

                }
                break;

            case Picker.PICK_IMAGE_CAMERA:
                if (resultCode == RESULT_OK) {
                    mCameraImagePicker.submit(data);
                } else {

                }
                break;

        }
    }

    @Override
    public void onRegister() {
        hideProgress();
        setResult(Activity.RESULT_OK);
        finish();

    }

    @Override
    public void onFetchCompany(CommomResponse response) {

    }

    @Override
    public void onSearchCompany(CommomResponse response) {

    }

    @Override
    public void onFailure(String msg) {
        hideProgress();
        if (msg.equalsIgnoreCase("invalid token")) {
            securePrefManager.clearAppsAllPrefs();
            startActivity(new Intent(Companyregistration.this, MainActivity.class));
            finish();
        } else {
            showToast(msg);
        }

    }

    private void openPlacePicker() {

        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), getString(R.string.google_api));
        }
        List<Place.Field> fields = Arrays.asList(Place.Field.ADDRESS_COMPONENTS, Place.Field.NAME, Place.Field.LAT_LNG);

        // Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).build(Objects.requireNonNull(this));
        startActivityForResult(intent, REQUEST_FOR_PLACE);
    }

    private void getCityNameByCoordinates(double lat, double lon) throws IOException {

        List<Address> addresses = mGeocoder.getFromLocation(lat, lon, 1);
        if (addresses != null && addresses.size() > 0) {

            tvState.setText(addresses.get(0).getAdminArea());
            //   addLine2.setText(addLine2.getText() + " " + addresses.get(0).getSubLocality());
            if (addresses.get(0).getSubAdminArea().isEmpty() || addresses.get(0).getSubAdminArea().equalsIgnoreCase("")) {
                tvCity.setVisibility(View.VISIBLE);
                tvCity.setVisibility(View.GONE);
            } else {
                tvCity.setVisibility(View.GONE);
                tvCity.setVisibility(View.VISIBLE);
                tvCity.setText(addresses.get(0).getSubAdminArea());
            }

            etPincode.setText(addresses.get(0).getPostalCode());
            tvCountry.setText(addresses.get(0).getCountryName());
        }

    }

    private String getStaticMapURL(String lat, String lng) {

        url = "http://maps.googleapis.com/maps/api/staticmap?center=" + lat + "," + lng + "&zoom=15&size=200x200&markers=color:red|label:S|" + lat + "," + lng + "&sensor=true&key=" + getString(R.string.google_api);
        return url;
    }

    private boolean validateName() {
        if (etCompanyName.getText().toString().trim().length() < 3) {
            showToast("Enter valid Company Name");
            requestFocus(etCompanyName);
            return false;
        }
        return true;
    }

    private boolean validateType() {
        if (companyType.getText().toString().trim().isEmpty()) {
            showToast("Select Company Type");
            return false;
        }
        return true;
    }

    private boolean validateSubType() {
        if (subCompanyType.getText().toString().trim().isEmpty()) {
            showToast("Select Sub-Company Type");
            return false;
        }
        return true;
    }

    private boolean validateAddress1() {
        if (etLine1Add.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.err_msg_add));
            requestFocus(etLine1Add);
            return false;
        }
        return true;
    }

    private boolean validatePhone() {
        if (telephone.getText().toString().trim().length() < 10) {
            showToast("Please enter Valid Phone number");
            requestFocus(telephone);
            return false;
        }
        return true;
    }

    private boolean validateAddress2() {
        if (tvline2Add.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.err_msg_add));
            requestFocus(tvline2Add);
            return false;
        }

        return true;
    }

    private boolean validateCountry() {
        if (tvCountry.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.err_msg_country));
            requestFocus(tvCountry);
            return false;
        }

        return true;
    }

    private boolean validateState() {
        if (tvState.getText().toString().trim().isEmpty()) {
            showToast("Enter State");
            requestFocus(tvState);
            return false;
        }

        return true;
    }

    private boolean pincode() {
        if (etPincode.getText().toString().trim().isEmpty()) {
            showToast("Enter Pincode");
            requestFocus(etPincode);
            return false;
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void disableAutofill() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            getWindow().getDecorView().setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS);
    }

    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    view.requestFocus();
                    view.findViewById(R.id.relativeLay).clearFocus();
                    etPincode.clearFocus();
                    etLine1Add.clearFocus();

                    etGSTNumber.clearFocus();
                    etCompanyName.clearFocus();
                    hideKeyboard(Companyregistration.this);
                    return false;
                }
            });
        }
    }

    @Override
    public void onImagesChosen(List<ChosenImage> list) {

        mPhotoFile = new File(list.get(0).getOriginalPath());
        Bitmap bitmap = Utils.compressImage(mPhotoFile.getPath());
        companyLogo.setImageBitmap(bitmap);


        OutputStream os = null;
        try {
            os = new BufferedOutputStream(new FileOutputStream(mPhotoFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
        try {
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    public void showAlertWithList(final String[] dataArray, String title, final TextView textView, boolean isCompanyType) {
        hideKeyboard(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setItems(dataArray, (dialog, item) -> {
            textView.setText(dataArray[item]);
            if (isCompanyType) {
                if (dataArray[item].equalsIgnoreCase("Implementer")) {
                    subCompanyType.setText("Implementer");
                    etisIndian.setOnClickListener(null );
                    subCompanyType.setOnClickListener(null);

                } else {
                    subCompanyType.setText("");
                    etisIndian.setText("");
                    subCompanyType.setOnClickListener(this);
                    etisIndian.setOnClickListener(this);

                }
            }

            textView.clearFocus();
        });
        AlertDialog alerts = builder.create();
        alerts.show();
    }

    @Override
    public void onError(String s) {
        showToast(s);

    }
}
