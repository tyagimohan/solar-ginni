package com.solarginni.SingleTonApi;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.APIBody.UserRegisteration;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.UserIdModel;
import com.solarginni.ProspectUser.GetQuotationActivity;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;
import com.solarginni.network.RestClient;

import retrofit2.Retrofit;

import static com.solarginni.Utility.AppConstants.OPTYID;


public abstract  class RegisterNewUserApi {

    private Retrofit retrofit;
    private ApiInterface mApiInterface;

    public RegisterNewUserApi( ) {
        retrofit= RestClient.getRetrofitBuilder();
        mApiInterface= retrofit.create(ApiInterface.class);
    }

    public void hit(String token, UserRegisteration model){
        mApiInterface.registerUserBySGIM(model,token).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                onComplete(response);

            }

            @Override
            public void onError(ApiError error) {
                onFailur(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                onFailur(throwable.getMessage());
            }
        });
    }



    public abstract void onComplete(CommomResponse response);
    protected abstract void onFailur(String msg);


}

