package com.solarginni.GetDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.solarginni.DBModel.Testimonial;

import java.io.Serializable;
import java.util.List;

public class Details implements Serializable {

    @SerializedName("totalSite")
    @Expose
    private Integer totalSite;
    @SerializedName("totalRequest")
    @Expose
    private Integer totalRequest;
    @SerializedName("totalImplementerSite")
    @Expose
    private Integer totalImplementerSite;
    @SerializedName("totalSolarGinieSite")
    @Expose
    private Integer totalSolarGinieSite;
    @SerializedName("lastMonthSite")
    @Expose
    private Integer lastMonthSite;

    public List<Testimonial> getTestimonialModelList() {
        return testimonialModelList;
    }

    public void setTestimonialModelList(List<Testimonial> testimonialModelList) {
        this.testimonialModelList = testimonialModelList;
    }

    @SerializedName("testimonialDtoList")
    @Expose
    private List<Testimonial> testimonialModelList;

    public String getTopImplementerStateUserId() {
        return topImplementerStateUserId;
    }

    public void setTopImplementerStateUserId(String topImplementerStateUserId) {
        this.topImplementerStateUserId = topImplementerStateUserId;
    }

    public String getTopImplCityUserId() {
        return topImplCityUserId;
    }

    public void setTopImplCityUserId(String topImplCityUserId) {
        this.topImplCityUserId = topImplCityUserId;
    }

    @SerializedName("topImplementerStateUserId")
    @Expose
    private String topImplementerStateUserId;
    @SerializedName("topImplCity")
    @Expose
    private String topImplCity;

    public String getTopImplCity() {
        return topImplCity;
    }

    public String getTopImplementerState() {
        return topImplementerState;
    }

    @SerializedName("topImplementerState")
    @Expose
    private String topImplementerState;
    @SerializedName("topImplCityUserId")
    @Expose
    private String topImplCityUserId;
    @SerializedName("currentYearSite")
    @Expose
    private Integer currentYearSite;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("testimonialCreated")
    @Expose
    public Boolean testimonialCreated;

    public Integer getNotificationCount() {
        return notificationCount;
    }

    public void setNotificationCount(Integer notificationCount) {
        this.notificationCount = notificationCount;
    }

    @SerializedName("notificationCount")
    @Expose
    private Integer notificationCount;

    public Integer getTotalSite() {
        return totalSite;
    }

    public void setTotalSite(Integer totalSite) {
        this.totalSite = totalSite;
    }

    public Integer getTotalRequest() {
        return totalRequest;
    }

    public void setTotalRequest(Integer totalRequest) {
        this.totalRequest = totalRequest;
    }

    public Integer getTotalImplementerSite() {
        return totalImplementerSite;
    }

    public void setTotalImplementerSite(Integer totalImplementerSite) {
        this.totalImplementerSite = totalImplementerSite;
    }

    public Integer getTotalSolarGinieSite() {
        return totalSolarGinieSite;
    }

    public void setTotalSolarGinieSite(Integer totalSolarGinieSite) {
        this.totalSolarGinieSite = totalSolarGinieSite;
    }

    public Integer getLastMonthSite() {
        return lastMonthSite;
    }

    public void setLastMonthSite(Integer lastMonthSite) {
        this.lastMonthSite = lastMonthSite;
    }

    public Integer getCurrentYearSite() {
        return currentYearSite;
    }

    public void setCurrentYearSite(Integer currentYearSite) {
        this.currentYearSite = currentYearSite;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }


}