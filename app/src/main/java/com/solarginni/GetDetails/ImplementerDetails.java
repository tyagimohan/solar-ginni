package com.solarginni.GetDetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.solarginni.DBModel.Testimonial;

import java.io.Serializable;
import java.util.List;

public class ImplementerDetails implements Serializable {
    @SerializedName("installSite")
    @Expose
    private Integer installSite;
    @SerializedName("installCapacity")
    @Expose
    private Double installCapacity;
    @SerializedName("respondedQuotes")
    @Expose
    private Integer respondedQuotes;
    @SerializedName("convertedOrder")
    @Expose
    private Integer convertedOrder;
    @SerializedName("pendingQuotes")
    @Expose
    private Integer pendingQuotes;
    @SerializedName("serviceRequest")
    @Expose
    private Integer serviceRequest;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("stateCount")
    @Expose
    public Integer stateCount;
    @SerializedName("testimonialCreated")
    @Expose
    public Boolean testimonialCreated;


    @SerializedName("national")
    @Expose
    public PieChartModel nationalModel;
    @SerializedName("state")
    @Expose
    public PieChartModel stateModel;

    public List<Testimonial> getTestimonialModelList() {
        return testimonialModelList;
    }

    public void setTestimonialModelList(List<Testimonial> testimonialModelList) {
        this.testimonialModelList = testimonialModelList;
    }

    @SerializedName("testimonialDtoList")
    @Expose
    private List<Testimonial> testimonialModelList;
    @SerializedName("advertisement")
    @Expose
    public List<String> advertisementList;

    public Integer getNotificationCount() {
        return notificationCount;
    }

    public void setNotificationCount(Integer notificationCount) {
        this.notificationCount = notificationCount;
    }

    @SerializedName("notificationCount")
    @Expose
    private Integer notificationCount;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getInstallSite() {
        return installSite;
    }

    public void setInstallSite(Integer installSite) {
        this.installSite = installSite;
    }

    public Double getInstallCapacity() {
        return installCapacity;
    }

    public void setInstallCapacity(Double installCapacity) {
        this.installCapacity = installCapacity;
    }

    public Integer getRespondedQuotes() {
        return respondedQuotes;
    }

    public void setRespondedQuotes(Integer respondedQuotes) {
        this.respondedQuotes = respondedQuotes;
    }

    public Integer getConvertedOrder() {
        return convertedOrder;
    }

    public void setConvertedOrder(Integer convertedOrder) {
        this.convertedOrder = convertedOrder;
    }

    public Integer getPendingQuotes() {
        return pendingQuotes;
    }

    public void setPendingQuotes(Integer pendingQuotes) {
        this.pendingQuotes = pendingQuotes;
    }

    public Integer getServiceRequest() {
        return serviceRequest;
    }

    public void setServiceRequest(Integer serviceRequest) {
        this.serviceRequest = serviceRequest;
    }


    public class PieChartModel implements Serializable{

        @SerializedName("commercial")
        @Expose
        public float commercial;
        @SerializedName("industrial")
        @Expose
        public float industrial;
        @SerializedName("institutional")
        @Expose
        public float institutional;
        @SerializedName("religious")
        @Expose
        public float religious;
        @SerializedName("residential")
        @Expose
        public float residential;
        @SerializedName("others")
        @Expose
        public float others;

    }

}
