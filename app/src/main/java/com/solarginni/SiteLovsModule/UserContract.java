package com.solarginni.SiteLovsModule;

import com.solarginni.DBModel.CommomResponse;
import com.solarginni.SiteLovsModule.SiteDetailModel.SiteLov;

public interface UserContract {

    interface View {
        void onGetSiteLov(SiteLov details);

        void onGetAccountDetails(CommomResponse response);

        void onGetLandingDetails(CommomResponse response);

        void onGetSiteDetails(CommomResponse response);

        void OnGetSGDetails(CommomResponse response);

        void onFailure(String msg);

        void onGetProspectDetails(CommomResponse commomResponse);

    }

    interface Presenter {

        void getAccount(String token);

        void getLandingDetails(String token);

        void getSiteDetails(String token);
        void getSGDetails(String token);

        void getProspectLAndingDetails(String token);


    }

    interface Interactor {

        void getSiteDetails(String token, UserContract.OnInteraction listener);

        void getAccount(String token, UserContract.OnInteraction listener);

        void getLandingDetails(String token, UserContract.OnInteraction listener);

        void getSGDetails(String token, UserContract.OnInteraction listener);

        void getProspectLAndingDetails(String token, UserContract.OnInteraction listener);



    }

    interface OnInteraction {
        void onGetSiteLov(SiteLov details);

        void onGetAccountDetails(CommomResponse response);

        void onGetLandingDetails(CommomResponse response);

        void onGetSiteDetails(CommomResponse response);

        void OnGetSGDetails(CommomResponse response);

        void onGetProspectDetails(CommomResponse commomResponse);

        void onFailure(String msg);
    }
}
