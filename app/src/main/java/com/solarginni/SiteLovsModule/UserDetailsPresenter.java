package com.solarginni.SiteLovsModule;

import com.solarginni.DBModel.CommomResponse;
import com.solarginni.SiteLovsModule.SiteDetailModel.SiteLov;
import com.solarginni.network.RestClient;

public class UserDetailsPresenter implements UserContract.Presenter, UserContract.OnInteraction {

    private UserContract.View view;
    private UserDetailInteractor interactor;


    public UserDetailsPresenter(UserContract.View view) {
        this.view = view;
        interactor = new UserDetailInteractor(RestClient.getRetrofitBuilder());
    }


    @Override
    public void getAccount(String token) {
        interactor.getAccount(token, this);

    }

    @Override
    public void getLandingDetails(String token) {
        interactor.getLandingDetails(token, this);
    }

    @Override
    public void getSiteDetails(String token) {
        interactor.getSiteDetails(token, this);

    }

    @Override
    public void getSGDetails(String token) {
                interactor.getSGDetails(token,this);
    }


    @Override
    public void getProspectLAndingDetails(String token) {
        interactor.getProspectLAndingDetails(token,this);

    }


    @Override
    public void onGetSiteLov(SiteLov details) {
        view.onGetSiteLov(details);

    }

    @Override
    public void onGetAccountDetails(CommomResponse response) {
        view.onGetAccountDetails(response);

    }

    @Override
    public void onGetLandingDetails(CommomResponse response) {
        view.onGetLandingDetails(response);

    }

    @Override
    public void onGetSiteDetails(CommomResponse response) {
        view.onGetSiteDetails(response);

    }

    @Override
    public void OnGetSGDetails(CommomResponse response) {
        view.OnGetSGDetails(response);

    }

    @Override
    public void onGetProspectDetails(CommomResponse commomResponse) {
        view.onGetProspectDetails(commomResponse);


    }


    @Override
    public void onFailure(String msg) {
        view.onFailure(msg);

    }
}
