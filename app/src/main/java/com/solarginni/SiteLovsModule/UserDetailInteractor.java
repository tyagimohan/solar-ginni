package com.solarginni.SiteLovsModule;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;

import retrofit2.Retrofit;

public class UserDetailInteractor implements UserContract.Interactor {

    private Retrofit retrofit;
    private ApiInterface mApiInterface;

    public UserDetailInteractor(Retrofit retrofit) {
        this.retrofit = retrofit;
        mApiInterface = retrofit.create(ApiInterface.class);

    }



    @Override
    public void getSiteDetails(String token, UserContract.OnInteraction listener) {
        mApiInterface.getSiteDetails(token,"All","All").enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                listener.onGetSiteDetails(response);

            }

            @Override
            public void onError(ApiError error) {
                listener.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                listener.onFailure(throwable.getMessage());

            }
        });

    }

    @Override
    public void getAccount(String token, UserContract.OnInteraction listener) {
        mApiInterface.getAccountDetails(token).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                listener.onGetAccountDetails(response);
            }

            @Override
            public void onError(ApiError error) {
                listener.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                listener.onFailure(throwable.getMessage());
            }
        });
    }

    @Override
    public void getLandingDetails(String token, UserContract.OnInteraction listener) {
        mApiInterface.getCustomerDetails(token).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                listener.onGetLandingDetails(response);
            }

            @Override
            public void onError(ApiError error) {
                listener.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                listener.onFailure(throwable.getMessage());

            }
        });

    }

    @Override
    public void getSGDetails(String token, UserContract.OnInteraction listener) {
        mApiInterface.getSGLading(token).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                listener.OnGetSGDetails(response);
            }

            @Override
            public void onError(ApiError error) {
                listener.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {

                listener.onFailure(throwable.getMessage());
            }
        });

    }

    @Override
    public void getProspectLAndingDetails(String token, UserContract.OnInteraction listener) {
        mApiInterface.getProspectLandingDetails(token).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                listener.onGetProspectDetails(response);

            }

            @Override
            public void onError(ApiError error) {
                listener.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                     listener.onFailure(throwable.getMessage());
            }
        });

    }

}
