package com.solarginni.SiteLovsModule.SiteDetailModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class LovValues implements Serializable {



    @SerializedName("values")
    @Expose
    private List<String> values = null;
    @SerializedName("keyName")
    @Expose
    private String keyName;

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    public String getKeyName() {
        return keyName;
    }

    public void setKeyName(String keyName) {
        this.keyName = keyName;
    }


}
