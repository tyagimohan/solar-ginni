package com.solarginni.SiteLovsModule.SiteDetailModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Implementer {



    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("companyName")
    @Expose
    private String name;
    @SerializedName("companyId")
    @Expose
    private CompanyId companyId;
    @SerializedName("country_code")
    @Expose
    private String countryCode;
    @SerializedName("promotionCode")
    @Expose
    private String promotionCode;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("onBoardDate")
    @Expose
    private String onBoardDate;
    @SerializedName("roles")
    @Expose
    private List<Role> roles = null;
    @SerializedName("images")
    @Expose
    private Object images;
    @SerializedName("site")
    @Expose
    private List<Object> site = null;
    @SerializedName("ratingActive")
    @Expose
    private Boolean ratingActive;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CompanyId getCompanyId() {
        return companyId;
    }

    public void setCompanyId(CompanyId companyId) {
        this.companyId = companyId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getPromotionCode() {
        return promotionCode;
    }

    public void setPromotionCode(String promotionCode) {
        this.promotionCode = promotionCode;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOnBoardDate() {
        return onBoardDate;
    }

    public void setOnBoardDate(String onBoardDate) {
        this.onBoardDate = onBoardDate;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }


    public Object getImages() {
        return images;
    }

    public void setImages(Object images) {
        this.images = images;
    }

    public List<Object> getSite() {
        return site;
    }

    public void setSite(List<Object> site) {
        this.site = site;
    }

    public Boolean getRatingActive() {
        return ratingActive;
    }

    public void setRatingActive(Boolean ratingActive) {
        this.ratingActive = ratingActive;
    }


}
