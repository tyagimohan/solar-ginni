
package com.solarginni.SiteLovsModule.SiteDetailModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PanelMake {



    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("companyName")
    @Expose
    private String companyName;
    @SerializedName("companyType")
    @Expose
    private String companyType;
    @SerializedName("industry")
    @Expose
    private String industry;
    @SerializedName("gstNumber")
    @Expose
    private Object gstNumber;
    @SerializedName("images")
    @Expose
    private Object images;
    @SerializedName("address")
    @Expose
    private Address address;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyType() {
        return companyType;
    }

    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public Object getGstNumber() {
        return gstNumber;
    }

    public void setGstNumber(Object gstNumber) {
        this.gstNumber = gstNumber;
    }

    public Object getImages() {
        return images;
    }

    public void setImages(Object images) {
        this.images = images;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }



}
