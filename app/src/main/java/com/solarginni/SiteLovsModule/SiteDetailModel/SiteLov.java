
package com.solarginni.SiteLovsModule.SiteDetailModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.solarginni.DBModel.CompanyMakeModel;

import java.io.Serializable;
import java.util.List;

public class SiteLov {
    @SerializedName("siteDetails")
    @Expose
    private SiteParam siteDetails;

    public SiteParam getSiteDetails() {
        return siteDetails;
    }

    public void setSiteDetail(SiteParam siteDetails) {
        this.siteDetails = siteDetails;
    }

    public static class SiteParam implements Serializable{


        @SerializedName("solution")
        @Expose
        private Solution solution;
        @SerializedName("siteType")
        @Expose
        private SiteType siteType;
        @SerializedName("panelMake")
        @Expose
        private List<CompanyMakeModel> panelMake = null;
        @SerializedName("panelType")
        @Expose
        private PanelType panelType;
        @SerializedName("inverterMake")
        @Expose
        private List<CompanyMakeModel> inverterMake = null;
        @SerializedName("batteryMake")
        @Expose
        private List<CompanyMakeModel> batteryMake = null;

        public List<CompanyMakeModel> getBatteryMake() {
            return batteryMake;
        }

        public void setBatteryMake(List<CompanyMakeModel> batteryMake) {
            this.batteryMake = batteryMake;
        }

        @SerializedName("implementer")
        @Expose
        private List<CompanyMakeModel> implementer = null;

        public Solution getSolution() {
            return solution;
        }

        public void setSolution(Solution solution) {
            this.solution = solution;
        }

        public SiteType getSiteType() {
            return siteType;
        }

        public void setSiteType(SiteType siteType) {
            this.siteType = siteType;
        }

        public List<CompanyMakeModel> getPanelMake() {
            return panelMake;
        }

        public void setPanelMake(List<CompanyMakeModel> panelMake) {
            this.panelMake = panelMake;
        }

        public PanelType getPanelType() {
            return panelType;
        }

        public void setPanelType(PanelType panelType) {
            this.panelType = panelType;
        }

        public List<CompanyMakeModel> getInverterMake() {
            return inverterMake;
        }

        public void setInverterMake(List<CompanyMakeModel> inverterMake) {
            this.inverterMake = inverterMake;
        }

        public List<CompanyMakeModel> getImplementer() {
            return implementer;
        }

        public void setImplementer(List<CompanyMakeModel> implementer) {
            this.implementer = implementer;
        }

    }



}
