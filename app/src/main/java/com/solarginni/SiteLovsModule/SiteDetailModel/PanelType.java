
package com.solarginni.SiteLovsModule.SiteDetailModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PanelType {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("lovType")
    @Expose
    private String lovType;
    @SerializedName("lovValues")
    @Expose
    private List<String> lovValues;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLovType() {
        return lovType;
    }

    public void setLovType(String lovType) {
        this.lovType = lovType;
    }

    public List<String> getLovValues() {
        return lovValues;
    }




}
