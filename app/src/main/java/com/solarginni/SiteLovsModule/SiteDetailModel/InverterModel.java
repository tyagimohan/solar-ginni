
package com.solarginni.SiteLovsModule.SiteDetailModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InverterModel {

    @SerializedName("id")
    @Expose
    private Object id;
    @SerializedName("lovType")
    @Expose
    private String lovType;
    @SerializedName("lovValues")
    @Expose
    private String lovValues;

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public String getLovType() {
        return lovType;
    }

    public void setLovType(String lovType) {
        this.lovType = lovType;
    }

    public String getLovValues() {
        return lovValues;
    }

    public void setLovValues(String lovValues) {
        this.lovValues = lovValues;
    }

}
