package com.solarginni.SGUser.RequestModule;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;

import retrofit2.Retrofit;

public
class AllRequestInteractor implements AllRequestContract.Interactor {

    private Retrofit retrofit;
    private ApiInterface apiInterface;


    public AllRequestInteractor(Retrofit retrofit) {
        this.retrofit = retrofit;
        apiInterface= retrofit.create(ApiInterface.class);
    }

    @Override
    public void fetchRequest(String token, String key, int page, String type, String status, AllRequestContract.OnInteraction listener) {

        apiInterface.getRequest(token,key,page,type,status).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                if (key.equalsIgnoreCase("#"))
                    listener.showRequest(response);

                else
                    listener.onSerachRequest(response);

            }

            @Override
            public void onError(ApiError error) {
                listener.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                listener.onFailure(throwable.getMessage());

            }
        });

    }
}
