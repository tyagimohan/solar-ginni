package com.solarginni.SGUser.RequestModule;

import com.solarginni.DBModel.CommomResponse;

public interface AllRequestContract {

    interface View {
        void showRequest(CommomResponse response);

        void onFailure(String msg);

        void onSerachRequest(CommomResponse response);
    }

    ;


    interface Presenter {
        void fetchRequest(String token, String key, int page, String type, String status);
    }

    interface Interactor {
        void fetchRequest(String token, String key, int page, String type, String status,OnInteraction listener);
    }

    interface OnInteraction {
        void showRequest(CommomResponse response);

        void onFailure(String msg);

        void onSerachRequest(CommomResponse response);
    }
}
