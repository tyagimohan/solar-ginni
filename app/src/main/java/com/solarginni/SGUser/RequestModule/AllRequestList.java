package com.solarginni.SGUser.RequestModule;

import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.solarginni.Base.BaseFragment;
import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.CommonModule.MainActivity;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.RequestData;
import com.solarginni.Implementer.ImplementerDetails.ImplemeterDetailsActivity;
import com.solarginni.R;
import com.solarginni.Utility.Utils;
import com.solarginni.user.QuoteRequestModule.QuoteRequestDetailsActivity;
import com.solarginni.user.RequestDetailsModule.RequestDetailsActivity;
import com.solarginni.user.customerDetail.CustomerDetailActivity;

import org.angmarch.views.NiceSpinner;

import java.io.Serializable;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static com.solarginni.Utility.AppConstants.TOKEN;

public class AllRequestList extends BaseFragment implements View.OnClickListener, AllRequestContract.View {

    private RecyclerView recyclerView;
    private NiceSpinner statusSpinner, typeSpinner;
    private LinearLayoutManager layoutManager;
    private boolean isLoading = false;
    private LinearLayout progressPagination;
    private AllRequestPresenter presenter;
    private final RecyclerView.OnScrollListener recyclerScroller = new RecyclerView.OnScrollListener() {
        @SuppressWarnings("EmptyMethod")
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

            if (dy > 0)
                if (!isLoading) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition > 0) {
                        progressPagination.setVisibility(View.VISIBLE);
                        loadMoreItems(visibleItemCount + layoutManager.findFirstVisibleItemPosition());
                    }
                } else {

                }
        }
    };
    private AllRequestAdapter adapter;
    private EditText etMobileNumber;
    private Button searchButton;
    private boolean isRefresh = false;
    private boolean isFilter = false;
    private AllRequestAdapter.onItemClick<RequestData.RequestDetail> longListenr = (data, position, view) -> {
        switch (view.getId()) {
            case R.id.customerMob:
                Bundle bundle = new Bundle();
                bundle.putBoolean("isFormasking", false);
                bundle.putString("id", data.getUserId());
                goToNextScreen(CustomerDetailActivity.class, bundle);
                break;
            case R.id.implementerName:
                bundle = new Bundle();
                bundle.putBoolean("isFormasking", false);
                bundle.putString("id", data.getCompanyId());
                goToNextScreen(ImplemeterDetailsActivity.class, bundle);
                break;
            case R.id.request_id:
                bundle = new Bundle();
                bundle.putSerializable("data", (Serializable) data);
                bundle.putBoolean("isForSG", true);
                if (data.getSiteId() != null) {
                    goToNextScreen(RequestDetailsActivity.class, bundle);
                } else {
                    goToNextScreen(QuoteRequestDetailsActivity.class, bundle);
                }


                break;
        }
    };

    @Override
    public int getLayout() {
        return R.layout.all_request_list;
    }

    @Override
    public void initViews(View view) {
        recyclerView = view.findViewById(R.id.recyclerView);
        statusSpinner = view.findViewById(R.id.status_spinner);
        typeSpinner = view.findViewById(R.id.type_spinner);
        searchButton = view.findViewById(R.id.serach_btn);
        etMobileNumber = view.findViewById(R.id.mobileNumber);
        progressPagination = (LinearLayout) view.findViewById(R.id.progressPagination);
        setupUI(view.findViewById(R.id.parent));
        setupUI(view.findViewById(R.id.recyclerView));
        adapter = new AllRequestAdapter(getContext());
        recyclerView.setAdapter(adapter);
        adapter.setOnLongClickListener(longListenr);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new BaseRecyclerAdapter.DividerItemDecoration(getContext(), RecyclerView.VERTICAL));
        presenter = new AllRequestPresenter(this);

        recyclerView.addOnScrollListener(recyclerScroller);

        view.findViewById(R.id.refresh).setOnClickListener(this);

        List<String> dataset = new LinkedList<>(Arrays.asList("All","Complaint", "Enquiry", "Billing", "Payment", "Subsidy", "Others"));
        typeSpinner.attachDataSource(dataset);
        List<String> statedataset = new LinkedList<>(Arrays.asList("All","Open", "Assigned", "Resolved", "Closed", "Re-Opened"));
        statusSpinner.attachDataSource(statedataset);

    }

    @Override
    public void setUp() {
        searchButton.setOnClickListener(this);

        statusSpinner.setOnSpinnerItemSelectedListener((parent, view, position, id) -> {
            if (Utils.hasNetwork(getContext())) {
                isFilter = true;
                showProgress("Please wait..");
                presenter.fetchRequest(securePrefManager.getSharedValue(TOKEN), "#", 0, typeSpinner.getText().toString(), "" + parent.getItemAtPosition(position));
            } else {
                showToast("Please check Internet");

            }
        });

        typeSpinner.setOnSpinnerItemSelectedListener((parent, view, position, id) -> {
            if (Utils.hasNetwork(getContext())) {
                isFilter = true;
                showProgress("Please wait..");
                presenter.fetchRequest(securePrefManager.getSharedValue(TOKEN), "#", 0, "" + parent.getItemAtPosition(position), statusSpinner.getText().toString());
            } else {
                showToast("Please check Internet");

            }
        });


        if (Utils.hasNetwork(getContext())) {
            showProgress("Please wait..");
            presenter.fetchRequest(securePrefManager.getSharedValue(TOKEN), "#", 0, "All", "All");
        } else {
            showToast("Please check Internet");

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.serach_btn:
                hideKeyboard(getActivity());
                if (Utils.hasNetwork(getContext())) {
                    if (etMobileNumber.getText().toString().trim().length() == 0) {
                        showToast("Please enter mobile");
                        return;
                    } else {
                        showProgress("Please wait...");
                        etMobileNumber.clearFocus();
                        hideKeyboard(getActivity());
                        presenter.fetchRequest(securePrefManager.getSharedValue(TOKEN), etMobileNumber.getText().toString(), 0, "All", "All");
                        typeSpinner.setSelectedIndex(0);
                        statusSpinner.setSelectedIndex(0);
                    }

                } else {
                    showToast("Please check Internet");
                }
                break;

            case R.id.refresh:
                if (Utils.hasNetwork(getContext())) {
                    showProgress("Please wait...");
                    isRefresh = true;
                    presenter.fetchRequest(securePrefManager.getSharedValue(TOKEN), "#", 0, "All", "All");

                } else {
                    showToast("Please check internet ");
                }
                break;
        }

    }

    private void loadMoreItems(int pos) {
        isLoading = true;
        int page = (pos / 10);
        presenter.fetchRequest(securePrefManager.getSharedValue(TOKEN), "#", page, typeSpinner.getText().toString(), statusSpinner.getText().toString());
    }

    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    view.requestFocus();

                    hideKeyboard(getActivity());
                    return false;
                }
            });
        }
    }

    @Override
    public void showRequest(CommomResponse response) {
        hideProgress();
        progressPagination.setVisibility(View.GONE);
        RequestData model = response.toResponseModel(RequestData.class);
        etMobileNumber.setText("");


        if (isFilter) {
            isFilter = false;
            adapter.notifyItemRangeRemoved(0, adapter.getItemCount());
            adapter.clear();
            adapter.notifyDataSetChanged();
        }

        if (model.getRequestDetails().size()== 0 && isFilter) {
            showToast("No Request Found");
            adapter.notifyItemRangeRemoved(0, adapter.getItemCount());
            adapter.clear();
            adapter.notifyDataSetChanged();
            if (isRefresh) {
                isRefresh = false;
                typeSpinner.setSelectedIndex(0);
                statusSpinner.setSelectedIndex(0);

            }

        } else {
            isLoading = false;
            if (model.getRequestDetails().size() > 0 && model.getRequestDetails().size() <= 9) {
                isLoading = true;
            }
            if (isRefresh) {
                isRefresh = false;
                adapter.notifyItemRangeRemoved(0, adapter.getItemCount());
                adapter.clear();
                adapter.notifyDataSetChanged();
                typeSpinner.setSelectedIndex(0);
                statusSpinner.setSelectedIndex(0);


            }
            int initialCount = adapter.getItemCount();
            adapter.addAllForPagination(model.getRequestDetails());
            adapter.notifyItemRangeChanged(initialCount, model.getRequestDetails().size());
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onFailure(String msg) {
        hideProgress();
        progressPagination.setVisibility(View.GONE);

        if (msg.equalsIgnoreCase("invalid token")) {
            securePrefManager.clearAppsAllPrefs();
            startActivity(new Intent(getActivity(), MainActivity.class));
            getActivity().finish();
        } else
            showToast(msg);
    }

    @Override
    public void onSerachRequest(CommomResponse response) {
        hideProgress();
        RequestData model = response.toResponseModel(RequestData.class);
        isLoading = true;
        adapter.notifyItemRangeRemoved(0, adapter.getItemCount());
        if (model.getRequestDetails().size() == 0)
            showToast("No Request Found");
        typeSpinner.setSelectedIndex(0);
        statusSpinner.setSelectedIndex(0);
        adapter.addAll(model.getRequestDetails());
        adapter.notifyDataSetChanged();
    }

}
