package com.solarginni.SGUser.RequestModule;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.DBModel.RequestData;
import com.solarginni.R;
import com.solarginni.Utility.Utils;

public class AllRequestAdapter extends BaseRecyclerAdapter<RequestData.RequestDetail, AllRequestAdapter.Holder> {


    public AllRequestAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public int getLayout(int viewType) {
        return R.layout.all_request_item;
    }

    @Override
    public Holder getViewHolder(View view, int viewType) {
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        SpannableString company = new SpannableString(getItem(i).getCompanyName());
        company.setSpan(new UnderlineSpan(), 0, getItem(i).getCompanyName().length(), 0);
        holder.implementerName.setText(company);
        holder.implementerName.setOnClickListener(v -> {
            getLongClickListener().onItemClick(getItem(i),i,holder.implementerName);
        });
        SpannableString content = new SpannableString(getItem(i).getCustomerPhone());
        content.setSpan(new UnderlineSpan(), 0, getItem(i).getCustomerPhone().length(), 0);
        holder.mobileNumber.setText(content);
        holder.mobileNumber.setOnClickListener(view -> {
            getLongClickListener().onItemClick(getItem(i),i,holder.mobileNumber);
        });

        SpannableString reqesId = new SpannableString(getItem(i).getRequestId());
        reqesId.setSpan(new UnderlineSpan(), 0,getItem(i).getRequestId().length(), 0);
        holder.request_id.setText(reqesId);

        holder.request_id.setOnClickListener(view -> {
            getLongClickListener().onItemClick(getItem(i),i,holder.request_id);
        });

        holder.requestType.setText(getItem(i).getRequestType());
        holder.requestStatus.setText(getItem(i).getRequestStatus());
        holder.raisedOn.setText(Utils.convertDate(getItem(i).getRaisedOn()));
        if (getItem(i).getRaisedOn()!=null){
            holder.resolvedOn.setText(Utils.convertDate(getItem(i).getResolvedOn()));
        }
        else {

        }
        if (getItem(i).getSiteId()!=null){
            holder.ratingImge.setVisibility(View.VISIBLE);
            if (getItem(i).isRequestRating()){
                holder.ratingImge.setBackgroundResource(R.drawable.ic_happy);
            }
            else{
                holder.ratingImge.setBackgroundResource(R.drawable.ic_happy_grey);
            }
        }
        else {
            holder.ratingImge.setVisibility(View.GONE);
        }


    }

    public class Holder extends RecyclerView.ViewHolder{

        private TextView request_id, requestType, mobileNumber, raisedOn, requestStatus;
        private TextView resolvedOn,implementerName;
        private ImageView ratingImge;

        public Holder(@NonNull View itemView) {
            super(itemView);
            request_id = itemView.findViewById(R.id.request_id);
            requestType = itemView.findViewById(R.id.requestType);
            mobileNumber = itemView.findViewById(R.id.customerMob);
            raisedOn = itemView.findViewById(R.id.raisedOn);
            ratingImge = itemView.findViewById(R.id.ratingImg);
            resolvedOn = itemView.findViewById(R.id.resolvedOn);
            requestStatus = itemView.findViewById(R.id.requestStatus);
            implementerName = itemView.findViewById(R.id.implementerName);
        }


    }

}
