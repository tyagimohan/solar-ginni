package com.solarginni.SGUser.RequestModule;

import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.RestClient;

public
class AllRequestPresenter implements AllRequestContract.OnInteraction, AllRequestContract.Presenter {

    private AllRequestInteractor interactor;
    private AllRequestContract.View view;


    public AllRequestPresenter(AllRequestContract.View view) {
        this.view = view;
        interactor = new AllRequestInteractor(RestClient.getRetrofitBuilder());
    }

    @Override
    public void fetchRequest(String token, String key, int page, String type, String status) {
        interactor.fetchRequest(token,key,page,type,status,this);

    }

    @Override
    public void showRequest(CommomResponse response) {
        view.showRequest(response);

    }

    @Override
    public void onFailure(String msg) {
        view.onFailure(msg);

    }

    @Override
    public void onSerachRequest(CommomResponse response) {
            view.onSerachRequest(response);
    }
}
