package com.solarginni.SGUser.SGModule;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;

import retrofit2.Retrofit;

public class SGInteractor implements SGUserContract.Interaction{

    private Retrofit retrofit;
    private ApiInterface apiInterface;


    public SGInteractor(Retrofit retrofit) {
        this.retrofit = retrofit;
        apiInterface= retrofit.create(ApiInterface.class);
    }

    @Override
    public void getAccountDetails(String token, SGUserContract.OnInteraction listener) {
              apiInterface.getAccountDetails(token).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
                  @Override
                  public void onSuccess(CommomResponse response) {
                      listener.onFetchAccountDeatils(response);
                  }

                  @Override
                  public void onError(ApiError error) {
                           listener.onFailure(error.getMessage());
                  }

                  @Override
                  public void onFailure(Throwable throwable) {

                      listener.onFailure(throwable.getMessage());

                  }
              });
    }

    @Override
    public void getSGDetails(String token, SGUserContract.OnInteraction listener) {
        apiInterface.getSGLading(token).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                listener.OnGetSGDetails(response);
            }

            @Override
            public void onError(ApiError error) {
                listener.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {

                listener.onFailure(throwable.getMessage());
            }
        });
    }
}
