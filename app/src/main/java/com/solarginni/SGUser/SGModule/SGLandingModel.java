package com.solarginni.SGUser.SGModule;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public  class SGLandingModel  implements Serializable {

    @SerializedName("details")
    @Expose
    public LandingData data;

    public class LandingData implements  Serializable{

        @SerializedName("customerUser")
        @Expose
        public Integer customerUser;
        @SerializedName("prospectUser")
        @Expose
        public Integer prospectUser;
        @SerializedName("implementerUser")
        @Expose
        public Integer implementerUser;
        @SerializedName("implementCompany")
        @Expose
        public Integer implementCompany;
        @SerializedName("sites")
        @Expose
        public Integer sites;
        @SerializedName("installedCapacity")
        @Expose
        public String installedCapacity;
        @SerializedName("respondedQuote")
        @Expose
        public Integer respondedQuote;
        @SerializedName("convertedOrder")
        @Expose
        public Integer convertedOrder;
        @SerializedName("pendingRequest")
        @Expose
        public Integer pendingRequest;
        @SerializedName("resolvedRequest")
        @Expose
        public Integer resolvedRequest;
        @SerializedName("requestedQuote")
        @Expose
        public Integer requestedQuote;
        @SerializedName("notificationCount")
        @Expose
        public Integer notificationCount;
    }

}
