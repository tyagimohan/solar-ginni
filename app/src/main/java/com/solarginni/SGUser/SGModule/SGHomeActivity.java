package com.solarginni.SGUser.SGModule;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.appcompat.app.AlertDialog;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.solarginni.Base.BaseActivity;
import com.solarginni.CommonModule.FAQs.FAQsActivity;
import com.solarginni.CommonModule.LogoutApi;
import com.solarginni.CommonModule.MainActivity;
import com.solarginni.CommonModule.Referral.ReferralActivity;
import com.solarginni.CommonModule.Referral.SGPromotionActivity;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.UserRoleModel;
import com.solarginni.NotificationModule.NotificationActivity;
import com.solarginni.R;
import com.solarginni.SGUser.QuoteModule.AllQuoteList;
import com.solarginni.SGUser.RequestModule.AllRequestList;
import com.solarginni.SGUser.SiteModule.AllSiteListFragment;
import com.solarginni.Utility.AppConstants;
import com.solarginni.Utility.Converter;
import com.solarginni.Utility.Utils;
import com.solarginni.user.MyProfileActivity;

import static com.solarginni.Utility.AppConstants.BO_MANAGER;
import static com.solarginni.Utility.AppConstants.MOBILE;
import static com.solarginni.Utility.AppConstants.NOTIFICATION_COUNT;
import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.TOKEN;
import static com.solarginni.Utility.AppConstants.dynamicUrl;

public class SGHomeActivity extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener, View.OnClickListener, SGUserContract.View {
    public static BottomNavigationView bottomNavigationView;
    private ImageView myAccount;
    private SGUserPresenter presenter;
    private int exitCount=0;
    private int notificationCount=0;

    @Override
    public int getLayout() {
        return R.layout.sg_home;
    }

    @Override
    public void initViews() {
        bottomNavigationView = findViewById(R.id.nav_bottom);
        myAccount = findViewById(R.id.myAccount);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        myAccount.setOnClickListener(this);

        presenter = new SGUserPresenter(this);
        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("");
        toolbar.setTitle("");
    }

    @Override
    public void setUp() {
        Bundle bundle = getIntent().getExtras();

        notificationCount = Integer.valueOf(securePrefManager.getSharedValue(NOTIFICATION_COUNT));
        invalidateOptionsMenu();
        loadFragment(new SGUserLanding(), bundle, SGUserLanding.class.getName());


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.alert_menu, menu);
        MenuItem menuItem2 = menu.findItem(R.id.notification_action);
        menuItem2.setIcon(Converter.convertLayoutToImage(SGHomeActivity.this, notificationCount, R.drawable.ic_bell));
        return true;
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case R.id.nav_home:
                if (Utils.hasNetwork(this)) {
                    showProgress("Please wait..");
                    presenter.getSGDetails(securePrefManager.getSharedValue(TOKEN));
                } else {
                    showToast(getString(R.string.internet_error));
                }
                break;

            case R.id.nav_site:
                loadFragment(new AllSiteListFragment(), null, AllSiteListFragment.class.getName());
                break;
            case R.id.nav_request:
                if (Utils.hasNetwork(this)) {
                    loadFragment(new AllRequestList(), null, AllRequestList.class.getSimpleName());
                } else {
                    showToast(getString(R.string.internet_error));

                }
                break;
            case R.id.nav_quote:
                loadFragment(new AllQuoteList(), null, AllQuoteList.class.getSimpleName());
                break;
            case R.id.nav_more:
                showFilterDialog(this);
                break;
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        exitCount++;
        if (exitCount > 1)
            super.onBackPressed();
        else {
            Handler handler = new Handler();
            handler.postDelayed(() -> exitCount = 0, 3000);
            Toast.makeText(this, "Press again to exit the application", Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.myAccount:
                if (Utils.hasNetwork(this)) {
                    showProgress("Please wait..");
                    presenter.getAccountDetails(securePrefManager.getSharedValue(TOKEN));
                } else {
                    showToast("Please wait...");

                }
        }
    }

    @Override
    public void onFetchAccountDeatils(CommomResponse response) {
        hideProgress();
        UserRoleModel userRoleModel = response.toResponseModel(UserRoleModel.class);
        securePrefManager.storeSharedValue(AppConstants.ACCOUNT_MODEL, Utils.toString(userRoleModel.getMyAccount()));
        Bundle bundle = new Bundle();
        bundle.putString("isFrom", "Landing");
        Intent intent = new Intent(SGHomeActivity.this, MyProfileActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public void onFailure(String msg) {
        hideProgress();
        showToast(msg);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.notification_action:
                startActivity(new Intent(SGHomeActivity.this, NotificationActivity.class));
                notificationCount = 0;
                invalidateOptionsMenu();
                break;
        }
        return true;
    }


    @Override
    public void OnGetSGDetails(CommomResponse response) {
        hideProgress();
        SGLandingModel model = response.toResponseModel(SGLandingModel.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("LandingData", model);
        bundle.putString("for", "landing");
        securePrefManager.storeSharedValue(NOTIFICATION_COUNT, String.valueOf(model.data.notificationCount));
        notificationCount = model.data.notificationCount;
        invalidateOptionsMenu();
        loadFragment(new SGUserLanding(), bundle, SGUserLanding.class.getName());
    }

    private void showFilterDialog(Context context) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.more_layout, null);
        Animation slideUpIn;
        slideUpIn = AnimationUtils.loadAnimation(this, R.anim.slide_in_bottom);
        dialogView.startAnimation(slideUpIn);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);
        AlertDialog alertDialog = dialogBuilder.create();
        RelativeLayout logout, faqs, addTestimonial,share,refer;

        logout = dialogView.findViewById(R.id.logout);
        share = dialogView.findViewById(R.id.share);
        refer = dialogView.findViewById(R.id.refer);
        faqs = dialogView.findViewById(R.id.faqs);
        addTestimonial = dialogView.findViewById(R.id.addTestimonial);

       faqs.setVisibility(View.GONE);

        refer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(BO_MANAGER))
                startActivity(new Intent(SGHomeActivity.this, ReferralActivity.class));
                else
                startActivity(new Intent(SGHomeActivity.this, SGPromotionActivity.class));
                alertDialog.dismiss();
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String content = "Hello Friend, I have used Solar Ginie for my Solar rooftop need and found it very interesting. Recommend you to download Solar Ginie from Android playstore " + dynamicUrl + "\n" + "Use my Promotion code " + securePrefManager.getSharedValue(MOBILE).substring(3) + " during Registration to earn Bonus points";


                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/html");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, content);
                if (sharingIntent.resolveActivity(getPackageManager()) != null)
                    startActivity(Intent.createChooser(sharingIntent, "Share Using"));
                else
                    showToast("Your device haven't any app to share this content");

                alertDialog.dismiss();

            }
        });


            addTestimonial.setVisibility(View.GONE);



        logout.setOnClickListener(view -> {
            if (Utils.hasNetwork(this)) {
                showProgress("Please wait...");
                LogoutApi api = new LogoutApi() {
                    @Override
                    public void onComplete(CommomResponse response) {
                        hideProgress();
                        securePrefManager.clearAppsAllPrefs();
                        startActivity(new Intent(SGHomeActivity.this, MainActivity.class));
                        finish();
                    }

                    @Override
                    protected void onFailur(String msg) {
                        hideProgress();
                        securePrefManager.clearAppsAllPrefs();
                        startActivity(new Intent(SGHomeActivity.this, MainActivity.class));
                        finish();

                    }
                };
                api.hit(securePrefManager.getSharedValue(TOKEN));
            } else {
                showToast("Please check Internet");

            }

            alertDialog.dismiss();
        });

        faqs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToNextScreen(FAQsActivity.class, null);
                alertDialog.dismiss();
            }
        });



        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.BOTTOM;

        window.setAttributes(wlp);

        alertDialog.show();

    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
