package com.solarginni.SGUser.SGModule;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.solarginni.Base.BaseFragment;
import com.solarginni.R;
import com.solarginni.SGUser.AllUserModule.AllUserListActivity;
import com.solarginni.SGUser.CompanyModule.CompanyListActivity;
import com.solarginni.SGUser.CompanyPriceModule.CompanyPriceActivity;
import com.solarginni.Utility.AppConstants;
import com.solarginni.Utility.Utils;

import static com.solarginni.Utility.AppConstants.BO_MANAGER;
import static com.solarginni.Utility.AppConstants.DETAILS;
import static com.solarginni.Utility.AppConstants.ROLE;

public class SGUserLanding extends BaseFragment implements View.OnClickListener {
    private TextView resolvedReq, pendingReq, convertedOrder, capacity, respondedQuote, totalSite, implenter, imlpementerUser, prospectUser, customer_user, requestedQuote;

    @Override
    public int getLayout() {
        return R.layout.sg_landing;
    }

    @Override
    public void initViews(View view) {
        resolvedReq = view.findViewById(R.id.resolvedReq);
        pendingReq = view.findViewById(R.id.pendingReq);
        convertedOrder = view.findViewById(R.id.convertedOrder);
        capacity = view.findViewById(R.id.capacity);
        respondedQuote = view.findViewById(R.id.respondedQuote);
        totalSite = view.findViewById(R.id.totalSite);
        implenter = view.findViewById(R.id.implenter);
        imlpementerUser = view.findViewById(R.id.imlpementerUser);
        prospectUser = view.findViewById(R.id.prospectUser);
        customer_user = view.findViewById(R.id.customer_user);
        requestedQuote = view.findViewById(R.id.requestedQuote);
        view.findViewById(R.id.lay4).setOnClickListener(this);
        view.findViewById(R.id.lay1).setOnClickListener(this);
        view.findViewById(R.id.lay2).setOnClickListener(this);
        view.findViewById(R.id.lay3).setOnClickListener(this);
        view.findViewById(R.id.lay5).setOnClickListener(this);
        view.findViewById(R.id.lay7).setOnClickListener(this);
        view.findViewById(R.id.lay9).setOnClickListener(this);
        view.findViewById(R.id.lay11).setOnClickListener(this);
        view.findViewById(R.id.priceCard).setOnClickListener(this);

        if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(BO_MANAGER)){
            view.findViewById(R.id.priceCard).setVisibility(View.INVISIBLE);

        }

    }

    @Override
    public void setUp() {

        SGLandingModel details;
        String isFor = getArguments().getString("for", "");


        if (isFor.trim().equalsIgnoreCase("landing")) {
            details = (SGLandingModel) getArguments().getSerializable("LandingData");
        } else {
            details = Utils.getObj(securePrefManager.getSharedValue(DETAILS), SGLandingModel.class);
        }

        resolvedReq.setText("" + details.data.resolvedRequest);
        respondedQuote.setText("" + details.data.respondedQuote);
        convertedOrder.setText("" + details.data.convertedOrder);
        totalSite.setText("" + details.data.sites);
        implenter.setText("" + details.data.implementCompany);
        imlpementerUser.setText("" + details.data.implementerUser);
        prospectUser.setText("" + details.data.prospectUser);
        customer_user.setText("" + details.data.customerUser);
        pendingReq.setText("" + details.data.pendingRequest);
        capacity.setText("" + details.data.installedCapacity);
        requestedQuote.setText("" + details.data.requestedQuote);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay4:
                goToNextScreen(CompanyListActivity.class, null);
                break;
            case R.id.lay1:
                Bundle bundle = new Bundle();
                bundle.putString("role", AppConstants.CUSTOMER_ROLE_ID);
                goToNextScreen(AllUserListActivity.class, bundle);

                break;
            case R.id.lay2:
                bundle = new Bundle();
                bundle.putString("role", AppConstants.PROSPECT_ROLE_ID);
                goToNextScreen(AllUserListActivity.class, bundle);
                break;
            case R.id.lay3:
                bundle = new Bundle();
                bundle.putString("role", AppConstants.SUPER_IMPLEMENTER_ROLE_ID);
                goToNextScreen(AllUserListActivity.class, bundle);
                break;
            case R.id.lay5:
                ((SGHomeActivity) getActivity()).bottomNavigationView.setSelectedItemId(R.id.nav_site);
                break;
            case R.id.lay7:
            case R.id.lay11:
                ((SGHomeActivity) getActivity()).bottomNavigationView.setSelectedItemId(R.id.nav_quote);
                break;
            case R.id.lay9:
                ((SGHomeActivity) getActivity()).bottomNavigationView.setSelectedItemId(R.id.nav_request);
                break;
            case R.id.priceCard:
                goToNextScreen(CompanyPriceActivity.class, null);
                break;
        }

    }
}
