package com.solarginni.SGUser.SGModule;

import com.solarginni.DBModel.CommomResponse;
import com.solarginni.SiteLovsModule.UserContract;

public interface SGUserContract {

    interface View {
        void onFetchAccountDeatils(CommomResponse response);
        void onFailure(String msg);
        void OnGetSGDetails(CommomResponse response);
    }

    interface Interaction {
        void getAccountDetails(String token,OnInteraction listener);
        void getSGDetails(String token,OnInteraction listener);

    }

    interface Presenter {
        void getAccountDetails(String token);
        void getSGDetails(String token);
    }

    interface OnInteraction {
        void onFetchAccountDeatils(CommomResponse response);
        void onFailure(String msg);
        void OnGetSGDetails(CommomResponse response);

    }

}
