package com.solarginni.SGUser.SGModule;

import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.RestClient;

public class SGUserPresenter implements SGUserContract.Presenter, SGUserContract.OnInteraction {

    private SGUserContract.View view;
    private SGInteractor interactor;


    public SGUserPresenter(SGUserContract.View view) {
        this.view = view;
        interactor = new SGInteractor(RestClient.getRetrofitBuilder());
    }

    @Override
    public void getAccountDetails(String token) {
        interactor.getAccountDetails(token, this);
    }

    @Override
    public void getSGDetails(String token) {
        interactor.getSGDetails(token,this);
    }

    @Override
    public void onFetchAccountDeatils(CommomResponse response) {
        view.onFetchAccountDeatils(response);

    }

    @Override
    public void onFailure(String msg) {
        view.onFailure(msg);

    }

    @Override
    public void OnGetSGDetails(CommomResponse response) {
      view.OnGetSGDetails(response);
    }
}
