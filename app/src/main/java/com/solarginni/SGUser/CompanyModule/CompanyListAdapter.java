package com.solarginni.SGUser.CompanyModule;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.R;

public class CompanyListAdapter extends BaseRecyclerAdapter<CompanyListModel.CompanyDt, CompanyListAdapter.Holder> {


    public CompanyListAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public int getLayout(int viewType) {
        return R.layout.company_list_item;
    }

    @Override
    public Holder getViewHolder(View view, int viewType) {
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {


        SpannableString content = new SpannableString(getItem(i).companyName);
        content.setSpan(new UnderlineSpan(), 0, getItem(i).companyName.length(), 0);
        holder.implementerName.setText(content);

        holder.implementerName.setOnClickListener(v -> {
            getLongClickListener().onItemClick(getItem(i), i, holder.implementerName);

        });

        holder.implementerId.setText(getItem(i).userId);
        if (getItem(i).companyType.equalsIgnoreCase("Implementer")){
            holder.companyType.setText(getItem(i).companyType);
        }
        else {
            holder.companyType.setText("Manufacturer - " +getItem(i).industry);
        }


        holder.phone.setText(getItem(i).spocTelephone);
        holder.state.setText(getItem(i).address.getState());

        holder.firstUserDate.setText(getItem(i).date);

        holder.userCount.setText("" + getItem(i).userCount);

        holder.editCompany.setOnClickListener(v -> {
            getLongClickListener().onItemClick(getItem(i), i, holder.editCompany);
        });


    }

    public class Holder extends RecyclerView.ViewHolder {
        private TextView firstUserDate, phone, userCount, companyType, state, implementerId, implementerName;
        private ImageView editCompany;

        public Holder(@NonNull View itemView) {
            super(itemView);
            firstUserDate = itemView.findViewById(R.id.firstUserDate);
            phone = itemView.findViewById(R.id.phone);
            userCount = itemView.findViewById(R.id.userCount);
            companyType = itemView.findViewById(R.id.companyType);
            state = itemView.findViewById(R.id.state);
            implementerId = itemView.findViewById(R.id.implementerId);
            implementerName = itemView.findViewById(R.id.implementerName);
            editCompany = itemView.findViewById(R.id.editCompany);
        }
    }

}
