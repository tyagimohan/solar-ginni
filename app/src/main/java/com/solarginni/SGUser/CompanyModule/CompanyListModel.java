package com.solarginni.SGUser.CompanyModule;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.solarginni.SiteLovsModule.SiteDetailModel.Address;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CompanyListModel implements Serializable {

    @SerializedName("details")
    @Expose
    public List<CompanyDt> data = new ArrayList<>();

    public class CompanyDt implements Serializable {
        @SerializedName("userId")
        @Expose
        public String userId;
        @SerializedName("companyName")
        @Expose
        public String companyName;
        @SerializedName("companyType")
        @Expose
        public String companyType;
        @SerializedName("industry")
        @Expose
        public String industry;
        @SerializedName("gstNumber")
        @Expose
        public String gstNumber;
        @SerializedName("mailId")
        @Expose
        public String mailId;
        @SerializedName("spocTelephone")
        @Expose
        public String spocTelephone;
        @SerializedName("deleted")
        @Expose
        public Boolean deleted;
        @SerializedName("spocName")
        @Expose
        public String spocName;

        @SerializedName("indian")
        @Expose
        public Boolean indian;
        @SerializedName("userCount")
        @Expose
        public int userCount;
        @SerializedName("date")
        @Expose
        public String date;
        @SerializedName("imageUrl")
        @Expose
        public String imageUrl;
        @SerializedName("websiteUrl")
        @Expose
        public String websiteUrl;
        @SerializedName("videoUrl")
        @Expose
        public String videoUrl;
        @SerializedName("address")
        @Expose
        public Address address;

    }
}
