package com.solarginni.SGUser.CompanyModule;

import android.content.Context;

import com.solarginni.DBModel.APIBody.CompanyModel;
import com.solarginni.DBModel.CommomResponse;

import java.io.File;

public interface CompanyContract {

    interface View {
        void onRegister();

        void onFetchCompany(CommomResponse response);

        void onSearchCompany(CommomResponse response);

        void onFailure(String msg);
    }

    interface Presenter {
        void registerImplementer(Context context, CompanyModel model, String token, File file);

        void updateCompany(Context context, CompanyModel model, String token, File file, String userId);

        void fetchCompanies(String token, String key, int page, String type, String state);
    }

    interface Interactor {
        void registerImplementer(Context context, CompanyModel model, String token, File file, CompanyContract.OnInteraction listner);

        void updateCompany(Context context, CompanyModel model, String token, File file, String userId, CompanyContract.OnInteraction listner);

        void fetchCompanies(String token, String key, int page, String type, String state, OnInteraction listner);

    }

    interface OnInteraction {
        void onRegister();

        void onFetchCompany(CommomResponse response);

        void onSearchCompany(CommomResponse response);

        void onFailure(String msg);
    }

}
