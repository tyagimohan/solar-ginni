package com.solarginni.SGUser.CompanyModule;

import android.content.Context;

import com.solarginni.DBModel.APIBody.CompanyModel;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.RestClient;

import java.io.File;

public class CompanyPresenter implements CompanyContract.Presenter, CompanyContract.OnInteraction {

    private CompanyContract.View view;
    private CompanyInteractor interactor;

    public CompanyPresenter(CompanyContract.View view) {
        this.view = view;
        interactor= new CompanyInteractor(RestClient.getRetrofitBuilder());
    }

    @Override
    public void registerImplementer(Context context, CompanyModel model, String token, File file) {
        interactor.registerImplementer(context,model,token,file,this);

    }

    @Override
    public void updateCompany(Context context, CompanyModel model, String token, File file, String userId) {
        interactor.updateCompany(context,model,token,file,userId,this);
    }

    @Override
    public void fetchCompanies(String token, String key, int page, String type, String state) {
           interactor.fetchCompanies(token,key,page,type,state,this);
    }

    @Override
    public void onRegister() {
        view.onRegister();

    }

    @Override
    public void onFetchCompany(CommomResponse response) {
        view.onFetchCompany(response);

    }

    @Override
    public void onSearchCompany(CommomResponse response) {
        view.onSearchCompany(response);
    }

    @Override
    public void onFailure(String msg) {
        view.onFailure(msg);
    }
}
