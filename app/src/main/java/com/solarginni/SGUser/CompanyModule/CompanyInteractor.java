package com.solarginni.SGUser.CompanyModule;

import android.content.Context;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.APIBody.CompanyModel;
import com.solarginni.DBModel.APIBody.ImageType;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.UserIdModel;
import com.solarginni.Utility.AppConstants;
import com.solarginni.Utility.SecurePrefManager;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;
import com.solarginni.network.MultipartParams;

import java.io.File;

import retrofit2.Retrofit;

public class CompanyInteractor implements CompanyContract.Interactor {
    private Retrofit mRetrofit;
    private ApiInterface mApiInterface;

    public CompanyInteractor(Retrofit mRetrofit) {
        this.mRetrofit = mRetrofit;
        mApiInterface = mRetrofit.create(ApiInterface.class);
    }

    @Override
    public void registerImplementer(Context context, CompanyModel model, String token, File file, CompanyContract.OnInteraction listner) {


        if (file == null) {
            mApiInterface.registerCompany(token, model).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                @Override
                public void onSuccess(CommomResponse commomResponse) {

                    //      UserIdModel model = commomResponse.toResponseModel(UserIdModel.class);
                    listner.onRegister();

                }

                @Override
                public void onError(ApiError error) {
                    listner.onFailure(error.getMessage());

                }

                @Override
                public void onFailure(Throwable throwable) {
                    listner.onFailure(throwable.getMessage());

                }
            });

        } else {
            MultipartParams multipartParams = new MultipartParams.Builder()
                    .addFile("upload", file).build();

            mApiInterface.uploadImage(multipartParams.getMap(), ImageType.COMPANY_TYPE, token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                @Override
                public void onSuccess(CommomResponse commomResponse) {
                    String profileImage = commomResponse.toResponseModel(String.class);
                    SecurePrefManager securePrefManager = new SecurePrefManager(context);
                    securePrefManager.storeSharedValue(AppConstants.PROFILE_IMAGE, profileImage);
                    model.imageUrl = profileImage;

                    mApiInterface.registerCompany(token, model).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                        @Override
                        public void onSuccess(CommomResponse commomResponse) {

                            UserIdModel model = commomResponse.toResponseModel(UserIdModel.class);
                            listner.onRegister();

                        }

                        @Override
                        public void onError(ApiError error) {
                            listner.onFailure(error.getMessage());

                        }

                        @Override
                        public void onFailure(Throwable throwable) {
                            listner.onFailure(throwable.getMessage());

                        }
                    });


                }

                @Override
                public void onError(ApiError error) {
                    listner.onFailure(error.getMessage());

                }

                @Override
                public void onFailure(Throwable throwable) {
                    listner.onFailure(throwable.getMessage());
                }
            });

        }
    }

    @Override
    public void updateCompany(Context context, CompanyModel model, String token, File file, String userId, CompanyContract.OnInteraction listner) {
        if (file == null) {
            mApiInterface.updateCompany(token, model, userId).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                @Override
                public void onSuccess(CommomResponse commomResponse) {

                    //      UserIdModel model = commomResponse.toResponseModel(UserIdModel.class);
                    listner.onRegister();

                }

                @Override
                public void onError(ApiError error) {
                    listner.onFailure(error.getMessage());

                }

                @Override
                public void onFailure(Throwable throwable) {
                    listner.onFailure(throwable.getMessage());

                }
            });

        } else {
            MultipartParams multipartParams = new MultipartParams.Builder()
                    .addFile("upload", file).build();

            mApiInterface.uploadImage(multipartParams.getMap(), ImageType.COMPANY_TYPE, token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                @Override
                public void onSuccess(CommomResponse commomResponse) {
                    String profileImage = commomResponse.toResponseModel(String.class);
                    SecurePrefManager securePrefManager = new SecurePrefManager(context);
                    securePrefManager.storeSharedValue(AppConstants.PROFILE_IMAGE, profileImage);
                    model.imageUrl = profileImage;

                    mApiInterface.updateCompany(token, model, userId).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                        @Override
                        public void onSuccess(CommomResponse commomResponse) {

                            UserIdModel model = commomResponse.toResponseModel(UserIdModel.class);
                            listner.onRegister();

                        }

                        @Override
                        public void onError(ApiError error) {
                            listner.onFailure(error.getMessage());

                        }

                        @Override
                        public void onFailure(Throwable throwable) {
                            listner.onFailure(throwable.getMessage());

                        }
                    });


                }

                @Override
                public void onError(ApiError error) {
                    listner.onFailure(error.getMessage());

                }

                @Override
                public void onFailure(Throwable throwable) {
                    listner.onFailure(throwable.getMessage());
                }
            });

        }

    }

    @Override
    public void fetchCompanies(String token, String key, int page, String type, String statem, CompanyContract.OnInteraction listner) {

        mApiInterface.getCompanyList(token, key, page, type, statem).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                if (key.equalsIgnoreCase("#"))
                    listner.onFetchCompany(response);

                else
                    listner.onSearchCompany(response);


            }

            @Override
            public void onError(ApiError error) {
                listner.onFailure(error.getMessage());
            }

            @Override
            public void onFailure(Throwable throwable) {
                listner.onFailure(throwable.getMessage());

            }
        });


    }
}
