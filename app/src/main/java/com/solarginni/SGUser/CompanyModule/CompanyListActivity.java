package com.solarginni.SGUser.CompanyModule;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.solarginni.Base.BaseActivity;
import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.Company.Companyregistration;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.Implementer.ImplementerDetails.ImplemeterDetailsActivity;
import com.solarginni.R;
import com.solarginni.Utility.Utils;

import org.angmarch.views.NiceSpinner;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.SG_ADMIN;
import static com.solarginni.Utility.AppConstants.TOKEN;

public class CompanyListActivity extends BaseActivity implements CompanyContract.View, View.OnClickListener {

    private static final int REQUEST_FOR_REGISTRATION = 5;
    private RecyclerView recyclerView;
    private NiceSpinner stateSpinnier, typeSpinner;
    private String stateType = "All", companyType = "All";
    private FloatingActionButton addCompany;
    private CompanyPresenter presenter;
    private LinearLayoutManager layoutManager;
    private boolean isLoading = false;
    private LinearLayout progressPagination;
    private final RecyclerView.OnScrollListener recyclerScroller = new RecyclerView.OnScrollListener() {
        @SuppressWarnings("EmptyMethod")
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

            if (dy > 0)
                if (!isLoading) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition > 0) {
                        progressPagination.setVisibility(View.VISIBLE);
                        loadMoreItems(visibleItemCount + layoutManager.findFirstVisibleItemPosition());
                    }
                } else {

                }
        }

    };
    private CompanyListAdapter adapter;
    private EditText companyName;
    private Button searchButton;
    private boolean isRefresh = false;
    private boolean isFilter = false;
    private CompanyListAdapter.onItemClick<CompanyListModel.CompanyDt> longListenr = (data, position, v) -> {
        switch (v.getId()) {
            case R.id.implementerName:
                Bundle bundle = new Bundle();
                bundle.putBoolean("isFormasking", false);
                bundle.putString("id", data.userId);
                if (data.companyType.equalsIgnoreCase("Implementer"))
                    bundle.putBoolean("isImlementer", false);
                else
                    bundle.putBoolean("isImlementer", true);

                goToNextScreen(ImplemeterDetailsActivity.class, bundle);
                break;
            case R.id.editCompany:
                Intent intent = new Intent(this, Companyregistration.class);
                bundle = new Bundle();
                bundle.putBoolean("isForEdit", true);
                bundle.putSerializable("data", data);
                intent.putExtras(bundle);
                startActivityForResult(intent, REQUEST_FOR_REGISTRATION);
                break;
        }


    };

    @Override
    public int getLayout() {
        return R.layout.company_list;
    }

    @Override
    public void initViews() {

        recyclerView = findViewById(R.id.recyclerView);
        stateSpinnier = findViewById(R.id.state_spinner);
        typeSpinner = findViewById(R.id.type_spinner);
        addCompany = findViewById(R.id.addCompany);
        searchButton = findViewById(R.id.serach_btn);
        companyName = findViewById(R.id.companyName);
        progressPagination = (LinearLayout) findViewById(R.id.progressPagination);


        if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SG_ADMIN)){
            addCompany.show();

        }
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new BaseRecyclerAdapter.DividerItemDecoration(this, RecyclerView.VERTICAL));
        adapter = new CompanyListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(recyclerScroller);
        adapter.setOnLongClickListener(longListenr);

        findViewById(R.id.refresh).setOnClickListener(this);
        presenter = new CompanyPresenter(this);

        List<String> dataset = new LinkedList<>(Arrays.asList("All", "Implementer", "Panel", "On-Grid Inverter", "Hybrid Inverter", "Solar Battery"));
        typeSpinner.attachDataSource(dataset);
        List<String> statedataset = new LinkedList<>(Arrays.asList(getResources().getStringArray(R.array.stateAll)));
        stateSpinnier.attachDataSource(statedataset);
        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("");
        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_FOR_REGISTRATION:
                if (resultCode == RESULT_OK)
                    findViewById(R.id.refresh).performClick();
                break;
        }
    }

    @Override
    public void setUp() {

        setupUI(findViewById(R.id.parent));
        setupUI(findViewById(R.id.recyclerView));
        addCompany.setOnClickListener(this);
        searchButton.setOnClickListener(this);

        stateSpinnier.setOnSpinnerItemSelectedListener((parent, view, position, id) -> {
            stateType = "" + parent.getItemAtPosition(position);
            if (Utils.hasNetwork(this)) {
                isFilter = true;
                showProgress("Please wait..");
                presenter.fetchCompanies(securePrefManager.getSharedValue(TOKEN), "#", 0, companyType, "" + parent.getItemAtPosition(position));
            } else {
                showToast("Please check Internet");

            }
        });

        typeSpinner.setOnSpinnerItemSelectedListener((parent, view, position, id) -> {
            companyType = "" + parent.getItemAtPosition(position);
            if (Utils.hasNetwork(this)) {
                isFilter = true;
                showProgress("Please wait..");
                presenter.fetchCompanies(securePrefManager.getSharedValue(TOKEN), "#", 0, "" + parent.getItemAtPosition(position), stateType);
            } else {
                showToast("Please check Internet");

            }
        });
        typeSpinner.setSelectedIndex(1);
        companyType= "Implementer";
        if (Utils.hasNetwork(this)) {
            showProgress("Please wait...");
            presenter.fetchCompanies(securePrefManager.getSharedValue(TOKEN), "#", 0, "Implementer", "All");
        } else {
            showToast("Please check Internet");
        }

    }

    @Override
    public void onRegister() {

    }

    @Override
    public void onFetchCompany(CommomResponse response) {
        hideProgress();
        progressPagination.setVisibility(View.GONE);

        CompanyListModel model = response.toResponseModel(CompanyListModel.class);

        companyName.setText("");


        if (isFilter) {
            isFilter = false;
            adapter.notifyItemRangeRemoved(0, adapter.getItemCount());
            adapter.clear();
            adapter.notifyDataSetChanged();
        }

        if (model.data.size()== 0 && isFilter) {
            showToast("No Company Found");
            adapter.notifyItemRangeRemoved(0, adapter.getItemCount());
            adapter.clear();
            adapter.notifyDataSetChanged();

        } else {
            isLoading = false;
            if (model.data.size() > 0 && model.data.size() <= 9) {
                isLoading = true;
            }
            if (isRefresh) {
                isRefresh = false;
                adapter.notifyItemRangeRemoved(0, adapter.getItemCount());
                adapter.clear();
                adapter.notifyDataSetChanged();
                stateSpinnier.setSelectedIndex(0);
                typeSpinner.setSelectedIndex(0);
                stateType = "All";
                companyType = "All";

            }
            int initialCount = adapter.getItemCount();
            adapter.addAllForPagination(model.data);
            adapter.notifyItemRangeChanged(initialCount, model.data.size());
            adapter.notifyDataSetChanged();
        }


    }

    @Override
    public void onSearchCompany(CommomResponse response) {
        hideProgress();
        CompanyListModel model = response.toResponseModel(CompanyListModel.class);
        isLoading = true;
        adapter.notifyItemRangeRemoved(0, adapter.getItemCount());
        if (model.data.size() == 0)
            showToast("No Company Found");
        adapter.addAll(model.data);
        adapter.notifyDataSetChanged();

    }

    @Override
    public void onFailure(String msg) {

        hideProgress();
        showToast(msg);

    }

    private void loadMoreItems(int pos) {
        isLoading = true;
        int page = (pos / 10);
        presenter.fetchCompanies(securePrefManager.getSharedValue(TOKEN), "#", page, companyType, stateType);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addCompany:
                Intent intent = new Intent(this, Companyregistration.class);
                Bundle bundle = new Bundle();
                bundle.putBoolean("isForEdit", false);
                intent.putExtras(bundle);
                startActivityForResult(intent, REQUEST_FOR_REGISTRATION);
                break;
            case R.id.serach_btn:
                hideKeyboard(CompanyListActivity.this);
                if (Utils.hasNetwork(this)) {
                    if (companyName.getText().toString().trim().length() == 0) {
                        showToast("Please enter keyword");
                        return;
                    } else {
                        showProgress("Please wait...");
                        companyName.clearFocus();
                        hideKeyboard(CompanyListActivity.this);
                        presenter.fetchCompanies(securePrefManager.getSharedValue(TOKEN), companyName.getText().toString(), 0, "All", "All");
                        stateSpinnier.setSelectedIndex(0);
                        typeSpinner.setSelectedIndex(0);
                    }

                } else {
                    showToast("Please check Internet");
                }
                break;

            case R.id.refresh:
                if (Utils.hasNetwork(this)) {
                    showProgress("Please wait...");
                    isRefresh = true;
                    presenter.fetchCompanies(securePrefManager.getSharedValue(TOKEN), "#", 0, "All", "All");

                } else {
                    showToast("Please check internet ");
                }
                break;
        }
    }

    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    view.requestFocus();
                    findViewById(R.id.serchView).clearFocus();
                    hideKeyboard(CompanyListActivity.this);
                    return false;
                }
            });
        }
    }
}
