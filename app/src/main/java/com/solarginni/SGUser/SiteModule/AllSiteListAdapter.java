package com.solarginni.SGUser.SiteModule;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.DBModel.SiteDetailsModel.SiteDetails;
import com.solarginni.R;
import com.solarginni.Utility.Utils;

public class AllSiteListAdapter extends BaseRecyclerAdapter<SiteDetails, AllSiteListAdapter.Holder> {


    public AllSiteListAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public int getLayout(int viewType) {
        return R.layout.all_site_item;
    }

    @Override
    public Holder getViewHolder(View view, int viewType) {
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        String concatinatedString = getItem(i).getSiteId() + "-" + getItem(i).getSolutionType();
        SpannableString siteID = new SpannableString(concatinatedString);

        siteID.setSpan(new UnderlineSpan(), 0, concatinatedString.length(), 0);
        holder.siteID.setText(siteID);
        holder.siteID.setOnClickListener(v -> {
            getLongClickListener().onItemClick(getItem(i), i, holder.siteID);
        });

        if (getItem(i).getImageUrl().get(0)!=null){
            holder.siteImage.setVisibility(View.VISIBLE);
        }else {
            holder.siteImage.setVisibility(View.GONE);

        }

        SpannableString mobile = new SpannableString(getItem(i).getPhone());
        mobile.setSpan(new UnderlineSpan(), 0, getItem(i).getPhone().length(), 0);

        if (getItem(i).getCustomerUserId() != null) {
            holder.customerMob.setText(mobile);
            holder.customerMob.setOnClickListener(v -> {
                getLongClickListener().onItemClick(getItem(i), i, holder.customerMob);
            });

        } else {
            holder.customerMob.setText(getItem(i).getPhone());
        }

        if (getItem(i).status) {
            holder.status.setText("Status : Active");
        } else {
            holder.status.setText("Status : Inactive");
        }

        holder.siteSize.setText(getItem(i).getSiteSize());
        holder.state.setText(getItem(i).getAddress().getState());
        holder.siteType.setText(getItem(i).getSiteType());
        if (getItem(i).getAmcEnd() != null) {
            holder.amcTill.setText(Utils.convertDate(getItem(i).getAmcEnd()));
            holder.amcLay.setVisibility(View.VISIBLE);

        } else {
            holder.amcLay.setVisibility(View.GONE);
        }

        if (getItem(i).panelLogo != null) {
            holder.panelMake.setVisibility(View.VISIBLE);

            ImageLoader.getInstance().displayImage(getItem(i).panelLogo, holder.panelMake);
        } else {
            holder.panelMake.setVisibility(View.GONE);
        }
        if (getItem(i).getInverter().get(0).inverterLogo != null) {
            holder.inverterMake.setVisibility(View.VISIBLE);
            ImageLoader.getInstance().displayImage(getItem(i).getInverter().get(0).inverterLogo, holder.inverterMake);
        } else {
            holder.inverterMake.setVisibility(View.GONE);
        }
        if (getItem(i).getBattery() != null) {
            holder.batteryMake.setVisibility(View.VISIBLE);
            holder.batteryMakeLay.setVisibility(View.VISIBLE);


            ImageLoader.getInstance().displayImage(getItem(i).getBattery().batteryLogo, holder.batteryMake);
        } else {
            holder.batteryMakeLay.setVisibility(View.GONE);
            holder.batteryMake.setVisibility(View.GONE);
        }


        holder.address.setText(getItem(i).getAddress().getAddressLine1() + ", " + getItem(i).getAddress().getAddressLine2() + ", " + getItem(i).getAddress().getCity()
                + ", " + getItem(i).getAddress().getState() + ", " + getItem(i).getAddress().getCountry());

        holder.installedOn.setText(Utils.convertDate(getItem(i).getInstalledOn()));
        SpannableString content = new SpannableString(getItem(i).getInstalledBy());
        content.setSpan(new UnderlineSpan(), 0, getItem(i).getInstalledBy().length(), 0);
        holder.implementerName.setText(content);

        holder.implementerName.setOnClickListener(v -> {
            getLongClickListener().onItemClick(getItem(i), i, holder.implementerName);
        });

        if (getItem(i).getSiteRated()) {
            holder.rating.setText("Rating : Rated");
        } else {
            holder.rating.setText("Rating : Pending");

        }
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView siteID, implementerName, siteSize, rating, siteType, state, customerMob, status, address, amcTill, installedOn;
        RelativeLayout cardView1, cardView2;
        View aniview;
        LinearLayout amcLay,siteImage,batteryMakeLay;
        ImageView batteryMake, panelMake, inverterMake;


        public Holder(@NonNull View itemView) {
            super(itemView);
            aniview = itemView;
            siteID = itemView.findViewById(R.id.siteId);
            implementerName = itemView.findViewById(R.id.implementerName);
            rating = itemView.findViewById(R.id.rating);
            state = itemView.findViewById(R.id.state);
            address = itemView.findViewById(R.id.address);
            customerMob = itemView.findViewById(R.id.customerMob);
            status = itemView.findViewById(R.id.status);
            siteType = itemView.findViewById(R.id.siteType);
            installedOn = itemView.findViewById(R.id.installedOn);
            amcTill = itemView.findViewById(R.id.amc);
            amcLay = itemView.findViewById(R.id.amcLay);
            siteSize = itemView.findViewById(R.id.siteSize);
            inverterMake = itemView.findViewById(R.id.inverterMake);
            batteryMakeLay = itemView.findViewById(R.id.batteryMakeLay);

            panelMake = itemView.findViewById(R.id.panelMake);
            batteryMake = itemView.findViewById(R.id.batteryMake);
            cardView1 = itemView.findViewById(R.id.card1);
            cardView2 = itemView.findViewById(R.id.card2);
            itemView.findViewById(R.id.showmore).setOnClickListener(this);
            itemView.findViewById(R.id.showless).setOnClickListener(this);
            itemView.findViewById(R.id.editSite).setOnClickListener(this);
            siteImage = itemView.findViewById(R.id.siteImage);
            siteImage.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.siteImage:
                case R.id.editSite:
                    getLongClickListener().onItemClick(getItem(getAdapterPosition()), getAdapterPosition(), v);
                    break;
                case R.id.showmore:
                case R.id.showless:
                    if (cardView2.getVisibility() == View.VISIBLE) {
                        cardView2.setVisibility(View.GONE);
                        cardView1.setVisibility(View.VISIBLE);
                    } else {
                        cardView2.setVisibility(View.VISIBLE);
                        cardView1.setVisibility(View.GONE);
                    }
                    Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.rotate_anim);
                    aniview.startAnimation(animation);

                    //     getListener().onClick(getItem(getAdapterPosition()), getAdapterPosition());
                    break;
            }

        }
    }
}
