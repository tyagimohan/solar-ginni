package com.solarginni.SGUser.SiteModule;

import android.view.View;
import android.widget.EditText;

import com.solarginni.Base.BaseFragment;
import com.solarginni.DBModel.SiteDetailsModel.SiteDetails;
import com.solarginni.R;
import com.solarginni.Utility.Utils;

public class SiteDetailsFragment1 extends BaseFragment {

    public EditText tvSolution, tvSiteType, tvConnectionID,tvPhase,tvSiteSize,installedOn,amc,siteStatus,rated;
    public  EditText tvSiteId;

    @Override
    public int getLayout() {
        return R.layout.site_details1;
    }

    @Override
    public void initViews(View view) {
        tvSiteId = view.findViewById(R.id.tvSiteId);
        tvSolution = view.findViewById(R.id.solution);
        tvSiteType = view.findViewById(R.id.siteType);
        tvPhase = view.findViewById(R.id.phase);
        tvConnectionID = view.findViewById(R.id.connectionId);
        tvSiteSize = view.findViewById(R.id.siteSize);
        installedOn = view.findViewById(R.id.installedOn);
        siteStatus = view.findViewById(R.id.siteStatus);
        amc = view.findViewById(R.id.amc);
        rated = view.findViewById(R.id.rated);

        disableSuggestion(tvSiteType);
        disableSuggestion(tvSolution);
        disableSuggestion(tvPhase);
        disableSuggestion(tvConnectionID);
        disableSuggestion(tvSiteSize);
        disableSuggestion(tvSiteId);
        disableSuggestion(rated);
        disableSuggestion(amc);
        disableSuggestion(siteStatus);
        disableSuggestion(installedOn);
    }

    @Override
    public void setUp() {

        SiteDetails details= (SiteDetails) getArguments().getSerializable("data");

        tvSiteSize.setText(details.getSiteSize());
        tvSiteId.setText(details.getSiteId());
        tvConnectionID.setText(details.getConnectionId());
        tvPhase.setText(details.getPhase());
        tvSolution.setText(details.getSolutionType());
        tvSiteType.setText(details.getSiteType());
        installedOn.setText(Utils.convertDate(details.getInstalledOn()));
        amc.setText(Utils.convertDate(details.getAmcEnd()));
        siteStatus.setText(details.status?"Active":"Inactive");
        rated.setText(details.getSiteRated()?"Rated":"Pending");
    }
}
