package com.solarginni.SGUser.SiteModule;

import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.RestClient;

public class AllSitePresenter implements AllSiteContract.Presenter, AllSiteContract.OnInteraction {

    private AllSiteContract.View view;
    private AllSiteInteractor interactor;


    public AllSitePresenter(AllSiteContract.View view) {
        this.view = view;
        interactor = new AllSiteInteractor(RestClient.getRetrofitBuilder());
    }

    @Override
    public void fetchSite(String token, String key, int page, String siteType, String state) {
        interactor.fetchSite(token, key, page, siteType, state, this);

    }

    @Override
    public void failure(String msg) {
        view.failure(msg);

    }

    @Override
    public void showSites(CommomResponse response) {
        view.showSites(response);

    }

    @Override
    public void onSearchSite(CommomResponse response) {
        view.onSearchSite(response);

    }
}
