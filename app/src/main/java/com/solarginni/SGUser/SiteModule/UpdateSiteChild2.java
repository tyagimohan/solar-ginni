package com.solarginni.SGUser.SiteModule;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.view.View;
import android.widget.EditText;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.solarginni.Base.BaseFragment;
import com.solarginni.CustomizeView.CompanyListDialog;
import com.solarginni.DBModel.CompanyMakeModel;
import com.solarginni.DBModel.SiteDetailsModel.SiteDetails;
import com.solarginni.R;
import com.solarginni.SiteLovsModule.SiteDetailModel.SiteLov;
import com.solarginni.Utility.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.solarginni.Utility.AppConstants.BO_MANAGER;
import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.SG_ADMIN;
import static com.solarginni.Utility.AppConstants.SITELOVS;

public class UpdateSiteChild2 extends BaseFragment implements View.OnClickListener {
    private static final int REQUEST_FOR_PLACE = 2;
    public static Double lat, lng;
    private final Calendar myCalendar = Calendar.getInstance();
    public static  EditText tvInstalledBy, city, state, country, pincode, customerId, implementerId,etLine1,addLine2;
    public EditText customerMob;
    private List<CompanyMakeModel> installedByArray = new ArrayList<>();
    private SiteLov siteLov;

    private Geocoder mGeocoder;

    @Override
    public int getLayout() {
        return R.layout.update_site2;
    }

    @Override
    public void initViews(View view) {
        tvInstalledBy = view.findViewById(R.id.installedBy);
        etLine1 = view.findViewById(R.id.etLine1);
        addLine2 = view.findViewById(R.id.line2);
        state = view.findViewById(R.id.state);
        city = view.findViewById(R.id.cityName);
        country = view.findViewById(R.id.country);
        pincode = view.findViewById(R.id.pincode);
        implementerId = view.findViewById(R.id.implementerId);
        customerId = view.findViewById(R.id.customerId);
        customerMob = view.findViewById(R.id.customerMob);

        disableSuggestion(tvInstalledBy);
        disableSuggestion(city);
        disableSuggestion(etLine1);
        disableSuggestion(addLine2);
        disableSuggestion(country);
        disableSuggestion(state);
        disableSuggestion(pincode);
    }

    @Override
    public void setUp() {


        siteLov = Utils.getObj(securePrefManager.getSharedValue(SITELOVS), SiteLov.class);
        addLine2.setOnClickListener(this);
        installedByArray.addAll(siteLov.getSiteDetails().getImplementer());

        if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SG_ADMIN)|securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(BO_MANAGER)){
            tvInstalledBy.setOnClickListener(this);
            implementerId.setVisibility(View.VISIBLE);



        }
        else {
            implementerId.setVisibility(View.GONE);

        }

        SiteDetails details = (SiteDetails) getArguments().getSerializable("data");


        etLine1.setText(details.getAddress().getAddressLine1());
        addLine2.setText(details.getAddress().getAddressLine2());
        city.setText(details.getAddress().getCity());
        state.setText(details.getAddress().getState());
        country.setText(details.getAddress().getCountry());
        pincode.setText(details.getAddress().getPinCode());
        customerId.setText(details.getCustomerUserId());
        customerMob.setText(details.getPhone());
        tvInstalledBy.setText(details.getInstalledBy());
        implementerId.setText(details.getCompanyId());
        lat=details.getAddress().getLatitude();
        lng=details.getAddress().getLongitude();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.installedBy:
                CompanyListDialog.show(getActivity(), installedByArray, item -> {
                    tvInstalledBy.setText(item.getCompanyName());
                    implementerId.setText(item.getCompanyId());

                });
                break;
            case R.id.line2:
                openPlacePicker();
                break;
        }

    }

    private void openPlacePicker() {

        if (!Places.isInitialized()) {
            Places.initialize(getContext(), getString(R.string.google_api));
        }
        List<Place.Field> fields = Arrays.asList(Place.Field.ADDRESS_COMPONENTS, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.ADDRESS_COMPONENTS, Place.Field.LAT_LNG);

        // Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).build(Objects.requireNonNull(getActivity()));
        startActivityForResult(intent, REQUEST_FOR_PLACE);
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_FOR_PLACE:
                if (resultCode == RESULT_OK) {
                    hideKeyboard(getActivity());
                    Place place = Autocomplete.getPlaceFromIntent(data);
                    addLine2.setText(place.getName());
                    LatLng latLng = place.getLatLng();
                    lat = latLng.latitude;
                    lng = latLng.longitude;
                    try {
                        getCityNameByCoordinates(latLng.latitude, latLng.longitude);
                    } catch (IOException e) {
                        e.printStackTrace();

                    }


                    //  onParamSelected();
                } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                    Status status = Autocomplete.getStatusFromIntent(data);


                } else if (resultCode == RESULT_CANCELED) {

                }
                break;

        }
    }

    private void getCityNameByCoordinates(double lat, double lon) throws IOException {
        mGeocoder = new Geocoder(getContext(), Locale.getDefault());
        List<Address> addresses = mGeocoder.getFromLocation(lat, lon, 1);
        if (addresses != null && addresses.size() > 0) {
            state.setText(addresses.get(0).getAdminArea());
            city.setText(addresses.get(0).getSubAdminArea());
            country.setText(addresses.get(0).getCountryName());
            pincode.setText(addresses.get(0).getPostalCode());

        }

    }
}
