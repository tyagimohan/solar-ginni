package com.solarginni.SGUser.SiteModule;

import com.solarginni.DBModel.CommomResponse;

public interface AllSiteContract {

    interface View {
        void failure(String msg);

        void showSites(CommomResponse response);

        void onSearchSite(CommomResponse response);

    }

    interface Presenter {
        void fetchSite(String token, String key, int page, String siteType, String state);

    }

    interface Interactor {
        void fetchSite(String token, String key, int page, String siteType, String state, OnInteraction listener);

    }

    interface OnInteraction {
        void failure(String msg);

        void showSites(CommomResponse response);

        void onSearchSite(CommomResponse response);
    }
}
