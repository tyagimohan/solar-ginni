package com.solarginni.SGUser.SiteModule;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AlertDialog;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kbeanie.multipicker.api.CacheLocation;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;
import com.solarginni.Base.BaseActivity;
import com.solarginni.CommonModule.MainActivity;
import com.solarginni.CustomizeView.CustomPager;
import com.solarginni.DBModel.APIBody.SiteRegistrationModel;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.SiteDetailsModel.SiteDetails;
import com.solarginni.R;
import com.solarginni.SiteLovsModule.SiteDetailModel.Address;
import com.solarginni.SiteLovsModule.SiteDetailModel.SiteLov;
import com.solarginni.SiteRegistrationModule.SiteContract;
import com.solarginni.SiteRegistrationModule.SitePresenter;
import com.solarginni.Utility.AppConstants;
import com.solarginni.Utility.Permissions;
import com.solarginni.Utility.Utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import static com.solarginni.Utility.AppConstants.TOKEN;

public class UpdateSiteActivity extends BaseActivity implements View.OnClickListener, ImagePickerCallback, SiteContract.View, UpdateSiteChild1.SolutionListener {
    private static final int REQUEST_CODE_FOR_PERMISSION = 2;
    public CustomPager viewPager;
    private TextView addImage;
    private TabLayout tabIndicator;
    private ImagePicker mImagePicker;
    private CameraImagePicker mCameraImagePicker;
    private ViewPagerAdapter adapter;
    private ImageView siteImage;
    private File mPhotoFile;
    private Button submitBtn;
    private ImageView preBtn, nextBtn;
    private SitePresenter presenter;
    private String siteId;


    @Override
    public int getLayout() {
        return R.layout.edit_site;
    }

    @Override
    public void initViews() {
        viewPager = findViewById(R.id.viewPager);
        tabIndicator = findViewById(R.id.tabIndicator);
        addImage = findViewById(R.id.addImage);
        submitBtn = findViewById(R.id.submitBtn);
        preBtn = findViewById(R.id.prevBtn);
        siteImage = findViewById(R.id.site_image);

        nextBtn = findViewById(R.id.nextBtn);
        adapter = new ViewPagerAdapter(getSupportFragmentManager());

        mImagePicker = new ImagePicker(this);
        mImagePicker.setCacheLocation(CacheLocation.INTERNAL_APP_DIR);
        mImagePicker.setImagePickerCallback(this);
        mImagePicker.shouldGenerateThumbnails(false);
        mImagePicker.allowMultiple();
        mImagePicker.shouldGenerateMetadata(false);
        mCameraImagePicker = new CameraImagePicker(this);
        mCameraImagePicker.setCacheLocation(CacheLocation.INTERNAL_APP_DIR);
        mCameraImagePicker.setImagePickerCallback(this);
        mCameraImagePicker.shouldGenerateThumbnails(false);
        mCameraImagePicker.shouldGenerateMetadata(false);
        preBtn.setOnClickListener(this);
        nextBtn.setOnClickListener(this);
        tabIndicator.clearOnTabSelectedListeners();
        tabIndicator.setEnabled(false);
        viewPager.disableScroll(false);
        presenter = new SitePresenter(this);


        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("");
        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });


        setupUI(viewPager);


    }

    @Override
    public void setUp() {
        if (Utils.hasNetwork(this)) {
            showProgress("Please Wait...");
            presenter.getSiteLov(securePrefManager.getSharedValue(TOKEN));
        } else {
            showToast("Please Check Internet");
            finish();
        }
        SiteDetails details =  (SiteDetails) getIntent().getExtras().getSerializable("data");
        siteId= details.getSiteId();
        if (details.getImageUrl().get(0) == null) {
            siteImage.setVisibility(View.GONE);

        } else {
            siteImage.setVisibility(View.VISIBLE);
            Glide.with(this).load(details.getImageUrl().get(0)).into(siteImage);
        }

        addImage.setOnClickListener(this);
        submitBtn.setOnClickListener(this);
        submitBtn.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.prevBtn:

                viewPager.setCurrentItem(getItem(-1), true);

                break;
            case R.id.nextBtn:


                viewPager.setCurrentItem(getItem(+1), true);


                break;

            case R.id.addImage:
                if (checkPermissionAPI23()) {
                    getImagePicker();
                }
                break;
            case R.id.submitBtn:
                hideKeyboard(this);
                UpdateSiteChild3.prepareInverterLis();
                SiteRegistrationModel model = new SiteRegistrationModel();
                if (UpdateSiteChild3.isListPrepared) {
                    model.setInverterModelList(UpdateSiteChild3.lists);
                } else {
                    showToast("Please Fill * marked Field");
                    return;
                }

                if ( UpdateSiteChild1.tvSiteType.getText().length() == 0
                        || UpdateSiteChild1.tvSolution.getText().length() == 0 || UpdateSiteChild1.tvSiteSize.getText().length() == 0 | UpdateSiteChild1.tvInstalledOn.getText().toString().length() == 0) {
                    viewPager.setCurrentItem(0, true);
                    showToast("Please Fill * marked Field");
                    return;
                } else if (UpdateSiteChild2.tvInstalledBy.getText().length() == 0) {
                    viewPager.setCurrentItem(1, true);
                    showToast("Please Fill * marked Field");
                    return;

                } else if ((UpdateSiteChild2.etLine1.getText().length() == 0 || UpdateSiteChild2.pincode.getText().length() == 0)) {
                    showToast("Please Fill * marked Field");
                    viewPager.setCurrentItem(1, true);
                    return;
                } else if (UpdateSiteChild3.tvPanelMake.getText().length() == 0) {
                    showToast("Please Fill * marked Field");
                    viewPager.setCurrentItem(2, true);
                    return;
                } else if (UpdateSiteChild1.tvSolution.getText().toString().equalsIgnoreCase("Hybrid") && (UpdateSiteChild3.batteryId.length() == 0)) {
                    viewPager.setCurrentItem(2, true);
                    showToast("Please Fill * marked Field");
                    return;
                } else {
                    if (Utils.hasNetwork(this)) {

                        showProgress("Please wait");


                        model.setAmctill(UpdateSiteChild1.amc.getText().toString());
                        model.setConnectionId(UpdateSiteChild1.tvConnectionID.getText().toString().trim().equalsIgnoreCase("")?null:UpdateSiteChild1.tvConnectionID.getText().toString().trim());
                        model.setInstalledBy(UpdateSiteChild2.implementerId.getText().toString());
                        model.setInstalledOn(UpdateSiteChild1.tvInstalledOn.getText().toString());

                        model.setPanelSize(UpdateSiteChild3.etPanelSize.getText().toString());
                        model.setPanelType(UpdateSiteChild3.tvPanelType.getText().toString());
                        model.setPhase(UpdateSiteChild1.tvPhase.getText().toString());

                        model.setSiteSize(UpdateSiteChild1.tvSiteSize.getText().toString());
                        model.setBatteryCompanyId(UpdateSiteChild3.batteryId);
                        model.setBatterySerial(UpdateSiteChild3.batterySerial.getText().toString());
                        model.setBatterySize(UpdateSiteChild3.batterySize.getText().toString());
                        model.setSolution(UpdateSiteChild1.tvSolution.getText().toString());
                        model.setPanelMake(UpdateSiteChild3.panelMakeId);
                        model.setSiteType(UpdateSiteChild1.tvSiteType.getText().toString());
                        model.status = UpdateSiteChild1.siteStatus.getText().toString().equalsIgnoreCase("Active") ? true : false;
                        Address address = new Address();
                        address.setAddressLine1(UpdateSiteChild2.etLine1.getText().toString());
                        address.setAddressLine2(UpdateSiteChild2.addLine2.getText().toString());
                        address.setCity(UpdateSiteChild2.city.getText().toString());
                        address.setCountry(UpdateSiteChild2.country.getText().toString());
                        address.setState(UpdateSiteChild2.state.getText().toString());
                        address.setPinCode(UpdateSiteChild2.pincode.getText().toString());
                        address.setLatitude(UpdateSiteChild2.lat);
                        address.setLongitude(UpdateSiteChild2.lng);
                        model.setAddresSame(false);
                        model.setAddress(address);

                        presenter.update(this, model, securePrefManager.getSharedValue(TOKEN),siteId, mPhotoFile);

                    } else {
                        showToast(getString(R.string.internet_error));
                    }
                }


                break;
        }

    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Picker.PICK_IMAGE_DEVICE:
                if (resultCode == RESULT_OK) {
                    mImagePicker.submit(data);
                } else {

                }
                break;

            case Picker.PICK_IMAGE_CAMERA:
                if (resultCode == RESULT_OK) {
                    mCameraImagePicker.submit(data);
                } else {

                }
                break;
        }

    }

    @Override
    public void onImagesChosen(List<ChosenImage> list) {
        siteImage.setVisibility(View.VISIBLE);
        mPhotoFile = new File(list.get(0).getOriginalPath());
        Bitmap bitmap = Utils.compressImage(mPhotoFile.getPath());
        siteImage.setImageBitmap(bitmap);


        OutputStream os = null;
        try {
            os = new BufferedOutputStream(new FileOutputStream(mPhotoFile));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
        try {
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onError(String s) {
        showToast(s);

    }

    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    ///  view.requestFocus();
                    hideKeyboard();
                    view.clearFocus();
                    return false;
                }
            });
        }
    }

    @Override
    public void OnRegisterSite(CommomResponse response) {

    }

    @Override
    public void onSiteUpdated(CommomResponse response) {
        hideProgress();
        showToast("Site Updated");
        finish();

    }

    @Override
    public void onSiteRated() {

    }

    @Override
    public void OnFailure(String msg) {
        hideProgress();
        if (msg.equalsIgnoreCase("invalid token")) {
            securePrefManager.clearAppsAllPrefs();
            startActivity(new Intent(UpdateSiteActivity.this, MainActivity.class));
            finish();
        } else
            showToast(msg);

    }

    @Override
    public void onGetSiteLovs(CommomResponse siteLov) {
        hideProgress();
        SiteLov details = siteLov.toResponseModel(SiteLov.class);
        securePrefManager.storeSharedValue(AppConstants.SITELOVS, Utils.toString(details));
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(adapter);
        viewPager.setOffscreenPageLimit(2);
        tabIndicator.setupWithViewPager(viewPager);
        tabIndicator.setSelected(false);

    }

    private boolean checkPermissionAPI23() {
        boolean isPermissionRequired = false;
        List<String> permissionArray = new ArrayList<>();

        if (Permissions.getInstance().hasCameraPermission(this).permissions.size() > 0) {
            permissionArray.add(Manifest.permission.CAMERA);
            isPermissionRequired = true;
        }

        if (Permissions.getInstance().checkReadWritePermissions(this).permissions.size() > 0) {
            isPermissionRequired = true;
            permissionArray.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            permissionArray.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }

        if (isPermissionRequired) {
            Permissions.getInstance().addPermission(permissionArray).askForPermissions(this, REQUEST_CODE_FOR_PERMISSION);
            return false;
        }
        return true;
    }

    private void getImagePicker() {
        CharSequence[] options = getResources().getStringArray(R.array.image_picker_options);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(options, (dialog, which) -> {
            switch (which) {
                case 0:
                    getImageFromCamera();
                    break;
                case 1:
                    getImageFromGallery();
                    break;
                default:
                    break;
            }
        });
        builder.show();
    }

    private void getImageFromGallery() {
        mImagePicker.pickImage();

    }

    private void getImageFromCamera() {

        mCameraImagePicker.pickImage();

    }

    @Override
    public void selectHybrid(boolean isHybrid) {
        if (isHybrid) {
            UpdateSiteChild3.batteryLayout.setVisibility(View.VISIBLE);
        } else {
            UpdateSiteChild3.batteryLayout.setVisibility(View.GONE);

        }
    }

    public class ViewPagerAdapter extends FragmentStatePagerAdapter implements ViewPager.OnPageChangeListener {

        private Fragment[] childFragments;

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
            childFragments = new Fragment[]{
                    new UpdateSiteChild1(), //0
                    new UpdateSiteChild2(), //1
                    new UpdateSiteChild3() //2
            };
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            super.setPrimaryItem(container, position, object);
            int mCurrentPosition = -1;
            if (position != mCurrentPosition) {
                Fragment fragment = (Fragment) object;
                CustomPager pager = (CustomPager) container;
                if (fragment != null && fragment.getView() != null) {
                    mCurrentPosition = position;
                    pager.measureCurrentView(fragment.getView());
                }
            }
        }


        @Override
        public Fragment getItem(int position) {

            childFragments[position].setArguments(getIntent().getExtras());

            return childFragments[position];
        }

        @Override
        public int getCount() {
            return childFragments.length; //3 items
        }

        @Override
        public void onPageScrolled(int i, float v, int i1) {
            hideKeyboard(UpdateSiteActivity.this);

        }

        @Override
        public void onPageSelected(int i) {

            if (i == 2) {
                submitBtn.setVisibility(View.VISIBLE);
                nextBtn.setVisibility(View.GONE);
            } else {
                nextBtn.setVisibility(View.VISIBLE);
                submitBtn.setVisibility(View.GONE);
            }
            if (i == 0) {
                preBtn.setVisibility(View.GONE);
            } else {
                preBtn.setVisibility(View.VISIBLE);
            }

        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    }
}
