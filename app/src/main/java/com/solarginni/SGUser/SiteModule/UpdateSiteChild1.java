package com.solarginni.SGUser.SiteModule;

import android.app.DatePickerDialog;
import android.content.Context;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.solarginni.Base.BaseFragment;
import com.solarginni.DBModel.SiteDetailsModel.SiteDetails;
import com.solarginni.R;
import com.solarginni.SiteLovsModule.SiteDetailModel.SiteLov;
import com.solarginni.Utility.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import static com.solarginni.Utility.AppConstants.BO_MANAGER;
import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.SG_ADMIN;
import static com.solarginni.Utility.AppConstants.SITELOVS;

public class UpdateSiteChild1 extends BaseFragment implements View.OnClickListener {

    public static EditText tvSolution, tvSiteType, tvConnectionID, tvPhase, tvSiteSize, installedOn, amc, siteStatus, tvInstalledOn;
    private final Calendar myCalendar = Calendar.getInstance();
    public EditText rated,tvSiteId;
    private SolutionListener listener;
    private String[] phaseArray, solutionArray, siteTypeAray, statusArray;


    @Override
    public int getLayout() {
        return R.layout.update_site1;
    }

    @Override
    public void initViews(View view) {
        tvSiteId = view.findViewById(R.id.tvSiteId);
        tvSolution = view.findViewById(R.id.solution);
        tvInstalledOn = view.findViewById(R.id.installedOn);
        tvSiteType = view.findViewById(R.id.siteType);
        tvPhase = view.findViewById(R.id.phase);
        tvConnectionID = view.findViewById(R.id.connectionId);
        tvSiteSize = view.findViewById(R.id.siteSize);
        installedOn = view.findViewById(R.id.installedOn);
        siteStatus = view.findViewById(R.id.siteStatus);
        amc = view.findViewById(R.id.amc);
        rated = view.findViewById(R.id.rated);

        disableSuggestion(tvSiteType);
        disableSuggestion(tvSolution);
        disableSuggestion(tvPhase);
        disableSuggestion(tvConnectionID);
        disableSuggestion(tvSiteSize);
        disableSuggestion(tvSiteId);
        disableSuggestion(rated);
        disableSuggestion(amc);
        disableSuggestion(siteStatus);
        disableSuggestion(installedOn);

    }

    @Override
    public void setUp() {

        SiteDetails details = (SiteDetails) getArguments().getSerializable("data");

        tvSiteSize.setText(details.getSiteSize());
        tvSiteId.setText(details.getSiteId());
        tvConnectionID.setText(details.getConnectionId());
        tvPhase.setText(details.getPhase());
        tvSolution.setText(details.getSolutionType());
        tvSiteType.setText(details.getSiteType());
        installedOn.setText(Utils.convertDate(details.getInstalledOn()));
        amc.setText(Utils.convertDate(details.getAmcEnd()));
        siteStatus.setText(details.status ? "Active" : "Inactive");
        rated.setText(details.getSiteRated() ? "Rated" : "Pending");

        if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SG_ADMIN)|securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(BO_MANAGER)){
            tvSolution.setOnClickListener(this);
            siteStatus.setOnClickListener(this);
            rated.setVisibility(View.VISIBLE);



        }
        else {
            rated.setVisibility(View.GONE);
            tvSiteSize.setFocusable(false);
            tvSiteSize.setFocusableInTouchMode(false);
        }
        tvPhase.setOnClickListener(this);
        tvInstalledOn.setOnClickListener(this);
        amc.setOnClickListener(this);
        tvSiteType.setOnClickListener(this);


        SiteLov siteLov = Utils.getObj(securePrefManager.getSharedValue(SITELOVS), SiteLov.class);


        phaseArray = getContext().getResources().getStringArray(R.array.phase_type);
        statusArray = getContext().getResources().getStringArray(R.array.status_type);
        this.siteTypeAray = new String[siteLov.getSiteDetails().getSiteType().getLovValues().size()];
        for (int i = 0; i < siteLov.getSiteDetails().getSiteType().getLovValues().size(); i++) {
            try {
                this.siteTypeAray = siteLov.getSiteDetails().getSiteType().getLovValues().toArray(siteTypeAray);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        solutionArray = new String[siteLov.getSiteDetails().getSolution().getLovValues().size()];
        for (int i = 0; i < siteLov.getSiteDetails().getSolution().getLovValues().size(); i++) {
            try {
                this.solutionArray = siteLov.getSiteDetails().getSolution().getLovValues().toArray(solutionArray);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SolutionListener) {
            listener = (SolutionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement TextClicked");
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDetach() {

        super.onDetach();
        listener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.solution:
                showAlertWithListForSOl(solutionArray, R.string.select_solution, tvSolution);
                break;

            case R.id.phase:
                showAlertWithList(phaseArray, R.string.select_phase, tvPhase);
                break;

            case R.id.siteType:
                showAlertWithList(siteTypeAray, R.string.select_site, tvSiteType);
                break;
            case R.id.installedOn:
                openDatePickerForInstalledOn(tvInstalledOn);
                break;
            case R.id.siteStatus:
                showAlertWithList(statusArray, R.string.select_site_status, siteStatus);
                break;
            case R.id.amc:
                if (installedOn.getText().toString().trim().length()==0)
                    showToast("Please select Installed On Date first");

                else
                    openDatePickerForAmc(amc);

                break;
        }
    }

    private void openDatePickerForInstalledOn(TextView textView) {

        int year = myCalendar.get(Calendar.YEAR);
        int month = myCalendar.get(Calendar.MONTH);
        int day = myCalendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), R.style.DialogTheme, (datePicker, year1, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year1);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            String myFormat = "dd/MM/yyyy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
            textView.setText(sdf.format(myCalendar.getTime()));


        }, year, month,
                day);
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());


        datePickerDialog.show();

    }

    private void openDatePickerForAmc(TextView textView) {

        int year = myCalendar.get(Calendar.YEAR);
        int month = myCalendar.get(Calendar.MONTH);
        int day = myCalendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), R.style.DialogTheme, (datePicker, year1, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year1);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            String myFormat = "dd/MM/yyyy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
            textView.setText(sdf.format(myCalendar.getTime()));


        }, year, month,
                day);
            datePickerDialog.getDatePicker().setMinDate(convertTime(tvInstalledOn.getText().toString()));
        datePickerDialog.show();

    }


    public long convertTime(String time) {
        long time1 = (long) 0.0;
        SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");


        try {
            Date date = df.parse(time);
            time1 = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return time1;

    }

    public void showAlertWithListForSOl(final String[] dataArray, final @StringRes int title, final TextView textView) {
        hideKeyboard(getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(title));
        builder.setItems(dataArray, (dialog, item) -> {
            textView.setText(dataArray[item]);
            if (dataArray[item].equalsIgnoreCase("Hybrid")) {
                listener.selectHybrid(true);
            } else {
                listener.selectHybrid(false);

            }
        });
        AlertDialog alerts = builder.create();
        alerts.show();
    }

    public interface SolutionListener {
        void selectHybrid(boolean isHybrid);
    }
}
