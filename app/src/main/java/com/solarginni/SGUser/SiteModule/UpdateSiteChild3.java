package com.solarginni.SGUser.SiteModule;

import android.content.Context;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.solarginni.Base.BaseFragment;
import com.solarginni.CustomizeView.CompanyListDialog;
import com.solarginni.DBModel.APIBody.InverterModelList;
import com.solarginni.DBModel.CompanyMakeModel;
import com.solarginni.DBModel.SiteDetailsModel.Inverter;
import com.solarginni.DBModel.SiteDetailsModel.SiteDetails;
import com.solarginni.R;
import com.solarginni.SiteLovsModule.SiteDetailModel.SiteLov;
import com.solarginni.Utility.Utils;

import java.util.ArrayList;
import java.util.List;

import static com.solarginni.Utility.AppConstants.SITELOVS;

public class UpdateSiteChild3 extends BaseFragment implements  View.OnClickListener{

    public static EditText etPanelSize, batterySize, batterySerial,inverterCount ;
    private String[] panelTypeArray, invertelModelArray;
    public static EditText tvPanelMake, tvPanelType;
    public  EditText tvBatteryMake;
    public static LinearLayout linearLayout;
    private View dynamicEntryView;
    public static String panelMakeId, inverterMakeId, batteryId ="";
    private Boolean isExtraLayout = true;
    public static LinearLayout batteryLayout;
    private SiteLov siteLov;
    private List<CompanyMakeModel> panelMakeArray = new ArrayList<>();
    private List<CompanyMakeModel> inverterMakeArray = new ArrayList<>();
    private List<CompanyMakeModel> batteryArray = new ArrayList<>();
    private String[] inverterArray = {"1", "2", "3"};
    public static boolean isListPrepared = false;
    public static List<InverterModelList> lists;

    @Override
    public int getLayout() {
        return R.layout.update_site3;
    }

    @Override
    public void initViews(View view) {
        etPanelSize = view.findViewById(R.id.panelSize);
        tvPanelType = view.findViewById(R.id.panelType);
        tvPanelMake = view.findViewById(R.id.panelMake);
        linearLayout = view.findViewById(R.id.linearLayout);
        batteryLayout = view.findViewById(R.id.batteryLayout);
        batterySize = view.findViewById(R.id.batterySize);
        batterySerial = view.findViewById(R.id.batterySerial);
        tvBatteryMake = view.findViewById(R.id.batteryMake);
        inverterCount = view.findViewById(R.id.inverterCount);

        disableSuggestion(etPanelSize);
        disableSuggestion(tvBatteryMake);
        disableSuggestion(tvPanelMake);
        disableSuggestion(batterySerial);
        disableSuggestion(batterySize);
        disableSuggestion(tvPanelType);

    }

    @Override
    public void setUp() {

        SiteDetails details = (SiteDetails) getArguments().getSerializable("data");

        createDynamicLayout(details.getInverter());
        tvPanelMake.setText(details.getPanelMake());
        etPanelSize.setText(details.getPanelSize());
        tvPanelType.setText(details.getPanelType());
        inverterCount.setText(""+details.getInverter().size());
        panelMakeId= details.panelMakeId;
        if (details.getBattery()!=null)
        batteryId=details.getBattery().batteryCompanyId;

        if (details.getSolutionType().equalsIgnoreCase("Hybrid")) {
            batteryLayout.setVisibility(View.VISIBLE);
            batterySerial.setText(details.getBattery().getBatterySerial());
            tvBatteryMake.setText(details.getBattery().getBatteryMake());
            batterySize.setText(details.getBattery().getBatterySize());
        } else {
            batteryLayout.setVisibility(View.GONE);

        }


        siteLov = Utils.getObj(securePrefManager.getSharedValue(SITELOVS), SiteLov.class);


        panelMakeArray.addAll(siteLov.getSiteDetails().getPanelMake());
        inverterMakeArray.addAll(siteLov.getSiteDetails().getInverterMake());
        batteryArray.addAll(siteLov.getSiteDetails().getBatteryMake());

        panelTypeArray = new String[siteLov.getSiteDetails().getPanelType().getLovValues().size()];
        for (int i = 0; i < siteLov.getSiteDetails().getPanelType().getLovValues().size(); i++) {
            panelTypeArray = siteLov.getSiteDetails().getPanelType().getLovValues().toArray(panelTypeArray);
        }


        tvPanelMake.setOnClickListener(this);
        tvPanelType.setOnClickListener(this);
        //tvInverterMake.setOnClickListener(this);
        tvBatteryMake.setOnClickListener(this);
        inverterCount.setOnClickListener(this);
    }

    private void createDynamicLayout(List<Inverter> list) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        linearLayout.removeAllViews();

        for (int i = 0; i < list.size(); i++) {
            dynamicEntryView = inflater.inflate(R.layout.child_layout, null);


            TextView inverterMake = dynamicEntryView.findViewById(R.id.inverterMake);
            TextView inertMakeModel = dynamicEntryView.findViewById(R.id.inverterModel);
            TextView inertMakeSize = dynamicEntryView.findViewById(R.id.inverterSize);
            TextView textView =  dynamicEntryView.findViewById(R.id.inertMakeId);

            disableSuggestion(inverterMake);
            disableSuggestion(inertMakeModel);
            disableSuggestion(inertMakeSize);

            inertMakeModel.setFocusableInTouchMode(true);
            inertMakeModel.setFocusable(true);

            inertMakeSize.setFocusableInTouchMode(true);
            inertMakeSize.setFocusable(true);

            inverterMake.setFocusableInTouchMode(false);
            inverterMake.setFocusable(false);

            inertMakeModel.setText(list.get(i).getInverterSerial());
            inverterMake.setText(list.get(i).getInverterMake());
            inertMakeSize.setText(list.get(i).getInverterSize());

            textView.setText(list.get(i).companyId);

            inverterMake.setOnClickListener(clickHandler(inverterMake, textView));

            linearLayout.addView(dynamicEntryView);
        }


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.panelMake:
                CompanyListDialog.show(getActivity(), panelMakeArray, item -> {
                    tvPanelMake.setText(item.getCompanyName());
                    panelMakeId = item.getCompanyId();

                });
                break;
            case R.id.panelType:
                showAlertWithList(panelTypeArray, R.string.panel_type, tvPanelType);
                break;
            case R.id.inverterMake:
                CompanyListDialog.show(getActivity(), inverterMakeArray, item -> {
                    inverterMakeId = item.getCompanyId();
                });

                break;
            case R.id.batteryMake:
                CompanyListDialog.show(getActivity(), batteryArray, item -> {
                    tvBatteryMake.setText(item.getCompanyName());
                    batteryId = item.getCompanyId();

                });
                break;
            case R.id.inverterCount:
                showAlertWithListForCount(inverterArray, R.string.select_number, inverterCount);
                break;

        }

    }

    public void showAlertWithListForCount(final String[] dataArray, final @StringRes int title, final TextView textView) {
        hideKeyboard(getActivity());
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(title));
        builder.setItems(dataArray, (dialog, item) -> {
            textView.setText(dataArray[item]);
            createInverterLayout(Integer.parseInt(dataArray[item]));
        });
        AlertDialog alerts = builder.create();
        alerts.show();
    }



    private void createInverterLayout(int size) {

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        linearLayout.removeAllViews();

        for (int i = 0; i < size; i++) {
            dynamicEntryView = inflater.inflate(R.layout.child_layout, null);

            linearLayout.addView(dynamicEntryView);

            View view = linearLayout.getChildAt(i);

            TextView inverterMake = view.findViewById(R.id.inverterMake);
            TextView inertMakeId = view.findViewById(R.id.inertMakeId);
            disableSuggestion(((EditText) view.findViewById(R.id.inverterModel)));
            disableSuggestion(((EditText) view.findViewById(R.id.inverterSize)));



            inverterMake.setOnClickListener(clickHandler(inverterMake, inertMakeId));
        }


    }

    private View.OnClickListener clickHandler(TextView textView, TextView inverterId) {

        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CompanyListDialog.show(getActivity(), inverterMakeArray, item -> {
                    textView.setText(item.getCompanyName());
                    inverterId.setText(item.getCompanyId());
                });


            }
        };
    }

    public static void prepareInverterLis() {
        isListPrepared = false;
        lists = new ArrayList<>();


        for (int i = 0; i < Integer.parseInt(inverterCount.getText().toString()); i++) {
            View view = linearLayout.getChildAt(i);
            InverterModelList model = new InverterModelList();
            EditText textView = ((EditText) view.findViewById(R.id.inertMakeId));
            if (textView.getText().toString().equalsIgnoreCase("")) {

                isListPrepared = false;
                break;
            } else {
                isListPrepared = true;
                model.setInverterMake(textView.getText().toString());
                model.setInverterModel(((EditText) view.findViewById(R.id.inverterModel)).getText().toString());
                model.setInverterSize(((EditText) view.findViewById(R.id.inverterSize)).getText().toString());
                lists.add(model);

            }


        }


    }


}
