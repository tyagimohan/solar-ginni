package com.solarginni.SGUser.SiteModule;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;

import retrofit2.Retrofit;

public class AllSiteInteractor implements AllSiteContract.Interactor {

    private Retrofit retrofit;
    private ApiInterface apiInterface;


    public AllSiteInteractor(Retrofit retrofit) {
        this.retrofit = retrofit;
        apiInterface= retrofit.create(ApiInterface.class);
    }

    @Override
    public void fetchSite(String token, String key, int page, String siteType, String state, AllSiteContract.OnInteraction listener) {
             apiInterface.getSiteLilst(token,key,page,siteType,state).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
                 @Override
                 public void onSuccess(CommomResponse response) {
                     if (key.equalsIgnoreCase("#"))
                         listener.showSites(response);

                     else
                         listener.onSearchSite(response);
                 }
                 @Override
                 public void onError(ApiError error) {
                     listener.failure(error.getMessage());

                 }

                 @Override
                 public void onFailure(Throwable throwable) {
                     listener.failure(throwable.getMessage());

                 }
             });
    }
}
