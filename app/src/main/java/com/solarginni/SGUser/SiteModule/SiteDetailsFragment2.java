package com.solarginni.SGUser.SiteModule;

import android.view.View;
import android.widget.EditText;

import com.solarginni.Base.BaseFragment;
import com.solarginni.DBModel.SiteDetailsModel.SiteDetails;
import com.solarginni.R;

public class SiteDetailsFragment2 extends BaseFragment {
    public EditText tvInstalledBy, city, state, country, pincode, customerMob,customerId,implementerId;
    public EditText etLine1, addLine2;

    @Override
    public int getLayout() {
        return R.layout.site_details2;
    }

    @Override
    public void initViews(View view) {
        tvInstalledBy = view.findViewById(R.id.installedBy);
        customerMob = view.findViewById(R.id.customerMob);
        state = view.findViewById(R.id.state);
        city = view.findViewById(R.id.cityName);
        country = view.findViewById(R.id.country);
        pincode = view.findViewById(R.id.pincode);
        customerId = view.findViewById(R.id.customerId);
        etLine1 = view.findViewById(R.id.etLine1);
        addLine2 = view.findViewById(R.id.line2);
        implementerId = view.findViewById(R.id.implementerId);

        disableSuggestion(tvInstalledBy);
        disableSuggestion(city);
        disableSuggestion(etLine1);
        disableSuggestion(addLine2);
        disableSuggestion(country);
        disableSuggestion(state);
        disableSuggestion(pincode);
        disableSuggestion(customerId);
        disableSuggestion(customerMob);
        disableSuggestion(tvInstalledBy);
        disableSuggestion(implementerId);

    }

    @Override
    public void setUp() {
        SiteDetails details = (SiteDetails) getArguments().getSerializable("data");


        etLine1.setText(details.getAddress().getAddressLine1());
        addLine2.setText(details.getAddress().getAddressLine2());
        city.setText(details.getAddress().getCity());
        state.setText(details.getAddress().getState());
        country.setText(details.getAddress().getCountry());
        pincode.setText(details.getAddress().getPinCode());
        customerId.setText(details.getCustomerUserId());
        customerMob.setText(details.getPhone());
        tvInstalledBy.setText(details.getInstalledBy());
        implementerId.setText(details.getCompanyId());


    }
}
