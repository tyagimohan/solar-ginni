package com.solarginni.SGUser.SiteModule;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.solarginni.Base.BaseFragment;
import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.CommonModule.MainActivity;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.SiteDetailsModel.SiteDetails;
import com.solarginni.DBModel.SiteDetailsModel.SiteDetailsModel;
import com.solarginni.Implementer.ImplementerDetails.ImplemeterDetailsActivity;
import com.solarginni.R;
import com.solarginni.Utility.Utils;
import com.solarginni.user.SiteRegistrationActivity;
import com.solarginni.user.UserSiteModule.SiteLandingAcity;
import com.solarginni.user.customerDetail.CustomerDetailActivity;

import org.angmarch.views.NiceSpinner;

import java.io.Serializable;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static com.solarginni.Utility.AppConstants.TOKEN;

public class AllSiteListFragment extends BaseFragment implements AllSiteContract.View, View.OnClickListener {

    private static final int REQUEST_FOR_SITE = 10;
    private static final int REQUEST_SITE = 11;
    private AllSitePresenter presenter;
    private RecyclerView recyclerView;
    private NiceSpinner stateSpinnier, siteTypeSpinner;
    private LinearLayoutManager layoutManager;
    private boolean isLoading = false;
    private LinearLayout progressPagination;
    private final RecyclerView.OnScrollListener recyclerScroller = new RecyclerView.OnScrollListener() {
        @SuppressWarnings("EmptyMethod")
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

            if (dy > 0)
                if (!isLoading) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition > 0) {
                        progressPagination.setVisibility(View.VISIBLE);
                        loadMoreItems(visibleItemCount + layoutManager.findFirstVisibleItemPosition());
                    }
                } else {

                }
        }
    };
    private AllSiteListAdapter adapter;
    private EditText etMobileNumber;
    private Button searchButton;
    private boolean isRefresh = false;
    private boolean isFilter = false;
    private AllSiteListAdapter.onItemClick<SiteDetails> longListenr = (data, position, view) -> {
        switch (view.getId()) {
            case R.id.customerMob:
                Bundle bundle = new Bundle();
                if (data.getCustomerUserId() != null) {

                    bundle.putBoolean("isFormasking", false);
                    bundle.putString("id", data.getCustomerUserId());
                    goToNextScreen(CustomerDetailActivity.class, bundle);
                } else {

                }
                break;
            case R.id.implementerName:
                bundle = new Bundle();
                bundle.putBoolean("isFormasking", false);
                bundle.putString("id", data.getCompanyId());
                goToNextScreen(ImplemeterDetailsActivity.class, bundle);
                break;
            case R.id.siteId:
                bundle = new Bundle();
                bundle.putSerializable("data", (Serializable) data);
                bundle.putBoolean("isForSG", true);
                Intent intent = new Intent(getActivity(), SiteLandingAcity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, REQUEST_SITE);
                break;
            case R.id.editSite:
                bundle = new Bundle();
                bundle.putSerializable("data", data);
                goToNextScreen(UpdateSiteActivity.class, bundle);
                break;
            case R.id.siteImage:

                if (data.getImageUrl() != null) {
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
                    LayoutInflater inflater = this.getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.advertise_layout, null);

                    dialogView.findViewById(R.id.advertisementImage).setVisibility(View.GONE);
                    ImageView siteImage = dialogView.findViewById(R.id.imageView);
                    siteImage.setVisibility(View.VISIBLE);
                    Glide.with(getContext()).load(data.getImageUrl().get(0)).into(siteImage);
                    dialogBuilder.setView(dialogView);
                    dialogBuilder.setCancelable(true);
                    AlertDialog alertDialog = dialogBuilder.create();
                    alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                    alertDialog.show();
                }

                break;
        }
    };

    @Override
    public int getLayout() {
        return R.layout.all_site_list;
    }


    @Override
    public void initViews(View view) {
        recyclerView = view.findViewById(R.id.recyclerView);
        stateSpinnier = view.findViewById(R.id.state_spinner);
        siteTypeSpinner = view.findViewById(R.id.type_spinner);
        searchButton = view.findViewById(R.id.serach_btn);
        etMobileNumber = view.findViewById(R.id.mobileNumber);
        progressPagination = (LinearLayout) view.findViewById(R.id.progressPagination);
        setupUI(view.findViewById(R.id.parent));
        setupUI(view.findViewById(R.id.recyclerView));

        view.findViewById(R.id.addSite).setOnClickListener(this);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new BaseRecyclerAdapter.DividerItemDecoration(getContext(), RecyclerView.VERTICAL));
        adapter = new AllSiteListAdapter(getContext());
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(recyclerScroller);
        adapter.setOnLongClickListener(longListenr);
        view.findViewById(R.id.refresh).setOnClickListener(this);
        presenter = new AllSitePresenter(this);

        List<String> dataset = new LinkedList<>(Arrays.asList("All", "Residential", "Institutional", "Industrial", "Commercial", "Religious", "Others"));
        siteTypeSpinner.attachDataSource(dataset);
        List<String> statedataset = new LinkedList<>(Arrays.asList(getResources().getStringArray(R.array.stateAll)));
        stateSpinnier.attachDataSource(statedataset);

    }

    @Override
    public void setUp() {

        searchButton.setOnClickListener(this);

        stateSpinnier.setOnSpinnerItemSelectedListener((parent, view, position, id) -> {
            if (Utils.hasNetwork(getContext())) {
                isFilter = true;
                showProgress("Please wait..");
                presenter.fetchSite(securePrefManager.getSharedValue(TOKEN), "#", 0, siteTypeSpinner.getText().toString(), "" + parent.getItemAtPosition(position));
            } else {
                showToast("Please check Internet");

            }
        });

        siteTypeSpinner.setOnSpinnerItemSelectedListener((parent, view, position, id) -> {
            if (Utils.hasNetwork(getContext())) {
                isFilter = true;
                showProgress("Please wait..");
                presenter.fetchSite(securePrefManager.getSharedValue(TOKEN), "#", 0, "" + parent.getItemAtPosition(position), stateSpinnier.getText().toString());
            } else {
                showToast("Please check Internet");

            }
        });


        if (Utils.hasNetwork(getContext())) {
            showProgress("Please wait..");
            presenter.fetchSite(securePrefManager.getSharedValue(TOKEN), "#", 0, "All", "All");
        } else {
            showToast("Please check Internet");

        }
    }

    @Override
    public void failure(String msg) {
        hideProgress();
        progressPagination.setVisibility(View.GONE);

        if (msg.equalsIgnoreCase("invalid token")) {
            securePrefManager.clearAppsAllPrefs();
            startActivity(new Intent(getActivity(), MainActivity.class));
            getActivity().finish();
        } else
            showToast(msg);
    }

    @Override
    public void showSites(CommomResponse response) {
        hideProgress();
        progressPagination.setVisibility(View.GONE);

        SiteDetailsModel model = response.toResponseModel(SiteDetailsModel.class);
        etMobileNumber.setText("");


        if (isFilter) {
            isFilter = false;
            adapter.notifyItemRangeRemoved(0, adapter.getItemCount());
            adapter.clear();
            adapter.notifyDataSetChanged();
        }


        /*if */

        if (model.getSiteDetails().size() == 0 && isFilter) {
            showToast("No Site Found");
            adapter.notifyItemRangeRemoved(0, adapter.getItemCount());
            adapter.clear();
            adapter.notifyDataSetChanged();
            if (isRefresh) {
                isRefresh = false;
                stateSpinnier.setSelectedIndex(0);
                siteTypeSpinner.setSelectedIndex(0);

            }

        } else {
            isLoading = false;
            if (model.getSiteDetails().size() > 0 && model.getSiteDetails().size() <= 9) {
                isLoading = true;
            }
            if (isRefresh) {
                isRefresh = false;
                adapter.notifyItemRangeRemoved(0, adapter.getItemCount());
                adapter.clear();
                adapter.notifyDataSetChanged();
                stateSpinnier.setSelectedIndex(0);
                siteTypeSpinner.setSelectedIndex(0);


            }
            int initialCount = adapter.getItemCount();
            adapter.addAllForPagination(model.getSiteDetails());
            adapter.notifyItemRangeChanged(initialCount, model.getSiteDetails().size());
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onSearchSite(CommomResponse response) {
        hideProgress();
        SiteDetailsModel model = response.toResponseModel(SiteDetailsModel.class);
        isLoading = true;
        adapter.notifyItemRangeRemoved(0, adapter.getItemCount());
        if (model.getSiteDetails().size() == 0)
            showToast("No Site Found");
        stateSpinnier.setSelectedIndex(0);
        siteTypeSpinner.setSelectedIndex(0);
        adapter.addAll(model.getSiteDetails());
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.serach_btn:
                hideKeyboard(getActivity());
                if (Utils.hasNetwork(getContext())) {
                    if (etMobileNumber.getText().toString().trim().length() == 0) {
                        showToast("Please enter mobile");
                        return;
                    } else {
                        showProgress("Please wait...");
                        etMobileNumber.clearFocus();
                        hideKeyboard(getActivity());
                        presenter.fetchSite(securePrefManager.getSharedValue(TOKEN), etMobileNumber.getText().toString(), 0, "All", "All");
                        stateSpinnier.setSelectedIndex(0);
                        siteTypeSpinner.setSelectedIndex(0);
                    }

                } else {
                    showToast("Please check Internet");
                }
                break;

            case R.id.refresh:
                if (Utils.hasNetwork(getContext())) {
                    showProgress("Please wait...");
                    isRefresh = true;
                    presenter.fetchSite(securePrefManager.getSharedValue(TOKEN), "#", 0, "All", "All");

                } else {
                    showToast("Please check internet ");
                }
                break;
            case R.id.addSite:
            case R.id.regiterSite:
                Bundle bundle = new Bundle();
                bundle.putBoolean("isFromLanding", true);
                Intent intent = new Intent(getActivity(), SiteRegistrationActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, REQUEST_FOR_SITE);


                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_FOR_SITE:

                if (resultCode == Activity.RESULT_OK) {
                    getView().findViewById(R.id.refresh).performClick();
                }
                break;
            case REQUEST_SITE:
                getView().findViewById(R.id.refresh).performClick();
                break;
        }
    }

    private void loadMoreItems(int pos) {
        isLoading = true;
        int page = (pos / 10);
        presenter.fetchSite(securePrefManager.getSharedValue(TOKEN), "#", page, siteTypeSpinner.getText().toString(), stateSpinnier.getText().toString());
    }

    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    view.requestFocus();

                    hideKeyboard(getActivity());
                    return false;
                }
            });
        }
    }

}
