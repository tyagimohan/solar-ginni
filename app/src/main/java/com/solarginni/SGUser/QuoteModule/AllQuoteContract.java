package com.solarginni.SGUser.QuoteModule;

import com.solarginni.DBModel.CommomResponse;

public interface AllQuoteContract {

    interface View {
        void showQuoteList(CommomResponse response);

        void onFailure(String msg);

        void onGetComparision(CommomResponse response);

        void onSearchQuote(CommomResponse response);

        void onGetRecall(CommomResponse response);

    }

    interface Presenter {

        void fetchQuote(String token, String key, int page, String type, String status);

        void recallQuote(String token, String optId);

        void getComparision(String token, String optId);


    }

    interface Interactor {
        void fetchQuote(String token, String key, int page, String type, String status, Oninteraction listener);

        void recallQuote(String token, String optId, Oninteraction listenr);

        void getComparision(String token, String optId, Oninteraction listenr);


    }

    interface Oninteraction {
        void showQuoteList(CommomResponse response);

        void onFailure(String msg);

        void onGetRecall(CommomResponse response);

        void onGetComparision(CommomResponse response);


        void onSearchQuote(CommomResponse response);
    }

}
