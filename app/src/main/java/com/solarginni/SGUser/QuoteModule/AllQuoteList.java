package com.solarginni.SGUser.QuoteModule;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.solarginni.Base.BaseFragment;
import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.CommonModule.MainActivity;
import com.solarginni.CustomizeView.MovableFloatingActionButton;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.ComparisonViewModel;
import com.solarginni.DBModel.QuoteListModel;
import com.solarginni.Implementer.ImplementerDetails.ImplemeterDetailsActivity;
import com.solarginni.ProspectUser.ComparisonActivity;
import com.solarginni.ProspectUser.GetQuotationActivity;
import com.solarginni.ProspectUser.PendingQuoteListAdapter;
import com.solarginni.ProspectUser.QuoteDetailActivity;
import com.solarginni.R;
import com.solarginni.SGUser.RegisterNewUser;
import com.solarginni.Utility.AppDialogs;
import com.solarginni.Utility.Utils;
import com.solarginni.user.addFeasibility.AddFeasibililty;
import com.solarginni.user.addFeasibility.ShowFeasibility;
import com.solarginni.user.customerDetail.CustomerDetailActivity;

import org.angmarch.views.NiceSpinner;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static com.solarginni.Utility.AppConstants.OPTYID;
import static com.solarginni.Utility.AppConstants.TOKEN;

public class AllQuoteList extends BaseFragment implements View.OnClickListener, AllQuoteContract.View {

    private static final int REQUEST_FOR_FEASIBILITY = 5;
    private RecyclerView recyclerView;
    private NiceSpinner stateSpinner, typeSpinner;
    private LinearLayoutManager layoutManager;
    private boolean isLoading = false;
    protected AppDialogs mAppDialogs;
    private LinearLayout progressPagination;
    private AllQuotePresenter presenter;
    private PendingQuoteListAdapter adapter;
    private EditText etMobileNumber;
    private Button searchButton;
    private boolean isRefresh = false;
    private boolean isFilter = false;
    private MovableFloatingActionButton raiseQuote;



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_FOR_FEASIBILITY:
                if (resultCode == Activity.RESULT_OK) {
                    getView().findViewById(R.id.refresh).performClick();
                }
                break;
        }
    }

    @Override
    public int getLayout() {
        return R.layout.all_quote_list;
    }

    @Override
    public void initViews(View view) {

        recyclerView = view.findViewById(R.id.recyclerView);
        stateSpinner = view.findViewById(R.id.state_spinner);
        typeSpinner = view.findViewById(R.id.type_spinner);
        searchButton = view.findViewById(R.id.serach_btn);
        etMobileNumber = view.findViewById(R.id.mobileNumber);
        raiseQuote = view.findViewById(R.id.raiseQuote);
        progressPagination = (LinearLayout) view.findViewById(R.id.progressPagination);
        setupUI(view.findViewById(R.id.parent));
        setupUI(view.findViewById(R.id.recyclerView));
        adapter = new PendingQuoteListAdapter(getContext(), true);
        recyclerView.setAdapter(adapter);
        adapter.setOnLongClickListener(longListenr);
        adapter.setIndexClikListener(onIndexedListener);
        adapter.setOnClickListener(listner);
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new BaseRecyclerAdapter.DividerItemDecoration(getContext(), RecyclerView.HORIZONTAL));
        presenter = new AllQuotePresenter(this);
        mAppDialogs = new AppDialogs(getActivity());
        recyclerView.addOnScrollListener(recyclerScroller);

        view.findViewById(R.id.refresh).setOnClickListener(this);
        raiseQuote.setOnClickListener(this);

        List<String> dataset = new LinkedList<>(Arrays.asList("All", "Residential", "Institutional", "Industrial", "Commercial", "Religious", "Others"));
        typeSpinner.attachDataSource(dataset);
        List<String> statedataset = new LinkedList<>(Arrays.asList(getResources().getStringArray(R.array.stateAll)));
        stateSpinner.attachDataSource(statedataset);

    }

    @Override
    public void setUp() {

        searchButton.setOnClickListener(this);

        stateSpinner.setOnSpinnerItemSelectedListener((parent, view, position, id) -> {
            if (Utils.hasNetwork(getContext())) {
                isFilter = true;
                showProgress("Please wait..");
                presenter.fetchQuote(securePrefManager.getSharedValue(TOKEN), "#", 0, typeSpinner.getText().toString(), "" + parent.getItemAtPosition(position));
            } else {
                showToast("Please check Internet");

            }
        });

        typeSpinner.setOnSpinnerItemSelectedListener((parent, view, position, id) -> {
            if (Utils.hasNetwork(getContext())) {
                isFilter = true;
                showProgress("Please wait..");
                presenter.fetchQuote(securePrefManager.getSharedValue(TOKEN), "#", 0, "" + parent.getItemAtPosition(position), stateSpinner.getText().toString());
            } else {
                showToast("Please check Internet");

            }
        });


        if (Utils.hasNetwork(getContext())) {
            showProgress("Please wait..");
            presenter.fetchQuote(securePrefManager.getSharedValue(TOKEN), "#", 0, "All", "All");
        } else {
            showToast("Please check Internet");

        }

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.serach_btn:
                hideKeyboard(getActivity());
                if (Utils.hasNetwork(getContext())) {
                    if (etMobileNumber.getText().toString().trim().length() == 0) {
                        showToast("Please enter mobile");
                        return;
                    } else {
                        showProgress("Please wait...");
                        etMobileNumber.clearFocus();
                        hideKeyboard(getActivity());
                        presenter.fetchQuote(securePrefManager.getSharedValue(TOKEN), etMobileNumber.getText().toString(), 0, "All", "All");
                        typeSpinner.setSelectedIndex(0);
                        stateSpinner.setSelectedIndex(0);
                    }

                } else {
                    showToast("Please check Internet");
                }
                break;

            case R.id.refresh:
                if (Utils.hasNetwork(getContext())) {
                    showProgress("Please wait...");
                    isRefresh = true;
                    presenter.fetchQuote(securePrefManager.getSharedValue(TOKEN), "#", 0, "All", "All");

                } else {
                    showToast("Please check internet ");
                }
                break;
            case R.id.raiseQuote:
                Bundle bundle = new Bundle();
                goToNextScreen(RegisterNewUser.class, bundle);


                break;
        }

    }

    private void loadMoreItems(int pos) {
        isLoading = true;
        int page = (pos / 10);
        presenter.fetchQuote(securePrefManager.getSharedValue(TOKEN), "#", page, typeSpinner.getText().toString(), stateSpinner.getText().toString());
    }


    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    view.requestFocus();
                    hideKeyboard(getActivity());
                    return false;
                }
            });
        }
    }


    @Override
    public void showQuoteList(CommomResponse response) {

        hideProgress();
        progressPagination.setVisibility(View.GONE);
        QuoteListModel model = response.toResponseModel(QuoteListModel.class);
        etMobileNumber.setText("");


        if (isFilter) {
            isFilter = false;
            adapter.notifyItemRangeRemoved(0, adapter.getItemCount());
            adapter.clear();
            adapter.notifyDataSetChanged();
        }

        if (model.getDetails().size() == 0 && isFilter) {
            showToast("No Quote Found");
            adapter.notifyItemRangeRemoved(0, adapter.getItemCount());
            adapter.clear();
            adapter.notifyDataSetChanged();
            if (isRefresh) {
                isRefresh = false;
                typeSpinner.setSelectedIndex(0);
                stateSpinner.setSelectedIndex(0);

            }

        } else {
            isLoading = false;
            if (model.getDetails().size() > 0 && model.getDetails().size() <= 9) {
                isLoading = true;
            }
            if (isRefresh) {
                isRefresh = false;
                adapter.notifyItemRangeRemoved(0, adapter.getItemCount());
                adapter.clear();
                adapter.notifyDataSetChanged();
                typeSpinner.setSelectedIndex(0);
                stateSpinner.setSelectedIndex(0);


            }
            int initialCount = adapter.getItemCount();
            adapter.addAllForPagination(model.getDetails());

            int count = 0;

            for (int i = 0; i < adapter.getList().size(); i++) {
                for (int j = 0; j < adapter.getList().get(i).getDemandQuotes().size(); j++) {
                    if (adapter.getList().get(i).getDemandQuotes().get(j).getStatus().equalsIgnoreCase("Responded")) {
                        count++;
                        Log.e("Index", "" + i);
                    }
                }
            }

            Log.e("Count", "" + count);

            adapter.notifyItemRangeChanged(initialCount, model.getDetails().size());
            adapter.notifyDataSetChanged();
        }

    }

    @Override
    public void onFailure(String msg) {
        hideProgress();
        progressPagination.setVisibility(View.GONE);

        if (msg.equalsIgnoreCase("invalid token")) {
            securePrefManager.clearAppsAllPrefs();
            startActivity(new Intent(getActivity(), MainActivity.class));
            getActivity().finish();
        } else
            showToast(msg);
    }

    @Override
    public void onGetComparision(CommomResponse response) {
        hideProgress();
        ComparisonViewModel model = response.toResponseModel(ComparisonViewModel.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("data",model);
        goToNextScreen(ComparisonActivity.class, bundle);
    }

    @Override
    public void onSearchQuote(CommomResponse response) {

        hideProgress();
        QuoteListModel model = response.toResponseModel(QuoteListModel.class);
        isLoading = true;
        adapter.notifyItemRangeRemoved(0, adapter.getItemCount());
        if (model.getDetails().size() == 0)
            showToast("No Quote Found");
        typeSpinner.setSelectedIndex(0);
        stateSpinner.setSelectedIndex(0);
        adapter.addAll(model.getDetails());
        adapter.notifyDataSetChanged();

    }

    @Override
    public void onGetRecall(CommomResponse response) {
        getView().findViewById(R.id.refresh).performClick();


    }
    private final RecyclerView.OnScrollListener recyclerScroller = new RecyclerView.OnScrollListener() {
        @SuppressWarnings("EmptyMethod")
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

            if (dy > 0)
                if (!isLoading) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition > 0) {
                        progressPagination.setVisibility(View.VISIBLE);
                        loadMoreItems(visibleItemCount + layoutManager.findFirstVisibleItemPosition());
                    }
                } else {

                }
        }
    };
    private PendingQuoteListAdapter.onItemClick<QuoteListModel.QuoteList> longListenr = (data, position, view) -> {
        Bundle bundle = new Bundle();
        bundle.putString("optId", data.getOptyId());
        switch (view.getId()) {
            case R.id.showfeasibilityLay:
                goToNextScreen(ShowFeasibility.class, bundle);

                break;
            case R.id.feasibility:
                if (data.feasibilityCreated) {
                    goToNextScreen(ShowFeasibility.class, bundle);

                } else {
                    Intent intent = new Intent(getActivity(), AddFeasibililty.class);
                    intent.putExtras(bundle);
                    startActivityForResult(intent, REQUEST_FOR_FEASIBILITY);
                }

                break;

            case R.id.recallQuote:

                mAppDialogs.showInfoDialogBothButtons("Cancel", "Do you want to recall this Opportunity?", new AppDialogs.DialogInfo() {
                    @Override
                    public void onPositiveButton(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }

                    @Override
                    public void onNegativeButton(DialogInterface dialog, int which) {
                        if (Utils.hasNetwork(getContext())) {
                            showProgress("Please wait...");
                            presenter.recallQuote(securePrefManager.getSharedValue(TOKEN), data.getOptyId());

                        } else {
                            showToast("Please check Internet");
                        }
                    }
                },"Ok");


                break;
            case R.id.userId:
                bundle = new Bundle();
                bundle.putBoolean("isFormasking", false);
                bundle.putString("id", data.getUserId());
                goToNextScreen(CustomerDetailActivity.class, bundle);
                break;
            case R.id.compareProposals:
                if (Utils.hasNetwork(getContext())) {
                    showProgress("Please wait...");
                    presenter.getComparision(securePrefManager.getSharedValue(TOKEN), data.getOptyId());

                } else {
                    showToast("Please check Internet");
                }
                break;
        }
    };
    private PendingQuoteListAdapter.onIndexedClick<QuoteListModel.QuoteList> onIndexedListener = (data, position, view, index) -> {

        switch (view.getId()) {
            case R.id.implementerName:
                Bundle bundle = new Bundle();
                bundle.putBoolean("isFormasking", false);
                bundle.putString("id", data.getDemandQuotes().get(index).getImpleId());
                goToNextScreen(ImplemeterDetailsActivity.class, bundle);
                break;
            case R.id.quotation_id:
                bundle = new Bundle();
                bundle.putSerializable("data", data);
                bundle.putInt("index", index);
                goToNextScreen(QuoteDetailActivity.class, bundle);
                break;
            case R.id.quoteStatus:
                bundle = new Bundle();
                bundle.putBoolean("isFormasking", false);
                bundle.putString("id", data.getUserId());
                goToNextScreen(CustomerDetailActivity.class, bundle);
                break;
            case R.id.demandQuote:
                bundle = new Bundle();
                bundle.putSerializable("data", data);
                bundle.putBoolean("isFrom", true);
                Intent intent = new Intent(getActivity(), GetQuotationActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, 3);
                securePrefManager.storeSharedValue(OPTYID, data.getOptyId());
                break;
        }


    };
    PendingQuoteListAdapter.OnClick<QuoteListModel.QuoteList> listner = (data, position) -> {


        if (data.getDemandQuotes().size() < 3) {
            Bundle bundle = new Bundle();
            bundle.putSerializable("data", data);
//        if (data.getDemandQuotes().size() !=0) {
//
//            goToNextScreen(QuoteDetailsActivity.class, bundle);
//        } else {
            bundle.putBoolean("isFrom", true);
            Intent intent = new Intent(getActivity(), GetQuotationActivity.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, 3);
            securePrefManager.storeSharedValue(OPTYID, data.getOptyId());
        }


    };


}
