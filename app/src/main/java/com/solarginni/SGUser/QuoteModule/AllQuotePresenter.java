package com.solarginni.SGUser.QuoteModule;

import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.RestClient;

public class AllQuotePresenter  implements AllQuoteContract.Oninteraction,AllQuoteContract.Presenter{

    private AllQuoteContract.View view;
    private AllQuoteInteractor interactor;

    public AllQuotePresenter(AllQuoteContract.View view) {
        this.view = view;
        interactor= new AllQuoteInteractor(RestClient.getRetrofitBuilder());
    }

    @Override
    public void showQuoteList(CommomResponse response) {
          view.showQuoteList(response);
    }

    @Override
    public void onFailure(String msg) {
        view.onFailure(msg);
    }

    @Override
    public void onGetRecall(CommomResponse response) {
        view.onGetRecall(response);

    }

    @Override
    public void onGetComparision(CommomResponse response) {
        view.onGetComparision(response);

    }

    @Override
    public void onSearchQuote(CommomResponse response) {
         view.onSearchQuote(response);
    }

    @Override
    public void fetchQuote(String token, String key, int page, String type, String status) {
            interactor.fetchQuote(token,key,page,type,status,this);
    }

    @Override
    public void recallQuote(String token, String optId) {
        interactor.recallQuote(token,optId,this);
    }

    @Override
    public void getComparision(String token, String optId) {
        interactor.getComparision(token,optId,this);

    }
}
