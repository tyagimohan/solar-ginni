package com.solarginni.SGUser.QuoteModule;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;

import retrofit2.Retrofit;

public class AllQuoteInteractor implements AllQuoteContract.Interactor {

    private Retrofit retrofit;
    private ApiInterface apiInterface;


    public AllQuoteInteractor(Retrofit retrofit) {
        this.retrofit = retrofit;
        apiInterface= retrofit.create(ApiInterface.class);
    }

    @Override
    public void fetchQuote(String token, String key, int page, String type, String status, AllQuoteContract.Oninteraction listener) {
        apiInterface.getQuote(token,key,page,type, status).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                if (key.equalsIgnoreCase("#"))
                    listener.showQuoteList(response);

                else
                    listener.onSearchQuote(response);

            }

            @Override
            public void onError(ApiError error) {
                listener.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                listener.onFailure(throwable.getMessage());

            }
        });


    }

    @Override
    public void recallQuote(String token, String optId, AllQuoteContract.Oninteraction listenr) {
        apiInterface.recallQuote(token,optId).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                listenr.onGetRecall(response);

            }

            @Override
            public void onError(ApiError error) {
                listenr.onFailure(error.getMessage());
            }

            @Override
            public void onFailure(Throwable throwable) {
                listenr.onFailure(throwable.getMessage());
            }
        });
    }

    @Override
    public void getComparision(String token, String optId, AllQuoteContract.Oninteraction listenr) {
        apiInterface.getComparision(token,optId).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse commomResponse) {
                listenr.onGetComparision(commomResponse);

            }

            @Override
            public void onError(ApiError error) {
                listenr.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                listenr.onFailure(throwable.getMessage());

            }
        });
    }
}
