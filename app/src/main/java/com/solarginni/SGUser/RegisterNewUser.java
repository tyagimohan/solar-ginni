package com.solarginni.SGUser;

import android.annotation.TargetApi;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.solarginni.Base.BaseActivity;
import com.solarginni.DBModel.APIBody.UserRegisteration;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.UserIdModel;
import com.solarginni.R;
import com.solarginni.RaiseQuote.RaiseQuotationActivity;
import com.solarginni.SingleTonApi.RegisterNewUserApi;
import com.solarginni.Utility.AppConstants;
import com.solarginni.Utility.Utils;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import static com.solarginni.Utility.AppConstants.OPTYID;
import static com.solarginni.Utility.AppConstants.TOKEN;
import static com.solarginni.Utility.AppConstants.USER_ID;

public class RegisterNewUser extends BaseActivity implements View.OnClickListener {

    private static final int REQUEST_FOR_PLACE = 2;
    private Button submitBtn;
    private TextView etPincode, etLine1, etName, etEmail, etPromotion, addLine2;
    private TextView tvCity, tvState, tvCountry, tvMobileNumber;
    private Geocoder mGeocoder;
    private TextView etCity;
    private String mobileNumber;
    private String lat, lng;
    private int role;
    private String city;

    @Override
    public int getLayout() {
        return R.layout.activity_profile_register;
    }

    @Override
    public void initViews() {

        submitBtn = findViewById(R.id.submit_btn);
        addLine2 = findViewById(R.id.line2);
        tvCity = findViewById(R.id.tvCity);
        tvState = findViewById(R.id.tvState);
        tvMobileNumber = findViewById(R.id.mobileNumber);
        tvCountry = findViewById(R.id.tvCountry);
        etLine1 = findViewById(R.id.etLine1);
        etCity = findViewById(R.id.tvCity);
        etPromotion = findViewById(R.id.etPromotion);
        etName = findViewById(R.id.etName);
        etEmail = findViewById(R.id.etEmail);
        etPincode = findViewById(R.id.etPincode);
        mGeocoder = new Geocoder(this, Locale.getDefault());
        Places.initialize(getApplicationContext(), getString(R.string.google_api));
        disableSuggestion(etCity);
        disableSuggestion(etEmail);
        disableSuggestion(etLine1);
        disableSuggestion(etName);
        disableSuggestion(etPincode);
        disableSuggestion(etPromotion);
        disableSuggestion(tvCountry);
        disableSuggestion(tvCity);
        disableSuggestion(tvState);
        disableSuggestion(tvMobileNumber);
        tvMobileNumber.setFocusable(true);
        tvMobileNumber.setFocusableInTouchMode(true);

        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });

        setupUI(findViewById(R.id.parent));

    }

    @Override
    public void setUp() {

        role = Integer.parseInt(AppConstants.PROSPECT_ROLE_ID);

        disableAutofill();


        submitBtn.setOnClickListener(this);
        addLine2.setOnClickListener(this);
        etPromotion.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() == 10) {
                    hideKeyboard(RegisterNewUser.this);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submit_btn:
                try {
                    submitForm();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                break;
            case R.id.line2:
                openPlacePicker();
                break;

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_FOR_PLACE:
                if (resultCode == RESULT_OK) {
                    hideKeyboard(RegisterNewUser.this);
                    Place place = Autocomplete.getPlaceFromIntent(data);
                    addLine2.setText(place.getName());

                    LatLng latLng = place.getLatLng();
                    lat = String.valueOf(latLng.latitude);
                    lng = String.valueOf(latLng.longitude);
                    try {
                        getCityNameByCoordinates(latLng.latitude, latLng.longitude);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    //  onParamSelected();
                } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                    Status status = Autocomplete.getStatusFromIntent(data);


                } else if (resultCode == RESULT_CANCELED) {

                }
                break;


        }

    }


    private void getCityNameByCoordinates(double lat, double lon) throws IOException {

        List<Address> addresses = mGeocoder.getFromLocation(lat, lon, 1);
        if (addresses != null && addresses.size() > 0) {
            tvState.setText(addresses.get(0).getAdminArea());

//            if (addresses.get(0).getSubAdminArea() == null) {
//                etCity.setVisibility(View.VISIBLE);
//                tvCity.setVisibility(View.GONE);
//            } else {
//                etCity.setVisibility(View.GONE);
//                tvCity.setVisibility(View.VISIBLE);
//                tvCity.setText(addresses.get(0).getSubAdminArea());
//            }

            tvCity.setText(addresses.get(0).getSubAdminArea());

            tvCountry.setText(addresses.get(0).getCountryName());
            etPincode.setText(addresses.get(0).getPostalCode());


        }

    }


    private void submitForm() throws IOException {
        if (!validateName()) {
            return;
        }

        if (!validateAddress1()) {
            return;
        }
        if (!validateAddress2()) {
            return;
        }
        if (!validateCity()) {
            return;
        }
        if (!pincode()) {
            return;
        }
        if (!phoneNumber()) {
            return;
        }
        if (!etEmail.getText().toString().trim().isEmpty()) {
            if (!Utils.isValidEmail(etEmail.getText().toString().trim())) {
                showToast(getString(R.string.email_error));
                return;
            }

        }

        /*Hit Registration API*/
        if (Utils.hasNetwork(this)) {
            mobileNumber = tvMobileNumber.getText().toString();
            if (!tvMobileNumber.getText().toString().startsWith("+91"))
                mobileNumber = "+91" + tvMobileNumber.getText().toString();
            showProgress("Please wait...");
            UserRegisteration user = new UserRegisteration();
            user.adLine1 = etLine1.getText().toString();
            user.adLine2 = addLine2.getText().toString();
            user.city = city;
            user.country = tvCountry.getText().toString();
            user.email = etEmail.getText().toString();
            user.latitude = lat;
            user.longitude = lng;
            user.name = etName.getText().toString();
            user.pinCode = etPincode.getText().toString();
            user.promotionCode = etPromotion.getText().toString();
            user.state = tvState.getText().toString();
            user.roles = role;
            user.phone = mobileNumber;

            /*,  role, tvState.getText().toString()*/
            RegisterNewUserApi api = new RegisterNewUserApi() {
                @Override
                public void onComplete(CommomResponse response) {
                    hideProgress();
                    UserIdModel model = response.toResponseModel(UserIdModel.class);
                    securePrefManager.storeSharedValue(OPTYID, "-1");
                    securePrefManager.storeSharedValue(USER_ID, model.getUserId());
                    Bundle bundle = new Bundle();
                    com.solarginni.SiteLovsModule.SiteDetailModel.Address address = new com.solarginni.SiteLovsModule.SiteDetailModel.Address();
                    address.setAddressLine1(user.adLine1);
                    address.setAddressLine2(user.adLine2);
                    address.setCity(user.city);
                    address.setCountry(user.country);
                    address.setPinCode(user.pinCode);
                    address.setLatitude(Double.valueOf(user.latitude));
                    address.setLongitude(Double.valueOf(user.longitude));
                    address.setState(user.state);
                    bundle.putSerializable("address", address);
                    bundle.putBoolean("usedForNewUser", true);
                    goToNextScreen(RaiseQuotationActivity.class, bundle);
                    finish();
                }

                @Override
                protected void onFailur(String msg) {
                    hideProgress();
                    showToast(msg);

                }
            };
            api.hit(securePrefManager.getSharedValue(TOKEN), user);

        } else {
            showToast(getResources().getString(R.string.internet_error));
        }


    }


    private boolean validateName() {
        if (etName.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.err_msg_name));
            requestFocus(etName);
            return false;
        }


        return true;
    }

    private boolean validateAddress1() {
        if (etLine1.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.err_msg_add));
            requestFocus(etLine1);
            return false;
        }

        return true;
    }

    private boolean validateAddress2() {
        if (addLine2.getText().toString().trim().isEmpty()) {
            showToast(getString(R.string.err_msg_add));
            requestFocus(addLine2);
            return false;
        }

        return true;
    }

    private boolean validateCity() {
        city = tvCity.getText().toString().equalsIgnoreCase("") ? etCity.getText().toString() : tvCity.getText().toString();

        if (city.trim().isEmpty() || city.length() < 3) {
            showToast("Please Add City");
            return false;
        }
        return true;
    }

    private boolean pincode() {
        if (etPincode.getText().toString().trim().isEmpty() || etPincode.getText().length() < 5) {
            showToast(getString(R.string.pincode_err));
            requestFocus(etPincode);
            return false;
        }

        return true;
    }

    private boolean phoneNumber() {
        if (tvMobileNumber.getText().toString().trim().isEmpty() || tvMobileNumber.getText().length() < 10) {
            showToast("Enter Valid Phone Number");
            requestFocus(tvMobileNumber);
            return false;
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void disableAutofill() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            getWindow().getDecorView().setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS);
    }

    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideKeyboard(RegisterNewUser.this);
                    etPincode.clearFocus();
                    etEmail.clearFocus();
                    etName.clearFocus();
                    etPromotion.clearFocus();
                    etLine1.clearFocus();
                    return false;
                }
            });
        }
    }

    private void openPlacePicker() {

        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), getString(R.string.google_api));
        }
        List<Place.Field> fields = Arrays.asList(Place.Field.ADDRESS_COMPONENTS, Place.Field.NAME, Place.Field.LAT_LNG);

        // Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields).build(Objects.requireNonNull(this));
        startActivityForResult(intent, REQUEST_FOR_PLACE);
    }


}
