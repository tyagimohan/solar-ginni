package com.solarginni.SGUser.AllUserModule;

import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.RestClient;

public class AllUserPresenter implements AllUserContract.Presenter, AllUserContract.onInterection {

    private AllUserContract.View view;
    private AllUSerInteractor interactor;


    public AllUserPresenter(AllUserContract.View view) {
        this.view = view;
        interactor = new AllUSerInteractor(RestClient.getRetrofitBuilder());
    }

    @Override
    public void fetchUser(String token, String key, int page, String status, String state) {
        interactor.fetchUser(token,key,page,status,state,this);

    }

    @Override
    public void updateStatus(String token, int value, String state) {

    }

    @Override
    public void onUpdateStatus() {

    }

    @Override
    public void onFetchAllUser(CommomResponse response) {
        view.onFetchAllUser(response);

    }

    @Override
    public void onSearchUser(CommomResponse response) {
        view.onSearchUser(response);

    }

    @Override
    public void onFailure(String msg) {
        view.onFailure(msg);

    }
}
