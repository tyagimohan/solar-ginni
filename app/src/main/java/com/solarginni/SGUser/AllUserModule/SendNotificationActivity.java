package com.solarginni.SGUser.AllUserModule;

import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.solarginni.Base.BaseActivity;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.SendNotificationModel;
import com.solarginni.R;
import com.solarginni.Utility.AppConstants;

public class SendNotificationActivity extends BaseActivity {
    private String[] typeArray = { "Mobile App","SMS", "Both"};
    private TextView notificationText, notificationType, customerId, contact, customerName;
    private int type = 1;
    private Button sendNotification;

    @Override
    public int getLayout() {
        return R.layout.send_notification_layout;
    }

    @Override
    public void initViews() {
        notificationText = findViewById(R.id.notificationText);
        notificationType = findViewById(R.id.notificationType);
        customerId = findViewById(R.id.customerId);
        sendNotification = findViewById(R.id.sendNotification);
        contact = findViewById(R.id.contact);
        customerName = findViewById(R.id.customerName);

        disableSuggestion(notificationText);

        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });

    }

    @Override
    public void setUp() {

        customerId.setText(getIntent().getExtras().getString("customerId", ""));
        customerName.setText(getIntent().getExtras().getString("customerName", ""));
        contact.setText(getIntent().getExtras().getString("contact", ""));


        notificationType.setOnClickListener(v -> showAlertWithList(typeArray, "Select Notification Typw", notificationType));
        sendNotification.setOnClickListener(v -> {
            if (!notificationText.getText().toString().trim().equalsIgnoreCase("")) {
                showProgress("Please wait..");
                SendNotificationModel model = new SendNotificationModel();
                model.message = notificationText.getText().toString();
                model.userId = customerId.getText().toString();
                model.notificationType = type;

                SendNotificationApi api = new SendNotificationApi() {
                    @Override
                    public void onComplete(CommomResponse response) {
                        hideProgress();
                        notificationText.clearFocus();
                        showToast("Notification Sent Successfully");
                    }

                    @Override
                    protected void onFailur(String msg) {
                        notificationText.clearFocus();

                        hideProgress();
                        showToast(msg);

                    }
                };
                api.hit(securePrefManager.getSharedValue(AppConstants.TOKEN), model);
            } else {
                showToast("Enter Notification Text");
            }

        });


    }

    public void showAlertWithList(final String[] dataArray, String title, final TextView textView) {
        hideKeyboard(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setItems(dataArray, (dialog, item) -> {
            textView.setText(dataArray[item]);
            type = item+1;
            textView.clearFocus();
        });
        AlertDialog alerts = builder.create();
        alerts.show();
    }
}
