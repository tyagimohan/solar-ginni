package com.solarginni.SGUser.AllUserModule;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;

import retrofit2.Retrofit;

public class AllUSerInteractor implements AllUserContract.Interector {

    private Retrofit retrofit;
    private ApiInterface apiInterface;

    public AllUSerInteractor(Retrofit retrofit) {
        this.retrofit = retrofit;
        apiInterface = retrofit.create(ApiInterface.class);
    }

    @Override
    public void fetchUser(String token, String key, int page, String status, String state, AllUserContract.onInterection listener) {
        apiInterface.getUserList(token, key, page, status, "All",state).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                if (key.equalsIgnoreCase("#"))
                    listener.onFetchAllUser(response);

                else
                    listener.onSearchUser(response);
            }

            @Override
            public void onError(ApiError error) {
                listener.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                listener.onFailure(throwable.getMessage());

            }
        });

    }

    @Override
    public void updateStatus(String token, int value, String state, AllUserContract.onInterection listener) {

    }
}
