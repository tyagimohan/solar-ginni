package com.solarginni.SGUser.AllUserModule;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.solarginni.DBModel.UserRoleModel;

import java.io.Serializable;
import java.util.List;


public class AllUserModel implements Serializable {


    @SerializedName("details")
    @Expose
    public List<Detail> details = null;

    public class Detail implements Serializable {
        @SerializedName("phone")
        @Expose
        public String phone;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("userId")
        @Expose
        public String userId;
        @SerializedName("companyName")
        @Expose
        public String companyName;
        @SerializedName("companyId")
        @Expose
        public String companyId;
        @SerializedName("onBoardDate")
        @Expose
        public String onboardDate;
        @SerializedName("email")
        @Expose
        public String email;
        @SerializedName("role")
        @Expose
        public Integer role;
        @SerializedName("address")
        @Expose
        public UserRoleModel.Address address;
        @SerializedName("imageUrl")
        @Expose
        public String imageUrl;
        @SerializedName("activate")
        @Expose
        public Boolean activate;
    }


}
