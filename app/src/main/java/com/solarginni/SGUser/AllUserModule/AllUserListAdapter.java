package com.solarginni.SGUser.AllUserModule;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.R;

public class AllUserListAdapter extends BaseRecyclerAdapter<AllUserModel.Detail, AllUserListAdapter.Holder> {


    public AllUserListAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public int getLayout(int viewType) {
        return R.layout.user_list_item;
    }

    @Override
    public Holder getViewHolder(View view, int viewType) {
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {

        holder.userPhone.setText(getItem(i).phone);

        SpannableString content = new SpannableString(getItem(i).name);
        content.setSpan(new UnderlineSpan(), 0, getItem(i).name.length(), 0);
        holder.userName.setText(content);


        holder.userId.setText(getItem(i).userId);

        switch (getItem(i).role) {
            case 7:
                holder.type.setText("Customer");
                break;
            case 1:
            case 9:
                holder.type.setText("Implementer");
                break;
            case 10:
                holder.type.setText("SG Admin");
                break;
            case 11:
                holder.type.setText("BO Manager");
                break;
            case 3:
                holder.type.setText("Prospect");
                break;
        }

        if (getItem(i).activate) {
            holder.status.setText("Activated");
        } else {
            holder.status.setText("Inactive");
        }

        holder.userName.setOnClickListener(v -> {
            getLongClickListener().onItemClick(getItem(i), i, holder.userName);
        });

            if (getItem(i).imageUrl!=null)
                ImageLoader.getInstance().displayImage(getItem(i).imageUrl,holder.profileImage);
            else {
                holder.profileImage.setImageDrawable(getContext().getDrawable(R.drawable.placeholder));
            }

    }

    public class Holder extends RecyclerView.ViewHolder {
        public TextView userName, userPhone, userId, type, status;
        public ImageView profileImage;

        public Holder(@NonNull View itemView) {
            super(itemView);
            userId = itemView.findViewById(R.id.userId);
            userName = itemView.findViewById(R.id.userName);
            userPhone = itemView.findViewById(R.id.userPhone);
            status = itemView.findViewById(R.id.status);
            type = itemView.findViewById(R.id.type);
            profileImage = itemView.findViewById(R.id.profile_image);
        }
    }

}
