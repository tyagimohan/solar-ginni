package com.solarginni.SGUser.AllUserModule;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.solarginni.Base.BaseActivity;
import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.CommonModule.MainActivity;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.Implementer.ImplementerUserModule.ImplementerUserDetail;
import com.solarginni.R;
import com.solarginni.Utility.AppConstants;
import com.solarginni.Utility.Utils;
import com.solarginni.user.customerDetail.CustomerDetailActivity;

import org.angmarch.views.NiceSpinner;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static com.solarginni.Utility.AppConstants.TOKEN;

public class AllUserListActivity extends BaseActivity implements AllUserContract.View, View.OnClickListener {

    private static final int REQUEST_FOR_USER = 5;
    private AllUserPresenter presenter;
    private RecyclerView recyclerView;
    private NiceSpinner roleSpinnier, statusSpinner;
    private LinearLayoutManager layoutManager;
    private boolean isLoading = false;
    private LinearLayout progressPagination;
    private String role = "12";
    private final RecyclerView.OnScrollListener recyclerScroller = new RecyclerView.OnScrollListener() {
        @SuppressWarnings("EmptyMethod")
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

            if (dy > 0)
                if (!isLoading) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition > 0) {
                        progressPagination.setVisibility(View.VISIBLE);
                        loadMoreItems(visibleItemCount + layoutManager.findFirstVisibleItemPosition());
                    }
                } else {

                }
        }

    };
    private AllUserListAdapter adapter;
    private EditText etMobileNumber;
    private Button searchButton;
    private boolean isRefresh = false;
    private boolean isFilter = false;
    private AllUserListAdapter.onItemClick<AllUserModel.Detail> longListenr = (data, position, view) -> {
        switch (view.getId()) {
            case R.id.userName:
                Intent intent = null;
                Bundle bundle = new Bundle();
                bundle.putBoolean("isFormasking", false);
                bundle.putBoolean("isForActivate", true);
                bundle.putInt("role", data.role);

                bundle.putString("id", data.userId);
                if (data.role == 1 | data.role == 9) {
                    bundle.putSerializable("data", data);
                    intent = new Intent(this, ImplementerUserDetail.class);

                } else {
                    intent = new Intent(this, CustomerDetailActivity.class);

                }
                intent.putExtras(bundle);
                startActivityForResult(intent, REQUEST_FOR_USER);
                break;
        }
    };

    @Override
    public int getLayout() {
        return R.layout.all_user_list;
    }

    @Override
    public void initViews() {

        recyclerView = findViewById(R.id.recyclerView);
        roleSpinnier = findViewById(R.id.state_spinner);
        statusSpinner = findViewById(R.id.status_spinner);
        searchButton = findViewById(R.id.serach_btn);
        etMobileNumber = findViewById(R.id.mobileNumber);
        progressPagination = (LinearLayout) findViewById(R.id.progressPagination);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new BaseRecyclerAdapter.DividerItemDecoration(this, RecyclerView.VERTICAL));
        adapter = new AllUserListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnScrollListener(recyclerScroller);
        adapter.setOnLongClickListener(longListenr);
        findViewById(R.id.refresh).setOnClickListener(this);
        presenter = new AllUserPresenter(this);

        List<String> dataset = new LinkedList<>(Arrays.asList("Active", "Inactive"));
        statusSpinner.attachDataSource(dataset);
        List<String> roledataset = new LinkedList<>(Arrays.asList(getResources().getStringArray(R.array.role_array)));
        roleSpinnier.attachDataSource(roledataset);
        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("");
        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_FOR_USER && resultCode == RESULT_OK) {
            findViewById(R.id.refresh).performClick();

        }
    }

    @Override
    public void setUp() {
        setupUI(findViewById(R.id.parent));
        setupUI(findViewById(R.id.recyclerView));
        searchButton.setOnClickListener(this);

        roleSpinnier.setOnSpinnerItemSelectedListener((parent, view, position, id) -> {
            if (Utils.hasNetwork(this)) {
                isFilter = true;
                showProgress("Please wait..");
                switch ("" + parent.getItemAtPosition(position)) {
                    case "National Controller":
                        role = AppConstants.SUPER_IMPLEMENTER_ROLE_ID;
                        break;
                    case "Sub-Controller":
                        role = AppConstants.IMPLEMENTER_ROLE_ID;

                        break;
                    case "Prospect":
                        role = AppConstants.PROSPECT_ROLE_ID;

                        break;
                    case "Customer":
                        role = AppConstants.CUSTOMER_ROLE_ID;

                        break;
                    case "Bo Manager":
                        role = AppConstants.BO_MANAGER;

                        break;
                    case "SG Admin":
                        role = AppConstants.SG_ADMIN;

                        break;
                    case "All":
                        role = AppConstants.ALL;
                        break;
                }
                presenter.fetchUser(securePrefManager.getSharedValue(TOKEN), "#", 0, statusSpinner.getText().toString(), role);
            } else {
                showToast("Please check Internet");

            }
        });

        statusSpinner.setOnSpinnerItemSelectedListener((parent, view, position, id) -> {
            if (Utils.hasNetwork(this)) {
                isFilter = true;
                showProgress("Please wait..");
                presenter.fetchUser(securePrefManager.getSharedValue(TOKEN), "#", 0, "" + parent.getItemAtPosition(position), role);
            } else {
                showToast("Please check Internet");

            }
        });

        role = getIntent().getExtras().getString("role", "12");
        if (role.equalsIgnoreCase("9")) {

            roleSpinnier.setSelectedIndex(1);

        } else if (role.equalsIgnoreCase("3")) {

            roleSpinnier.setSelectedIndex(3);


        } else if (role.equalsIgnoreCase("7")) {

            roleSpinnier.setSelectedIndex(4);

        }


        if (Utils.hasNetwork(this)) {
            showProgress("Please wait..");
            presenter.fetchUser(securePrefManager.getSharedValue(TOKEN), "#", 0, "Active", role);
        } else {
            showToast("Please check Internet");

        }
    }

    @Override
    public void onUpdateStatus() {

    }

    @Override
    public void onFetchAllUser(CommomResponse response) {
        hideProgress();
        progressPagination.setVisibility(View.GONE);

        AllUserModel model = response.toResponseModel(AllUserModel.class);
        etMobileNumber.setText("");


        if (isFilter) {
            isFilter = false;
            adapter.notifyItemRangeRemoved(0, adapter.getItemCount());
            adapter.clear();
            adapter.notifyDataSetChanged();
        }

        if (model.details.size() == 0 && isFilter) {
            showToast("No User Found");
            adapter.notifyItemRangeRemoved(0, adapter.getItemCount());
            adapter.clear();
            adapter.notifyDataSetChanged();
            if (isRefresh) {
                isRefresh = true;
                roleSpinnier.setText("All");
                statusSpinner.setText("Active");

            }

        } else {
            isLoading = false;
            if (model.details.size() > 0 && model.details.size() <= 9) {
                isLoading = true;
            }
            if (isRefresh) {
                isRefresh = false;
                adapter.notifyItemRangeRemoved(0, adapter.getItemCount());
                adapter.clear();
                adapter.notifyDataSetChanged();
                roleSpinnier.setSelectedIndex(0);
                statusSpinner.setSelectedIndex(0);


            }
            int initialCount = adapter.getItemCount();
            adapter.addAllForPagination(model.details);
            adapter.notifyItemRangeChanged(initialCount, model.details.size());
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onSearchUser(CommomResponse response) {
        hideProgress();
        AllUserModel model = response.toResponseModel(AllUserModel.class);
        isLoading = true;
        adapter.notifyItemRangeRemoved(0, adapter.getItemCount());
        if (model.details.size() == 0)
            showToast("No User Found");

        List<String> dataset = new LinkedList<>(Arrays.asList("Active", "Inactive"));
        statusSpinner.attachDataSource(dataset);
        List<String> roledataset = new LinkedList<>(Arrays.asList(getResources().getStringArray(R.array.role_array)));
        roleSpinnier.attachDataSource(roledataset);
        roleSpinnier.setSelectedIndex(0);
        statusSpinner.setSelectedIndex(0);
        adapter.addAll(model.details);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onFailure(String msg) {
        hideProgress();
        progressPagination.setVisibility(View.GONE);

        if (msg.equalsIgnoreCase("invalid token")) {
            securePrefManager.clearAppsAllPrefs();
            startActivity(new Intent(AllUserListActivity.this, MainActivity.class));
            finish();
        } else
            showToast(msg);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.serach_btn:
                hideKeyboard(AllUserListActivity.this);

                if (Utils.hasNetwork(this)) {
                    if (etMobileNumber.getText().toString().trim().length() == 0) {
                        showToast("Please enter mobile");
                        return;
                    } else {
                        showProgress("Please wait...");
                        etMobileNumber.clearFocus();
                        hideKeyboard(AllUserListActivity.this);
                        presenter.fetchUser(securePrefManager.getSharedValue(TOKEN), etMobileNumber.getText().toString(), 0, "Active", "12");
                        roleSpinnier.setSelectedIndex(0);
                        statusSpinner.setSelectedIndex(0);
                    }

                } else {
                    showToast("Please check Internet");
                }
                break;

            case R.id.refresh:
                if (Utils.hasNetwork(this)) {
                    showProgress("Please wait...");
                    isRefresh = true;
                    presenter.fetchUser(securePrefManager.getSharedValue(TOKEN), "#", 0, "Active", "12");

                } else {
                    showToast("Please check internet ");
                }
                break;
        }
    }

    private void loadMoreItems(int pos) {
        isLoading = true;
        int page = (pos / 10);
        presenter.fetchUser(securePrefManager.getSharedValue(TOKEN), "#", page, statusSpinner.getText().toString(), role);
    }

    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    view.requestFocus();
                    findViewById(R.id.serchView).clearFocus();
                    hideKeyboard(AllUserListActivity.this);
                    return false;
                }
            });
        }
    }

}
