package com.solarginni.SGUser.AllUserModule;

import com.solarginni.DBModel.CommomResponse;

interface AllUserContract {

    interface View {

        void onUpdateStatus();

        void onFetchAllUser(CommomResponse response);

        void onSearchUser(CommomResponse response);

        void onFailure(String msg);
    }

    interface Interector {


        void fetchUser(String token, String key, int page, String status, String state, onInterection listener);

        void updateStatus(String token, int value, String state, onInterection listener);
    }

    interface Presenter {
        void fetchUser(String token, String key, int page, String status, String state);

        void updateStatus(String token, int value, String state);
    }

    interface onInterection {

        void onUpdateStatus();

        void onFetchAllUser(CommomResponse response);

        void onSearchUser(CommomResponse response);

        void onFailure(String msg);
    }

}
