package com.solarginni.SGUser.CompanyPriceModule;

import android.content.Intent;
import android.graphics.Color;
import androidx.appcompat.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.solarginni.Base.BaseActivity;
import com.solarginni.R;

import java.util.Calendar;

public class SetPriceFilterActivity extends BaseActivity implements View.OnClickListener {

    private EditText state, month, year, siteRange, priceStatus;
    private TextView resdential, nonResidential, doneBtn, siteRange1, siteRange2, siteRange3;
    private String siteType;
    private String[] stateArr, monthArr, yearArr, siteRangeArr, priceStatusArr;
    private int range = 1;
    private Calendar calendar = Calendar.getInstance();


    @Override
    public int getLayout() {
        return R.layout.set_price_filter;
    }

    @Override
    public void initViews() {
        state = findViewById(R.id.state);
        month = findViewById(R.id.month);
        year = findViewById(R.id.year);
        resdential = findViewById(R.id.resdential_site);
        nonResidential = findViewById(R.id.non_residential);
        siteRange1 = findViewById(R.id.siteRange1);
        siteRange2 = findViewById(R.id.siteRange2);
        siteRange3 = findViewById(R.id.siteRange3);
        priceStatus = findViewById(R.id.priceStatus);
        doneBtn = findViewById(R.id.doneBtn);
        siteRange = findViewById(R.id.range);

    }

    @Override
    public void setUp() {

        resdential.setOnClickListener(this);
        nonResidential.setOnClickListener(this);
        doneBtn.setOnClickListener(this);
        state.setOnClickListener(this);
        month.setOnClickListener(this);
        year.setOnClickListener(this);
        siteRange.setOnClickListener(this);
        priceStatus.setOnClickListener(this);
        siteRange1.setOnClickListener(this);
        siteRange2.setOnClickListener(this);
        siteRange3.setOnClickListener(this);

        monthArr = getResources().getStringArray(R.array.months_array);
        stateArr = getResources().getStringArray(R.array.stateAll);
        yearArr = getResources().getStringArray(R.array.year);
        priceStatusArr = getResources().getStringArray(R.array.priceStatus);
        siteRangeArr = getResources().getStringArray(R.array.siteRange);
        month.setText(monthArr[calendar.get(Calendar.MONTH)]);

        Intent data = getIntent();

        state.setText(data.getStringExtra("state"));
        month.setText(data.getStringExtra("month"));
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (data.getStringExtra("siteType").equalsIgnoreCase("Residential")) {
            resdential.performClick();
        } else {
            nonResidential.performClick();

        }

        priceStatus.setText(data.getStringExtra("priceStatus"));

        switch (data.getIntExtra("range", 1)) {
            case 1:
                siteRange1.performClick();
                break;
            case 2:
                siteRange2.performClick();
                break;
            case 3:
                siteRange3.performClick();
                break;
        }


        siteRange.setText(siteRangeArr[data.getIntExtra("range", 1) - 1]);
        year.setText("" + data.getIntExtra("year", 2021));


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.doneBtn:
                Intent returnIntent = new Intent();
                returnIntent.putExtra("siteType", siteType);
                returnIntent.putExtra("range", range);
                returnIntent.putExtra("state", state.getText().toString());
                returnIntent.putExtra("month", month.getText().toString());
                returnIntent.putExtra("priceStatus", priceStatus.getText().toString());
                returnIntent.putExtra("year", year.getText().toString());

                setResult(RESULT_OK, returnIntent);
                finish();

                break;
            case R.id.resdential_site:
                siteType = "Residential";

                resdential.setBackgroundResource(R.drawable.button_bg);
                resdential.setTextColor(Color.WHITE);

                nonResidential.setBackgroundResource(R.drawable.button_bg_blue);
                nonResidential.setTextColor(getResources().getColor(R.color.colortheme));

                break;
            case R.id.non_residential:
                siteType = "Non-Residential";

                nonResidential.setBackgroundResource(R.drawable.button_bg);
                nonResidential.setTextColor(Color.WHITE);

                resdential.setBackgroundResource(R.drawable.button_bg_blue);
                resdential.setTextColor(getResources().getColor(R.color.colortheme));
                break;

            case R.id.range:
                showAlertWithRangeList(siteRangeArr, "Select Range", siteRange);

                break;
            case R.id.state:
                showAlertWithList(stateArr, "Select State", state);
                break;
            case R.id.month:
                showAlertWithList(monthArr, "Select Month", month);

                break;
            case R.id.year:
                showAlertWithList(yearArr, "Select Year", year);

                break;
            case R.id.priceStatus:
                showAlertWithList(priceStatusArr, "Select Status", priceStatus);

                break;
            case R.id.siteRange1:
                range = 1;

                siteRange1.setBackgroundResource(R.drawable.button_bg);
                siteRange1.setTextColor(Color.WHITE);

                siteRange2.setBackgroundResource(R.drawable.button_bg_blue);
                siteRange2.setTextColor(getResources().getColor(R.color.colortheme));

                siteRange3.setBackgroundResource(R.drawable.button_bg_blue);
                siteRange3.setTextColor(getResources().getColor(R.color.colortheme));

                break;
            case R.id.siteRange2:
                range = 2;

                siteRange2.setBackgroundResource(R.drawable.button_bg);
                siteRange2.setTextColor(Color.WHITE);

                siteRange1.setBackgroundResource(R.drawable.button_bg_blue);
                siteRange1.setTextColor(getResources().getColor(R.color.colortheme));

                siteRange3.setBackgroundResource(R.drawable.button_bg_blue);
                siteRange3.setTextColor(getResources().getColor(R.color.colortheme));

                break;
            case R.id.siteRange3:

                range = 3;

                siteRange3.setBackgroundResource(R.drawable.button_bg);
                siteRange3.setTextColor(Color.WHITE);

                siteRange1.setBackgroundResource(R.drawable.button_bg_blue);
                siteRange1.setTextColor(getResources().getColor(R.color.colortheme));

                siteRange2.setBackgroundResource(R.drawable.button_bg_blue);
                siteRange2.setTextColor(getResources().getColor(R.color.colortheme));
                break;
        }

    }


    public void showAlertWithRangeList(final String[] dataArray, String title, final TextView textView) {
        hideKeyboard(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setItems(dataArray, (dialog, item) -> {
            textView.setText(dataArray[item]);
            range = item + 1;
            textView.clearFocus();
        });
        AlertDialog alerts = builder.create();
        alerts.show();
    }
}
