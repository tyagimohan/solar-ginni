package com.solarginni.SGUser.CompanyPriceModule;

import com.solarginni.DBModel.CommomResponse;

interface CompanyPriceContract {

    interface CompanyPricePresenter{
        void fetchCompany(String token, String key,  String state,  String month,  String siteType,
                          String undefined,  String orderBy,  int page,  int siteRange,  int year,  String sort);
    }

    interface CompanyPriceInteract{
        void fetchCompany(String token, String key,  String state,  String month,  String siteType,
                          String undefined,  String orderBy,  int page,  int siteRange,  int year,  String sort,OnInetraction listener);
    }

    interface OnInetraction{
        void onFetchCompanies(CommomResponse response);
        void onSearchCompany(CommomResponse response);
        void Onfailure(String msg);
    }

    interface View{

        void onFetchCompanies(CommomResponse response);
        void onSearchCompany(CommomResponse response);
        void Onfailure(String msg);

    }

}
