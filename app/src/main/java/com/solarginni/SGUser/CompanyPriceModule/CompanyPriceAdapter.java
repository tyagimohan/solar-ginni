package com.solarginni.SGUser.CompanyPriceModule;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.TextView;

import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.R;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class CompanyPriceAdapter extends BaseRecyclerAdapter<CompanyFilterData.Pricelist, CompanyPriceAdapter.Holder> {


    public CompanyPriceAdapter(Context mContext) {
        super(mContext);
    }

    @Override
    public int getLayout(int viewType) {
        return R.layout.company_price_item;
    }

    @Override
    public Holder getViewHolder(View view, int viewType) {
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {


        if (getItem(i).range!=null){
            if (getItem(i).range== 1) {
                holder.siteRange.setText("1.00-10.00");

            } else if (getItem(i).range == 2) {
                holder.siteRange.setText("10.01-100.00");
            } else {
                holder.siteRange.setText(" >100.01");
            }
        }
        else {
            holder.siteRange.setText("");
        }
        holder.siteType.setText(getItem(i).type);

        if (getItem(i).price!=null){
            DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
            formatter.applyPattern("#,##,##,###");
            String formattedString = formatter.format(getItem(i).price.longValue());
            holder.pricing.setText("\u20B9 "+formattedString);
        }
        else {
            holder.pricing.setText("");
        }

        if (getItem(i).state!=null){
            holder.state.setText(getItem(i).state);
        }
        else {
            holder.state.setText("");
        }


        SpannableString content = new SpannableString(getItem(i).companyname);
        content.setSpan(new UnderlineSpan(), 0, getItem(i).companyname.length(), 0);
        holder.implementerName.setText(content);

        holder.implementerName.setOnClickListener(v -> {
            getListener().onClick(getItem(i),i);

        });
        //holder.implementerName.setText(getItem(i).companyname);



    }

    public class Holder extends RecyclerView.ViewHolder{
        private TextView pricing,implementerName,siteType,siteRange,state;

        public Holder(@NonNull View itemView) {
            super(itemView);
            pricing= itemView.findViewById(R.id.pricing);
            implementerName= itemView.findViewById(R.id.implementerName);
            siteType= itemView.findViewById(R.id.siteType);
            siteRange= itemView.findViewById(R.id.siteRange);
            state= itemView.findViewById(R.id.state);
        }
    }

}
