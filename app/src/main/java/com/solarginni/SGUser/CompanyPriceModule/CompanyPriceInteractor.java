package com.solarginni.SGUser.CompanyPriceModule;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;

import retrofit2.Retrofit;

public
class CompanyPriceInteractor implements CompanyPriceContract.CompanyPriceInteract {

    private Retrofit retrofit;
    private ApiInterface apiInterface;

    public CompanyPriceInteractor(Retrofit retrofit) {
        this.retrofit = retrofit;
        apiInterface = retrofit.create(ApiInterface.class);
    }

    @Override
    public void fetchCompany(String token, String key, String state, String month, String siteType,
                             String undefined, String orderBy, int page, int siteRange, int year, String sort, CompanyPriceContract.OnInetraction listener) {

        apiInterface.fetchPricedCompany(token, key, state, month, siteType, undefined, orderBy, page, siteRange, year, sort).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                if (key.equalsIgnoreCase("#")) {
                    listener.onFetchCompanies(response);
                } else {
                    listener.onSearchCompany(response);
                }

            }

            @Override
            public void onError(ApiError error) {

                listener.Onfailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {

                listener.Onfailure(throwable.getMessage());

            }
        });


    }
}
