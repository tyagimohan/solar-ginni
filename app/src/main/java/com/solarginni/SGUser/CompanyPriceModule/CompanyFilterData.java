package com.solarginni.SGUser.CompanyPriceModule;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CompanyFilterData implements Serializable {

    @SerializedName("pricelist")
    @Expose
    public List<Pricelist> pricelist = new ArrayList();


    public class Pricelist {

        @SerializedName("companyname")
        @Expose
        public String companyname;
        @SerializedName("type")
        @Expose
        public String type;
        @SerializedName("userid")
        @Expose
        public String userId;
        @SerializedName("range")
        @Expose
        public Integer range;
        @SerializedName("price")
        @Expose
        public Integer price;
        @SerializedName("state")
        @Expose
        public String state;
        @SerializedName("month")
        @Expose
        public String month;

    }
}
