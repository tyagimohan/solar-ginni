package com.solarginni.SGUser.CompanyPriceModule;

import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.RestClient;

public class CompanyPricePresenter implements CompanyPriceContract.CompanyPricePresenter, CompanyPriceContract.OnInetraction {

    private CompanyPriceContract.View view;
    private CompanyPriceInteractor interactor;

    public CompanyPricePresenter(CompanyPriceContract.View view) {
        this.view = view;
        ;
        interactor = new CompanyPriceInteractor(RestClient.getRetrofitBuilder());
    }

    @Override
    public void fetchCompany(String token, String key,  String state,  String month,  String siteType,
                             String undefined,  String orderBy,  int page,  int siteRange,  int year,  String sort) {
        interactor.fetchCompany(token,key,state,month,siteType,undefined,orderBy,page,siteRange,year,sort,this);

    }

    @Override
    public void onFetchCompanies(CommomResponse response) {


        view.onFetchCompanies(response);
    }

    @Override
    public void onSearchCompany(CommomResponse response) {
             view.onSearchCompany(response);
    }

    @Override
    public void Onfailure(String msg) {

        view.Onfailure(msg);

    }
}
