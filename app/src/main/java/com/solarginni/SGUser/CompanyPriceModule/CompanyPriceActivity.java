package com.solarginni.SGUser.CompanyPriceModule;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.solarginni.Base.BaseActivity;
import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.Implementer.ImplementerDetails.ImplemeterDetailsActivity;
import com.solarginni.R;
import com.solarginni.Utility.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.solarginni.Utility.AppConstants.TOKEN;

public class CompanyPriceActivity extends BaseActivity implements CompanyPriceContract.View, View.OnClickListener {
    private static final int REQUEST_FOR_FILTER = 10;
    private LinearLayout lay1, lay2;
    private RecyclerView recyclerView;
    SortingAdapter sortingAdapter;
    List<SortingModel> sortedList = new ArrayList<>();
    private CompanyPricePresenter pricePresenter;
    private Calendar calendar = Calendar.getInstance();


    private String key = "#", state = "All", month, siteTyp = "Residential";
    private String undefined = "defined", orderBy = "asc", sort = "price";
    private int page = 0, siteRange = 1, year = 2021;
    private EditText companyName;
    private Button searchButton;
    private boolean isRefresh = false;
    private boolean isFilter = false;
    private boolean isLoading = false;
    private LinearLayout progressPagination;
    private CompanyPriceAdapter adapter;
    private LinearLayoutManager layoutManager;
    private String[] monthArr;


    @Override
    public int getLayout() {
        return R.layout.company_price_activity;
    }

    @Override
    public void initViews() {
        lay1 = findViewById(R.id.lay1);
        lay2 = findViewById(R.id.lay2);
        recyclerView = findViewById(R.id.recyclerView);


        searchButton = findViewById(R.id.serach_btn);
        companyName = findViewById(R.id.companyName);
        progressPagination = (LinearLayout) findViewById(R.id.progressPagination);


        adapter = new CompanyPriceAdapter(this);
        recyclerView.setAdapter(adapter);
        adapter.setOnClickListener(listner);
        recyclerView.addOnScrollListener(recyclerScroller);


        findViewById(R.id.refresh).setOnClickListener(this);

        monthArr = getResources().getStringArray(R.array.months_array);

        month= monthArr[calendar.get(Calendar.MONTH)];
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new BaseRecyclerAdapter.DividerItemDecoration(this, RecyclerView.VERTICAL));

        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        pricePresenter = new CompanyPricePresenter(this);

        sortingAdapter = new SortingAdapter(this);
        prepaerList();
        sortingAdapter.addAll(sortedList);

        getSupportActionBar().setTitle("");
        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });

    }

    @Override
    public void setUp() {

        setupUI(findViewById(R.id.parent));
        setupUI(findViewById(R.id.recyclerView));

        searchButton.setOnClickListener(this);
        lay1.setOnClickListener(this);
        lay2.setOnClickListener(this);

        if (Utils.hasNetwork(this)) {
            showProgress("Please wait...");
            pricePresenter.fetchCompany(securePrefManager.getSharedValue(TOKEN), "#", state, monthArr[calendar.get(Calendar.MONTH)], siteTyp, undefined, orderBy, page, siteRange, year, sort);
        } else {
            showToast("Please check internet");
        }

    }

    @Override
    public void onFetchCompanies(CommomResponse response) {
        hideProgress();

        progressPagination.setVisibility(View.GONE);

        CompanyFilterData data = response.toResponseModel(CompanyFilterData.class);

        if (isFilter) {
            isFilter = false;
            adapter.notifyItemRangeRemoved(0, adapter.getItemCount());
            adapter.clear();
            adapter.notifyDataSetChanged();
        }

        if (data.pricelist.size() == 0 && isFilter) {
            showToast("No Company Found");
            adapter.notifyItemRangeRemoved(0, adapter.getItemCount());
            adapter.clear();
            adapter.notifyDataSetChanged();

        } else {
            isLoading = false;
            if (data.pricelist.size() > 0 && data.pricelist.size() <= 9) {
                isLoading = true;
            }
            if (isRefresh) {
                isRefresh = false;
                companyName.setText("");
                adapter.notifyItemRangeRemoved(0, adapter.getItemCount());
                adapter.clear();
                adapter.notifyDataSetChanged();

            }
            int initialCount = adapter.getItemCount();
            adapter.addAllForPagination(data.pricelist);
            adapter.notifyItemRangeChanged(initialCount, data.pricelist.size());
            adapter.notifyDataSetChanged();
        }


    }

    @Override
    public void onSearchCompany(CommomResponse response) {
        hideProgress();
        CompanyFilterData data = response.toResponseModel(CompanyFilterData.class);

        isLoading = true;
        adapter.notifyItemRangeRemoved(0, adapter.getItemCount());
        if (data.pricelist.size() == 0)
            showToast("No Company Found");
        adapter.addAll(data.pricelist);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void Onfailure(String msg) {
        hideProgress();
        showToast(msg);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay1:
                showSortingDialog(this);
                break;

            case R.id.lay2:
                Intent intent = new Intent(this, SetPriceFilterActivity.class);
                intent.putExtra("siteType", siteTyp);
                intent.putExtra("range", siteRange);
                intent.putExtra("state", state);
                intent.putExtra("month", month);
                intent.putExtra("priceStatus", undefined);
                intent.putExtra("year", year);
                startActivityForResult(intent, REQUEST_FOR_FILTER);
                break;

            case R.id.serach_btn:
                hideKeyboard(CompanyPriceActivity.this);
                if (Utils.hasNetwork(this)) {
                    if (companyName.getText().toString().trim().length() == 0) {
                        showToast("Please enter keyword");
                        return;
                    } else {
                        showProgress("Please wait...");
                        companyName.clearFocus();
                        hideKeyboard(CompanyPriceActivity.this);
                        pricePresenter.fetchCompany(securePrefManager.getSharedValue(TOKEN), companyName.getText().toString().trim(), state, monthArr[calendar.get(Calendar.MONTH)], siteTyp, undefined, orderBy, page, siteRange, year, sort);
                    }

                } else {
                    showToast("Please check Internet");
                }
                break;

            case R.id.refresh:
                if (Utils.hasNetwork(this)) {
                    showProgress("Please wait...");
                    isRefresh = true;
                    key = "#";
                    state = "All";
                    month = monthArr[calendar.get(Calendar.MONTH)];
                    siteTyp = "Residential";
                    undefined = "defined";
                    orderBy = "asc";
                    sort = "price";
                    page = 0;
                    siteRange = 1;
                    year = 2021;
                    handleSortClick(0);

                    pricePresenter.fetchCompany(securePrefManager.getSharedValue(TOKEN), "#", state, monthArr[calendar.get(Calendar.MONTH)], siteTyp, undefined, orderBy, page, siteRange, year, sort);

                } else {
                    showToast("Please check internet ");
                }
                break;
        }
    }

    private void showSortingDialog(Context context) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.sorting_layout, null);
        Animation slideUpIn;
        slideUpIn = AnimationUtils.loadAnimation(this, R.anim.slide_in_bottom);
        dialogView.startAnimation(slideUpIn);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);
        AlertDialog alertDialog = dialogBuilder.create();

        RecyclerView recyclerView = dialogView.findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(manager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.addItemDecoration(new DividerItemDecoration(context, 0));


        recyclerView.setAdapter(sortingAdapter);
        sortingAdapter.setOnClickListener((data, position) -> {
            handleSortClick(position);

            switch (position) {
                case 0:
                    sort = "price";
                    orderBy = "asc";
                    break;
                case 1:
                    sort = "price";
                    orderBy = "desc";
                    break;
                case 2:
                    sort = "site_range";
                    orderBy = "asc";
                    break;
                case 3:
                    sort = "site_range";
                    orderBy = "desc";
                    break;
            }
            isFilter=true;

            String key="#";
            if (!companyName.getText().toString().trim().equalsIgnoreCase("")){
                key=companyName.getText().toString().trim();
            }


            pricePresenter.fetchCompany(securePrefManager.getSharedValue(TOKEN), key, state, month, siteTyp, undefined, orderBy, page, siteRange, year, sort);


            sortingAdapter.notifyDataSetChanged();
            alertDialog.dismiss();
        });


        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.BOTTOM;
        window.setAttributes(wlp);

        alertDialog.show();

    }

    public class SortingAdapter extends BaseRecyclerAdapter<SortingModel, SortingAdapter.Holder> {


        public SortingAdapter(Context mContext) {
            super(mContext);
        }

        @Override
        public int getLayout(int viewType) {
            return R.layout.sort_layout_item;
        }

        @Override
        public SortingAdapter.Holder getViewHolder(View view, int viewType) {
            return new SortingAdapter.Holder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull SortingAdapter.Holder holder, int i) {
            holder.sortText.setText(getItem(i).sortingText);
            if (getItem(i).getSelected()) {
                holder.radioButton.setChecked(true);
            } else
                holder.radioButton.setChecked(false);

        }


        public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView sortText;
            RadioButton radioButton;

            public Holder(@NonNull View itemView) {
                super(itemView);
                sortText = itemView.findViewById(R.id.sortText);
                radioButton = itemView.findViewById(R.id.radio_btn);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                getListener().onClick(getItem(getAdapterPosition()), getAdapterPosition());
            }
        }
    }

    public class SortingModel {
        private String sortingText;
        private Boolean isSelected;


        public SortingModel() {

        }

        public String getSortingText() {
            return sortingText;
        }

        public void setSortingText(String sortingText) {
            this.sortingText = sortingText;
        }

        public Boolean getSelected() {
            return isSelected;
        }

        public void setSelected(Boolean selected) {
            isSelected = selected;
        }

    }

    public void prepaerList() {

        SortingModel model1 = new SortingModel();

        model1.setSortingText("Price -- Low to High");
        model1.setSelected(true);
        sortedList.add(model1);

        String[] array = new String[]{"Price -- High to Low"};
        for (int i = 0; i < array.length; i++) {
            SortingModel model = new SortingModel();
            model.setSelected(false);
            model.setSortingText(array[i]);
            sortedList.add(model);
        }


    }

    private void handleSortClick(int pos) {




        for (int i = 0; i < sortedList.size(); i++) {
            if (i == pos) {
                sortedList.get(i).setSelected(true);
            } else {
                sortedList.get(i).setSelected(false);
            }
        }

    }

    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    view.requestFocus();
                    findViewById(R.id.serchView).clearFocus();
                    hideKeyboard(CompanyPriceActivity.this);
                    return false;
                }
            });
        }
    }

    private final RecyclerView.OnScrollListener recyclerScroller = new RecyclerView.OnScrollListener() {
        @SuppressWarnings("EmptyMethod")
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            int visibleItemCount = layoutManager.getChildCount();
            int totalItemCount = layoutManager.getItemCount();
            int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

            if (dy > 0)
                if (!isLoading) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition > 0) {
                        progressPagination.setVisibility(View.VISIBLE);
                        loadMoreItems(visibleItemCount + layoutManager.findFirstVisibleItemPosition());
                    }
                } else {

                }
        }

    };


    private void loadMoreItems(int pos) {
        isLoading = true;
        int count = (pos / 10);
        pricePresenter.fetchCompany(securePrefManager.getSharedValue(TOKEN), "#", state, monthArr[calendar.get(Calendar.MONTH)], siteTyp, undefined, orderBy, count, siteRange, year, sort);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            key = companyName.getText().toString();
            state = data.getStringExtra("state");
            month = data.getStringExtra("month");
            siteTyp = data.getStringExtra("siteType");
            undefined = data.getStringExtra("priceStatus");
            siteRange = data.getIntExtra("range", 1);
            year = Integer.parseInt(data.getStringExtra("year"));
            page = 0;
            isFilter = true;

            pricePresenter.fetchCompany(securePrefManager.getSharedValue(TOKEN), key, state,month, siteTyp, undefined, orderBy, page, siteRange, year, sort);

        }

    }


    private CompanyPriceAdapter.OnClick<CompanyFilterData.Pricelist> listner = (data, position) -> {

        Bundle bundle = new Bundle();
        bundle.putBoolean("isFormasking", false);
        bundle.putString("id", data.userId);
        goToNextScreen(ImplemeterDetailsActivity.class, bundle);

    };
}
