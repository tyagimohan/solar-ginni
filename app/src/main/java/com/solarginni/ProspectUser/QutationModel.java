package com.solarginni.ProspectUser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class QutationModel implements Serializable {

    @SerializedName("filter")
    @Expose
    private List<Filter> filter = null;

    public List<Filter> getFilter() {
        return filter;
    }

    public void setFilter(List<Filter> filter) {
        this.filter = filter;
    }

    public static class Filter implements Serializable {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("userid")
        @Expose
        private String userId;
        @SerializedName("name")
        @Expose
        private String companyName;
        @SerializedName("url")
        @Expose
        public String url;
        @SerializedName("status")
        @Expose
        private boolean isQuoted;
        @SerializedName("sitesize")
        @Expose
        private Double sitesize;
        @SerializedName("sitesizestate")
        @Expose
        private Double stateSitesize;
        @SerializedName("sitecount")
        @Expose
        private Integer sitecount;

        public Double getStateSitesize() {
            return stateSitesize;
        }

        public void setStateSitesize(Double stateSitesize) {
            this.stateSitesize = stateSitesize;
        }

        public Integer getStateSiteCount() {
            return stateSiteCount;
        }

        public void setStateSiteCount(Integer stateSiteCount) {
            this.stateSiteCount = stateSiteCount;
        }

        @SerializedName("sitecountstate")
        @Expose
        private Integer stateSiteCount;
        @SerializedName("max")
        @Expose
        private Double max;
        @SerializedName("min")
        @Expose
        private Double min;
        @SerializedName("price")
        private Integer price;

        public Double getSitesize() {
            return sitesize;
        }

        public void setSitesize(Double sitesize) {
            this.sitesize = sitesize;
        }

        public Integer getSitecount() {
            return sitecount;
        }

        public void setSitecount(Integer sitecount) {
            this.sitecount = sitecount;
        }

        public Double getMax() {
            return max;
        }

        public void setMax(Double max) {
            this.max = max;
        }

        public Double getMin() {
            return min;
        }

        public void setMin(Double min) {
            this.min = min;
        }

        public Integer getPrice() {
            return price;
        }

        public void setPrice(Integer price) {
            this.price = price;
        }

        public boolean isQuoted() {
            return isQuoted;
        }

        public void setQuoted(boolean quoted) {
            isQuoted = quoted;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }


    }


}
