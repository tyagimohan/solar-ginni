package com.solarginni.ProspectUser;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.solarginni.Base.BaseFragment;
import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.DBModel.DemandQuoteModel;
import com.solarginni.Implementer.ImplementerDetails.ImplemeterDetailsActivity;
import com.solarginni.R;
import com.solarginni.Utility.AppDialogs;
import com.solarginni.Utility.Utils;
import com.xw.repo.BubbleSeekBar;

import java.util.ArrayList;
import java.util.List;

import static com.solarginni.Utility.AppConstants.OPTYID;
import static com.solarginni.Utility.AppConstants.SORTING_VALUE;
import static com.solarginni.Utility.AppConstants.TOKEN;
import static com.solarginni.Utility.AppConstants.USER_ID;

public class ProsepectFragment3 extends BaseFragment implements GetQuotationContract.View, View.OnClickListener {

    public static List<QutationModel.Filter> list = new ArrayList<>();
    public static QuotatImplementerListAdapter adapter;
    List<SortingModel> sortedList = new ArrayList<>();
    SortingAdapter sortingAdapter;
    private TextView sortValue, filterValue;
    private RecyclerView recyclerView;
    private GetQuotePresenter presenter;
    private LinearLayout lay1, lay2;
    private ImageView prevBtn;
    private boolean isFilterApply = false;
    private boolean isResetValue = false;
    private int pos;
    private int siteFiltervalue = 0, distanceFilterVal = 100, priceFilterValue = 70;
    private int sortedIndex = 0;
    private boolean isEmpaneledImplementer = false;
    private String[] sortedValueArray;
    protected AppDialogs mAppDialogs;


    @Override
    public int getLayout() {
        return R.layout.final_quote_page;
    }

    @Override
    public void initViews(View view) {
        recyclerView = view.findViewById(R.id.recyclerView);
        sortValue = view.findViewById(R.id.sortValue);
        filterValue = view.findViewById(R.id.filterValue);

        lay1 = view.findViewById(R.id.lay1);
        lay2 = view.findViewById(R.id.lay2);


        prevBtn = view.findViewById(R.id.prevBtn);

        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.addItemDecoration(new BaseRecyclerAdapter.DividerItemDecoration(getContext(), RecyclerView.VERTICAL));
        presenter = new GetQuotePresenter(this);
        sortingAdapter = new SortingAdapter(getContext());
        sortedValueArray = getResources().getStringArray(R.array.sorted_value);
        prepaerList();
        sortingAdapter.addAll(sortedList);
        mAppDialogs = new AppDialogs(getActivity());



    }

    @Override
    public void setUp() {

        lay1.setOnClickListener(this);
        lay2.setOnClickListener(this);

        prevBtn.setOnClickListener(this);
        sortValue.setOnClickListener(this);
        filterValue.setOnClickListener(this);


        adapter = new QuotatImplementerListAdapter(getContext());
        adapter.addAll(list);
        adapter.setOnClickListener(listner);
        adapter.setOnLongClickListener(longListener);
        recyclerView.setAdapter(adapter);


    }

    @Override
    public void onApplyFilter(List<QutationModel.Filter> list) {
        hideProgress();

        isFilterApply = true;
        if (isResetValue) {
            handleSortClick(0);
        }
        adapter.notifyItemRangeRemoved(0, adapter.getItemCount());

        adapter.addAll(list);
        adapter.notifyDataSetChanged();

    }

    @Override
    public void onGetQuote() {

        hideProgress();
        adapter.getItem(pos).setQuoted(true);
        adapter.notifyDataSetChanged();
        prevBtn.setVisibility(View.GONE);
        showToast("Quote Requested");

    }

    @Override
    public void failure(String msg) {

        hideProgress();
        showToast(msg);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.prevBtn:
            case R.id.sortValue:
            case R.id.lay1:
                GetQuotationActivity.viewPager.setCurrentItem(GetQuotationActivity.viewPager.getCurrentItem()-1);
                break;
            case R.id.filterValue:
            case R.id.lay2:
                showFilterDialog(getContext());
                break;

        }

    }

    private void showSortingDialog(Context context) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.sorting_layout, null);
        Animation slideUpIn;
        slideUpIn = AnimationUtils.loadAnimation(getContext(), R.anim.slide_in_bottom);
        dialogView.startAnimation(slideUpIn);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);
        AlertDialog alertDialog = dialogBuilder.create();

        RecyclerView recyclerView = dialogView.findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.addItemDecoration(new DividerItemDecoration(context, 0));


        recyclerView.setAdapter(sortingAdapter);
        sortingAdapter.setOnClickListener((data, position) -> {

            presenter.sortListByValue(getContext(), sortingAdapter.getItem(position).getSortingText());
            handleSortClick(position);
            sortingAdapter.notifyDataSetChanged();
            alertDialog.dismiss();
        });


        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.BOTTOM;
        window.setAttributes(wlp);

        alertDialog.show();

    }


    private void showFilterDialog(Context context) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.filter_layout, null);
        Animation slideUpIn;
        slideUpIn = AnimationUtils.loadAnimation(getContext(), R.anim.slide_in_bottom);
        dialogView.startAnimation(slideUpIn);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);
        AlertDialog alertDialog = dialogBuilder.create();
        BubbleSeekBar priceSeekBar, siteSeekbar, distanceSeekbar;
        TextView applyFilter, withEmpaneled, withoutEmapaneled;

        priceSeekBar = dialogView.findViewById(R.id.priceSeekbar);
        siteSeekbar = dialogView.findViewById(R.id.siteSeekbar);
        distanceSeekbar = dialogView.findViewById(R.id.distanceSeekbar);
        applyFilter = dialogView.findViewById(R.id.applyFilter);
        withEmpaneled = dialogView.findViewById(R.id.with_empaneled);
        withoutEmapaneled = dialogView.findViewById(R.id.without_empaneled);

        withEmpaneled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                isEmpaneledImplementer = true;
                withEmpaneled.setBackgroundResource(R.drawable.button_bg);
                withEmpaneled.setTextColor(Color.WHITE);

                withoutEmapaneled.setBackgroundResource(R.drawable.button_bg_blue);
                withoutEmapaneled.setTextColor(getResources().getColor(R.color.colortheme));

            }
        });

        withoutEmapaneled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isEmpaneledImplementer = false;
                withoutEmapaneled.setBackgroundResource(R.drawable.button_bg);
                withoutEmapaneled.setTextColor(Color.WHITE);

                withEmpaneled.setBackgroundResource(R.drawable.button_bg_blue);
                withEmpaneled.setTextColor(getResources().getColor(R.color.colortheme));

            }
        });

        priceSeekBar.setProgress(priceFilterValue);
        siteSeekbar.setProgress(siteFiltervalue);
        distanceSeekbar.setProgress(distanceFilterVal);
        if (isEmpaneledImplementer) {
            withEmpaneled.performClick();
        } else {
            withoutEmapaneled.performClick();
        }

        applyFilter.setOnClickListener(view -> {
            if (Utils.hasNetwork(getContext())) {
                showProgress("Please wait");
                siteFiltervalue = siteSeekbar.getProgress();
                distanceFilterVal = distanceSeekbar.getProgress();
                priceFilterValue = priceSeekBar.getProgress();
                isResetValue = true;
                presenter.applyFilter(getContext(), securePrefManager.getSharedValue(TOKEN), priceSeekBar.getProgress() + "000", String.valueOf(distanceSeekbar.getProgress()), String.valueOf(siteSeekbar.getProgress()), securePrefManager.getSharedValue(OPTYID), isEmpaneledImplementer, Integer.parseInt(securePrefManager.getSharedValue(SORTING_VALUE)));
            } else {
                showToast("Please check Internet");

            }

            alertDialog.dismiss();
        });


        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.BOTTOM;
        window.setAttributes(wlp);

        alertDialog.show();

    }

    public void prepaerList() {

        SortingModel model1 = new SortingModel();

        model1.setSortingText(sortedValueArray[0]);
        model1.setSelected(true);
        sortedList.add(model1);


        for (int i = 1; i < sortedValueArray.length; i++) {
            SortingModel model = new SortingModel();
            model.setSelected(false);
            model.setSortingText(sortedValueArray[i]);
            sortedList.add(model);
        }


    }

    private void handleSortClick(int pos) {
        sortedIndex = pos;

        for (int i = 0; i < sortedList.size(); i++) {
            if (i == pos) {
                sortedList.get(i).setSelected(true);
            } else {
                sortedList.get(i).setSelected(false);
            }
        }

    }

    public class SortingAdapter extends BaseRecyclerAdapter<SortingModel, SortingAdapter.Holder> {


        public SortingAdapter(Context mContext) {
            super(mContext);
        }

        @Override
        public int getLayout(int viewType) {
            return R.layout.sort_layout_item;
        }

        @Override
        public Holder getViewHolder(View view, int viewType) {
            return new Holder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull Holder holder, int i) {
            holder.sortText.setText(getItem(i).sortingText);
            if (getItem(i).getSelected()) {
                holder.radioButton.setChecked(true);
            } else
                holder.radioButton.setChecked(false);

        }


        public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView sortText;
            RadioButton radioButton;

            public Holder(@NonNull View itemView) {
                super(itemView);
                sortText = itemView.findViewById(R.id.sortText);
                radioButton = itemView.findViewById(R.id.radio_btn);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                getListener().onClick(getItem(getAdapterPosition()), getAdapterPosition());
            }
        }
    }

    public class SortingModel {
        private String sortingText;
        private Boolean isSelected;


        public SortingModel() {

        }

        public String getSortingText() {
            return sortingText;
        }

        public void setSortingText(String sortingText) {
            this.sortingText = sortingText;
        }

        public Boolean getSelected() {
            return isSelected;
        }

        public void setSelected(Boolean selected) {
            isSelected = selected;
        }

    }

    private QuotatImplementerListAdapter.onItemClick<QutationModel.Filter> longListener = (data, position, view) -> {
        DemandQuoteModel model = new DemandQuoteModel();
        model.setImpleId(data.getUserId());
        Bundle bundle = new Bundle();
        bundle.putBoolean("isFormasking", true);
        bundle.putBoolean("isDemanded", data.isQuoted());
        bundle.putString("id", data.getUserId());
        bundle.putString("data", Utils.toString(model));
        goToNextScreen(ImplemeterDetailsActivity.class, bundle);
    };

    QuotatImplementerListAdapter.OnClick<QutationModel.Filter> listner = (data, position) -> {


        mAppDialogs.showInfoDialogBothButtons("Cancel", "Do you want to demand quote from this Implementer?", new AppDialogs.DialogInfo() {
            @Override
            public void onPositiveButton(DialogInterface dialog, int which) {
                            dialog.dismiss();
            }

            @Override
            public void onNegativeButton(DialogInterface dialog, int which) {

                DemandQuoteModel model = new DemandQuoteModel();
                model.setImpleId(data.getUserId());
                if (Utils.hasNetwork(getContext())){
                    pos = position;
                    showProgress("Please wait...");
                    if (isFilterApply) {
                        model.setImpleSite(siteFiltervalue);
                        model.setPriceRange(priceFilterValue*1000);
                        model.setSiteDist(distanceFilterVal);
                        model.sortedValue = Integer.parseInt(securePrefManager.getSharedValue(SORTING_VALUE));
                        model.empanel=isEmpaneledImplementer;
//                        model.userId=securePrefManager.getSharedValue(USER_ID);;


                        presenter.getQuote(getContext(), securePrefManager.getSharedValue(TOKEN), model, securePrefManager.getSharedValue(OPTYID));
                    } else {
                        model.setImpleSite(0);
                        model.setPriceRange(70000);
                        model.setSiteDist(100);
                        model.sortedValue = Integer.parseInt(securePrefManager.getSharedValue(SORTING_VALUE));
                        model.empanel=isEmpaneledImplementer;
//                        model.userId=securePrefManager.getSharedValue(USER_ID);;
                        presenter.getQuote(getContext(), securePrefManager.getSharedValue(TOKEN), model, securePrefManager.getSharedValue(OPTYID));

                    }
                }
                else {
                    showToast("Please check Internet");

                }


            }
        }, "Demand Quote");




    };

}
