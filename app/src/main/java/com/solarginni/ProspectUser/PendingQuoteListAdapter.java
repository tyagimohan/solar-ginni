package com.solarginni.ProspectUser;

import android.app.Activity;
import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.DBModel.QuoteListModel;
import com.solarginni.R;

public class PendingQuoteListAdapter extends BaseRecyclerAdapter<QuoteListModel.QuoteList, PendingQuoteListAdapter.Holder> {

    private boolean isForSg;

    public PendingQuoteListAdapter(Context mContext, boolean isForSG) {
        super(mContext);
        this.isForSg = isForSG;
    }

    @Override
    public int getLayout(int viewType) {
        return R.layout.pending_quotation_item;
    }

    @Override
    public Holder getViewHolder(View view, int viewType) {
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        boolean isShowComparison = false;


        holder.siteType.setText(getItem(i).getLovModel().getSiteType());

        holder.siteSize.setText(getItem(i).getSolarSize() + " Kw");

        if (getItem(i).getDemandQuotes().size() > 0) {
            for (int j = 0; j < getItem(i).getDemandQuotes().size(); j++) {
                if (getItem(i).getDemandQuotes().get(j).getStatus().contains("Respond") || getItem(i).getDemandQuotes().get(j).getStatus().contains("Accepted") ||
                        getItem(i).getDemandQuotes().get(j).getStatus().contains("Rejected") || getItem(i).getDemandQuotes().get(j).getStatus().contains("Booked")) {
                    isShowComparison = true;
                    break;
                }
            }
        } else {

        }

        if (getItem(i).getDemandQuotes().size() > 1 && isShowComparison) {
            holder.compareProposals.setVisibility(View.VISIBLE);
           // holder.feasibilityDiv.setVisibility(View.VISIBLE);
            holder.compareProposals.setOnClickListener(v -> getLongClickListener().onItemClick(getItem(i), i, holder.compareProposals));

        } else {
            holder.compareProposals.setVisibility(View.GONE);
          //  holder.feasibilityDiv.setVisibility(View.GONE);

        }
        //   holder.compareProposals.setOnClickListener(v-> getLongClickListener().onItemClick(getItem(i),i,holder.compareProposals));


        if (getItem(i).feasibilityCreated) {

            holder.feasibilityLay.setText("SHOW FEASIBILITY");
            holder.feasibilityLay.setOnClickListener(v -> getLongClickListener().onItemClick(getItem(i), i, holder.feasibilityLay));
        } else {

            holder.feasibilityLay.setVisibility(View.VISIBLE);
            holder.feasibilityLay.setText("ADD FEASIBILITY");
            holder.feasibilityLay.setOnClickListener(v -> {
                getLongClickListener().onItemClick(getItem(i), i, holder.feasibilityLay);
            });
        }

        if (isForSg) {
            holder.userId.setText(" | "+getItem(i).getUserId());
            holder.userId.setVisibility(View.VISIBLE);
            if (getItem(i).getDemandQuotes().size() == 0) {
                holder.recallQuote.setVisibility(View.VISIBLE);
            } else {
                holder.recallQuote.setVisibility(View.GONE);
            }
            holder.recallQuote.setOnClickListener(v -> {
                getLongClickListener().onItemClick(getItem(i), i, holder.recallQuote);
            });
            holder.userId.setOnClickListener(v -> {
                getLongClickListener().onItemClick(getItem(i), i, holder.userId);
            });

        } else {

        }


        LayoutInflater inflater = ((Activity) getContext()).getLayoutInflater();
        holder.parentLay.removeAllViews();

        for (int j = 0; j < 3; j++) {

            View impView = inflater.inflate(R.layout.implementer_list_item, null);
            TextView implementerName = impView.findViewById(R.id.implementerName);
            TextView quotationId = impView.findViewById(R.id.quotation_id);
            TextView quoteStatus = impView.findViewById(R.id.quoteStatus);
            ImageView demandQuote = impView.findViewById(R.id.demandQuote);
            LinearLayout demandedQuote = impView.findViewById(R.id.demandedQuote);
            ImageView status_bulb = impView.findViewById(R.id.status_bulb);
            int finalJ = j;

            try {
                demandedQuote.setVisibility(View.VISIBLE);
                demandQuote.setVisibility(View.GONE);
                SpannableString content = new SpannableString(getItem(i).getDemandQuotes().get(j).getCompanyName());
                content.setSpan(new UnderlineSpan(), 0, getItem(i).getDemandQuotes().get(j).getCompanyName().length(), 0);
                implementerName.setText(content);
                implementerName.setOnClickListener(view -> getIndexedClick().onIndexedClick(getItem(i), i, implementerName, finalJ));

                SpannableString quoteId = new SpannableString(getItem(i).getDemandQuotes().get(j).getQuoteId());
                quoteId.setSpan(new UnderlineSpan(), 0, getItem(i).getDemandQuotes().get(j).getQuoteId().length(), 0);
                quotationId.setText(quoteId);

                quotationId.setOnClickListener(view -> getIndexedClick().onIndexedClick(getItem(i), i, quotationId, finalJ));


                quoteStatus.setText(getItem(i).getDemandQuotes().get(j).getStatus());


                if (getItem(i).getDemandQuotes().get(j).getStatus().equalsIgnoreCase("Requested") || getItem(i).getDemandQuotes().get(j).getStatus().equalsIgnoreCase("Working")|getItem(i).getDemandQuotes().get(j).getStatus().equalsIgnoreCase("Revise"))
                    status_bulb.setBackgroundResource(R.drawable.ic_light_bulb);
                else if (getItem(i).getDemandQuotes().get(j).getStatus().contains("Respond"))
                    status_bulb.setBackgroundResource(R.drawable.ic_green_blub);
                else if (getItem(i).getDemandQuotes().get(j).getStatus().equalsIgnoreCase("Rejected") || getItem(i).getDemandQuotes().get(j).getStatus().equalsIgnoreCase("Declined") || getItem(i).getDemandQuotes().get(j).getStatus().contains("Cancel"))
                    status_bulb.setBackgroundResource(R.drawable.ic_red_blub);
                else if (getItem(i).getDemandQuotes().get(j).getStatus().equalsIgnoreCase("Accepted"))
                    status_bulb.setBackgroundResource(R.drawable.ic_blue_blub);
                else if (getItem(i).getDemandQuotes().get(j).getStatus().contains("Book"))
                    status_bulb.setBackgroundResource(R.drawable.ic_orange_blub);

            } catch (IndexOutOfBoundsException e) {
                demandedQuote.setVisibility(View.GONE);
                demandQuote.setOnClickListener(v -> getIndexedClick().onIndexedClick(getItem(i), i, demandQuote, finalJ));
                demandQuote.setVisibility(View.VISIBLE);
            }


            holder.parentLay.addView(impView);
        }
        if (getItem(i).getDemandQuotes().size() != 0 | isForSg) {
            holder.editQuote.setVisibility(View.GONE);
//            SpannableString siteSize = new SpannableString(holder.siteSize.getText());
//            siteSize.setSpan(new UnderlineSpan(), 0, holder.siteSize.getText().length(), 0);
//            holder.siteSize.setText(siteSize);
//
//            SpannableString siteType = new SpannableString(getItem(i).getLovModel().getSiteType());
//            siteType.setSpan(new UnderlineSpan(), 0, getItem(i).getLovModel().getSiteType().length(), 0);
//            holder.siteType.setText(siteType);


        }
//        else {
//
//        }
        holder.editQuote.setOnClickListener(view -> getListener().onClick(getItem(i), i));


    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView siteType, siteSize, feasibilityLay, compareProposals, userId;
        ImageView editQuote, recallQuote;
        LinearLayout parentLay;
        View feasibilityDiv;

        public Holder(@NonNull View itemView) {
            super(itemView);
            siteType = itemView.findViewById(R.id.siteType);

            siteSize = itemView.findViewById(R.id.siteSize);
            userId = itemView.findViewById(R.id.userId);
            feasibilityDiv = itemView.findViewById(R.id.feasibilityDiv);
            compareProposals = itemView.findViewById(R.id.compareProposals);
            editQuote = itemView.findViewById(R.id.edit_quote);
            parentLay = itemView.findViewById(R.id.parentLay);
            recallQuote = itemView.findViewById(R.id.recallQuote);
            feasibilityLay = itemView.findViewById(R.id.feasibility);
            //  implementerName= itemView.findViewById(R.id.implementerName);
            //   itemView.setOnClickListener(this);
        }


    }

}
