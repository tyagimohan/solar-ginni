package com.solarginni.ProspectUser;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.solarginni.Base.BaseFragment;
import com.solarginni.Base.BaseRecyclerAdapter;
import com.solarginni.CommonModule.MainActivity;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.ComparisonViewModel;
import com.solarginni.DBModel.QuoteListModel;
import com.solarginni.Implementer.ImplementerDetails.ImplemeterDetailsActivity;
import com.solarginni.R;
import com.solarginni.Utility.AppConstants;
import com.solarginni.Utility.Utils;
import com.solarginni.user.addFeasibility.AddFeasibililty;
import com.solarginni.user.addFeasibility.ShowFeasibility;

import static com.solarginni.Utility.AppConstants.OPTYID;
import static com.solarginni.Utility.AppConstants.PROSPECT_ROLE_ID;
import static com.solarginni.Utility.AppConstants.TOKEN;

public class CustomerQuotationListFragment extends BaseFragment implements FetchQuoteContract.View {

    PendingQuoteListAdapter.OnClick<QuoteListModel.QuoteList> listner = (data, position) -> {


        if (data.getDemandQuotes().size() < 3) {
            Bundle bundle = new Bundle();
            bundle.putSerializable("data", data);
//        if (data.getDemandQuotes().size() !=0) {
//
//            goToNextScreen(QuoteDetailsActivity.class, bundle);
//        } else {
            bundle.putBoolean("isFrom", true);
            Intent intent = new Intent(getActivity(), GetQuotationActivity.class);
            intent.putExtras(bundle);
            startActivityForResult(intent, 3);
            securePrefManager.storeSharedValue(OPTYID, data.getOptyId());
        }


        // }


    };
    private FetchQuotePresenter presenter;
    private RecyclerView recyclerView;
    private TextView emptyView;
    private LinearLayout upperLayout;


    @Override
    public int getLayout() {
        return R.layout.quotation_list;
    }

    @Override
    public void initViews(View view) {
        recyclerView = view.findViewById(R.id.recyclerView);
        emptyView = view.findViewById(R.id.emptyView);
        upperLayout = view.findViewById(R.id.upperLayout);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new BaseRecyclerAdapter.DividerItemDecoration(getContext(), RecyclerView.HORIZONTAL));
        presenter = new FetchQuotePresenter(this);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 3:
                if (resultCode == Activity.RESULT_OK) {
                    presenter.getPendingQuote(securePrefManager.getSharedValue(TOKEN), AppConstants.PROSPECT_ROLE_ID, "", "",0);
                } else {

                }
                break;
        }
    }

    @Override
    public void setUp() {


    }

    @Override
    public void onFetchQuote(CommomResponse response) {
        hideProgress();

        QuoteListModel model = response.toResponseModel(QuoteListModel.class);

        PendingQuoteListAdapter adapter = new PendingQuoteListAdapter(getContext(), false);

        if (model.getDetails().size() == 0) {
            upperLayout.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        } else {
            upperLayout.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            adapter.addAll(model.getDetails());

            adapter.setOnClickListener(listner);
            adapter.setOnLongClickListener(longListenr);
            adapter.setIndexClikListener(onIndexedListener);
            recyclerView.setAdapter(adapter);
        }


    }

    @Override
    public void onGetRecall(CommomResponse response) {
        hideProgress();

    }

    @Override
    public void onGetComparision(CommomResponse response) {
        hideProgress();
        ComparisonViewModel model = response.toResponseModel(ComparisonViewModel.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("data",model);
        goToNextScreen(ComparisonActivity.class, bundle);



    }


    private PendingQuoteListAdapter.onItemClick<QuoteListModel.QuoteList> longListenr = (data, position, v) -> {
        Bundle bundle = new Bundle();
        bundle.putString("optId", data.getOptyId());
        switch (v.getId()) {
            case R.id.feasibility:
                if (data.feasibilityCreated) {
                    goToNextScreen(ShowFeasibility.class, bundle);

                } else {
                    goToNextScreen(AddFeasibililty.class, bundle);

                }
                break;
            case R.id.compareProposals:
                if (Utils.hasNetwork(getContext())) {
                    showProgress("Please wait...");
                    presenter.getComparision(securePrefManager.getSharedValue(TOKEN), data.getOptyId());

                } else {
                    showToast("Please check Internet");
                }
                break;
            case R.id.recallQuote:
                if (Utils.hasNetwork(getContext())) {
                    showProgress("Please wait...");
                    presenter.recallQuote(securePrefManager.getSharedValue(TOKEN), data.getOptyId());

                } else {
                    showToast("Please check Internet");
                }
                break;

        }


    };

    private PendingQuoteListAdapter.onIndexedClick<QuoteListModel.QuoteList> onIndexedListener = (data, position, view, index) -> {

        switch (view.getId()) {
            case R.id.implementerName:
                if (data.getDemandQuotes().get(0).getStatus().equalsIgnoreCase("Accepted")) {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("isFormasking", false);
                    bundle.putString("id", data.getDemandQuotes().get(index).getImpleId());
                    goToNextScreen(ImplemeterDetailsActivity.class, bundle);
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("isFormasking", true);
                    bundle.putString("id", data.getDemandQuotes().get(index).getImpleId());
                    goToNextScreen(ImplemeterDetailsActivity.class, bundle);
                }
                break;
            case R.id.quotation_id:
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", data);
                bundle.putInt("index", index);
                goToNextScreen(QuoteDetailActivity.class, bundle);
                break;
            case R.id.demandQuote:
                bundle = new Bundle();
                bundle.putSerializable("data", data);
                bundle.putBoolean("isFrom", true);
                Intent intent = new Intent(getActivity(), GetQuotationActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, 3);
                securePrefManager.storeSharedValue(OPTYID, data.getOptyId());
                break;
        }


    };

    @Override
    public void onFailure(String msg) {
        hideProgress();
        if (msg.equalsIgnoreCase("invalid token")) {
            securePrefManager.clearAppsAllPrefs();
            startActivity(new Intent(getActivity(), MainActivity.class));

        } else
            showToast(msg);

    }

    @Override
    public void onResume() {
        super.onResume();
        if (Utils.hasNetwork(getContext())) {
            showProgress("Please wait...");
            presenter.getPendingQuote(securePrefManager.getSharedValue(TOKEN), PROSPECT_ROLE_ID, "", "",0);
        } else {
            showToast("Please check Internet");

        }

    }
}
