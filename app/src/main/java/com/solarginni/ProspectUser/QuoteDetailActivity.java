package com.solarginni.ProspectUser;

import android.view.View;
import android.widget.Button;

import com.solarginni.Base.BaseActivity;
import com.solarginni.Base.ResponseResolver;
import com.solarginni.ComponentModel;
import com.solarginni.QuoteDetailModule.QuoteDetailFragment3;
import com.solarginni.QuoteDetailModule.QuoteDetailsFragment1;
import com.solarginni.R;
import com.solarginni.Utility.AppConstants;
import com.solarginni.Utility.Utils;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;
import com.solarginni.network.RestClient;

import retrofit2.Retrofit;

import static com.solarginni.Utility.AppConstants.BO_MANAGER;
import static com.solarginni.Utility.AppConstants.CUSTOMER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.IMPLEMENTER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.PROSPECT_ROLE_ID;
import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.SG_ADMIN;
import static com.solarginni.Utility.AppConstants.SUPER_IMPLEMENTER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.TOKEN;

public class QuoteDetailActivity extends BaseActivity implements View.OnClickListener {

    private Button quoteRequest, implementerResponse;
    private Retrofit retrofit;
    private ApiInterface apiInterface;
    private boolean isHitApi;


    @Override
    public int getLayout() {
        return R.layout.quote_details_activity;
    }

    @Override
    public void initViews() {

        quoteRequest = findViewById(R.id.myRequest);
        implementerResponse = findViewById(R.id.implementerResponse);

        quoteRequest.setOnClickListener(this);
        implementerResponse.setOnClickListener(this);
        retrofit = RestClient.getRetrofitBuilder();
        apiInterface = retrofit.create(ApiInterface.class);

        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });

    }

    @Override
    public void setUp() {

        loadFragment(new QuoteDetailsFragment1(), getIntent().getExtras(), QuoteDetailsFragment1.class.getSimpleName());

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.myRequest:
                quoteRequest.setBackground(getDrawable(R.drawable.blue_button));
                implementerResponse.setTextColor(getResources().getColor(R.color.colortheme));
                quoteRequest.setTextColor(getResources().getColor(R.color.white));
                implementerResponse.setBackground(null);
                loadFragment(new QuoteDetailsFragment1(), getIntent().getExtras(), QuoteDetailsFragment1.class.getSimpleName());
                break;

            case R.id.implementerResponse:

                implementerResponse.setBackground(getDrawable(R.drawable.blue_button));
                quoteRequest.setTextColor(getResources().getColor(R.color.colortheme));
                implementerResponse.setTextColor(getResources().getColor(R.color.white));
                quoteRequest.setBackground(null);

                if ((securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SUPER_IMPLEMENTER_ROLE_ID) | securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(IMPLEMENTER_ROLE_ID))) {
                    showProgress("Please wait...");
                    apiInterface.getComponentMake(securePrefManager.getSharedValue(TOKEN)).enqueue(new ResponseResolver<ComponentModel>(retrofit) {
                        @Override
                        public void onSuccess(ComponentModel commomResponse) {
                            hideProgress();

                            securePrefManager.storeSharedValue(AppConstants.COMPONENT_MODEL, Utils.toString(commomResponse));
                            loadFragment(new QuoteDetailFragment3(), getIntent().getExtras(), QuoteDetailFragment3.class.getSimpleName());

                        }


                        @Override
                        public void onError(ApiError error) {
                            hideProgress();

                            showToast(error.getMessage());
                        }

                        @Override
                        public void onFailure(Throwable throwable) {
                            hideProgress();
                            showToast(throwable.getMessage());

                        }
                    });
                }


                if ((securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(SG_ADMIN) | securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(PROSPECT_ROLE_ID) | securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(CUSTOMER_ROLE_ID) | securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(BO_MANAGER))) {
                    loadFragment(new QuoteDetailFragment3(), getIntent().getExtras(), QuoteDetailFragment3.class.getSimpleName());

                }

                break;

        }

    }
}
