package com.solarginni.ProspectUser;

import com.solarginni.DBModel.CommomResponse;

public interface FetchQuoteContract {

    interface View {

        void onFetchQuote(CommomResponse response);

        void onGetRecall(CommomResponse response);
        void onGetComparision(CommomResponse response);
        void onFailure(String msg);

    }

    interface Presenter {
        void getPendingQuote(String token,String role,String siteType,String status,int createdBy);
        void recallQuote(String token,String optId);
        void getComparision(String token,String optId);

    }

    interface Interactor {
        void getPendingQuote(String token,String role,String siteType,String status,int createdBy, OnInteractor listenr);
        void recallQuote(String token,String optId, OnInteractor listenr);
        void getComparision(String token,String optId, OnInteractor listenr);


    }

    interface OnInteractor {

        void onFetchQuote(CommomResponse response);
        void onGetRecall(CommomResponse response);
        void onGetComparision(CommomResponse response);
        void onFailure(String msg);
    }

}
