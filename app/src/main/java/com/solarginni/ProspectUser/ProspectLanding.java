package com.solarginni.ProspectUser;

import android.animation.ObjectAnimator;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.github.aakira.expandablelayout.ExpandableLayoutListenerAdapter;
import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.makeramen.roundedimageview.RoundedImageView;
import com.solarginni.Base.BaseFragment;
import com.solarginni.CommonModule.MainActivity;
import com.solarginni.CommonModule.Testimonial.StatePagerAdapter;
import com.solarginni.DBModel.ProspectLandingModel;
import com.solarginni.Implementer.ImplementerDetails.ImplemeterDetailsActivity;
import com.solarginni.R;
import com.solarginni.Utility.AppDialogs;
import com.solarginni.Utility.Utils;
import com.solarginni.Utility.VideoPlayerActivity;
import com.solarginni.user.SiteNearMe.SiteNearMeActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.solarginni.Utility.AppConstants.CONTACTEMAIL;
import static com.solarginni.Utility.AppConstants.CONTACTNUMBER;
import static com.solarginni.Utility.AppConstants.GUEST_ROLE;
import static com.solarginni.Utility.AppConstants.OPTYID;
import static com.solarginni.Utility.AppConstants.PROSPECT_LANDIND;
import static com.solarginni.Utility.AppConstants.ROLE;

public class ProspectLanding extends BaseFragment {

    private TextView cityImplementer, stateImplementer, totalCityCount, totalStateCount, yearlySiteCount;
    private LinearLayout lay8;
    private ViewPager adViewPager;
    private TabLayout adTabIndicator;
    private int adCurrentPage = 0, tmCurentPage = 0;
    private Handler adHandler, tmHandler;
    private AdviewPageAdapter adviewPageAdapter;
    private ViewPager tmViewpager;
    private TabLayout tmIndicator;
    Timer timer, tmTimer;
    final long DELAY_MS = 500;//delay in milliseconds before task is to be executed
    final long PERIOD_MS = 3000;
    private List<String> array;
    private RelativeLayout videoLayout;
    private AppDialogs mAppDialogs;
    public ExpandableLinearLayout expandableLayout,expandableLayout2,expandableLayout3,expandableLayout4,expandableLayout5,expandableLayout6;
    private View arrow,arrow2,arrow3,arrow4,arrow5,arrow6;


    @Override
    public int getLayout() {
        return R.layout.prospect_landing_page;
    }

    @Override
    public void initViews(View view) {
        cityImplementer = view.findViewById(R.id.cityImplementer);
        stateImplementer = view.findViewById(R.id.stateImplementer);
        totalCityCount = view.findViewById(R.id.citySolarSite);

        totalStateCount = view.findViewById(R.id.stateTotalSite);
        yearlySiteCount = view.findViewById(R.id.tvNewYearSiteCount);
        lay8 = view.findViewById(R.id.lay8);

        adViewPager = view.findViewById(R.id.adViewPager);
        tmViewpager = view.findViewById(R.id.tmViewPager);
        adTabIndicator = view.findViewById(R.id.advTabIndicator);
        tmIndicator = view.findViewById(R.id.tmTabIndicator);
        videoLayout = view.findViewById(R.id.videoLayout);
        mAppDialogs = new AppDialogs(getContext());
        arrow = (View) view.findViewById(R.id.arrow);
        arrow2 = (View) view.findViewById(R.id.arrow2);
        arrow3 = (View) view.findViewById(R.id.arrow3);
        arrow4 = (View) view.findViewById(R.id.arrow4);
        arrow5 = (View) view.findViewById(R.id.arrow5);
        arrow6 = (View) view.findViewById(R.id.arrow6);
        expandableLayout = (ExpandableLinearLayout) view.findViewById(R.id.expandableLayout);
        expandableLayout2 = (ExpandableLinearLayout) view.findViewById(R.id.expandableLayout2);
        expandableLayout3 = (ExpandableLinearLayout) view.findViewById(R.id.expandableLayout3);
        expandableLayout4 = (ExpandableLinearLayout) view.findViewById(R.id.expandableLayout4);
        expandableLayout5 = (ExpandableLinearLayout) view.findViewById(R.id.expandableLayout5);
        expandableLayout6 = (ExpandableLinearLayout) view.findViewById(R.id.expandableLayout6);


        array = new ArrayList<>();


        /*For View 1*/
        view.findViewById(R.id.expadLay).setOnClickListener(v -> {
            expandableLayout.toggle();
        });

        expandableLayout.setListener(new ExpandableLayoutListenerAdapter() {
            @Override
            public void onPreOpen() {
                createRotateAnimator(arrow, 0f, 180f).start();
            }

            @Override
            public void onPreClose() {
                createRotateAnimator(arrow, 180f, 0f).start();
            }
        });

        /*For View 2*/

        view.findViewById(R.id.expadLay2).setOnClickListener(v -> {
            expandableLayout2.toggle();
        });

        expandableLayout2.setListener(new ExpandableLayoutListenerAdapter() {
            @Override
            public void onPreOpen() {
                createRotateAnimator(arrow2, 0f, 180f).start();
            }

            @Override
            public void onPreClose() {
                createRotateAnimator(arrow2, 180f, 0f).start();
            }
        });

        /*For View 3*/

        view.findViewById(R.id.expadLay3).setOnClickListener(v -> {
            expandableLayout3.toggle();
        });

        expandableLayout3.setListener(new ExpandableLayoutListenerAdapter() {
            @Override
            public void onPreOpen() {
                createRotateAnimator(arrow3, 0f, 180f).start();
            }

            @Override
            public void onPreClose() {
                createRotateAnimator(arrow3, 180f, 0f).start();
            }
        });
        /*For View 4*/

        view.findViewById(R.id.expadLay4).setOnClickListener(v -> {
            expandableLayout4.toggle();
        });

        expandableLayout4.setListener(new ExpandableLayoutListenerAdapter() {
            @Override
            public void onPreOpen() {
                createRotateAnimator(arrow4, 0f, 180f).start();
            }

            @Override
            public void onPreClose() {
                createRotateAnimator(arrow4, 180f, 0f).start();
            }
        });

        /*For View 5*/

        view.findViewById(R.id.expadLay5).setOnClickListener(v -> {
            expandableLayout5.toggle();
        });

        expandableLayout5.setListener(new ExpandableLayoutListenerAdapter() {
            @Override
            public void onPreOpen() {
                createRotateAnimator(arrow5, 0f, 180f).start();
            }

            @Override
            public void onPreClose() {
                createRotateAnimator(arrow5, 180f, 0f).start();
            }
        });

        /*For View6*/

        view.findViewById(R.id.expadLay6).setOnClickListener(v -> {
            expandableLayout6.toggle();
        });

        expandableLayout6.setListener(new ExpandableLayoutListenerAdapter() {
            @Override
            public void onPreOpen() {
                createRotateAnimator(arrow6, 0f, 180f).start();
            }

            @Override
            public void onPreClose() {
                createRotateAnimator(arrow6, 180f, 0f).start();
            }
        });

        view.findViewById(R.id.lay7).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(openLink("https://solarginie.com/evaluator"));
            }
        });


        view.findViewById(R.id.needEvalator).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(openLink("https://solarginie.com/evaluator"));
            }
        });

        view.findViewById(R.id.EmailUs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                    emailIntent.setType("text/html");
                    emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{CONTACTEMAIL});
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
                    emailIntent.putExtra(Intent.EXTRA_TEXT, "");

                    ResolveInfo best = null;
                    try {
                        final PackageManager pm = getContext().getPackageManager();
                        final List<ResolveInfo> matches = pm.queryIntentActivities(emailIntent, 0);
                        for (final ResolveInfo info : matches) {
                            if (info.activityInfo.packageName.endsWith(".gm") || info.activityInfo.name.toLowerCase().contains("gmail")) {
                                best = info;
                                break;
                            }
                        }
                    } catch (Exception e) {
                    }

                    if (best != null) {
                        emailIntent.setClassName(best.activityInfo.packageName, best.activityInfo.name);
                        getContext().startActivity(emailIntent);
                    } else {
                        emailIntent.setType("message/rfc822");
                        getContext().startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                    }
                } catch (Exception e) {
                }
            }
        });
        view.findViewById(R.id.callUs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + CONTACTNUMBER));
                startActivity(intent);
            }
        });

        videoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), VideoPlayerActivity.class);
                intent.putExtra("URL", getResources().getString(R.string.video_url));
                startActivity(intent);
            }
        });

//        if(((HorizontalScrollView)getView().findViewById(R.id.horizontalScrollview)).getChildCount()==1){
//            getView().findViewById(R.id.informationDashboard).setVisibility(View.GONE);
//        }
//        else {
//            getView().findViewById(R.id.informationDashboard).setVisibility(View.VISIBLE);
//        }


    }

    @Override
    public void setUp() {

        ProspectLandingModel model = Utils.getObj(securePrefManager.getSharedValue(PROSPECT_LANDIND), ProspectLandingModel.class);
        cityImplementer.setText(model.getDetails().getTopImplCity());
        stateImplementer.setText(model.getDetails().getTopImplementerState());
        yearlySiteCount.setText(String.valueOf(model.getDetails().getTotalSiteYear()));


        if (model.getDetails().getTotalSiteCity() == 0) {
            getView().findViewById(R.id.cityLay).setVisibility(View.GONE);
        }
        totalCityCount.setText(String.valueOf(model.getDetails().getTotalSiteCity()));


        if (model.getDetails().getTotalSiteState() == 0) {
            getView().findViewById(R.id.statelay).setVisibility(View.GONE);
        }


        totalStateCount.setText(String.valueOf(model.getDetails().getTotalSiteState()));
        array.addAll(model.getDetails().advertisementList);

        HandlerViewPager(model);
        tmViewpager.setVisibility(View.VISIBLE);

        if (!cityImplementer.getText().toString().equalsIgnoreCase("No Implementer Found")) {
            cityImplementer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putString("id", model.getDetails().getTopImplCityUserId());
                    bundle.putBoolean("isFormasking", true);
                    goToNextScreen(ImplemeterDetailsActivity.class, bundle);

                }
            });
        } else {
            getView().findViewById(R.id.lay1).setVisibility(View.GONE);
        }

        if (!stateImplementer.getText().toString().equalsIgnoreCase("No Implementer Found")) {
            stateImplementer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putBoolean("isFormasking", true);
                    bundle.putString("id", model.getDetails().getTopImplementerStateUserId());
                    goToNextScreen(ImplemeterDetailsActivity.class, bundle);

                }
            });
        } else {
            getView().findViewById(R.id.lay2).setVisibility(View.GONE);
        }
        lay8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(GUEST_ROLE)) {
                    mAppDialogs.showInfoDialogBothButtons("Cancel", "Registered User can demand free Quotation.\nTake me to registration Page", new AppDialogs.DialogInfo() {
                        @Override
                        public void onPositiveButton(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }

                        @Override
                        public void onNegativeButton(DialogInterface dialog, int which) {
                            Bundle bundle = new Bundle();
                            bundle.putBoolean("isForGuest", true);
                            Intent intent = new Intent(getActivity(), MainActivity.class);
                            intent.putExtras(bundle);
                            startActivity(intent);
                        }
                    }, "Yes");
                } else {
                    securePrefManager.storeSharedValue(OPTYID, "-1");
                    goToNextScreen(GetQuotationActivity.class, null);
                }


            }
        });
        getView().findViewById(R.id.nearSite).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToNextScreen(SiteNearMeActivity.class, null);
            }
        });
        getView().findViewById(R.id.siteLabel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToNextScreen(SiteNearMeActivity.class, null);
            }
        });
    }


    private class AdviewPageAdapter extends PagerAdapter {

        private List<String> adList = new ArrayList<>();


        public AdviewPageAdapter(List<String> adList) {
            this.adList = adList;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {

            View view = LayoutInflater.from(getContext()).inflate(R.layout.advertise_layout, container, false);
            RoundedImageView imageView = view.findViewById(R.id.advertisementImage);
            Glide.with(getContext()).load(adList.get(position)).into(imageView);

//            ImageLoader.getInstance().displayImage(adList.get(position), imageView);

            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return adList.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
            return view == o;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {

        }
    }

    private void HandlerViewPager(ProspectLandingModel details) {

        adviewPageAdapter = new AdviewPageAdapter(array);
        adViewPager.setAdapter(adviewPageAdapter);
        adTabIndicator.setupWithViewPager(adViewPager, true);

        adHandler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (adCurrentPage == array.size()) {
                    adCurrentPage = 0;
                }
                adViewPager.setCurrentItem(adCurrentPage++, true);
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer.schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                adHandler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);

        adViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                adCurrentPage = i;
                adViewPager.setCurrentItem(adCurrentPage++, true);

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });


        StatePagerAdapter adapter = new StatePagerAdapter(getChildFragmentManager(), details.getDetails().getTestimonialModelList());

        tmViewpager.setAdapter(adapter);


        tmIndicator.setupWithViewPager(tmViewpager, true);
        if (details.getDetails().getTestimonialModelList().size() == 0) {
            getView().findViewById(R.id.tmLayout).setVisibility(View.GONE);
        } else {
            getView().findViewById(R.id.tmLayout).setVisibility(View.VISIBLE);


        }

        tmHandler = new Handler();
        final Runnable UpdateTm = new Runnable() {
            public void run() {
                if (tmCurentPage == details.getDetails().getTestimonialModelList().size()) {
                    tmCurentPage = 0;
                }
                tmViewpager.setCurrentItem(tmCurentPage++, true);
            }
        };

        tmTimer = new Timer(); // This will create a new Thread
        tmTimer.schedule(new TimerTask() { // task to be scheduled
            @Override
            public void run() {
                tmHandler.post(UpdateTm);
            }
        }, DELAY_MS, PERIOD_MS);

        tmViewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                tmCurentPage = i;
                tmViewpager.setCurrentItem(tmCurentPage++, true);

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

    }

    public ObjectAnimator createRotateAnimator(final View target, final float from, final float to) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(target, "rotation", from, to);
        animator.setDuration(300);
        animator.setInterpolator(com.github.aakira.expandablelayout.Utils.createInterpolator(com.github.aakira.expandablelayout.Utils.LINEAR_INTERPOLATOR));
        return animator;
    }
}
