package com.solarginni.ProspectUser;

import android.content.Context;

import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.DemandQuoteModel;
import com.solarginni.DBModel.QuotationModel;

import java.util.List;

public interface GetQuotationContract {
    interface View {
        void onApplyFilter(List<QutationModel.Filter> list);

        void onGetQuote();

        void failure(String msg);
    }

    interface Presenter {
        void getQuote(Context context,String token, DemandQuoteModel model,String optId);

        void sortListByValue(Context context, String value);

        void applyFilter(Context context,String token,String price,String distance,String siteCount,String optId,boolean isEmpaneled,int sorting);


    }

    interface Intreactor {
        void getQuote(Context context,String token, DemandQuoteModel model,String optId, OnInteraction interaction);

        void sortListByValue(Context context, String value,OnInteraction interaction);


        void applyFilter(Context context,String token,String price,String distance,String siteCount,String optId,boolean isEmpaneled,int sorting, OnInteraction interaction);

    }

    interface OnInteraction {
        void onGetQuote();

        void failure(String msg);

        void onApplyFilter(List <QutationModel.Filter> list);
    }
}
