package com.solarginni.ProspectUser;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.solarginni.Base.BaseFragment;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.OportunityResponse;
import com.solarginni.MyBounceInterpolator;
import com.solarginni.R;
import com.solarginni.Utility.AppConstants;
import com.solarginni.Utility.Utils;

import java.util.List;

import static com.solarginni.Utility.AppConstants.BO_MANAGER;
import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.SG_ADMIN;
import static com.solarginni.Utility.AppConstants.SORTING_VALUE;
import static com.solarginni.Utility.AppConstants.TOKEN;

public class ProspectFilterFragement extends BaseFragment implements View.OnClickListener,ProspectContract.View {

    private ImageView higherSiteInIndiaTick, higherSiteInStateTick, priceCardTick, capacityCardTick;
    private ProspectPresenter presenter;
    public SendData SD;
    private int sortingValue=0;


    @Override
    public int getLayout() {
        return R.layout.quote_filter_card;
    }

    @Override
    public void initViews(View view) {

        capacityCardTick = view.findViewById(R.id.capacityCardTick);
        higherSiteInStateTick = view.findViewById(R.id.higherSiteInStateTick);
        higherSiteInIndiaTick = view.findViewById(R.id.higherSiteInIndiaTick);
        priceCardTick = view.findViewById(R.id.priceCardTick);

        view.findViewById(R.id.higherSiteInIndiaCard).setOnClickListener(this);
        view.findViewById(R.id.higherSiteInStateCard).setOnClickListener(this);
        view.findViewById(R.id.priceCard).setOnClickListener(this);
        view.findViewById(R.id.capacityCard).setOnClickListener(this);
        view.findViewById(R.id.nextBtn).setOnClickListener(this);
        view.findViewById(R.id.prevBtn).setOnClickListener(this);

        presenter = new ProspectPresenter(this);

    }

    @Override
    public void setUp() {



    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            SD = (SendData) context;
        } catch (ClassCastException e) {
            throw new ClassCastException("Error in retrieving data. Please try again");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.priceCard:
                capacityCardTick.setVisibility(View.GONE);
                higherSiteInIndiaTick.setVisibility(View.GONE);
                higherSiteInStateTick.setVisibility(View.GONE);
                priceCardTick.setVisibility(View.VISIBLE);
                sortingValue=2;
                break;
            case R.id.higherSiteInIndiaCard:
                sortingValue=0;
                capacityCardTick.setVisibility(View.GONE);
                higherSiteInIndiaTick.setVisibility(View.VISIBLE);
                higherSiteInStateTick.setVisibility(View.GONE);
                priceCardTick.setVisibility(View.GONE);
                break;
            case R.id.higherSiteInStateCard:
                capacityCardTick.setVisibility(View.GONE);
                higherSiteInIndiaTick.setVisibility(View.GONE);
                higherSiteInStateTick.setVisibility(View.VISIBLE);
                priceCardTick.setVisibility(View.GONE);
                sortingValue=1;

                break;
            case R.id.capacityCard:
                capacityCardTick.setVisibility(View.VISIBLE);
                higherSiteInIndiaTick.setVisibility(View.GONE);
                higherSiteInStateTick.setVisibility(View.GONE);
                priceCardTick.setVisibility(View.GONE);
                sortingValue=3;
                break;
            case R.id.prevBtn:
                GetQuotationActivity.viewPager.setCurrentItem(GetQuotationActivity.viewPager.getCurrentItem()-1);
                break;
            case R.id.nextBtn:
                    if (Utils.hasNetwork(getContext())){
                        showProgress("Please wait...");
                        securePrefManager.storeSharedValue(SORTING_VALUE,String.valueOf(sortingValue));
                        presenter.filterQuote(securePrefManager.getSharedValue(TOKEN), "100", "70000", "0", securePrefManager.getSharedValue(AppConstants.OPTYID), sortingValue);

                    }
                    else
                        showToast("Please check Internet");

                break;
        }


    }
    @Override
    public void onDetach() {
        super.onDetach();
        SD = null;
    }

    @Override
    public void onFetchAccountInfo(CommomResponse response) {

    }

    @Override
    public void onFetchLandingDetails(CommomResponse response) {

    }

    @Override
    public void onSaveFirstQuotation(OportunityResponse response) {

    }
    public interface SendData {
        void sendData(List<QutationModel.Filter> filterList);
    }

    @Override
    public void fetChQuotation(CommomResponse response) {
        hideProgress();
        QutationModel model = response.toResponseModel(QutationModel.class);
        SD.sendData(model.getFilter());
        GetQuotationActivity.viewPager.setCurrentItem(GetQuotationActivity.viewPager.getCurrentItem()+1);

    }

    @Override
    public void fetchPendingQuote(CommomResponse response) {

    }

    @Override
    public void onGetQuote() {

    }

    @Override
    public void guestUserApi(CommomResponse response) {

    }

    @Override
    public void onFailure(String msg) {

    }
}
