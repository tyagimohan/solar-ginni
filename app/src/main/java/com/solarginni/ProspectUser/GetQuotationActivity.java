package com.solarginni.ProspectUser;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.MotionEvent;
import android.view.View;

import com.solarginni.Backend.DAOs.QuoteImpDao;
import com.solarginni.Base.BaseActivity;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.ManufacturerModel;
import com.solarginni.R;
import com.solarginni.Utility.AppConstants;
import com.solarginni.Utility.Utils;

import java.util.List;

import static com.solarginni.Utility.AppConstants.ANYBATTERY;
import static com.solarginni.Utility.AppConstants.TOKEN;

public class GetQuotationActivity extends BaseActivity implements View.OnClickListener, ProsepectFratgment1.HybridSol, ProspectFilterFragement.SendData {
    public static ProspectViewPager viewPager;
    private ViewPagerAdapter adapter;


    @Override
    public int getLayout() {
        return R.layout.quotation_activity;
    }

    @Override
    public void initViews() {
        viewPager = findViewById(R.id.viewPager);
        adapter = new ViewPagerAdapter(getSupportFragmentManager());

        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });


    }

    @Override
    public void setUp() {

        if (Utils.hasNetwork(this)) {
            GetManuFacturerDataApi api = new GetManuFacturerDataApi() {
                @Override
                public void onComplete(CommomResponse response) {
                    ManufacturerModel manufacturerModel = response.toResponseModel(ManufacturerModel.class);
                    securePrefManager.storeSharedValue(AppConstants.MANUFACTURER_MODEL, Utils.toString(manufacturerModel));

                    viewPager.setAdapter(adapter);
                    viewPager.addOnPageChangeListener(adapter);
                    viewPager.setOffscreenPageLimit(3);
                }

                @Override
                protected void onFailur(String msg) {

                }
            };

            api.hit(securePrefManager.getSharedValue(TOKEN),true,"");
        } else {
            showToast("Please check Internet");
        }




        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });


    }

    @Override
    public void onClick(View view) {


    }

    @Override
    public void selectHybrid(boolean isHybrid) {

        if (!isHybrid) {
            ProspectFragment2.batteryLayout.setVisibility(View.GONE);
        } else {
            ProspectFragment2.batteryLayout.setVisibility(View.VISIBLE);

            ProspectFragment2.batteryType = ANYBATTERY;
        }

    }

    @Override
    public void sendData(List<QutationModel.Filter> filterList) {
        QuoteImpDao.getInstance(this).insertData(filterList);
        ProsepectFragment3.adapter.notifyItemRangeRemoved(0, ProsepectFragment3.adapter.getItemCount());
        ProsepectFragment3.adapter.addAll(filterList);
        ProsepectFragment3.adapter.notifyDataSetChanged();

    }

    public class ViewPagerAdapter extends FragmentStatePagerAdapter implements ViewPager.OnPageChangeListener {

        private Fragment[] childFragments;

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
            childFragments = new Fragment[]{
                    new ProsepectFratgment1(), //0
                    new ProspectFragment2(),
                    new ProspectFilterFragement(),
                    new ProsepectFragment3()
            };
        }


        @Override
        public Fragment getItem(int position) {
            childFragments[position].setArguments(getIntent().getExtras());

            return childFragments[position];
        }

        @Override
        public int getCount() {
            return childFragments.length;
        }

        @Override
        public void onPageScrolled(int i, float v, int i1) {

        }

        @Override
        public void onPageSelected(int i) {


        }

        @Override
        public void onPageScrollStateChanged(int i) {

        }
    }

}
