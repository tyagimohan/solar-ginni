package com.solarginni.ProspectUser;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.solarginni.Base.BaseActivity;
import com.solarginni.CommonModule.FAQs.FAQsActivity;
import com.solarginni.CommonModule.FAQs.FAQsFragment;
import com.solarginni.CommonModule.LogoutApi;
import com.solarginni.CommonModule.MainActivity;
import com.solarginni.CommonModule.Referral.ReferralActivity;
import com.solarginni.CommonModule.Testimonial.AddTestimonialActivity;
import com.solarginni.DBModel.APIBody.GuestModel;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.OportunityResponse;
import com.solarginni.DBModel.ProspectLandingModel;
import com.solarginni.DBModel.QuoteListModel;
import com.solarginni.DBModel.UserRoleModel;
import com.solarginni.NotificationModule.NotificationActivity;
import com.solarginni.R;
import com.solarginni.Utility.AppConstants;
import com.solarginni.Utility.AppDialogs;
import com.solarginni.Utility.Converter;
import com.solarginni.Utility.Utils;
import com.solarginni.user.MyProfileActivity;
import com.solarginni.user.RequestDetailsModule.RequestListFragment;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static com.solarginni.Utility.AppConstants.FCM_TOKEN;
import static com.solarginni.Utility.AppConstants.GUEST_ROLE;
import static com.solarginni.Utility.AppConstants.MOBILE;
import static com.solarginni.Utility.AppConstants.OPTYID;
import static com.solarginni.Utility.AppConstants.PROSPECT_LANDIND;
import static com.solarginni.Utility.AppConstants.ROLE;
import static com.solarginni.Utility.AppConstants.TOKEN;
import static com.solarginni.Utility.AppConstants.dynamicUrl;

public class ProspectHomeActivity extends BaseActivity implements View.OnClickListener, ProspectContract.View, BottomNavigationView.OnNavigationItemSelectedListener {

    private static final int REQUEST_FOR_EDIT = 10;
    private static final int ADD_TESTIMONILA = 2;
    private static final int PERMISSION_REQUEST_CODE_LOCATION = 1;
    private static final String TAG = ProspectHomeActivity.class.getSimpleName();
    private static final int REQUEST_CODE_ACCESS_GPS = 0;
    public static BottomNavigationView bottomNavigationView;
    private TextView tvHome, tvSite, tvRequest, tvImplemeter, tvMore;
    private ImageView ivHome, ivSite, ivRequest, ivImplemeter, ivMore;
    private ImageView myAccount;
    private ProspectPresenter presenter;
    private int notificationCount = 0;
    private FloatingActionButton getQutation, logout;
    private LinearLayout homeLay, siteLay, reqLay, impLay, moreLay;
    private Boolean testimonialCreated = false;
    private int exitCount = 0;
    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;
    private LatLng mLatLng;
    protected AppDialogs mAppDialogs;


    @Override
    public int getLayout() {
        return R.layout.prospect_landing;
    }

    @Override
    public void initViews() {



        /*TextView*/
        tvHome = findViewById(R.id.tvHome);
        tvImplemeter = findViewById(R.id.tvImp);
        tvMore = findViewById(R.id.tvMore);
        tvSite = findViewById(R.id.tvSite);
        tvRequest = findViewById(R.id.tvRequest);


        /*LinearLayout*/
        homeLay = findViewById(R.id.home_lay);
        siteLay = findViewById(R.id.site_lay);
        reqLay = findViewById(R.id.request_lay);
        impLay = findViewById(R.id.imp_lay);
        moreLay = findViewById(R.id.more_lay);
        logout = findViewById(R.id.logout);
        logout.setOnClickListener(this);

        /*ImageView*/
        ivHome = findViewById(R.id.ivHome);
        ivImplemeter = findViewById(R.id.ivImp);
        ivMore = findViewById(R.id.ivMore);
        ivRequest = findViewById(R.id.ivReq);
        ivSite = findViewById(R.id.ivSite);

        bottomNavigationView = findViewById(R.id.nav_bottom);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        myAccount = findViewById(R.id.myAccount);
        presenter = new ProspectPresenter(this);
        androidx.appcompat.widget.Toolbar toolbar = (androidx.appcompat.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getQutation = findViewById(R.id.getQutation);
        getSupportActionBar().setTitle("");

        mAppDialogs = new AppDialogs(this);


        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);


    }


    @Override
    public void onBackPressed() {
        exitCount++;
        if (exitCount > 1)
            super.onBackPressed();
        else {
            Handler handler = new Handler();
            handler.postDelayed(() -> exitCount = 0, 3000);
            Toast.makeText(this, "Press again to exit the application", Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    public void setUp() {


        if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(GUEST_ROLE)) {
            myAccount.setVisibility(View.INVISIBLE);
        } else {
            myAccount.setVisibility(View.VISIBLE);

            ProspectLandingModel model = Utils.getObj(securePrefManager.getSharedValue(PROSPECT_LANDIND), ProspectLandingModel.class);


            notificationCount = model.getDetails().getNotificationCount();
            testimonialCreated = model.getDetails().testimonialCreated;
            invalidateOptionsMenu();


            tvHome.setTextColor(getResources().getColor(R.color.colortheme));
            ivHome.setImageDrawable(getResources().getDrawable(R.drawable.ic_home_white));

            loadFragment(new ProspectLanding(), null, ProspectLanding.class.getSimpleName());

            myAccount.setOnClickListener(this);
            getQutation.setOnClickListener(this);
        }

        if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(GUEST_ROLE))
            checkLocation();
        homeLay.setOnClickListener(this);
        siteLay.setOnClickListener(this);
        reqLay.setOnClickListener(this);
        impLay.setOnClickListener(this);
        moreLay.setOnClickListener(this);


    }

    @Override
    protected void onResume() {


        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.notification_action:
                startActivity(new Intent(ProspectHomeActivity.this, NotificationActivity.class));
                notificationCount = 0;
                invalidateOptionsMenu();
                break;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.myAccount:
                if (Utils.hasNetwork(this)) {
                    showProgress("Please wait..");
                    presenter.getAccountInfo(securePrefManager.getSharedValue(TOKEN));
                } else {
                    showToast("Please check Internet");

                }
                break;
            case R.id.getQutation:
                securePrefManager.storeSharedValue(OPTYID, "-1");
                goToNextScreen(GetQuotationActivity.class, null);
                // showToast("Under Developement");

                break;

            case R.id.logout:
                securePrefManager.clearAppsAllPrefs();
                startActivity(new Intent(ProspectHomeActivity.this, MainActivity.class));
                finish();
                break;

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_FOR_EDIT | requestCode == ADD_TESTIMONILA) {
            bottomNavigationView.setSelectedItemId(R.id.nav_home);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.alert_menu, menu);
        MenuItem menuItem2 = menu.findItem(R.id.notification_action);
        menuItem2.setIcon(Converter.convertLayoutToImage(ProspectHomeActivity.this, notificationCount, R.drawable.ic_bell));
        if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(GUEST_ROLE)) {
            return false;
        }
        return true;
    }

    @Override
    public void onFetchAccountInfo(CommomResponse response) {
        hideProgress();
        UserRoleModel userRoleModel = response.toResponseModel(UserRoleModel.class);
        securePrefManager.storeSharedValue(AppConstants.ACCOUNT_MODEL, Utils.toString(userRoleModel.getMyAccount()));
        Bundle bundle = new Bundle();
        bundle.putString("isFrom", "Landing");
        Intent intent = new Intent(ProspectHomeActivity.this, MyProfileActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, REQUEST_FOR_EDIT);


    }

    @Override
    public void onFetchLandingDetails(CommomResponse response) {
        hideProgress();
        getQutation.hide();
        ProspectLandingModel prospectLandingModel = response.toResponseModel(ProspectLandingModel.class);
        testimonialCreated = prospectLandingModel.getDetails().testimonialCreated;
        securePrefManager.storeSharedValue(AppConstants.PROSPECT_LANDIND, Utils.toString(prospectLandingModel));
        setUp();
        tvHome.setTextColor(getResources().getColor(R.color.colortheme));
        ivHome.setImageDrawable(getResources().getDrawable(R.drawable.ic_home_white));

        loadFragment(new ProspectLanding(), null, ProspectLanding.class.getSimpleName());
    }

    @Override
    public void onSaveFirstQuotation(OportunityResponse response) {

    }

    @Override
    public void fetChQuotation(CommomResponse response) {

    }

    @Override
    public void fetchPendingQuote(CommomResponse response) {
        hideProgress();
        getQutation.hide();
        QuoteListModel model = response.toResponseModel(QuoteListModel.class);
        securePrefManager.storeSharedValue(AppConstants.PENDING_QUOTE, Utils.toString(model));


    }

    @Override
    public void onGetQuote() {

    }

    @Override
    public void guestUserApi(CommomResponse response) {
        hideProgress();
        getQutation.hide();
        ProspectLandingModel prospectLandingModel = response.toResponseModel(ProspectLandingModel.class);
        testimonialCreated = prospectLandingModel.getDetails().testimonialCreated;
        securePrefManager.storeSharedValue(AppConstants.PROSPECT_LANDIND, Utils.toString(prospectLandingModel));

        tvHome.setTextColor(getResources().getColor(R.color.colortheme));
        ivHome.setImageDrawable(getResources().getDrawable(R.drawable.ic_home_white));

        loadFragment(new ProspectLanding(), null, ProspectLanding.class.getSimpleName());
    }


    @Override
    public void onFailure(String msg) {
        hideProgress();
        if (msg.equalsIgnoreCase("invalid token")) {
            securePrefManager.clearAppsAllPrefs();
            startActivity(new Intent(ProspectHomeActivity.this, MainActivity.class));
            finish();
        } else
            showToast(msg);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        switch (menuItem.getItemId()) {
            case R.id.nav_home:
                if (Utils.hasNetwork(this)) {
                    showProgress("Please wait...");
                    presenter.getLandingDetails(securePrefManager.getSharedValue(TOKEN));
                } else {
                    showToast("Check Internet ");
                }
                break;
            case R.id.nav_quote:

                if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(GUEST_ROLE)) {
                    mAppDialogs.showInfoDialogBothButtons("Cancel", "Registered User can demand free Quotation.\nTake me to registration Page", new AppDialogs.DialogInfo() {
                        @Override
                        public void onPositiveButton(DialogInterface dialog, int which) {
                            dialog.dismiss();

                        }

                        @Override
                        public void onNegativeButton(DialogInterface dialog, int which) {
                            Bundle bundle = new Bundle();
                            bundle.putBoolean("isForGuest", true);
                            Intent intent = new Intent(ProspectHomeActivity.this, MainActivity.class);
                            intent.putExtras(bundle);
                            startActivity(intent);
                        }
                    }, "Yes");

                    return  false;
                } else {
                    getQutation.show();
                    loadFragment(new CustomerQuotationListFragment(), null, CustomerQuotationListFragment.class.getSimpleName());
                }


                break;
            case R.id.nav_request:

                if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(GUEST_ROLE)) {
                    mAppDialogs.showInfoDialogBothButtons("Cancel", "Registered User can raise request.\nTake me to registration Page", new AppDialogs.DialogInfo() {
                        @Override
                        public void onPositiveButton(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }

                        @Override
                        public void onNegativeButton(DialogInterface dialog, int which) {
                            Bundle bundle = new Bundle();
                            bundle.putBoolean("isForGuest", true);
                            Intent intent = new Intent(ProspectHomeActivity.this, MainActivity.class);
                            intent.putExtras(bundle);
                            startActivity(intent);
                        }
                    }, "Yes");
                    return false;
                } else {
                    getQutation.hide();
                    loadFragment(new RequestListFragment(), null, RequestListFragment.class.getSimpleName());
                }


                break;
            case R.id.nav_news:
                getQutation.hide();
                loadFragment(new FAQsFragment(), null, FAQsFragment.class.getSimpleName());
                break;
            case R.id.nav_more:
                getQutation.hide();
                showMore(this);
                break;
        }

        return true;
    }


    private void showMore(Context context) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.more_layout, null);
        Animation slideUpIn;
        slideUpIn = AnimationUtils.loadAnimation(this, R.anim.slide_in_bottom);
        dialogView.startAnimation(slideUpIn);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(true);
        AlertDialog alertDialog = dialogBuilder.create();
        RelativeLayout logout, faqs, addTestimonial, share,refer;


        logout = dialogView.findViewById(R.id.logout);
        refer = dialogView.findViewById(R.id.refer);
        share = dialogView.findViewById(R.id.share);
        faqs = dialogView.findViewById(R.id.faqs);
        faqs.setVisibility(View.GONE);
        addTestimonial = dialogView.findViewById(R.id.addTestimonial);

        if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(GUEST_ROLE)) {
            logout.setVisibility(View.GONE);
            addTestimonial.setVisibility(View.GONE);
            refer.setVisibility(View.GONE);
            share.setVisibility(View.VISIBLE);


        }
        else {
            share.setVisibility(View.GONE);

        }
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mobileNumber = securePrefManager.getSharedValue(MOBILE);
                if (mobileNumber.equalsIgnoreCase("")){
                    mobileNumber="+919501232200";
                }

                String content = "Hello Friend, I have used Solar Ginie for my Solar rooftop need and found it very interesting. Recommend you to download Solar Ginie from Android playstore " + dynamicUrl + "\n" + "Use my Promotion code " + mobileNumber.substring(3) + " during Registration to earn Bonus points";

                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/html");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, content);
                if (sharingIntent.resolveActivity(getPackageManager()) != null)
                    startActivity(Intent.createChooser(sharingIntent, "Share Using"));
                else
                    showToast("Your device haven't any app to share this content");

                alertDialog.dismiss();

            }
        });

        if (testimonialCreated) {
            addTestimonial.setVisibility(View.GONE);
        } else {
            addTestimonial.setVisibility(View.VISIBLE);
        }
        if (securePrefManager.getSharedValue(ROLE).equalsIgnoreCase(GUEST_ROLE)) {
            logout.setVisibility(View.GONE);
            addTestimonial.setVisibility(View.GONE);
            refer.setVisibility(View.GONE);
        }

        refer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProspectHomeActivity.this, ReferralActivity.class));
            }
        });

        logout.setOnClickListener(view -> {
            if (Utils.hasNetwork(this)) {
                showProgress("Please wait...");
                LogoutApi api = new LogoutApi() {
                    @Override
                    public void onComplete(CommomResponse response) {
                        hideProgress();
                        securePrefManager.clearAppsAllPrefs();
                        startActivity(new Intent(ProspectHomeActivity.this, MainActivity.class));
                        finish();
                    }

                    @Override
                    protected void onFailur(String msg) {
                        hideProgress();
                        securePrefManager.clearAppsAllPrefs();
                        startActivity(new Intent(ProspectHomeActivity.this, MainActivity.class));
                        finish();

                    }
                };
                api.hit(securePrefManager.getSharedValue(TOKEN));
            } else {
                showToast("Please check Internet");

            }

            alertDialog.dismiss();
        });

        faqs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToNextScreen(FAQsActivity.class, null);
                alertDialog.dismiss();
            }
        });

        addTestimonial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(ProspectHomeActivity.this, AddTestimonialActivity.class), ADD_TESTIMONILA);
                alertDialog.dismiss();
            }
        });


        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.BOTTOM;

        window.setAttributes(wlp);

        alertDialog.show();

    }


    public void checkLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
            ActivityCompat.requestPermissions(this, permissions, PERMISSION_REQUEST_CODE_LOCATION);

            return;
        }
        getLastKnownLocation(true);


    }


    /**
     * build message dialog if gps is not enabled
     */
    private void showNoGpsDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you want to enable your location").setCancelable(false).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(@SuppressWarnings("unused") final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), REQUEST_CODE_ACCESS_GPS);
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(final DialogInterface dialog, @SuppressWarnings("unused") final int id) {
                dialog.cancel();
            }
        });
        final AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * request new location
     */
    @SuppressLint("MissingPermission")
    private void requestLocation() {
        if (mLocationCallback == null) {
            mLocationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(final LocationResult locationResult) {
                    // do work here
                    onLocationChanged(locationResult.getLastLocation());
                }
            };
        }
        // Create the location request to start receiving updates
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(3000);

        // Create LocationSettingsRequest object using location request
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        LocationSettingsRequest locationSettingsRequest = builder.build();

        // Check whether location settings are satisfied
        // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
        SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        settingsClient.checkLocationSettings(locationSettingsRequest);

        // new Google API SDK v11 uses getFusedLocationProviderClient(this)
        mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper());
    }


    private boolean checkIfGPSisEnabled() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return manager != null && manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    private void onLocationChanged(final Location location) {
        mLatLng = new LatLng(location.getLatitude(), location.getLongitude());

        // stop receiving updates if location has been received
        if (mLocationCallback != null) {
            mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        }
        //onParamSelected();
    }

    @SuppressLint("MissingPermission")
    private void getLastKnownLocation(boolean isLocationUpdate) {

        mFusedLocationClient.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(final Location location) {
                // Got last known location. In some rare situations this can be null.
                if (location != null) {

                    try {
                        getCityNameByCoordinates(location.getLatitude(), location.getLongitude());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (isLocationUpdate)
                        onLocationChanged(location);
                    Log.d(TAG, "location accuracy: " + location.getAccuracy());
                    Log.d(TAG, "location : " + location.getLatitude() + "," + location.getLongitude());
                } else {
                    if (checkIfGPSisEnabled()) {
                        requestLocation();
                    } else {
                        showNoGpsDialog();
                    }
                }
            }
        }).addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull final Exception e) {
                requestLocation();
            }
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case PERMISSION_REQUEST_CODE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLastKnownLocation(true);
                }
                else {
                    AppDialogs     mAppDialogs = new AppDialogs(this);

                    mAppDialogs.showInfoDialogBothButtons("OK", "We will show you experience of user in Mohali (Punjab) location", new AppDialogs.DialogInfo() {
                        @Override
                        public void onPositiveButton(DialogInterface dialog, int which) {

                            try {
                                getCityNameByCoordinates(30.70029422305781, 76.69173013697333);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }


                            dialog.dismiss();
                        }

                        @Override
                        public void onNegativeButton(DialogInterface dialog, int which) {

                        }
                    },"");
                }
            }
            break;

        }
    }


    private void getCityNameByCoordinates(double lat, double lon) throws IOException {
        Geocoder mGeocoder = new Geocoder(this, Locale.getDefault());

        List<Address> addresses = mGeocoder.getFromLocation(lat, lon, 1);
        if (addresses != null && addresses.size() > 0) {
            GuestModel model = new GuestModel();
            com.solarginni.SiteLovsModule.SiteDetailModel.Address address = new com.solarginni.SiteLovsModule.SiteDetailModel.Address();

            address.setAddressLine1("");
            address.setAddressLine2("");
            address.setCity(addresses.get(0).getSubAdminArea());
            address.setState(addresses.get(0).getAdminArea());
            address.setCountry(addresses.get(0).getCountryName());
            address.setPinCode(addresses.get(0).getPostalCode());
            address.setLatitude(lat);
            address.setLongitude(lon);

            model.address = address;
            model.deviceId = securePrefManager.getSharedValue(FCM_TOKEN);

            showProgress("Please wait...");
            presenter.guestUserApi(this, getString(R.string.static_token), model);


            Log.e("Location", addresses.get(0).getAdminArea());


        }

    }


}
