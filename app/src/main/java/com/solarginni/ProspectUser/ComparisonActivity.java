package com.solarginni.ProspectUser;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.solarginni.Base.BaseActivity;
import com.solarginni.DBModel.ComparisonViewModel;
import com.solarginni.R;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class ComparisonActivity extends BaseActivity {

    private LinearLayout parentLay1, parentLay2, parentLay3;
    private TextView implementerName1, implementerName2, implementerName3;

    @Override
    public int getLayout() {
        return R.layout.comparision_layout;
    }

    @Override
    public void initViews() {
        parentLay1 = findViewById(R.id.parentLay1);
        parentLay2 = findViewById(R.id.parentLay2);
        parentLay3 = findViewById(R.id.parentLay3);
        implementerName1 = findViewById(R.id.implementerName1);
        implementerName2 = findViewById(R.id.implementerName2);
        implementerName3 = findViewById(R.id.implementerName3);

        findViewById(R.id.back).setOnClickListener(v -> {
            finish();
        });


    }

    @Override
    public void setUp() {

        ComparisonViewModel model = (ComparisonViewModel) getIntent().getExtras().getSerializable("data");





        for (int i = 0; i < model.details.size(); i++) {

            if (i == 0) {
                implementerName1.setText(model.details.get(0).companyName);

                View view = LayoutInflater.from(this).inflate(R.layout.comparison_item, null);
                TextView panelType, inverterSize, recommendSize, priceRange, siteCount, capacity, respondedIn;
                panelType = view.findViewById(R.id.panelType);
                panelType.setText(model.details.get(0).panelType);
               ImageView inverterMake = view.findViewById(R.id.inverterMake);
                ImageLoader.getInstance().displayImage(model.details.get(0).inverterMake, inverterMake);

                ImageView panelMake = view.findViewById(R.id.panelMake);
                ImageLoader.getInstance().displayImage(model.details.get(0).panelMake, panelMake);

                inverterSize = view.findViewById(R.id.inverterSize);
                inverterSize.setText(model.details.get(0).inverterSize);



              TextView  bidirectional = view.findViewById(R.id.bidirectional);
                bidirectional.setText(model.details.get(0).bidirectionalMeter);

                TextView priceKw = view.findViewById(R.id.priceKw);

                int pw = (int) (model.details.get(0).finalPrice/model.details.get(0).recmndSiteSize);

                priceKw.setText(" \u20B9 "+String.valueOf(pw));

                TextView structureType = view.findViewById(R.id.structureType);
                structureType.setText(model.details.get(0).structureType);

                TextView distributionBox = view.findViewById(R.id.distributionBox);
                distributionBox.setText(model.details.get(0).distributionBox);

                TextView labourWarranty = view.findViewById(R.id.labourWarranty);
                labourWarranty.setText(""+model.details.get(0).warrantyTerm);

                recommendSize = view.findViewById(R.id.recommendSize);
                recommendSize.setText(model.details.get(0).recmndSiteSize.toString());
                respondedIn = view.findViewById(R.id.respondedIn);
                respondedIn.setText(model.details.get(0).respondDate.toString());

                priceRange = view.findViewById(R.id.priceRange);
                DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                formatter.applyPattern("#,##,##,###");
                String formattedString = formatter.format(model.details.get(0).finalPrice.longValue());

                //setting text after format to EditText


                priceRange.setText(" \u20B9 "+formattedString);

                siteCount = view.findViewById(R.id.siteCount);
                siteCount.setText(model.details.get(0).sites.toString());

                capacity = view.findViewById(R.id.capacity);
                capacity.setText(model.details.get(0).installedCapacity.toString() + "  ");


                parentLay1.addView(view);
            } else if (i == 1) {
                implementerName2.setText(model.details.get(1).companyName);

                View view = LayoutInflater.from(this).inflate(R.layout.comparison_item, null);
                TextView panelType, inverterSize, recommendSize, priceRange, siteCount, capacity, respondedIn;

                ImageView inverterMake = view.findViewById(R.id.inverterMake);
                ImageLoader.getInstance().displayImage(model.details.get(1).inverterMake, inverterMake);

                ImageView panelMake = view.findViewById(R.id.panelMake);
                ImageLoader.getInstance().displayImage(model.details.get(1).panelMake, panelMake);

                panelType = view.findViewById(R.id.panelType);
                panelType.setText(model.details.get(1).panelType);

                inverterSize = view.findViewById(R.id.inverterSize);
                inverterSize.setText(model.details.get(1).inverterSize);


                TextView priceKw = view.findViewById(R.id.priceKw);

                int pw = (int) (model.details.get(1).finalPrice/model.details.get(1).recmndSiteSize);

                priceKw.setText(" \u20B9 "+String.valueOf(pw));

                TextView  bidirectional = view.findViewById(R.id.bidirectional);
                bidirectional.setText(model.details.get(1).bidirectionalMeter);

                TextView structureType = view.findViewById(R.id.structureType);
                structureType.setText(model.details.get(1).structureType);

                TextView distributionBox = view.findViewById(R.id.distributionBox);
                distributionBox.setText(model.details.get(1).distributionBox);

                TextView labourWarranty = view.findViewById(R.id.labourWarranty);
                labourWarranty.setText(""+model.details.get(1).warrantyTerm);


                recommendSize = view.findViewById(R.id.recommendSize);
                recommendSize.setText(model.details.get(1).recmndSiteSize.toString());
                respondedIn = view.findViewById(R.id.respondedIn);
                respondedIn.setText(model.details.get(1).respondDate.toString());

                priceRange = view.findViewById(R.id.priceRange);

                DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                formatter.applyPattern("#,##,##,###");
                String formattedString = formatter.format(model.details.get(1).finalPrice.longValue());

                //setting text after format to EditText


                priceRange.setText(" \u20B9 "+formattedString);

                siteCount = view.findViewById(R.id.siteCount);
                siteCount.setText(model.details.get(1).sites.toString());

                capacity = view.findViewById(R.id.capacity);
                capacity.setText(model.details.get(1).installedCapacity.toString() + "  ");

                parentLay2.addView(view);
            } else if (i == 2) {
                implementerName3.setText(model.details.get(2).companyName);

                TextView panelType, inverterSize, recommendSize, priceRange, siteCount, capacity, respondedIn;

                View view = LayoutInflater.from(this).inflate(R.layout.comparison_item, null);


                ImageView inverterMake = view.findViewById(R.id.inverterMake);
                ImageLoader.getInstance().displayImage(model.details.get(2).inverterMake, inverterMake);

                ImageView panelMake = view.findViewById(R.id.panelMake);
                ImageLoader.getInstance().displayImage(model.details.get(2).panelMake, panelMake);

                panelType = view.findViewById(R.id.panelType);
                panelType.setText(model.details.get(2).panelType);

                inverterSize = view.findViewById(R.id.inverterSize);
                inverterSize.setText(model.details.get(2).inverterSize);

                TextView priceKw = view.findViewById(R.id.priceKw);

                int pw = (int) (model.details.get(2).finalPrice/model.details.get(2).recmndSiteSize);

                priceKw.setText(" \u20B9 "+String.valueOf(pw));
                TextView  bidirectional = view.findViewById(R.id.bidirectional);
                bidirectional.setText(model.details.get(2).bidirectionalMeter);

                TextView structureType = view.findViewById(R.id.structureType);
                structureType.setText(model.details.get(2).structureType);

                TextView distributionBox = view.findViewById(R.id.distributionBox);
                distributionBox.setText(model.details.get(2).distributionBox);

                TextView labourWarranty = view.findViewById(R.id.labourWarranty);
                labourWarranty.setText(""+model.details.get(2).warrantyTerm);


                recommendSize = view.findViewById(R.id.recommendSize);
                recommendSize.setText(model.details.get(2).recmndSiteSize.toString());
                respondedIn = view.findViewById(R.id.respondedIn);
                respondedIn.setText(model.details.get(2).respondDate.toString());

                priceRange = view.findViewById(R.id.priceRange);
                DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                formatter.applyPattern("#,##,##,###");
                String formattedString = formatter.format(model.details.get(2).finalPrice.longValue());

                //setting text after format to EditText


                priceRange.setText(" \u20B9 "+formattedString);

                siteCount = view.findViewById(R.id.siteCount);
                siteCount.setText(model.details.get(2).sites.toString());

                capacity = view.findViewById(R.id.capacity);
                capacity.setText(model.details.get(2).installedCapacity.toString() + "  ");


                parentLay3.addView(view);
            }


        }


    }
}
