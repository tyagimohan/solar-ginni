package com.solarginni.ProspectUser;

import android.content.Context;

import com.solarginni.DBModel.APIBody.GuestModel;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.DemandQuoteModel;
import com.solarginni.DBModel.ManufacturerModel;
import com.solarginni.DBModel.OportunityResponse;
import com.solarginni.DBModel.QuotationModel;

public interface ProspectContract {

    interface View {
        void onFetchAccountInfo(CommomResponse response);

        void onFetchLandingDetails(CommomResponse response);

        void onSaveFirstQuotation(OportunityResponse response );

        void fetChQuotation(CommomResponse response);

        void fetchPendingQuote(CommomResponse response);

        void onGetQuote();


        void guestUserApi(CommomResponse response);


        void onFailure(String msg);
    }

    interface Presenter {
        void getAccountInfo(String token);

        void getLandingDetails(String token);
        void getQuote(Context context, String token, DemandQuoteModel model, String optId);

        void filterQuote(String token,String distance,String price,String siteCount,String optID,int sorting);

        void saveFirstQuotattion(String token, QuotationModel model,String page,String optyId);

        void guestUserApi(Context context,String token, GuestModel model);


    }

    interface Interactor {
        void getAccountInfo(String token, OniInteraction listenr);

        void getLandingDetails(String token, OniInteraction listenr);

        void getQuote(Context context,String token, DemandQuoteModel model,String optId,OniInteraction interaction);

        void filterQuote(String token,String distance,String price,String siteCount,String optID,int sorting,OniInteraction listenr);

        void saveFirstQuotattion(String token, QuotationModel model,String page,String optyId,OniInteraction listenr);
        void guestUserApi(Context context,String token, GuestModel model,OniInteraction listenr);

    }

    interface OniInteraction {
        void onFetchAccountInfo(CommomResponse response);

        void onFetchLandingDetails(CommomResponse response);

        void onSaveFirstQuotation(OportunityResponse response);

        void fetChQuotation(CommomResponse response);

        void onGetQuote();


        void fetchPendingQuote(CommomResponse response);


        void guestUserApi(CommomResponse response);



        void onFailure(String msg);
    }
}
