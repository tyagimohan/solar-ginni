package com.solarginni.ProspectUser;

import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.RestClient;

public class FetchQuotePresenter implements FetchQuoteContract.Presenter, FetchQuoteContract.OnInteractor {

    private FetchQuoteContract.View view;
    private FetchQuoteInteractor interactor;

    public FetchQuotePresenter(FetchQuoteContract.View view) {
        this.view = view;
        interactor= new FetchQuoteInteractor(RestClient.getRetrofitBuilder());
    }

    @Override
    public void getPendingQuote(String token,String role,String siteType,String status,int createdBy) {
        interactor.getPendingQuote(token,role,siteType,status,createdBy,this);

    }

    @Override
    public void recallQuote(String token, String optId) {

        interactor.recallQuote(token,optId,this);


    }

    @Override
    public void getComparision(String token, String optId) {
        interactor.getComparision(token,optId,this);
    }

    @Override
    public void onFetchQuote(CommomResponse response) {
        view.onFetchQuote(response);

    }

    @Override
    public void onGetRecall(CommomResponse response) {
        view.onGetRecall(response);

    }

    @Override
    public void onGetComparision(CommomResponse response) {
        view.onGetComparision(response);

    }

    @Override
    public void onFailure(String msg) {
        view.onFailure(msg);

    }
}
