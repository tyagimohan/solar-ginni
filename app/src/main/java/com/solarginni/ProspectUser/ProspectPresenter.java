package com.solarginni.ProspectUser;

import android.content.Context;

import com.solarginni.DBModel.APIBody.GuestModel;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.DemandQuoteModel;
import com.solarginni.DBModel.ManufacturerModel;
import com.solarginni.DBModel.OportunityResponse;
import com.solarginni.DBModel.QuotationModel;
import com.solarginni.network.RestClient;

public class ProspectPresenter implements ProspectContract.Presenter,ProspectContract.OniInteraction {

    private ProspectContract.View view;
    private ProspectInteractor interactor;

    public ProspectPresenter(ProspectContract.View view) {
        this.view = view;
        interactor= new ProspectInteractor(RestClient.getRetrofitBuilder());
    }

    @Override
    public void getAccountInfo(String token) {
        interactor.getAccountInfo(token,this);

    }

    @Override
    public void getLandingDetails(String token) {
        interactor.getLandingDetails(token,this);

    }

    @Override
    public void getQuote(Context context, String token, DemandQuoteModel model, String optId) {
        interactor.getQuote(context,token,model,optId,this);

    }

    @Override
    public void filterQuote(String token, String distance, String price, String siteCount, String optID,int sorting) {
        interactor.filterQuote(token,distance,price,siteCount,optID,sorting,this);
    }

    @Override
    public void saveFirstQuotattion(String token, QuotationModel model,String page,String optyId) {

        interactor.saveFirstQuotattion(token,model,page,optyId,this);

    }

    @Override
    public void guestUserApi(Context context, String token, GuestModel model) {
        interactor.guestUserApi(context,token,model,this);

    }

    @Override
    public void onFetchAccountInfo(CommomResponse response) {
      view.onFetchAccountInfo(response);
    }

    @Override
    public void onFetchLandingDetails(CommomResponse response) {
        view.onFetchLandingDetails(response);

    }

    @Override
    public void onSaveFirstQuotation(OportunityResponse response) {
        view.onSaveFirstQuotation(response);

    }

    @Override
    public void fetChQuotation(CommomResponse response) {
               view.fetChQuotation(response);
    }

    @Override
    public void onGetQuote() {
        view.onGetQuote();

    }

    @Override
    public void fetchPendingQuote(CommomResponse response) {
        view.fetchPendingQuote(response);

    }

    @Override
    public void guestUserApi(CommomResponse response) {
        view.guestUserApi(response);

    }

    @Override
    public void onFailure(String msg) {
                view.onFailure(msg);
    }
}
