package com.solarginni.ProspectUser;

import android.content.Context;

import com.solarginni.Backend.DAOs.QuoteImpDao;
import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.APIBody.GuestModel;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.DemandQuoteModel;
import com.solarginni.DBModel.ManufacturerModel;
import com.solarginni.DBModel.OportunityResponse;
import com.solarginni.DBModel.QuotationModel;
import com.solarginni.Utility.SecurePrefManager;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;

import retrofit2.Retrofit;

import static com.solarginni.Utility.AppConstants.TOKEN;

public class ProspectInteractor implements ProspectContract.Interactor {
    private Retrofit retrofit;
    private ApiInterface apiInterface;

    public ProspectInteractor(Retrofit retrofit) {
        this.retrofit = retrofit;
        apiInterface = retrofit.create(ApiInterface.class);
    }

    @Override
    public void getAccountInfo(String token, ProspectContract.OniInteraction listenr) {

        apiInterface.getAccountDetails(token).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                listenr.onFetchAccountInfo(response);
            }

            @Override
            public void onError(ApiError error) {
                listenr.onFailure(error.getMessage());
            }

            @Override
            public void onFailure(Throwable throwable) {
                listenr.onFailure(throwable.getMessage());
            }
        });

    }

    @Override
    public void getLandingDetails(String token, ProspectContract.OniInteraction listenr) {
        apiInterface.getProspectLandingDetails(token).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                listenr.onFetchLandingDetails(response);

            }

            @Override
            public void onError(ApiError error) {
                listenr.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                listenr.onFailure(throwable.getMessage());

            }
        });

    }

    @Override
    public void getQuote(Context context, String token, DemandQuoteModel model, String optId, ProspectContract.OniInteraction interaction) {
        apiInterface.demandQuote(token,model,optId).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                interaction.onGetQuote();

            }

            @Override
            public void onError(ApiError error) {
                interaction.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                interaction.onFailure(throwable.getMessage());

            }
        });

    }

    @Override
    public void filterQuote(String token, String distance, String price, String siteCount, String optID,int sorting, ProspectContract.OniInteraction listenr) {
        apiInterface.filterQuote(token, "100", "70000", "0", optID, false,sorting).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                listenr.fetChQuotation(response);

            }

            @Override
            public void onError(ApiError error) {
                listenr.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                listenr.onFailure(throwable.getMessage());

            }
        });
    }

    @Override
    public void saveFirstQuotattion(String token, QuotationModel model, String page, String optyId, ProspectContract.OniInteraction listenr) {


        apiInterface.saveQuotation(token, model, page, optyId).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                OportunityResponse oportunityResponse = response.toResponseModel(OportunityResponse.class);
//
//                if (page.equalsIgnoreCase("3")) {
//
//                    apiInterface.filterQuote(token, "100", "70000", "0", optyId, false).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
//                        @Override
//                        public void onSuccess(CommomResponse response) {
//                            listenr.fetChQuotation(response);
//
//                        }
//
//                        @Override
//                        public void onError(ApiError error) {
//                            listenr.onFailure(error.getMessage());
//
//                        }
//
//                        @Override
//                        public void onFailure(Throwable throwable) {
//                            listenr.onFailure(throwable.getMessage());
//
//                        }
//                    });
//                } else {

                    listenr.onSaveFirstQuotation(oportunityResponse);
//                }


            }

            @Override
            public void onError(ApiError error) {
                listenr.onFailure(error.getMessage());
            }

            @Override
            public void onFailure(Throwable throwable) {
                listenr.onFailure(throwable.getMessage());

            }
        });
    }

    @Override
    public void guestUserApi(Context context, String token, GuestModel model, ProspectContract.OniInteraction listenr) {
        apiInterface.guestUserApi(token,model).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse commomResponse) {
                SecurePrefManager securePrefManager= new SecurePrefManager(context);
                String tempToken= commomResponse.toResponseModel(String.class);
                securePrefManager.storeSharedValue(TOKEN,tempToken);

                apiInterface.getProspectLandingDetails(tempToken).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
                    @Override
                    public void onSuccess(CommomResponse response) {
                        listenr.guestUserApi(response);
                    }

                    @Override
                    public void onError(ApiError error) {
                        listenr.onFailure(error.getMessage());

                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        listenr.onFailure(throwable.getMessage());

                    }
                });
            }

            @Override
            public void onError(ApiError error) {
                listenr.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                listenr.onFailure(throwable.getMessage());

            }
        });

    }

}
