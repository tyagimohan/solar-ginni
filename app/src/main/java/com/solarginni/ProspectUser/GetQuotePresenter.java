package com.solarginni.ProspectUser;

import android.content.Context;

import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.DemandQuoteModel;
import com.solarginni.DBModel.QuotationModel;
import com.solarginni.network.RestClient;

import java.util.List;

public class GetQuotePresenter implements GetQuotationContract.Presenter, GetQuotationContract.OnInteraction {

    private GetQuotationContract.View view;
    private GetQuoteInteractor interactor;

    public GetQuotePresenter(GetQuotationContract.View view) {
        this.view = view;
        interactor= new GetQuoteInteractor(RestClient.getRetrofitBuilder());
    }

    @Override
    public void getQuote(Context context,String token, DemandQuoteModel model,String optId) {
        interactor.getQuote(context,token, model,optId,this);

    }

    @Override
    public void sortListByValue(Context context, String value) {
       interactor.sortListByValue(context,value,this);
    }

    @Override
    public void applyFilter(Context context,String token, String price, String distance, String siteCount, String optId,boolean isEmpaneled,int sorting) {
         interactor.applyFilter(context,token,price,distance,siteCount,optId,isEmpaneled,sorting,this);
    }



    @Override
    public void onGetQuote() {
        view.onGetQuote();

    }

    @Override
    public void failure(String msg) {
        view.failure(msg);

    }

    @Override
    public void onApplyFilter(List<QutationModel.Filter> list) {
        view.onApplyFilter(list);

    }
}
