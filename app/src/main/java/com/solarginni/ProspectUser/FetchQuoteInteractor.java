package com.solarginni.ProspectUser;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;

import retrofit2.Retrofit;

public class FetchQuoteInteractor implements FetchQuoteContract.Interactor {
    private Retrofit retrofit;
    private ApiInterface apiInterface;

    public FetchQuoteInteractor(Retrofit retrofit) {
        this.retrofit = retrofit;
        apiInterface = retrofit.create(ApiInterface.class);
    }

    @Override
    public void getPendingQuote(String token,String role,String siteType,String status,int createdBy, FetchQuoteContract.OnInteractor listenr) {

        if (role.equalsIgnoreCase("3")|role.equalsIgnoreCase("7")){
            apiInterface.getPedningQuote(token).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
                @Override
                public void onSuccess(CommomResponse response) {
                    listenr.onFetchQuote(response);

                }

                @Override
                public void onError(ApiError error) {
                    listenr.onFailure(error.getMessage());

                }

                @Override
                public void onFailure(Throwable throwable) {
                    listenr.onFailure(throwable.getMessage());

                }
            });
        }
        else if (role.equalsIgnoreCase("1")|role.equalsIgnoreCase("9")){


            apiInterface.getImplementerQuote(token,siteType,status,createdBy).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
                @Override
                public void onSuccess(CommomResponse response) {
                    listenr.onFetchQuote(response);

                }

                @Override
                public void onError(ApiError error) {
                    listenr.onFailure(error.getMessage());

                }

                @Override
                public void onFailure(Throwable throwable) {
                    listenr.onFailure(throwable.getMessage());

                }
            });

        }




    }

    @Override
    public void recallQuote(String token, String optId, FetchQuoteContract.OnInteractor listenr) {
        apiInterface.recallQuote(token,optId).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                listenr.onGetRecall(response);

            }

            @Override
            public void onError(ApiError error) {
                     listenr.onFailure(error.getMessage());
            }

            @Override
            public void onFailure(Throwable throwable) {
           listenr.onFailure(throwable.getMessage());
            }
        });

    }

    @Override
    public void getComparision(String token, String optId, FetchQuoteContract.OnInteractor listenr) {
        apiInterface.getComparision(token,optId).enqueue(new ResponseResolver<CommomResponse>(retrofit) {
            @Override
            public void onSuccess(CommomResponse commomResponse) {
                listenr.onGetComparision(commomResponse);

            }

            @Override
            public void onError(ApiError error) {
                listenr.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                listenr.onFailure(throwable.getMessage());

            }
        });

    }
}
