package com.solarginni.VerifyModule;

import android.content.Context;

import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.OtpModel;
import com.solarginni.GetDetails.Details;

public interface VerifyContract {

    interface View {
        void onVerifyOtp(String msg ,OtpModel model);
        void onOtpSent( String msg,String token);
        void onFailure(String msg);
        void OngetSiteDetails(CommomResponse response);
        void onFetchDetails(CommomResponse response,String role);
    }

    interface Presenter {

        void verifyOtp(Context context, String otp,String token);

        void resedOtp(Context context, String mobile);
        void getDetails( String token,String role);
        void getSiteDetails(String token);
    }

    interface Interactor {
        void verifyOtp(Context context,String otp, String token, VerifyContract.OnInteraction listner);
        void resedOtp(Context context, String mobile, VerifyContract.OnInteraction listner);
        void getDetails( String token,String role, VerifyContract.OnInteraction listner);
        void getSiteDetails(String token, OnInteraction listener);
    }

    interface OnInteraction {
        void onVerifyOtp(String msg, OtpModel model);
        void onOtpSent(String msg,String token);
        void onFetchDetails(CommomResponse response,String role);
        void OngetSiteDetails(CommomResponse response);
        void onFailure(String msg);
    }
}
