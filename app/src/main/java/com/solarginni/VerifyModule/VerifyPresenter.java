package com.solarginni.VerifyModule;

import android.content.Context;

import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.OtpModel;
import com.solarginni.network.RestClient;

public class VerifyPresenter implements VerifyContract.Presenter, VerifyContract.OnInteraction {
    private VerifyContract.View view;private VerifyInteractor interactor;

    public VerifyPresenter(VerifyContract.View view) {
        this.view = view;
        interactor= new VerifyInteractor(RestClient.getRetrofitBuilder());
    }

    @Override
    public void verifyOtp(Context context, String otp,String token) { interactor.verifyOtp(context,otp,token,this);

    }

    @Override
    public void resedOtp(Context context, String mobile) {
        interactor.resedOtp(context,mobile,this);
    }

    @Override
    public void getDetails(String token, String role) {
        interactor.getDetails(token,role,this);
    }

    @Override
    public void getSiteDetails(String token) {
        interactor.getSiteDetails(token,this);
    }

    @Override
    public void onVerifyOtp(String msg, OtpModel model) {
        view.onVerifyOtp(msg, model);

    }

    @Override
    public void onOtpSent(String msg, String token) {
        view.onOtpSent(msg,token);
    }

    @Override
    public void onFetchDetails(CommomResponse response, String role) {
         view.onFetchDetails(response,role);
    }

    @Override
    public void OngetSiteDetails(CommomResponse response) {
        view.OngetSiteDetails(response);

    }

    @Override
    public void onFailure(String msg) {
        view.onFailure(msg);

    }
}
