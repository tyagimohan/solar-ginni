package com.solarginni.VerifyModule;

import android.content.Context;
import android.provider.Settings;

import com.solarginni.Base.ResponseResolver;
import com.solarginni.DBModel.APIBody.SendOtp;
import com.solarginni.DBModel.CommomResponse;
import com.solarginni.DBModel.OtpModel;
import com.solarginni.DBModel.TokenModel;
import com.solarginni.R;
import com.solarginni.network.ApiError;
import com.solarginni.network.ApiInterface;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Retrofit;

import static com.solarginni.Utility.AppConstants.BO_MANAGER;
import static com.solarginni.Utility.AppConstants.CUSTOMER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.IMPLEMENTER_ROLE_ID;
import static com.solarginni.Utility.AppConstants.PROSPECT_ROLE_ID;
import static com.solarginni.Utility.AppConstants.SG_ADMIN;
import static com.solarginni.Utility.AppConstants.SUPER_IMPLEMENTER_ROLE_ID;

public class VerifyInteractor implements VerifyContract.Interactor {
    private Retrofit mRetrofit;
    private ApiInterface mApiInterface;

    public VerifyInteractor(Retrofit retrofit) {
        this.mRetrofit = retrofit;
        mApiInterface = mRetrofit.create(ApiInterface.class);

    }

    @Override
    public void verifyOtp(Context context, String otp, String token, VerifyContract.OnInteraction listner) {


        mApiInterface.verifyOtp(otp, token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
            @Override
            public void onSuccess(CommomResponse commomResponse) {
                OtpModel otpModel = commomResponse.toResponseModel(OtpModel.class);

                listner.onVerifyOtp(commomResponse.getMessage(), otpModel);
            }

            @Override
            public void onError(ApiError error) {
                listner.onFailure(error.getMessage());
            }

            @Override
            public void onFailure(Throwable throwable) {
                listner.onFailure(throwable.getMessage());
            }
        });


    }

    @Override
    public void resedOtp(Context context, String mobileNumber, VerifyContract.OnInteraction listner) {

        String deviceId = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Map<String, String> map = new HashMap<>();
        map.put("phone", mobileNumber);
        map.put("deviceId", deviceId);

        SendOtp otp = new SendOtp(mobileNumber, deviceId);


        mApiInterface.sendOtp(otp, context.getString(R.string.static_token)).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
            @Override
            public void onSuccess(CommomResponse commomResponse) {
                TokenModel otpModel = commomResponse.toResponseModel(TokenModel.class);

                //TODO Save Authentication Token
                listner.onOtpSent(commomResponse.getMessage(), otpModel.getToken());


            }

            @Override
            public void onError(ApiError error) {
                listner.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                listner.onFailure(throwable.getMessage());


            }
        });

    }

    @Override
    public void getDetails(String token, String role, VerifyContract.OnInteraction listner) {

        switch (role) {
            case CUSTOMER_ROLE_ID:
                mApiInterface.getCustomerDetails(token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                    @Override
                    public void onSuccess(CommomResponse response) {
                        listner.onFetchDetails(response, role);
                    }

                    @Override
                    public void onError(ApiError error) {
                        listner.onFailure(error.getMessage());

                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        listner.onFailure(throwable.getMessage());

                    }
                });
                break;
            case IMPLEMENTER_ROLE_ID:
            case SUPER_IMPLEMENTER_ROLE_ID:
                mApiInterface.getImplementer(token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                    @Override
                    public void onSuccess(CommomResponse response) {
                        listner.onFetchDetails(response, role);

                    }

                    @Override
                    public void onError(ApiError error) {
                        listner.onFailure(error.getMessage());
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        listner.onFailure(throwable.getMessage());

                    }
                });
                break;

            case PROSPECT_ROLE_ID:
                mApiInterface.getProspectLandingDetails(token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                    @Override
                    public void onSuccess(CommomResponse response) {
                        listner.onFetchDetails(response,role);

                    }

                    @Override
                    public void onError(ApiError error) {
                        listner.onFailure(error.getMessage());

                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        listner.onFailure(throwable.getMessage());
                    }
                });
                break;
            case SG_ADMIN:
            case BO_MANAGER:
                mApiInterface.getSGLading(token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
                    @Override
                    public void onSuccess(CommomResponse response) {
                        listner.onFetchDetails(response, role);

                    }

                    @Override
                    public void onError(ApiError error) {
                        listner.onFailure(error.getMessage());

                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        listner.onFailure(throwable.getMessage());
                    }
                });
                break;
        }

    }

    @Override
    public void getSiteDetails(String token, VerifyContract.OnInteraction listener) {

        mApiInterface.getSiteLOVs(token).enqueue(new ResponseResolver<CommomResponse>(mRetrofit) {
            @Override
            public void onSuccess(CommomResponse response) {
                listener.OngetSiteDetails(response);
            }

            @Override
            public void onError(ApiError error) {
                listener.onFailure(error.getMessage());

            }

            @Override
            public void onFailure(Throwable throwable) {
                listener.onFailure(throwable.getMessage());

            }
        });

    }
}
