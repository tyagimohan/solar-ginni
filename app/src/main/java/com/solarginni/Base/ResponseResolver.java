package com.solarginni.Base;

import android.util.Log;

import com.solarginni.network.ApiError;
import com.solarginni.network.ErrorUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Mohan on 15/1/20.
 */

public abstract class ResponseResolver<T> implements Callback<T> {

    private Retrofit mRetrofit;

    /**
     * public parameter constructor
     *
     * @param retrofit the retrofit
     */
    public ResponseResolver(Retrofit retrofit) {
        mRetrofit = retrofit;
    }

    /**
     * On Api response success
     *
     * @param t the response
     */
    public abstract void onSuccess(T t);

    public abstract void onError(ApiError error);

    /**
     * Indicates failure of the request
     *
     * @param throwable the throwable generated
     */
    public abstract void onFailure(Throwable throwable);

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        Log.e("APIResponse",""+response.body());
        Log.e("APIName",""+response.toString());
        if (response.isSuccessful()) {
            onSuccess(response.body());
        } else {
            onError(ErrorUtils.parseError(response, mRetrofit));
            Log.e("Error", " "+ ErrorUtils.parseError(response, mRetrofit));
        }

    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {



        onFailure(t);

    }
}
