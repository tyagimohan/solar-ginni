package com.solarginni.Base;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

/**
 * Created by Mohan on 3/1/20.
 */

public interface IBaseView {


    void showProgress(String progressMsg);

    void hideProgress();

    void showToast(String msg);
    void loadFragment(Fragment fragment, Bundle bundle, String tag);

    void goToNextScreen(@NonNull Class<?> className, @Nullable Bundle bundle);

    void goToNextScreen(@NonNull Class<?> className, @Nullable Bundle bundle, int flag);

    void setToolbar();

    void setToolbar(@NonNull String title);

    void setToolbar(@NonNull String title, boolean enableBackBtn);

    void showAlert(@NonNull String msg);


    void hideKeyboard(Activity activity);
    void hideKeyboard();
}
