package com.solarginni.Base;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collection;
import java.util.List;

public abstract class BasePaginationAdapter<T, Holder extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<Holder> {


    private static final int ITEM = 0;
    private static final int LOADING = 1;

    private List<T> mLIst;
    private Context context;

    private boolean isLoadingAdded = false;

    public BasePaginationAdapter(Context context) {
        this.context = context;
    }



    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(getLayout(viewType), parent, false);
        return getViewHolder(view, viewType);
    }

//    @NonNull
//    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
//        RecyclerView.ViewHolder viewHolder;
//        View v1 = inflater.inflate(R.layout.item_list, parent, false);
//        viewHolder = new MovieVH(v1);
//        return viewHolder;
//    }

//    @Override
//    public void onBindViewHolder(Holder holder, int position) {
//
//        Movie movie = movies.get(position);
//
//        switch (getItemViewType(position)) {
//            case ITEM:
//                MovieVH movieVH = (MovieVH) holder;
//
//                movieVH.textView.setText(movie.getTitle());
//                break;
//            case LOADING:
////                Do nothing
//                break;
//        }
//
//    }

    @Override
    public int getItemCount() {
        return mLIst == null ? 0 : mLIst.size();
    }

    public void add(T item) {
        mLIst.add(item);
//        notifyItemChanged(getPosition(item));
        notifyDataSetChanged();
    }

    public Context getContext() {
        return context;
    }

    public void add(int position, T item) {
        mLIst.add(position, item);
        notifyItemInserted(position);
    }

    public void addAll(Collection<? extends T> collection) {
        mLIst.clear();
        mLIst.addAll(collection);
    }


    public abstract int getLayout(int viewType);

    public abstract Holder getViewHolder(View view, int viewType);


    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;

    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = mLIst.size() - 1;
        T item = getItem(position);

        if (item != null) {
            mLIst.remove(position);
            notifyItemRemoved(position);
        }
    }

    public T getItem(int position) {
        return mLIst.get(position);
    }

    protected class LoadingVH extends RecyclerView.ViewHolder {

        public LoadingVH(View itemView) {
            super(itemView);
        }
    }


}
