package com.solarginni.Base;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.solarginni.Utility.SecurePrefManager;

/**
 * Created by Mohan on 7/1/20.
 */

public abstract class BaseFragment extends Fragment implements IBaseView {

    public SecurePrefManager securePrefManager;
    View view;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(getLayout(), container, false);
        securePrefManager = new SecurePrefManager(getContext());

        initViews(view);
        setUp();
        return view;
    }

   public View getView(){
        return view;
   }
    public abstract int getLayout();

    public abstract void initViews(View view);

    public abstract void setUp();

    @Override
    public void showProgress(String progressMsg) {
        ((BaseActivity) getActivity()).showProgress(progressMsg);

    }

    @Override
    public void hideProgress() {

        try {
            ((BaseActivity) getActivity()).hideProgress();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onDestroyView() {

        super.onDestroyView();
        view = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void hideKeyboard(Activity activity) {
        ((BaseActivity) getActivity()).hideKeyboard(activity);

    }

    @Override
    public void showToast(String msg) {
        ((BaseActivity) getActivity()).showToast(msg);

    }

    @Override
    public void loadFragment(Fragment fragment, Bundle bundle, String tag) {
        ((BaseActivity) getActivity()).loadFragment(fragment, bundle, tag);

    }

    @Override
    public void goToNextScreen(@NonNull Class<?> className, @Nullable Bundle bundle) {
        ((BaseActivity) getActivity()).goToNextScreen(className, bundle);

    }

    @Override
    public void goToNextScreen(@NonNull Class<?> className, @Nullable Bundle bundle, int flag) {

    }

    @Override
    public void setToolbar() {

    }

    public void disableSuggestion(TextView editText) {
        ((BaseActivity) getActivity()).disableSuggestion(editText);
    }

    public Intent openLink(String url) {
        return ((BaseActivity) getActivity()).openLink(url);
    }


    @Override
    public void setToolbar(@NonNull String title) {

    }

    @Override
    public void setToolbar(@NonNull String title, boolean enableBackBtn) {

    }

    @Override
    public void showAlert(@NonNull String msg) {

    }

    @Override
    public void hideKeyboard() {
        ((BaseActivity) getActivity()).hideKeyboard(getActivity());

    }

    public void showAlertWithList(final String[] dataArray, final @StringRes int title, final TextView textView) {
        ((BaseActivity) getActivity()).showAlertWithList(dataArray, title, textView);
    }
}
