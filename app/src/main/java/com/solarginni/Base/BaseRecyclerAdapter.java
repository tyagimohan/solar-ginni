package com.solarginni.Base;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.solarginni.Utility.Utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by JSN on 5/12/16.
 */

public abstract class BaseRecyclerAdapter<T, Holder extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<Holder> {

    private static int EMPTY_VIEW = 0;
    private static int LAYOUT_VIEW = 1;
    private Context mContext;
    private List<T> mItemlist = new ArrayList<>();
    private OnClick<T> mListener;
    private onItemClick<T> mlongListener;
    private onIndexedClick<T> mIndexClikListener;

    public BaseRecyclerAdapter(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(mContext).inflate(mItemlist.isEmpty() ? emptyViewLayout : getLayout(), parent, false);
//        return !mItemlist.isEmpty() ? getViewHolder(view) : getViewHolder(view);

        View view = LayoutInflater.from(mContext).inflate(getLayout(viewType), parent, false);
        return getViewHolder(view, viewType);


    }

//    @Override
//    public void onBindViewHolder(Holder holder, int position) {
//        if (getItemViewType(position) != EMPTY_VIEW) {
//            onBindHolder(holder, position);
//        }
//    }

    @Override
    public int getItemCount() {
//        return mItemlist.size() == 0 ? 1 : mItemlist.size();
        return mItemlist.size();
    }

//    @Override
//    public int getItemViewType(int position) {
//        return mItemlist.size() == 0 ? EMPTY_VIEW : LAYOUT_VIEW;
//    }

    public abstract int getLayout(int viewType);

    public abstract Holder getViewHolder(View view, int viewType);

//    public abstract void onBindHolder(Holder holder, int position);

    public void add(T item) {
        mItemlist.add(item);
//        notifyItemChanged(getPosition(item));
        notifyDataSetChanged();
    }

    public void add(int position, T item) {
        mItemlist.add(position, item);
        notifyItemInserted(position);
    }


    public int getPosition(T data) {
        return mItemlist.indexOf(data);
    }


    public void remove(int position) {
        mItemlist.remove(position);
        notifyItemRemoved(position);
    }

    public void addAt(int pos, T item) {
        mItemlist.add(pos, item);

        notifyDataSetChanged();
    }

    public void addAtChat(int pos, T item) {
        mItemlist.add(pos, item);

        //  notifyDataSetChanged();
    }

    public void addAll(Collection<? extends T> collection) {
        mItemlist.clear();
        mItemlist.addAll(collection);
    }

    public void addAllForPagination(Collection<? extends T> collection) {
        mItemlist.addAll(collection);
    }


    public int getListSize() {
        return mItemlist.size();
    }

    public void clear() {
        mItemlist.clear();
    }


    public void replaceElement(int position, T element) {
        mItemlist.remove(position);
        mItemlist.add(position, element);
        notifyItemChanged(position);
    }

    public Context getContext() {
        return mContext;
    }

    public T getItem(int position) {
        return mItemlist.get(position);
    }

    public void setOnClickListener(OnClick mListener) {
        this.mListener = mListener;
    }

    public OnClick getListener() {
        return mListener;
    }

    public void setOnLongClickListener(onItemClick mListener) {
        this.mlongListener = mListener;
    }

    public void setIndexClikListener(onIndexedClick mListener) {
        this.mIndexClikListener = mListener;
    }

    public onItemClick getLongClickListener() {
        return mlongListener;
    }

    public  onIndexedClick getIndexedClick(){
           return mIndexClikListener;
    }

    public List<T> getList() {
        return mItemlist;
    }

    public interface OnClick<T> {
        void onClick(T data, int position);
    }
    public interface onItemClick<T> {
        void onItemClick(T data, int position,View view);
    }

    public interface onIndexedClick<T> {
        void onIndexedClick(T data, int position, View view,int index);
    }


    public static class SpaceDecoration extends RecyclerView.ItemDecoration {
        private int mSpace = 0;
        private boolean mVerticalOrientation = true;

        public SpaceDecoration(int space) {
            this.mSpace = space;
        }

        public SpaceDecoration(int space, boolean verticalOrientation) {
            this.mSpace = space;
            this.mVerticalOrientation = verticalOrientation;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

            outRect.top = Utils.dp2px(view.getContext(), mSpace);
            if (mVerticalOrientation) {
                if (parent.getChildAdapterPosition(view) == 0) {
                    outRect.set(0, Utils.dp2px(view.getContext(), mSpace), 0, Utils.dp2px(view.getContext(), mSpace));
                } else {
                    outRect.set(0, 0, 0, Utils.dp2px(view.getContext(), mSpace));
                }
            } else {
                if (parent.getChildAdapterPosition(view) == 0) {
                    outRect.set(Utils.dp2px(view.getContext(), mSpace), 0, 0, 0);
                } else {
                    outRect.set(Utils.dp2px(view.getContext(), mSpace), 0, Utils.dp2px(view.getContext(), mSpace), 0);
                }
            }
        }
    }

    public static class DividerItemDecoration extends RecyclerView.ItemDecoration {

        public static final int HORIZONTAL_LIST = LinearLayoutManager.HORIZONTAL;
        public static final int VERTICAL_LIST = LinearLayoutManager.VERTICAL;
        private final int[] ATTRS = new int[]{
                android.R.attr.listDivider
        };
        private Drawable mDivider;

        private int mOrientation;

        public DividerItemDecoration(Context context, int orientation) {
            final TypedArray a = context.obtainStyledAttributes(ATTRS);
            mDivider = a.getDrawable(0);
            a.recycle();
            setOrientation(orientation);
        }

        public void setOrientation(int orientation) {
            if (orientation != HORIZONTAL_LIST && orientation != VERTICAL_LIST) {
                throw new IllegalArgumentException("invalid orientation");
            }
            mOrientation = orientation;
        }

        @Override
        public void onDraw(Canvas c, RecyclerView parent) {
//            if (mOrientation == VERTICAL_LIST) {
//                drawVertical(c, parent);
//            } else {
//                drawHorizontal(c, parent);
//            }
        }

        public void drawVertical(Canvas c, RecyclerView parent) {
            final int left = parent.getPaddingLeft();
            final int right = parent.getWidth() - parent.getPaddingRight();

            final int childCount = parent.getChildCount();
            for (int i = 0; i < childCount; i++) {
                final View child = parent.getChildAt(i);

                if (parent.getAdapter().getItemViewType(i) == 1) {
                    continue;
                }

                final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
                        .getLayoutParams();
                final int top = child.getBottom() + params.bottomMargin;
                final int bottom = top + mDivider.getIntrinsicHeight();
                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(c);
            }
        }

        public void drawHorizontal(Canvas c, RecyclerView parent) {
            final int top = parent.getPaddingTop();
            final int bottom = parent.getHeight() - parent.getPaddingBottom();

            final int childCount = parent.getChildCount();
            for (int i = 0; i < childCount; i++) {
                final View child = parent.getChildAt(i);
                final RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child
                        .getLayoutParams();
                final int left = child.getRight() + params.rightMargin;
                final int right = left + mDivider.getIntrinsicHeight();
                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(c);
            }
        }

        @Override
        public void getItemOffsets(Rect outRect, int itemPosition, RecyclerView parent) {

            if (parent.getAdapter().getItemViewType(itemPosition) == 1) {
                return;
            }

            if (mOrientation == VERTICAL_LIST) {
                outRect.set(0, 0, 0, mDivider.getIntrinsicHeight());
            } else {
                outRect.set(0, 0, mDivider.getIntrinsicWidth(), 0);
            }
        }
    }

    public class EmptyViewHolder extends RecyclerView.ViewHolder {
        public EmptyViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class BaseViewHolder extends RecyclerView.ViewHolder {
        public BaseViewHolder(View itemView) {
            super(itemView);
        }
    }
}
