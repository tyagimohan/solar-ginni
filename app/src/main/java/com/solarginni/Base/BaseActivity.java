package com.solarginni.Base;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.solarginni.BuildConfig;
import com.solarginni.R;
import com.solarginni.Utility.SecurePrefManager;
import com.solarginni.Utility.Utils;

/**
 * Created by Mohan on 3/1/20.
 */

public abstract class BaseActivity extends AppCompatActivity implements IBaseView {
    private static final String OVERLAY_TEXT = BuildConfig.APP_NAME + "_" + BuildConfig.FLAVOR + "_v" + BuildConfig.VERSION_CODE;
    public SecurePrefManager securePrefManager;
    private Handler handler = new Handler();
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayout());
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        securePrefManager = new SecurePrefManager(this);
        initViews();
        setUp();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            disableAutofill();
    }

    public abstract int getLayout();

    public abstract void initViews();

    public abstract void setUp();

    @Override
    public void showProgress(String progressMsg) {
        if (mProgressDialog == null) {
            setProgressDialog(progressMsg);
            mProgressDialog.show();
        }
    }

    @Override
    public void hideProgress() {
        runOnUiThread(() -> {
            if (mProgressDialog != null && mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
                mProgressDialog = null;
            }
        });

    }

    @Override
    public void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();

    }

    @Override
    public void loadFragment(Fragment fragment, Bundle bundle, String tag) {
        if (bundle != null) {
            fragment.setArguments(bundle);
        }



        handler.postDelayed(getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment, tag)::commitNowAllowingStateLoss, 400);
    }


    @Override
    public void goToNextScreen(@NonNull Class<?> className, @Nullable Bundle bundle) {
        intentForActivity(-1, className, bundle);
    }

    @Override
    public void goToNextScreen(@NonNull Class<?> className, @Nullable Bundle bundle, int flag) {
        intentForActivity(flag, className, bundle);
    }

    private void intentForActivity(int flag, @NonNull Class<?> className, @Nullable Bundle bundle) {
        Intent intent = new Intent(this, className);

        if (flag != -1)
            intent.setFlags(flag);

        if (bundle != null)
            intent.putExtras(bundle);

        startActivity(intent);
    }

    @Override
    public void setToolbar() {

    }

    @Override
    public void setToolbar(@NonNull String title) {

    }

    @Override
    public void setToolbar(@NonNull String title, boolean enableBackBtn) {

    }

    @Override
    public void showAlert(@NonNull String msg) {

    }

    public void hideKeyboard(Activity activity) {
        if (activity != null) {
            final InputMethodManager imm = (InputMethodManager)
                    activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            View view = activity.getCurrentFocus();
            if (view == null) {
                view = new View(activity);
            }
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (BuildConfig.WATER_MARK) {
            final DrawOnTop mDraw = new DrawOnTop(this);
            addContentView(mDraw, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            mDraw.bringToFront();
        }
    }

    @Override
    public void hideKeyboard() {

    }

    private void setProgressDialog(String message) {
        mProgressDialog = new ProgressDialog(BaseActivity.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage(message);
    }

    public void showAlertWithList(final String[] dataArray, final @StringRes int title, final TextView textView) {
        hideKeyboard(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(title));
        builder.setItems(dataArray, (dialog, item) -> {
            textView.setText(dataArray[item]);
        });
        AlertDialog alerts = builder.create();
        alerts.show();
    }

    public void showAlertWithList(final String[] dataArray, String title, final TextView textView) {
        hideKeyboard(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setItems(dataArray, (dialog, item) -> {
            textView.setText(dataArray[item]);
            textView.clearFocus();
        });
        AlertDialog alerts = builder.create();
        alerts.show();
    }

    public void disableSuggestion(TextView editText) {

        editText.setLongClickable(false);
        editText.setTextIsSelectable(false);
        editText.setInputType(editText.getInputType() | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        editText.setImeOptions(EditorInfo.IME_ACTION_DONE);
        editText.setCustomSelectionActionModeCallback(new ActionMode.Callback() {

            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void disableAutofill() {
        getWindow().getDecorView().setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_NO_EXCLUDE_DESCENDANTS);
    }

    public Intent openLink(String url) {
        String mUrl = url;
        // if protocol isn't defined use http by default
        if (!TextUtils.isEmpty(url) && !url.contains("://")) {
            mUrl = "http://" + url;
        }
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(mUrl));
        return intent;
    }

    public class DrawOnTop extends View {

        private Paint paintText;
        private Rect bounds;

        /**
         * Instantiates a new Draw on top.
         *
         * @param activity current activity context
         */
        public DrawOnTop(final Context activity) {
            super(activity);
            paintText = new Paint();
            bounds = new Rect();
        }

        @Override
        protected void onDraw(final Canvas canvas) {
            // put your drawing commands here
            paintText.setColor(Color.GRAY);
            paintText.setTextSize(Utils.dpToPx(BaseActivity.this, 15));
            paintText.getTextBounds(OVERLAY_TEXT, 0, OVERLAY_TEXT.length(), bounds);
            canvas.drawText(OVERLAY_TEXT, getWidth() - (bounds.width() + 10), this.getHeight() - 15, paintText);
        }
    }


}
