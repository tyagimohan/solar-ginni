
package com.solarginni.DBModel.SiteDetailsModel;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.solarginni.SiteLovsModule.SiteDetailModel.CompanyId;

public class Equipment {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("panelType")
    @Expose
    private String panelType;
    @SerializedName("panelSize")
    @Expose
    private String panelSize;
    @SerializedName("companyId")
    @Expose
    private CompanyId companyId;
    @SerializedName("inverter")
    @Expose
    private List<Inverter> inverter = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPanelType() {
        return panelType;
    }

    public void setPanelType(String panelType) {
        this.panelType = panelType;
    }

    public String getPanelSize() {
        return panelSize;
    }

    public void setPanelSize(String panelSize) {
        this.panelSize = panelSize;
    }

    public CompanyId getCompanyId() {
        return companyId;
    }

    public void setCompanyId(CompanyId companyId) {
        this.companyId = companyId;
    }

    public List<Inverter> getInverter() {
        return inverter;
    }

    public void setInverter(List<Inverter> inverter) {
        this.inverter = inverter;
    }

}
