package com.solarginni.DBModel.SiteDetailsModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class SiteDetailsModel implements Serializable {


    @SerializedName("details")
    @Expose
    private List<SiteDetails> siteDetails = null;

    public List<SiteDetails> getSiteDetails() {
        return siteDetails;
    }

    public void setSiteDetails(List<SiteDetails> siteDetails) {
        this.siteDetails = siteDetails;
    }

}
