package com.solarginni.DBModel.SiteDetailsModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class EmapnelmentInfo implements Serializable {

    @SerializedName("empanelment-info")
    @Expose
    private List<EmpInfo> empInfo = null;

    public List<EmpInfo> getEmpInfo() {
        return empInfo;
    }

    public void setEmpInfo(List<EmpInfo> empInfo) {
        this.empInfo = empInfo;
    }

    public class EmpInfo implements Serializable {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("nodalBody")
        @Expose
        private String nodalBody;
        @SerializedName("from")
        @Expose
        private String from;
        @SerializedName("end")
        @Expose
        private String end;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getNodalBody() {
            return nodalBody;
        }

        public void setNodalBody(String nodalBody) {
            this.nodalBody = nodalBody;
        }

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public String getEnd() {
            return end;
        }

        public void setEnd(String end) {
            this.end = end;
        }

    }
}
