package com.solarginni.DBModel.SiteDetailsModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.solarginni.SiteLovsModule.SiteDetailModel.Address;

import java.io.Serializable;
import java.util.List;

public class SiteDetails implements Serializable {

    @SerializedName("address")
    @Expose
    private Address address;
    @SerializedName("phase")
    @Expose
    private String phase;
    @SerializedName("connectionId")
    @Expose
    private String connectionId;
    @SerializedName("siteId")
    @Expose
    private String siteId;
    @SerializedName("customerUserId")
    @Expose
    private String customerUserId;
    @SerializedName("installedBy")
    @Expose
    private String companyId;
    @SerializedName("solutionType")
    @Expose
    private String solutionType;
    @SerializedName("siteType")
    @Expose
    private String siteType;
    @SerializedName("siteSize")
    @Expose
    private String siteSize;
    @SerializedName("installedByName")
    @Expose
    private String installedBy;
    @SerializedName("installedOn")
    @Expose
    private String installedOn;
    @SerializedName("amcEnd")
    @Expose
    private String amcEnd;
    @SerializedName("panelType")
    @Expose
    private String panelType;
    @SerializedName("panelLogo")
    @Expose
    public String panelLogo;
    @SerializedName("companyId")
    @Expose
    public String panelMakeId;
    @SerializedName("panelSize")
    @Expose
    private String panelSize;
    @SerializedName("panelMake")
    @Expose
    private String panelMake;
    @SerializedName("inverter")
    @Expose
    private List<Inverter> inverter = null;
    @SerializedName("battery")
    @Expose
    private Battery battery;
    @SerializedName("isSiteRated")
    @Expose
    private Boolean isSiteRated;
    @SerializedName("image")
    @Expose
    private List<String> imageUrl = null;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("status")
    @Expose
    public boolean status;

    public String getCompanyId() {
        return companyId;
    }

    public String getCustomerUserId() {
        return customerUserId;
    }

    public Boolean getSiteRated() {
        return isSiteRated;
    }

    public void setSiteRated(Boolean siteRated) {
        isSiteRated = siteRated;
    }

    public List<Inverter> getInverter() {
        return inverter;
    }

    public void setInverter(List<Inverter> inverter) {
        this.inverter = inverter;
    }

    public Battery getBattery() {
        return battery;
    }

    public void setBattery(Battery battery) {
        this.battery = battery;
    }

    public List<String> getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(List<String> imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getPhase() {
        return phase;
    }

    public void setPhase(String phase) {
        this.phase = phase;
    }

    public String getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(String connectionId) {
        this.connectionId = connectionId;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public String getSolutionType() {
        return solutionType;
    }

    public void setSolutionType(String solutionType) {
        this.solutionType = solutionType;
    }

    public String getSiteType() {
        return siteType;
    }

    public void setSiteType(String siteType) {
        this.siteType = siteType;
    }

    public String getSiteSize() {
        return siteSize;
    }

    public void setSiteSize(String siteSize) {
        this.siteSize = siteSize;
    }

    public String getInstalledBy() {
        return installedBy;
    }

    public void setInstalledBy(String installedBy) {
        this.installedBy = installedBy;
    }

    public String getInstalledOn() {
        return installedOn;
    }

    public void setInstalledOn(String installedOn) {
        this.installedOn = installedOn;
    }

    public String getAmcEnd() {
        return amcEnd;
    }

    public void setAmcEnd(String amcEnd) {
        this.amcEnd = amcEnd;
    }

    public String getPanelType() {
        return panelType;
    }

    public void setPanelType(String panelType) {
        this.panelType = panelType;
    }

    public String getPanelSize() {
        return panelSize;
    }

    public void setPanelSize(String panelSize) {
        this.panelSize = panelSize;
    }

    public String getPanelMake() {
        return panelMake;
    }

    public void setPanelMake(String panelMake) {
        this.panelMake = panelMake;
    }

    public class Battery implements Serializable {

        @SerializedName("batterySerial")
        @Expose
        private String batterySerial;
        @SerializedName("batteryCompanyId")
        @Expose
        public String batteryCompanyId;
        @SerializedName("batterySize")
        @Expose
        private String batterySize;
        @SerializedName("batteryMake")
        @Expose
        private String batteryMake;
        @SerializedName("batteryLogo")
        @Expose
        public String batteryLogo;

        public String getBatterySerial() {
            return batterySerial;
        }

        public void setBatterySerial(String batterySerial) {
            this.batterySerial = batterySerial;
        }

        public String getBatterySize() {
            return batterySize;
        }

        public void setBatterySize(String batterySize) {
            this.batterySize = batterySize;
        }

        public String getBatteryMake() {
            return batteryMake;
        }

        public void setBatteryMake(String batteryMake) {
            this.batteryMake = batteryMake;
        }

    }

}
