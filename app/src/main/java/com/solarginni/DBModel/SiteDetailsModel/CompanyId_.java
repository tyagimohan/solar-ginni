
package com.solarginni.DBModel.SiteDetailsModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CompanyId_ {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("userId")
    @Expose
    private Integer userId;
    @SerializedName("companyName")
    @Expose
    private String companyName;
    @SerializedName("companyType")
    @Expose
    private String companyType;
    @SerializedName("industry")
    @Expose
    private String industry;
    @SerializedName("gstNumber")
    @Expose
    private Object gstNumber;
    @SerializedName("images")
    @Expose
    private Object images;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyType() {
        return companyType;
    }

    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public Object getGstNumber() {
        return gstNumber;
    }

    public void setGstNumber(Object gstNumber) {
        this.gstNumber = gstNumber;
    }

    public Object getImages() {
        return images;
    }

    public void setImages(Object images) {
        this.images = images;
    }

}
