
package com.solarginni.DBModel.SiteDetailsModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Inverter implements Serializable {

    @SerializedName("companyId")
    @Expose
    public String companyId;
    @SerializedName("inverterSerial")
    @Expose
    private String inverterSerial;
    @SerializedName("inverterSize")
    @Expose
    private String inverterSize;

    @SerializedName("inverterMake")
    @Expose
    private String inverterMake;
    @SerializedName("inverterLogo")
    @Expose
    public String inverterLogo;

    public String getInverterMake() {
        return inverterMake;
    }

    public String getInverterSerial() {
        return inverterSerial;
    }

    public String getInverterSize() {
        return inverterSize;
    }





}
