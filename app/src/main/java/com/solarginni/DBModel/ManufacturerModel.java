package com.solarginni.DBModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ManufacturerModel implements Serializable {

    @SerializedName("inverterMake")
    @Expose
    public List<CompanyMake> inverterMakeList = new ArrayList<>();
    @SerializedName("panelMake")
    @Expose
    public List<CompanyMake> panelMakeList = new ArrayList<>();
    @SerializedName("batteryMake")
    @Expose
    public List<CompanyMake> batteryMakeList = new ArrayList<>();



    public class CompanyMake implements Serializable {


        @SerializedName("companyId")
        @Expose
        public String companyId;
        @SerializedName("url")
        @Expose
        public String url;
        @SerializedName("type")
        @Expose
        public Integer type;
        @SerializedName("default")
        @Expose
        public Boolean _default;


    }
}
