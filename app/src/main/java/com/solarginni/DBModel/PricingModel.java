package com.solarginni.DBModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PricingModel implements Serializable {


    @SerializedName("price-info")
    @Expose
    private List<PriceInfo> priceInfo = null;

    public List<PriceInfo> getPriceInfo() {
        return priceInfo;
    }

    public void setPriceInfo(List<PriceInfo> priceInfo) {
        this.priceInfo = priceInfo;
    }


    public class PriceInfo implements Serializable {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("month")
        @Expose
        private String month;
        @SerializedName("userId")
        @Expose
        private String userId;
        @SerializedName("siteType")
        @Expose
        private String siteType;
        @SerializedName("price")
        @Expose
        private Integer price;
        @SerializedName("year")
        @Expose
        private Integer year;
        @SerializedName("componentCreated")
        @Expose
        public Boolean componentCreated;
        @SerializedName("district")
        @Expose
        public List<String> districtList = new ArrayList<>();
        @SerializedName("pricingModel")
        @Expose
        public List<String> pricingModel = new ArrayList<>();


        public Integer getRange() {
            return range;
        }

        public void setRange(Integer range) {
            this.range = range;
        }

        @SerializedName("range")
        @Expose
        private Integer range;
        @SerializedName("state")
        @Expose
        private String state;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getMonth() {
            return month;
        }

        public void setMonth(String month) {
            this.month = month;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getSiteType() {
            return siteType;
        }

        public void setSiteType(String siteType) {
            this.siteType = siteType;
        }

        public Integer getPrice() {
            return price;
        }

        public void setPrice(Integer price) {
            this.price = price;
        }

        public Integer getYear() {
            return year;
        }

        public void setYear(Integer year) {
            this.year = year;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

    }
}
