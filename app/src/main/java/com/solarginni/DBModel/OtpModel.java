package com.solarginni.DBModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.solarginni.CommonModule.ImplementerCompanyModel;
import com.solarginni.SiteLovsModule.SiteDetailModel.Role;

import java.io.Serializable;
import java.util.List;

public class OtpModel implements Serializable {

    @SerializedName("user")
    @Expose
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    public class User {
        @SerializedName("verified")
        @Expose
        private Boolean verified;
        @SerializedName("registered")
        @Expose
        private Boolean registered;
        @SerializedName("accessToken")
        @Expose
        private String accessToken;
        @SerializedName("name")
        @Expose
        private String name;

        public ImplementerCompanyModel.Company getCompany() {
            return company;
        }

        public void setCompany(ImplementerCompanyModel.Company company) {
            this.company = company;
        }

        @SerializedName("company")
        @Expose
        private ImplementerCompanyModel.Company company;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUserID() {
            return userID;
        }

        public void setUserID(String userID) {
            this.userID = userID;
        }

        @SerializedName("userId")
        @Expose
        private String userID;
        @SerializedName("role")
        @Expose
        //private List<Role> roles = null;
        private int role;


        public Boolean getVerified() {
            return verified;
        }

        public void setVerified(Boolean verified) {
            this.verified = verified;
        }

        public Boolean getRegistered() {
            return registered;
        }

        public void setRegistered(Boolean registered) {
            this.registered = registered;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public int getRoles() {
            return role;
        }

        public void setRoles(int roles) {
            this.role = roles;
        }
    }

}
