package com.solarginni.DBModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RequestData implements Serializable {


    @SerializedName("request-details")
    @Expose
    private List<RequestDetail> requestDetails = new ArrayList<>();

    public List<RequestDetail> getRequestDetails() {
        return requestDetails;
    }

    public void setRequestDetails(List<RequestDetail> requestDetails) {
        this.requestDetails = requestDetails;
    }

    public static class RequestDetail implements Serializable {
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("requestType")
        @Expose
        private String requestType;
        @SerializedName("userId")
        @Expose
        private String userId;
        @SerializedName("siteId")
        @Expose
        private String siteId;
        @SerializedName("quoteId")
        @Expose
        private String quoteId;
        @SerializedName("requestId")
        @Expose
        private String requestId;
        @SerializedName("raisedOn")
        @Expose
        private String raisedOn;
        @SerializedName("downSince")
        @Expose
        private String downSince;
        @SerializedName("requestStatus")
        @Expose
        private String requestStatus;
        @SerializedName("requestCategory")
        @Expose
        private String requestCategory;

        public String getCompanyId() {
            return companyId;
        }

        @SerializedName("companyId")
        @Expose
        private String companyId;
        @SerializedName("engineer")
        @Expose
        private Integer engineer;
        @SerializedName("resolvedOn")
        @Expose
        private String resolvedOn;

        public String getCustomerPhone() {
            return customerPhone;
        }

        public void setCustomerPhone(String customerPhone) {
            this.customerPhone = customerPhone;
        }

        @SerializedName("customerPhone")
        @Expose
        private String customerPhone;
        @SerializedName("comment")
        @Expose
        private String comment;

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public boolean isRequestRating() {
            return isRequestRating;
        }

        public void setRequestRating(boolean requestRating) {
            isRequestRating = requestRating;
        }

        @SerializedName("requestRating")
        @Expose
        private boolean isRequestRating;

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        @SerializedName("companyName")
        @Expose
        private String companyName;

        public String getAssignTo() {
            return assignTo;
        }

        public void setAssignTo(String assignTo) {
            this.assignTo = assignTo;
        }

        @SerializedName("assignTo")
        @Expose
        private String assignTo;
        @SerializedName("resolutionNotes")
        @Expose
        private String resolutionNotes;
        @SerializedName("closedOn")
        @Expose
        private String closedOn;
        @SerializedName("closureNotes")
        @Expose
        private String closureNotes;
        @SerializedName("responseRating")
        @Expose
        private Integer responseRating;
        @SerializedName("resolveRating")
        @Expose
        private Integer resolveRating;
        @SerializedName("engineerRating")
        @Expose
        private Integer engineerRating;
        @SerializedName("filledOn")
        @Expose
        private Object filledOn;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getRequestType() {
            return requestType;
        }

        public void setRequestType(String requestType) {
            this.requestType = requestType;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getQuoteId() {
            return quoteId;
        }

        public void setQuoteId(String quoteId) {
            this.quoteId = quoteId;
        }

        public String getSiteId() {
            return siteId;
        }

        public void setSiteId(String siteId) {
            this.siteId = siteId;
        }

        public String getRequestId() {
            return requestId;
        }

        public void setRequestId(String requestId) {
            this.requestId = requestId;
        }

        public String getRaisedOn() {
            return raisedOn;
        }

        public void setRaisedOn(String raisedOn) {
            this.raisedOn = raisedOn;
        }

        public String getDownSince() {
            return downSince;
        }

        public void setDownSince(String downSince) {
            this.downSince = downSince;
        }

        public String getRequestStatus() {
            return requestStatus;
        }

        public void setRequestStatus(String requestStatus) {
            this.requestStatus = requestStatus;
        }

        public String getRequestCategory() {
            return requestCategory;
        }

        public void setRequestCategory(String requestCategory) {
            this.requestCategory = requestCategory;
        }

        public Integer getEngineer() {
            return engineer;
        }

        public void setEngineer(Integer engineer) {
            this.engineer = engineer;
        }

        public String getResolvedOn() {
            return resolvedOn;
        }

        public void setResolvedOn(String resolvedOn) {
            this.resolvedOn = resolvedOn;
        }

        public String getResolutionNotes() {
            return resolutionNotes;
        }

        public void setResolutionNotes(String resolutionNotes) {
            this.resolutionNotes = resolutionNotes;
        }

        public String getClosedOn() {
            return closedOn;
        }

        public void setClosedOn(String closedOn) {
            this.closedOn = closedOn;
        }

        public String getClosureNotes() {
            return closureNotes;
        }

        public void setClosureNotes(String closureNotes) {
            this.closureNotes = closureNotes;
        }

        public Integer getResponseRating() {
            return responseRating;
        }

        public void setResponseRating(Integer responseRating) {
            this.responseRating = responseRating;
        }

        public Integer getResolveRating() {
            return resolveRating;
        }

        public void setResolveRating(Integer resolveRating) {
            this.resolveRating = resolveRating;
        }

        public Integer getEngineerRating() {
            return engineerRating;
        }

        public void setEngineerRating(Integer engineerRating) {
            this.engineerRating = engineerRating;
        }

        public Object getFilledOn() {
            return filledOn;
        }

        public void setFilledOn(Object filledOn) {
            this.filledOn = filledOn;
        }


    }


}
