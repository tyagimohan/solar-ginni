package com.solarginni.DBModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class EmpanelMentBody implements Serializable {
    @SerializedName("end")
    @Expose
    private String end;
    @SerializedName("from")
    @Expose
    private String from;
    @SerializedName("nodalBody")
    @Expose
    private String nodalBody;
    @SerializedName("city")
    @Expose
    private String city;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @SerializedName("state")
    @Expose
    private String state;


    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getNodalBody() {
        return nodalBody;
    }

    public void setNodalBody(String nodalBody) {
        this.nodalBody = nodalBody;
    }

}
