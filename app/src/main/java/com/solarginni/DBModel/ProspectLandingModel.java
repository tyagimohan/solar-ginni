package com.solarginni.DBModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ProspectLandingModel implements Serializable {

    @SerializedName("details")
    @Expose
    private LandingDeatails details;

    public LandingDeatails getDetails() {
        return details;
    }

    public void setDetails(LandingDeatails details) {
        this.details = details;
    }

    public static class LandingDeatails {
        @SerializedName("topImplCity")
        @Expose
        private String topImplCity;
        @SerializedName("topImplementerState")
        @Expose
        private String topImplementerState;
        @SerializedName("topImplementerStateUserId")
        @Expose
        private String topImplementerStateUserId;
        @SerializedName("topImplCityUserId")
        @Expose
        private String topImplCityUserId;
        @SerializedName("totalSiteCity")
        @Expose
        private Integer totalSiteCity;
        @SerializedName("totalSiteState")
        @Expose
        private Integer totalSiteState;
        @SerializedName("totalSiteMonth")
        @Expose
        private Integer totalSiteMonth;
        @SerializedName("totalSiteYear")
        @Expose
        private Integer totalSiteYear;
        @SerializedName("notificationCount")
        @Expose
        private Integer notificationCount;
        @SerializedName("testimonialCreated")
        @Expose
        public Boolean testimonialCreated;

        public List<Testimonial> getTestimonialModelList() {
            return testimonialModelList;
        }

        public void setTestimonialModelList(List<Testimonial> testimonialModelList) {
            this.testimonialModelList = testimonialModelList;
        }

        @SerializedName("testimonialDtoList")
        @Expose
        private List<Testimonial> testimonialModelList;
        @SerializedName("advertisement")
        @Expose
        public List<String> advertisementList;

        public String getTopImplementerState() {
            return topImplementerState;
        }

        public void setTopImplementerState(String topImplementerState) {
            this.topImplementerState = topImplementerState;
        }

        public String getTopImplementerStateUserId() {
            return topImplementerStateUserId;
        }

        public void setTopImplementerStateUserId(String topImplementerStateUserId) {
            this.topImplementerStateUserId = topImplementerStateUserId;
        }

        public String getTopImplCityUserId() {
            return topImplCityUserId;
        }

        public void setTopImplCityUserId(String topImplCityUserId) {
            this.topImplCityUserId = topImplCityUserId;
        }

        public Integer getNotificationCount() {
            return notificationCount;
        }

        public void setNotificationCount(Integer notificationCount) {
            this.notificationCount = notificationCount;
        }

        public String getTopImplCity() {
            return topImplCity;
        }

        public void setTopImplCity(String topImplCity) {
            this.topImplCity = topImplCity;
        }

        public Integer getTotalSiteCity() {
            return totalSiteCity;
        }

        public void setTotalSiteCity(Integer totalSiteCity) {
            this.totalSiteCity = totalSiteCity;
        }

        public Integer getTotalSiteState() {
            return totalSiteState;
        }

        public void setTotalSiteState(Integer totalSiteState) {
            this.totalSiteState = totalSiteState;
        }

        public Integer getTotalSiteMonth() {
            return totalSiteMonth;
        }

        public void setTotalSiteMonth(Integer totalSiteMonth) {
            this.totalSiteMonth = totalSiteMonth;
        }

        public Integer getTotalSiteYear() {
            return totalSiteYear;
        }

        public void setTotalSiteYear(Integer totalSiteYear) {
            this.totalSiteYear = totalSiteYear;
        }
    }

}
