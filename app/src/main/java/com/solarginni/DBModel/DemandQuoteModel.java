package com.solarginni.DBModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DemandQuoteModel {


    @SerializedName("impleId")
    @Expose
    private String impleId;
//    @SerializedName("userId")
//    @Expose
//    public String userId;
    @SerializedName("empanel")
    @Expose
    public boolean empanel;
    @SerializedName("impleRating")
    @Expose
    private int impleRating;
    @SerializedName("priceRange")
    @Expose
    private int priceRange;
    @SerializedName("impleSite")
    @Expose
    private int impleSite;
    @SerializedName("siteDist")
    @Expose
    private int siteDist;
    @SerializedName("sorted")
    @Expose
    public int sortedValue;


    public String getImpleId() {
        return impleId;
    }

    public void setImpleId(String impleId) {
        this.impleId = impleId;
    }


    public void setImpleRating(int impleRating) {
        this.impleRating = impleRating;
    }


    public void setPriceRange(int priceRange) {
        this.priceRange = priceRange;
    }

    public void setImpleSite(int impleSite) {
        this.impleSite = impleSite;
    }



    public void setSiteDist(int siteDist) {
        this.siteDist = siteDist;
    }
}
