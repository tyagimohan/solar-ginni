package com.solarginni.DBModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ComparisonViewModel implements Serializable {

    @SerializedName("details")
    @Expose
    public List<ComparisonnDetails> details = null;


    public class ComparisonnDetails implements Serializable {

        @SerializedName("panelType")
        @Expose
        public String panelType;
        @SerializedName("companyName")
        @Expose
        public String companyName;
        @SerializedName("panelMake")
        @Expose
        public String panelMake;
        @SerializedName("inverterSize")
        @Expose
        public String inverterSize;
        @SerializedName("distributionBox")
        @Expose
        public String distributionBox;
        @SerializedName("bidirectionalMeter")
        @Expose
        public String bidirectionalMeter;
        @SerializedName("warrantyTerm")
        @Expose
        public int warrantyTerm;
        @SerializedName("structureType")
        @Expose
        public String structureType;
        @SerializedName("inverterMake")
        @Expose
        public String inverterMake;
        @SerializedName("finalPrice")
        @Expose
        public Integer finalPrice;
        @SerializedName("recmndSiteSize")
        @Expose
        public Double recmndSiteSize;
        @SerializedName("respondDate")
        @Expose
        public Integer respondDate;
        @SerializedName("sites")
        @Expose
        public Integer sites;
        @SerializedName("installedCapacity")
        @Expose
        public Double installedCapacity;

    }

}
