package com.solarginni.DBModel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CompanyMakeModel implements Serializable {

    @SerializedName("companyName")
    private  String companyName;
    @SerializedName("userId")
    private String companyId;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyId() {
        return companyId;
    }

}
