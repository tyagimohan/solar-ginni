package com.solarginni.DBModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.solarginni.SiteLovsModule.SiteDetailModel.Role;

import java.io.Serializable;
import java.util.List;

public class UserRoleModel implements Serializable {


    @SerializedName("my_account")
    @Expose
    private MyAccount myAccount;

    public MyAccount getMyAccount() {
        return myAccount;
    }

    public void setMyAccount(MyAccount myAccount) {
        this.myAccount = myAccount;
    }

    public  static  class MyAccount {
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("company")
        @Expose
        private CompanyId companyId;
        @SerializedName("verified")
        @Expose
        private Boolean verified;
        @SerializedName("registered")
        @Expose
        private Boolean registered;

        @SerializedName("accessToken")
        @Expose
        private String accessToken;
        @SerializedName("country_code")
        @Expose
        private String countryCode;
        @SerializedName("promotionCode")
        @Expose
        private String promotionCode;
        @SerializedName("userId")
        @Expose
        private String userId;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("onBoardDate")
        @Expose
        private String onBoardDate;
        @SerializedName("roles")
        @Expose
        private List<Role> roles = null;
        @SerializedName("address")
        @Expose
        private Address address;
        @SerializedName("imageUrl")
        @Expose
        private String images;
        @SerializedName("ratingActive")
        @Expose
        private Boolean ratingActive;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public CompanyId getCompanyId() {
            return companyId;
        }

        public void setCompanyId(CompanyId companyId) {
            this.companyId = companyId;
        }

        public Boolean getVerified() {
            return verified;
        }

        public void setVerified(Boolean verified) {
            this.verified = verified;
        }

        public Boolean getRegistered() {
            return registered;
        }

        public void setRegistered(Boolean registered) {
            this.registered = registered;
        }

        public String getAccessToken() {
            return accessToken;
        }

        public void setAccessToken(String accessToken) {
            this.accessToken = accessToken;
        }

        public String getCountryCode() {
            return countryCode;
        }

        public void setCountryCode(String countryCode) {
            this.countryCode = countryCode;
        }

        public String getPromotionCode() {
            return promotionCode;
        }

        public void setPromotionCode(String promotionCode) {
            this.promotionCode = promotionCode;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getOnBoardDate() {
            return onBoardDate;
        }

        public void setOnBoardDate(String onBoardDate) {
            this.onBoardDate = onBoardDate;
        }

        public List<Role> getRoles() {
            return roles;
        }

        public void setRoles(List<Role> roles) {
            this.roles = roles;
        }

        public Address getAddress() {
            return address;
        }

        public void setAddress(Address address) {
            this.address = address;
        }

        public String getImages() {
            return images;
        }

        public void setImages(String images) {
            this.images = images;
        }

        public Boolean getRatingActive() {
            return ratingActive;
        }

        public void setRatingActive(Boolean ratingActive) {
            this.ratingActive = ratingActive;
        }

    }


    public static class Address implements Serializable {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("addressLine1")
        @Expose
        private String addressLine1;
        @SerializedName("addressLine2")
        @Expose
        private String addressLine2;
        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("state")
        @Expose
        private String state;
        @SerializedName("pinCode")
        @Expose
        private String pinCode;
        @SerializedName("latitude")
        @Expose
        private Double latitude;
        @SerializedName("longitude")
        @Expose
        private Double longitude;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getAddressLine1() {
            return addressLine1;
        }

        public void setAddressLine1(String addressLine1) {
            this.addressLine1 = addressLine1;
        }

        public String getAddressLine2() {
            return addressLine2;
        }

        public void setAddressLine2(String addressLine2) {
            this.addressLine2 = addressLine2;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getPinCode() {
            return pinCode;
        }

        public void setPinCode(String pinCode) {
            this.pinCode = pinCode;
        }

        public Double getLatitude() {
            return latitude;
        }

        public void setLatitude(Double latitude) {
            this.latitude = latitude;
        }

        public Double getLongitude() {
            return longitude;
        }

        public void setLongitude(Double longitude) {
            this.longitude = longitude;
        }

    }
    public class Images {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("url")
        @Expose
        private String url;
        @SerializedName("type")
        @Expose
        private Integer type;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public Integer getType() {
            return type;
        }

        public void setType(Integer type) {
            this.type = type;
        }

    }

    public class CompanyId implements Serializable{
        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("userId")
        @Expose
        private Object userId;
        @SerializedName("companyName")
        @Expose
        private String companyName;
        @SerializedName("companyType")
        @Expose
        private String companyType;
        @SerializedName("industry")
        @Expose
        private Object industry;
        @SerializedName("gstNumber")
        @Expose
        private Object gstNumber;
        @SerializedName("image")
        @Expose
        private Object image;
        @SerializedName("address")
        @Expose
        private Address address;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Object getUserId() {
            return userId;
        }

        public void setUserId(Object userId) {
            this.userId = userId;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String getCompanyType() {
            return companyType;
        }

        public void setCompanyType(String companyType) {
            this.companyType = companyType;
        }

        public Object getIndustry() {
            return industry;
        }

        public void setIndustry(Object industry) {
            this.industry = industry;
        }

        public Object getGstNumber() {
            return gstNumber;
        }

        public void setGstNumber(Object gstNumber) {
            this.gstNumber = gstNumber;
        }

        public Object getImage() {
            return image;
        }

        public void setImage(Object image) {
            this.image = image;
        }

        public Address getAddress() {
            return address;
        }

        public void setAddress(Address address) {
            this.address = address;
        }
    }


}
