package com.solarginni.DBModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.solarginni.SiteLovsModule.SiteDetailModel.Address;

import java.io.Serializable;
import java.util.List;

public class ImplementerQuoteListModel {
    @SerializedName("quote-info")
    @Expose
    private List<QuoteInfo> quoteInfo = null;

    public List<QuoteInfo> getQuoteInfo() {
        return quoteInfo;
    }

    public void setQuoteInfo(List<QuoteInfo> quoteInfo) {
        this.quoteInfo = quoteInfo;
    }

    public class QuoteInfo implements Serializable {
        @SerializedName("priceRange")
        @Expose
        private Integer priceRange;
        @SerializedName("finalPrice")
        @Expose
        private Integer finalPrice;
        @SerializedName("recmndSiteSize")
        @Expose
        private Double recmndSiteSize;
        @SerializedName("comment")
        @Expose
        private String comment;
        @SerializedName("panelType")
        @Expose
        public String panelType;
        @SerializedName("panelMakeName")
        @Expose
        public String panelMakeName;
        @SerializedName("inverterSize")
        @Expose
        public String inverterSize;
        @SerializedName("inverterMakeName")
        @Expose
        public String inverterMakeName;
        @SerializedName("inverterMake")
        @Expose
        public String inverterMake;
        @SerializedName("batteryMake")
        @Expose
        public String batteryMake;
        @SerializedName("panelMake")
        @Expose
        public String panelMake;
        @SerializedName("batteryMakeName")
        @Expose
        public String batteryMakeName;
        @SerializedName("structureType")
        @Expose
        public String structureType;
        @SerializedName("warrantyTerm")
        @Expose
        public int warrantyTerm;
        @SerializedName("bidirectionalMeter")
        @Expose
        public String bidirectionalMeter;
        @SerializedName("distributionBox")
        @Expose
        public String distributionBox;
        @SerializedName("impleSite")
        @Expose
        private Integer impleSite;
        @SerializedName("impleId")
        @Expose
        private String impleId;
        @SerializedName("impleRating")
        @Expose
        private Integer impleRating;
        @SerializedName("siteDist")
        @Expose
        private Integer siteDist;
        @SerializedName("optId")
        @Expose
        private String optId;
        @SerializedName("quoteId")
        @Expose
        private String quoteId;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("feasibilityCreated")
        @Expose
        public Boolean feasibilityCreated;
        @SerializedName("url")
        @Expose
        public String attachment;

        public String getRespondedDate() {
            return respondedDate;
        }

        public void setRespondedDate(String respondedDate) {
            this.respondedDate = respondedDate;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        @SerializedName("respondedDate")
        @Expose
        private String respondedDate;
        @SerializedName("createdAt")
        @Expose
        private String createdAt;
        @SerializedName("quoteImpDao")
        @Expose
        private QuoteImpData quoteImpData;

        public Integer getPriceRange() {
            return priceRange;
        }

        public void setPriceRange(Integer priceRange) {
            this.priceRange = priceRange;
        }

        public Integer getFinalPrice() {
            return finalPrice;
        }

        public void setFinalPrice(Integer finalPrice) {
            this.finalPrice = finalPrice;
        }

        public Double getRecmndSiteSize() {
            return recmndSiteSize;
        }

        public void setRecmndSiteSize(Double recmndSiteSize) {
            this.recmndSiteSize = recmndSiteSize;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public Integer getImpleSite() {
            return impleSite;
        }

        public void setImpleSite(Integer impleSite) {
            this.impleSite = impleSite;
        }

        public String getImpleId() {
            return impleId;
        }

        public void setImpleId(String impleId) {
            this.impleId = impleId;
        }

        public Integer getImpleRating() {
            return impleRating;
        }

        public void setImpleRating(Integer impleRating) {
            this.impleRating = impleRating;
        }

        public Integer getSiteDist() {
            return siteDist;
        }

        public void setSiteDist(Integer siteDist) {
            this.siteDist = siteDist;
        }

        public String getOptId() {
            return optId;
        }

        public void setOptId(String optId) {
            this.optId = optId;
        }

        public String getQuoteId() {
            return quoteId;
        }

        public void setQuoteId(String quoteId) {
            this.quoteId = quoteId;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public QuoteImpData getQuoteImpData() {
            return quoteImpData;
        }

        public void setQuoteImpData(QuoteImpData quoteImpData) {
            this.quoteImpData = quoteImpData;
        }


    }

    public class QuoteImpData implements Serializable {

        @SerializedName("userId")
        @Expose
        private String userId;
        @SerializedName("optyId")
        @Expose
        private String optyId;
        @SerializedName("solarSize")
        @Expose
        private String solarSize;
        @SerializedName("serviceLevel")
        @Expose
        private Integer serviceLevel;
        @SerializedName("paymentTerm")
        @Expose
        private Integer paymentTerm;
        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("address")
        @Expose
        private Address address;

        public Address getAddress() {
            return address;
        }

        public String getPinCode() {
            return pinCode;
        }

        public void setPinCode(String pinCode) {
            this.pinCode = pinCode;
        }

        @SerializedName("pinCode")
        @Expose
        private String pinCode;
        @SerializedName("state")
        @Expose
        private String state;
        @SerializedName("lovModel")
        @Expose
        private QuoteListModel.LOVModel lovModel;

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getOptyId() {
            return optyId;
        }

        public void setOptyId(String optyId) {
            this.optyId = optyId;
        }

        public String getSolarSize() {
            return solarSize;
        }

        public void setSolarSize(String solarSize) {
            this.solarSize = solarSize;
        }

        public Integer getServiceLevel() {
            return serviceLevel;
        }

        public void setServiceLevel(Integer serviceLevel) {
            this.serviceLevel = serviceLevel;
        }

        public Integer getPaymentTerm() {
            return paymentTerm;
        }

        public void setPaymentTerm(Integer paymentTerm) {
            this.paymentTerm = paymentTerm;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public QuoteListModel.LOVModel getLovModel() {
            return lovModel;
        }

        public void setLovModel(QuoteListModel.LOVModel lovModel) {
            this.lovModel = lovModel;
        }

    }
}
