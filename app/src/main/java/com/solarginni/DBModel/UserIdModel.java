package com.solarginni.DBModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserIdModel implements Serializable {

    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("role")
    @Expose
    public String role;

    public Boolean getSiteExits() {
        return isSiteExits;
    }

    public void setSiteExits(Boolean siteExits) {
        isSiteExits = siteExits;
    }

    @SerializedName("isSiteExists")
    @Expose
    private Boolean isSiteExits;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
