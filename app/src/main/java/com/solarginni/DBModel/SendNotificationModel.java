package com.solarginni.DBModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SendNotificationModel {

    @SerializedName("code")
    @Expose
    public String code;
    @SerializedName("companyId")
    @Expose
    public List<String> companyId = null;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("notificationType")
    @Expose
    public Integer notificationType;
    @SerializedName("role")
    @Expose
    public Integer role;
    @SerializedName("userId")
    @Expose
    public String userId;
}
