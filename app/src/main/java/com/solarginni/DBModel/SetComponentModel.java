package com.solarginni.DBModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SetComponentModel implements Serializable {
    @SerializedName("acCableBrand")
    @Expose
    public String acCableBrand;
    @SerializedName("acCableLength")
    @Expose
    public String acCableLength;
    @SerializedName("batteryMake")
    @Expose
    public String batteryMake;
    @SerializedName("bidirectionalMeter")
    @Expose
    public String bidirectionalMeter;
    @SerializedName("dcCableBrand")
    @Expose
    public String dcCableBrand;
    @SerializedName("dcCableLength")
    @Expose
    public String dcCableLength;
    @SerializedName("earthingKit")
    @Expose
    public String earthingKit;
    @SerializedName("inverterMake")
    @Expose
    public String inverterMake;
    @SerializedName("irradianceSensor")
    @Expose
    public Boolean irradianceSensor;
    @SerializedName("lightingArrestor")
    @Expose
    public String lightingArrestor;
    @SerializedName("panelMake")
    @Expose
    public String panelMake;
    @SerializedName("panelType")
    @Expose
    public String panelType;
    @SerializedName("priceId")
    @Expose
    public Integer priceId;
    @SerializedName("remark")
    @Expose
    public String remark;
    @SerializedName("serviceWarranty")
    @Expose
    public Integer serviceWarranty;
    @SerializedName("structureType")
    @Expose
    public String structureType;
    @SerializedName("zeroTouch")
    @Expose
    public Boolean zeroTouch;
    @SerializedName("washingSystem")
    @Expose
    public Boolean washingSystem;
    @SerializedName("monitoring")
    @Expose
    public Boolean monitoring;
    @SerializedName("distributionBox")
    @Expose
    public String distributionBox;
}
