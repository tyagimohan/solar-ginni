package com.solarginni.DBModel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OportunityResponse implements Serializable {


    @SerializedName("otpId")
    public String optId = "";
    @SerializedName("solution")
    public String solution = "";


}
