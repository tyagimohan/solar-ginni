package com.solarginni.DBModel.APIBody;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SendOtp implements Serializable {

    public SendOtp(String phone, String deviceId) {
        this.phone = phone;
        this.deviceId = deviceId;
    }

    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("deviceId")
    @Expose
    private String deviceId;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
