package com.solarginni.DBModel.APIBody;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ImplementerModel implements Serializable {

    @SerializedName("ad_line1")
    @Expose
    private String adLine1;
    @SerializedName("ad_line2")
    @Expose
    private String adLine2;
    @SerializedName("companyId")
    @Expose
    private String companyId;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("country_code")
    @Expose
    private String countryCode;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("latitude")
    @Expose
    private String latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("pinCode")
    @Expose
    private String pinCode;
    @SerializedName("promotionCode")
    @Expose
    private String promotionCode;
    @SerializedName("profileImage")
    @Expose
    public String profileImage;
    @SerializedName("roles")
    @Expose
    private Integer roles;
    @SerializedName("state")
    @Expose
    private String state;

    public ImplementerModel(String adLine1, String adLine2, String city, String country, String countryCode, String email, String latitude, String longitude, String name, String pinCode, String promotionCode, Integer roles, String state, String companyName) {
        this.adLine1 = adLine1;
        this.adLine2 = adLine2;
        this.city = city;
        this.country = country;
        this.countryCode = countryCode;
        this.email = email;
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
        this.pinCode = pinCode;
        this.promotionCode = promotionCode;
        this.roles = roles;
        this.state = state;
        this.companyId = companyName;
    }
}
