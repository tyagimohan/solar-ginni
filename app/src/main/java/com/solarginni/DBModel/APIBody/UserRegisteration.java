package com.solarginni.DBModel.APIBody;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserRegisteration implements Serializable {

    @SerializedName("ad_line1")
    @Expose
    public
    String adLine1;
    @SerializedName("ad_line2")
    @Expose
    public
    String adLine2;
    @SerializedName("city")
    @Expose
    public
    String city;
    @SerializedName("country")
    @Expose
    public
    String country;
    @SerializedName("country_code")
    @Expose
    public
    String countryCode;
    @SerializedName("email")
    @Expose
    public
    String email;
    @SerializedName("latitude")
    @Expose
    public
    String latitude;
    @SerializedName("longitude")
    @Expose
    public
    String longitude;
    @SerializedName("name")
    @Expose
    public
    String name;
    @SerializedName("phone")
    @Expose
    public
    String phone;
    @SerializedName("pinCode")
    @Expose
    public
    String pinCode;
    @SerializedName("promotionCode")
    @Expose
    public
    String promotionCode;
    @SerializedName("profileImage")
    @Expose
    public String profileImage;
    @SerializedName("roles")
    @Expose
    public
    Integer roles;
    @SerializedName("state")
    @Expose
    public
    String state;



    public void setAdLine1(String adLine1) {
        this.adLine1 = adLine1;
    }

    public void setAdLine2(String adLine2) {
        this.adLine2 = adLine2;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public void setPromotionCode(String promotionCode) {
        this.promotionCode = promotionCode;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public void setRoles(Integer roles) {
        this.roles = roles;
    }

    public void setState(String state) {
        this.state = state;
    }


    public UserRegisteration() {
    }

    public UserRegisteration(String adLine1, String adLine2, String city, String country, String countryCode, String email, String latitude, String longitude, String name, String pinCode, String promotionCode, Integer roles, String state) {
        this.adLine1 = adLine1;
        this.adLine2 = adLine2;
        this.city = city;
        this.country = country;
        this.countryCode = countryCode;
        this.email = email;
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
        this.pinCode = pinCode;
        this.promotionCode = promotionCode;
        this.roles = roles;
        this.state = state;
        this.phone = state;
    }

    public UserRegisteration(String name, String adLine1, String adLine2, String city, String latitude, String longitude, String pinCode, String state, String email) {
        this.adLine1 = adLine1;
        this.name = name;
        this.adLine2 = adLine2;
        this.city = city;
        this.latitude = latitude;
        this.longitude = longitude;
        this.pinCode = pinCode;
        this.email = email;
        this.profileImage = profileImage;
        this.state = state;
    }
}
