package com.solarginni.DBModel.APIBody;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RequestParam implements Serializable {

    @SerializedName("engineerRating")
    private int engineerRating;
    @SerializedName("resolveRating")
    private int resolveRating;
    @SerializedName("responseRating")
    private int responseRating;

    public int getEngineerRating() {
        return engineerRating;
    }

    public void setEngineerRating(int engineerRating) {
        this.engineerRating = engineerRating;
    }

    public int getResolveRating() {
        return resolveRating;
    }

    public void setResolveRating(int resolveRating) {
        this.resolveRating = resolveRating;
    }

    public int getResponseRating() {
        return responseRating;
    }

    public void setResponseRating(int responseRating) {
        this.responseRating = responseRating;
    }


}
