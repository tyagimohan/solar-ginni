package com.solarginni.DBModel.APIBody;

public enum ImageType {
    USER_TYPE("profile-image"),
    SITE_TYPE("site-image"),
    COMPANY_TYPE("company-image");
    private final String keyName;

    ImageType(String name) {
        this.keyName = name;
    }

    public String getKeyName() {
        return this.keyName;
    }
}
