package com.solarginni.DBModel.APIBody;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.solarginni.SiteLovsModule.SiteDetailModel.Address;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SiteRegistrationModel implements Serializable {


    @SerializedName("status")
    @Expose
    public boolean status;
    @SerializedName("amctill")
    @Expose
    private String amctill;
    @SerializedName("connectionId")
    @Expose
    private String connectionId;
    @SerializedName("installedBy")
    @Expose
    private String installedBy;
    @SerializedName("installedOn")
    @Expose
    private String installedOn;
    @SerializedName("inverterModelList")
    @Expose
    private List<InverterModelList> inverterModelList = new ArrayList<>();
    @SerializedName("panelMake")
    @Expose
    private String panelMake;
    @SerializedName("same")
    @Expose
    private Boolean isAddresSame;
    @SerializedName("panelSize")
    @Expose
    private String panelSize;
    @SerializedName("panelType")
    @Expose
    private String panelType;
    @SerializedName("phase")
    @Expose
    private String phase;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("siteSize")
    @Expose
    private String siteSize;
    @SerializedName("siteType")
    @Expose
    private String siteType;
    @SerializedName("solution")
    @Expose
    private String solution;
    @SerializedName("batterySerial")
    @Expose
    private String batterySerial;
    @SerializedName("batterySize")
    @Expose
    private String batterySize;
    @SerializedName("batteryCompanyId")
    @Expose
    private String batteryCompanyId;
    @SerializedName("address")
    @Expose
    private Address address;
    @SerializedName("imageUrl")
    @Expose
    private String imageUrl;
    public SiteRegistrationModel() {
    }

    public List<InverterModelList> getInverterModelList() {
        return inverterModelList;
    }

    public void setInverterModelList(List<InverterModelList> inverterModelList) {
        this.inverterModelList = inverterModelList;
    }

    public String getPanelMake() {
        return panelMake;
    }

    public void setPanelMake(String panelMake) {
        this.panelMake = panelMake;
    }

    public Boolean getAddresSame() {
        return isAddresSame;
    }

    public void setAddresSame(Boolean addresSame) {
        isAddresSame = addresSame;
    }

    public String getBatterySerial() {
        return batterySerial;
    }

    public void setBatterySerial(String batterySerial) {
        this.batterySerial = batterySerial;
    }

    public String getBatterySize() {
        return batterySize;
    }

    public void setBatterySize(String batterySize) {
        this.batterySize = batterySize;
    }

    public String getBatteryCompanyId() {
        return batteryCompanyId;
    }

    public void setBatteryCompanyId(String batteryCompanyId) {
        this.batteryCompanyId = batteryCompanyId;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getAmctill() {
        return amctill;
    }

    public void setAmctill(String amctill) {
        this.amctill = amctill;
    }

    public String getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(String connectionId) {
        this.connectionId = connectionId;
    }

    public String getInstalledBy() {
        return installedBy;
    }

    public void setInstalledBy(String installedBy) {
        this.installedBy = installedBy;
    }

    public String getInstalledOn() {
        return installedOn;
    }

    public void setInstalledOn(String installedOn) {
        this.installedOn = installedOn;
    }


    public String getPanelSize() {
        return panelSize;
    }

    public void setPanelSize(String panelSize) {
        this.panelSize = panelSize;
    }

    public String getPanelType() {
        return panelType;
    }

    public void setPanelType(String panelType) {
        this.panelType = panelType;
    }

    public String getPhase() {
        return phase;
    }

    public void setPhase(String phase) {
        this.phase = phase;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSiteSize() {
        return siteSize;
    }

    public void setSiteSize(String siteSize) {
        this.siteSize = siteSize;
    }

    public String getSiteType() {
        return siteType;
    }

    public void setSiteType(String siteType) {
        this.siteType = siteType;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }


}
