package com.solarginni.DBModel.APIBody;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.solarginni.SiteLovsModule.SiteDetailModel.Address;

import java.io.Serializable;

public class GuestModel implements Serializable {

    @SerializedName("address")
    @Expose
    public Address address;
    @SerializedName("deviceId")
    @Expose
    public String deviceId;

}
