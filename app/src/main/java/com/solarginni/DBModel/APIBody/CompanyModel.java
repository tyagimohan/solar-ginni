package com.solarginni.DBModel.APIBody;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.solarginni.DBModel.UserRoleModel;

import java.io.Serializable;

public class CompanyModel implements Serializable {
    @SerializedName("address")
    @Expose
    public UserRoleModel.Address address;
    @SerializedName("companyName")
    @Expose
    public String companyName;
    @SerializedName("companyType")
    @Expose
    public String companyType;
    @SerializedName("gstNumber")
    @Expose
    public String gstNumber;
    @SerializedName("imageUrl")
    @Expose
    public String imageUrl;
    @SerializedName("indian")
    @Expose
    public Boolean indian;
    @SerializedName("industry")
    @Expose
    public String industry;
    @SerializedName("mailId")
    @Expose
    public String mailId;
    @SerializedName("spocName")
    @Expose
    public String spocName;
    @SerializedName("spocTelephone")
    @Expose
    public String spocTelephone;
    @SerializedName("videoUrl")
    @Expose
    public String videoUrl;
    @SerializedName("websiteUrl")
    @Expose
    public String websiteUrl;

    public CompanyModel() {
    }
}
