package com.solarginni.DBModel.APIBody;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class InverterModelList implements Serializable {

    public InverterModelList() {
    }

    @SerializedName("inverterMake")
    @Expose
    private String inverterMake;
    @SerializedName("inverterModel")
    @Expose
    private String inverterModel;
    @SerializedName("inverterSize")
    @Expose
    private String inverterSize;

    public String getInverterMake() {
        return inverterMake;
    }

    public void setInverterMake(String inverterMake) {
        this.inverterMake = inverterMake;
    }

    public String getInverterModel() {
        return inverterModel;
    }

    public void setInverterModel(String inverterModel) {
        this.inverterModel = inverterModel;
    }

    public String getInverterSize() {
        return inverterSize;
    }

    public void setInverterSize(String inverterSize) {
        this.inverterSize = inverterSize;
    }
}
