package com.solarginni.DBModel.APIBody;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.solarginni.CustomizeView.districtSearchDialog.DistrictModel;

import java.util.List;

public class DistrictStateModel {



    @SerializedName("state_lov")
    @Expose
    public List<StateLov> stateLov = null;


    public class StateLov {

        @SerializedName("state")
        @Expose
        public String state;
        @SerializedName("districts")
        @Expose
        public List<String> districts = null;

    }


}



