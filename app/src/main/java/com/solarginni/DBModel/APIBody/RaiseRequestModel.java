package com.solarginni.DBModel.APIBody;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RaiseRequestModel {


    @SerializedName("amcCovered")
    @Expose
    String amcCovered;
    @SerializedName("downSince")
    @Expose
    String downSince;
    @SerializedName("raisedOn")
    @Expose
    String raisedOn;
    @SerializedName("requestCategory")
    @Expose
    String requestCategory;
    @SerializedName("requestType")
    @Expose
    String requestType;
    @SerializedName("comment")
    @Expose
    String comment;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getAmcCovered() {
        return amcCovered;
    }

    public void setAmcCovered(String amcCovered) {
        this.amcCovered = amcCovered;
    }

    public String getDownSince() {
        return downSince;
    }

    public void setDownSince(String downSince) {
        this.downSince = downSince;
    }

    public String getRaisedOn() {
        return raisedOn;
    }

    public void setRaisedOn(String raisedOn) {
        this.raisedOn = raisedOn;
    }

    public String getRequestCategory() {
        return requestCategory;
    }

    public void setRequestCategory(String requestCategory) {
        this.requestCategory = requestCategory;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }
}
