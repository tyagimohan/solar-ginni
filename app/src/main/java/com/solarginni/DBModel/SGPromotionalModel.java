package com.solarginni.DBModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SGPromotionalModel implements Serializable {

    @SerializedName("referredCount")
    @Expose
    public Integer referredCount;
    @SerializedName("earned")
    @Expose
    public Integer earned;
    @SerializedName("dispursed")
    @Expose
    public Integer dispursed;
    @SerializedName("topRanker")
    @Expose
    public String topRanker;
    @SerializedName("sgPromotionModelList")
    @Expose
    public List<SgPromotionModelList> sgPromotionModelList = new ArrayList<>();


    public class SgPromotionModelList {

        @SerializedName("phone")
        @Expose
        public String phone;
        @SerializedName("registerDate")
        @Expose
        public String registerDate;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("state")
        @Expose
        public String state;
        @SerializedName("status")
        @Expose
        public String status;
        @SerializedName("amount")
        @Expose
        public Integer amount;
        @SerializedName("referredBy")
        @Expose
        public String referredBy;

    }

}
