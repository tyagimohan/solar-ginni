package com.solarginni.DBModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.solarginni.SiteLovsModule.SiteDetailModel.Address;

import java.io.Serializable;

public class QuotationModel implements Serializable {

    @SerializedName("solarSize")
    @Expose
    public Double solarSize;
    @SerializedName("serviceLevel")
    @Expose
    public String serviceLevel;
    @SerializedName("paymentTerm")
    @Expose
    public String paymentTerm;
    @SerializedName("projectType")
    @Expose
    public String projectType;
    @SerializedName("userId")
    @Expose
    public String userId;


    @SerializedName("phase")
    @Expose
    public String phase;
    @SerializedName("siteType")
    @Expose
    public String siteType;
    @SerializedName("roofStyle")
    @Expose
    public String roofStyle;
    @SerializedName("solution")
    @Expose
    public String solution;
    @SerializedName("panelMake")
    @Expose
    public String panelMake;
    @SerializedName("panelType")
    @Expose
    public String panelType;
    @SerializedName("inverterMake")
    @Expose
    public String inverterMake;
    @SerializedName("batteryMake")
    @Expose
    public String batteryMake;
    @SerializedName("same")
    @Expose
    public Boolean isAddresSame;

    @SerializedName("address")
    @Expose
    private Address address;

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public QuotationModel() {
    }




    public String getServiceLevel() {
        return serviceLevel;
    }

    public void setServiceLevel(String serviceLevel) {
        this.serviceLevel = serviceLevel;
    }

    public String getPaymentTerm() {
        return paymentTerm;
    }

    public void setPaymentTerm(String paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    public String getPhase() {
        return phase;
    }

    public void setPhase(String phase) {
        this.phase = phase;
    }

    public String getSiteType() {
        return siteType;
    }

    public void setSiteType(String siteType) {
        this.siteType = siteType;
    }

    public String getRoofStyle() {
        return roofStyle;
    }

    public void setRoofStyle(String roofStyle) {
        this.roofStyle = roofStyle;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public String getPanelMake() {
        return panelMake;
    }

    public void setPanelMake(String panelMake) {
        this.panelMake = panelMake;
    }

    public String getInverterMake() {
        return inverterMake;
    }

    public void setInverterMake(String inverterMake) {
        this.inverterMake = inverterMake;
    }

    public String getBatteryMake() {
        return batteryMake;
    }

    public void setBatteryMake(String batteryMake) {
        this.batteryMake = batteryMake;
    }
}
