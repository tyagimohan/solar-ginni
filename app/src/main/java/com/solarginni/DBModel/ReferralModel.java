package com.solarginni.DBModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ReferralModel implements Serializable {

    @SerializedName("referContact")
    @Expose
    public Integer referContact;
    @SerializedName("rank")
    @Expose
    public Integer rank;
    @SerializedName("earned")
    @Expose
    public Integer earned;
    @SerializedName("dispursed")
    @Expose
    public Integer dispursed;
    @SerializedName("referralModelList")
    @Expose
    public List<ReferralModelList> referralModelList = null;



    public class ReferralModelList implements Serializable{

        @SerializedName("phone")
        @Expose
        public String phone;
        @SerializedName("registerDate")
        @Expose
        public String registerDate;
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("state")
        @Expose
        public String state;
        @SerializedName("status")
        @Expose
        public String status;
        @SerializedName("amount")
        @Expose
        public Integer amount;

    }


}
