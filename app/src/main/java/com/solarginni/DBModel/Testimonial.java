package com.solarginni.DBModel;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Testimonial implements Serializable {
    @SerializedName("comment")
    public String comment;
    @SerializedName("name")
    public String name;
    @SerializedName("imageUrl")
    public String image;

}
