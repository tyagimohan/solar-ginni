package com.solarginni.DBModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class PriceComponentModel implements Serializable {

    @SerializedName("price-component-details")
    @Expose
    public List<PriceComponentDetail> priceComponentDetails = null;
    @SerializedName("price-component-detail")
    @Expose
    public PriceComponentDetail priceComponentDetail;
    public class PriceComponentDetail implements  Serializable{

        @SerializedName("panelType")
        @Expose
        public String panelType;
        @SerializedName("state")
        @Expose
        public String state;
        @SerializedName("siteType")
        @Expose
        public String siteType;
        @SerializedName("panelMake")
        @Expose
        public String panelMake;
        @SerializedName("panelCompanyName")
        @Expose
        public String panelCompanyName;
        @SerializedName("inverterMake")
        @Expose
        public String inverterMake;
        @SerializedName("inverterCompanyName")
        @Expose
        public String inverterCompanyName;
        @SerializedName("batteryMake")
        @Expose
        public String batteryMake;
        @SerializedName("batteryCompanyName")
        @Expose
        public String batteryCompanyName;
        @SerializedName("structureType")
        @Expose
        public String structureType;
        @SerializedName("priceId")
        @Expose
        public Integer priceId;
        @SerializedName("panelLogo")
        @Expose
        public String panelLogo;
        @SerializedName("inverterLogo")
        @Expose
        public String inverterLogo;
        @SerializedName("batteryLogo")
        @Expose
        public String batteryLogo;
        @SerializedName("acCableBrand")
        @Expose
        public String acCableBrand;
        @SerializedName("acCableLength")
        @Expose
        public String acCableLength;
        @SerializedName("dcCableBrand")
        @Expose
        public String dcCableBrand;
        @SerializedName("dcCableLength")
        @Expose
        public String dcCableLength;
        @SerializedName("earthingKit")
        @Expose
        public String earthingKit;
        @SerializedName("lightingArrestor")
        @Expose
        public String lightingArrestor;
        @SerializedName("bidirectionalMeter")
        @Expose
        public String bidirectionalMeter;
        @SerializedName("irradianceSensor")
        @Expose
        public Boolean irradianceSensor;
        @SerializedName("serviceWarranty")
        @Expose
        public Integer serviceWarranty;
        @SerializedName("remark")
        @Expose
        public String remark;
        @SerializedName("zeroTouch")
        @Expose
        public Boolean zeroTouch;
        @SerializedName("monitoring")
        @Expose
        public Boolean monitoring;
        @SerializedName("distributionBox")
        @Expose
        public String distributionBox;
        @SerializedName("washingSystem")
        @Expose
        public Boolean washingSystem;

    }

}
