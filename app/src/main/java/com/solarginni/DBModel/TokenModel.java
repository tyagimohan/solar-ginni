package com.solarginni.DBModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TokenModel implements Serializable {
    @SerializedName("token")
    @Expose
    private String token;

    public String getOtp() {
        return otp;
    }

    @SerializedName("otp")
    @Expose
    private String otp;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


}
