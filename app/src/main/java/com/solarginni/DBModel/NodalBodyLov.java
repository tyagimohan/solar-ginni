package com.solarginni.DBModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.solarginni.SiteLovsModule.SiteDetailModel.LovValues;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class NodalBodyLov implements Serializable {


    @SerializedName("lov")
    @Expose
    public List<Lov> lov = new ArrayList<>();

    public class Lov implements Serializable {

        @SerializedName("city")
        @Expose
        private List<String> city = null;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("state")
        @Expose
        private String state;

        public List<String> getCity() {
            return city;
        }

        public void setCity(List<String> city) {
            this.city = city;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

    }

    public class Value implements Serializable {

        @SerializedName("city")
        @Expose
        private List<String> city = null;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("state")
        @Expose
        private String state;

        public List<String> getCity() {
            return city;
        }

        public void setCity(List<String> city) {
            this.city = city;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

    }
}
