package com.solarginni.DBModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.solarginni.SiteLovsModule.SiteDetailModel.Address;

import java.io.Serializable;
import java.util.List;

public class QuoteListModel implements Serializable {

    @SerializedName("details")
    @Expose
    private List<QuoteList> details = null;

    public List<QuoteList> getDetails() {
        return details;
    }

    public void setDetails(List<QuoteList> details) {
        this.details = details;
    }

    public class QuoteList implements Serializable {


        @SerializedName("userId")
        @Expose
        private String userId;
        @SerializedName("optyId")
        @Expose
        private String optyId;
        @SerializedName("solarSize")
        @Expose
        private String solarSize;
        @SerializedName("serviceLevel")
        @Expose
        private Integer serviceLevel;
        @SerializedName("paymentTerm")
        @Expose
        private Integer paymentTerm;
        @SerializedName("state")
        @Expose
        private String state;
        @SerializedName("city")
        @Expose
        private String city;


        @SerializedName("address")
        @Expose
        private Address address;

        public Address getAddress() {
            return address;
        }

        public void setAddress(Address address) {
            this.address = address;
        }

        public List<DemandQuote> getDemandQuotes() {
            return demandQuotes;
        }

        @SerializedName("pinCode")
        @Expose
        private String pinCode;
        @SerializedName("demandQuotes")
        @Expose
        private List<DemandQuote> demandQuotes = null;
        @SerializedName("lovModel")
        @Expose
        private LOVModel lovModel;
        @SerializedName("quoted")
        @Expose
        private Boolean isQuote;
        @SerializedName("feasibilityCreated")
        @Expose
        public Boolean feasibilityCreated;


        public String getPinCode() {
            return pinCode;
        }


        public Boolean getQuote() {
            return isQuote;
        }

        public void setQuote(Boolean quote) {
            isQuote = quote;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }


        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getOptyId() {
            return optyId;
        }


        public String getSolarSize() {
            return solarSize;
        }


        public Integer getServiceLevel() {
            return serviceLevel;
        }


        public Integer getPaymentTerm() {
            return paymentTerm;
        }


        public LOVModel getLovModel() {
            return lovModel;
        }


    }

    public class DemandQuote implements Serializable {


        @SerializedName("priceRange")
        @Expose
        private Integer priceRange;
        @SerializedName("impleSite")
        @Expose
        private Integer impleSite;
        @SerializedName("impleId")
        @Expose
        private String impleId;
        @SerializedName("impleRating")
        @Expose
        private Integer impleRating;
        @SerializedName("siteDist")
        @Expose
        private Integer siteDist;
        @SerializedName("optId")
        @Expose
        private String optId;
        @SerializedName("finalPrice")
        @Expose
        private Integer finalPrice;
        @SerializedName("quoteId")
        @Expose
        private String quoteId;
        @SerializedName("recmndSiteSize")
        @Expose
        private Double recmndSiteSize;
        @SerializedName("panelType")
        @Expose
        public String panelType;
        @SerializedName("structureType")
        @Expose
        public String structureType;
        @SerializedName("warrantyTerm")
        @Expose
        public int warrantyTerm;
        @SerializedName("bidirectionalMeter")
        @Expose
        public String bidirectionalMeter;
        @SerializedName("distributionBox")
        @Expose
        public String distributionBox;
        @SerializedName("panelMake")
        @Expose
        public String panelMake;
        @SerializedName("inverterSize")
        @Expose
        public String inverterSize;
        @SerializedName("inverterMake")
        @Expose
        public String inverterMake;
        @SerializedName("comment")
        @Expose
        private String comment;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("companyName")
        @Expose
        private String companyName;
        @SerializedName("createdAt")
        @Expose
        private String createdAt;
        @SerializedName("respondedDate")
        @Expose
        private String respondedDate;
        @SerializedName("url")
        @Expose
        public String attachment;
        @SerializedName("sorted")
        @Expose
        public int sorted;


        public String getCreatedAt() {
            return createdAt;
        }


        public String getRespondedDate() {
            return respondedDate;
        }


        public Integer getFinalPrice() {
            return finalPrice;
        }


        public String getComment() {
            return comment;
        }


        public Double getRecmndSiteSize() {
            return recmndSiteSize;
        }

        public void setRecmndSiteSize(Double recmndSiteSize) {
            this.recmndSiteSize = recmndSiteSize;
        }


        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public Integer getPriceRange() {
            return priceRange;
        }


        public Integer getImpleSite() {
            return impleSite;
        }


        public String getImpleId() {
            return impleId;
        }


        public Integer getImpleRating() {
            return impleRating;
        }


        public Integer getSiteDist() {
            return siteDist;
        }


        public String getOptId() {
            return optId;
        }

        public String getQuoteId() {
            return quoteId;
        }


        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

    }

    public class LOVModel implements Serializable {

        @SerializedName("phase")
        @Expose
        private String phase;
        @SerializedName("siteType")
        @Expose
        private String siteType;
        @SerializedName("roofStyle")
        @Expose
        private String roofStyle;
        @SerializedName("solution")
        @Expose
        private String solution;
        @SerializedName("panelMake")
        @Expose
        private String panelMake;
        @SerializedName("inverterMake")
        @Expose
        private String inverterMake;
        @SerializedName("batteryMake")
        @Expose
        private String batteryMake;
        @SerializedName("panelName")
        @Expose
        private String panelName;
        @SerializedName("inverterName")
        @Expose
        private String inverterName;
        @SerializedName("panelType")
        @Expose
        public String panelType;
        @SerializedName("batteryName")
        @Expose
        private String batteryName;

        public String getPanelName() {
            return panelName;
        }

        public void setPanelName(String panelName) {
            this.panelName = panelName;
        }

        public String getInverterName() {
            return inverterName;
        }

        public void setInverterName(String inverterName) {
            this.inverterName = inverterName;
        }

        public String getBatteryName() {
            return batteryName;
        }

        public void setBatteryName(String batteryName) {
            this.batteryName = batteryName;
        }

        public String getPhase() {
            return phase;
        }

        public void setPhase(String phase) {
            this.phase = phase;
        }

        public String getSiteType() {
            return siteType;
        }

        public void setSiteType(String siteType) {
            this.siteType = siteType;
        }

        public String getRoofStyle() {
            return roofStyle;
        }

        public void setRoofStyle(String roofStyle) {
            this.roofStyle = roofStyle;
        }

        public String getSolution() {
            return solution;
        }

        public void setSolution(String solution) {
            this.solution = solution;
        }

        public String getPanelMake() {
            return panelMake;
        }

        public void setPanelMake(String panelMake) {
            this.panelMake = panelMake;
        }

        public String getInverterMake() {
            return inverterMake;
        }

        public void setInverterMake(String inverterMake) {
            this.inverterMake = inverterMake;
        }

        public String getBatteryMake() {
            return batteryMake;
        }

        public void setBatteryMake(String batteryMake) {
            this.batteryMake = batteryMake;

        }
    }

}
