package com.solarginni.DBModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SetPriceModel implements Serializable {
    @SerializedName("district")
    @Expose
    public List<String> district = new ArrayList<>();
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("siteType")
    @Expose
    private String siteType;
    @SerializedName("month")
    @Expose
    private String month;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("year")
    @Expose
    private String year;
    @SerializedName("pricingModel")
    @Expose
    public List<String> pricingModel = new ArrayList<>();

    public int getSiteRange() {
        return siteRange;
    }

    public void setSiteRange(int siteRange) {
        this.siteRange = siteRange;
    }

    @SerializedName("range")
    @Expose
    private int siteRange;


    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSiteType() {
        return siteType;
    }

    public void setSiteType(String siteType) {
        this.siteType = siteType;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
