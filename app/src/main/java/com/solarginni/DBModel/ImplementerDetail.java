package com.solarginni.DBModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.solarginni.SiteLovsModule.SiteDetailModel.Address;

import java.io.Serializable;

public class ImplementerDetail implements Serializable {

    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("companyName")
    @Expose
    private String companyName;
    @SerializedName("address")
    @Expose
    private Address address;
    @SerializedName("totalCapacity")
    @Expose
    private String totalCapacity;
    @SerializedName("totalSite")
    @Expose
    private Integer totalSite;
    @SerializedName("stateCount")
    @Expose
    public Integer stateCount;

    @SerializedName("firstUserName")
    @Expose
    private String firstUserName;
    @SerializedName("firstUserPhone")
    @Expose
    private String firstUserPhone;

    public String getUrl() {
        return url;
    }

    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("activate")
    @Expose
    public boolean activate;

    @SerializedName("national")
    @Expose
    public PieChartModel nationalModel;
    @SerializedName("state")
    @Expose
    public PieChartModel stateModel;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getTotalCapacity() {
        return totalCapacity;
    }

    public void setTotalCapacity(String totalCapacity) {
        this.totalCapacity = totalCapacity;
    }

    public Integer getTotalSite() {
        return totalSite;
    }

    public void setTotalSite(Integer totalSite) {
        this.totalSite = totalSite;
    }

    public String getFirstUserName() {
        return firstUserName;
    }

    public void setFirstUserName(String firstUserName) {
        this.firstUserName = firstUserName;
    }

    public String getFirstUserPhone() {
        return firstUserPhone;
    }

    public void setFirstUserPhone(String firstUserPhone) {
        this.firstUserPhone = firstUserPhone;
    }


    public class PieChartModel{

        @SerializedName("commercial")
        @Expose
        public float commercial;
        @SerializedName("industrial")
        @Expose
        public float industrial;
        @SerializedName("institutional")
        @Expose
        public float institutional;
        @SerializedName("religious")
        @Expose
        public float religious;
        @SerializedName("residential")
        @Expose
        public float residential;
        @SerializedName("others")
        @Expose
        public float others;

    }

}
