package com.solarginni.DBModel.RequestLovs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RequestLoVs implements Serializable {
    @SerializedName("requestType")
    @Expose
    private RequestType requestType;
    @SerializedName("requestCategory")
    @Expose
    private RequestCategory requestCategory;
    @SerializedName("amc")
    @Expose
    private Boolean amc;
    @SerializedName("siteIds")
    @Expose
    public List<String> siteIds = new ArrayList<>();

    public RequestType getRequestType() {
        return requestType;
    }

    public void setRequestType(RequestType requestType) {
        this.requestType = requestType;
    }

    public RequestCategory getRequestCategory() {
        return requestCategory;
    }



    public Boolean getAmc() {
        return amc;
    }


}
