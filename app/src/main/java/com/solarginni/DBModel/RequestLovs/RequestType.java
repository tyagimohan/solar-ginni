package com.solarginni.DBModel.RequestLovs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class RequestType implements Serializable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("lovType")
    @Expose
    private String lovType;
    @SerializedName("lovValues")
    @Expose
    private List<String> lovValues;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<String> getLovValues() {
        return lovValues;
    }

}
