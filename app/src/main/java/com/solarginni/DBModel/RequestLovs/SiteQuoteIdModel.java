package com.solarginni.DBModel.RequestLovs;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SiteQuoteIdModel implements Serializable {
    @SerializedName("siteId")
    @Expose
    public ArrayList<String> siteIdList = new ArrayList<>();
    @SerializedName("quoteId")
    @Expose
    public ArrayList<String> quoteIdList = new ArrayList<>();

}
